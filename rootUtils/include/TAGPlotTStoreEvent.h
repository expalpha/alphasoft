#if BUILD_AG
#ifndef __TAGPLOTTStoreEvent__
#define __TAGPLOTTStoreEvent__


#include "TAGPlot.h"


class TAGPlotTStoreEvent: public TAGPlot
{
   public:
    TAGPlotTStoreEvent();
    ~TAGPlotTStoreEvent();
    
    virtual void LoadRun(const int runNumber, const double firstTime, const double lastTime, int verbose = 1);

    ClassDef(TAGPlotTStoreEvent, 1)
};

#endif
#endif