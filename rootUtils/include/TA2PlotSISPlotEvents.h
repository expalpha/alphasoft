#if BUILD_A2

#ifndef _TA2PLOTSISPLOTEVENTS_
#define _TA2PLOTSISPLOTEVENTS_

#include "TAPlotScalerEvents.h"

#include "TSISEvent.h"
#include "TSISChannels.h"
/// @brief Class for SIS data in ALPHA2 for the TA2Plot class
class TA2PlotSISPlotEvents: public TAPlotScalerEvents
{
   private:
      std::vector<TSISChannel> fSISChannel; ///< Vector of TSISChannels
   public:
      TA2PlotSISPlotEvents();
      ~TA2PlotSISPlotEvents();
      TA2PlotSISPlotEvents(const TA2PlotSISPlotEvents& sisPlotEvents);
      TA2PlotSISPlotEvents operator+=(const TA2PlotSISPlotEvents &rhs);
      TA2PlotSISPlotEvents& operator=(const TA2PlotSISPlotEvents& sisPlotEvents);
      friend TA2PlotSISPlotEvents operator+(const TA2PlotSISPlotEvents& lhs, const TA2PlotSISPlotEvents& rhs);
      void AddEvent(const int runNumber, const double time, const double officialTime, const int counts, const TSISChannel& channel);
      /// @brief Get run number of event at index event
      /// @param event 
      /// @return 
      /// Performs a range check on read
      int GetEventRunNumber(int event) const { return fRunNumber.at(event); }
      
      std::string CSVTitleLine() const override;
      std::string CSVLine(size_t i) const override;
      
      int CountTotalCountsInChannel(const TSISChannel& ch) const;
      /// @brief Get the SIS channel at index event
      /// @param index 
      /// @return 
      /// Does not perform range check on read
      const TSISChannel& SISChannel(size_t index) { return fSISChannel[index]; }
      ClassDefOverride(TA2PlotSISPlotEvents,2);
};

#endif
#endif