#ifndef _TA2PLOTHHELIXEVENTS_
#define _TA2PLOTHHELIXEVENTS_

#ifdef BUILD_A2

#include "TObject.h"
#include <vector>
#include "TAPlotVertexEventsPassedCuts.h"

class TA2PlotHelixEvents : public TObject {
public:
   std::vector<int>    fRunNumber;
   std::vector<double> fTime; // Plot time (based off official time)
   std::vector<double> fOfficialTime;
   std::vector<double> Chi2;
   std::vector<double> parD;
   std::vector<double> Curvature;
   std::vector<double> Phi;
   std::vector<double> Lambda;
   std::vector<double> Z0;
   std::vector<int>    nPoints;
   std::vector<TAPlotVertexEventsPassedCuts> fCutResults;
   TA2PlotHelixEvents();

   ~TA2PlotHelixEvents();
   // Coipy ctor
   TA2PlotHelixEvents(const TA2PlotHelixEvents &h);

   TA2PlotHelixEvents &operator=(const TA2PlotHelixEvents &h);
   TA2PlotHelixEvents &operator+=(const TA2PlotHelixEvents &h);

   const TAPlotVertexEventsPassedCuts &CutsResults(const size_t index) const { return fCutResults[index]; }

   void AddEvent(int runNumber, double time, double officialTime, double chi2,
                 double pard, double curvature, double phi, double lambda, double z0,
                int npoints, const std::vector<bool>& cuts);

   size_t size() const { return fRunNumber.size(); }

   void clear();
   ClassDef(TA2PlotHelixEvents, 1);
};

#endif
#endif
