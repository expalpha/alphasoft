#ifndef _TEnvDataPlot_
#define _TEnvDataPlot_

#include "TObject.h"
#include <vector>
#include "TGraph.h"

/// Generic feLabVIEW / feGEM data inside a time window
///
/// Each data point contains the run number, the time (as provided by user), the run time (second since start of run),
/// and a data point (double)
class TAPlotEnvDataPlot : public TObject {
private:
   std::vector<int>    fRunNumber; ///< Run Number of events in time window
   std::vector<double> fTimes;     ///< Time (with whatever offset user has given) of data point
   std::vector<double> fRunTimes;  ///< Run Time (second since start of run) of data point
   std::vector<double> fData;      ///< Data Point at given time
public:
   TAPlotEnvDataPlot();
   virtual ~TAPlotEnvDataPlot();
   TAPlotEnvDataPlot(const TAPlotEnvDataPlot &envDataPlot);
   TAPlotEnvDataPlot operator=(const TAPlotEnvDataPlot &rhs);
   void              AddPoint(const int runno, const double time, const double runTime, const double data);
   TGraph           *NewGraph(bool zeroTime) const;
   /// @brief Get the number of entries in this container
   /// @return
   ///
   /// The size of fRunNumber, fTimes, fRunTimes, fData are always the same
   size_t size() const { return fRunNumber.size(); }
   /// @brief Return run number at index
   /// @param index
   /// @return
   ///
   /// Caution, there is no range check on the index and you can read beyond the end of the vector without warning
   int RunNumber(const size_t index) const { return fRunNumber[index]; }
   /// @brief Return the time of a an event at index
   /// @param index
   /// @return
   ///
   /// Caution, there is no range check on the index and you can read beyond the end of the vector without warning
   double Time(const size_t index) const { return fTimes[index]; }
   /// @brief Return the runtime (time in second since the begining of run) of a an event at index
   /// @param index
   /// @return
   ///
   /// Caution, there is no range check on the index and you can read beyond the end of the vector without warning
   double RunTime(const size_t index) const { return fRunTimes[index]; }
   /// @brief Return the value of an event at index
   /// @param index
   /// @return
   ///
   /// Caution, there is no range check on the index and you can read beyond the end of the vector without warning
   double Data(const size_t index) const { return fData[index]; }
   ClassDef(TAPlotEnvDataPlot, 1);
};

#endif
