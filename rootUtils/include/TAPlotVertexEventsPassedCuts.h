#ifndef _TAPlotVertexEventsPassedCuts_
#define _TAPlotVertexEventsPassedCuts_

#include "TObject.h"
#include <vector>
#include <iostream>

// Thin wrapper for std::vector since Root's object stream can't handle std::vector<std::vector<X>>
class TAPlotVertexEventsPassedCuts : public TObject {
public:
   std::vector<bool> fCuts;
   TAPlotVertexEventsPassedCuts();
   ~TAPlotVertexEventsPassedCuts();
   std::string       title() const;
   std::string       line() const;
   bool              at(const size_t i) const;
   void              push_back(const bool &r);
   void              push_back(const std::vector<bool> &rr);

   ClassDef(TAPlotVertexEventsPassedCuts, 1);
};

#endif
