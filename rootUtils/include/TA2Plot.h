#if BUILD_A2

#ifndef _TALPHA2PLOT_
#define _TALPHA2PLOT_
#include "TCanvas.h"
#include "TLatex.h"
#include "TAPlot.h"
#include "TSISEvent.h"
#include "TSVD_QOD.h"
#include "TSISChannels.h"
#include "TA2Spill.h"
#include "TA2SpillGetters.h"
#include "DoubleGetters.h"
#include "TStyle.h"

#include "TPaveText.h"

#include "TA2PlotSISPlotEvents.h"

class TA2Plot : public TAPlot {
protected:
   std::vector<TSISChannel> fSISChannels; ///< List of TSISChannel s we want to plot in histograms

private:
   // Detector SIS channels mapped to run number
   std::map<int, TSISChannel> fTrig;
   std::map<int, TSISChannel> fTrigNobusy;
   std::map<int, TSISChannel> fAtomOr;

   // Dump marker SIS channels mapped to run number:
   std::map<int, TSISChannel> fCATStart;
   std::map<int, TSISChannel> fCATStop;
   std::map<int, TSISChannel> fRCTStart;
   std::map<int, TSISChannel> fRCTStop;
   std::map<int, TSISChannel> fATMStart;
   std::map<int, TSISChannel> fATMStop;

   // Beam injection/ ejection markers mapped to run number:
   std::map<int, TSISChannel> fBeamInjection;
   std::map<int, TSISChannel> fBeamEjection;

   // Beam injection/ ejection markers mapped to run number:
   std::map<int, TSISChannel> fMWSynthStart;
   std::map<int, TSISChannel> fMWSynthStop;

   /// List of runs in container
   std::vector<TA2AnalysisReport> fAnalysisReports;

public:
   TA2PlotSISPlotEvents SISEvents;

   // Default members, operators, and prints.
   TA2Plot(bool zeroTime = true);
   TA2Plot(double zMin, double zMax, bool zeroTime = true);
   TA2Plot(const TA2Plot &object);

   virtual ~TA2Plot();
   void            Reset();
   TA2Plot        &operator=(const TA2Plot &rhs);
   TA2Plot        &operator+=(const TA2Plot &rhs);
   friend TA2Plot &operator+(const TA2Plot &lhs, const TA2Plot &rhs);

   // Setters and getters
   void                            SetSISChannels(int runNumber);
   const std::vector<TSISChannel> &GetSISChannels() const { return fSISChannels; }

   const TSISChannel GetTrigNoBusyChannel(int runNumber) const { return fTrigNobusy.at(runNumber); }
   const TSISChannel GetTrigChannel(int runNumber) const { return fTrig.at(runNumber); }

   std::pair<double, double> GetSISRate(const TSISChannel& ch)
   {
      int counts = SISEvents.CountTotalCountsInChannel(ch);
      return std::make_pair(counts / GetTotalTime(), sqrt(counts) / GetTotalTime());
   }
   std::pair<double, double> GetVertexRate()
   {
      int counts = GetNVertexEvents();
      return std::make_pair(counts / GetTotalTime(), sqrt(counts) / GetTotalTime());
   }

   std::pair<double, double> GetPassCutRate(const std::string& cuts)
   {
      int counts = GetNPassedType(cuts);
      return std::make_pair(counts / GetTotalTime(), sqrt(counts) / GetTotalTime());
   }
   

   std::set<int> GetRunsInContainer() const {
      std::set<int> runs;
      for (const auto &report : fAnalysisReports) {
         runs.insert(report.GetRunNumber());
      }
      return runs;
   }

   // Adding events and dumps
   virtual int AddSVDEvent(const TSVD_QOD &SVDEvent);
   int AddSISEvent(const TSISEvent &SISEvent);

private:
   int AddEvent(const TSISEvent &event, const TSISChannel &channel, const double timeOffset = 0);
   int AddEvent(const TSVD_QOD &event, const double timeOffset = 0);

public:
   void AddDumpGates(const int runNumber, const std::vector<std::string> description, const std::vector<int> dumpIndex);
   void AddDumpGates(const int runNumber, const std::vector<TA2Spill> spills);
   // If spills are from one run, it is faster to call the function above
   void AddDumpGates(const std::vector<TA2Spill> spills);
   using TAPlot::AddTimeGate;
   void AddTimeGate(const int runNumber, const double tmin, const double tmax, const double tzero);
   // Load, fill, draw, or save the object
   void     LoadRun(int runNumber, double firstTime, double lastTime, int verbose);
   void     SetUpHistograms();
   void     FillHisto(int CutsMode = 1);
   void     FillHisto(const std::string &CutsMode = "1");
   TCanvas *DrawCanvas(const char *name = "cVTX", int CutsMode = 1);
   TCanvas *DrawVertexCanvas(const char *name = "cVTX", int CutsMode = 1) { return DrawCanvas(name, CutsMode); }
   void     ExportCSV(const std::string filename, const std::string options = "avts");

   friend class TA2Plot_Filler;
   ClassDef(TA2Plot, 2)
};

#endif
#endif
