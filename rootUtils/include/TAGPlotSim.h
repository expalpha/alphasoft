#if BUILD_AG
#ifndef __TAGPLOTSIM__
#define __TAGPLOTSIM__

#include "TAGPlot.h"

class TAGPlotSim : public TAGPlot
{
   protected:
   public:

      std::vector<double> fSimVertexX;
      std::vector<double> fSimVertexY;
      std::vector<double> fSimVertexZ;
      std::vector<double> fRecoVertexX;
      std::vector<double> fRecoVertexY;
      std::vector<double> fRecoVertexZ;

      TAGPlotSim(bool zeroTime = true);
      TAGPlotSim(double zMin, double zMax,bool zeroTime = true);
      TAGPlotSim(const TAGPlotSim& object);

      virtual ~TAGPlotSim();
      void Reset();
      TAGPlotSim& operator=(const TAGPlotSim& rhs);
      TAGPlotSim& operator+=(const TAGPlotSim& rhs);
      friend TAGPlotSim& operator+(const TAGPlotSim& lsh, const TAGPlotSim& rhs);

      void AddEvent(const TStoreEvent& event);
      void LoadRun(const int runNumber);
    
   public:
      void SetupSimHistos();
      void FillSimHisto();
      TCanvas* DrawSimCanvas(TString Name);

    ClassDef(TAGPlotSim, 1)
   
};


#endif
#endif
