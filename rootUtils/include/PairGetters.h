#ifndef _PAIRGETTERS_
#define _PAIRGETTERS_
#include "IntGetters.h"

#include <utility>



#ifdef BUILD_A2
#include "TSISEvent.h"
#include "TSISChannelGetters.h"
#include "TA2Spill.h"
std::vector<std::pair<double,int>> GetSISTimeAndCounts(Int_t runNumber, TSISChannel SIS_Channel,         std::vector<double> tmin, std::vector<double> tmax);
std::vector<std::pair<double,int>> GetSISTimeAndCounts(Int_t runNumber, const char* ChannelName, std::vector<double> tmin, std::vector<double> tmax);
std::vector<std::pair<double,int>> GetSISTimeAndCounts(Int_t runNumber, TSISChannel SIS_Channel,         const std::vector<TA2Spill>& spills);
std::vector<std::pair<double,int>> GetSISTimeAndCounts(Int_t runNumber, const char* ChannelName, const std::vector<TA2Spill>& spills);

#endif


#include "TStoreLabVIEWEvent.h"
std::vector<std::pair<double,double>> GetLVData(Int_t runNumber, const char* BankName, int ArrayNo, double tmin, double tmax);
#ifdef BUILD_A2
std::vector<std::pair<double,double>> GetLVData(Int_t runNumber, const char* BankName, int ArrayNo, const TA2Spill& spill);
#endif

#ifdef BUILD_AG
#include "TAGSpill.h"
std::vector<std::pair<double,int>> GetRunTimeOfChronoCount(Int_t runNumber, TChronoChannel chan, std::vector<double> tmin, std::vector<double> tmax);
std::vector<std::pair<double,int>> GetRunTimeOfChronoCount(Int_t runNumber, const char* ChannelName, std::vector<double> tmin, std::vector<double> tmax);
std::vector<std::pair<double,int>> GetRunTimeOfChronoCount(Int_t runNumber, TChronoChannel chan, const std::vector<TAGSpill>& spills);
std::vector<std::pair<double,int>> GetRunTimeOfChronoCount(Int_t runNumber, const char* ChannelName, const std::vector<TAGSpill>& spills);
#endif

#include "TStoreGEMEvent.h"
std::vector<std::pair<double,double>> GetFEGData(Int_t runNumber, const char* name, double firstTime, double lastTime, double index);
std::vector<std::pair<double,std::vector<double>>> GetFEGData(Int_t runNumber, const char* name, double firstTime, double lastTime);

std::pair<double,double> GetFEGPower(Int_t runNumber, const char* name, double firstTime, double lastTime, double offset);
std::pair<double,double> GetFEGPower(Int_t runNumber, const char* name, double firstTime, double lastTime, double offset, std::vector<double>& pbuf);
#endif
