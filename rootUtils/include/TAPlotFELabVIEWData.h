#ifndef _TAPLOTFELABVIEWDATA_
#define _TAPLOTFELABVIEWDATA_

#include "TAPlotEnvData.h"
#include "TStoreLabVIEWEvent.h"
#include "TAPlotTimeWindows.h"

/// feLabVIEW data container for the TAPlot (derrived from TAPlotEnvData)
class TAPlotFELabVIEWData : public TAPlotEnvData {
public:
   TAPlotFELabVIEWData();
   ~TAPlotFELabVIEWData();
   TAPlotFELabVIEWData(const std::string &varname, const std::string label, const int index);
   /// @brief Add data from TTree (Add TStoreLabVIEWEvent)
   /// @tparam T
   /// @param runno
   /// @param gemEvent
   /// @param timeWindows
   ///
   /// Data is grouped internally by time window (TAPlotTimeWindows)
   void AddLVEvent(int runNumber, const TStoreLabVIEWEvent &labviewEvent, const TAPlotTimeWindows &timeWindows)
   {
      double time = labviewEvent.GetRunTime();
      // O^2 complexity atleast... There isn't usually allot of feGEM data so maybe we can live with this...?
      // Hopefully now better than On^2
      const int index = timeWindows.GetValidWindowNumber(labviewEvent.GetRunNumber(),time);
      if (index >= 0) {
         GetTimeWindow(index).AddPoint(runNumber, time - timeWindows.ZeroTime(index), time,
                                       labviewEvent.GetArrayEntry(TAPlotEnvData::GetDataIndex()));
      }
      return;
   }
   ClassDef(TAPlotFELabVIEWData, 1);
};

#endif
