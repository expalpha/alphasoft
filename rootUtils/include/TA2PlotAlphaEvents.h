#if BUILD_A2
#ifndef __TA2PLOTFULL__
#define __TA2PLOTFULL__

#include "TA2Plot.h"
#include "TAlphaEvent.h"
#include "TA2PlotHelixEvents.h"
#include "ChronoReader.h"

class TA2PlotAlphaEvents : public TA2Plot
{
   protected:
   public:
      TA2PlotHelixEvents fHelixEvents;

      TA2PlotAlphaEvents(bool zeroTime = true);
      TA2PlotAlphaEvents(double zMin, double zMax,bool zeroTime = true);
      TA2PlotAlphaEvents(double zMin, double zMax, int barCut, bool zeroTime = true);
      TA2PlotAlphaEvents(const TA2PlotAlphaEvents& object);

      virtual ~TA2PlotAlphaEvents();
      void Reset();
      TA2PlotAlphaEvents& operator=(const TA2PlotAlphaEvents& rhs);
      TA2PlotAlphaEvents& operator+=(const TA2PlotAlphaEvents& rhs);
      friend TA2PlotAlphaEvents& operator+(const TA2PlotAlphaEvents& lsh, const TA2PlotAlphaEvents& rhs);

      void AddAlphaEvent(const TAlphaEvent& event, const double timeOffset = 0);
      virtual void LoadRun(const int runNumber, const double tmin, const double tmax, int verbose);

   private:
      void ProcessHelices(const double runNumber, const double time, const double officialtime,
                          const std::vector<TAlphaEventHelix*> tracks, const std::vector<bool>& cuts);

   public:
      void SetupTrackHistos();
      void FillTrackHisto(const std::string& CutsMode);
      virtual void FillHisto(const std::string& CutsMode);
      TCanvas* DrawTrackCanvas(TString Name, const std::string& CutsMode);

    ClassDef(TA2PlotAlphaEvents, 1)
   
};


#endif
#endif
