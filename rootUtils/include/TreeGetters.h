#ifndef _TreeGetters_
#define _TreeGetters_
#include "TTreeReader.h"
#include "FileGetters.h"
#include "TSystem.h"

/// @brief A shared pointer to a TTree object
/// This wrapper for the TTree object ensures that the TTree object is deleted when the last reference to it is gone.
/// We can't use std::shared_ptr<TTree> directly because TTree needs the TFile object to be open.
/// Here I ensures that the TFile object kept in scope as long as the TTree object is in scope.
class TTreePointer {
private:
   std::shared_ptr<TTree> tree; ///< The TTree object
   TFilePointer file; ///< The file that the TTree is in
public:
   TTree& operator*() { return *tree.get(); }
   TTree* operator->() { return tree.get(); }
   bool operator==(std::nullptr_t) { return tree == nullptr; }
   // operator to check if(!tree) {...}
   bool operator!() { return !tree.get(); }
   // operator to check if(tree) {...}
   operator bool() { return tree.get(); }
   TTreePointer() : tree(nullptr), file(nullptr) {}
   TTreePointer(TFilePointer f, TTree* t) : tree(t), file(f) {}
   TTreePointer(TFilePointer f, const char* name) : tree((TTree*)f->Get(name)), file(f) {}
   ~TTreePointer() {
       tree = nullptr; // Release the tree before releasing the file
       file = nullptr;
   }
};


/// @brief A shared pointer to a TTreeReader object
/// This wrapper for the TTreeReader object ensures that the TTreeReader object is deleted when the last reference to it is gone.
/// We can't use std::shared_ptr<TTreeReader> directly because TTreeReader needs the TFile object to be open.
/// Here I ensures that the TFile object kept in scope as long as the TTreeReader object is in scope.
class TTreeReaderPointer {
   std::shared_ptr<TTreeReader> tree; ///< The TTreeReader object
   TFilePointer file; ///< The file that the TTreeReader is reading from
public:
    // dereference overload
   TTreeReader& operator*() { return *tree.get(); }
   TTreeReader* operator->() { return tree.get(); }
   bool operator==(std::nullptr_t) { return tree == nullptr; }
   // operator to check if(!tree) {...}
   bool operator!() { return !tree.get(); }
   // operator to check if(tree) {...}
   operator bool() { return tree.get(); }
    
   TTreeReaderPointer() : tree(nullptr), file(nullptr) {}
   TTreeReaderPointer(TFilePointer f, std::shared_ptr<TTreeReader> t) : tree(t), file(f) {}
   TTreeReaderPointer(TFilePointer f, const char* name) :
        tree(new TTreeReader(name,f.get())),
        file(f)
    {}
    ~TTreeReaderPointer()
    {
        tree = nullptr; // Release the tree before releasing the file
        file = nullptr;
    }
};

TTreePointer Get_Tree_By_Name(const int runNumber,const char* name);

#ifdef BUILD_AG

TTreeReaderPointer Get_AGSpillTree(Int_t runNumber);

#include "TChronoChannel.h"
#include "store_cb.h"

TTreePointer Get_Chrono_Tree(const int runNumber, const TChronoChannel& channel);
TTreeReaderPointer Get_Chrono_TreeReader(const int runNumber, const TChronoChannel& channel);
//TTreePointer Get_Chrono_Tree(Int_t runNumber, const char* ChannelName, double &official_time);
TTreePointer Get_Chrono_Name_Tree(const int  runNumber);

TTreePointer Get_StoreEvent_Tree(const int runNumber);
TTreeReaderPointer Get_StoreEvent_TreeReader(const int runNumber);
TTreeReaderPointer Get_AGDetectorEvent_TreeReader(const int runNumber);
TTreeReaderPointer Get_BarEvent_TreeReader(const int runNumber);
//Legacy:



#endif
TTreePointer Get_Seq_Event_Tree(Int_t runNumber);
TTreePointer Get_Seq_State_Tree(Int_t runNumber);

// ALPHA 2 Getters:
#ifdef BUILD_A2

// ALPHA 2 Getters:
TTreeReaderPointer A2_SIS_Tree_Reader(Int_t runNumber, Int_t SIS_Module);
TTreeReaderPointer Get_A2_SVD_Tree(Int_t runNumber);
TTreeReaderPointer Get_A2SpillTree(Int_t runNumber);

TTreeReaderPointer Get_TA2AnalysisReport_Tree(Int_t runNumber);

TTreeReaderPointer Get_TAlphaEvent_Tree(Int_t runNumber);
#endif

#ifdef BUILD_AG

TTreeReaderPointer Get_TAGAnalysisReport_Tree(Int_t runNumber);

#endif

TTreeReaderPointer Get_feGEM_Tree(Int_t runNumber, const std::string& Category, const std::string& Varname);
TTreeReaderPointer Get_feGEM_Tree(Int_t runNumber, const std::string& CombinedName);

TTreeReaderPointer Get_feLV_Tree(Int_t runNumber, const std::string& BankName);

std::vector<TTreeReaderPointer> Get_feGEM_File_Trees(Int_t runNumber, const std::string& CombinedName);



#endif


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
