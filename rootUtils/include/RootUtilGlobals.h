#ifndef _ROOTUTILGLOBALS_
#define _ROOTUTILGLOBALS_
#include "Sequencer_Channels.h"
#include "TSystem.h"
#include <iostream>
#include <cmath>
#include <tuple>
#include <vector>
#define DEBUG 0
namespace rootUtils {
   ///Enum for the time axis plotting modes
   /**
   * Presently limited to Fixed number of time bins and fixed width of time bin.
   * Using more modes should report at run time to the std error out (please uses case
   * switches when using these enums)
   */
   enum TimeAxisMode{
       /// Use fixed number of time bins
      kFixedTimeBinNumber,
      /// Use fixed width of time bins
      kFixedTimeBinSize
   };
   

   // Plotting axis related functions
   void SetDefaultBinNumber(int i = 100);
   int GetDefaultBinNumber();
   void SetTimeAxisMode(TimeAxisMode mode = kFixedTimeBinNumber);
   TimeAxisMode GetTimeAxisMode();
   void SetTimeBinNumber(int i = 100);
   int GetTimeBinNumber();
   void SetTimeBinSize(double size = 1.0);
   double GetTimeBinSize();
   int CalculateNumberOfBinsInRange(double min, double max);
   void PrintTimeAxisMode();


   void ShowDumpMarkers(const SEQUENCER_ID& ID);
   void HideDumpMarkers(const SEQUENCER_ID& ID);
   void ShowCATDumpMarkers();
   void ShowRCTDumpMarkers();
   void ShowATMDumpMarkers();
   void ShowPOSDumpMarkers();
   void ShowRCT_BOTGDumpMarkers();
   void ShowATM_BOTGDumpMarkers();
   void ShowATM_TOPGDumpMarkers();
   void ShowRCT_TOPGDumpMarkers();
   void ShowBMLDumpMarkers();
   bool GetDumpMarkerDisplayMode(const SEQUENCER_ID& ID);
   void ShowAllDumpMarkers();
   void DisableDumpMarkerDrawing();
   void PrintDumpMarkerDisplayMode();

   // Self triggering analysis functions
   bool AutoRunAnalyzer();
   int PermittedCommitsBehind();
   void SetPermittedCommitsBehind(int n_commits);
   void SetDefaultAnalyzer(const std::vector<std::string> analyzerList,
                           const std::vector<std::string> arguments,
                           const std::vector<std::string> module_flags);
   void SetDefaultAnalyzer(const std::vector<std::string> analyzers);
   std::vector<std::tuple<std::string,std::string,std::string>> GetAnalyzerList();
   void PrintDefaultAnalyzers();
   
   void DisableAnalysisReportCheck();
   bool AllowGetAnalysisReport();
   void PrintAnalysisReportCheckState();
   
   void AddPriorityMIDASFilePath(const std::string& absolute_path);
   void AddBackupMIDASFilePath(const std::string& absolute_path);
   std::vector<std::string> GetListOfPossibleMIDASFilePaths();
   void AddPriorityROOTFilePath(const std::string& absolute_path);
   void AddBackupROOTFilePath(const std::string& absolute_path);
   std::vector<std::string> GetListOfPossibleROOTFilePaths();
   void DisableOptimisedFileLoading();
   void EnableOptimisedFileLoading();
   bool IsOptimisedFileLoadingPermitted();

   // FileGetters function
   std::string GetFileNamePattern();
   
   void PrintGlobalSettings();
}
#endif
