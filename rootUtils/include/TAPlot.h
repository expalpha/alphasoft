
#ifndef _TALPHAPLOT_
#define _TALPHAPLOT_

#include "TObject.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TLegend.h"
#include "TreeGetters.h"
#include "TStoreGEMEvent.h"
#include "TStoreLabVIEWEvent.h"
#include <algorithm>
#include <iostream>
#include <fstream>
#include "AlphaColourWheel.h"
#include "TMultiGraph.h"
#include "TTimeStamp.h"
#include "AnalysisReportGetters.h"
#include <numeric>

#define SCALECUT 0.6

// Basic internal containers:

#include "TAPlotVertexEvents.h"

#include "TAPlotTimeWindows.h"

#include "TAPlotEnvDataPlot.h"
#include "TAPlotEnvData.h"

#include "TAPlotFEGEMData.h"
#include "TAPlotFELabVIEWData.h"

#include "TAPlotCutsMode.h"
// Default bin numbering
#include "RootUtilGlobals.h"

/// \class TAPlot
/// \brief A dataframe like abstract class for plotting Vertex data for ALPHA experiments
///
/// Key components are TAPlotTimeWindows (which stores the time windows for the TAPlot
/// and TAPlotVertexEvents (which contains the vertex data used to plot). The TAPlot's children
/// should be considered as DataFrames
class TAPlot : public TObject {
private:
   /// \brief Track if data has been loaded (so that we can catch if the user forgets to call LoadData())
   ///
   /// Track internal freshness of data in container, LoadData() will set this false. Adding new time
   /// windows (or extra required feLabVIEW feGEM variables) will set this true (requiring data to
   /// be loaded again)
   bool fDataStale = true;

protected:
   std::string fCanvasTitle;  ///< Used to give the TCanvas a title
   int         fDrawStyle;    ///< Switch between colour modes
   int         fLegendDetail; // = 1;

   double fTimeFactor = 1.; ///< Time scaling factor (used to switch between seconds and milliseconds)

   double fZMinCut =
      -std::numeric_limits<double>::infinity(); ///< Minimum of the Z cut (default off by setting it to -ve infinity)
   double fZMaxCut =
      std::numeric_limits<double>::infinity(); ///< Maximum of the Z cut (default off by setting it to +ve infinity)

   std::map<std::string, int> fHistoPositions; ///< Label fHistos array elemments
   TObjArray fHistos; ///< Hold historams in a vector like container so that saved TAGPlot objects can be backwards and
                      ///< forwards compatable

   std::vector<double>              fEjections;  ///< Timing of AD ejections
   std::vector<double>              fInjections; ///< Timing of AD injections
   std::vector<double>              fDumpStarts; ///< Timing of Dump Start Markers
   std::vector<double>              fDumpStops;  ///< Timing of Dump Stop Markers
   std::vector<int>                 fRuns;       ///< Unique runs in this container
   std::vector<TAPlotFEGEMData>     fFEGEM;      ///< Data from feGEM
   std::vector<TAPlotFELabVIEWData> fFELV;       ///< Data from feLabVIEW
private:
   TAPlotTimeWindows  fTimeWindows;  ///< Time windows in which the TAPlot (and children)
protected:
   TAPlotVertexEvents fVertexEvents; ///< Vertex events used for histogram

   TTimeStamp fObjectConstructionTime{0}; ///< Set at construction time, used to estimate the loading time of TAPlots
   TTimeStamp fDataLoadedTime{0}; ///< Set when loading data is finished, used to estimate the loading time of TAPlots

   const bool kZeroTimeAxis; ///< Use a time axis that counts from Zero of a dump window (default true)
   bool IsDataStale() const { return fDataStale; }
protected:
   /// @brief Loading data finished
   ///
   /// fDataLoadedTime is set to current time (used in combinations with fObjectConstructionTime a total loading time
   /// can be estimated) fDataStale is set false, the data in the TAPlot is full up to date
   void LoadingDataLoadingDone()
   {
      fDataLoadedTime = TTimeStamp();
      fDataStale      = false;
   }
   /// @brief  Set scaling factor for time Axis
   /// @param time
   ///
   ///  Used to switch to ms scale, although it is more flexible if we even need other time scales
   void SetTimeFactor(double time) { fTimeFactor = time; }
   // Setters defined in .cxx
   void SetGEMChannel(const TAPlotFEGEMData &gemchannel);
   void SetGEMChannel(const std::string &name, int arrayEntry, std::string title = "");
   void SetGEMChannel(const std::string &category, const std::string &varName, int arrayEntry, std::string title = "");
   void SetLVChannel(const TAPlotFELabVIEWData &lvchannel);
   void SetLVChannel(const std::string &name, int ArrayEntry, std::string title = "");

protected:
   /// @brief Set the title of the TAPlot
   /// @param title
   void SetTAPlotTitle(const std::string &title)
   {
      if (fDataStale) LoadData();
      fCanvasTitle = title;
   }

public:
   /// @brief Get the container of vertex data
   /// @return
   const TAPlotVertexEvents &GetVertexEvents() const { return fVertexEvents; }
   /// @brief Get the container of the plots time windows
   /// @return
   const TAPlotTimeWindows &GetTimeWindows() const { return fTimeWindows; }
   /// @brief Get the title of the TAPlot
   /// @return
   std::string GetTAPlotTitle() const { return fCanvasTitle; }
   /// @brief Get the nunmber of vertex events in fVertexEvents
   /// @return
   size_t GetNVertexEvents() const { return fVertexEvents.size(); }
   /// @brief Get the time scaling factor
   /// @return
   double GetTimeFactor() const { return fTimeFactor; }
   /// @brief Get the time (in seconds) of the longest time window
   /// @return
   double GetMaxDumpLength() const { return fTimeWindows.GetMaxDumpLength(); }
   /// @brief Get the first start of all time windows
   /// @return
   double GetFirstTmin() const { return fTimeWindows.GetFirstTMin(); }
   /// @brief Get the last stop of all time windows
   /// @return
   double GetLastTmax() const { return fTimeWindows.GetLastTMax(); }
   /// @brief Get the largest offset before 'zero' used for plotting on a 'zeroed' time axis
   /// @return
   double GetBiggestTBeforeZero() const { return fTimeWindows.GetBiggestTBeforeZero(); }
   /// @brief Get the largest offset after 'zero' used for plotting on a 'zeroed' time axis
   /// @return
   double GetBiggestTAfterZero() const { return fTimeWindows.GetBiggestTAfterZero(); }
   /// @brief Get the number of Vetex Types '1' events (consider reviewing this functionality for ALPHAg)
   /// @return
   int GetNVerticies() const { return GetNVertexType(1); }
   /// @brief Return true if there is feGEM data in this container
   /// @return
   bool IsGEMData() { return (fFEGEM.size() != 0); }
   /// @brief Return tru eif there is feLabVIEW data in this container
   /// @return
   bool IsLVData() { return (fFEGEM.size() != 0); }
   /// @brief Container for 1D and 2D histograms
   /// @return
   const TObjArray &GetHisto() const { return fHistos; }
   /// @brief Get map of histograms to string keys
   /// @return
   const std::map<std::string, int> &GetHistoPosition() const { return fHistoPositions; }
   /// @brief Get a vector of unqiue run numbers included in this container
   /// @return
   const std::vector<int> &GetArrayOfRuns() const { return fRuns; }
   /// @brief Get a vector (one element for each element in TAPlotTimeWindows ) of the LabVIEW data
   /// @return
   const std::vector<TAPlotFELabVIEWData> GetLVData() const { return fFELV; }
   /// @brief Get a vector (one element for each element in TAPlotTimeWindows ) of the feGEM data
   /// @return
   const std::vector<TAPlotFEGEMData> &GetGEMChannels() const { return fFEGEM; };
   /// @brief Get a vector (one element for each element in TAPlotTimeWindows ) of the LabVIEW data
   /// @return
   const std::vector<TAPlotFELabVIEWData> &GetLVChannels() const { return fFELV; }
   std::pair<TLegend *, TMultiGraph *>     GetGEMGraphs();
   std::pair<TLegend *, TMultiGraph *>     GetLVGraphs();
   double                                  GetApproximateProcessingTime();
   int                                     GetNPassedType(const int type) const;
   int                                     GetNPassedType(const std::string &cuts) const;
   virtual int                             GetNVertexType(const int type) const;
   TString                                 GetListOfRuns();
   /// @brief Get the total time of all time windows in container
   /// @return
   double GetTotalTime() { return fTimeWindows.GetTotalTime(); }
protected:
   /// @brief Wrapper for access to fTimeWindows to add a time window. Only set by child classes
   void AddTimeWindow(const int runNumber, const double tmin, const double tmax, const double tzero)
   {
      AddRunNumber(runNumber);
      fDataStale = true;
      return fTimeWindows.AddTimeWindow(  runNumber, tmin, tmax, tzero);
   }
public:
   /// @brief Add dump by string
   /// @param runNumber
   /// @param description
   /// @param dumpIndex
   ///
   /// Wild card * is supported, eg "*RampDown" will fetch all dumps that end with the string "RampDown"
   /// If dumpIndex is negative, it will add multiple dumps, otherise it can be used as an index to select specific
   /// dumps (index counts from 0)
   virtual void AddDumpGates(int runNumber, std::vector<std::string> description, std::vector<int> dumpIndex) = 0;
   /// @brief Add start dump marker (line in the t histogram)
   /// @param time
   void AddStartDumpMarker(double time) { fDumpStarts.push_back(time); }
   /// @brief Add stop dump marker (line in the t histogram)
   /// @param time
   void AddStopDumpMarker(double time) { fDumpStops.push_back(time); }
   /// @brief Add the timing of a 'beam injection'
   /// @param time 
   /// 
   /// Used to mark the timing on the T plots as a line
   void AddInjection(double time) { fInjections.push_back(time); }
   /// @brief Add the timing of a 'beam ejection'
   /// @param time
   ///
   /// Used to mark the timing on the T plots as a line
   void AddEjection(double time) { fEjections.push_back(time); }

protected:
   void AddRunNumber(int runNumber);

public:
   void AddTimeGates(int runNumber, std::vector<double> minTimes, std::vector<double> maxTimes,
                     std::vector<double> timeZeros);
   void AddTimeGates(int runNumber, std::vector<double> minTimes, std::vector<double> maxTimes);
   void AddTimeGate(const int runNumber, const double minTime, const double maxTime);
   /// @breif A pure virtual function for adding a time window to the TAPlotTimeWindows class. tmax must be valid
   /// in TAPlotTimeWindows class, so child implementations of the TAPlot must interpret `-1` as the total time of the
   /// run, the source for this time differs between ALPHA2 and ALPHAg
   virtual void AddTimeGate(const int runNumber, const double minTime, const double maxTime, const double zeroTime) = 0;
   void AddVertexEvent(int runNumber, int eventNo, std::vector<bool> cutsResult, int vertexStatus, double x, double y,
                       double z, double t, double eventTime, double eunTime, int numHelices, int numTracks, double mva);

protected:
   // Load data functions.
   template <typename T>
   void LoadFEGEMData(int runNumber, TAPlotFEGEMData &gemData, TTreeReaderPointer gemReader, const char *name,
                      double firstTime, double lastTime, int verbose);
   void LoadFEGEMData(int runNumber, double firstTime, double lastTime, int verbose);
   void LoadFELVData(int runNumber, TAPlotFELabVIEWData &labviewData, TTreeReaderPointer labviewReader, const char *name,
                     double firstTime, double lastTime, int verbose);
   void LoadFELVData(int runNumber, double firstTime, double lastTime, int verbose);
   virtual void LoadRun(int runNumber, double firstTime, double lastTime, int verbose) = 0;
private:
   template <typename T>
   int ClearContainer(T& container, const std::string name);
protected:
   void ClearContainers();

public:
   void LoadData(int verbose = 1);

   // Default members, operators, and prints.
   TAPlot(const TAPlot &object);
   TAPlot(bool zeroTime = true); //, int MVAMode = 0);
   virtual ~TAPlot();
   TAPlot &operator=(const TAPlot &rhs);
   TAPlot &operator+=(const TAPlot &rhs);
   // Addition constructor...? Is this reasonable practice?
   TAPlot(const TAPlot &lhs, const TAPlot &rhs);
   using TObject::Print;
   void         Print() const;
   virtual void PrintFull();

   // Histogram functions
private:
   void AutoTimeRange();

protected:
   /// @brief Add histogram to fHisto container and track its array position with a map
   /// @tparam T 
   /// @param keyname 
   /// @param h 
   template <class T>
   void AddHistogram(const char *keyname, T *h) // This refuses to go in the .cxx for some reason.
   {
      fHistos.Add(h);
      fHistoPositions[keyname] = fHistos.GetEntries() - 1;
   }

   void FillHistogram(const char *keyname, double x, int counts);
   void FillHistogram(const char *keyname, double x);
   void FillHistogram(const char *keyname, double x, double y);

public:
   TH1D  *GetTH1D(const char *keyname);
   TH2D  *GetTH2D(const char *keyname);
   double GetTH1DMaximum(std::vector<std::string> keys);

protected:
   void     DrawHistogram(const char *keyname, const char *settings);
   TLegend *DrawLines(TLegend *legend, const char *keyname);
   TLegend *AddLegendIntegral(TLegend *legend, const char *message, const char *keyname);
   void     ClearHisto();
   void     SetUpHistograms();

public:
   void WriteEventList(std::string fileName, bool append = true, int cut = -1);
   void ApplyMVA(const int index, const double rfoutValue);
   void ExportCSV(const std::string filename, const std::string options = "vt");

   ClassDef(TAPlot, 1);
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

