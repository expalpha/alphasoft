#ifndef _TH1DGetter_
#define _TH1DGetter_

#include "TH1D.h"

#include "DoubleGetters.h"
#include "TStringGetters.h"

#include "RootUtilGlobals.h"

#ifdef BUILD_AG
#include "TAGSpillGetters.h"

/// Get Chronobox rates summed for all requested time windows
std::vector<TH1D*> Get_Summed_Chrono(Int_t runNumber, std::vector<TChronoChannel> chan, std::vector<double> tmin, std::vector<double> tmax,  double range = -1);

/// Get Chronobox rates summed for all requested spills
std::vector<TH1D*> Get_Summed_Chrono(Int_t runNumber, std::vector<TChronoChannel> chan, std::vector<TAGSpill> spills);

/// Get Chronobox rates summed for all requested spill descriptions
std::vector<TH1D*> Get_Summed_Chrono(Int_t runNumber, std::vector<std::string> description, std::vector<int> dumpIndex );

/// Get Chronobox rates summed for all requested spill descriptions
std::vector<TH1D*> Get_Summed_Chrono(Int_t runNumber, std::vector<std::string> Chrono_Channel_Names, std::vector<std::string> description, std::vector<int> dumpIndex );

/// Get Chronobox rates for each requested channel and time window
std::vector<std::vector<TH1D*>> Get_Chrono(Int_t runNumber, std::vector<TChronoChannel> chan, std::vector<double> tmin, std::vector<double> tmax,const std::vector<std::string> time_window_labels = {}, double range = -1);

/// Get Chronobox rates for each requested channel and spill
std::vector<std::vector<TH1D*>> Get_Chrono(Int_t runNumber, std::vector<TChronoChannel> chan, std::vector<TAGSpill> spills);

/// Get Chronobox rates for each requested channel and spill description
std::vector<std::vector<TH1D*>> Get_Chrono(Int_t runNumber, std::vector<std::string> description, std::vector<int> dumpIndex );

/// Get Chronobox timestamp difference between events
TH1D* Get_Delta_Chrono(Int_t runNumber, TChronoChannel chan, Double_t tmin=0., Double_t tmax=-1., Double_t PlotMin=0., Double_t PlotMax=2.);

/// Get Chronobox timestamp difference between events
TH1D* Get_Delta_Chrono(Int_t runNumber, TChronoChannel chan, const char* description, Int_t dumpIndex=0);

/// Get Chronobox timestamp difference between events
TH1D* Get_Delta_Chrono(Int_t runNumber, const char* ChannelName, Double_t tmin=0., Double_t tmax=-1.);

/// Get Chronobox timestamp difference between events
TH1D* Get_Delta_Chrono(Int_t runNumber, const char* ChannelName, const char* description, Int_t dumpIndex=0);
#endif

#ifdef BUILD_A2
#include "TA2SpillGetters.h"
#include "TSISEvent.h"
#include "TSISChannels.h"
//ALPHA2
std::vector<TH1D*> Get_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<double> tmin, std::vector<double> tmax, double range = -1);
std::vector<TH1D*> Get_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<TA2Spill> spills);
std::vector<std::vector<TH1D*>> Get_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<double> tmin, std::vector<double> tmax);
std::vector<std::vector<TH1D*>> Get_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<TA2Spill> spills);
#endif



#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
