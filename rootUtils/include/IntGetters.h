#ifndef _IntGetters_
#define _IntGetters_
#include "TChronoChannel.h"
#include "TChronoChannelName.h"
#if BUILD_AG
#include "TStoreEvent.hh"
#endif
#include "TreeGetters.h"
#include "DoubleGetters.h"
#include "PairGetters.h"

#if BUILD_AG
#include "TAGSpill.h"
#include "TAGSpillGetters.h"
Int_t Get_Chrono_Channel_In_Board(Int_t runNumber, const std::string& ChronoBoard, const char* ChannelName, Bool_t ExactMatch=kFALSE);
Int_t GetCountsInChannel(Int_t runNumber,  TChronoChannel channel, double tmin = 0., double tmax = -1.);
Int_t GetCountsInChannel(Int_t runNumber,  TChronoChannel channel, std::vector<double> tmin, std::vector<double> tmax);
Int_t GetCountsInChannel(Int_t runNumber,  TChronoChannel channel, std::vector<TAGSpill> spills);
Int_t GetCountsInChannel(Int_t runNumber,  TChronoChannel channel, std::vector<std::string> description, std::vector<int> index);

Int_t GetCountsInChannel(Int_t runNumber,  const char* ChannelName, double tmin = 0., double tmax = -1.);
Int_t ApplyCuts(TStoreEvent* e);

#endif

#if BUILD_A2
#include "TSISChannel.h"

int Count_SIS_Triggers(int runNumber, TSISChannel ch, std::vector<double> tmin, std::vector<double> tmax);

#endif
//*************************************************************
// Energy Analysis
//*************************************************************

Int_t LoadRampFile(const char* filename, std::vector<double>& x, std::vector<double>& y);
//*************************************************************

int GetRunNumber( TString fname );
#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
