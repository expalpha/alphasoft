#ifndef _TAPLOTCUTSMODE_
#define _TAPLOTCUTSMODE_
#include <string>
#include <vector>

#include "TAPlotVertexEventsPassedCuts.h"
#if BUILD_AG
#include "TAGDetectorEvent.hh"
#endif
class TAPlotCutsMode
{
   private:
      const std::string fCutsString;
      std::vector<std::vector<int>> fCutsMode;

   public:
      TAPlotCutsMode(const std::string& s, bool verbose = false) : fCutsString(s)
      {
         const std::vector<std::string> OR_statements = TokeniseByOR(s);
         fCutsMode = DecodeCutStrings(OR_statements);
         if (!verbose)
            return;
         Print();
      }
      ~TAPlotCutsMode()
      {
         fCutsMode.clear();
      }

      void Print();
      std::string FullCutName() const;
      /*
      inline std::vector<std::vector<int>>::const_iterator begin() const
      {
         return fCutsMode.begin();
      }
      inline std::vector<std::vector<int>>::const_iterator end() const
      {
         return fCutsMode.end();
      }*/
      inline bool ApplyCuts(const TAPlotVertexEventsPassedCuts & cuts) const
      {
         for (const std::vector<int>& AndStatement: fCutsMode)
         {
            bool passed = true;
            for (const int& i: AndStatement)
            {
               if (i >= 0)
               {
                  if (!cuts.at(i))
                  {
                     passed = false;
                  }
               }
               // Negative numbers are 'not' statements, offset by 1
               else
               {
                  if (cuts.at(- i - 1))
                  {
                     passed = false;
                  }
               }
               // We've already hit a fail in the AND statment... break
               if (!passed)
                  break;
            }
            if (passed)
               return true;
         }
         return false;
      }
   private:
      std::vector<std::string> TokeniseByOR( const std::string& cuts) const;
      std::vector<int> DecodeCutString(const std::string& cutsString) const;
      std::vector<std::vector<int>> DecodeCutStrings(const std::vector<std::string> cuts) const;
};

#endif