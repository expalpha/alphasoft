#ifndef _TAPlotEnvData_
#define _TAPlotEnvData_

#include "TAPlotEnvDataPlot.h"
#include "TObject.h"
#include <vector>
#include <string>

// Collection of feLabVIEW / feGEM data with the same name (the same source)
class TAPlotEnvData : public TObject {
private:
   std::string fVariableName; ///< Name of the Variable in MIDAS
   std::string fLabel;        ///< Human readable label added by user
   int         fDataIndex;    ///< Index of fVariableName
   /// @brief Data per time window
   ///
   /// TAPlotEnvDataPlot per TTimeWindow in TAPlot
   std::vector<TAPlotEnvDataPlot> fTimeWindowData;

public:
   /// @brief Return the variable name with its array index
   /// @return
   std::string GetVariable() const { return fVariableName + "[" + std::to_string(fDataIndex) + "]"; }
   /// @brief Return the variable name with its array index (without special characters)
   /// @return
   std::string GetVariableID() const { return fVariableName + "_" + std::to_string(fDataIndex); }
   /// @brief Return just the variable name
   /// @return
   const std::string &GetVariableName() const { return fVariableName; }
   /// @brief Return the human readable title added by the user
   /// @return
   std::string GetLabel() const { return fLabel; }
   /// @brief Get the array element index for this data
   /// @return
   int GetDataIndex() const { return fDataIndex; }

   TAPlotEnvData();
   TAPlotEnvData(const std::string name, const std::string label, const int arraynumber);
   virtual ~TAPlotEnvData();
   TAPlotEnvData(const TAPlotEnvData &envData);
   TAPlotEnvData             operator=(const TAPlotEnvData &rhs);
   std::pair<double, double> GetMinMax() const;
   TAPlotEnvDataPlot        &GetTimeWindow(const size_t index);
   const TAPlotEnvDataPlot  &GetTimeWindow(const size_t index) const;
   TGraph                   *BuildGraph(const size_t index, bool zeroTime);
   /// @brief return the size of fTimeWindowData
   /// @return fTimeWindowData.size()
   size_t size() const { return fTimeWindowData.size(); }
   /// @brief return true if fTimeWindowData is empty
   /// @return fTimeWindowData.empty()
   bool empty() const { return fTimeWindowData.empty(); }
   /// @brief Clear fTimeWindowData only (variable name, label and index remain unchanged)
   void clear() { fTimeWindowData.clear(); }
   ClassDef(TAPlotEnvData, 1);
};

bool operator==(const TAPlotEnvData &lhs, const TAPlotEnvData &rhs);

#endif
