#ifndef _BinaryRunners_
#define _BinaryRunners_

#include <vector>

int RunAnalyzer(const int runNumber);

void PrepareRuns(std::vector<int> runs, int max_threads = 4);

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
