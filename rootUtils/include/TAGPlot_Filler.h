#ifdef BUILD_AG
#ifndef _TAGPlot_Filler_
#define _TAGPlot_Filler_

#include "TAGPlot.h"
#include <set>

class TAGPlot_Filler {
private:
   std::vector<TAGPlot *>                plots; ///< Pointer to TA2Plots booked in for filling
   std::vector<int>                      runNumbers; ///< Vector of unque run numbers from TA2Plots booked for filling
   std::vector<TAPlotFEGEMData> GEMChannels; ///< List of feGEM channels we want to add to all TA2Plots
   std::vector<TAPlotFELabVIEWData>  LVChannels;///< List of feLabVIEW channels we want to add to all TA2Plots
   /// @brief Add runNumber to list of unique runs
   /// @param runNumber 
   ///
   /// Skip adding the run if its already in the list
   void AddRunNumber(int runNumber)
   {
      for (auto &no : runNumbers) {
         if (no == runNumber) {
            return;
         }
      }
      // std::cout<<"Add run number:"<<runNumber<<std::endl;
      runNumbers.push_back(runNumber);
   }

public:
   TAGPlot_Filler();
   ~TAGPlot_Filler();
   void BookPlot(TAGPlot *plot);
   void TrackGEMChannel(const TAPlotFEGEMData &channel);
   void TrackLVChannel(const TAPlotFELabVIEWData &channel);
private:
   void LoadfeGEMData(int runNumber, double first_time, double last_time, int verbose);
   void LoadfeLVData(int runNumber, double first_time, double last_time, int verbose);
   void LoadData(int runNumber, double first_time, double last_time, int verbose);
public:
   void LoadData(int verbose = 1);
};
#endif
#endif
