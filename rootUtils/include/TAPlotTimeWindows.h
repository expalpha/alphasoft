#ifndef _TAPlotTimeWindows_
#define _TAPlotTimeWindows_


#include "TObject.h"
#include <vector>
#include <assert.h>
#include <algorithm>
#include <string.h>
#include <numeric> //iota
#include <set>

/** \class TAPlotTimeWindows some ttext
A container for time windows for TAPlot

*/
class TAPlotTimeWindows : public TObject
{
   private:
      /// @brief Track if this container is sorted by time (fMinTime)
      ///
      /// This is set true in SortTimeWindows() only. Adding a time window will set it to false
      bool isSorted = false; 
      
      double fFirstTMin; ///< Track the earliest valid timestamp in all windows (used to minimise the time range to read
                      ///< root files)
      double fLastTMax;  ///< Track the latest valid timestamp in all windows (used to minimise the time range to read root
                        ///< files)
      double fBiggestTBeforeZero;  ///< Track Largest time before 'zero' (-ve number), used for plotting on a 'zeroed' time axis
      double fBiggestTAfterZero;  ///< Track Largest time after 'zero' (+ve number), used for plotting on a 'zeroed' time axis
      double fMaxDumpLength; ///< Track the longest time window

      std::vector<int>    fRunNumber;
      std::vector<double> fMinTime;
      std::vector<double> fMaxTime;
      std::vector<double> fZeroTime;
   public:
      //Standard ctor and dtor
      TAPlotTimeWindows();
      ~TAPlotTimeWindows();
      //Copy ctor
      TAPlotTimeWindows(const TAPlotTimeWindows& timeWindows);
      TAPlotTimeWindows& operator=(const TAPlotTimeWindows& rhs);
      friend TAPlotTimeWindows operator+(const TAPlotTimeWindows &lhs, const TAPlotTimeWindows &rhs);
      TAPlotTimeWindows operator+=(const TAPlotTimeWindows &rhs);
      void CheckForOverLap(int runNumber, double minTime, double maxTime);
      void AddTimeWindow(int runNumber, double minTime, double maxTime, double timeZero);

      int GetValidWindowNumber(const int runNumber, const double time); // Cannot be const because it will sort
      int GetValidWindowNumber(const int runNumber, const double time) const;
      double GetTimeToNextWindow(const int runNumber, const double time) const;

      std::pair<double, double> GetWindowTimeRange(const int runNumber) const;
   private:
      template <class T>
      static std::vector<T> reorder(std::vector<T>& nums, std::vector<size_t>& index)
      {
         const int kNumsSize = nums.size();
         std::vector<T> newest(kNumsSize);
         newest.resize(kNumsSize);
         for (int i = 0; i < kNumsSize; i++)
         {
            newest[i] = nums[ index[i] ] ;
         }
         nums.clear();
         return newest;  
      }
   public:
      void SortTimeWindows();
      std::string CSVTitleLine() const;
      std::string CSVLine(size_t i) const;
      size_t size() const
      {
         return fRunNumber.size();
      }

      int RunNumber(const size_t index) const { return fRunNumber[index]; }
      double MinTime(const size_t index) const { return fMinTime[index]; }
      double MaxTime(const size_t index) const { return fMaxTime[index]; }
      double ZeroTime(const size_t index) const { return fZeroTime[index]; }


      /// @brief Get the time (in seconds) of the longest time window
      /// @return
      double GetMaxDumpLength() const { return fMaxDumpLength; }

      /// @brief Get the first start of all time windows
      /// @return
      double GetFirstTMin() const { return fFirstTMin; }

      /// @brief Get the last stop of all time windows
      /// @return
      double GetLastTMax() const { return fLastTMax; }

      /// @brief Get the largest offset before 'zero' used for plotting on a 'zeroed' time axis
      /// @return
      double GetBiggestTBeforeZero() const { return fBiggestTBeforeZero; }
      
      double GetBiggestTAfterZero() const { return fBiggestTAfterZero; }

      /// @brief Get the total time of all time windows in container
      /// @return
      double GetTotalTime() 
      { 
         if(!isSorted) 
         {
            SortTimeWindows();
         }

         std::set<int> uniqueRuns;
         //This generates a list of unique runs.
         for(size_t i=0; i<fRunNumber.size(); i++)
         {
            uniqueRuns.insert(fRunNumber.at(i));
         }

         double runningCount=0;
         for(int run: uniqueRuns)
         {
            double maxTimeForThisRun = 0;
            for(size_t i=0; i<fRunNumber.size(); i++)
            {
               if(fRunNumber.at(i) == run) //now loop over time windows
               {
                  //RunNumbers match: check overlapping intervals
                  if(fMinTime.at(i)>=maxTimeForThisRun)
                  {
                     runningCount+=fMaxTime.at(i)-fMinTime.at(i);
                     maxTimeForThisRun=fMaxTime.at(i);
                  }
                  else if(fMinTime.at(i)<maxTimeForThisRun && fMaxTime.at(i)>maxTimeForThisRun)
                  {
                     runningCount+=fMaxTime.at(i)-maxTimeForThisRun;
                     maxTimeForThisRun=fMaxTime.at(i);
                  }
               }
            }
         }
         return runningCount; 
   }
      ClassDef(TAPlotTimeWindows, 2);
};


#endif
