#if BUILD_A2
#ifndef __TA2PlotWithGEM_h__
#define __TA2PlotWithGEM_h__

#include "TA2Plot.h"
#include "PairGetters.h"
#include "TA2Plot_Filler.h"

class TA2PlotWithGEM : public TA2Plot {

public:

   std::vector<std::string> varname;
   std::vector<std::string> label;
   std::vector<int> index;
   std::vector<int> precisions;

   std::vector<std::deque<double>> vtx_gem_data;

   // Default members, operators, and prints.
   TA2PlotWithGEM(bool zeroTime = true);
   TA2PlotWithGEM(double zMin, double zMax, bool zeroTime = true);
   TA2PlotWithGEM(const TA2PlotWithGEM &object);

   virtual ~TA2PlotWithGEM();
   void            Reset();
   TA2PlotWithGEM        &operator=(const TA2Plot &rhs);
   TA2PlotWithGEM        &operator+=(const TA2Plot &rhs);
   friend TA2PlotWithGEM &operator+(const TA2PlotWithGEM &lhs, const TA2PlotWithGEM &rhs);

   TA2PlotWithGEM &operator=(const TA2PlotWithGEM &rhs) {
      TA2Plot::operator=(rhs);
      return *this;
   }
   TA2PlotWithGEM &operator+=(const TA2PlotWithGEM &rhs) {
      TA2Plot::operator+=(rhs);
      return *this;
   }
   friend TA2PlotWithGEM &operator+(const TA2PlotWithGEM &lhs, const TA2PlotWithGEM &rhs) {
      TA2PlotWithGEM *sum = new TA2PlotWithGEM(lhs);
      *sum += rhs;
      return *sum;
   }

   void AddGEMVariable(std::string variable_name, int index, std::string label_name, int precision = 6);
   void LoadDataWithGEM(int verbose = 1);

   TGraph* GetGEMGraph(std::string name);

   void AssignGEMData();
   void ExportCSV(const std::string filename, const std::string options = "avts");


   ClassDef(TA2PlotWithGEM, 1)
};

#endif
#endif
