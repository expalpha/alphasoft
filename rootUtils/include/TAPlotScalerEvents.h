#ifndef _TAPLOTSCALERPLOTEVENTS_
#define _TAPLOTSCALERPLOTEVENTS_

#include "TObject.h"
#include <vector>

/// \class TAPlotScalerEvents
/// \brief Parent class for scaler like data (SIS and chronobox)
class TAPlotScalerEvents : public TObject {
protected:
   std::vector<int>    fRunNumber; ///< Run Number of event
   std::vector<double> fTime; ///< Plot time (based off official time)
   std::vector<double> fOfficialTime; ///< Calibrated run time from start of run
   std::vector<int>    fCounts; ///< Counts in this scaler channel

public:
   // Std ctor and dtor
   TAPlotScalerEvents();

   ~TAPlotScalerEvents();

   // Copy ctor - !!!
   TAPlotScalerEvents(const TAPlotScalerEvents &scalerPlotEvent);

   TAPlotScalerEvents &operator=(const TAPlotScalerEvents &scalerPlotEvents);

protected:
   void AddEvent(int runNumber, double time, double officialTime, int counts);

public:
   /// @brief Get run number of event with range check
   /// @param event Index 
   /// @return 
   int GetEventRunNumber(int event) const { return fRunNumber.at(event); }
   /// @brief Create string for colum titles for CSVLine function 
   /// @return
   ///
   /// Pure virtual function (child classes must implment this function) 
   virtual std::string CSVTitleLine() const = 0;
    /// @brief Export one event to std::string to writing to CSV file
   /// @return
   ///
   /// Pure virtual function (child classes must implment this function)   
   virtual std::string CSVLine(size_t i) const = 0;

   /// @brief Get number if events in container
   /// @return 
   size_t size() const { return fRunNumber.size(); }
   /// @brief Get run number at index
   /// @param index 
   /// @return 
   /// Caution there is no range check on the read
   int    RunNumber(const size_t index) const { return fRunNumber[index]; }
   /// @brief Get plot time at index
   /// @param index 
   /// @return 
   /// Caution there is no range check on the read
   double Time(const size_t index) const { return fTime[index]; }
   /// @brief Get run time at index
   /// @param index 
   /// @return 
   /// Caution there is no range check on the read
   double OfficialTime(const size_t index) const { return fOfficialTime[index]; }
   /// @brief Get the scaler counts of event
   /// @param index 
   /// @return 
   /// Caution there is no range check on the read
   int    Counts(const size_t index) const { return fCounts[index]; }
   ClassDef(TAPlotScalerEvents, 1);
};

#endif