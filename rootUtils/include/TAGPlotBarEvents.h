#ifndef _TAPLOTBAREVENTS_
#define _TAPLOTBAREVENTS_

#ifdef BUILD_AG

#include "TObject.h"
#include <vector>
#include <map>
#include "TBarEvent.hh"

class TAGPlotBarEvents : public TObject {
public:
   std::vector<int>    fRunNumber;
   std::vector<double> fTime; // Plot time (based off official time)
   std::vector<double> fOfficialTime;
   std::vector<int> fNTracksMatched;
   std::vector<int> fNTracksUnmatched;
   std::vector<int> fNPileUpTracks;
   std::vector<double> fVertexZ;
   std::vector<TBarEvent> fBarEvents; // Easier for now to just store the whole TBarEvent. Can rethink this if speed/memory becomes an issue.
   TAGPlotBarEvents();

   ~TAGPlotBarEvents();
   // Copy ctor
   TAGPlotBarEvents(const TAGPlotBarEvents &h);

   TAGPlotBarEvents &operator=(const TAGPlotBarEvents &h);
   TAGPlotBarEvents &operator+=(const TAGPlotBarEvents &h);

   void AddEvent(int runNumber, double time, double officialTime, int nmatch, int nunmatch, int npileup, const TBarEvent& barEvt, double vertexZ=-9999);

   size_t size() const { return fRunNumber.size(); }

   void clear();
   ClassDef(TAGPlotBarEvents, 2);
};

#endif
#endif
