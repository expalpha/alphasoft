#ifndef _TA2SpillGetters_
#define _TA2SpillGetters_


#ifdef BUILD_A2

#include <vector>
#include "TA2Spill.h"
#include "TreeGetters.h"

std::vector<TA2Spill> Get_A2_Spills(int runNumber, std::vector<std::string> description, std::vector<int> dumpIndex);
std::vector<TA2Spill> Get_All_A2_Spills(int runNumber);
std::vector<TA2Spill> Get_A2_Spills(const int runNumber, const double start_time, double stop_time);
#endif

#endif
