#ifndef _TAGPLOTSPACEPOINTEVENT_
#define _TAGPLOTSPACEPOINTEVENT_

#include "TObject.h"
#include <vector>
#include "TAPlotVertexEventsPassedCuts.h"

class TAGPlotSpacePointEvent: public TObject
{
public:
   std::vector<int> fRunNumber;
   std::vector<double> fTime;  //Plot time (based off official time)
   std::vector<double> fOfficialTime;
   std::vector<double> fX;
   std::vector<double> fY;
   std::vector<double> fZ;
   std::vector<double> fR;
   std::vector<double> fP;
   std::vector<TAPlotVertexEventsPassedCuts> fCutResults;
   TAGPlotSpacePointEvent();
   virtual ~TAGPlotSpacePointEvent();
   TAGPlotSpacePointEvent(const TAGPlotSpacePointEvent& s);

   TAGPlotSpacePointEvent& operator=(const TAGPlotSpacePointEvent& s);
   TAGPlotSpacePointEvent& operator+=(const TAGPlotSpacePointEvent& s);
  
   const TAPlotVertexEventsPassedCuts &CutsResults(const size_t index) const { return fCutResults[index]; }
  
   void clear();
   size_t size() const;
   void AddEvent(
      int runNumber,
      double time,
      double officialTime,
      double x,
      double y,
      double z,
      double r,
      double p,
      const std::vector<bool>& cuts
   );
   ClassDef(TAGPlotSpacePointEvent,2)
};


#endif
