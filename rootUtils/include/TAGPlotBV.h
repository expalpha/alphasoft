#if BUILD_AG
#ifndef __TAGPLOTBV__
#define __TAGPLOTBV__

#include "TAGPlot.h"
#include "TAGPlotBarEvents.h"
#include "TBarMVAVars.h"
#include "ChronoReader.h"

class TAGPlotBV : public TAGPlot
{
   protected:
   public:
      TAGPlotBarEvents fPlotBarEvents;
      bool fLoadStoreEvent;

      TAGPlotBV(bool zeroTime = true);
      TAGPlotBV(double zMin, double zMax,bool zeroTime = true);
      TAGPlotBV(const TAGPlotBV& object);

      virtual ~TAGPlotBV();
      void Reset();
      TAGPlotBV& operator=(const TAGPlotBV& rhs);
      TAGPlotBV& operator+=(const TAGPlotBV& rhs);
      friend TAGPlotBV& operator+(const TAGPlotBV& lsh, const TAGPlotBV& rhs);

      void AddEvent(const TBarEvent& barEvent, const int runNumber, const double timeOffset = 0, const double vertexZ = -9999);
      virtual void LoadRun(const int runNumber, const double tmin, const double tmax, int verbose);
      void AddBarEvent(const TBarEvent& event, const int runNumber, const double vertexZ = -9999);

      void SetLoadStoreEvent(bool load) { fLoadStoreEvent = load; }

      void SetupBarHistos();
      void FillMVAHisto();
      void FillBarHisto(std::vector<int> barIDs = {});
      void FillTOFHisto(double min_dist=-1, std::vector<int> barIDs = {});
      virtual void FillHisto(const std::string& CutsMode);

      TCanvas* DrawZedCanvas(TString Name = "cZed", std::vector<int> barIDs = {});
      TCanvas* DrawInvestigationCanvas(TString Name = "cInvestigation", std::vector<int> barIDs = {});
      TCanvas* DrawTOFCanvas(TString Name = "cTOF", double min_dist=-1, std::vector<int> barIDs = {});
      TCanvas* DrawTOFAmpCanvas(TString Name = "cTOFAmp", double min_dist=-1, std::vector<int> barIDs = {});
      TCanvas* DrawMVACanvas(TString Name = "cMVA");
      TCanvas* DrawMVAvsZedCanvas(TString Name = "cMVAvsZed");
      TCanvas* DrawPileUpCanvas(TString Name = "cPileUp");
      TCanvas* DrawTOFFitCanvas(TString Name = "cTOF", double min_dist=-1, std::vector<int> barIDs = {});
      TCanvas* DrawDirectionCanvas(TString name = "cDirection");
      TCanvas* DrawTrackMatchCanvas(TString name = "cTrackMatch");
      TCanvas* DrawTOFDemonstration(TString name = "cTOFDemonstration", bool c_line=true);

      std::array<int,7> GetHitTypeCounts();
      TH1D* GetTopADC();
      TH1D* GetBotADC();

    ClassDef(TAGPlotBV, 3)
   
};


#endif
#endif
