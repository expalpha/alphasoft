#if BUILD_A2
#ifndef __TA2PlotMicrowave_h__
#define __TA2PlotMicrowave_h__

#include "TA2Plot.h"
#include "PairGetters.h"
#include "TA2Plot_Filler.h"
#include "TA2PlotWithGEM.h"

#define N_UWAVE_FREQS 400

class TA2PlotMicrowave : public TA2PlotWithGEM {

public:

   // List of microwave windows starts/stops/freqs/powers, per run, per microwave dump in that run
   std::map<int, std::vector<std::deque<double>>> fUwaveStarts;
   std::map<int, std::vector<std::deque<double>>> fUwaveStops;
   std::map<int, std::vector<std::deque<double>>> fUwaveFreqs;
   std::map<int, std::vector<std::deque<double>>> fUwavePowers;

   // List of frequencies and powers per vertex event
   std::vector<double> fFrequencies;
   std::vector<double> fPowers;

   // Default members, operators, and prints.
   TA2PlotMicrowave(bool zeroTime = true);
   TA2PlotMicrowave(double zMin, double zMax, bool zeroTime = true);
   TA2PlotMicrowave(const TA2PlotMicrowave &object);

   virtual ~TA2PlotMicrowave();
   void            Reset();
   TA2PlotMicrowave        &operator=(const TA2Plot &rhs);
   TA2PlotMicrowave        &operator+=(const TA2Plot &rhs);
   friend TA2PlotMicrowave &operator+(const TA2PlotMicrowave &lhs, const TA2PlotMicrowave &rhs);

   TA2PlotMicrowave &operator=(const TA2PlotMicrowave &rhs) {
      TA2Plot::operator=(rhs);
      return *this;
   }
   TA2PlotMicrowave &operator+=(const TA2PlotMicrowave &rhs) {
      TA2Plot::operator+=(rhs);
      return *this;
   }
   friend TA2PlotMicrowave &operator+(const TA2PlotMicrowave &lhs, const TA2PlotMicrowave &rhs) {
      TA2PlotMicrowave *sum = new TA2PlotMicrowave(lhs);
      *sum += rhs;
      return *sum;
   }

   const std::deque<double>& GetMicrowaveStartTimes(int run_number, int microwave_sweep_number) const {return fUwaveStarts.at(run_number).at(microwave_sweep_number);}
   const std::deque<double>& GetMicrowaveStopTimes(int run_number, int microwave_sweep_number) const {return fUwaveStops.at(run_number).at(microwave_sweep_number);}

   void LoadDataMicrowave(int verbose = 1);
   void FigureOutSweepTimes();
   void FigureOutMicrowaveFrequencies();
   void AssignMicrowaveFrequencies();

   void ExportCSV(const std::string filename, const std::string options = "avts");

   ClassDef(TA2PlotMicrowave, 1)
};

#endif
#endif
