#ifndef _CHRONO_READER_
#define _CHRONO_READER_

#include "TTreeReader.h"
#include "RootUtilGlobals.h"

template <typename T>
TTreeReaderPointer  FindStartOfTree(TTreeReaderPointer& ChronoReader, TTreeReaderValue<T>& ChronoEvent, int p, int r, double firstTime, double tolerance )
{
   if (! rootUtils::IsOptimisedFileLoadingPermitted())
      return ChronoReader;
    //Rates are never that high... 
   if (
         (firstTime < 15.)     // if firstTime is small, skip binary search
      ||                       // or
         (p == 0 && r == 0)    // binary search found the firstTime is smaller than the first event in tree 
      ) 
   {
      ChronoReader->SetEntry(0);
      return ChronoReader;
   }
   // We are close enough... return early
   if ( r - p < 4)
   {
      ChronoReader->SetEntry(p);
      return ChronoReader;
   }
   
   if ( p <= r) {
      int mid = (p + r) / 2;
      //std::cout <<"Set entry " << mid << " ( " << p << " - " << r << " )" << std::endl;
      ChronoReader->SetEntry(mid);
      const double t = ChronoEvent->GetRunTime();
      // We can keep things simple, and find the time within a second
      if (t < firstTime && fabs(t - firstTime ) < tolerance )
      {
         return ChronoReader;
      }
      if (t > firstTime)
         return FindStartOfTree(ChronoReader, ChronoEvent, p, mid - 1, firstTime, tolerance);
      if (t < firstTime)
         return FindStartOfTree(ChronoReader, ChronoEvent,  mid + 1, r, firstTime, tolerance);
   }
   ChronoReader->SetEntry(ChronoReader->GetEntries() - 1);
   return ChronoReader;
}
#endif


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
