
#ifndef _TChronoChannelGetters_
#define _TChronoChannelGetters_
#include "TChronoChannel.h"
#include <queue>

#ifdef BUILD_AG
TChronoChannel Get_Chrono_Channel(Int_t runNumber, const char* ChannelName, Bool_t ExactMatch = kFALSE);
std::vector<TChronoChannel> Get_Chrono_Channels(const int runNumber, const std::vector<std::string> ChannelNames, Bool_t ExactMatch = kFALSE);
TChronoChannel Get_Chrono_Channel_By_Channel_Number(const std::string& ChronoBoard, Int_t channelNumber );
std::queue<double> TSGetter(int runNumber, TChronoChannel chan);
std::queue<double> TSGetter(int runNumber, const char* ChannelName);
std::map<std::string,std::queue<double>> GetDumpsMarker(int runNumber, bool startDumps);
#endif

#endif
