
#include "TFile.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TError.h"
#include <iostream>

#ifndef _FileGetters_
#define _FileGetters_


typedef std::shared_ptr<TFile> TFilePointer; //< Pointer to a TFile object


void SetFileNamePatter(const std::string pattern = "output%05d.root");
std::string GetFileNamePattern();

TFilePointer Get_File(const Int_t run_number, const Bool_t die=kFALSE);
bool Check_File(const Int_t run_number);

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
