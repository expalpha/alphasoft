#ifndef _TAPlotVertexEvents_
#define _TAPlotVertexEvents_

#include <vector>
#include <iostream>
#include "TObject.h"
#include "TVector3.h"

#include "TAPlotVertexEventsPassedCuts.h"

// SVD / TPC vertex
class TAPlotVertexEvents : public TObject {
private:
   std::vector<int>                          fRunNumbers;     ///< Run Number of event
   std::vector<int>                          fEventNos;       ///< Event number
   std::vector<TAPlotVertexEventsPassedCuts> fCutsResults;    ///< TAPlotVertexEventsPassedCuts vector of pass cut event
   std::vector<int>                          fVertexStatuses; ///< Reconstructed vertex status
   std::vector<double>                       fXVertex;        ///< X position of reconstructed vertex
   std::vector<double>                       fYVertex;        ///< Y position of reconstructed vertex
   std::vector<double>                       fZVertex;        ///< Z position of reconstructed vertex
   std::vector<double>                       fTimes;          ///< Plot time (based off official time)
   std::vector<double>                       fEventTimes;     ///< TPC time stamp
   std::vector<double>                       fRunTimes;       ///< Official Time
   std::vector<int>                          fNumHelices;     ///< helices used for vertexing
   std::vector<int>                          fNumTracks;      ///< reconstructed (good) helices
   std::vector<double>                       fMVA;            ///< MVA rfout value
public:
   // Copy and assign operators
   TAPlotVertexEvents();
   ~TAPlotVertexEvents();
   // Basic copy constructor.
   TAPlotVertexEvents(const TAPlotVertexEvents &vertexEvents);
   // Assignment operator.
   TAPlotVertexEvents &operator=(const TAPlotVertexEvents &rhs);
   //=+ Operator.
   TAPlotVertexEvents operator+=(const TAPlotVertexEvents &rhs);

   void AddVertexEvent(const int runNumber, const int eventNo, const std::vector<bool> &cutsResult,
                       const int vertexStatus, const double x, const double y, const double z, const double t,
                       const double eventTime, const double runTime, const int numHelices, const int numTracks, const double mva);

   std::string                         CSVTitleLine() const;
   std::string                         CSVLine(const size_t i) const;
   size_t                              size() const;
   bool                                empty() const;
   void                                clear();
   int                                 CountPassedCuts(const int cut) const;
   int                                 RunNumber(const size_t index) const { return fRunNumbers[index]; }
   int                                 EventNo(const size_t index) const { return fEventNos[index]; }
   const TAPlotVertexEventsPassedCuts &CutsResults(const size_t index) const { return fCutsResults[index]; }
   int                                 VertexStatus(const size_t index) const { return fVertexStatuses[index]; }
   double                              XVertex(const size_t index) const { return fXVertex[index]; }
   double                              YVertex(const size_t index) const { return fYVertex[index]; }
   double                              ZVertex(const size_t index) const { return fZVertex[index]; }
   TVector3                            VertexVector(const size_t index) const
   {
      return TVector3(fXVertex[index], fYVertex[index], fZVertex[index]);
   }
   double Times(const size_t index) const { return fTimes[index]; }
   double EventTime(const size_t index) const { return fEventTimes[index]; }
   double RunTime(const size_t index) const { return fRunTimes[index]; }
   int    NumHelices(const size_t index) const { return fNumHelices[index]; }
   int    NumTracks(const size_t index) const { return fNumTracks[index]; }

   void ApplyMVACuts(const int MVACutIndex, const double cutValue)
   {
      for(size_t i=0; i<fCutsResults.size(); i++)
      {
         //std::cout << fMVA.at(i) <<std::endl;
         fCutsResults.at(i).fCuts.at(MVACutIndex) = fMVA.at(i) > cutValue;
      }
   }   
   ClassDef(TAPlotVertexEvents, 1)
};

#endif
