#ifndef _TAPLOTHHELIXEVENTS_
#define _TAPLOTHHELIXEVENTS_

#ifdef BUILD_AG

#include "TObject.h"
#include <vector>
#include "TAPlotVertexEventsPassedCuts.h"

class TAGPlotHelixEvents : public TObject {
public:
   std::vector<int>    fRunNumber;
   std::vector<double> fTime; // Plot time (based off official time)
   std::vector<double> fOfficialTime;
   std::vector<double> pT;
   std::vector<double> pZ;
   std::vector<double> pTot;
   std::vector<double> parD;
   std::vector<double> Curvature;
   std::vector<int>    nPoints;
   std::vector<TAPlotVertexEventsPassedCuts> fCutResults;
   TAGPlotHelixEvents();

   ~TAGPlotHelixEvents();
   // Coipy ctor
   TAGPlotHelixEvents(const TAGPlotHelixEvents &h);

   TAGPlotHelixEvents &operator=(const TAGPlotHelixEvents &h);
   TAGPlotHelixEvents &operator+=(const TAGPlotHelixEvents &h);

   const TAPlotVertexEventsPassedCuts &CutsResults(const size_t index) const { return fCutResults[index]; }

   void AddEvent(int runNumber, double time, double officialTime, double pt, double pz, double ptot, double pard,
                 double curvature, int npoints, const std::vector<bool>& cuts);

   size_t size() const { return fRunNumber.size(); }

   void clear();
   ClassDef(TAGPlotHelixEvents, 2);
};

#endif
#endif
