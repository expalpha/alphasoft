#ifndef _TAPlotFEGEMDATA_
#define _TAPlotFEGEMDATA_

#include "TAPlotEnvData.h"
#include "TStoreGEMEvent.h"
#include "TAPlotTimeWindows.h"

/// @brief feGEM data container for the TAPlot (derrived from TAPlotEnvData)
class TAPlotFEGEMData : public TAPlotEnvData {
public:
   TAPlotFEGEMData();
   TAPlotFEGEMData(const std::string &varname, const std::string label, const int index);
   virtual ~TAPlotFEGEMData();
   /// @brief Add data from TTree (Add TStoreGEMData<T>)
   /// @tparam T
   /// @param runno
   /// @param gemEvent
   /// @param timeWindows
   ///
   /// Data is grouped internally by time window (TAPlotTimeWindows)
   template <typename T>
   void AddGEMEvent(int runno, const TStoreGEMData<T> &gemEvent, const TAPlotTimeWindows &timeWindows)
   {
      double time = gemEvent.GetRunTime();
      // O^2 complexity atleast... There isn't usually allot of feGEM data so maybe we can live with this...?
      // Hopefully now better than On^2
      int index = timeWindows.GetValidWindowNumber(gemEvent.GetRunNumber(),time);
      if (index >= 0) {
         GetTimeWindow(index).AddPoint(runno, time - timeWindows.ZeroTime(index), time,
                                       (double)gemEvent.GetArrayEntry(TAPlotEnvData::GetDataIndex()));
      }
      return;
   }
   /// @brief Combine the category and variable name from feGEM into a single string (required for TAPlotEnvData parent)
   /// @param category
   /// @param varName
   /// @return
   static std::string CombinedName(const std::string &category, const std::string &varName)
   {
      return std::string(category + "\\" + varName);
   }
   ClassDef(TAPlotFEGEMData, 1);
};

#endif
