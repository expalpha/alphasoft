#if BUILD_A2
#ifndef __TA2PlotQOD_h__
#define __TA2PlotQOD_h__

#include "TA2Plot.h"
#include "TSVD_QOD.h"

class TA2PlotQOD : public TA2Plot
{
    private:
        std::vector<TSVD_QOD> fQODs; ///< List of QODs we want to plot in histograms
        std::vector<TH1I*> fOccupancy;
        std::vector<TH2I*> fOccupancyT;
        void SetUpHistograms(const size_t n_cuts);
        void FillHistograms();
    public:

    TA2PlotQOD(bool zeroTime = true);
    TA2PlotQOD(double zMin, double zMax, bool zeroTime = true);
    TA2PlotQOD(const TA2PlotQOD &object);
    virtual ~TA2PlotQOD();    
    TA2PlotQOD &operator=(const TA2PlotQOD &rhs) {
        TA2Plot::operator=(rhs);
        fQODs = rhs.fQODs;
        return *this;
    }
    TA2PlotQOD &operator+=(const TA2PlotQOD &rhs) {
        TA2Plot::operator+=(rhs);
        fQODs.insert(fQODs.end(), rhs.fQODs.begin(), rhs.fQODs.end());
        return *this;
    }
    friend TA2PlotQOD &operator+(const TA2PlotQOD &lhs, const TA2PlotQOD &rhs) {
        TA2PlotQOD *sum = new TA2PlotQOD(lhs);
        *sum += rhs;
        return *sum;
    }
    virtual int AddSVDEvent(const TSVD_QOD &qod);

    bool LikelyCosmicRun();
    void RunCosmicChecks();

    TCanvas* DrawOccupancy(const std::string &name = "cOccupancy", int CutsMode = -1);
    TCanvas* DrawSummedOccupancy(const std::string& name = "cSummedOccupancy", int CutsMode = -1);
    ClassDef(TA2PlotQOD, 1)
};

#endif
#endif