
#ifndef _PlotGetters_
#define _PlotGetters_
#include "TH1D.h"
#include "TSpline.h"
#include "THStack.h"
#include "TPaveText.h"

#include "TAPlot.h"
#include "TA2Plot.h"
#include "TAGPlot.h"
#include "TAGPlotTracks.h"
#include "TAGPlotSim.h"

#include "TChronoChannel.h"
#include "TH1DGetters.h"
#include "TStringGetters.h"

#include "TSplineGetters.h"
#include "RootUtilGlobals.h"
#include <sstream>
#if BUILD_A2
#include "TSISChannelGetters.h"
#include "TA2SpillGetters.h"
#endif
#if BUILD_AG
#include "TAGSpillGetters.h"
#endif

#if BUILD_AG


/// Plot Chronobox rates summed for all requested time windows
TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<double>& tmin, const std::vector<double>& tmax );
/// Plot Chronobox rates summed for all requested time windows
TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<double>& tmin, const std::vector<double>& tmax);

/// Plot Chronobox rates summed for all requested spills
TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<TAGSpill>& spills);
/// Plot Chronobox rates summed for all requested spills
TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<TAGSpill>& spills);

/// Plot Chronobox rates summed for all requested spill descriptions
TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<std::string>& description, const std::vector<int>& dumpIndex);
/// Plot Chronobox rates summed for all requested spill descriptions
TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<std::string>& description, const std::vector<int>& dumpIndex);


/// Plot Chronobox rates for each requested channel and time window
TCanvas* Plot_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<double>& tmin, const std::vector<double>& tmax, const std::vector<std::string> labels = {});
/// Plot Chronobox rates for each requested channel and time window
TCanvas* Plot_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<double>& tmin, const std::vector<double>& tmax, const std::vector<std::string> labels = {});

/// Plot Chronobox rates for each requested channel and spill
TCanvas* Plot_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<TAGSpill>& spills);
/// Plot Chronobox rates for each requested channel and spill
TCanvas* Plot_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<TAGSpill>& spills);

/// Plot Chronobox rates for each requested channel and spill description
TCanvas* Plot_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<std::string>& description, const std::vector<int>& dumpIndex);
/// Plot Chronobox rates for each requested channel and spill description
TCanvas* Plot_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<std::string>& description, const std::vector<int>& dumpIndex);

/// Plot Chronobox timestamp difference between events
void Plot_Delta_Chrono(Int_t runNumber, TChronoChannel channel, Double_t tmin=0., Double_t tmax=-1.);
/// Plot Chronobox timestamp difference between events
void Plot_Delta_Chrono(Int_t runNumber, TChronoChannel channel, const char* description, Int_t dumpIndex=0);
/// Plot Chronobox timestamp difference between events
void Plot_Delta_Chrono(Int_t runNumber, const char* ChannelName, Double_t tmin=0., Double_t tmax=-1.);
/// Plot Chronobox timestamp difference between events
void Plot_Delta_Chrono(Int_t runNumber, const char* ChannelName, const char* description, Int_t dumpIndex=0);

/// Plot SiPM chrono channels
void Plot_Chrono_Scintillators(Int_t runNumber, Double_t tmin=0., Double_t tmax=-1.);
/// Plot SiPM chrono channels
void Plot_Chrono_Scintillators(Int_t runNumber, const char* description, Int_t dumpIndex=0);

/// Plot 8 TPC vertex plots
TCanvas* Plot_TPC(const int runNumber,  const std::vector<double> tmin, const std::vector<double> tmax, const std::string cuts = "0");
/// Plot 8 TPC vertex plots
TCanvas* Plot_TPC(const int runNumber,  const std::vector<std::string> description, const std::vector<int> dumpIndex={0}, const std::string cuts = "0");

void Plot_FRD_Zed(std::vector<int> runNumberQWPleft, std::vector<int> runNumberQWPright, const std::string cuts = "0");
TCanvas* Plot_Zed_Multi_Run(std::vector<std::vector<int>> runNumber, std::vector<std::string> description, std::vector<int> repetition, const std::string cuts = "0", bool fit=false);

/// Plot 8 TPC vertex plots and 20 track plots
void Plot_Vertices_And_Tracks(const int runNumber, const std::vector<double> tmin, const std::vector<double> tmax, const std::string cuts = "0");
/// Plot 8 TPC vertex plots and 20 track plots
void Plot_Vertices_And_Tracks(const int runNumber,  const std::vector<std::string> description, const std::vector<int> dumpIndex={0}, const std::string cuts = "0");

/// Plot 8 TPC MC vertex plots 
void Plot_AGSim(const int runNumber);
#endif

//*************************************************************
// Energy Analysis
//*************************************************************

#if BUILD_AG

/// Plot AlphaG dump info histograms
TCanvas* Plot_AG_AnyDump(Int_t runNumber, Int_t dumpIndex, 
                          Int_t binNumber, 
                          const char* dumpFile,
                          Double_t EnergyRangeFactor,
                          const char* Chrono_Channel_Name,
                          const char* Dump_Name);

/*! \brief Plot AlphaG cold dump
 *
 * shortcut for Plot_AG_AnyDump( runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,Chrono_Channel_Name,"Cold Dump");
 */
TCanvas* Plot_AG_ColdDump(Int_t runNumber, Int_t dumpIndex, 
                          Int_t binNumber, 
                          const char* dumpFile,
                          Double_t EnergyRangeFactor,
                          const char* Chrono_Channel_Name);

/*! \brief Plot AlphaG CT cold dump
 *
 * shortcut for Plot_AG_AnyDump( runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"PMT_CATCH_OR","Cold Dump");
 */
TCanvas* Plot_AG_CT_ColdDump(Int_t runNumber, Int_t dumpIndex = 0, 
             Int_t binNumber = 1000, 
             const char* dumpFile = nullptr,
             Double_t EnergyRangeFactor = 10);

/*! \brief Plot AlphaG RCT cold dump
 *
 * shortcut for Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"SiPM_E","Cold Dump");
 */
TCanvas* Plot_AG_RCT_ColdDump(Int_t runNumber,Int_t dumpIndex = 0,
             Int_t binNumber = 1000,
             const char* dumpFile = "ana/macros/E4E5_rct_botg_dump_500ms.dump",
             Double_t EnergyRangeFactor = 10);

/*! \brief Plot AlphaG RCTtop cold dump
 *
 * shortcut for Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"SiPM_B","Cold Dump");
 */
TCanvas* Plot_AG_RCT_Top_ColdDump(Int_t runNumber,Int_t dumpIndex = 0,
             Int_t binNumber = 1000,
             const char* dumpFile = "ana/macros/E4E5_rct_botg_dump_500ms.dump",
             Double_t EnergyRangeFactor = 10);

/*! \brief Plot AlphaG ATMPreMix top cold dump
 *
 * shortcut for Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"SiPM_B","Cold Dump");
 */
TCanvas* Plot_AG_ATMPreMix_Top_ColdDump(Int_t runNumber,Int_t dumpIndex = 0,
             Int_t binNumber = 1000,
             const char* dumpFile = nullptr,
             Double_t EnergyRangeFactor = 10);

/*! \brief Plot AlphaG ATM left dump
 *
 * shortcut for Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"SiPM_E","left dump (down)");
 */
TCanvas* Plot_AG_ATM_LeftDump(Int_t runNumber,Int_t dumpIndex = 0,
             Int_t binNumber = 1000,
             const char* dumpFile = "ana/macros/LDS_fastdump_Left_pbars",
             Double_t EnergyRangeFactor = 10);

/*! \brief Plot AlphaG ATM right dump
 *
 * shortcut for Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"SiPM_B","right dump (up)");
 */
TCanvas* Plot_AG_ATM_RightDump(Int_t runNumber,Int_t dumpIndex = 0,
             Int_t binNumber = 1000,
             const char* dumpFile = "ana/macros/UDS_fastdump_Right_pbars",
             Double_t EnergyRangeFactor = 10);

#endif

#ifdef BUILD_A2
TCanvas* Plot_A2_AnyDump(Int_t runNumber, int dump_index, const char* SIS_Channel_Name, const char* Dump_Name);

TCanvas* Plot_A2_CT_HotDump(Int_t runNumber, int dumpIndex=0);

TCanvas* Plot_A2_ColdDump(Int_t runNumber,int repetition, Int_t binNumber, 
                          const char* dumpFile, Double_t EnergyRangeFactor, const char* SIS_Channel_Name);

TCanvas* Plot_A2_CT_ColdDump(Int_t runNumber, int dump_index = 0, Int_t binNumber=1000, 
                          const char* dumpFile = nullptr,
                          Double_t EnergyRangeFactor=10.);

TCanvas* Plot_A2_RCT_ColdDump(Int_t runNumber, int dump_index = 0, Int_t binNumber=1000, 
                              const char* dumpFile="alpha2/macros/ColdDump_E5E6_500ms_withOffsets_20141105.dat",
                              Double_t EnergyRangeFactor=10., const char* sis_chan = "SiPM_J");//sis_chan = "PMT_11"

TCanvas* Plot_A2_PreMix_ColdDump(Int_t runNumber,int dump_index, Int_t binNumber,
                                 const char* dumpFile = "alpha2/macros/ColdDump_A2_E11_2000ms_20230708.dat",
                                 //const char* dumpFile = "alpha2/macros/ColdDump_E11_500ms_20141105.dat",
                                 Double_t EnergyRangeFactor=10., const char* sis_chan = "SiPM_J");//sis_chan = "PMT_11"


TCanvas* MultiPlotRunsAndDumps(std::vector<Int_t> runNumbers, std::string SISChannel, 
                                std::vector<std::string> description, std::vector<std::vector<int>> dumpNumbers, 
                                std::string drawOption = "3dheat");

void Generate3DTHStack(std::vector<TH1D*> allHistos, THStack* emptyStack, TLegend* legend, std::vector<std::string> legendStrings);

#endif

/*! \brief Fit exponential to energy histogram from dump plots
 *
 * Fits exponential to determine temperature
 * @param fit histo pointer to fit, if omitted or  NULL defaults to last Plot_*_Dump result
 */
Double_t FitEnergyDump(Double_t Emin, Double_t Emax,TH1D* fit=NULL);

void SaveCanvas();
void SaveCanvas(Int_t runNumber, const char* Description);
void SaveCanvas(TString Description);
void SaveCanvas( TCanvas* iSaveCanvas, TString iDescription);

#if BUILD_A2

TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<double> tmin, std::vector<double> tmax);
TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<double> tmin, std::vector<double> tmax);
TCanvas* Plot_SIS_on_pulse(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<std::pair<double,int>> SIS_Counts,double tstart, double tstop);

TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<TA2Spill> spills);
TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<TA2Spill> spills);

TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<std::string> description, std::vector<int> dumpIndex);
TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<std::string> description, std::vector<int> dumpIndex);


TCanvas* Plot_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<double> tmin, std::vector<double> tmax);
TCanvas* Plot_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<double> tmin, std::vector<double> tmax);
TCanvas* Plot_SIS_on_pulse(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<std::pair<double,int>> SIS_Counts,double tstart, double tstop);

TCanvas* Plot_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<TA2Spill> spills);
TCanvas* Plot_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<TA2Spill> spills);

TCanvas* Plot_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<std::string> description, std::vector<int> dumpIndex);
TCanvas* Plot_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<std::string> description, std::vector<int> dumpIndex);


void Plot_SVD(Int_t runNumber, std::vector<double> tmin, std::vector<double> tmax, int cutsMode = 1);
void Plot_SVD(Int_t runNumber, std::vector<TA2Spill> spills, int cutsMode = 1);
void Plot_SVD(Int_t runNumber, std::vector<std::string> description, std::vector<int> dumpIndex, int cutsMode = 1);

void Plot_feGEM(Int_t runNumber, std::vector<std::string> description, std::vector<int> dumpIndex, std::string category, std::string variable, int feGEMindex);
#endif

#endif



/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
