#ifdef BUILD_A2
#ifndef _TA2Plot_Filler_
#define _TA2Plot_Filler_

#include "TA2Plot.h"
#include <set>
class TA2Plot_Filler {
private:
   std::vector<TA2Plot *>                plots; ///< Pointer to TA2Plots booked in for filling
   std::vector<int>                      runNumbers; ///< Vector of unque run numbers from TA2Plots booked for filling
   std::vector<TAPlotFEGEMData> GEMChannels; ///< List of feGEM channels we want to add to all TA2Plots
   std::vector<TAPlotFELabVIEWData>  LVChannels; ///< List of feLabVIEW channels we want to add to all TA2Plots
   /// @brief Add runNumber to list of unique runs
   /// @param runNumber 
   ///
   /// Skip adding the run if its already in the list
   void AddRunNumber(int runNumber)
   {
      for (auto &no : runNumbers) {
         if (no == runNumber) {
            return;
         }
      }
      runNumbers.push_back(runNumber);
   }

public:
   TA2Plot_Filler();
   ~TA2Plot_Filler();
   void BookPlot(TA2Plot *plot);
   void TrackGEMChannel(const TAPlotFEGEMData &channel);
   void TrackLVChannel(const TAPlotFELabVIEWData &channel);
private:
   void LoadfeGEMData(int runNumber, double first_time, double last_time, int verbose);
   void LoadfeLVData(int runNumber, double first_time, double last_time, int verbose);
   void LoadData(int runNumber, double first_time, double last_time, int verbose);
public:
   void LoadData(int verbose = 1);
};
#endif
#endif
