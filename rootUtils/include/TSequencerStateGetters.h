
#ifndef SEQ_STATE_GETTERS
#define SEQ_STATE_GETTERS

#include "TSequencerState.h"
#include "TreeGetters.h"

TSequencerState* Get_Seq_State(Int_t runNumber, int SequencerID, int state);


#endif
