#if BUILD_A2
#if HAVE_MIDAS
#include "midas.h"
#endif
#include "TA2PlotWithGEM.h"

TA2PlotWithGEM::TA2PlotWithGEM(bool zeroTime) : TA2Plot(zeroTime) {}

TA2PlotWithGEM::TA2PlotWithGEM(double zMin, double zMax, bool zeroTime) : TA2Plot(zMin, zMax, zeroTime) {}

TA2PlotWithGEM::TA2PlotWithGEM(const TA2PlotWithGEM &object) : TA2Plot(object) {}

TA2PlotWithGEM::~TA2PlotWithGEM() {}

void TA2PlotWithGEM::AddGEMVariable(std::string variable_name, int index_id, std::string label_name, int precision) {
   varname.push_back(variable_name);
   label.push_back(label_name);
   index.push_back(index_id);
   precisions.push_back(precision);
}

void TA2PlotWithGEM::LoadDataWithGEM(int verbose) {
   TA2Plot_Filler plot_filler;
   for (size_t i=0; i<varname.size();i++) {
      plot_filler.TrackGEMChannel(TAPlotFEGEMData(varname[i].c_str(),label[i].c_str(),index[i]));
   }
   TA2Plot* this_plot = (TA2Plot*)this;
   plot_filler.BookPlot(this_plot);
   plot_filler.LoadData(verbose);
   AssignGEMData();
}

TGraph* TA2PlotWithGEM::GetGEMGraph(std::string name) {
   for (size_t i=0; i<label.size();i++) {
      if (label[i]==name or varname[i]==name) return TAPlotFEGEMData(fFEGEM[i]).BuildGraph(0,kZeroTimeAxis);
   }
   return NULL;
}

void TA2PlotWithGEM::AssignGEMData() {
   std::vector<std::vector<int>> var_data_current_id_all_windows;
   std::vector<std::vector<double>> run_time_next_all_windows, value_current_all_windows;
   for (size_t i_timewindow=0; i_timewindow<GetTimeWindows().size(); i_timewindow++) {
      std::vector<int> var_data_current_id(varname.size(),0);
      std::vector<double> run_time_next, value_current;
      for (size_t i_var=0; i_var<varname.size(); i_var++) {
         const TAPlotEnvDataPlot& env = GetGEMChannels().at(i_var).GetTimeWindow(i_timewindow);
         if (env.size()==0) continue;
         if (env.RunNumber(0)!=GetTimeWindows().RunNumber(i_timewindow)) continue;
         if (env.size()==1) {
            run_time_next.push_back(1e12);
            value_current.push_back(env.Data(0));
         }
         else {
            run_time_next.push_back(env.RunTime(1));
            value_current.push_back(env.Data(0));
         }
      }
      var_data_current_id_all_windows.push_back(var_data_current_id);
      run_time_next_all_windows.push_back(run_time_next);
      value_current_all_windows.push_back(value_current);

   }
   for (size_t i_var=0; i_var<varname.size(); i_var++) {
      std::deque<double> deq;
      vtx_gem_data.push_back(deq);
   }
   for (size_t i=0; i<fVertexEvents.size(); i++) {
      bool event_done = false;
      for (size_t i_timewindow=0; i_timewindow<GetTimeWindows().size(); i_timewindow++) {
         if (fVertexEvents.RunNumber(i)!=GetTimeWindows().RunNumber(i_timewindow)) continue;
         if (fVertexEvents.RunTime(i)<GetTimeWindows().MinTime(i_timewindow) or fVertexEvents.RunTime(i)>GetTimeWindows().MaxTime(i_timewindow)) continue;
         for (size_t i_var=0; i_var<varname.size(); i_var++) {
            const TAPlotEnvDataPlot& env = GetGEMChannels().at(i_var).GetTimeWindow(i_timewindow);
            // If a new data point has been taken, use it, and read the timestamp of the next data point so we know when to update next.
            if (fVertexEvents.RunTime(i)>run_time_next_all_windows.at(i_timewindow).at(i_var)) {
               var_data_current_id_all_windows.at(i_timewindow).at(i_var)++;
               if (var_data_current_id_all_windows.at(i_timewindow).at(i_var)+1==env.size()) {
                  run_time_next_all_windows.at(i_timewindow).at(i_var) = 1e12;
                  value_current_all_windows.at(i_timewindow).at(i_var) = env.Data(var_data_current_id_all_windows.at(i_timewindow).at(i_var));
               }
               else {
                  run_time_next_all_windows.at(i_timewindow).at(i_var) = env.RunTime(var_data_current_id_all_windows.at(i_timewindow).at(i_var)+1);
                  value_current_all_windows.at(i_timewindow).at(i_var) = env.Data(var_data_current_id_all_windows.at(i_timewindow).at(i_var));
               }
            }
            // Save the vertex gem data with the most recent data point
            vtx_gem_data.at(i_var).push_back(value_current_all_windows.at(i_timewindow).at(i_var));

         }
         // event done, don't look in other time windows
         event_done = true;
         break;
      }
      if (!event_done) {
         for (size_t i_var=0; i_var<varname.size(); i_var++) {
            vtx_gem_data.at(i_var).push_back(-1.);
         } 
      }
   }
}

void TA2PlotWithGEM::ExportCSV(const std::string filename, const std::string options)
{
   if (IsDataStale()) LoadDataWithGEM();
   // Save vertex plots if 'v' is listed in options
   if (options.find('v') != std::string::npos)
   {
      std::string   vertexFilename     = filename + ".vertex.csv";
      std::ofstream verts;
      verts.open(vertexFilename);
      verts << fVertexEvents.CSVTitleLine().substr(0,fVertexEvents.CSVTitleLine().size()-2); // Without the newline character
      verts << ",";
      for (size_t i_var=0; i_var<varname.size(); i_var++) {
         verts << label.at(i_var);
         if (i_var+1==varname.size()) verts << "\n";
         else verts << ",";
      }
      int normal_precision = verts.precision();
      for (size_t i = 0; i < fVertexEvents.size(); i++) {
         std::string line = fVertexEvents.CSVLine(i);
         verts << line.substr(0,line.size()-2); // Without the newline character
         for (size_t i_var=0; i_var<varname.size(); i_var++) {
            verts << std::setprecision( precisions.at(i_var));
            verts << vtx_gem_data.at(i_var).at(i);
            if (i_var+1==varname.size()) verts << "\n";
            else verts << ",";
            verts << std::setprecision( normal_precision);
         }
      }
      verts.close();
      std::cout << vertexFilename << " saved\n";
   }

   // Save timewindows if 't' is listed in options
   if (options.find('t') != std::string::npos)
   {
      std::string   timeWindowFilename = filename + ".timewindows.csv";
      std::ofstream times;
      times.open(timeWindowFilename);
      times << GetTimeWindows().CSVTitleLine();
      for (size_t i = 0; i < GetTimeWindows().size(); i++) times << GetTimeWindows().CSVLine(i);
      times.close();
      std::cout << timeWindowFilename << " saved\n";
   }

   // Save A2 AnalysisReport data if 'a' is set in options
   // Commented out since I can't access the analysis report easily from here, and does anyone use this anyway?
   /*
   if (options.find('a') != std::string::npos)
   {
      std::string   analysisReportFilename = filename + ".AnalysisReport.csv";
      std::ofstream aReport;
      aReport.open(analysisReportFilename);
      aReport << fAnalysisReports.front().CSVTitleLine();
      for (const TA2AnalysisReport &r : fAnalysisReports) aReport << r.CSVLine();
      aReport.close();
      std::cout << analysisReportFilename << " saved\n";
   }
   */
   // Save Scaler Data if 's' is set in options
   if (options.find('s') != std::string::npos)
   {
      std::string   scalerFilename = filename + ".scaler.csv";
      std::ofstream sis;
      sis.open(scalerFilename);
      sis << SISEvents.CSVTitleLine();
      for (size_t i = 0; i < SISEvents.size(); i++) sis << SISEvents.CSVLine(i);
      sis.close();
      std::cout << scalerFilename << " saved\n";
   }

}


#endif
