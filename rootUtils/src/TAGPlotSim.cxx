#if BUILD_AG
#include "TAGPlotSim.h"

/// @brief Constructor
/// @param zeroTime
TAGPlotSim::TAGPlotSim(bool zeroTime) : TAGPlot(zeroTime) {}

/// @brief Constructor
/// @param zMin
/// @param zMax
/// @param zeroTime
TAGPlotSim::TAGPlotSim(double zMin, double zMax, bool zeroTime) : TAGPlot(zMin, zMax, zeroTime) {}

/// @brief Copy constructor
/// @param object 
TAGPlotSim::TAGPlotSim(const TAGPlotSim &object)
   : TAGPlot(object), fSimVertexX(object.fSimVertexX), fSimVertexY(object.fSimVertexY), fSimVertexZ(object.fSimVertexZ),
   fRecoVertexX(object.fRecoVertexX), fRecoVertexY(object.fRecoVertexY), fRecoVertexZ(object.fRecoVertexZ)
{
}

/// @brief Deconstructor
TAGPlotSim::~TAGPlotSim()
{
   Reset();
}

void TAGPlotSim::Reset()
{
   TAGPlot::Reset();
   fSimVertexX.clear();
   fSimVertexY.clear();
   fSimVertexZ.clear();
   fRecoVertexX.clear();
   fRecoVertexY.clear();
   fRecoVertexZ.clear();
}

/// @brief = operator
/// @param rhs 
/// @return 
TAGPlotSim &TAGPlotSim::operator=(const TAGPlotSim &rhs)
{
   TAGPlot::operator=(rhs);
   fSimVertexX = rhs.fSimVertexX;
   fSimVertexY = rhs.fSimVertexY;
   fSimVertexZ = rhs.fSimVertexZ;
   fRecoVertexX = rhs.fRecoVertexX;
   fRecoVertexY = rhs.fRecoVertexY;
   fRecoVertexZ = rhs.fRecoVertexZ;
   return *this;
}

/// @brief += operator
/// @param rhs 
/// @return 
TAGPlotSim &TAGPlotSim::operator+=(const TAGPlotSim &rhs)
{
   // This calls the parent += operator first.
   TAGPlot::operator+=(rhs);
   for (double x: rhs.fSimVertexX) fSimVertexX.push_back(x);
   for (double y: rhs.fSimVertexY) fSimVertexY.push_back(y);
   for (double z: rhs.fSimVertexZ) fSimVertexZ.push_back(z);
   for (double x: rhs.fRecoVertexX) fRecoVertexX.push_back(x);
   for (double y: rhs.fRecoVertexY) fRecoVertexY.push_back(y);
   for (double z: rhs.fRecoVertexZ) fRecoVertexZ.push_back(z);
   return *this;
}

/// @brief Addition operator
/// @param lhs
/// @param rhs
/// @return
///
/// Add the contents of the lhs and rhs and return a new TAGPlot object
TAGPlotSim &operator+(const TAGPlotSim &lhs, const TAGPlotSim &rhs)
{
   TAGPlotSim *basePlot = new TAGPlotSim;
   *basePlot += lhs;
   *basePlot += rhs;
   return *basePlot;
}

/// @brief Add TStoreEvent to container
/// @param event 
/// @param timeOffset 
void TAGPlotSim::AddEvent(const TStoreEvent &event)
{
   if (event.GetVertexStatus()<0) return;
   if (event.GetVertexZ()<-5000 or event.GetVertexZ()>5000) return;
   fSimVertexX.push_back(event.GetSimEvent()->GetVertexX());
   fSimVertexY.push_back(event.GetSimEvent()->GetVertexY());
   fSimVertexZ.push_back(event.GetSimEvent()->GetVertexZ());
   fRecoVertexX.push_back(event.GetVertexX());
   fRecoVertexY.push_back(event.GetVertexY());
   fRecoVertexZ.push_back(event.GetVertexZ());
   return;
}

/// @brie Load TStoreEvent and Chronobox data into container for a single run
/// @param runNumber 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
void TAGPlotSim::LoadRun(const int runNumber)
{
   TTreeReaderPointer TPCTreeReader = Get_StoreEvent_TreeReader(runNumber);
   if (!TPCTreeReader) {
      printf("no tree reader!\n");
      return;
   }
   TTreeReaderValue<TStoreEvent> TPCEvent(*TPCTreeReader, "StoredEvent");
   while (TPCTreeReader->Next()) {
      AddEvent(*TPCEvent);
   }
}

/// @brief Setup sim histograms
void TAGPlotSim::SetupSimHistos()
{
   TH1D *hdR = new TH1D("hdR", "R_reconstructed - R_simulated;R [mm]", 100, -100., 100.);
   AddHistogram(hdR->GetName(), hdR);
   TH1D *hdPhi = new TH1D("hdPhi", "Phi_reconstructed - Phi_simulated; Phi [rad]", 100, -TMath::Pi(), TMath::Pi());
   AddHistogram(hdPhi->GetName(), hdPhi);
   TH1D *hdZ = new TH1D("hdZ", "Z_reconstructed - Z_simulated;Z [mm]", 100, -100., 100.);
   AddHistogram(hdZ->GetName(), hdZ);

   TH2D *hRvR = new TH2D("hRvR", "R_reconstructed vs R_simulated; R_sim [mm]; R_reco [mm]", 100, 0., 100., 100, 0., 100.);
   AddHistogram(hRvR->GetName(), hRvR);
   TH2D *hPhivPhi = new TH2D("hPhivPhi", "Phi_reconstructed vs Phi_simulated; Phi_sim [mm]; Phi_reco [mm]", 100, 0, 2*TMath::Pi(), 100, 0, 2*TMath::Pi());
   AddHistogram(hPhivPhi->GetName(), hPhivPhi);
   TH2D *hZvZ = new TH2D("hZvZ", "Z_reconstructed vs Z_simulated; Z_sim [mm]; Z_reco [mm]", 100, -1300., 1300., 100, -1300., 1300.);
   AddHistogram(hZvZ->GetName(), hZvZ);

   TH1D *hZsim = new TH1D("hZsim", "Z_simulated;Z [mm]", 200, -1300., 1300.);
   AddHistogram(hZsim->GetName(), hZsim);
   TH1D *hZreco = new TH1D("hZreco", "Z_reconstructed;Z [mm]", 200, -1300., 1300.);
   AddHistogram(hZreco->GetName(), hZreco);
   TH2D *hXYsim = new TH2D("hXYsim", "XY_simulated; X_sim [mm]; Y_sim [mm]", 100, -100., 100., 100, -100., 100.);
   AddHistogram(hXYsim->GetName(), hXYsim);
   TH2D *hXYreco = new TH2D("hXYreco", "XY_reconstructed; X_reco [mm]; Y_reco [mm]", 100, -100., 100., 100, -100., 100.);
   AddHistogram(hXYreco->GetName(), hXYreco);

}

/// @brief Fill track metric histograms
void TAGPlotSim::FillSimHisto()
{
   for (size_t i = 0; i < fSimVertexX.size(); i++) {
      TVector2 simXY(fSimVertexX[i],fSimVertexY[i]);
      TVector2 recoXY(fRecoVertexX[i],fRecoVertexY[i]);
      FillHistogram("hdR", recoXY.Mod() - simXY.Mod());
      FillHistogram("hdPhi", recoXY.DeltaPhi(simXY));
      FillHistogram("hdZ", fRecoVertexZ[i] - fSimVertexZ[i]);
      FillHistogram("hRvR", simXY.Mod(),recoXY.Mod());
      FillHistogram("hPhivPhi", simXY.Phi(),recoXY.Phi());
      FillHistogram("hZvZ", fSimVertexZ[i],fRecoVertexZ[i]);
      FillHistogram("hZreco",fRecoVertexZ[i]);
      FillHistogram("hZsim",fSimVertexZ[i]);
      FillHistogram("hXYsim",fSimVertexX[i],fSimVertexY[i]);
      FillHistogram("hXYreco",fRecoVertexX[i],fRecoVertexY[i]);
   }

}

/// @brief Draw track data
/// @param Name 
/// @return 
TCanvas *TAGPlotSim::DrawSimCanvas(TString Name)
{
   SetupSimHistos();
   FillSimHisto();

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1400);
   ct->Divide(5, 2);

   ct->cd(1);
   ((TH2D *)fHistos.At(fHistoPositions.at("hXYsim")))->Draw("colz");
   ct->cd(6);
   ((TH2D *)fHistos.At(fHistoPositions.at("hXYreco")))->Draw("colz");
   ct->cd(2);
   ((TH1D *)fHistos.At(fHistoPositions.at("hZsim")))->Draw("hist");
   ct->cd(7);
   ((TH1D *)fHistos.At(fHistoPositions.at("hZreco")))->Draw("hist");
   ct->cd(3);
   TH1D* hhdR = (TH1D *)fHistos.At(fHistoPositions.at("hdR"));
   hhdR->Draw();
   ct->cd(8);
   ((TH2D *)fHistos.At(fHistoPositions.at("hRvR")))->Draw("colz");
   ct->cd(4);
   gStyle->SetOptFit(1);
   TH1D* hhdPhi = (TH1D *)fHistos.At(fHistoPositions.at("hdPhi"));
   hhdPhi->Fit("gaus");
   hhdPhi->Draw();
   ct->cd(9);
   ((TH2D *)fHistos.At(fHistoPositions.at("hPhivPhi")))->Draw("colz");
   ct->cd(5);
   TH1D* hhdZ = (TH1D *)fHistos.At(fHistoPositions.at("hdZ"));
   hhdZ->Fit("gaus");
   hhdZ->Draw();
   ct->cd(10);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZvZ")))->Draw("colz");

   return ct;
}

#endif
