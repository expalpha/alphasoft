#include "TChronoChannelGetters.h"
#include "IntGetters.h"

#ifdef BUILD_AG

TChronoChannel Get_Chrono_Channel(Int_t runNumber, const char* ChannelName, Bool_t ExactMatch)
{
   for (const std::pair<std::string,int> board : TChronoChannel::CBMAP)
   {
      int chan = Get_Chrono_Channel_In_Board(runNumber, board.first, ChannelName, ExactMatch);
      if (chan >= 0)
      {
         TChronoChannel c(board.first,chan);
         return c;
      }
   }
   return {"", -1};
}

std::vector<TChronoChannel> Get_Chrono_Channels(const int runNumber, const std::vector<std::string> ChannelNames, Bool_t ExactMatch)
{
   std::vector<TChronoChannel> channels;
   for (const std::string& name: ChannelNames)
   {
      channels.emplace_back(Get_Chrono_Channel(runNumber,name.c_str(), ExactMatch));
   }
   return channels;
}

TChronoChannel Get_Chrono_Channel_By_Channel_Number(const std::string& ChronoBoard, Int_t channelNumber ) {
    if (channelNumber<0 or channelNumber>=CHRONO_N_CHANNELS) return TChronoChannel("",-1);
    return TChronoChannel(ChronoBoard,channelNumber);
}

std::queue<double> TSGetter(int runNumber, TChronoChannel chan)
{
  TTreePointer t=Get_Chrono_Tree(runNumber,chan);
  TCbFIFOEvent* e=new TCbFIFOEvent();
  // TString name=Get_Chrono_Name(runNumber,chan);
  // std::cout<<name<<std::endl;
  t->SetBranchAddress("FIFOData", &e);
  std::queue<double> v;
  for (Int_t i = 0; i < t->GetEntries(); ++i)
    {
      t->GetEntry(i);
      if( (int)e->GetChannel() != chan.GetChannel() ) continue;
      if( e->IsLeadingEdge() && e->fCounts > 0 ) v.push(e->GetRunTime());
    }
  return v;
}

std::queue<double> TSGetter(int runNumber, const char* ChannelName)
{
  TChronoChannel chan = Get_Chrono_Channel(runNumber,ChannelName);
  return TSGetter( runNumber, chan );
}


std::map<std::string,std::queue<double>> GetDumpsMarker(int runNumber, bool startDumps)
{
  std::map<std::string,std::queue<double>> dumps;
  if( startDumps )
    {
      dumps["rct_botg"]=TSGetter(runNumber,"RCT_BOTG_START_DUMP");
      dumps["atm_botg"]=TSGetter(runNumber,"ATM_BOTG_START_DUMP");
      dumps["cat"]=TSGetter(runNumber,"CAT_START_DUMP");
      dumps["pos"]=TSGetter(runNumber,"POS_START_DUMP");
    }
  else
    {
      dumps["rct_botg"]=TSGetter(runNumber,"RCT_BOTG_STOP_DUMP");
      dumps["atm_botg"]=TSGetter(runNumber,"ATM_BOTG_STOP_DUMP");
      dumps["cat"]=TSGetter(runNumber,"CAT_STOP_DUMP");
      dumps["pos"]=TSGetter(runNumber,"POS_STOP_DUMP");
    }
  return dumps;
}

#endif 
