#include "BinaryRunners.h"

#include "TSystemDirectory.h"
#include "TList.h"
#include "TSystem.h"
#include "RootUtilGlobals.h"
#include "TStringGetters.h"

#include <algorithm>
#include <deque>
#include <thread>

int RunAnalyzer(const int runNumber)
{
   if (rootUtils::GetAnalyzerList().empty()) {
      std::cerr << "No default analyzers set... set with rootUtils::SetDefaultAnalyzer()" << std::endl;
      return 1;
   }

   for (const std::string &p : rootUtils::GetListOfPossibleMIDASFilePaths()) std::cout << p << "\n";

   std::vector<std::string> files = GetListOfRunFiles(runNumber);
   for (std::tuple<std::string,std::string,std::string> &program : rootUtils::GetAnalyzerList()) {
      // Set up program to run
      std::string cmd = std::get<0>(program) + " ";
      // Set agruments for program, eg --mt
      cmd += std::get<1>(program) + " ";

      if (files.empty()) {
         std::cerr << "No MIDAS files found for run " << runNumber << ". Aborting analysis with " << std::get<0>(program)
                   << std::endl;
         continue;
      }
      for (const std::string &f : files) cmd += f + " ";
      // Add module flags
      cmd += std::string(" -- ") + std::get<2>(program);
      std::cout << "Executing command: " << cmd <<std::endl;
      int status = gSystem->Exec(cmd.c_str());
      if (status) {
         std::cerr << "Non zero return code from program... halting more processing" << std::endl;
         return status;
      }
   }
   return 0;
}

/// @brief Asyncronously open run files (useful for multithreading analyzers if they are triggered on open)
/// @param runs 
void PrepareRuns(std::vector<int> runs, int max_threads )
{
   if( max_threads < 1)
   {
      std::cerr << "Minimum number of threads is 1" << std::endl;
      return;
   }
   std::vector<int>::iterator ip = std::unique(runs.begin(), runs.end());
   // Now runs becomes undefined beyond ip
   // Resizing the vector so as to remove the undefined terms
   runs.resize(std::distance(runs.begin(), ip));
   int threads_running = 0;
   std::deque<std::thread*> threads;
   for (const int runNumber: runs)
   {
      if (threads_running >= max_threads)
      {
         std::cout << "Thread limit reached... waiting\n";
         threads.front()->join();
         delete threads.front();
         threads.pop_front();
         threads_running--;
      }
      threads.emplace_back(new std::thread(Get_File, runNumber,false));
      sleep(1);
      threads_running++;
   }
   std::cout <<"All threads running..." <<std::endl;
   for (std::thread* t: threads)
   {
      t->join();
      delete t;
   }
   std::cout <<"Add threads finished " << std::endl;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
