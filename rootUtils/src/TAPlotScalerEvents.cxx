
#include "TAPlotScalerEvents.h"

/// @brief Constructor
TAPlotScalerEvents::TAPlotScalerEvents()
{
}

/// @brief Deconstructor
TAPlotScalerEvents::~TAPlotScalerEvents()
{

}

/// @brief Copy constructor
/// @param sisPlotEvents 
TAPlotScalerEvents::TAPlotScalerEvents(const TAPlotScalerEvents &sisPlotEvents) : TObject(sisPlotEvents)
{
   // Deep copy vectors.
   for (size_t i = 0; i < sisPlotEvents.fTime.size(); i++) {
      fRunNumber.push_back(sisPlotEvents.fRunNumber[i]);
      fTime.push_back(sisPlotEvents.fTime[i]);
      fOfficialTime.push_back(sisPlotEvents.fOfficialTime[i]);
      fCounts.push_back(sisPlotEvents.fCounts[i]);
   }
}

/// @brief = operator
/// @param sisPlotEvents 
/// @return 
TAPlotScalerEvents &TAPlotScalerEvents::operator=(const TAPlotScalerEvents &sisPlotEvents)
{
   for (size_t i = 0; i < sisPlotEvents.fTime.size(); i++) {
      this->fRunNumber.push_back(sisPlotEvents.fRunNumber[i]);
      this->fTime.push_back(sisPlotEvents.fTime[i]);
      this->fOfficialTime.push_back(sisPlotEvents.fOfficialTime[i]);
      this->fCounts.push_back(sisPlotEvents.fCounts[i]);
   }
   return *this;
}

/// @brief Add a scaler event
/// @param runNumber Run Number
/// @param time      User provided time
/// @param officialTime Run time (seconds since the start of run)
/// @param counts    Counts in scaler event
void TAPlotScalerEvents::AddEvent(int runNumber, double time, double officialTime, int counts)
{
   fRunNumber.push_back(runNumber);
   fTime.push_back(time);
   fOfficialTime.push_back(officialTime);
   fCounts.push_back(counts);
}