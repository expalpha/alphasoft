#ifdef BUILD_AG

#include "TAGPlot.h"
#include "ChronoReader.h"
#include <limits>
#define SCALECUT 0.6

/*! \class TG2Plot
    \brief Class for plotting vertex data in ALPHAg


Child class of TAPlot, works as a Data Frame for vertex data

Example usage, plot cosmic data in run
~~~~{.cpp}
TAGPlot a;
// Add a time window to the container
a.AddDumpGates(8037,{"FastRampDown"},{0});
// Fill the container within time window
// Not the end of the world if you miss LoadData, DrawVertexCanvas will auto call it if you forgot
a.LoadData();
// Draw the plot (optional)
a.DrawVertexCanvas("8037",0);
~~~~
You can get more advanced adding multiple runs and times directly without depending on dumps
~~~~{.cpp}

TAGPlot a;

// If you would like to plot a time range instead of some dump markers:
a.AddTimeGate(8037,4780.1,4810.7);

// If you would like to add multiple runs:
a.AddDumpGates(8035,{"FastRampDown"},{0})
a.AddDumpGates(8036,{"FastRampDown"},{0})
a.AddDumpGates(8037,{"FastRampDown"},{0})

// To plot with cuts:
a.DrawVertexCanvas("8037",2)

The second argument specifies the cut type:
0 - no cuts
1 - vertex radius cut
2 - BV multiplicity cut
3 - TOF cuts, work in progress
~~~~
*/

/// @brief Constructor
/// @param zeroTime If true, the time axis is 'zeroed' allowing plotting time around a defined zero time
///
/// Each time window has its own zero time, allowing for complex plotting like events around a LyALPHA pulse
TAGPlot::TAGPlot(bool zeroTime) : TAPlot(zeroTime) {}

/// @brief Constructor with z cut
/// @param zMin
/// @param zMax
/// @param zeroTime
TAGPlot::TAGPlot(double zMin, double zMax, bool zeroTime) : TAPlot(zeroTime)
{
   fZMinCut = zMin;
   fZMaxCut = zMax;
}

/// Copy Constructor
TAGPlot::TAGPlot(const TAGPlot &object)
   : TAPlot(object), top(object.top), bottom(object.bottom), sipmad(object.sipmad), sipmcf(object.sipmcf),
     TPC_TRIG(object.TPC_TRIG), fCATStart(object.fCATStart), fCATStop(object.fCATStop), fRCTStart(object.fRCTStart),
     fRCTStop(object.fRCTStop), fATMStart(object.fATMStart), fATMStop(object.fATMStop),
     fBeamInjection(object.fBeamInjection), fBeamEjection(object.fBeamEjection)
{
   fZMinCut = object.fZMinCut;
   fZMaxCut = object.fZMaxCut;
}

/// @brief Deconstructor
TAGPlot::~TAGPlot()
{
}

/// @brief = assignment operator
/// @param rhs
/// @return
TAGPlot &TAGPlot::operator=(const TAGPlot &rhs)
{
   TAPlot::operator=(rhs);
   top            = rhs.top;
   bottom         = rhs.bottom;
   sipmad         = rhs.sipmad;
   sipmcf         = rhs.sipmcf;
   TPC_TRIG       = rhs.TPC_TRIG;
   fCATStart      = rhs.fCATStart;
   fCATStop       = rhs.fCATStop;
   fRCTStart      = rhs.fRCTStart;
   fRCTStop       = rhs.fRCTStop;
   fATMStart      = rhs.fATMStart;
   fATMStop       = rhs.fATMStop;
   fBeamInjection = rhs.fBeamInjection;
   fBeamEjection  = rhs.fBeamEjection;
   return *this;
}

/// @brief += operator
/// @param rhs
/// @return
TAGPlot &TAGPlot::operator+=(const TAGPlot &rhs)
{
   // This calls the parent += operator first.
   TAPlot::operator+=(rhs);
   top.insert(rhs.top.begin(), rhs.top.end());
   bottom.insert(rhs.bottom.begin(), rhs.bottom.end());
   sipmad.insert(rhs.sipmad.begin(), rhs.sipmad.end());
   sipmcf.insert(rhs.sipmcf.begin(), rhs.sipmcf.end());
   TPC_TRIG.insert(rhs.TPC_TRIG.begin(), rhs.TPC_TRIG.end());
   fCATStart.insert(rhs.fCATStart.begin(), rhs.fCATStart.end());
   fCATStop.insert(rhs.fCATStop.begin(), rhs.fCATStop.end());
   fRCTStart.insert(rhs.fRCTStart.begin(), rhs.fRCTStart.end());
   fRCTStop.insert(rhs.fRCTStop.begin(), rhs.fRCTStop.end());
   fATMStart.insert(rhs.fATMStart.begin(), rhs.fATMStart.end());
   fATMStop.insert(rhs.fATMStop.begin(), rhs.fATMStop.end());
   fBeamInjection.insert(rhs.fBeamInjection.begin(), rhs.fBeamInjection.end());
   fBeamEjection.insert(rhs.fBeamEjection.begin(), rhs.fBeamEjection.end());
   return *this;
}

/// @brief Addition operator
/// @param lhs
/// @param rhs
/// @return
///
/// Add the contents of the lhs and rhs and return a new TAGPlot object
TAGPlot &operator+(const TAGPlot &lhs, const TAGPlot &rhs)
{
   TAGPlot *basePlot = new TAGPlot;
   *basePlot += lhs;
   *basePlot += rhs;
   return *basePlot;
}

/// @brief Set default ChronoChannels
/// @param runNumber
void TAGPlot::SetChronoChannels(Int_t runNumber)
{
   top.insert(std::pair<int, TChronoChannel>(runNumber, Get_Chrono_Channel(runNumber, "SIPM_UDS")));
   bottom.insert(std::pair<int, TChronoChannel>(runNumber, Get_Chrono_Channel(runNumber, "SIPM_LDS")));
   sipmad.insert(std::pair<int, TChronoChannel>(runNumber, Get_Chrono_Channel(runNumber, "SIPM_BOTG_TOP_OR")));
   sipmcf.insert(std::pair<int, TChronoChannel>(runNumber, Get_Chrono_Channel(runNumber, "SIPM_BOTG_BOTTOM_OR")));
   // TPC_TRIG.insert(      std::pair<int,TChronoChannel>(runNumber, Get_Chrono_Channel( runNumber, "ADC_TRG")));
   TPC_TRIG.insert(std::pair<int, TChronoChannel>(runNumber, Get_Chrono_Channel(runNumber, "trig_out")));
   // TPC_TRIG.insert(      std::pair<int,TChronoChannel>(runNumber, Get_Chrono_Channel( runNumber, "cb02_lemo_4")));
   fBeamInjection.insert(std::pair<int, TChronoChannel>(runNumber, Get_Chrono_Channel(runNumber, "AD_TRIG")));

   // Add all valid SIS channels to a list for later:
   if (top.find(runNumber)->second.IsValidChannel()) fChronoChannels.push_back(top.find(runNumber)->second);
   if (bottom.find(runNumber)->second.IsValidChannel()) fChronoChannels.push_back(bottom.find(runNumber)->second);
   if (sipmad.find(runNumber)->second.IsValidChannel()) fChronoChannels.push_back(sipmad.find(runNumber)->second);
   if (sipmcf.find(runNumber)->second.IsValidChannel()) fChronoChannels.push_back(sipmcf.find(runNumber)->second);
   if (TPC_TRIG.find(runNumber)->second.IsValidChannel()) fChronoChannels.push_back(TPC_TRIG.find(runNumber)->second);
   if (fBeamInjection.find(runNumber)->second.IsValidChannel())
      fChronoChannels.push_back(fBeamInjection.find(runNumber)->second);

   // std::cout <<"Top:";top[runNumber].Print();
   // std::cout <<"Bottom:";bottom[runNumber].Print();
   // std::cout <<"TPC_TRIG:"; TPC_TRIG[runNumber].Print();
   return;
}

/// @brief Add a single TAGDetectorEvent to container
/// @param event
///
/// Do not add if it fails a z cut
double TAGPlot::AddAGDetectorEvent(const TAGDetectorEvent &event)
{
   double       time = event.fRunTime;
   const double z    = event.fVertex.Z();
   if (z < fZMinCut) return 0.;
   if (z >= fZMaxCut) return 0.;

   int index = GetTimeWindows().GetValidWindowNumber(event.fRunNumber,time);

   if (index >= 0) {
      AddEvent(event, GetTimeWindows().ZeroTime(index));
      return 0.;
   }
   return GetTimeWindows().GetTimeToNextWindow(event.fRunNumber,time);
}

/// @brief Add a single chronobox event to container
/// @param event
double TAGPlot::AddChronoEvent(const TCbFIFOEvent &event)
{
   const double time = event.GetRunTime();

   // Loop over all time windows
   const int index = GetTimeWindows().GetValidWindowNumber(event.fRunNumber,time);
   if (index >= 0) {
      for (const TChronoChannel &c : fChronoChannels) {
         if (event == c) {
            AddEvent(event, c, GetTimeWindows().ZeroTime(index));
            return 0;
         }
      }
   }
   return GetTimeWindows().GetTimeToNextWindow(event.fRunNumber,time);
}

/// @brief Add TAGDetectorEvent to container
/// @param event
/// @param timeOffset used for 'zerotime' mode
void TAGPlot::AddEvent(const TAGDetectorEvent &event, const double timeOffset)
{
   const double tMinusOffset = (event.fRunTime - timeOffset);
   // if ( event.fBarMaxTOF > 0)
   // std::cout << event.fBarTOFstdv * 1E9 << "\t" << event.fBarMaxTOF * 1E9 <<std::endl;
   AddVertexEvent(
      event.fRunNumber, event.fEventNo,
      event.fCutsResult, //  event.fBarMaxTOF * 1E9 < 500 && event.fBarMaxTOF >0,// && event.fBarMaxTOF  > 0,
      event.fVertexStatus, event.fVertex.X(), event.fVertex.Y(), event.fVertex.Z(), tMinusOffset, event.fRunTime,
      event.fTPCTime, event.fNumHelices, event.fNumTracks, event.fMVA);
   return;
}

/// @brief Add chronobox event to event
/// @param event
/// @param channel
/// @param StartOffset used for 'zerotime' mode
void TAGPlot::AddEvent(const TCbFIFOEvent &event, const TChronoChannel &channel, const double StartOffset)
{
   if (!event.IsLeadingEdge()) return;
   fChronoEvents.AddEvent(event.GetRunNumber(), event.GetRunTime() - StartOffset, event.GetRunTime(), event.fCounts,
                          channel);
}

/// @brief Add dump gates to container
/// @param runNumber
/// @param description
/// @param dumpIndex
///
/// Implmentation of parents pure virtual function
void TAGPlot::AddDumpGates(const int runNumber, const std::vector<std::string> description,
                           const std::vector<int> dumpIndex)
{
   std::vector<TAGSpill> spills = Get_AG_Spills(runNumber, {description}, {dumpIndex});
   return AddDumpGates(runNumber, spills);
}

/// @brief Add time gates to container with TAGSpills
/// @param runNumber
/// @param spills
void TAGPlot::AddDumpGates(const int runNumber, const std::vector<TAGSpill> spills)
{
   std::vector<double> minTime;
   std::vector<double> maxTime;

   for (auto &spill : spills) {
      if (spill.fScalerData) {
         minTime.push_back(spill.fScalerData->fStartTime);
         maxTime.push_back(spill.fScalerData->fStopTime);
      } else {
         std::cout << "Spill didn't have Scaler data!? Was there an aborted sequence?" << std::endl;
      }
   }
   return AddTimeGates(runNumber, minTime, maxTime);
}

/// @brief Add time gates to container with TAGSpills
/// @param spills
///
/// If spills are from one run, it is slightly faster to call the function above: AddDumpGates(const int runNumber,
/// const std::vector<TAGSpill> spills)
void TAGPlot::AddDumpGates(const std::vector<TAGSpill> spills)
{
   for (const TAGSpill &spill : spills) {
      if (spill.fScalerData) {
         AddTimeGate(spill.fRunNumber, spill.GetStartTime(), spill.GetStopTime());
      } else {
         std::cout << "Spill didn't have Scaler data!? Was there an aborted sequence?" << std::endl;
      }
   }
   return;
}

/// @brief Wrapper function to add time gates to TAGPlot, replace -ve tmax with 'End of run' time from chronobox
void TAGPlot::AddTimeGate(const int runNumber, const double tmin, const double tmax, const double tzero)
{
   AddRunNumber(runNumber);
   if (tmax < 0)
      return AddTimeWindow(runNumber,tmin,GetTotalRunTimeFromChrono(runNumber),tzero);
   else
      return AddTimeWindow(runNumber,tmin,tmax,tzero);
}

/// @brie Load TAGDetectorEvent and Chronobox data into container for a single run
/// @param runNumber 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
void TAGPlot::LoadRun(const int runNumber, const double firstTime, const double lastTime, int verbose)
{
   if (verbose > 0) std::cout << "Loading run " << runNumber << "\n";

   bool AlreadyHaveAnalysisReport = false;
   for (const TAGAnalysisReport &r : fAnalysisReports) {
      if (r.GetRunNumber() == runNumber) {
         AlreadyHaveAnalysisReport = true;
         break;
      }
   }
   if (!AlreadyHaveAnalysisReport) {
      fAnalysisReports.emplace_back(Get_AGAnalysis_Report(runNumber));
   }

   TTreeReaderPointer TPCTreeReader = Get_AGDetectorEvent_TreeReader(runNumber);
   TTreeReaderValue<TAGDetectorEvent> VertexEvent(*TPCTreeReader, "TADetectorEvent");
   TPCTreeReader = FindStartOfTree(TPCTreeReader,VertexEvent,0,TPCTreeReader->GetEntries(),firstTime,1.0);
   while (TPCTreeReader->Next()) {
      const double t = VertexEvent->fRunTime;
      if (t < firstTime) continue;
      if (t > lastTime) break;
      double TimeToNextEvent = AddAGDetectorEvent(*VertexEvent);
      if (TimeToNextEvent > 10.)
      {
         //std::cout << "T to next event: "<< TimeToNextEvent <<std::endl;
         TPCTreeReader = FindStartOfTree(
                            TPCTreeReader,
                            VertexEvent,
                            TPCTreeReader->GetCurrentEntry() + 1,
                            TPCTreeReader->GetEntries(),
                            TimeToNextEvent + t,
                            1.0);
      }
   }
   LoadChronoEvents(runNumber, firstTime, lastTime, verbose);
   return;
}

/// @brief Load Chronobox data into container for a single run
/// @param runNumber 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
void TAGPlot::LoadChronoEvents(const int runNumber, const double firstTime, const double lastTime, int verbose)
{
   SetChronoChannels(runNumber);
   std::set<TChronoChannel> cbs;
   for (UInt_t j = 0; j < fChronoChannels.size(); j++) cbs.insert(fChronoChannels[j]);
   for (const auto &cb : cbs) {
      if (verbose) std::cout << "Loading TreeReader ChronoBox_" << cb << " for run " << runNumber << std::endl;

      TTreeReaderPointer ChronoReader = Get_Chrono_TreeReader(runNumber, cb);
      if (!ChronoReader) continue;
      TTreeReaderValue<TCbFIFOEvent> ChronoEvent(*ChronoReader, "FIFOData");
      ChronoReader = FindStartOfTree(ChronoReader,ChronoEvent,0,ChronoReader->GetEntries(),firstTime,1.0);
      while (ChronoReader->Next()) {
         const double t = ChronoEvent->GetRunTime();
         if (t < firstTime) continue;
         if (t > lastTime) break;
         double TimeToNextEvent = AddChronoEvent(*ChronoEvent); //,fChronoChannels[j].GetBoard());
         if (TimeToNextEvent > 10.)
         {
            //std::cout << "T to next event chrono: "<< TimeToNextEvent <<std::endl;
            ChronoReader = FindStartOfTree(
                              ChronoReader,
                              ChronoEvent,
                              ChronoReader->GetCurrentEntry() + 1,
                              ChronoReader->GetEntries(),
                              TimeToNextEvent + t,
                              1.0);
         }
      }
   }
}

/// @brief Configure the TAGPlot histograms
/// @param cuts 
void TAGPlot::SetUpHistograms(const std::string &cuts)
{
   double XMAX(100.), YMAX(100.), RMAX(100.), ZMAX(1300.), ZMIN(-1300.);
   if (!std::isinf(fZMaxCut)) ZMAX = fZMaxCut;
   if (!std::isinf(fZMinCut)) ZMIN = fZMinCut;

   double minTime;
   double maxTime;
   if (kZeroTimeAxis) {
      minTime = GetBiggestTBeforeZero();
      maxTime = GetBiggestTAfterZero();
   } else {
      minTime = GetFirstTmin();
      maxTime = GetLastTmax();
   }
   std::string units;
   if (GetMaxDumpLength() < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber) {
      fTimeFactor = 1000;
      units       = "[ms]";
   } else {
      fTimeFactor = 1;
      units       = "[s]";
   }

   TH1D       *top        = nullptr;
   TH1D       *bot        = nullptr;
   TH1D       *aandd      = nullptr;
   TH1D       *candf      = nullptr;
   TH1D       *TPC        = nullptr;
   TH1D       *htcut      = nullptr;
   TH1D       *htvtx      = nullptr;
   TH1D       *htvtxcut   = nullptr;
   TH1D       *htbar      = nullptr;
   TH2D       *hzt        = nullptr;
   TH2D       *hphit      = nullptr;
   std::string CutsString = TAPlotCutsMode(cuts).FullCutName();

   switch (rootUtils::GetTimeAxisMode()) {
   case rootUtils::kFixedTimeBinNumber:
      top      = new TH1D((GetTAPlotTitle() + "top_pm").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      bot      = new TH1D((GetTAPlotTitle() + "bot_pm").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      aandd    = new TH1D((GetTAPlotTitle() + "aandd_pm").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      candf    = new TH1D((GetTAPlotTitle() + "candf_pm").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      TPC      = new TH1D((GetTAPlotTitle() + "TPC_TRIG").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      htcut    = new TH1D((GetTAPlotTitle() + "tcut").c_str(),
                          (std::string("t Vertex [") + CutsString + "];t " + units + ";events").c_str(),
                          rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      htvtx    = new TH1D((GetTAPlotTitle() + "tvtx").c_str(),
                          (std::string("t Vertex [") + CutsString + "];t " + units + ";events").c_str(),
                          rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      htvtxcut = new TH1D((GetTAPlotTitle() + "tvtxcut").c_str(),
                          (std::string("t Vertex [") + CutsString + "];t " + units + ";events").c_str(),
                          rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);

      htbar =
         new TH1D((GetTAPlotTitle() + "tbar").c_str(), (std::string("t ;t ") + units + std::string(";events")).c_str(),
                  rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      hzt = new TH2D((GetTAPlotTitle() + "ztvtx").c_str(),
                     (std::string("Z-T Vertex [") + CutsString + "];z [mm];t " + units).c_str(),
                     rootUtils::GetDefaultBinNumber(), ZMIN, ZMAX, rootUtils::GetTimeBinNumber(), minTime * fTimeFactor,
                     maxTime * fTimeFactor);

      hphit = new TH2D((GetTAPlotTitle() + "phitvtx").c_str(),
                       (std::string("Phi-T Vertex ") + CutsString + "];phi [rad];t " + units).c_str(),
                       rootUtils::GetDefaultBinNumber(), -TMath::Pi(), TMath::Pi(), rootUtils::GetTimeBinNumber(),
                       minTime * fTimeFactor, maxTime * fTimeFactor);

      break;
   case rootUtils::kFixedTimeBinSize:

      top      = new TH1D((GetTAPlotTitle() + "top_pm").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      bot      = new TH1D((GetTAPlotTitle() + "bot_pm").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      aandd    = new TH1D((GetTAPlotTitle() + "aandd_pm").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      candf    = new TH1D((GetTAPlotTitle() + "candf_pm").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      TPC      = new TH1D((GetTAPlotTitle() + "TPC_TRIG").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                          rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      htcut    = new TH1D((GetTAPlotTitle() + "tcut").c_str(),
                          (std::string("t Vertex [") + CutsString + "];t " + units + ";events").c_str(),
                          rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      htvtx    = new TH1D((GetTAPlotTitle() + "tvtx").c_str(),
                          (std::string("t Vertex [") + CutsString + "];t " + units + ";events").c_str(),
                          rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      htvtxcut = new TH1D((GetTAPlotTitle() + "tvtxcut").c_str(),
                          (std::string("t Vertex [") + CutsString + "];t " + units + ";events").c_str(),
                          rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);

      htbar =
         new TH1D((GetTAPlotTitle() + "tbar").c_str(), (std::string("t ;t ") + units + std::string(";events")).c_str(),
                  rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      hzt = new TH2D((GetTAPlotTitle() + "ztvtx").c_str(),
                     (std::string("Z-T Vertex [") + CutsString + "];z [mm];t " + units).c_str(),
                     rootUtils::GetDefaultBinNumber(), ZMIN, ZMAX,
                     rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);

      hphit = new TH2D((GetTAPlotTitle() + "phitvtx").c_str(),
                       (std::string("Phi-T Vertex ") + CutsString + "];phi [rad];t " + units).c_str(),
                       rootUtils::GetDefaultBinNumber(), -TMath::Pi(), TMath::Pi(),
                       rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      break;
   default: std::cerr << "Unknown TimeAxisMode from rootUtils\n"; break;
   }
   if (top) {
      top->SetLineColor(kGreen);
      top->SetMarkerColor(kGreen);
      top->SetMinimum(0);
      AddHistogram("top_pm", top);
   }

   if (bot) {
      bot->SetLineColor(kAzure - 8);
      bot->SetMarkerColor(kAzure - 8);
      bot->SetMinimum(0);
      AddHistogram("bot_pm", bot);
   }

   if (aandd) {
      aandd->SetLineColor(kMagenta);
      aandd->SetMarkerColor(kMagenta);
      aandd->SetMinimum(0);
      AddHistogram("aandd_pm", aandd);
   }

   if (candf) {
      candf->SetLineColor(kAzure - 8);
      candf->SetMarkerColor(kAzure - 8);
      candf->SetMinimum(0);
      AddHistogram("candf_pm", candf);
   }

   if (TPC) {
      TPC->SetMarkerColor(kRed);
      TPC->SetLineColor(kRed);
      TPC->SetMinimum(0);
      AddHistogram("TPC_TRIG", TPC);
   }

   AddHistogram("zvtx",
                new TH1D((GetTAPlotTitle() + "zvtx").c_str(), Form("Z Vertex [%s];z [mm];events", CutsString.c_str()),
                         rootUtils::GetDefaultBinNumber(), ZMIN, ZMAX));

   TH1D *hr = new TH1D((GetTAPlotTitle() + "rvtx").c_str(), Form("R Vertex [%s];r [mm];events", CutsString.c_str()),
                       rootUtils::GetDefaultBinNumber(), 0., RMAX);
   hr->SetMinimum(0);
   AddHistogram("rvtx", hr);

   TH1D *hphi =
      new TH1D((GetTAPlotTitle() + "phivtx").c_str(), Form("phi Vertex [%s];phi [rad];events", CutsString.c_str()),
               rootUtils::GetDefaultBinNumber(), -TMath::Pi(), TMath::Pi());
   hphi->SetMinimum(0);
   AddHistogram("phivtx", hphi);

   TH2D *hxy = new TH2D((GetTAPlotTitle() + "xyvtx").c_str(), Form("X-Y Vertex [%s];x [mm];y [mm]", CutsString.c_str()),
                        rootUtils::GetDefaultBinNumber(), -XMAX, XMAX, rootUtils::GetDefaultBinNumber(), -YMAX, YMAX);
   AddHistogram("xyvtx", hxy);

   TH2D *hzr = new TH2D((GetTAPlotTitle() + "zrvtx").c_str(), Form("Z-R Vertex [%s];z [mm];r [mm]", CutsString.c_str()),
                        rootUtils::GetDefaultBinNumber(), ZMIN, ZMAX, rootUtils::GetDefaultBinNumber(), 0., RMAX);
   AddHistogram("zrvtx", hzr);

   TH2D *hzphi = new TH2D(
      (GetTAPlotTitle() + "zphivtx").c_str(), Form("Z-Phi Vertex [%s];z [mm];phi [rad]", CutsString.c_str()),
      rootUtils::GetDefaultBinNumber(), ZMIN, ZMAX, rootUtils::GetDefaultBinNumber(), -TMath::Pi(), TMath::Pi());
   AddHistogram("zphivtx", hzphi);

   if (htcut) {
      htcut->SetLineColor(kGreen);
      htcut->SetLineStyle(2);
      htcut->SetMarkerColor(kGreen);
      htcut->SetMinimum(0);
      fHistos.Add(htcut);
      fHistoPositions["tcut"] = fHistos.GetEntries() - 1;
   }

   if (htvtx) {
      htvtx->SetLineColor(kMagenta);
      htvtx->SetLineStyle(9);
      htvtx->SetMarkerColor(kMagenta);
      htvtx->SetMinimum(0);
      fHistos.Add(htvtx);
      fHistoPositions["tvtx"] = fHistos.GetEntries() - 1;
   }

   if (htvtxcut) {
      htvtxcut->SetLineColor(kBlue);
      htvtxcut->SetMarkerColor(kBlue);
      htvtxcut->SetMinimum(0);
      fHistos.Add(htvtxcut);
      fHistoPositions["tvtxcut"] = fHistos.GetEntries() - 1;
   }

   if (htbar) {
      htbar->SetMinimum(0);
      AddHistogram("tbar", htbar);
   }

   if (hzt) AddHistogram("ztvtx", hzt);

   if (hphit) AddHistogram("phitvtx", hphit);

   // if (MVAMode)
   //    ht_MVA = new TH1D("htMVA", "Vertex, Passcut and MVA;t [ms];Counts", GetNBins(), TMin*1000., TMax*1000.);
   return;
}

/// @brief Comfigure BV Bar histograms
void SetUpBarHistos()
{
   // Get some cool looking bar plots going!
}

/// @brief Fill the TAGPlot histograms
/// @param CutsMode 
void TAGPlot::FillHisto(const std::string &CutsMode)
{

   ClearHisto();
   SetUpHistograms(CutsMode);

   FillChronoHistograms();
   // Fill Vertex Histograms
   FillVertexHistograms(CutsMode);
}

/// @brief Fill the chronobox histograms
void TAGPlot::FillChronoHistograms()
{
   int runNum = 0;
   // Fill Chronobox events
   for (size_t i = 0; i < fChronoEvents.size(); i++) {
      if (fChronoEvents.RunNumber(i) != runNum) {
         runNum = fChronoEvents.RunNumber(i);
         SetChronoChannels(runNum);
      }
      double time;
      if (kZeroTimeAxis)
         time = fChronoEvents.Time(i);
      else
         time = fChronoEvents.OfficialTime(i);
      if (GetMaxDumpLength() < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber)
         time = time * 1000.;

      const TChronoChannel &channel         = fChronoEvents.fChronoChannel[i];
      const int             CountsInChannel = fChronoEvents.Counts(i);
      const int             runNumber       = fChronoEvents.RunNumber(i);

      if (channel == top.find(runNumber)->second)
         FillHistogram("top_pm", time, CountsInChannel);
      else if (channel == bottom.find(runNumber)->second)
         FillHistogram("bot_pm", time, CountsInChannel);
      else if (channel == sipmad.find(runNumber)->second)
         FillHistogram("aandd_pm", time, CountsInChannel);
      else if (channel == sipmcf.find(runNumber)->second)
         FillHistogram("candf_pm", time, CountsInChannel);
      else if (channel == TPC_TRIG.find(runNumber)->second)
         FillHistogram("TPC_TRIG", time, CountsInChannel);
      else
         std::cout << "Unconfigured Chrono channel in TAGPlot... Run: " << runNumber << "\t" << channel << std::endl;
   }
}

/// @brief Fill vertex histograms
/// @param CutsMode 
void TAGPlot::FillVertexHistograms(const std::string &CutsMode)
{
   TAPlotCutsMode applyCuts(CutsMode);
   TVector3       vertex;
   const size_t   NVertexEvents = fVertexEvents.size();
   for (size_t i = 0; i < NVertexEvents; i++) {
      Double_t time;
      if (kZeroTimeAxis)
         time = fVertexEvents.Times(i);
      else
         time = fVertexEvents.RunTime(i);
      if (GetMaxDumpLength() < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber)
         time = time * 1000.;

      FillHistogram("tTPC", time);

      bool PassCuts = applyCuts.ApplyCuts(fVertexEvents.CutsResults(i));
      // if (fVertexEvents.NBars[i] > BarMultiplicityCut)
      //    FillHistogram("tbar",time);
      if (PassCuts) // Passed cut result!
      {
         FillHistogram("tcut", time);
      }
      if (fVertexEvents.VertexStatus(i) <= 0) continue; // Don't draw invaid vertices
      FillHistogram("tvtx", time);
      if (!PassCuts) continue; // Passed cut result!
      FillHistogram("tvtxcut", time);
      vertex = fVertexEvents.VertexVector(i);
      FillHistogram("phivtx", vertex.Phi());
      FillHistogram("zphivtx", vertex.Z(), vertex.Phi());
      // FillHistogram("phitvtx",vtx.Phi(),time);
      FillHistogram("xyvtx", vertex.X(), vertex.Y());
      FillHistogram("zvtx", vertex.Z());
      FillHistogram("rvtx", vertex.Perp());
      FillHistogram("zrvtx", vertex.Z(), vertex.Perp());
      FillHistogram("ztvtx", vertex.Z(), time);
   }
   TH1D *rHisto = GetTH1D("rvtx");
   if (rHisto) {
      TH1D *rDensityHisto = (TH1D *)rHisto->Clone("radial density");
      rDensityHisto->Sumw2();
      TF1 *function = new TF1("fr", "x", -100, 100);
      rDensityHisto->Divide(function);
      rDensityHisto->Scale(rHisto->GetBinContent(rHisto->GetMaximumBin()) /
                           rDensityHisto->GetBinContent(rDensityHisto->GetMaximumBin()));
      rDensityHisto->SetMarkerColor(kRed);
      rDensityHisto->SetLineColor(kRed);
      delete function;
      AddHistogram("rdens", rDensityHisto);
   }
}

/// @brief Draw the TAGPlot
/// @param name  Give name to canvas and histograms (can avoid the memory leak warnings to unique names per plot)
/// @param CutsMode Apply cuts to the drawn data
/// @return 
TCanvas *TAGPlot::DrawVertexCanvas(const char *name, const std::string CutsMode)
{
   SetTAPlotTitle(name);
   if (fVertexEvents.size() == 0 && fChronoEvents.size() == 0) {
      std::cout << "Nothing to draw... no verticies, no chrono events " << std::endl;
      return nullptr;
   }
   std::cout << "TAGPlot Processing time : ~" << GetApproximateProcessingTime() << "s" << std::endl;
   TCanvas *canvas = new TCanvas(name, name, 1800, 1000);
   std::cout << "Filling histograms" << std::endl;
   FillHisto(CutsMode);
   std::cout << "Filling histograms done" << std::endl;
   // Scale factor to scale down to ms:
   if (GetMaxDumpLength() < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber)
      SetTimeFactor(1000.);
   canvas->Divide(4, 2);

   if (fLegendDetail >= 1) {
      gStyle->SetOptStat(11111);
   } else {
      gStyle->SetOptStat("ni"); // just like the knights of the same name
   }

   // Canvas 1
   canvas->cd(1);
   DrawHistogram("zvtx", "HIST E1");

   // Canvas 2
   canvas->cd(2); // Z-counts (with electrodes?)4
   TVirtualPad *cVTX_1 = canvas->cd(2);
   gPad->Divide(1, 2);
   // Canvas 2 - Pad 1
   cVTX_1->cd(1);
   DrawHistogram("rvtx", "HIST E1");
   DrawHistogram("rdens", "HIST E1 SAME");
   TPaveText *radialDensityLabel = new TPaveText(0.6, 0.8, 0.90, 0.85, "NDC NB");
   radialDensityLabel->AddText("radial density [arbs]");
   radialDensityLabel->SetTextColor(kRed);
   radialDensityLabel->SetFillStyle(0);
   radialDensityLabel->SetLineStyle(0);
   radialDensityLabel->Draw();
   // Canvas 2 - Pad 2
   cVTX_1->cd(2);
   DrawHistogram("phivtx", "HIST E1");

   // Canvas 3
   canvas->cd(3); // T-counts

   TLegend *legend = 0;
   // Find tallest histogram:
   double trigmax = GetTH1DMaximum({"TPC_TRIG", "tTPC", "top_pm", "bot_pm"});
   // This is the first plot drawn, so we can set the range now
   GetTH1D("TPC_TRIG")->SetMaximum(trigmax * 1.05);

   DrawHistogram("TPC_TRIG", "HIST SAME");
   legend = AddLegendIntegral(legend, "Triggers: %5.0lf", "TPC_TRIG");
   // DrawHistogram("tvtx","HIST SAME");
   // legend=AddLegendIntegral(legend,"Verts: %5.0lf","ttvtx");
   // DrawHistogram("tbar","HIST SAME");
   // legend=AddLegendIntegral(legend,"Reads: %5.0lf","ttbar");
   DrawHistogram("tTPC", "HIST SAME");
   legend = AddLegendIntegral(legend, "Reads: %5.0lf", "tTPC");
   DrawHistogram("aandd_pm", "HIST SAME");
   legend = AddLegendIntegral(legend, "SIPM_BOTG_TOP_OR: %5.0lf", "aandd_pm");
   DrawHistogram("top_pm", "HIST SAME");
   legend = AddLegendIntegral(legend, "SIPM_UDS: %5.0lf", "top_pm");
   DrawHistogram("bot_pm", "HIST SAME");
   legend = AddLegendIntegral(legend, "SIPM_LDS: %5.0lf", "bot_pm");

   DrawHistogram("tvtxcut", "HIST SAME");
   if (CutsMode > 0) {

      std::string name = "Pass Cuts [" + TAPlotCutsMode(CutsMode).FullCutName() + " ] : %5.0lf";
      legend           = AddLegendIntegral(legend, name.c_str(), "tvtxcut");
   } else {
      legend = AddLegendIntegral(legend, "Vertices: %5.0lf", "tvtxcut");
   }
   // legend=DrawLines(legend,"tTPC");

   // Canvas 4
   canvas->cd(4);
   DrawHistogram("xyvtx", "colz");

   // Canvas 5
   canvas->cd(5);
   DrawHistogram("ztvtx", "colz");

   // Canvas 6
   canvas->cd(6);
   TVirtualPad *subPadCD6 = NULL;
   if (IsGEMData() && IsLVData()) {
      subPadCD6 = canvas->cd(6);
      gPad->Divide(1, 2);
   }
   if (subPadCD6) {
      subPadCD6->cd(1);
      std::pair<TLegend *, TMultiGraph *> gemDataMG = GetGEMGraphs();
      if (gemDataMG.first) {
         // Draw TMultigraph
         gemDataMG.second->Draw("AL*");
         // Draw legend
         gemDataMG.first->Draw();
      }

      subPadCD6->cd(2);
      std::pair<TLegend *, TMultiGraph *> labviewDataMG = GetLVGraphs();
      if (labviewDataMG.first) {
         // Draw TMultigraph
         labviewDataMG.second->Draw("AL*");
         // Draw legend
         labviewDataMG.first->Draw();
      }
   }
   // Canvas 7
   canvas->cd(7);
   // Find tallest histogram:
   double ymax = GetTH1DMaximum({"tvtxcut", "tcut", "tvtx"});
   // This is the first plot drawn, so we can set the range now
   GetTH1D("tvtxcut")->SetMaximum(ymax * 1.05);

   TLegend *legend7 = 0;
   DrawHistogram("tvtxcut", "HIST SAME");
   legend7 = AddLegendIntegral(legend7, "Pass cut & good vertex: %5.0lf", "tvtxcut");
   DrawHistogram("tvtx", "HIST SAME");
   legend7 = AddLegendIntegral(legend7, "Good vertex: %5.0lf", "tvtx");

   // This is the last plot drawn, so we can set the range now
   // gPad->GetYAxis()->SetUserRange(0,ymax);
   // Canvas 8
   canvas->cd(8);
   DrawHistogram("zphivtx", "colz");
   if (CutsMode > 0) {
      canvas->cd(1);
      TPaveText  *applyCutsLabel = new TPaveText(0., 0.95, 0.20, 1.0, "NDC NB");
      std::string mode_string    = "Cuts type " + TAPlotCutsMode(CutsMode).FullCutName() + " applied";
      applyCutsLabel->AddText(mode_string.c_str());
      applyCutsLabel->SetTextColor(kRed);
      applyCutsLabel->SetFillColor(kWhite);
      applyCutsLabel->Draw();
   }

   // Canvas 0 - Global
   canvas->cd(0);
   TString runText = GetTAPlotTitle();
   runText += " | Run(s): ";
   runText += GetListOfRuns();
   runText += " | ";
   runText += std::to_string(GetTotalTime());
   runText += "s | ";
   if (fZMinCut != -std::numeric_limits<double>::infinity())
      runText += std::string("Z > ") + fZMinCut + " | ";
   if (fZMaxCut != std::numeric_limits<double>::infinity())
      runText += std::string("Z < ") + fZMaxCut + " | ";
   runText += rootUtils::GetDefaultBinNumber();
   runText += " bins | ";

   switch (rootUtils::GetTimeAxisMode()) {
   case rootUtils::kFixedTimeBinNumber:
      runText += rootUtils::GetTimeBinNumber();
      runText += " time bins";
      break;
   case rootUtils::kFixedTimeBinSize:
      runText += rootUtils::GetTimeBinSize();
      runText += "s time bins";
      break;
   default: std::cerr << "Unknown TimeAxisMode from rootUtils\n"; break;
   }
   runText += " | ";
   runText += GetListOfJSONFiles();

   TLatex *runsLabel = new TLatex(0., 0.0012, runText);
   runsLabel->SetTextSize(0.014);
   runsLabel->Draw();

   std::cout << runText << std::endl;
   return canvas;
}

/// @brief Export container to CSV
/// @param filename
/// @param option select what data gets exported to csv
///
/// options (order doesn't matter):
/// "v" for vertex (filename.vertex.csv)
/// "t" for timewindows (filename.timewindows.csv)
/// "a" for AnalysisReport (filename.AnalysisReport.csv)
/// "s" for scaler, ie chronobox (filename.scaler.csv)
void TAGPlot::ExportCSV(const std::string filename, const std::string options)
{
   // Save Time windows and vertex data
   TAPlot::ExportCSV(filename,options);

   // Save AG AnalysisReport data if 'a' is listed in options
   if (options.find('a') != std::string::npos)
   {
      std::string   analysisReportFilename = filename + ".AnalysisReport.csv";
      std::ofstream aReport;
      aReport.open(analysisReportFilename);
      aReport << fAnalysisReports.front().CSVTitleLine();
      for (const TAGAnalysisReport &r : fAnalysisReports) aReport << r.CSVLine();
      aReport.close();
      std::cout << analysisReportFilename << " saved\n";
   }

   // Save chronobox data if 's' is listed in options
   if (options.find('s') != std::string::npos)
   {
      std::string   scalerFilename = filename + ".scaler.csv";
      std::ofstream chrono;
      chrono.open(scalerFilename);
      chrono << fChronoEvents.CSVTitleLine();
      for (size_t i = 0; i < fChronoEvents.size(); i++) chrono << fChronoEvents.CSVLine(i);
      chrono.close();
      std::cout << scalerFilename << " saved\n";
   }
}

/// @brief Get the anasettings json filename used to generate the loaded root files from the TAnalysisReport
/// @return
std::string TAGPlot::GetListOfJSONFiles()
{
   std::set<std::string> json_files;
   // Grab only unique json files... no need to spam the same string
   for (const TAGAnalysisReport &r : fAnalysisReports)
      json_files.insert(r.GetAnaSettingsFilename());
   // unfold std::set into stingle string
   std::string list;
   for (const std::string& s: json_files)
   {
      if (list.size())
         list += ", ";
      list += s;
   }
   return list;
}

/// @brief Reset entire container and remove all vertex data (time windows untouched)
void TAGPlot::Reset()
{
   fEjections.clear();
   fInjections.clear();
   fDumpStarts.clear();
   fDumpStops.clear();
   fChronoChannels.clear();
   fRuns.clear();
   fVertexEvents.Clear();
   fChronoEvents.Clear();
}

#endif
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
