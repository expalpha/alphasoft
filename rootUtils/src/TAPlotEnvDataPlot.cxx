
#include "TAPlotEnvDataPlot.h"

/// @brief Constructor
TAPlotEnvDataPlot::TAPlotEnvDataPlot()
{
}

/// @brief Deconstructor
TAPlotEnvDataPlot::~TAPlotEnvDataPlot()
{
}

/// @brief Copy constructor
/// @param envDataPlot 
TAPlotEnvDataPlot::TAPlotEnvDataPlot(const TAPlotEnvDataPlot& envDataPlot) : TObject(envDataPlot)
{
   fTimes = envDataPlot.fTimes;
   fRunTimes = envDataPlot.fRunTimes;
   fData = envDataPlot.fData;
}

/// = operator
TAPlotEnvDataPlot TAPlotEnvDataPlot::operator=(const TAPlotEnvDataPlot& rhs)
{
   this->fTimes = rhs.fTimes;
   this->fRunTimes = rhs.fRunTimes;
   this->fData = rhs.fData;
   return *this;
}

/// @brief Add single data point to container
/// @param runNumber 
/// @param time 
/// @param runTime 
/// @param data 
void TAPlotEnvDataPlot::AddPoint(int runNumber, double time, double runTime, double data)
{
   fRunNumber.emplace_back(runNumber);
   fTimes.emplace_back(time);
   fRunTimes.emplace_back(runTime);
   fData.emplace_back(data);
}

/// @brief Generate a TGraph from data in this container
/// @param zeroTime 
/// @return 
/// If zeroTime is True, use the fTimes (user given time with offset) for the X axis
/// If zeroTime if False, use the fRunTimes (run time) for the X axis
TGraph* TAPlotEnvDataPlot::NewGraph(bool zeroTime) const
{
   if (zeroTime)
      return new TGraph(fData.size(),fTimes.data(),fData.data());
   else
      return new TGraph(fData.size(),fRunTimes.data(),fData.data());
}
