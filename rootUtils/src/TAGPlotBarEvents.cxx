
#ifdef BUILD_AG

#include "TAGPlotBarEvents.h"

ClassImp(TAGPlotBarEvents)

TAGPlotBarEvents::TAGPlotBarEvents()
{

}

TAGPlotBarEvents::~TAGPlotBarEvents()
{

}
// Coipy ctor
TAGPlotBarEvents::TAGPlotBarEvents(const TAGPlotBarEvents &h)
   : TObject(h), fRunNumber(h.fRunNumber), fTime(h.fTime), fOfficialTime(h.fOfficialTime),
     fNTracksMatched(h.fNTracksMatched), fNTracksUnmatched(h.fNTracksUnmatched), fNPileUpTracks(h.fNPileUpTracks)
{
}


TAGPlotBarEvents &TAGPlotBarEvents::operator=(const TAGPlotBarEvents &h)
{
   fRunNumber = h.fRunNumber;
   fTime = h.fTime;
   fOfficialTime = h.fOfficialTime;
   fNTracksMatched = h.fNTracksMatched;
   fNTracksUnmatched = h.fNTracksUnmatched;
   fNPileUpTracks = h.fNPileUpTracks;
   return *this;
}

TAGPlotBarEvents &TAGPlotBarEvents::operator+=(const TAGPlotBarEvents &h)
{
   fRunNumber.insert(fRunNumber.end(), h.fRunNumber.begin(), h.fRunNumber.end());
   fTime.insert(fTime.end(), h.fTime.begin(), h.fTime.end());
   fOfficialTime.insert(fOfficialTime.end(), h.fOfficialTime.begin(), h.fOfficialTime.end());
   fNTracksMatched.insert(fNTracksMatched.end(), h.fNTracksMatched.begin(), h.fNTracksMatched.end());
   fNTracksUnmatched.insert(fNTracksUnmatched.end(), h.fNTracksUnmatched.begin(), h.fNTracksUnmatched.end());
   fNPileUpTracks.insert(fNPileUpTracks.end(),h.fNPileUpTracks.begin(),h.fNPileUpTracks.end());
   return *this;
}

void TAGPlotBarEvents::AddEvent(int runNumber, double time, double officialTime, int nmatch, int nunmatch, int npileup, const TBarEvent& barEvt, double vertexZ)
{
   fRunNumber.push_back(runNumber);
   fTime.push_back(time);
   fOfficialTime.push_back(officialTime);
   fNTracksMatched.push_back(nmatch);
   fNTracksUnmatched.push_back(nunmatch);
   fNPileUpTracks.push_back(npileup);
   fBarEvents.push_back(barEvt);
   fVertexZ.push_back(vertexZ);
}

void TAGPlotBarEvents::clear()
{
   fRunNumber.clear();
   fTime.clear();
   fOfficialTime.clear();
   fNTracksMatched.clear();
   fNTracksUnmatched.clear();
   fNPileUpTracks.clear();
}

#endif