#if BUILD_A2
#if HAVE_MIDAS
#include "midas.h"
#endif
#include "TA2PlotQOD.h"

TA2PlotQOD::TA2PlotQOD(bool zeroTime) : TA2Plot(zeroTime) {}

TA2PlotQOD::TA2PlotQOD(double zMin, double zMax, bool zeroTime) : TA2Plot(zMin, zMax, zeroTime) {}

TA2PlotQOD::TA2PlotQOD(const TA2PlotQOD &object) : TA2Plot(object), fQODs(object.fQODs) {}

TA2PlotQOD::~TA2PlotQOD() {}

int TA2PlotQOD::AddSVDEvent(const TSVD_QOD &qod)
{
   int events_added = 0;
   events_added += TA2Plot::AddSVDEvent(qod);
   // TA2Plot didn't add events (time cut or z cut failed)
   if (events_added == 0)
      return events_added;
   fQODs.push_back(qod);
   return events_added;
}

void TA2PlotQOD::SetUpHistograms(const size_t n_cuts)
{
   double minTime;
   double maxTime;
   if (kZeroTimeAxis) {
      minTime = GetBiggestTBeforeZero();
      maxTime = GetBiggestTAfterZero();
   } else {
      minTime = GetFirstTmin();
      maxTime = GetLastTmax();
   }

   fOccupancyT.reserve( n_cuts);

   for (size_t i = 0; i < n_cuts; i++) {
      std::string name  = std::string("Hit Occupancy ") + fQODs.front().GetOnlinePassCutsName(i);
      std::string title = std::string("Hit Occupancy (") + fQODs.front().GetOnlinePassCutsName(i) +
                          ") ; Run Time(s); Silicon Module No;";
         fOccupancy.push_back(
            new TH1I(name.c_str(), title.c_str(),
            nSil, 0, nSil)
         );
         fOccupancyT.push_back(
         new TH2I(name.c_str(), title.c_str(),
         rootUtils::GetTimeBinNumber(),minTime, maxTime,
         nSil, 0, nSil)
      );
   }
}


void TA2PlotQOD::FillHistograms()
{

   for (size_t i = 0; i < fQODs.size(); i++) {
      for (size_t j = 0; j < fQODs[i].GetPassedCuts().size(); j++) {
         if (!fQODs[i].GetOnlinePassCuts(j)) continue;
         for (int k = 0; k < nSil; k++) {
            const int hits = fQODs[i].GetHitOccupancy(k);
            double time;
            if (kZeroTimeAxis)
               time =  GetVertexEvents().Times(i);
            else
               time =  GetVertexEvents().RunTime(i);
            if (hits) {
               fOccupancy[j]->Fill(k);
               fOccupancyT[j]->Fill(time, k, hits);
               //std::cout << time << "\t" << k << "\t" << hits << std::endl;
            }
         }
      }
   }
}


TCanvas* TA2PlotQOD::DrawSummedOccupancy(const std::string& name, int CutsMode)
{
   SetTAPlotTitle(name);
   if (fQODs.empty()) {
      std::cerr << "No QODs to plot" << std::endl;
      return nullptr;
   }

   if (fQODs.size() != GetVertexEvents().size()) {
      std::cerr << "Number of QODs and number of events do not match" <<
         "(" << fQODs.size() << "!=" << GetVertexEvents().size() << ")" << std::endl;
      return nullptr;
   }

   const size_t n_cuts = fQODs.front().GetPassedCuts().size();
   SetUpHistograms(n_cuts);
   TCanvas *liveOccupancy = new TCanvas(fCanvasTitle.c_str(), fCanvasTitle.c_str(), 800, 600);

   FillHistograms();
   if (CutsMode < 0) {
      liveOccupancy->Divide(1, n_cuts);
      for (size_t i = 0; i < n_cuts; i++) {
         liveOccupancy->cd(i + 1);
         fOccupancy[i]->Draw("HIST E1");
      }
   } else {
      fOccupancy[CutsMode]->Draw("HIST E1");
   }
   //liveOccupancy->Update();
   liveOccupancy->Draw();
   return liveOccupancy;
}


TCanvas* TA2PlotQOD::DrawOccupancy(const std::string &name, int CutsMode)
{
   SetTAPlotTitle(name);
   if (fQODs.empty()) {
      std::cerr << "No QODs to plot" << std::endl;
      return nullptr;
   }

   if (fQODs.size() != GetVertexEvents().size()) {
      std::cerr << "Number of QODs and number of events do not match" <<
         "(" << fQODs.size() << "!=" << GetVertexEvents().size() << ")" << std::endl;
      return nullptr;
   }
   const size_t n_cuts = fQODs.front().GetPassedCuts().size();
   SetUpHistograms(n_cuts);

   TCanvas *liveOccupancy = new TCanvas(fCanvasTitle.c_str(), fCanvasTitle.c_str(), 800, 600);

   FillHistograms();
   if (CutsMode < 0) {
      liveOccupancy->Divide(1, n_cuts);
      for (size_t i = 0; i < n_cuts; i++) {
         liveOccupancy->cd(i + 1);
         fOccupancyT[i]->Draw();
      }
   } else {
      fOccupancyT[CutsMode]->Draw();
   }
   //liveOccupancy->Update();
   liveOccupancy->Draw();
   return liveOccupancy;
}

bool TA2PlotQOD::LikelyCosmicRun()
{
   if (IsDataStale()) LoadData();
   // Likely cosmic run
   std::vector<TA2Spill> spills;
   // Loops over all the runs in the container
   for (int runNumber: GetRunsInContainer()) {
      std::vector<TA2Spill> s = Get_A2_Spills(runNumber, 0., -1);
   
      for (TA2Spill& spill: s) {

         std::pair<double,double> time = GetTimeWindows().GetWindowTimeRange(runNumber);
         if (
            // If spill start time is within the time window
            (spill.GetStartTime() > time.first && spill.GetStartTime() < time.second )
            // or
            ||
            // If spill stop time is within the time window
            (spill.GetStopTime() > time.first && spill.GetStopTime() < time.second)
         )
            spills.push_back(spill);
      }
   }
   if (spills.empty()) return true;

   // Look for spills with the name "Mixing" or "Pbar"
   for (TA2Spill& spill: spills) {
      if (spill.GetSequenceName() != "atm")
         continue;
      std::string name = spill.GetSanitisedName();
      //std::cout << "Checking spill: " << name << "\n";
      if (name.find("Mixing") != std::string::npos)
         return false;
      if (name.find("Pbar") != std::string::npos)
         return false;
   }
   // No spill have antiproton like spills
   return true;
}

void CheckValueRange(const std::pair<double, double>& rate, double low, double high, const std::string& name)
{
   if (rate.first + 3 * rate.second < low)
   {
      std::cout << name << " rate is too low (for cosmics)" << std::endl;
#if HAVE_MIDAS
   cm_msg1(MTALK, "OfflineQOD", "TA2PlotQOD", "%s rate is too low (for cosmics)", name.c_str()); 
#endif
   }
   if (rate.first - 3 * rate.second > high)
   {
      std::cout << name << " rate is too high (for cosmics)" << std::endl;
#if HAVE_MIDAS
   cm_msg1(MTALK, "OfflineQOD", "TA2PlotQOD", "%s rate is too high (for cosmics)", name.c_str());
#endif
   }
}

void TA2PlotQOD::RunCosmicChecks()
{
   if (not LikelyCosmicRun())
   {
      std::cout << "This time window is not likely a cosmic run (based off the spill log)" << std::endl;
   }
   std::cout << "VF48 Rate:\t";
   auto rate = GetSISRate(
      GetTrigNoBusyChannel(
         *GetRunsInContainer().begin()
      )
   );
   std::cout << rate.first << " +/- " << rate.second << " Hz" << std::endl;
   CheckValueRange(rate, 8, 12, "VF48 trigger");

   std::cout << "Read event Rate:\t";
   rate = GetVertexRate();
   std::cout << rate.first << " +/- " << rate.second << " Hz" << std::endl;
   CheckValueRange(rate, 8, 12, "Read");

   std::cout << "Vertex Cut Rate:\t";
   rate = GetPassCutRate("0");
   std::cout << rate.first << " +/- " << rate.second << " Hz" << std::endl;
   CheckValueRange(rate, 3, 7, "Vertex rate");

   std::cout << "Pass Cut Rate:\t";
   rate = GetPassCutRate("1");
   // ~45mHz ~= once in 22 seconds
   if ( GetTotalTime() > 60.)
     CheckValueRange(rate, 0.045 - 0.020, 0.045 + 0.020, "Pass rate");
   std::cout << rate.first << " +/- " << rate.second << " Hz" << std::endl;

   std::cout << "Online MVA Cut Rate:\t";
   rate = GetPassCutRate("2");
   std::cout << rate.first << " +/- " << rate.second << " Hz" << std::endl;
   // Its reasonable unlikely that an online MVA event happens... 
   // only check range when we have decent statistics (4mHz ~= once in 250 seconds)
   if ( GetTotalTime() > 600.)
     CheckValueRange(rate, 0.007 - 0.004, 0.007 + 0.004, "Online MVA rate");
}

#endif
