#include "TAPlotFEGEMData.h"
/// @brief Constructor
TAPlotFEGEMData::TAPlotFEGEMData() {}

/// @brief Constructor
/// @param varname 
/// @param label 
/// @param index 
TAPlotFEGEMData::TAPlotFEGEMData(const std::string &varname, const std::string label, const int index)
   : TAPlotEnvData(varname, label, index)
{
}

/// @brief Deconstructor
TAPlotFEGEMData::~TAPlotFEGEMData() {}
