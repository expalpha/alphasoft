
#ifdef BUILD_A2

#include "TA2PlotHelixEvents.h"

ClassImp(TA2PlotHelixEvents)

TA2PlotHelixEvents::TA2PlotHelixEvents()
{

}

TA2PlotHelixEvents::~TA2PlotHelixEvents()
{

}
// Coipy ctor
TA2PlotHelixEvents::TA2PlotHelixEvents(const TA2PlotHelixEvents &h)
   : TObject(h), fRunNumber(h.fRunNumber), fTime(h.fTime), fOfficialTime(h.fOfficialTime),
     Chi2(h.Chi2), parD(h.parD), Curvature(h.Curvature), Phi(h.Phi), Lambda(h.Lambda), Z0(h.Z0),
     nPoints(h.nPoints), fCutResults(h.fCutResults)
{
}


TA2PlotHelixEvents &TA2PlotHelixEvents::operator=(const TA2PlotHelixEvents &h)
{
   fRunNumber = h.fRunNumber;
   fTime = h.fTime;
   fOfficialTime = h.fOfficialTime;
   Chi2 = h.Chi2;
   parD = h.parD;
   Curvature = h.Curvature;
   Phi = h.Phi;
   Lambda = h.Lambda;
   Z0 = h.Z0;
   nPoints = h.nPoints;
   fCutResults.assign(h.fCutResults.begin(),h.fCutResults.end());
   return *this;
}

TA2PlotHelixEvents &TA2PlotHelixEvents::operator+=(const TA2PlotHelixEvents &h)
{
   fRunNumber.insert(fRunNumber.end(), h.fRunNumber.begin(), h.fRunNumber.end());
   fTime.insert(fTime.end(), h.fTime.begin(), h.fTime.end());
   fOfficialTime.insert(fOfficialTime.end(), h.fOfficialTime.begin(), h.fOfficialTime.end());
   Chi2.insert(Chi2.end(), h.Chi2.begin(), h.Chi2.end());
   parD.insert(parD.end(), h.parD.begin(), h.parD.end());
   Curvature.insert(Curvature.end(), h.Curvature.begin(), h.Curvature.end());
   Phi.insert(Phi.end(), h.Phi.begin(), h.Phi.end());
   Lambda.insert(Lambda.end(), h.Lambda.begin(), h.Lambda.end());
   Lambda.insert(Z0.end(), h.Z0.begin(), h.Z0.end());
   nPoints.insert(nPoints.end(), h.nPoints.begin(), h.nPoints.end());
   fCutResults.insert(fCutResults.end(), h.fCutResults.begin(), h.fCutResults.end() );
   return *this;
}

void TA2PlotHelixEvents::AddEvent(int runNumber, double time, double officialTime, double chi2,
                                  double pard, double curvature, double phi, double lambda, double z0,
                                  int npoints, const std::vector<bool>& cuts)
{
   fRunNumber.push_back(runNumber);
   fTime.push_back(time);
   fOfficialTime.push_back(officialTime);
   Chi2.push_back(chi2);
   parD.push_back(pard);
   Curvature.push_back(curvature);
   Phi.push_back(phi);
   Lambda.push_back(lambda);
   Z0.push_back(z0);
   nPoints.push_back(npoints);
   fCutResults.emplace_back();
   fCutResults.back().push_back(cuts);
}

void TA2PlotHelixEvents::clear()
{
   fRunNumber.clear();
   fTime.clear();
   fOfficialTime.clear();
   Chi2.clear();
   parD.clear();
   Curvature.clear();
   Phi.clear();
   Lambda.clear();
   Z0.clear();
   nPoints.clear();
   fCutResults.clear();
}

#endif
