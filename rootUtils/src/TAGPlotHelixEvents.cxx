
#ifdef BUILD_AG

#include "TAGPlotHelixEvents.h"

ClassImp(TAGPlotHelixEvents)

TAGPlotHelixEvents::TAGPlotHelixEvents()
{

}

TAGPlotHelixEvents::~TAGPlotHelixEvents()
{

}
// Coipy ctor
TAGPlotHelixEvents::TAGPlotHelixEvents(const TAGPlotHelixEvents &h)
   : TObject(h), fRunNumber(h.fRunNumber), fTime(h.fTime), fOfficialTime(h.fOfficialTime), pT(h.pT), pZ(h.pZ),
     pTot(h.pTot), parD(h.parD), Curvature(h.Curvature), nPoints(h.nPoints), fCutResults(h.fCutResults)
{
}


TAGPlotHelixEvents &TAGPlotHelixEvents::operator=(const TAGPlotHelixEvents &h)
{
   fRunNumber = h.fRunNumber;
   fTime = h.fTime;
   fOfficialTime = h.fOfficialTime;
   pT = h.pT;
   pZ = h.pZ;
   pTot = h.pTot;
   parD = h.parD;
   Curvature = h.Curvature;
   nPoints = h.nPoints;
   fCutResults.assign(h.fCutResults.begin(),h.fCutResults.end());
   return *this;
}

TAGPlotHelixEvents &TAGPlotHelixEvents::operator+=(const TAGPlotHelixEvents &h)
{
   fRunNumber.insert(fRunNumber.end(), h.fRunNumber.begin(), h.fRunNumber.end());
   fTime.insert(fTime.end(), h.fTime.begin(), h.fTime.end());
   fOfficialTime.insert(fOfficialTime.end(), h.fOfficialTime.begin(), h.fOfficialTime.end());
   pT.insert(pT.end(), h.pT.begin(), h.pT.end());
   pZ.insert(pZ.end(), h.pZ.begin(), h.pZ.end());
   pTot.insert(pTot.end(), h.pTot.begin(), h.pTot.end());
   parD.insert(parD.end(), h.parD.begin(), h.parD.end());
   Curvature.insert(Curvature.end(), h.Curvature.begin(), h.Curvature.end());
   nPoints.insert(nPoints.end(), h.nPoints.begin(), h.nPoints.end());
   fCutResults.insert(fCutResults.end(), h.fCutResults.begin(), h.fCutResults.end() );
   return *this;
}

void TAGPlotHelixEvents::AddEvent(int runNumber, double time, double officialTime, double pt, double pz, double ptot,
                                  double pard, double curvature, int npoints, const std::vector<bool>& cuts)
{
   fRunNumber.push_back(runNumber);
   fTime.push_back(time);
   fOfficialTime.push_back(officialTime);
   pT.push_back(pt);
   pZ.push_back(pz);
   pTot.push_back(ptot);
   parD.push_back(pard);
   Curvature.push_back(curvature);
   nPoints.push_back(npoints);
   fCutResults.emplace_back();
   fCutResults.back().push_back(cuts);
}

void TAGPlotHelixEvents::clear()
{
   fRunNumber.clear();
   fTime.clear();
   fOfficialTime.clear();
   pT.clear();
   pZ.clear();
   pTot.clear();
   parD.clear();
   Curvature.clear();
   nPoints.clear();
   fCutResults.clear();
}

#endif
