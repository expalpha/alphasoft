
#include "TAPlotVertexEventsPassedCuts.h"

// Thin wrapper for std::vector since Root's object stream can't handle std::vector<std::vector<X>>
ClassImp(TAPlotVertexEventsPassedCuts)

TAPlotVertexEventsPassedCuts::TAPlotVertexEventsPassedCuts()
{

}

TAPlotVertexEventsPassedCuts::~TAPlotVertexEventsPassedCuts()
{

}

std::string TAPlotVertexEventsPassedCuts::title() const
{
   std::string line = "";
   for (size_t i = 0; i < fCuts.size(); i++)
       line += std::string("CutsType") + std::to_string(i) + ",";
   return line;
}


std::string TAPlotVertexEventsPassedCuts::line() const
{
   std::string line = "";
   for (bool result : fCuts) line += std::to_string(result) + ", ";
   return line;
}

bool TAPlotVertexEventsPassedCuts::at(const size_t i) const
{
   if (i >= fCuts.size()) {
      std::cerr << "Cut number requested beyond class size... stale root file?" << std::endl;
      //exit(1);
      return false;
   }
   return fCuts.at(i);
}

void TAPlotVertexEventsPassedCuts::push_back(const bool &r)
{
   return fCuts.push_back(r);
}

void TAPlotVertexEventsPassedCuts::push_back(const std::vector<bool> &rr)
{
   for (const bool &r : rr) push_back(r);
}
