#include "DoubleGetters.h"
#include "TAGDetectorEvent.hh"
#ifdef BUILD_AG
Double_t GetTotalRunTimeFromChrono(Int_t runNumber, const TChronoChannel& chan)
{
   if (!chan.IsValidChannel())
      return -1;
   TTreeReaderPointer ChronoReader = Get_Chrono_TreeReader(runNumber, chan);
   if (!ChronoReader)
      return -1;
   TTreeReaderValue<TCbFIFOEvent> ChronoEvent(*ChronoReader, "FIFOData");
   if (!ChronoReader->GetEntries())
      return -1;
   ChronoReader->SetEntry(ChronoReader->GetEntries() - 1);
   const double t = ChronoEvent->GetRunTime();

   return t;
}

Double_t GetTotalRunTimeFromChrono(Int_t runNumber, const std::string& Board)
{
      Double_t chan_max = -99;
      for (Int_t i=0; i<CHRONO_N_CHANNELS; i++) {
         const TChronoChannel& chan = Get_Chrono_Channel_By_Channel_Number(Board,i);
         const double temp = GetTotalRunTimeFromChrono(runNumber, chan);
         if (temp > chan_max )
            chan_max = temp;
      }
      std::cout << "End time from CB " << Board << ":" << chan_max << std::endl;
      return chan_max;

}


Double_t GetTotalRunTimeFromChrono(Int_t runNumber)
{
   Double_t max = -99;
   for (const std::pair<std::string, int> board: TChronoChannel::CBMAP)
   {
      const double temp = GetTotalRunTimeFromChrono(runNumber, board.first);
      if (temp > max )
         max = temp;
   }
   return max;
}

#endif
#ifdef BUILD_AG
Double_t GetTotalRunTimeFromTPC(Int_t runNumber)
{
   TTreeReaderPointer TPCTreeReader = Get_AGDetectorEvent_TreeReader(runNumber);
   if (!TPCTreeReader)
      return 0.;
   TTreeReaderValue<TAGDetectorEvent> VertexEvent(*TPCTreeReader, "TADetectorEvent");
   double RunTime = -1;
   if(TPCTreeReader->Next() )
   {
      TPCTreeReader->SetEntry(TPCTreeReader->GetEntries() - 1);
      RunTime = VertexEvent->fRunTime;
   }
   //We may want to choose to only use official time once its well calibrated
   std::cout<<"End time from TPC (official time):"<<RunTime<<std::endl;
   return RunTime;
}
#endif
#ifdef BUILD_AG
Double_t GetAGTotalRunTime(Int_t runNumber)
{
   return GetTotalRunTimeFromChrono(runNumber);
}
#endif

#if BUILD_A2
Double_t GetTotalRunTimeFromSIS(Int_t runNumber)
{
   double t = -1;
   for (int sis_module_no = 0; sis_module_no < NUM_SIS_MODULES; sis_module_no++)
   {
      TTreeReaderPointer SISReader = A2_SIS_Tree_Reader(runNumber, sis_module_no);
      if (!SISReader->GetTree())
         continue;
      TTreeReaderValue<TSISEvent> SISEvent(*SISReader, "TSISEvent");
      SISReader->SetEntry(SISReader->GetEntries(false) - 1 );
      std::cout <<"mod: " << sis_module_no << "\t" <<SISReader->GetEntries(true) -1 <<std::endl;
      std::cout << SISEvent->GetRunTime() << std::endl;
      if (SISEvent->GetRunTime() > t)
         t = SISEvent->GetRunTime();
   }
   return t;   
}
Double_t GetTotalRunTimeFromSVD(Int_t runNumber)
{
      //More performance is maybe available if we use DataFrames...
   TTreeReaderPointer SVDReader = Get_A2_SVD_Tree(runNumber);
   if (!SVDReader->GetTree())
      return -1.;
   TTreeReaderValue<TSVD_QOD> SVDEvent(*SVDReader, "OfficialTime");
   SVDReader->SetEntry(SVDReader->GetEntries(false) -1 );
   double time = SVDEvent->GetTimeOfEvent();
   return time;
}
Double_t GetA2TotalRunTime(Int_t runNumber)
{
   double SISTime = GetTotalRunTimeFromSIS(runNumber);
   double SVDTime = GetTotalRunTimeFromSVD(runNumber);
   if (SISTime > SVDTime)
      return SISTime;
   else
      return SVDTime;
}


#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
