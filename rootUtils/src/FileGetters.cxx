#include "FileGetters.h"
#include "TreeGetters.h"
#include "AnalysisReportGetters.h"
#include "GitInfo.build_time.h"
#include "RootUtilGlobals.h"
#include "BinaryRunners.h"
#include "TStringGetters.h"
#include <stdexcept>
#include <sstream>

TFilePointer Get_File(const Int_t run_number, const Bool_t die)
{

  // Turn off root file buffering
  // By default, all histograms will get added to the current directory
  // This is undesired behavior, since we might want to open file A,
  // create a histogram, close file A and open file B.
  //
  // If this is set to true (root default), then we will get a segfault when we try to write to the histogram in file B.
  TH1::AddDirectory(false);

  TFile* f = NULL;
#ifdef ALPHASOFT_DB_INSTALL_PATH
   // Local data path unknown... use current path... then AGOUTPUT to find files
   char    buf[200];
   char   *status = getcwd(buf, 200);
   TString file_name;
   if (!status) file_name = buf;
      // get_current_dir_name is not supported in MacOS
      // TString file_name(get_current_dir_name());
#else
   TString file_name(getenv("AGRELEASE"));
   if (file_name.Length() < 10) {
      std::cout << "$AGRELEASE not set... please source agconfig.sh" << std::endl;
      exit(0123);
   }
#endif

   // Get list of possible local root files
   std::vector<std::string> possible_file_names;
   char root_filename[200];
   snprintf(root_filename,200,rootUtils::GetFileNamePattern().c_str(),run_number);
   for (std::string& basepath: rootUtils::GetListOfPossibleROOTFilePaths()) {
      std::string fname = basepath;
      if (fname.back() != '/')
         fname+="/";
      fname += root_filename;
      possible_file_names.emplace_back(fname);
   }

   // Open the file locally
   for (std::string& filename: possible_file_names) {
      f = TFile::Open(filename.c_str());
      if (f != NULL) break;
   }

   if (f == NULL) {
      if (die) {
         Error("Get_File", "\033[33mCould not open tree file for run %d\033[00m", run_number);
         std::stringstream msg;
         msg << "File NOT FOUND";
         throw std::runtime_error(msg.str());
      } else if (rootUtils::AutoRunAnalyzer()) {
         RunAnalyzer(run_number);
         // analysis should have worked... die next time we call Get_File if it hasn't
         return Get_File(run_number, true);
      } else {
         return TFilePointer(nullptr);
      }
   }

   if (!rootUtils::AllowGetAnalysisReport())
   {
      std::shared_ptr<TFile> f_ptr(f);
      return f_ptr;
   }
   // Prevent recusion when checking the analysis report
   // Ie, check version -> open file -> check version -> open file -> ...
   rootUtils::DisableAnalysisReportCheck();
   // Check if the file was generated with the current build
   std::string git_hash = "";
#if BUILD_A2
   if (!git_hash.empty())
      git_hash = Get_A2_Analysis_Report_Git_Hash(run_number);
#endif
#if BUILD_AG
   if (!git_hash.empty()) 
      git_hash = Get_AG_Analysis_Report_Git_Hash(run_number);
#endif
   if (!git_hash.empty()) {
      int commits_behind = GitInfo::GetCommitsBehind(git_hash);
      if (commits_behind > 0)
         std::cerr << "Warning: " << file_name << " was generated with " << git_hash << " (" << commits_behind
                   << " commits behind current build)\n";
      if (commits_behind > rootUtils::PermittedCommitsBehind() && rootUtils::AutoRunAnalyzer())
      {
         f->Close();
         RunAnalyzer(run_number);
         // Re-enable analysis report check
         rootUtils::AllowGetAnalysisReport();
         return Get_File(run_number,true);
      }
   }
   // Re-enable analysis report check
   rootUtils::AllowGetAnalysisReport();
   TFilePointer f_ptr(f);
   return f_ptr;
}

bool Check_File(const Int_t run_number)
{

  TFile* f = NULL;
#ifdef ALPHASOFT_DB_INSTALL_PATH
   // Local data path unknown... use current path... then AGOUTPUT to find files
   char    buf[200];
   char   *status = getcwd(buf, 200);
   TString file_name;
   if (!status) file_name = buf;
      // get_current_dir_name is not supported in MacOS
      // TString file_name(get_current_dir_name());
#else
   TString file_name(getenv("AGRELEASE"));
   if (file_name.Length() < 10) {
      std::cout << "$AGRELEASE not set... please source agconfig.sh" << std::endl;
      exit(0123);
   }
#endif

   // Get list of possible local root files
   std::vector<std::string> possible_file_names;
   char root_filename[200];
   snprintf(root_filename,200,rootUtils::GetFileNamePattern().c_str(),run_number);
   for (std::string& basepath: rootUtils::GetListOfPossibleROOTFilePaths()) {
      std::string fname = basepath;
      if (fname.back() != '/')
         fname+="/";
      fname += root_filename;
      possible_file_names.emplace_back(fname);
   }

   // Open the file locally
   for (std::string& filename: possible_file_names) {
      f = TFile::Open(filename.c_str());
      if (f != NULL) {
         f->Close();
         return true;
      }
   }

   return false;

}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
