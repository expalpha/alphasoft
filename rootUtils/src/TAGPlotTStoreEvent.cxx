#include "TAGPlotTStoreEvent.h"

#if BUILD_AG
ClassImp(TAGPlotTStoreEvent)

TAGPlotTStoreEvent::TAGPlotTStoreEvent(): TAGPlot()
{
}

TAGPlotTStoreEvent::~TAGPlotTStoreEvent()
{
}

void TAGPlotTStoreEvent::LoadRun(const int runNumber, const double firstTime, const double lastTime, int verbose)
{
   TTreeReaderPointer TPCTreeReader = Get_StoreEvent_TreeReader(runNumber);
   TTreeReaderValue<TStoreEvent> VertexEvent(*TPCTreeReader, "StoredEvent");
   while (TPCTreeReader->Next())
   {
      const double t =VertexEvent->GetTimeOfEvent();
      if (t < firstTime)
         continue;
      if (t > lastTime)
         break;
      AddAGDetectorEvent(TAGDetectorEvent(&(*VertexEvent),VertexEvent->GetBarEvent()));
   }
   LoadChronoEvents(runNumber,firstTime,lastTime,verbose);
   return;
}

#endif