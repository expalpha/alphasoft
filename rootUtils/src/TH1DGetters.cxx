#include "TH1DGetters.h"
#include "ChronoReader.h"
#include <set>

#ifdef BUILD_AG
std::vector<TH1D*> Get_Summed_Chrono(Int_t runNumber, std::vector<TChronoChannel> chrono_chan, std::vector<double> tmin, std::vector<double> tmax, double range )
{
   assert(tmin.size()==tmax.size());
   double first_time = 1E99;
   double last_time = 0;

   const size_t n_times = tmin.size();

   //If range is not set, calcualte it
   if (range<0)
   {
      for (size_t i = 0; i < n_times; i++)
      {
         double diff=tmax[i]-tmin[i];
         if (range < diff)
            range = diff;
      }
   }

   for (auto& t: tmin)
   {
      if (t < first_time)
         first_time = t;
   }
   for (auto& t: tmax)
   {
      //Replace negative tmax times with the end of run...
      if (t < 0) t = 1E99; //FIXME: Get total run time!
      //Find the latest tmax time
      if (last_time < t )
         last_time = t;
   }

   int n_chans=chrono_chan.size();
   std::vector<TH1D*> hh;

   for (int i=0; i<n_chans; i++)
   {
      TString Title="R";
      Title+=runNumber;
      Title+=" Chrono ";
      Title+=chrono_chan[i].GetBoard();
      Title+=" Channel:";
      Title+=chrono_chan[i].GetChannel();
      Title+=" - ";
      Title+=Get_Chrono_Name(runNumber, chrono_chan[i]);

      //Replace this is a data base call to get the channel name
      TString name = "R";
      name += runNumber;
      name += "_";
      name += Get_Chrono_Name(runNumber, chrono_chan[i]);
      name += "_";
      name += i;
      //Title+=name.Data();

      switch (rootUtils::GetTimeAxisMode()) {
         case rootUtils::kFixedTimeBinNumber:
            hh.push_back(new TH1D( name.Data(),
                           Title.Data(),
                           rootUtils::GetTimeBinNumber(),0.,range ));
            break;
         case rootUtils::kFixedTimeBinSize:
            hh.push_back(new TH1D( name.Data(),
                           Title.Data(),
                           rootUtils::CalculateNumberOfBinsInRange(0,range),0.,range ));
            break;
         default:
            std::cerr <<"Unknown TimeAxisMode from rootUtils\n";
            break;
      }
   }

   std::set<TChronoChannel> channels;
   for (TChronoChannel& chan: chrono_chan)
      channels.insert(chan);

   //TTreeReaders are buffered... so this is faster than iterating over a TTree by hand
   //More performance is maybe available if we use DataFrames...
   for (TChronoChannel& chan: chrono_chan)
   {
      TTreeReaderPointer  CBTreeReader = Get_Chrono_TreeReader(runNumber, chan);
      if (!CBTreeReader)
         continue;
      TTreeReaderValue<TCbFIFOEvent> e(*CBTreeReader,"FIFOData");
      while (CBTreeReader->Next())
      {
         const double t = e->GetRunTime();
         if (t < first_time) continue;
         if (t > last_time) continue;
         //Loop over all time windows
         for (size_t j = 0; j < n_times; j++)
         {
            if (t > tmin[j] && t< tmax[j])
            {
               //const int counts = e->GetCounts();
               if (e->IsLeadingEdge())
               {
                  //std::cout<<t<<"\t"<<tmin[j]<<"\t"<<t-tmin[j]<<std::endl;
                  for (int i=0; i<n_chans; i++)
                  {
                     if (chrono_chan[i].GetChannel() == (int)e->GetChannel())
                        if (chrono_chan[i].GetBoard() == chan.GetBoard())
                           hh[i]->Fill(t - tmin[j], 1);
                  }
               }
               //This event has been written to the array... so I dont need
               //to check the other winodws... break! Move to next SISEvent
               continue;
            }
         }
      }
   }
   return hh;
}
#endif
#ifdef BUILD_AG
std::vector<TH1D*> Get_Summed_Chrono(Int_t runNumber, std::vector<TChronoChannel> chrono_chans, std::vector<TAGSpill> spills)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   for (auto & spill: spills)
   {
      if (spill.fScalerData)
      {
         tmin.push_back(spill.fScalerData->fStartTime);
         tmax.push_back(spill.fScalerData->fStopTime);
      }
      else
      {
         std::cout<<"Spill didn't have Scaler data!? Was there an aborted sequence?"<<std::endl;
      }
   }
   return Get_Summed_Chrono(runNumber,  chrono_chans, tmin, tmax );
}
#endif

#ifdef BUILD_AG
std::vector<TH1D*> Get_Summed_Chrono(Int_t runNumber, std::vector<TChronoChannel> chrono_chans, std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TAGSpill> spills=Get_AG_Spills(runNumber, description, dumpIndex);
   return Get_Summed_Chrono( runNumber, chrono_chans, spills);
}
#endif

#ifdef BUILD_AG
std::vector<TH1D*> Get_Summed_Chrono(Int_t runNumber, std::vector<std::string> Chrono_Channel_Names, std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TChronoChannel> chrono_chans;
   for (std::string name: Chrono_Channel_Names) {
      chrono_chans.push_back(Get_Chrono_Channel(runNumber,name.data()));
   }
   std::vector<TAGSpill> spills=Get_AG_Spills(runNumber, description, dumpIndex);
   return Get_Summed_Chrono( runNumber, chrono_chans, spills);
}
#endif


#ifdef BUILD_AG
std::vector<std::vector<TH1D*>> Get_Chrono(Int_t runNumber, std::vector<TChronoChannel> chrono_chan, std::vector<double> tmin, std::vector<double> tmax, std::vector<std::string> time_window_labels, double range)
{
   if (time_window_labels.empty())
   {
      for (size_t i = 0; i < tmin.size(); i++)
      {
         time_window_labels.emplace_back(
            std::to_string(tmin.at(i)) +
            " - " +
            std::to_string(tmax.at(i))
         );
      }
   }

   assert(tmin.size()==tmax.size());

   for (auto& t: tmax)
   {
      //Replace negative tmax times with the end of run...
      if (t < 0) t = GetAGTotalRunTime(runNumber);
   }

   double first_time = *std::min_element( std::begin(tmin), std::end(tmin));
   double last_time = *std::max_element( std::begin(tmax), std::end(tmax));

   const int n_chans=chrono_chan.size();
   const size_t n_times = tmin.size();

   std::vector<std::vector<TH1D*>> hh;
   for (int i=0; i<n_chans; i++)
   {
      std::vector<TH1D*> times;
      for (size_t j = 0; j < n_times; j++)
      {
         if (!chrono_chan[i].IsValidChannel())
            break;
         TString Title="R";
         Title+=runNumber;
         Title+=" Chrono ";
         Title+=chrono_chan[i].GetBoard();
         Title+=" Channel:";
         Title+=chrono_chan[i].GetChannel();
         Title+=" - ";
         Title+=Get_Chrono_Name(runNumber, chrono_chan[i]);
         Title+=" (";
         Title+=time_window_labels.at(j);
         Title+=")";

         //Replace this is a data base call to get the channel name
         TString name = "R";
         name += runNumber;
         name += "_";
         name += Get_Chrono_Name(runNumber, chrono_chan[i]);
         name += "_";
         name += i;
         name += "_";
         name += j;
         //Title+=name.Data();

         //If range is not set, calculate it
         double thisRange = range;
         if (thisRange<0)
         {
            thisRange = tmax[j] - tmin[j];
         }
         switch (rootUtils::GetTimeAxisMode()) {
            case rootUtils::kFixedTimeBinNumber:
             times.push_back(new TH1D( name.Data(),
                              Title.Data(),
                              rootUtils::GetTimeBinNumber(),0.,thisRange ));
               break;
            case rootUtils::kFixedTimeBinSize:
               times.push_back(new TH1D( name.Data(),
                              Title.Data(),
                              rootUtils::CalculateNumberOfBinsInRange(0,thisRange),0.,thisRange ));
               break;
            default:
               std::cerr <<"Unknown TimeAxisMode from rootUtils\n";
               break;
         }
      }
      hh.push_back(times);
   }

   //TTreeReaders are buffered... so this is faster than iterating over a TTree by hand
   //More performance is maybe available if we use DataFrames...
   for (size_t i = 0; i < chrono_chan.size(); i++)
   {
      std::cout << __FILE__ << ":" << __LINE__ << std::endl;
      if (!chrono_chan[i].IsValidChannel())
         continue;
      TTreeReaderPointer ChronoReader = Get_Chrono_TreeReader(runNumber, chrono_chan[i]);
      if (!ChronoReader) continue;
      TTreeReaderValue<TCbFIFOEvent> ChronoEvent(*ChronoReader, "FIFOData");
      ChronoReader = FindStartOfTree(ChronoReader,ChronoEvent,0,ChronoReader->GetEntries(),first_time,1.0);
      while (ChronoReader->Next())
      {
         const double t = ChronoEvent->GetRunTime();
         if (t < first_time) continue;
         if (t > last_time) break;
         //Loop over all time windows
         for (size_t j = 0; j < n_times; j++)
         {
            //Increase efficiency by breaking this look when we are outside of range
            if (t > tmin[j] && t < tmax[j])
            {
               //const int counts=e->GetCounts();
               if (ChronoEvent->IsLeadingEdge())
               {
                  //std::cout<<official_time<<"\t"<<tmin[j]<<"\t"<<official_time - tmin[j] <<"\t" << e->GetRunTime()<<std::endl;
                  hh.at(i).at(j)->Fill(t - tmin[j], ChronoEvent->fCounts);
               }
            }
         }
      }
      //delete e;
   }
   return hh;
}
#endif
#ifdef BUILD_AG
std::vector<std::vector<TH1D*>> Get_Chrono(Int_t runNumber,std::vector<TChronoChannel> channels,std::vector<TAGSpill> spills)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   std::vector<std::string> labels;
   for (const TAGSpill& s: spills)
   {
      tmin.push_back( s.GetStartTime() );
      tmax.push_back( s.GetStopTime() );
      labels.push_back( s.fName );
   }
   return Get_Chrono(runNumber,channels,tmin,tmax, labels);
}
#endif

#ifdef BUILD_AG
std::vector<std::vector<TH1D*>> Get_Chrono(Int_t runNumber,std::vector<TChronoChannel> channels, std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TAGSpill> spills = Get_AG_Spills(runNumber, description, dumpIndex);
   return Get_Chrono(runNumber, channels, spills);
}

#endif


#ifdef BUILD_AG
TH1D* Get_Delta_Chrono(Int_t runNumber, TChronoChannel chan, Double_t tmin, Double_t tmax, Double_t PlotMin, Double_t PlotMax)
{
   if (tmax<0.) tmax=GetAGTotalRunTime(runNumber);
   TTreePointer t = Get_Chrono_Tree(runNumber,chan);
   TCbFIFOEvent* e=new TCbFIFOEvent();
   TString name=Get_Chrono_Name(runNumber,chan);
   TString Title="Chrono Time between Events - Board:";
   Title+=chan.GetBoard();
   Title+=" Channel:";
   Title+=chan.GetChannel();
   Title+=" - ";
   Title+=name.Data();
   double range = PlotMax-PlotMin;
   TH1D* hh = nullptr;
   switch (rootUtils::GetTimeAxisMode()) {
      case rootUtils::kFixedTimeBinNumber:
         hh = new TH1D(name.Data(),
                        Title.Data(),
                        rootUtils::GetTimeBinNumber(), PlotMin, PlotMin + range);
         break;
      case rootUtils::kFixedTimeBinSize:
         hh = new TH1D(
            name.Data(),
            Title.Data(),
            rootUtils::CalculateNumberOfBinsInRange(PlotMin, PlotMin + range),
            PlotMin, PlotMin + range);
         break;
      default:
         std::cerr <<"Unknown TimeAxisMode from rootUtils\n";
         break;
   }

   t->SetBranchAddress("FIFOData", &e);
   t->GetEntry(0);
   double last_time = e->GetRunTime();
   for (Int_t i = 1; i < t->GetEntries(); ++i)
   {
      t->GetEntry(i);
      if (!(*e == chan)) continue;
      if (e->GetRunTime() < tmin) continue;
      if (e->GetRunTime() > tmax) continue;
      if (e->IsLeadingEdge())
         hh->Fill(e->GetRunTime() - last_time,1);

      last_time = e->GetRunTime();
   }
   return hh;
}
#endif
#ifdef BUILD_AG
TH1D* Get_Delta_Chrono(Int_t runNumber, TChronoChannel chan, const char* description, Int_t repetition)
{
   std::vector<TAGSpill> spill = Get_AG_Spills(runNumber,{description},{repetition});
   Double_t tmin = spill.front().GetStartTime();
   Double_t tmax = spill.front().GetStopTime();
   return Get_Delta_Chrono( runNumber, chan, tmin, tmax);
}
#endif
#ifdef BUILD_AG
TH1D* Get_Delta_Chrono(Int_t runNumber, const char* ChannelName, Double_t tmin, Double_t tmax)
{
   TChronoChannel chan = Get_Chrono_Channel(runNumber, ChannelName);
   if (chan.IsValidChannel())
      return Get_Delta_Chrono(runNumber,chan, tmin, tmax);
   return NULL;
}
#endif
#ifdef BUILD_AG
TH1D* Get_Delta_Chrono(Int_t runNumber,const char* ChannelName, const char* description, Int_t repetition)
{
   std::vector<TAGSpill> spill = Get_AG_Spills(runNumber,{description},{repetition});
   Double_t tmin = spill.front().GetStartTime();
   Double_t tmax = spill.front().GetStopTime();
   return Get_Delta_Chrono(runNumber, ChannelName, tmin, tmax);
}
#endif


#ifdef BUILD_A2
std::vector<TH1D*> Get_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<double> tmin, std::vector<double> tmax, double range )
{
   assert(tmin.size()==tmax.size());
   double first_time = *std::min_element(tmin.begin(), tmin.end());
   double last_time;
   if ( *std::min_element(tmax.begin(), tmax.end()) < 0)
      last_time = GetA2TotalRunTime(runNumber);
   else
      last_time = *std::max_element(tmax.begin(), tmax.end());

   int n_times=tmin.size();

   //If range is not set, calcualte it
   if (range<0)
   {
      for (int i=0; i<n_times; i++)
      {
         double diff=tmax[i]-tmin[i];
         if (range<diff) range=diff;
      }
   }


   int n_chans=SIS_Channel.size();
   std::vector<TH1D*> hh;

   TSISChannels chans(runNumber);

   for (int i=0; i<n_chans; i++)
   {
      TString Title="R";
      Title+=runNumber;
      Title+=" SIS Channel:";
      Title+=SIS_Channel[i];
      Title+=" - ";
      Title+=chans.GetDescription(SIS_Channel[i], runNumber);

      //Replace this is a data base call to get the channel name
      TString name = chans.GetDescription(SIS_Channel[i], runNumber);
      //Title+=name.Data();
      switch (rootUtils::GetTimeAxisMode()) {
         case rootUtils::kFixedTimeBinNumber:
            hh.push_back(new TH1D( name.Data(),
                           Title.Data(),
                           rootUtils::GetTimeBinNumber(),0.,range ));
            break;
         case rootUtils::kFixedTimeBinSize:
            hh.push_back(new TH1D( name.Data(),
                           Title.Data(),
                           rootUtils::CalculateNumberOfBinsInRange(0.,range),0.,range ));
            break;
         default:
            std::cerr <<"Unknown TimeAxisMode from rootUtils\n";
            break;
      }
   }

   //TTreeReaders are buffered... so this is faster than iterating over a TTree by hand
   //More performance is maybe available if we use DataFrames...
   for (int sis_module_no = 0; sis_module_no < NUM_SIS_MODULES; sis_module_no++)
   {
      TTreeReaderPointer  SISReader = A2_SIS_Tree_Reader(runNumber, sis_module_no);
      TTreeReaderValue<TSISEvent> SISEvent(*SISReader, "TSISEvent");
      // Binary search for the start of the data
      SISReader = FindStartOfTree(SISReader,SISEvent,0,SISReader->GetEntries(),first_time,10.0);
      // I assume that file IO is the slowest part of this function...
      // so get multiple channels and multiple time windows in one pass
      while (SISReader->Next())
      {
         double t = SISEvent->GetRunTime();
         if (t < first_time) continue;
         if (t > last_time) break;

         //Loop over all time windows
         for (int j = 0; j < n_times; j++)
         {
            if (t>tmin[j] && t< tmax[j])
            {
               for (int i = 0; i < n_chans; i++)
               {
                  int counts = SISEvent->GetCountsInChannel(SIS_Channel[i]);
                  if (counts)
                  {
                     //std::cout<<t<<"\t"<<tmin[j]<<"\t"<<t-tmin[j]<<std::endl;
                     hh[i]->Fill(t-tmin[j],counts);
                  }
               }
               //This event has been written to the array... so I dont need
               //to check the other winodws... break! Move to next SISEvent
               break;
            }
         }
      }
   }
   return hh;
}
#endif
#ifdef BUILD_A2
std::vector<TH1D*> Get_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<TA2Spill> spills)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   for (auto & spill: spills)
   {
      if (spill.fScalerData)
      {
         tmin.push_back(spill.fScalerData->fStartTime);
         tmax.push_back(spill.fScalerData->fStopTime);
      }
      else
      {
         std::cout<<"Spill didn't have Scaler data!? Was there an aborted sequence?"<<std::endl;
      }
   }
   return Get_Summed_SIS(runNumber,  SIS_Channel, tmin, tmax );
}
#endif
#ifdef BUILD_A2
std::vector<TH1D*> Get_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TA2Spill> spills=Get_A2_Spills(runNumber, description, dumpIndex);
   return Get_Summed_SIS( runNumber, SIS_Channel, spills);
}
#endif

#ifdef BUILD_A2
std::vector<std::vector<TH1D*>> Get_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<double> tmin, std::vector<double> tmax )
{
   assert(tmin.size()==tmax.size());
   double first_time = *std::min_element(tmin.begin(), tmin.end());
   double last_time;
   if ( *std::min_element(tmax.begin(), tmax.end()) < 0)
      last_time = GetA2TotalRunTime(runNumber);
   else
      last_time = *std::max_element(tmax.begin(), tmax.end());

   int n_times=tmin.size();

   //std::cout<< first_time << "\t" << last_time <<std::endl;

   int n_chans=SIS_Channel.size();

   std::vector<std::vector<TH1D*>> hh;

   TSISChannels chans(runNumber);

   for (int i=0; i<n_chans; i++)
   {
      std::vector<TH1D*> times;
      for (size_t j=0; j<tmax.size(); j++)
      {
         TString Title="R";
         Title+=runNumber;
         Title+=" SIS Channel:";
         Title+=SIS_Channel[i];
         Title+=" - ";
         Title+=chans.GetDescription(SIS_Channel[i], runNumber);

         //Replace this is a data base call to get the channel name
         TString name=chans.GetDescription(SIS_Channel[i], runNumber);
         name += "_";
         name += j;
         //Title+=name.Data();
         switch (rootUtils::GetTimeAxisMode()) {
            case rootUtils::kFixedTimeBinNumber:
               times.push_back(
                  new TH1D(
                     name.Data(),
                     Title.Data(),
                     rootUtils::GetTimeBinNumber(),0.,tmax.at(j) - tmin.at(j) ));
               break;
            case rootUtils::kFixedTimeBinSize:
               times.push_back(
                  new TH1D(
                     name.Data(),
                     Title.Data(),
                     rootUtils::CalculateNumberOfBinsInRange(0.,tmax.at(j) - tmin.at(j)),
                     0.,tmax.at(j) - tmin.at(j) ));
               break;
            default:
               std::cerr <<"Unknown TimeAxisMode from rootUtils\n";
               break;
            }
      }
      hh.push_back(times);
   }
   //TTreeReaders are buffered... so this is faster than iterating over a TTree by hand
   //More performance is maybe available if we use DataFrames...
   for (int sis_module_no = 0; sis_module_no < NUM_SIS_MODULES; sis_module_no++)
   {
      TTreeReaderPointer SISReader = A2_SIS_Tree_Reader(runNumber, sis_module_no);
      TTreeReaderValue<TSISEvent> SISEvent(*SISReader, "TSISEvent");

      // Binary search for the start of the data
      SISReader = FindStartOfTree(SISReader,SISEvent,0,SISReader->GetEntries(),first_time,10.0);
      
      // I assume that file IO is the slowest part of this function...
      // so get multiple channels and multiple time windows in one pass
      while (SISReader->Next())
      {
         double t = SISEvent->GetRunTime();
         if (t < first_time) continue;
         if (t > last_time) break;

         //Loop over all time windows
         for (int j = 0; j < n_times; j++)
         {
            if (t > tmin[j] && t < tmax[j])
            {
               for (int i = 0; i < n_chans; i++)
               {
                  const int counts = SISEvent->GetCountsInChannel(SIS_Channel[i]);
                  if (counts)
                  {
                     hh.at(i).at(j)->Fill(t-tmin[j],counts);
                  }
               }
               //This event has been written to the array... so I dont need
               //to check the other winodws... break! Move to next SISEvent
               //break;
            }
         }
      }
   }
   return hh;
}
#endif
#ifdef BUILD_A2
std::vector<std::vector<TH1D*>> Get_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<TA2Spill> spills)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   for (auto & spill: spills)
   {
      if (spill.fScalerData)
      {
         tmin.push_back(spill.fScalerData->fStartTime);
         tmax.push_back(spill.fScalerData->fStopTime);
      }
      else
      {
         std::cout<<"Spill didn't have Scaler data!? Was there an aborted sequence?"<<std::endl;
      }
   }
   return Get_SIS(runNumber,  SIS_Channel, tmin, tmax );
}
#endif
#ifdef BUILD_A2
std::vector<std::vector<TH1D*>> Get_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TA2Spill> spills=Get_A2_Spills(runNumber, description, dumpIndex);
   return Get_SIS( runNumber, SIS_Channel, spills);
}
#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
