#if BUILD_A2
#include "TA2PlotSISPlotEvents.h"

/// @brief Constructor
TA2PlotSISPlotEvents::TA2PlotSISPlotEvents()
{

}

/// @brief Deconstructor
TA2PlotSISPlotEvents::~TA2PlotSISPlotEvents()
{

}

/// @brief Copy constructor
/// @param sisPlotEvents 
TA2PlotSISPlotEvents::TA2PlotSISPlotEvents(const TA2PlotSISPlotEvents &sisPlotEvents)
   : TAPlotScalerEvents(sisPlotEvents)
{
   // Deep copy vectors.
   for (size_t i = 0; i < sisPlotEvents.fTime.size(); i++) {
      fSISChannel.push_back(sisPlotEvents.fSISChannel[i]);
   }
}

/// @brief += operator
/// @param rhs 
/// @return 
TA2PlotSISPlotEvents TA2PlotSISPlotEvents::operator+=(const TA2PlotSISPlotEvents &rhs)
{
   // std::cout << "TA2PlotSISPlotEvents += operator" << std::endl;
   this->fRunNumber.insert(this->fRunNumber.end(), rhs.fRunNumber.begin(), rhs.fRunNumber.end());
   this->fTime.insert(this->fTime.end(), rhs.fTime.begin(), rhs.fTime.end());
   this->fOfficialTime.insert(this->fOfficialTime.end(), rhs.fOfficialTime.begin(), rhs.fOfficialTime.end());
   this->fCounts.insert(this->fCounts.end(), rhs.fCounts.begin(), rhs.fCounts.end());
   this->fSISChannel.insert(this->fSISChannel.end(), rhs.fSISChannel.begin(), rhs.fSISChannel.end());
   return *this;
}

/// @brief = operator
/// @param sisPlotEvents 
/// @return 
TA2PlotSISPlotEvents &TA2PlotSISPlotEvents::operator=(const TA2PlotSISPlotEvents &sisPlotEvents)
{
   for (size_t i = 0; i < sisPlotEvents.fTime.size(); i++) {
      this->fRunNumber.emplace_back(sisPlotEvents.fRunNumber[i]);
      this->fTime.emplace_back(sisPlotEvents.fTime[i]);
      this->fOfficialTime.emplace_back(sisPlotEvents.fOfficialTime[i]);
      this->fCounts.emplace_back(sisPlotEvents.fCounts[i]);
      this->fSISChannel.emplace_back(sisPlotEvents.fSISChannel[i]);
   }
   return *this;
}

/// @brief + operator for two TAPlotSISPlotEvents
/// @param lhs 
/// @param rhs 
/// @return 
TA2PlotSISPlotEvents operator+(const TA2PlotSISPlotEvents &lhs,
                                                            const TA2PlotSISPlotEvents &rhs)
{
   // std::cout << "TA2PlotSISPlotEvents addition operator" << std::endl;
   TA2PlotSISPlotEvents outputplot(lhs); // Create new from copy
   // Vectors- need concacting
   outputplot.fRunNumber.insert(outputplot.fRunNumber.end(), rhs.fRunNumber.begin(), rhs.fRunNumber.end());
   outputplot.fTime.insert(outputplot.fTime.end(), rhs.fTime.begin(), rhs.fTime.end());
   outputplot.fOfficialTime.insert(outputplot.fOfficialTime.end(), rhs.fOfficialTime.begin(), rhs.fOfficialTime.end());
   outputplot.fCounts.insert(outputplot.fCounts.end(), rhs.fCounts.begin(), rhs.fCounts.end());
   outputplot.fSISChannel.insert(outputplot.fSISChannel.end(), rhs.fSISChannel.begin(), rhs.fSISChannel.end());
   return outputplot;
}

/// @brief Add single event to container
/// @param runNumber 
/// @param time 
/// @param officialTime 
/// @param counts 
/// @param channel 
void TA2PlotSISPlotEvents::AddEvent(const int runNumber, const double time, const double officialTime, const int counts,
                                    const TSISChannel &channel)
{
   TAPlotScalerEvents::AddEvent(runNumber, time, officialTime, counts);
   fSISChannel.emplace_back(channel);
}

/// @brief Generate names for columns in CSVLine(size_t i) function
/// @return 
std::string TA2PlotSISPlotEvents::CSVTitleLine() const
{
   return std::string("Run Number,") + "Plot Time (Time axis of TAPlot)," + "OfficialTime (run time)," +
          "SIS Channel, SIS Module," + "Counts\n";
}

/// @brief Generate a CSV line for event at index i
/// @param i 
/// @return 
std::string TA2PlotSISPlotEvents::CSVLine(size_t i) const
{
   // This is a little fugly
   std::string line;
   line = std::string("") + fRunNumber.at(i) + "," + fTime.at(i) + "," + fOfficialTime.at(i) + "," +
          fSISChannel.at(i).fChannel + "," + fSISChannel.at(i).fModule + "," + fCounts.at(i) + "\n";
   return line;
}

/// @brief Get the total count of events in specific TSISChannel
/// @param ch 
/// @return 
int TA2PlotSISPlotEvents::CountTotalCountsInChannel(const TSISChannel &ch) const
{
   int events = 0;
   for (size_t i = 0; i < fTime.size(); i++) {
      if (fSISChannel[i] == ch) events += fCounts[i];
   }
   return events;
}

#endif
