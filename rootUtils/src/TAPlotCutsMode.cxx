#include "TAPlotCutsMode.h"
#include <iostream>

void TAPlotCutsMode::Print()
{
   std::cout << "Cuts string: " << fCutsString <<std::endl;
   std::cout << "Cut logic:\n";
   for (auto AndStatement = fCutsMode.begin(); AndStatement != fCutsMode.end(); ++AndStatement)
   {
      for (auto cut = AndStatement->begin(); cut != AndStatement->end(); ++cut)
      {
         if (*cut < 0)
         {
            std::cout << "!" << -*cut - 1;
         }
         else
         {
            std::cout << *cut;
         }
         if (cut != AndStatement->end() - 1)
            std::cout << " AND ";
      }
      if (AndStatement != fCutsMode.end() - 1)
         std::cout << "\n\tOR\t\n";
      else
         std::cout <<"\n";
   }
   std::cout << std::endl;
}
#if BUILD_AG
std::string TAPlotCutsMode::FullCutName() const
{
   TAGDetectorEvent e;
   std::string name;
   // No special formatting needed for a single cut
   if (fCutsMode.size() == 1)
      if (fCutsMode.front().size() == 1)
         return e.GetOnlinePassCutsName( fCutsMode.front().front());
   for (auto AndStatement = fCutsMode.begin(); AndStatement != fCutsMode.end(); ++AndStatement)
   {
      for (auto cut = AndStatement->begin(); cut != AndStatement->end(); ++cut)
      {
         if (*cut < 0)
         {
            name += "!(" + e.GetOnlinePassCutsName(-*cut - 1) + ")";
         }
         else
         {
            name += "(" + e.GetOnlinePassCutsName(*cut) + ")";
         }
         if (cut != AndStatement->end() - 1)
            name += " AND ";
      }
      if (AndStatement != fCutsMode.end() - 1)
         name +=  "\n\tOR\t\n";
   }
   return name;
}
#endif

std::vector<std::string> TAPlotCutsMode::TokeniseByOR( const std::string& cuts) const 
{
   std::vector<std::string> ListOfCutSets;
   std::string temp;

   // Tokenise by '|'
   for (const char& c: cuts)
   {
      switch (c)
      {
         // Lock users out from using brackets
         case '(':
            std::cout << "Bracket logic unsupported..." <<std::endl;
            exit(1);
         case ')':
            std::cout << "Bracket logic unsupported..." <<std::endl;
            exit(1);
         // Ignore white space
         case ' ':
            break;
         case '\t':
            break;
         case '|':
            if (temp.size())
            {
               ListOfCutSets.emplace_back(temp);
               temp.clear();
            }
            break;
         default:
            temp.push_back(c);
      }
   }
   // Catch last entry
   if (temp.size())
      ListOfCutSets.emplace_back(temp);
   return ListOfCutSets;
}

std::vector<int> TAPlotCutsMode::DecodeCutString(const std::string& cutsString) const
{
   std::vector<int> ANDs;
   std::string number;
   // Tokenise by '&'
   for (const char& c: cutsString) {
      switch (c) {
         // Lock users out from using brackets
         case '(':
            std::cout << "Bracket logic unsupported..." <<std::endl;
            exit(1);
         case ')':
            std::cout << "Bracket logic unsupported..." <<std::endl;
            exit(1);
         // Ignore white space
         case ' ':
            break;
         case '\t':
            break;
         case '&':
            if (number.size())
            {
               if (number.front() == '!')
                  ANDs.emplace_back( - atoi(number.c_str() + 1) - 1);
               else
                  ANDs.emplace_back(atoi(number.c_str()));
               number.clear();
            }
            break;
         //Support 'anticuts'
         case '!':
            number.push_back(c);
            break;
         default:
            if (isdigit(c))
               number.push_back(c);
      }
   }

   // Catch last entry
   if (number.size())
   {
      if (number.front() == '!')
         ANDs.emplace_back( - atoi(number.c_str() + 1) - 1);
      else
         ANDs.emplace_back(atoi(number.c_str()));
   }
   return ANDs;
}

std::vector<std::vector<int>> TAPlotCutsMode::DecodeCutStrings(const std::vector<std::string> cuts) const
{
   std::vector<std::vector<int>> CutsLogic;
   for (const std::string& cut: cuts)
   {
      std::vector<int> ans = DecodeCutString(cut);
      if (ans.size())
         CutsLogic.push_back(ans);
   }
   return CutsLogic;
}
