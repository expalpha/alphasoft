#include "TreeGetters.h"

TTreePointer Get_Tree_By_Name(const int runNumber,const char* name)
{
   TFilePointer f = Get_File(runNumber);
   TTreePointer tree{f, name};
   if (!tree)
   {
      Error(name, "\033[31mTree %s for run number %d not found\033[00m", name, runNumber);
      //tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
   }
   return tree;
}

TTreeReaderPointer Get_TreeReader_By_Name(const int runNumber, const char* name)
{
   TFilePointer f = Get_File(runNumber);
   if (!f) return TTreeReaderPointer();
   TTreeReaderPointer tr{f,name};
   if(tr->IsZombie()){
      std::cerr << "Get_TreeReader_By_Name couldn't find tree '" << name << "' in file " << f->GetName() << std::endl;
      return TTreeReaderPointer();;
   }
   return tr;
}


#ifdef BUILD_AG

TTreeReaderPointer Get_AGSpillTree(Int_t runNumber)
{
   return Get_TreeReader_By_Name(runNumber, "AGSpillTree");
}

#endif
#ifdef BUILD_AG
TTreePointer Get_Chrono_Tree(const int runNumber, const TChronoChannel& channel)
{
   TFilePointer f = Get_File(runNumber);
   std::string name = channel.GetBoardName() + "/ChronoBox_" + channel.GetBranchName();
   return Get_Tree_By_Name(runNumber,name.c_str());
}
#endif
#ifdef BUILD_AG
TTreeReaderPointer Get_Chrono_TreeReader(const int runNumber, const TChronoChannel& channel)
{
   TFilePointer f = Get_File(runNumber);
   std::string name = channel.GetBoardName() + "/ChronoBox_" + channel.GetBranchName();
   return Get_TreeReader_By_Name(runNumber,name.c_str());
}
#endif
#ifdef BUILD_AG
TTreePointer Get_Chrono_Name_Tree(const int runNumber)
{
   return Get_Tree_By_Name(runNumber,"ChronoBoxChannels");
}
#endif

TTreePointer Get_Seq_Event_Tree(Int_t runNumber)
{
   return Get_Tree_By_Name(runNumber,"SequencerEventTree");
}

TTreePointer Get_Seq_State_Tree(Int_t runNumber)
{
   return Get_Tree_By_Name(runNumber,"SequencerStateTree");
}

#ifdef BUILD_AG
TTreePointer Get_StoreEvent_Tree(const int runNumber)
{
   return Get_Tree_By_Name(runNumber,"StoreEventTree");
}

TTreeReaderPointer Get_StoreEvent_TreeReader(const int runNumber)
{
   return Get_TreeReader_By_Name(runNumber,"StoreEventTree");
}

TTreeReaderPointer Get_BarEvent_TreeReader(const int runNumber)
{
   return Get_TreeReader_By_Name(runNumber,"TBarEventTree");
}

TTreeReaderPointer Get_AGDetectorEvent_TreeReader(const int runNumber)
{
   return Get_TreeReader_By_Name(runNumber,"TADetectorEventTree");
}
#endif

#ifdef BUILD_A2
// ALPHA 2 Getters:
TTreeReaderPointer A2_SIS_Tree_Reader(Int_t runNumber, Int_t Module_Number)
{
   TFilePointer f = Get_File(runNumber);
   std::string TreeName = "SIS" + std::to_string(Module_Number)+ std::string("Tree");
   return TTreeReaderPointer(f,TreeName.c_str());
}

TTreeReaderPointer Get_A2_SVD_Tree(Int_t runNumber)
{
   TFilePointer f = Get_File(runNumber);
   return TTreeReaderPointer(f,"SVDOfficialA2Time");
}
TTreeReaderPointer Get_A2SpillTree(Int_t runNumber)
{
   TFilePointer f = Get_File(runNumber);
   return TTreeReaderPointer(f,"A2SpillTree");
}

TTreeReaderPointer Get_TA2AnalysisReport_Tree(Int_t runNumber)
{
   TFilePointer f = Get_File(runNumber);

   TTreeReaderPointer t(f,"AnalysisReport/AnalysisReport");
   
   if (t->GetEntryStatus() == TTreeReader::EEntryStatus::kEntryNoTree)
      return TTreeReaderPointer();

   return t;
}

TTreeReaderPointer Get_TAlphaEvent_Tree(Int_t runNumber)
{
   TFilePointer f = Get_File(runNumber);
   return TTreeReaderPointer(f, "gAlphaEventTree");
}

#endif

#ifdef BUILD_AG

TTreeReaderPointer Get_TAGAnalysisReport_Tree(Int_t runNumber)
{
   TFilePointer f = Get_File(runNumber);
   TTreeReaderPointer t(f,"AnalysisReport/AnalysisReport");
   if (t->GetEntryStatus() == TTreeReader::EEntryStatus::kEntryNoTree)
      return TTreeReaderPointer();
   return t;
}

#endif

TTreeReaderPointer Get_feGEM_Tree(Int_t runNumber, const std::string& Category, const std::string& Varname)
{
   std::string combined_name = Category + "\\" + Varname;
   return Get_feGEM_Tree(runNumber,combined_name);
}

TTreeReaderPointer Get_feGEM_Tree(Int_t runNumber, const std::string& CombinedName)
{
   TFilePointer f = Get_File(runNumber);
   std::string name = "feGEM/" + CombinedName;
   
  TTreeReaderPointer t(f, name.c_str());
   if (t->GetEntryStatus() == TTreeReader::EEntryStatus::kEntryNoTree)
      return TTreeReaderPointer();
   return t;
}

TTreeReaderPointer Get_feLV_Tree(Int_t runNumber, const std::string& BankName)
{
   TFilePointer f = Get_File(runNumber);
   std::string name = "felabview/" + BankName;
   TTreeReaderPointer t(f, name.c_str());
   if (t->GetEntryStatus() == TTreeReader::EEntryStatus::kEntryNoTree)
      return TTreeReaderPointer();
   return t;
}

std::vector<TTreeReaderPointer> Get_feGEM_File_Trees(Int_t runNumber, const std::string& CombinedName)
{
   TFilePointer f = Get_File(runNumber);
   std::string name = "feGEM/" + CombinedName;
   TDirectory* d = f->GetDirectory("feGEM");
   if (!d)
   {
      Error("Get_feGEM_File_Trees", "Directory feGEM not found in file %s", f->GetName());
      return std::vector<TTreeReaderPointer>();
   }
   TList* aa = d->GetListOfKeys();
   std::vector<TTreeReaderPointer> trees;
   for (int i=0; i<aa->GetEntries(); i++)
   {
      if (strncmp(name.c_str(),aa->At(i)->GetName(),name.size())==0)
      {
         // Construct TTreePointer from TFilePointer and TTree name
         trees.emplace_back(f, CombinedName.c_str());
      }
   }
   return trees;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
