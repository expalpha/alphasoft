
#include "TAPlotEnvData.h"

/// @brief Constructor
TAPlotEnvData::TAPlotEnvData() : fDataIndex(-1) {}

/// @brief Constructor
/// @param name
/// @param label
/// @param arraynumber
TAPlotEnvData::TAPlotEnvData(const std::string name, const std::string label, const int arraynumber)
   : fVariableName(name), fLabel(label), fDataIndex(arraynumber)
{
}

/// @brief Deconstructor
TAPlotEnvData::~TAPlotEnvData() {}

/// @brief Copy Constructor
/// @param envData
TAPlotEnvData::TAPlotEnvData(const TAPlotEnvData &envData)
   : TObject(envData), fVariableName(envData.fVariableName), fLabel(envData.fLabel), fDataIndex(envData.fDataIndex)

{
   for (const TAPlotEnvDataPlot &plots : envData.fTimeWindowData) fTimeWindowData.emplace_back(plots);
}

/// @brief = Operator
/// @param rhs
/// @return
TAPlotEnvData TAPlotEnvData::operator=(const TAPlotEnvData &rhs)
{
   return TAPlotEnvData(rhs);
}

/// @brief Return the time range of contained data
/// @return
std::pair<double, double> TAPlotEnvData::GetMinMax() const
{
   double min = -1E99;
   double max = 1E99;
   for (const auto &plot : fTimeWindowData) {
      const size_t datasize = plot.size();
      for (size_t i = 0; i < datasize; ++i) {
         const double& data = plot.Data(i);
         if (data < min) min = data;
         if (data > max) max = data;
      }
   }
   return std::pair<double, double>(min, max);
}

/// @brief Get the TAPlotEnvDataPlot for TTimeWindow index number
/// @param index
/// @return
///
/// This getter will auto grow the vector if the index is beyond the current entires
TAPlotEnvDataPlot &TAPlotEnvData::GetTimeWindow(const size_t index)
{
   while (index >= fTimeWindowData.size()) {
      fTimeWindowData.emplace_back();
   }
   return fTimeWindowData.at(index);
}

/// @brief Get the TAPlotEnvDataPlot for TTimeWindow index number
/// @param index
/// @return
///
/// This const getter will not grow the vector
const TAPlotEnvDataPlot &TAPlotEnvData::GetTimeWindow(const size_t index) const
{
   return fTimeWindowData.at(index);
}

/// @brief Generate a TGraph for fTimeWindowData at index
/// @param index
/// @param zeroTime
/// @return
TGraph *TAPlotEnvData::BuildGraph(const size_t index, bool zeroTime)
{
   TGraph *graph = GetTimeWindow(index).NewGraph(zeroTime);
   graph->SetNameTitle(GetVariable().c_str(), std::string(GetLabel() + "; t [s];").c_str());
   return graph;
}

/// @brief Compare two TAPlotEnvData conatiners
/// @param lhs
/// @param rhs
/// @return
///
/// Comparison checks the variable name and index number, not the data contained inside
bool operator==(const TAPlotEnvData &lhs, const TAPlotEnvData &rhs)
{
   if (lhs.GetDataIndex() != rhs.GetDataIndex()) return false;
   if (lhs.GetVariable() != rhs.GetVariable()) return false;
   return true;
}
