#include "PairGetters.h"
#include "ChronoReader.h"

#ifdef BUILD_AG
std::vector<std::pair<double,int>> GetRunTimeOfChronoCount(Int_t runNumber, TChronoChannel chan, std::vector<double> tmin, std::vector<double> tmax) 
{
   std::vector<std::pair<double,int>> TimeCounts;

   assert(tmin.size() == tmax.size());
   const int entries = tmin.size();

   //Set broad range we are looking for
   double first_time = *std::min_element( std::begin(tmin), std::end(tmin));
   double last_time = *std::max_element( std::begin(tmax), std::end(tmax));

   if(last_time < 0){
      last_time=GetTotalRunTimeFromChrono(runNumber,chan.GetBoard());
      for(size_t i = 0; i < tmax.size(); i++)
      {
         tmax[i]=last_time;
      }
   } 

   TTreeReaderPointer ChronoReader = Get_Chrono_TreeReader(runNumber, chan);
   //if (!ChronoReader) continue;
   TTreeReaderValue<TCbFIFOEvent> ChronoEvent(*ChronoReader, "FIFOData");
   //ChronoReader = FindStartOfTree(ChronoReader,ChronoEvent,0,ChronoReader->GetEntries(),firstTime,1.0);
   while (ChronoReader->Next()) {
      const double t = ChronoEvent->GetRunTime();
      if (t < first_time) continue;
      if (t > last_time) break;
      for (int j = 0; j < entries; j++)
      {
         if (t > tmin[j])
            if (t < tmax[j])
            {
               TimeCounts.push_back(std::make_pair(t, ChronoEvent->fCounts));
            }
      }
   }
   return TimeCounts;
}

std::vector<std::pair<double,int>> GetRunTimeOfChronoCount(Int_t runNumber, const char* ChannelName, std::vector<double> tmin, std::vector<double> tmax)
{
  TChronoChannel chan = Get_Chrono_Channel(runNumber, ChannelName);
  return GetRunTimeOfChronoCount(runNumber, chan,  tmin, tmax);
}

std::vector<std::pair<double,int>> GetRunTimeOfChronoCount(Int_t runNumber, TChronoChannel chan, const std::vector<TAGSpill>& spills)
{
  std::vector<double> tmin;
  std::vector<double> tmax;
  tmin.reserve(spills.size());
  tmax.reserve(spills.size());
  for (auto& s: spills)
    {
      tmin.push_back(s.GetStartTime());
      tmax.push_back(s.GetStopTime());
    }
  return GetRunTimeOfChronoCount(runNumber, chan, tmin, tmax);
}

std::vector<std::pair<double,int>> GetRunTimeOfChronoCount(Int_t runNumber, const char* ChannelName, const std::vector<TAGSpill>& spills)
{
    return GetRunTimeOfChronoCount(runNumber,Get_Chrono_Channel(runNumber,ChannelName),spills);
}

#endif


#ifdef BUILD_A2
std::vector<std::pair<double,int>> GetSISTimeAndCounts(Int_t runNumber, TSISChannel SIS_Channel, std::vector<double> tmin, std::vector<double> tmax)
{
   assert(SIS_Channel.IsValid() );
   std::vector<std::pair<double,int>> TimeCounts;
   if (!SIS_Channel.IsValid())
   {
      std::cout<<"Unkown SIS channel!"<<std::endl;
      return TimeCounts;
   }
   
   //TimeCounts.reserve(1000000); //Ready for 1M results
   assert(tmin.size() == tmax.size());
   const int entries = tmin.size();

   //Set broad range we are looking for
   double first_time = *std::min_element( std::begin(tmin), std::end(tmin));
   double last_time = *std::max_element( std::begin(tmax), std::end(tmax));

   if(last_time < 0)
   {
      last_time = GetTotalRunTimeFromSIS(runNumber);
      for(size_t i=0; i<tmax.size(); i++)
         if (tmax[i] < 0)
            tmax[i]=last_time;
   }


   TTreeReaderPointer SISReader = A2_SIS_Tree_Reader( runNumber, SIS_Channel.fModule);
   TTreeReaderValue<TSISEvent> SISEvent(*SISReader, "TSISEvent");
   SISReader = FindStartOfTree(SISReader,SISEvent,0,SISReader->GetEntries(),first_time,10.0);

   // I assume that file IO is the slowest part of this function... 
   // so get multiple channels and multiple time windows in one pass
   while (SISReader->Next())
   {
      double t = SISEvent->GetRunTime();
      if (t < first_time)
         continue;
      if (t > last_time)
         break;
      for (int i=0; i<entries; i++)
      {
         if (t > tmin[i])
            if (t < tmax[i])
            {
               int counts = SISEvent->GetCountsInChannel(SIS_Channel);
               if (counts)
               {
                  TimeCounts.emplace_back(t, counts);
               }
            }
      }
   }
   return TimeCounts;
}

std::vector<std::pair<double,int>> GetSISTimeAndCounts(Int_t runNumber, const char* ChannelName, std::vector<double> tmin, std::vector<double> tmax)
{
   return GetSISTimeAndCounts(runNumber,GetSISChannel(runNumber,ChannelName),tmin,tmax);
}

std::vector<std::pair<double,int>> GetSISTimeAndCounts(Int_t runNumber, TSISChannel SIS_Channel, const std::vector<TA2Spill>& spills)
{
    assert(SIS_Channel.IsValid());
    std::vector<double> tmin;
    std::vector<double> tmax;
    tmin.reserve(spills.size());
    tmax.reserve(spills.size());
    for (auto& s: spills)
    {
        tmin.push_back(s.GetStartTime());
        tmax.push_back(s.GetStopTime());
    }
    return GetSISTimeAndCounts(runNumber, SIS_Channel, tmin, tmax);
}

std::vector<std::pair<double,int>> GetSISTimeAndCounts(Int_t runNumber, const char* ChannelName, const std::vector<TA2Spill>& spills)
{
    return GetSISTimeAndCounts(runNumber,GetSISChannel(runNumber,ChannelName),spills);
}

#endif

#include "TStoreLabVIEWEvent.h"

std::vector<std::pair<double,double>> GetLVData(Int_t runNumber, const char* BankName, int ArrayNo, double tmin, double tmax)
{
   std::vector<std::pair<double,double>> lvdata;
   TTreeReaderPointer feLVReader=Get_feLV_Tree(runNumber,BankName);
   TTree* tree = feLVReader->GetTree();
   if  (!tree)
   {
      std::cout<<"Warning: " << BankName << " not found for run " << runNumber << std::endl;
      return lvdata;
   }
   if (tree->GetBranchStatus("TStoreLabVIEWEvent"))
   {
      TTreeReaderValue<TStoreLabVIEWEvent> LVEvent(*feLVReader, "TStoreLabVIEWEvent");
      // I assume that file IO is the slowest part of this function... 
      // so get multiple channels and multiple time windows in one pass
      while (feLVReader->Next())
      { 
         double t=LVEvent->GetRunTime();
         //A rough cut on the time window is very fast...
         if (t < tmin)
            continue;
         if (t > tmax)
            break;
         lvdata.push_back(
            std::make_pair(t, LVEvent->GetArrayEntry(ArrayNo))
            );
      }
   }
   return lvdata;
}
#ifdef BUILD_A2
std::vector<std::pair<double,double>> GetLVData(Int_t runNumber, const char* BankName, int ArrayNo, const TA2Spill& spill)
{
    return GetLVData(runNumber,BankName,ArrayNo,spill.GetStartTime(), spill.GetStopTime());
}

#include "TStoreGEMEvent.h" 
std::vector<std::pair<double,double>> GetFEGData(Int_t runNumber, const char* name, double firstTime, double lastTime, double index)
{
   std::vector<std::pair<double,double>> data;
   //std::cout <<"Getting:"<< name << std::endl;
   TTreeReaderPointer feGEMReader = Get_feGEM_Tree(runNumber, name);
   //feGEMReader->Print();
   TTree* tree = feGEMReader->GetTree();
   //tree->Print();
   if  (!tree)
   {
      std::cout<<"Warning: " << name << " not found for run " << runNumber << std::endl;
      return data;
   }

   TTreeReaderValue<TStoreGEMData<double>> gemEvent(*feGEMReader,"TStoreGEMData<double>");
   // I assume that file IO is the slowest part of this function... 
   // so get multiple channels and multiple time windows in one pass
   while (feGEMReader->Next())
   {
      
      const double runTime = gemEvent->GetRunTime();
      //A rough cut on the time window is very fast...
      if (runTime < firstTime)
         continue;
      if (runTime > lastTime)
         break;
      data.emplace_back(runTime,gemEvent->GetArrayEntry(index));
   }
   return data;
}

std::vector<std::pair<double,std::vector<double>>> GetFEGData(Int_t runNumber, const char* name, double firstTime, double lastTime)
{
   std::vector<std::pair<double,std::vector<double>>> data;
   //std::cout <<"Getting:"<< name << std::endl;
   TTreeReaderPointer feGEMReader = Get_feGEM_Tree(runNumber, name);
   //feGEMReader->Print();
   TTree* tree = feGEMReader->GetTree();
   //tree->Print();
   if  (!tree)
   {
      std::cout<<"Warning: " << name << " not found for run " << runNumber << std::endl;
      return data;
   }

   TTreeReaderValue<TStoreGEMData<double>> gemEvent(*feGEMReader,"TStoreGEMData<double>");
   // I assume that file IO is the slowest part of this function... 
   // so get multiple channels and multiple time windows in one pass
   while (feGEMReader->Next())
   {
      
      const double runTime = gemEvent->GetRunTime();
      //A rough cut on the time window is very fast...
      if (runTime < firstTime)
         continue;
      if (runTime > lastTime)
         break;
      std::vector<double> array = gemEvent->GetData();
      
      data.emplace_back(runTime,array);
   }
   return data;
}
std::pair<double,double> GetFEGPower(Int_t runNumber, const char* name, double FirstTime, double LastTime, double offset)
{
   Double_t power(0.0);
   Int_t npowers(0);
   std::pair<double, double> presult=std::make_pair(0,0);
   //std::cout <<"Getting:"<< name << std::endl;
   Double_t firstTime =FirstTime-offset-1;
   Double_t lastTime=LastTime-offset;
   TTreeReaderPointer feGEMReader = Get_feGEM_Tree(runNumber, name);
   //feGEMReader->Print();
   TTree* tree = feGEMReader->GetTree();
   //tree->Print();
   if  (!tree)
   {
      std::cout<<"Warning: " << name << " not found for run " << runNumber << std::endl;
      return presult;
   }

   TTreeReaderValue<TStoreGEMData<double>> gemEvent(*feGEMReader,"TStoreGEMData<double>");
   // I assume that file IO is the slowest part of this function... 
   // so get multiple channels and multiple time windows in one pass
      Double_t ptime(0.);
      while (feGEMReader->Next())
   {
      
      const double runTime = gemEvent->GetRunTime();
      //A rough cut on the time window is very fast...
      // but scanning through the stacking period dominates the macro running time
      if (runTime < firstTime)
         continue;
      if (runTime > lastTime)
         break;
      auto data=gemEvent->GetData();
      Int_t datasize=(Int_t)data.size();
      Int_t start_index=Int_t((firstTime+1-runTime)*datasize);
      ptime = (start_index>0)?(-firstTime+runTime):ptime;
      Int_t end_index= Int_t((lastTime-runTime)*datasize);
      // std::cout<<"runTime "<< runTime<<" firstTime "<<firstTime<<" lastTime "<<lastTime<<" offset "<< offset;
      // std::cout<<" istart: "<<start_index<<" iend: "<<end_index<<" size: "<<datasize<<std::endl;
      for (int i=(start_index>0)?start_index:0;i<((end_index<datasize)?end_index:datasize);i++){

        npowers++;
        power+=data[i];
        //std::cout<<i<<" npowers "<<npowers<<" data "<<data[i]<<std::endl;
      }
      //std::cout<<"npowers: "<<npowers<<"power: "<< power/npowers<<" start "<<start_index<<" end "<<end_index<<std::endl; 
      //std::cout<<".";
   }
      presult=std::make_pair(ptime,power/npowers);
   return presult;
}
std::pair<double,double> GetFEGPower(Int_t runNumber, const char* name, double FirstTime, double LastTime, double offset, std::vector<double>& pbuff)
{
   Double_t power(0.0);
   Int_t npowers(0);
   std::pair<double, double> presult=std::make_pair(0,0);
   //std::cout <<"Getting:"<< name << std::endl;
   Double_t firstTime =FirstTime-offset-1;
   Double_t lastTime=LastTime-offset;
   TTreeReaderPointer feGEMReader = Get_feGEM_Tree(runNumber, name);
   //feGEMReader->Print();
   TTree* tree = feGEMReader->GetTree();
   //tree->Print();
   if  (!tree)
   {
      std::cout<<"Warning: " << name << " not found for run " << runNumber << std::endl;
      return presult;
   }

   TTreeReaderValue<TStoreGEMData<double>> gemEvent(*feGEMReader,"TStoreGEMData<double>");
   // I assume that file IO is the slowest part of this function... 
   // so get multiple channels and multiple time windows in one pass
      Double_t ptime(0.);
      while (feGEMReader->Next())
   {
      
      const double runTime = gemEvent->GetRunTime();

      if (runTime < firstTime)
         continue;
      if (runTime > lastTime)
         break;
      auto data=gemEvent->GetData();
      Int_t datasize=(Int_t)data.size();
      Int_t start_index=Int_t((firstTime+1-runTime)*datasize);
      ptime = (start_index>0)?(-firstTime+runTime):ptime;
      Int_t end_index= Int_t((-runTime+lastTime)*datasize); 
      //std::cout<<"runTime "<< runTime<<" firstTime "<<firstTime<<" lastTime "<<lastTime<<" offset "<< offset;
      //std::cout<<" istart: "<<start_index<<" iend: "<<end_index<<" size: "<<datasize<<std::endl;
      for (int i=(start_index>0)?start_index:0;i<((end_index<datasize)?end_index:datasize);i++){

        npowers++;
        power+=data[i];
        pbuff.emplace_back(data[i]);
        //std::cout<<i<<" npowers "<<npowers<<" data "<<data[i];
      }
      //std::cout<<"npowers: "<<npowers<<"power: "<< power/npowers<<" RunTime "<<runTime<< " start "<<start_index<<" end "<<end_index<<" pbuffsize "<<pbuff.size()<<" ptime "<<ptime<<std::endl; 
      //std::cout<<".";
   }
      presult=std::make_pair(ptime,power/npowers);
   return presult;
}

#endif
