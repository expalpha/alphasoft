#ifdef BUILD_A2
#include "TA2Plot_Filler.h"
#include "ChronoReader.h"

/*! \class TA2Plot_Filler
    \brief  Class for simultaniously filling multiple TA2Plot containers

 Give significant performance boost by only needed to read a root file once for many TA2Plots
 
~~~~{.cpp}
Begin_Macro(source)
{

   // Create some TA2Plots
   TA2Plot* a = new TA2Plot(); // Example of creating it as a pointer
   TA2Plot() b; // Example of creating a TA2Plot locally
 
   // Add time windows to TA2Plots
   a->AddDumpGates(39993, {"Mixing"},{0});
   b.AddTimeGate(39993,0,100.);

   // Create TA2PlotFiller
   TA2PlotFiller filler;
   filler.BookPlot(a);   // a is already a pointer
   filler.BookPlot(&b);  // Book plot needs the address (pointer) of TA2Plot
   // Load data in single pass over the run file
   filler.LoadData();

   // Data is loaded
   a->DrawVertexCanvas();
   b.DrawVertexCanvas();
End_Macro()
~~~~

*/

/// @brief Constructor
TA2Plot_Filler::TA2Plot_Filler() {}

/// @brief Deconstructor
///
/// Deconstructor does not destroy the TA2Plots booked in for filling, this class doesn't own them
TA2Plot_Filler::~TA2Plot_Filler() {}

/// @brief Book an existing TA2Plot for filling
/// @param plot 
///
/// Check TA2Plot pointer value to make sure you can't add the TA2Plot for booking twice
void TA2Plot_Filler::BookPlot(TA2Plot *plot)
{
   for (const auto &no : plot->GetArrayOfRuns())
      AddRunNumber(no);
   for (const auto &f : plot->GetGEMChannels())
      TrackGEMChannel(f);
   for (const auto &f : plot->GetLVChannels())
      TrackLVChannel(f);
   for (TA2Plot *p : plots) {
      if (p == plot) {
         std::cout << "Plot already booked" << std::endl;
         return;
      }
   }
   plots.push_back(plot);
}

/// @brief Track feGEM variable (Add it to all TA2Plots that have been booked)
/// @param gemchannel 
void TA2Plot_Filler::TrackGEMChannel(const TAPlotFEGEMData& gemchannel)
{
   for (const auto &g : GEMChannels) {
      if ( g == gemchannel)
         return;
   }
   GEMChannels.emplace_back(gemchannel);
   // We dont need the data in the copy of LVChannel...
   GEMChannels.back().clear();
}

/// @brief Track feLabVIEW variable (Add it to all TA2Plots that have been booked)
/// @param lvchannel
void TA2Plot_Filler::TrackLVChannel(const TAPlotFELabVIEWData &lvchannel)
{
   for (const auto &g : LVChannels) {
      if ( g == lvchannel)
         return;
   }
   LVChannels.emplace_back(lvchannel);
   // We dont need the data in the copy of LVChannel...
   LVChannels.back().clear();
}

/// @brief Load the feGEM data for a specific run number (to all booked TA2Plots)
/// @param runNumber 
/// @param first_time 
/// @param last_time 
/// @param verbose 
void TA2Plot_Filler::LoadfeGEMData(int runNumber, double first_time, double last_time, int verbose)
{
   // feGEM isn't grouped... we could gain speed by reading only once and filling groups of feGEM
   // Fill the plot, a fine time cut is done inside
   for (TA2Plot *p : plots) {
      // If current runNumber isn't in plot... skip
      if (std::find(p->GetArrayOfRuns().begin(), p->GetArrayOfRuns().end(), runNumber) == p->GetArrayOfRuns().end())
         continue;
      p->LoadFEGEMData(runNumber, first_time, last_time,verbose);
   }
}

/// @brief Load the feLabVIEW data for a specific run number (to all booked TA2Plots)
/// @param runNumber 
/// @param first_time 
/// @param last_time 
/// @param verbose 
void TA2Plot_Filler::LoadfeLVData(int runNumber, double first_time, double last_time, int verbose)
{
   for (TA2Plot *p : plots) {
      // If current runNumber isn't in plot... skip
      if (std::find(p->GetArrayOfRuns().begin(), p->GetArrayOfRuns().end(), runNumber) == p->GetArrayOfRuns().end())
         continue;
      p->LoadFELVData(runNumber, first_time, last_time, verbose);
   }
}

/// @brief Load the feGEM, feLabVIEW, SVD and SIS data for a specific run (to all TA2Plots booked)
/// @param runNumber 
/// @param first_time 
/// @param last_time 
/// @param verbose 
void TA2Plot_Filler::LoadData(int runNumber, double first_time, double last_time, int verbose)
{
   std::cout << "TA2Plot_Filler Loading run data:" << runNumber << " over the range between " << first_time << "s to "
             << last_time << "s" << std::endl;
   AlphaColourWheel colours;


   LoadfeGEMData(runNumber, first_time, last_time, verbose);
   LoadfeLVData(runNumber, first_time, last_time, verbose);

   // TTreeReaders are buffered... so this is faster than iterating over a TTree by hand
   // More performance is maybe available if we use DataFrames...
   TTreeReaderPointer SVDReader = Get_A2_SVD_Tree(runNumber);
   TTreeReaderValue<TSVD_QOD> SVDEvent(*SVDReader, "OfficialTime");
   SVDReader = FindStartOfTree(SVDReader,SVDEvent,0,SVDReader->GetEntries(),first_time,2.0);


   std::vector<TA2Plot*> stale_plots;
   for (TA2Plot* p : plots)
   {
      if (p->IsDataStale())
         stale_plots.emplace_back(p);
   }

   // I assume that file IO is the slowest part of this function...
   // so get multiple channels and multiple time windows in one pass
   while (SVDReader->Next()) {
      const double t = SVDEvent->GetTimeOfEvent();
      // A rough cut on the time window is very fast...
      if (t < first_time) continue;
      if (t >= last_time) break;
      // Fill the plot, a fine time cut is done inside
      for (auto &p : stale_plots)
         p->AddSVDEvent(*SVDEvent);
   }

   // TTreeReaders are buffered... so this is faster than iterating over a TTree by hand
   // More performance is maybe available if we use DataFrames...
   for (int sis_module_no = 0; sis_module_no < NUM_SIS_MODULES; sis_module_no++) {
      TTreeReaderPointer SISReader = A2_SIS_Tree_Reader(runNumber, sis_module_no);
      TTreeReaderValue<TSISEvent> SISEvent(*SISReader, "TSISEvent");
      SISReader = FindStartOfTree(SISReader,SISEvent,0,SISReader->GetEntries(),first_time,10.0);
            
// I assume that file IO is the slowest part of this function...
      // so get multiple channels and multiple time windows in one pass
      for (auto &p : stale_plots)
         p->SetSISChannels(runNumber);
      while (SISReader->Next()) {
         double t = SISEvent->GetRunTime();
         // A rough cut on the time window is very fast...
         if (t < first_time) continue;
         if (t >= last_time) break;
         // Fill the plot, a fine time cut is done inside
         for (auto &p : stale_plots)
            p->AddSISEvent(*SISEvent);
      }
   }
}

/// @brief Load data for all runs and all TA2Plots
/// @param verbose 
void TA2Plot_Filler::LoadData(int verbose)
{
   std::set<int> Runs;
   for (auto &plot : plots)
      for (auto &run : plot->GetArrayOfRuns()) Runs.insert(run);
   std::vector<int> UniqueRuns(Runs.begin(), Runs.end());
   Runs.clear();
   for (auto &plot : plots) {
      for (const auto &c : GEMChannels) {
         plot->SetGEMChannel(c);
      }
      for (const auto &c : LVChannels) {
         plot->SetLVChannel(c);
      }
   }
   std::vector<double> last_times(UniqueRuns.size(), 0.);
   std::vector<double> first_times(UniqueRuns.size(), 1E99);
   for (auto &plot : plots) {
      // TAPlotTimeWindows* temp = plot->GetTimeWindows();
      // Calculate our list time... so we can stop early
      for (size_t t = 0; t < plot->GetTimeWindows().size(); t++) {
         const double mintime = plot->GetTimeWindows().MinTime(t);
         const double maxtime = plot->GetTimeWindows().MaxTime(t);
         const int runNumber = plot->GetTimeWindows().RunNumber(t);
         for (size_t i = 0; i < UniqueRuns.size(); i++)
            if (runNumber == runNumbers[i]) {
               if (maxtime < 0) last_times[i] = 1E99;
               if (last_times[i] < maxtime) {
                  last_times[i] = maxtime;
               }
               if (first_times[i] > mintime) {
                  first_times[i] = mintime;
               }
            }
      }
   }
   // We only need to re-load data into stale plots... lets make sure 
   // they are empty
   for (TA2Plot* p : plots)
   {
      if (p->IsDataStale())
      {
         p->ClearContainers();
      }
   }
   // No load the data (I will skip stale plots)
   for (size_t i = 0; i < runNumbers.size(); i++) {
      LoadData(runNumbers[i], first_times[i], last_times[i], verbose);
   }
   // Give the TAPlots a timestamp so we can track how long processing has taken
   for (auto &plot : plots) {
      plot->LoadingDataLoadingDone();
   }
}

#endif
