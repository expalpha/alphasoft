#include "IntGetters.h"

#ifdef BUILD_AG
Int_t Get_Chrono_Channel_In_Board(Int_t runNumber, const std::string& ChronoBoard, const char* ChannelName, Bool_t ExactMatch)
{
   TTreePointer t=Get_Chrono_Name_Tree(runNumber);
   TChronoChannelName* n=new TChronoChannelName();
   t->SetBranchAddress("ChronoChannel", &n);
   t->GetEntry(TChronoChannel::CBMAP.at(ChronoBoard));
   Int_t Channel=n->GetChannel(ChannelName, ExactMatch);
   delete n;
   return Channel;
}
#endif

#ifdef BUILD_AG
Int_t GetCountsInChannel(Int_t runNumber,  TChronoChannel channel, std::vector<double> tmin, std::vector<double> tmax)
{
   if (!channel.IsValidChannel())
      return -2;

   Int_t Counts = 0;
   for (double& max: tmax)
      if (max<0.) max = GetAGTotalRunTime(runNumber);

   TTreeReaderPointer CBTreeReader = Get_Chrono_TreeReader(runNumber, channel);
   //TTree* t = Get_Chrono_Tree(runNumber, TChronoChannel(board.first,CHRONO_CLOCK_CHANNEL).GetBranchName());
   if (!CBTreeReader)
      return -1;
   TTreeReaderValue<TCbFIFOEvent> e(*CBTreeReader,"FIFOData");
   while (CBTreeReader->Next())
   {
      const double t = e->GetRunTime();
      bool time_match = false;
      for (size_t i = 0; i < tmin.size(); i++)
      {
         if (t < tmin[i]) continue;
         if (t > tmax[i]) continue;
         time_match = true;
         break;
      }
      if (!time_match) continue;
      if ( int(e->GetChannel()) != channel.GetChannel())
            continue;
      //Is leading edge pulse
      if (e->IsLeadingEdge())
         Counts++;
   }
   return Counts;
}

Int_t GetCountsInChannel(Int_t runNumber,  TChronoChannel channel, double tmin, double tmax )
{
   return GetCountsInChannel( runNumber, channel, std::vector<double>{tmin}, std::vector<double>{tmax});
}

Int_t GetCountsInChannel(Int_t runNumber,  TChronoChannel channel, std::vector<TAGSpill> spills)
{
   std::vector<double> tmin, tmax;
   for (const TAGSpill& s: spills)
   {
       tmin.emplace_back(s.GetStartTime());
       tmax.emplace_back(s.GetStopTime());
   }
   return GetCountsInChannel(runNumber,  channel, tmin, tmax);
}

Int_t GetCountsInChannel(Int_t runNumber,  TChronoChannel channel, std::vector<std::string> description, std::vector<int> index)
{
   std::vector<TAGSpill> spills = Get_AG_Spills(runNumber, description, index);
   return GetCountsInChannel(runNumber,channel,spills);
}


#endif
#ifdef BUILD_AG
Int_t GetCountsInChannel(Int_t runNumber,  const char* ChannelName,  double tmin, double tmax)
{
   TChronoChannel chan = Get_Chrono_Channel(runNumber, ChannelName);
   if (chan.IsValidChannel())
      return GetCountsInChannel( runNumber, chan, std::vector<double>{tmin}, std::vector<double>{tmax});
   else
      return -1;
}
#endif
#ifdef BUILD_AG
Int_t ApplyCuts(TStoreEvent* e)
{
   //Dummy example of ApplyCuts for a TStoreEvent... 
   //Change this when we have pbars!
   Double_t R=e->GetVertex().Perp();
   Int_t NTracks=e->GetNumberOfTracks();
   if (NTracks==2)
      if (R<50) return 1;
   if (NTracks>2)
      if (R<45) return 1;
   return 0;
}
#endif
#ifdef BUILD_AG

#endif

#if BUILD_A2

int Count_SIS_Triggers(int runNumber, TSISChannel ch, std::vector<double> tmin, std::vector<double> tmax)
{
   std::vector<std::pair<double,int>> counts = GetSISTimeAndCounts(runNumber, ch, tmin, tmax);
   int total = 0;
   for (const std::pair<double,int>& c: counts)
      total += c.second;
   return total;
}
#endif

//*************************************************************
// Energy Analysis
//*************************************************************

Int_t LoadRampFile(const char* filename, std::vector<double>& xvec,std::vector<double>& yvec)
{
  std::ifstream fin(filename);
  Int_t n=0;
  double x, y;
  while(fin.good())
    {
      fin>>x>>y;
      //std::cout<<n<<"\t"<<x<<"\t"<<y<<std::endl;
      xvec.emplace_back(x);
      yvec.emplace_back(y);
      ++n;
    }
  fin.close();

  // actual number of points
  --n;
  --n;
  //    cout<<"--> "<<n<<endl;
  Double_t endRampTime=-1.;

  for(Int_t i=n; i>0; --i)
    if(xvec[i]>0.000001)
      {
	endRampTime=xvec[i];
	break;
      }
  std::cout<<"Ramp Duration "<<"\t"<<endRampTime<<" s"<<std::endl;

  // time "normalization" to 1 second? Why would we ever need this?
  //for(Int_t i=0; i<n; ++i) x[i] = x[i]/endRampTime;

  return n;
}

#include <TRegexp.h>
#include <TObjString.h>
int GetRunNumber( TString fname )
{
  TString the_name = ((TObjString*)fname.Tokenize("/")->Last())->GetString();
  // TRegexp re("[0-9][0-9][0-9][0-9][0-9]");
  // TRegexp re("[0-9][0-9][0-9][0-9]");
  TRegexp re("[0-9][0-9][0-9]+");
  int pos = the_name.Index(re);
  //  int run = TString(fname(pos,5)).Atoi();
  int run = TString(the_name(pos,6)).Atoi();
  return run;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
