#include "TAPlotFELabVIEWData.h"

/// @brief Constructor
TAPlotFELabVIEWData::TAPlotFELabVIEWData() {}

/// @brief Deconstructor
TAPlotFELabVIEWData::~TAPlotFELabVIEWData() {}

/// @brief Constructor
/// @param varname 
/// @param label 
/// @param index 
TAPlotFELabVIEWData::TAPlotFELabVIEWData(const std::string &varname, const std::string label, const int index)
   : TAPlotEnvData(varname, label, index)
{
}