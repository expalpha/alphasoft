#include "RootUtilGlobals.h"
#include <iostream>
#include <vector>
#include <deque>
#include <string.h>
/*! \brief Namespace for globals within the rootUtils library
 *
 * Presently used for setting the number of bins in histograms
 * (or flipping between fixed with time bins and fixed number of time bins)
 */
namespace rootUtils
{
   /// This namespace shouldn't be reached by the user, can I do a private namespace?
   namespace internal
   {
      /// @brief Default number of bins in histogram (that isn't a time axis)
      static int gGeneralBinNumber = 100;
      /// @brief Enum to toggle between Time histogram modes
      static TimeAxisMode gTimeAxisMode = kFixedTimeBinNumber;
      /// @brief Default number of bins for a time based histogram
      static int gTimeBinNumber = 100;
      /// @brief Default time (in seconds) for a fixed time width time axis mode
      static double gTimeBinSize = 1.;
      /// @brief Default threshold at which we can still draw dump markers on Scaler plots (SIS)
      static std::vector<bool> gDisplayDumpMarkers(NUMSEQ,false);

      /// @brief Bool to enable auto execution of analyzer if root file is old or missing
      static bool gAutoRunAnalyzer = false;
      /// @brief Threshold for git commits behind a root file may be (if gAutoRunAnalyzer is enabled)
      static int gPermittedRootFileAge = 0;
      /// @brief Hold list of analyzer programs to trigger on instance of no (or outdated root file being found)
      static std::vector<std::string> gDefaultAnalyserPrograms;
      static std::vector<std::string> gAnalyzerArguments;
      static std::vector<std::string> gAnalyzerModuleFlags;
      /// @brief Bool to disable checking the analysis report on open of file
      static bool gCheckAnalysisReport = true;

      /// @brief List of possible places to look for MIDAS files
      static std::deque<std::string> gMIDASFilePaths = {
         ".", ///< First look in the current path
         "midasdata", ///< Second look in a typical local path name
         "/eos/experiment/alpha/midasdata/", ///< Next see if you can reach EOS (ALPHA2)
         "/eos/experiment/ALPHAg/midasdata_old/" ///< Next see if you can reach EOS (ALPHAg)
      };

      /// @brief List of possible places to look for ROOT files
      static std::deque<std::string> gROOTFilePaths = {
         // Should be added by user. After checking all here, checks in $AGOUTPUT, which will be default location.
      };

      /// @brief Define rootfile name using manalyzer convention
      static std::string gFileNamePattern = "output%05d.root";

      static bool gOptimisedFileLoading = true;
   }

   /// Set the number of bins in the typical histogram (anything that isn't the time axis)
   void SetDefaultBinNumber(int i)
   {
      rootUtils::internal::gGeneralBinNumber = i;
   }

   /// @brief Get the number of bins in the typical histogram (anything that isn't the time axis)
   /// @return 
   int GetDefaultBinNumber()
   {
      return rootUtils::internal::gGeneralBinNumber;
   }

   /// @brief Time Axis mode control
   /// @param mode 
   ///
   /// This function will be called if you use SetDefaultBinNumber or SetDefaultBinSize to
   /// automatically switch modes
   void SetTimeAxisMode(TimeAxisMode mode)
   {
      rootUtils::internal::gTimeAxisMode = mode;
   }

   /// Time Axis mode getter
   /**
    * This function will return the current time axis mode, a TimeAxisMode enum
    */
   TimeAxisMode GetTimeAxisMode()
   {
      return rootUtils::internal::gTimeAxisMode;
   }

   /// \brief Set bin configuration for fixed number of time bins mode
   /**
    * SetDefaultBinNumber will switch to kFixedTimeBinNumber mode
    */
   void SetTimeBinNumber(int i)
   {
      rootUtils::internal::gTimeBinNumber = i;
      if (GetTimeAxisMode() != kFixedTimeBinSize)
      {
         std::cout <<"Switching to fix number of bins on time axis ("
            << rootUtils::internal::gTimeBinNumber << ")\n";
         SetTimeAxisMode(kFixedTimeBinNumber);
      }
   }

   /// \brief Get number of bins for fixed number of time bins mode
   /**
    * GetTimeBinNumber will throw a warning if you are
    * reading it outside of the kFixedTimeBinNumber mode
    */
   int GetTimeBinNumber()
   {
      return rootUtils::internal::gTimeBinNumber;
   }

   /// Set bin configuration for fixed time bin width
   /**
    * SetDefaultBinNumber will switch to kFixedTimeBinSize mode
    */
   void SetTimeBinSize(double size)
   {
      rootUtils::internal::gTimeBinSize = size;
      if (GetTimeAxisMode() != kFixedTimeBinSize)
      {
         std::cout <<"Switching to fix number of bins on time axis (" << size << ")\n";
         SetTimeAxisMode(kFixedTimeBinSize);
      }
   }

   /// Get bin width for fixed time bin mode
    /**
    * GetTimeBinSize will throw a warning if you are
    * reading it outside of the kFixedTimeBinSize mode
    */
   double GetTimeBinSize()
   {
      if (GetTimeAxisMode() != kFixedTimeBinSize)
      {
         std::cerr <<"Warning: Reading kFixedTimeBinSize property when not in kFixedTimeBinSize mode\n";
      }
      return rootUtils::internal::gTimeBinSize;
   }

   /// Return a number of bins when using the fixed time bin width (rounding up to avoid overflow...)
   int CalculateNumberOfBinsInRange(double min, double max)
   {
      int bins = std::ceil( ( max - min ) / GetTimeBinSize());
      if (bins < 0)
      {
         std::cerr << "Error Calculating number of bins for fix time bins:" <<
            "min (" << min << "  > max ("  << max << ")";
         return abs(bins);
      }
      return bins;
   }
   
   /// Print the current state of the time axis plotting mode
   void PrintTimeAxisMode() {
      std::cout << "Time Axis Bin Mode:\t";
      if (GetTimeAxisMode() == kFixedTimeBinNumber)
      {
         std::cout << "Fixed Bin Number (" << GetTimeBinNumber() << " bins)\n";
         return;
      }
      if (GetTimeAxisMode() == kFixedTimeBinSize)
      {
         std::cout << "Fix Width (" << GetTimeBinSize() << " seconds)\n";
         return;
      }
      std::cerr << "Unknown Time Axis Mode\n";
      return;
   }

   /// Enable the display of dump markers in plots
   void ShowDumpMarkers(const SEQUENCER_ID& ID)
   {
      rootUtils::internal::gDisplayDumpMarkers.at(ID) = true;
   }
   /// Disable the display of dump markers in plots
   void HideDumpMarkers(const SEQUENCER_ID& ID)
   {
      rootUtils::internal::gDisplayDumpMarkers.at(ID) = false;
   }
   /// Turn on the display of CAT Dump Markers
   void ShowCATDumpMarkers()
   {
      return ShowDumpMarkers(SEQUENCER_ID::PBAR);
   }
   /// Turn on the display of RCT Dump Markers
   void ShowRCTDumpMarkers()
   {
      return ShowDumpMarkers(SEQUENCER_ID::RECATCH);
   }
   /// Turn on the display of ATM Dump Markers
   void ShowATMDumpMarkers()
   {
      return ShowDumpMarkers(SEQUENCER_ID::ATOM);
   }
   /// Turn on the display of POS Dump Markers
   void ShowPOSDumpMarkers()
   {
      return ShowDumpMarkers(SEQUENCER_ID::POS);
   }
   /// Turn on the display of RCT_BOTG Dump Markers
   void ShowRCT_BOTGDumpMarkers()
   {
      return ShowDumpMarkers(SEQUENCER_ID::RCT_BOTG);
   }
   /// Turn on the display of ATM_BOTG Dump Markers
   void ShowATM_BOTGDumpMarkers()
   {
      return ShowDumpMarkers(SEQUENCER_ID::ATM_BOTG);
   }
   /// Turn on the display of ATM_TOPG Dump Markers
   void ShowATM_TOPGDumpMarkers()
   {
      return ShowDumpMarkers(SEQUENCER_ID::ATM_TOPG);
   }
   /// Turn on the display of RCT_TOPG Dump Markers
   void ShowRCT_TOPGDumpMarkers()
   {
      return ShowDumpMarkers(SEQUENCER_ID::RCT_TOPG);
   }
   /// Turn on the display of BML Dump Markers
   void ShowBMLDumpMarkers()
   {
      return ShowDumpMarkers(SEQUENCER_ID::BML);
   }
   /// Query if we should display a sequences dump markers
   bool GetDumpMarkerDisplayMode(const SEQUENCER_ID& ID)
   {
      return rootUtils::internal::gDisplayDumpMarkers.at(ID);
   }
   /// Turn on all sequences dump markers drawn in time histograms
   void ShowAllDumpMarkers()
   {
      for (size_t i = 0; i < rootUtils::internal::gDisplayDumpMarkers.size(); i++)
         rootUtils::internal::gDisplayDumpMarkers.at(i) = true;
   }
   /// Turn off all sequences dump markers drawn in time histograms
   void DisableDumpMarkerDrawing()
   {
      for (size_t i = 0; i < rootUtils::internal::gDisplayDumpMarkers.size(); i++)
         rootUtils::internal::gDisplayDumpMarkers.at(i) = false;
   }
   /// Display the dump marker display mode for all sequences to the user
   void PrintDumpMarkerDisplayMode()
   {
      int enabled_count = 0;
      for (size_t i = 0; i < rootUtils::internal::gDisplayDumpMarkers.size(); i++)
      {
         if (rootUtils::internal::gDisplayDumpMarkers.at(i))
         {
            enabled_count++;
            std::cout <<"Display of " << GetSequencerName(i) << " enabled\n";
         }
      }
      if (enabled_count == 0)
         std::cout << "Adding of dump markers to plots disabled\n";
   }

   /// @brief Check if Auto running of analyzer is enabled (default off)
   /// @return 
   bool AutoRunAnalyzer()
   {
      return rootUtils::internal::gAutoRunAnalyzer;
   }

   /// @brief Check the number of commits behind we are permitted to be before we trigger a re-analysis
   /// @return 
   int PermittedCommitsBehind()
   {
      return rootUtils::internal::gPermittedRootFileAge;
   }

   /// @brief Set the number of commits behind to trigger a re-analysis
   /// @return 
   void SetPermittedCommitsBehind(int n_commits)
   {
      rootUtils::internal::gPermittedRootFileAge = n_commits;
   }

   void SetDefaultAnalyzer(const std::vector<std::string> analyzerList,
                           const std::vector<std::string> arguments,
                           const std::vector<std::string> module_flags)
   {
      if (analyzerList.size() != arguments.size() || analyzerList.size() != module_flags.size())
      {
         std::cerr<<"The vector sizes of all arguments to SetDefaultAnalyzer must be equal lengths. Aborting setting of list"<<std::endl;
         return;
      }

      rootUtils::internal::gDefaultAnalyserPrograms.clear();
      rootUtils::internal::gAnalyzerArguments.clear();
      rootUtils::internal::gAnalyzerModuleFlags.clear();
      for (const std::string& program: analyzerList)
         rootUtils::internal::gDefaultAnalyserPrograms.emplace_back(program);
      for (const std::string& manalzyer_args: arguments)
         rootUtils::internal::gAnalyzerArguments.emplace_back(manalzyer_args);
      for (const std::string& module_args: module_flags)
         rootUtils::internal::gAnalyzerModuleFlags.emplace_back(module_args);
     rootUtils::internal::gAutoRunAnalyzer = true;
   }

   void SetDefaultAnalyzer(const std::vector<std::string> analyzerList)
   {
      std::vector<std::string> arguments(analyzerList.size());
      std::vector<std::string> module_flags(analyzerList.size());
      return SetDefaultAnalyzer(analyzerList,arguments,module_flags);
   }

   std::vector<std::tuple<std::string,std::string,std::string>> GetAnalyzerList()
   {
      std::vector<std::tuple<std::string,std::string,std::string>> progs;
      for (size_t i = 0; i < rootUtils::internal::gDefaultAnalyserPrograms.size(); ++i)
      {
         progs.emplace_back(
            rootUtils::internal::gDefaultAnalyserPrograms.at(i),
            rootUtils::internal::gAnalyzerArguments.at(i),
            rootUtils::internal::gAnalyzerModuleFlags.at(i)
         );
      }
      return progs;
   }
   
   /// @brief Function to display the default analysers (used if root files doesn't exist)
   ///
   /// By default this isn't active as ALPHA2 and ALPHAg are separate
   void PrintDefaultAnalyzers()
   {
      if (GetAnalyzerList().empty())
      {
         std::cout <<"No default analyzers set (If a root file doesn't exist, I wont attempt to create it\n";
         return;
      }
      std::cout << "Analyzers:\n";
      for (std::tuple<std::string,std::string,std::string> &program : rootUtils::GetAnalyzerList()) {
         // Program to run
         std::cout << std::get<0>(program) << "\t";
         // Agruments for program, eg --mt
         std::cout << std::get<1>(program) << "\t";
         // Run files
         std::cout << "midasfiles.lz4\t";
         // Module arguments
         if (std::get<2>(program).size())
            std::cout << " -- " << std::get<2>(program);
         std::cout << "\n";
      }
      std::cout <<"MIDAS Paths searched for files\n";
      for (auto& s: GetListOfPossibleMIDASFilePaths())
         std::cout << "\t" << s << "\n";
   }


   /// @brief Disable the checking of the AnalysisReport on file open
   ///
   /// By default the check of the analysis report is enabled
   void DisableAnalysisReportCheck()
   {
      rootUtils::internal::gCheckAnalysisReport = false;
   }

   /// @brief Check if we should get the analysis report
   ///
   /// By default the check of the analysis report is enabled
   bool AllowGetAnalysisReport()
   {
      return rootUtils::internal::gCheckAnalysisReport;
   }

   /// @brief Report if we check the analysis report age
   ///
   /// By default the check of the analysis report is enabled
   void PrintAnalysisReportCheckState()
   {
      if (AllowGetAnalysisReport())
         std::cout << "Analysis Report Check (file age) on file open enabled\n";
      else
         std::cout << "Analysis Report Check (file age) on file open disabled\n";
      return;
   }

   /// @brief Add a path for MIDAS files at the front of the list
   /// @param absolute_path 
   ///
   /// This is the path that will be searched first
   void AddPriorityMIDASFilePath(const std::string& absolute_path)
   {
      rootUtils::internal::gMIDASFilePaths.emplace_front(absolute_path);
   }

   /// @brief Add a path for MIDAS files at the back  of the list
   /// @param absolute_path 
   /// 
   /// This is the path that will be searched last
   void AddBackupMIDASFilePath(const std::string& absolute_path)
   {
      rootUtils::internal::gMIDASFilePaths.emplace_back(absolute_path);
   }

   std::vector<std::string> GetListOfPossibleMIDASFilePaths()
   {
      std::vector<std::string> paths;
      for (const std::string& p: rootUtils::internal::gMIDASFilePaths)
         if (!p.empty()) {
            if (p.back()!= '/') paths.emplace_back(p+"/");
            else paths.emplace_back(p);
         }
      if (getenv("AGMIDASDATA")) {
         std::string agmidasdata = std::string(getenv("AGMIDASDATA"));
         if (agmidasdata.back() != '/') agmidasdata += "/";
         bool included = false;
         for (const std::string& p: paths) {
            if (p.c_str() == agmidasdata) included = true;
         }
         if (!included) paths.emplace_back(agmidasdata);
      }
      if (getenv("A2MIDASDATA")) {
         std::string a2midasdata = std::string(getenv("A2MIDASDATA"));
         if (a2midasdata.back() != '/') a2midasdata += "/";
         bool included = false;
         for (const std::string& p: paths) {
            if (p.c_str() == a2midasdata) included = true;
         }
         if (!included) paths.emplace_back(a2midasdata);
      }
      return paths;
   }

   /// @brief Add a path for ROOT files at the front of the list
   /// @param absolute_path 
   ///
   /// This is the path that will be searched first
   void AddPriorityROOTFilePath(const std::string& absolute_path)
   {
      rootUtils::internal::gROOTFilePaths.emplace_front(absolute_path);
   }

   /// @brief Add a path for ROOT files at the back  of the list
   /// @param absolute_path 
   /// 
   /// This is the path that will be searched last
   void AddBackupROOTFilePath(const std::string& absolute_path)
   {
      rootUtils::internal::gROOTFilePaths.emplace_back(absolute_path);
   }

   std::vector<std::string> GetListOfPossibleROOTFilePaths()
   {
      std::vector<std::string> paths;
      for (const std::string& p: rootUtils::internal::gROOTFilePaths)
         if (!p.empty()) {
            if (p.back()!= '/') paths.emplace_back(p+"/");
            else paths.emplace_back(p);
         }
      if (getenv("AGOUTPUT")) {
         std::string agoutput = std::string(getenv("AGOUTPUT"));
         if (agoutput.back() != '/') agoutput += "/";
         bool included = false;
         for (const std::string& p: paths) {
            if (p.c_str() == agoutput) included = true;
         }
         if (!included) paths.emplace_back(agoutput);
      }
      if (getenv("EOS_MGM_URL")) {
         if (strcmp(gSystem->HostName(), "alphasvnchecker.cern.ch") != 0) {
            std::string eospath = std::string(getenv("EOS_MGM_URL"));
            eospath += "//eos/experiment/alpha/new_analyzer_data/alphaAnalysis/";
            paths.emplace_back(eospath);
         }
      }
      return paths;
   }


   /// @brief Get ROOT filename pattern defined by manalyzer
   /// 
   /// This is the standard name
   std::string GetFileNamePattern()
   {
      return rootUtils::internal::gFileNamePattern;
   }

   /// @brief Disable 'GetRunTimee' binary search through trees when loading data
   ///
   /// By default the optimised file loading is enabled
   void DisableOptimisedFileLoading()
   {
      rootUtils::internal::gOptimisedFileLoading = false;
   }

   /// @brief Enable 'GetRunTimee' binary search through trees when loading data
   ///
   /// By default the optimised file loading is enabled
   void EnableOptimisedFileLoading()
   {
      rootUtils::internal::gOptimisedFileLoading = true;
   }

   /// @brief Check if we are allowed to use 'GetRunTimee' binary search through trees when loading data
   ///
   /// By default the optimised file loading is enabled
   bool IsOptimisedFileLoadingPermitted()
   {
      return rootUtils::internal::gOptimisedFileLoading;
   }

  /// @brief Print all global config settings

   void PrintGlobalSettings() {
     PrintTimeAxisMode();
     PrintDumpMarkerDisplayMode();
     PrintDefaultAnalyzers();
     PrintAnalysisReportCheckState();
   }
}
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
