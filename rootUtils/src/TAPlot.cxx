#include "TAPlot.h"
// Default bin number
#include "RootUtilGlobals.h"

/// @brief Default constructor
/// @param zerotime Define if the time axis of the TAPlot with a 'zero' time axis
TAPlot::TAPlot(bool zerotime) : kZeroTimeAxis(zerotime)
{
   fObjectConstructionTime = TTimeStamp();
   // Set to zero for 'unset'
   fDataLoadedTime = TTimeStamp(0);
   fDrawStyle      = 0;
   fLegendDetail   = 1;
   
   fDataStale = false;
}

/// @brief Copy constructor
/// @param object
TAPlot::TAPlot(const TAPlot &object) : TObject(object), kZeroTimeAxis(object.kZeroTimeAxis)
{
   fDataStale     = object.fDataStale;
   fCanvasTitle   = object.fCanvasTitle;
   fDrawStyle     = object.fDrawStyle;
   fLegendDetail  = object.fLegendDetail;
   fTimeFactor = object.fTimeFactor;

   fTimeWindows  = object.fTimeWindows;
   fVertexEvents = object.fVertexEvents;

   for (size_t i = 0; i < object.fEjections.size(); i++) fEjections.push_back(object.fEjections[i]);

   for (size_t i = 0; i < object.fInjections.size(); i++) fInjections.push_back(object.fInjections[i]);

   for (size_t i = 0; i < object.fDumpStarts.size(); i++) fDumpStarts.push_back(object.fDumpStarts[i]);

   for (size_t i = 0; i < object.fDumpStops.size(); i++) fDumpStops.push_back(object.fDumpStops[i]);

   for (size_t i = 0; i < object.fRuns.size(); i++) fRuns.push_back(object.fRuns[i]);

   for (size_t i = 0; i < object.fFEGEM.size(); i++) fFEGEM.push_back(object.fFEGEM[i]);

   for (size_t i = 0; i < object.fFELV.size(); i++) fFELV.push_back(object.fFELV[i]);

   fObjectConstructionTime = object.fObjectConstructionTime;
   fDataLoadedTime         = object.fDataLoadedTime;
}

/// @brief Deconstructor
TAPlot::~TAPlot()
{
   fEjections.clear();
   fInjections.clear();
   fDumpStarts.clear();
   fDumpStops.clear();
   fRuns.clear();
   ClearHisto();
}

/// @brief Assignement operator
/// @param rhs
/// @return
///
/// Copies internal data containers
TAPlot &TAPlot::operator=(const TAPlot &rhs)
{
   this->fDataStale = rhs.fDataStale;

   // std::cout << "TAPlot = operator" << std::endl;
   this->fCanvasTitle   = rhs.fCanvasTitle;
   this->fDrawStyle     = rhs.fDrawStyle;
   this->fLegendDetail  = rhs.fLegendDetail;
   this->fTimeFactor = rhs.fTimeFactor;

   this->fTimeWindows  = rhs.fTimeWindows;
   this->fVertexEvents = rhs.fVertexEvents;

   for (size_t i = 0; i < rhs.fEjections.size(); i++) this->fEjections.push_back(rhs.fEjections[i]);

   for (size_t i = 0; i < rhs.fInjections.size(); i++) this->fInjections.push_back(rhs.fInjections[i]);

   for (size_t i = 0; i < rhs.fDumpStarts.size(); i++) this->fDumpStarts.push_back(rhs.fDumpStarts[i]);

   for (size_t i = 0; i < rhs.fDumpStops.size(); i++) this->fDumpStops.push_back(rhs.fDumpStops[i]);

   for (size_t i = 0; i < rhs.fRuns.size(); i++) this->fRuns.push_back(rhs.fRuns[i]);

   for (size_t i = 0; i < rhs.fFEGEM.size(); i++) this->fFEGEM.push_back(rhs.fFEGEM[i]);

   for (size_t i = 0; i < rhs.fFELV.size(); i++) this->fFELV.push_back(rhs.fFELV[i]);

   this->fObjectConstructionTime = rhs.fObjectConstructionTime;
   this->fDataLoadedTime         = rhs.fDataLoadedTime;

   return *this;
}

/// @brief += operator
/// @param rhs
/// @return
///
/// Add the contents of rhs to this TAPlot
TAPlot &TAPlot::operator+=(const TAPlot &rhs)
{
   if (this->fDataStale || rhs.fDataStale) fDataStale = true;
   // std::cout << "TAPlot += operator" << std::endl;

   // Vectors- need concating
   this->fEjections.insert(this->fEjections.end(), rhs.fEjections.begin(), rhs.fEjections.end());
   this->fInjections.insert(this->fInjections.end(), rhs.fInjections.begin(), rhs.fInjections.end());
   this->fDumpStarts.insert(this->fDumpStarts.end(), rhs.fDumpStarts.begin(), rhs.fDumpStarts.end());
   this->fDumpStops.insert(this->fDumpStops.end(), rhs.fDumpStops.begin(), rhs.fDumpStops.end());
   this->fRuns.insert(this->fRuns.end(), rhs.fRuns.begin(),
                      rhs.fRuns.end()); // check dupes - ignore copies. AddRunNumber
   this->fFEGEM.insert(this->fFEGEM.end(), rhs.fFEGEM.begin(), rhs.fFEGEM.end());
   this->fFELV.insert(this->fFELV.end(), rhs.fFELV.begin(), rhs.fFELV.end());

   // Strings
   this->fCanvasTitle += ", ";
   this->fCanvasTitle += rhs.fCanvasTitle;

   // All doubles
   this->fObjectConstructionTime = (this->fObjectConstructionTime < rhs.fObjectConstructionTime)
                                      ? this->fObjectConstructionTime
                                      : rhs.fObjectConstructionTime;
   this->fDataLoadedTime = (this->fDataLoadedTime > rhs.fDataLoadedTime) ? this->fDataLoadedTime : rhs.fDataLoadedTime;
   this->fTimeFactor     = (this->fTimeFactor < rhs.fTimeFactor) ? this->fTimeFactor : rhs.fTimeFactor;

   this->fTimeWindows += rhs.fTimeWindows;
   this->fVertexEvents += rhs.fVertexEvents;

   return *this;
}

/// @brief Print detailed summary of container and its contents
void TAPlot::PrintFull()
{
   std::cout << "===========================" << std::endl;
   std::cout << "Printing TAPlot located at " << this << std::endl;
   std::cout << "===========================" << std::endl;
   std::cout << "Title is " << fCanvasTitle << std::endl;

   for (size_t i = 0; i < fVertexEvents.size(); i++) {
      std::cout << fVertexEvents.RunNumber(i) << std::endl;
   }
   for (size_t i = 0; i < fVertexEvents.size(); i++) {
      std::cout << fVertexEvents.EventNo(i) << std::endl;
   }

   std::cout << "===========================" << std::endl;
   std::cout << std::endl << std::endl << std::endl;
}

/// @brief Print summary of container
///
/// Override TObject Print
void TAPlot::Print() const
{
   std::cout << "TAPlot Summary" << std::endl;
   // FillHisto();
   std::cout << "" << std::endl << "Run(s): ";
   for (size_t i = 0; i < fRuns.size(); i++) {
      if (i > 1) std::cout << ", ";
      std::cout << fRuns[i];
   }
   std::cout << std::endl;

   // Loop over TObj array and print it?
   for (int i = 0; i < fHistos.GetEntries(); i++) {
      TH1D *histogram = dynamic_cast<TH1D *>(fHistos.At(i));
      if (histogram) {
         std::cout << histogram->GetTitle() << "\t" << histogram->Integral() << std::endl;
      }
   }
}

/// @brief Add feGEM bank name to track and to load into container at LoadData()
/// @param gemchannel 
void TAPlot::SetGEMChannel(const TAPlotFEGEMData &gemchannel)
{
   fDataStale = true;
   for (const auto &obj : fFEGEM) {
      if (obj == gemchannel) {
         std::cout << "GEM Channel " << gemchannel.GetVariable() << "] already registered" << std::endl;
         return;
      }
   }
   fFEGEM.emplace_back(gemchannel);
   fFEGEM.back().clear();
}

/// @brief Add feGEM bank name to track and to load into container at LoadData()
/// @param name 
/// @param arrayEntry 
/// @param title 
void TAPlot::SetGEMChannel(const std::string &name, int arrayEntry, std::string title)
{
   if (title.empty()) title = name;
   return SetGEMChannel(TAPlotFEGEMData(name, title, arrayEntry));
}


/// @brief Add feGEM bank name to track and to load into container at LoadData()
/// @param category 
/// @param varName 
/// @param arrayEntry 
/// @param title 
void TAPlot::SetGEMChannel(const std::string &category, const std::string &varName, int arrayEntry, std::string title)
{
   std::string name = TAPlotFEGEMData::CombinedName(category, varName);
   if (title.empty()) title = name;
   return SetGEMChannel(name, arrayEntry, title);
}

/// @brief Add LabVIEW bank name to track and to load into container at LoadData()
/// @param lvchannel 
void TAPlot::SetLVChannel(const TAPlotFELabVIEWData &lvchannel)
{
   fDataStale = true;

   for (auto &obj : fFELV) {
      if (obj == lvchannel) {
         std::cout << "LV Channel " << lvchannel.GetVariable() << "] already registered" << std::endl;
         return;
      }
   }
   fFELV.emplace_back(lvchannel);
   fFELV.back().clear();
}

/// @brief Add LabVIEW bank name to track and to load into container at LoadData()
/// @param name 
/// @param arrayEntry 
/// @param title 
void TAPlot::SetLVChannel(const std::string &name, int arrayEntry, std::string title)
{
   if (title.empty()) title = name;
   return SetLVChannel(TAPlotFELabVIEWData(name, title, arrayEntry));
}

/// @brief Get the plots from the feGEM data stored in this container
/// @return 
std::pair<TLegend *, TMultiGraph *> TAPlot::GetGEMGraphs()
{
   TMultiGraph *feGEMMG = NULL; // Technically feGEMMG is not right but i think this is okay too?
   TLegend     *legend  = NULL;
   if (fFELV.size() == 0) return {legend, feGEMMG};
   feGEMMG = new TMultiGraph();
   legend  = new TLegend(0.1, 0.7, 0.48, 0.9);
   // For each unique variable being logged
   int colourOffset = 0;
   for (auto &obj : fFEGEM) {
      std::map<std::string, TGraph *> uniqueLabels;
      const std::vector<int>          kUniqueRuns = GetArrayOfRuns();
      for (size_t i = 0; i < fTimeWindows.size(); i++) {
         size_t colourID = 0;
         for (; colourID < kUniqueRuns.size(); colourID++) {
            if (fTimeWindows.RunNumber(i) == kUniqueRuns.at(colourID)) break;
         }
         TGraph *graph = obj.BuildGraph(i, kZeroTimeAxis);
         graph->SetLineColor(GetColour(colourID + colourOffset));
         graph->SetMarkerColor(GetColour(colourID + colourOffset));
         uniqueLabels[obj.GetVariable()] = graph;
         // if (i==0)
         //    legend->AddEntry(graph,f.GetVariable().c_str());
         if (graph->GetN()) feGEMMG->Add(graph);
      }
      for (auto &label : uniqueLabels) {
         legend->AddEntry(label.second, label.first.c_str());
      }
      colourOffset++;
   }
   return {legend, feGEMMG};
}

/// @brief Get the plots from the LabVIEW data stored in this container
/// @return 
std::pair<TLegend *, TMultiGraph *> TAPlot::GetLVGraphs()
{
   TMultiGraph *feLVMG = NULL;
   TLegend     *legend = NULL;
   if (fFELV.size() == 0) return {legend, feLVMG};
   feLVMG = new TMultiGraph();
   legend = new TLegend(0.1, 0.7, 0.48, 0.9);
   // For each unique variable being logged
   int colourOffset = 0;
   for (auto &obj : fFELV) {
      std::map<std::string, TGraph *> uniqueLabels;

      const std::vector<int> uniqueRuns = GetArrayOfRuns();
      for (size_t i = 0; i < fTimeWindows.size(); i++) {
         size_t colourID = 0;
         for (; colourID < uniqueRuns.size(); colourID++) {
            if (fTimeWindows.RunNumber(i) == uniqueRuns.at(colourID)) break;
         }
         TGraph *graph = obj.BuildGraph(i, kZeroTimeAxis);
         graph->SetLineColor(GetColour(colourID + colourOffset));
         graph->SetMarkerColor(GetColour(colourID + colourOffset));
         uniqueLabels[obj.GetVariable()] = graph;

         // if (i==0)
         //    legend->AddEntry(graph,f.GetVariable().c_str());
         // Add the graph only if there is data in it
         if (graph->GetN()) feLVMG->Add(graph);
      }
      for (auto &label : uniqueLabels) {
         legend->AddEntry(label.second, label.first.c_str());
      }
      colourOffset++;
   }
   return {legend, feLVMG};
}

/// @brief Get the estimated time it took to load data into TAPlot in seconds
/// @return
///
/// The estimate is calcuated from object creation time to LoadData() finish time, so can include user time if being
/// used interactively
double TAPlot::GetApproximateProcessingTime()
{
   if (fDataLoadedTime == TTimeStamp(0)) LoadingDataLoadingDone();
   return fDataLoadedTime.AsDouble() - fObjectConstructionTime.AsDouble();
}

/// @brief Get the number of events that pass a single cut
/// @param Cuts
/// @return
int TAPlot::GetNPassedType(const int Cuts) const
{
   return GetNPassedType(std::to_string(Cuts));
}

/// @brief Get the number of cuts that pass 'Cuts'
/// @param Cuts
/// @return
/// Cuts can use a combination of cuts using basic logic statements, eg "1&3|5", (cut one AND cut three) OR cut 5. There
/// is no support for brackets yet
int TAPlot::GetNPassedType(const std::string &Cuts) const
{
   int n = 0;
   // for (auto& event: VertexEvents)
   // const TAPlotVertexEvents* event = GetVertexEvents();
   TAPlotCutsMode m(Cuts);
   for (size_t i = 0; i < fVertexEvents.size(); i++) {
      if (m.ApplyCuts(fVertexEvents.CutsResults(i))) n++;
   }
   return n;
}

/// @brief Get the number of vertex events (in fVertexEvents) of type kType
/// @param kType
/// @return
int TAPlot::GetNVertexType(const int kType) const
{
   int n = 0;
   // for (auto& event: VertexEvents)
   // const TAPlotVertexEvents* event = GetVertexEvents();
   for (size_t i = 0; i < fVertexEvents.size(); i++) {
      if ((fVertexEvents.VertexStatus(i)>0) && kType) n++;
   }
   return n;
}

/// @brief Get list of run number as string
/// @return
TString TAPlot::GetListOfRuns()
{
   TString runsString = "";
   std::sort(fRuns.begin(), fRuns.end());
   for (size_t i = 0; i < fRuns.size(); i++) {
      // std::cout <<"Run: "<<Runs[i] <<std::endl;
      if (i > 0) runsString += ",";
      runsString += fRuns[i];
   }
   return runsString;
}

/// @brief Track unique run numbers in this container (dont add duplicates)
/// @param runNumber
void TAPlot::AddRunNumber(int runNumber)
{
   for (const auto &number : fRuns) {
      if (number == runNumber) {
         return;
      }
   }
   fRuns.push_back(runNumber);
}

/// @brief Add multiple timegates to this container with an offset of zeroTime
/// @param runNumber 
/// @param minTime 
/// @param maxTime 
/// @param zeroTime  The time of Zero on the x axis of plots
void TAPlot::AddTimeGates(int runNumber, std::vector<double> minTime, std::vector<double> maxTime,
                          std::vector<double> zeroTime)
{
   fDataStale = true;

   AddRunNumber(runNumber);

   assert(minTime.size() == maxTime.size());
   assert(minTime.size() == zeroTime.size());

   for (size_t i = 0; i < minTime.size(); i++) {
      AddTimeGate(runNumber, minTime[i], maxTime[i], zeroTime[i]);
   }
   return;
}

/// @brief Add Multiple time gates of a single run to this container
/// @param runNumber 
/// @param minTime 
/// @param maxTime 
void TAPlot::AddTimeGates(int runNumber, std::vector<double> minTime, std::vector<double> maxTime)
{
   fDataStale = true;
   std::vector<double> zeroTime;
   zeroTime.reserve(minTime.size());
   for (auto &times : minTime) zeroTime.push_back(times);
   return AddTimeGates(runNumber, minTime, maxTime, zeroTime);
}

/// @brief Add time gate to this container
/// @param runNumber 
/// @param minTime 
/// @param maxTime 
/// It is slightly faster to call AddTimeGates than this function
void TAPlot::AddTimeGate(const int runNumber, const double minTime, const double maxTime)
{
   fDataStale = true;
   return AddTimeGate(runNumber, minTime, maxTime, minTime);
}

/// @brief Add a single vertex event to this container
/// @param runNumber 
/// @param eventNo 
/// @param cutsResult 
/// @param vertexStatus 
/// @param x 
/// @param y 
/// @param z 
/// @param t 
/// @param eventTime 
/// @param runTime 
/// @param numHelices 
/// @param numTracks 
/// @param mva
void TAPlot::AddVertexEvent(int runNumber, int eventNo, const std::vector<bool> cutsResult, int vertexStatus, double x,
                            double y, double z, double t, double eventTime, double runTime, int numHelices,
                            int numTracks, double mva)
{
   return fVertexEvents.AddVertexEvent(runNumber, eventNo, cutsResult, vertexStatus, x, y, z, t, eventTime, runTime,
                                       numHelices, numTracks, mva);
}

/// @brief  Load feGEM data into this container
/// @tparam T 
/// @param runNumber 
/// @param gemData 
/// @param gemReader 
/// @param name 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
template <typename T>
void TAPlot::LoadFEGEMData(int runNumber, TAPlotFEGEMData &gemData, TTreeReaderPointer gemReader, const char *name,
                           double firstTime, double lastTime, int verbose)
{
   TTreeReaderValue<TStoreGEMData<T>> gemEvent(*gemReader, name);
   if (verbose) std::cout << "Loading feGEM data: " << name << std::endl;
   // I assume that file IO is the slowest part of this function...
   // so get multiple channels and multiple time windows in one pass
   while (gemReader->Next()) {
      const double runTime = gemEvent->GetRunTime();
      // A rough cut on the time window is very fast...
      if (runTime < firstTime) continue;
      if (runTime > lastTime) break;
      gemData.AddGEMEvent(runNumber, *gemEvent, fTimeWindows);
   }
   return;
}

/// @brief Load feGEM data into this container
/// @param runNumber 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
void TAPlot::LoadFEGEMData(int runNumber, double firstTime, double lastTime, int verbose)
{
   for (auto &obj : fFEGEM) {
      TTreeReaderPointer feGEMReader = Get_feGEM_Tree(runNumber, obj.GetVariableName());
      TTree       *tree        = feGEMReader->GetTree();
      if (!tree) {
         std::cout << "Warning: " << obj.GetVariable() << " (" << obj.GetVariableName() << ") not found for run "
                   << runNumber << std::endl;
         continue;
      }
      if (tree->GetBranchStatus("TStoreGEMData<double>"))
         LoadFEGEMData<double>(runNumber, obj, feGEMReader, "TStoreGEMData<double>", firstTime, lastTime, verbose);
      else if (tree->GetBranchStatus("TStoreGEMData<float>"))
         LoadFEGEMData<float>(runNumber, obj, feGEMReader, "TStoreGEMData<float>", firstTime, lastTime, verbose);
      else if (tree->GetBranchStatus("TStoreGEMData<bool>"))
         LoadFEGEMData<bool>(runNumber, obj, feGEMReader, "TStoreGEMData<bool>", firstTime, lastTime, verbose);
      else if (tree->GetBranchStatus("TStoreGEMData<int32_t>"))
         LoadFEGEMData<int32_t>(runNumber, obj, feGEMReader, "TStoreGEMData<int32_t>", firstTime, lastTime, verbose);
      else if (tree->GetBranchStatus("TStoreGEMData<uint32_t>"))
         LoadFEGEMData<uint32_t>(runNumber, obj, feGEMReader, "TStoreGEMData<uint32_t>", firstTime, lastTime, verbose);
      else if (tree->GetBranchStatus("TStoreGEMData<uint16_t>"))
         LoadFEGEMData<uint16_t>(runNumber, obj, feGEMReader, "TStoreGEMData<uint16_t>", firstTime, lastTime, verbose);
      else if (tree->GetBranchStatus("TStoreGEMData<char>"))
         LoadFEGEMData<char>(runNumber, obj, feGEMReader, "TStoreGEMData<char>", firstTime, lastTime, verbose);
      else
         std::cout << "Warning unable to find TStoreGEMData type" << std::endl;
   }
}

/// @brief Load feLabVIEW data into this container
/// @param runNumber 
/// @param labviewData 
/// @param labviewReader 
/// @param name 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
void TAPlot::LoadFELVData(int runNumber, TAPlotFELabVIEWData &labviewData, TTreeReaderPointer labviewReader, const char *name,
                          double firstTime, double lastTime, int verbose)
{
   TTreeReaderValue<TStoreLabVIEWEvent> labviewEvent(*labviewReader, name);
   if (verbose > 1) std::cout << "Loading feLabVIEW " << name << "\n";
   // I assume that file IO is the slowest part of this function...
   // so get multiple channels and multiple time windows in one pass
   while (labviewReader->Next()) {
      const double runTime = labviewEvent->GetRunTime();
      // A rough cut on the time window is very fast...
      if (runTime < firstTime) continue;
      if (runTime > lastTime) break;
      labviewData.AddLVEvent(runNumber, *labviewEvent, fTimeWindows);
   }
   return;
}

/// @brief Load feLabVIEW data into this container
/// @param runNumber 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
void TAPlot::LoadFELVData(int runNumber, double firstTime, double lastTime, int verbose)
{
   // For each unique variable being logged
   for (auto &obj : fFELV) {
      TTreeReaderPointer labviewReader = Get_feLV_Tree(runNumber, obj.GetVariableName());
      TTree       *tree          = labviewReader->GetTree();
      if (!tree) {
         std::cout << "Warning: " << obj.GetVariable() << " (" << obj.GetVariableName() << ") not found for run "
                   << runNumber << std::endl;
         continue;
      }
      if (tree->GetBranchStatus("TStoreLabVIEWEvent"))
         LoadFELVData(runNumber, obj, labviewReader, "TStoreLabVIEWEvent", firstTime, lastTime, verbose);
      else
         std::cout << "Warning unable to find TStoreLVData type" << std::endl;
   }
}

template<typename T>
int TAPlot::ClearContainer(T& container, const std::string name)
{
   if (container.empty())
      return 0;
   const int size = container.size();
   container.clear();
   std::cerr << "TAPlot::ClearContainers() (" << fCanvasTitle << ") resetting " << name << " data\n";
   return size;
}

/// @brief Reset data held in the TAPlot ( but not the TTimeWindows or fRuns )
void TAPlot::ClearContainers()
{
   ClearContainer(fEjections,"fEjections");
   ClearContainer(fInjections,"fInjections");
   ClearContainer(fDumpStarts,"fDumpStarts");
   ClearContainer(fDumpStops,"fDumpStops");
   for (TAPlotFEGEMData& gem: fFEGEM)   // Data from feGEM
   {
      ClearContainer(gem,gem.GetVariableName());
   }
   for (TAPlotFELabVIEWData& lv: fFELV) // Data from feLabVIEW
   {
      ClearContainer(lv,lv.GetVariableName());
   }
   ClearContainer(fVertexEvents,"fVertexEvents");
}

/// @brief Load data into this data container (should only be called once)
/// @param verbose 
void TAPlot::LoadData(int verbose)
{
   // Check if data is fresh (it wont be fresh if a user added a time window)
   if (!fDataStale) return;

   ClearContainers();
   for (size_t i = 0; i < fRuns.size(); i++) {
      double lastTime  = 0;
      double firstTime = 1E99;
      int    runNumber = fRuns[i];
      // Calculate our list time... so we can stop early

      for (size_t i = 0; i < GetTimeWindows().size(); i++) {
         if (GetTimeWindows().RunNumber(i) == runNumber) {
            const double max = GetTimeWindows().MaxTime(i);
            const double min = GetTimeWindows().MinTime(i);
            if (max < 0) lastTime = 1E99;
            if (lastTime < max) lastTime = max;
            if (firstTime > min) firstTime = min;
         }
      }
      if (verbose > 1) {
         std::cout << "Loading data from run " << runNumber << " from t=" << firstTime << " to t=" << lastTime << "\n";
      }
      LoadFEGEMData(runNumber, firstTime, lastTime, verbose);
      LoadFELVData(runNumber, firstTime, lastTime, verbose);
      LoadRun(runNumber, firstTime, lastTime, verbose);
   }
   LoadingDataLoadingDone(); // Sets fDataStale as false (ie, all data in TAPlot container is up to date)
   return;
}

/// @brief Add counts of value x to TH1D histogram of keyname
/// @param keyname 
/// @param x 
/// @param counts 
void TAPlot::FillHistogram(const char *keyname, double x, int counts)
{
   if (fHistoPositions.count(keyname)) ((TH1D *)fHistos.At(fHistoPositions.at(keyname)))->Fill(x, counts);
}

/// @brief Add data to TH1D histogram of keyname
/// @param keyname 
/// @param x 
void TAPlot::FillHistogram(const char *keyname, double x)
{
   if (fHistoPositions.count(keyname)) ((TH1D *)fHistos.At(fHistoPositions.at(keyname)))->Fill(x);
}

/// @brief Add data to TH2D histogram of keyname
/// @param keyname 
/// @param x 
/// @param y 
void TAPlot::FillHistogram(const char *keyname, double x, double y)
{
   if (fHistoPositions.count(keyname)) ((TH2D *)fHistos.At(fHistoPositions.at(keyname)))->Fill(x, y);
}

/// @brief Get TH1D histogram of keyname
/// @param keyname 
/// @return 
TH1D *TAPlot::GetTH1D(const char *keyname)
{
   if (fDataStale) LoadData();
   if (fHistoPositions.count(keyname)) return (TH1D *)fHistos.At(fHistoPositions.at(keyname));
   return NULL;
}

/// @brief Get TH2D histogram of keyname
/// @param keyname 
/// @return 
TH2D *TAPlot::GetTH2D(const char *keyname)
{
   if (fDataStale) LoadData();
   if (fHistoPositions.count(keyname)) return (TH2D *)fHistos.At(fHistoPositions.at(keyname));
   return NULL;
}

/// @brief Get the peak of TH1D histograms with keys
/// @param keys 
/// @return 
double TAPlot::GetTH1DMaximum(std::vector<std::string> keys)
{
   double ymax = -1. / 0.; // -inf
   for (const std::string &key : keys) {
      TH1D *h = GetTH1D(key.c_str());
      if (!h) continue;
      double max = h->GetMaximum(); // h->GetBinContent(h->GetMaximumBin());
      if (max > ymax) ymax = max;
   }
   return ymax;
}

/// @brief Draw histogram of keyname
/// @param keyname 
/// @param settings 
void TAPlot::DrawHistogram(const char *keyname, const char *settings)
{
   if (fDataStale) LoadData();
   if (fHistoPositions.count(keyname))
      ((TH1D *)fHistos.At(fHistoPositions.at(keyname)))->Draw(settings);
   else
      std::cout << "Warning: Histogram " << keyname << " not found" << std::endl;
}

/// @brief Draw lines on histogram of keyname
/// @param legend 
/// @param keyname 
/// @return 
TLegend *TAPlot::DrawLines(TLegend *legend, const char *keyname)
{
   if (fDataStale) LoadData();
   double max = ((TH1D *)fHistos.At(fHistoPositions.at(keyname)))->GetMaximum();
   if (!legend) legend = new TLegend(1, 0.7, 0.55, .95); //, "NDC NB");
   for (UInt_t i = 0; i < fInjections.size(); i++) {
      TLine *line = new TLine(fInjections[i] * fTimeFactor, 0., fInjections[i] * fTimeFactor, max);
      line->SetLineColor(6);
      line->Draw();
      if (i == 0) legend->AddEntry(line, "AD fill", "line");
   }
   for (UInt_t i = 0; i < fEjections.size(); i++) {
      TLine *line = new TLine(fEjections[i] * fTimeFactor, 0., fEjections[i] * fTimeFactor, max);
      line->SetLineColor(7);
      line->Draw();
      if (i == 0) legend->AddEntry(line, "Beam to ALPHA", "line");
   }
   for (UInt_t i = 0; i < fDumpStarts.size(); i++) {
      if (fDumpStarts.size() > 4) continue; // Don't draw dumps if there are lots
      TLine *line = new TLine(fDumpStarts[i] * fTimeFactor, 0., fDumpStarts[i] * fTimeFactor, max);
      // line->SetLineColor(7);
      line->SetLineColorAlpha(kGreen, 0.35);
      // line->SetFillColorAlpha(kGreen,0.35);
      line->Draw();
      if (i == 0) legend->AddEntry(line, "Dump Start", "line");
   }
   for (UInt_t i = 0; i < fDumpStops.size(); i++) {
      if (fDumpStops.size() > 4) continue; // Don't draw dumps if there are lots
      TLine *line = new TLine(fDumpStops[i] * fTimeFactor, 0., fDumpStops[i] * fTimeFactor, max);
      // line->SetLineColor(7);
      line->SetLineColorAlpha(kRed, 0.35);
      line->Draw();
      if (i == 0) legend->AddEntry(line, "Dump Stop", "line");
   }
   legend->Draw();
   return legend;
}

/// @brief Add legend to histogram of keyname
/// @param legend 
/// @param message 
/// @param keyname 
/// @return 
TLegend *TAPlot::AddLegendIntegral(TLegend *legend, const char *message, const char *keyname)
{
   if (fDataStale) LoadData();
   char line[201];
   if (!fHistoPositions.count(keyname)) return legend;
   if (!legend) legend = new TLegend(1, 0.7, 0.55, .95); //, "NDC NB");
   snprintf(line, 200, message, ((TH1D *)fHistos.At(fHistoPositions.at(keyname)))->Integral());
   legend->AddEntry((TH1D *)fHistos.At(fHistoPositions.at(keyname)), line, "f");
   legend->SetFillColor(kWhite);
   legend->SetFillStyle(1001);
   legend->Draw();
   return legend;
}

/// @brief Destroy histograms in container (used if re-drawing the same data)
void TAPlot::ClearHisto() // Destroy all histograms
{
   fHistos.SetOwner(kTRUE);
   fHistos.Delete();
   fHistoPositions.clear();
}

/// @brief Write a list file of the events in this container
/// @param fileName 
/// @param append 
/// This function can be used for selecting events for MVA
void TAPlot::WriteEventList(std::string fileName, bool append, int cut)
{
   if (fDataStale) LoadData();
   // Initiate an ofstream to write to
   std::ofstream myFile;
   std::string   file = fileName + ".list";
   if (append)
      myFile.open(file, std::ios_base::app);
   else
      myFile.open(file);

   // Assert that the runNumbers and EventNos match up in terms of size.
   //assert(fVertexEvents.fRunNumbers.size() == fVertexEvents.fEventNos.size());

   size_t index          = 0;                            // Initialise at index 0
   if(cut>=0)//Use -1 to get all events, but if you use 0 or greater it will check the cut value.
   {
      while(!fVertexEvents.CutsResults(index).at(cut)) //Making sure vertex has passed the cut
      {
         index++;
      }
   }
   std::cout << "Made it here \n";
   int    currentEventNo = fVertexEvents.EventNo(index); // Set the current run number to be the one at index (0)
   std::cout << "Made it here too\n";
   int    currentRunNo   = fVertexEvents.RunNumber(index);   // Same for event no.
   std::cout << "But did we made it here?\n";

   myFile << currentRunNo << ":"
          << currentEventNo; // Print an initial statement to file, will look something like "39993:2"

   // While index is in range lets do all the checks to decide what to write.
   while (index < fVertexEvents.size() - 1) {
      index++; // Increment index since we're in a while loop not a for.
      if(cut>=0)//Use -1 to get all events, but if you use 0 or greater it will check the cut value.
      {
         if(!fVertexEvents.CutsResults(index).at(cut)) //Check vertex passes cut
         {
            continue;
         }
      }
      if (fVertexEvents.RunNumber(index) != currentRunNo) {
         // If runNumber has changed:
         myFile << "-" << currentEventNo << std::endl;    // 1. Close off current range eg: "39993:2-5"
         currentRunNo   = fVertexEvents.RunNumber(index); // Update currentrunNo and EventNo
         currentEventNo = fVertexEvents.EventNo(index);
         myFile << currentRunNo << ":" << currentEventNo; // Print initial line of new run eg: "45000:3"
      } else if (fVertexEvents.EventNo(index) == (currentEventNo + 1)) {
         // Else if runNumber is the same but the event is consecutive to the one after (ie 2-3)
         currentEventNo++; // Increment currentEventNo. This is equiv to currentEventNo =
                           // VertexEvents.EventNos.at(index) just quicker since we've already checked its consecutive.
      } else {
         // Else: Ie run number is the samer but the event number is not consecutive:
         myFile << "-" << currentEventNo << std::endl;    // Close line, eg: "39993:2-5"
         currentRunNo   = fVertexEvents.RunNumber(index); // Update run and event no.
         currentEventNo = fVertexEvents.EventNo(index);
         myFile << currentRunNo << ":" << currentEventNo; // Start new line eg: "39993:27"
      }
   }
   // Once out of the loop close what we have.
   myFile << "-" << currentEventNo << std::endl;
   // Close the file.
   myFile.close();
}

/// @brief Reapply the MVA on all TAPlotVertexEvents in this TAPlot
/// @param rfoutValue 
void TAPlot::ApplyMVA(const int MVAIndex, const double rfoutValue)
{
   fVertexEvents.ApplyMVACuts(MVAIndex, rfoutValue);
}


/// @brief Export all contents of container to CSV files 
/// @param filename
/// @param option select what data gets exported to csv
///
/// options (order doesn't matter):
/// "v" for vertex (filename.vertex.csv)
/// "t" for timewindows (filename.timewindows.csv)
void TAPlot::ExportCSV(const std::string filename, const std::string options)
{
   if (fDataStale) LoadData();
   // Save vertex plots if 'v' is listed in options
   if (options.find('v') != std::string::npos)
   {
      std::string   vertexFilename     = filename + ".vertex.csv";
      std::ofstream verts;
      verts.open(vertexFilename);
      verts << fVertexEvents.CSVTitleLine();
      for (size_t i = 0; i < fVertexEvents.size(); i++) verts << fVertexEvents.CSVLine(i);
      verts.close();
      std::cout << vertexFilename << " saved\n";
   }

   // Save timewindows if 't' is listed in options
   if (options.find('t') != std::string::npos)
   {
      std::string   timeWindowFilename = filename + ".timewindows.csv";
      std::ofstream times;
      times.open(timeWindowFilename);
      times << fTimeWindows.CSVTitleLine();
      for (size_t i = 0; i < fTimeWindows.size(); i++) times << fTimeWindows.CSVLine(i);
      times.close();
      std::cout << timeWindowFilename << " saved\n";
   }
}
