#include "TAPlotVertexEvents.h"

/// @brief Constructor
TAPlotVertexEvents::TAPlotVertexEvents()
{
}

/// @brief Deconstructor
TAPlotVertexEvents::~TAPlotVertexEvents() {}

/// Copy constructor
TAPlotVertexEvents::TAPlotVertexEvents(const TAPlotVertexEvents &vertexEvents) : TObject(vertexEvents)
{
   // Making deep copies of the vectors, we require loop method as the vectors are already initialised earlier.
   for (size_t i = 0; i < vertexEvents.fXVertex.size(); i++) {
      fRunNumbers.emplace_back(vertexEvents.fRunNumbers[i]);
      fEventNos.emplace_back(vertexEvents.fEventNos[i]);
      // TODO: Double check this copy ctor
      fCutsResults.emplace_back(vertexEvents.fCutsResults[i]);
      fVertexStatuses.emplace_back(vertexEvents.fVertexStatuses[i]);
      fXVertex.emplace_back(vertexEvents.fXVertex[i]);
      fYVertex.emplace_back(vertexEvents.fYVertex[i]);
      fZVertex.emplace_back(vertexEvents.fZVertex[i]);
      fTimes.emplace_back(vertexEvents.fTimes[i]);
      fEventTimes.emplace_back(vertexEvents.fEventTimes[i]);
      fRunTimes.emplace_back(vertexEvents.fRunTimes[i]);
      fNumHelices.emplace_back(vertexEvents.fNumHelices[i]);
      fNumTracks.emplace_back(vertexEvents.fNumTracks[i]);
   }
}

/// Assignment operator
TAPlotVertexEvents &TAPlotVertexEvents::operator=(const TAPlotVertexEvents &rhs)
{
   // Making deep copies of the vectors, we require loop method as the vectors are already initialised earlier.
   for (size_t i = 0; i < rhs.fXVertex.size(); i++) {
      this->fRunNumbers.emplace_back(rhs.fRunNumbers[i]);
      this->fEventNos.emplace_back(rhs.fEventNos[i]);
      // TODO, double check this copy ctor
      this->fCutsResults.emplace_back(rhs.fCutsResults[i]);
      this->fVertexStatuses.emplace_back(rhs.fVertexStatuses[i]);
      this->fXVertex.emplace_back(rhs.fXVertex[i]);
      this->fYVertex.emplace_back(rhs.fYVertex[i]);
      this->fZVertex.emplace_back(rhs.fZVertex[i]);
      this->fTimes.emplace_back(rhs.fTimes[i]);
      this->fEventTimes.emplace_back(rhs.fEventTimes[i]);
      this->fRunTimes.emplace_back(rhs.fRunTimes[i]);
      this->fNumHelices.emplace_back(rhs.fNumHelices[i]);
      this->fNumTracks.emplace_back(rhs.fNumTracks[i]);
   }
   return *this;
}

/// =+ Operator
TAPlotVertexEvents TAPlotVertexEvents::operator+=(const TAPlotVertexEvents &rhs)
{
   // std::cout << "TAVertexEvents += operator" << std::endl;
   this->fRunNumbers.insert(this->fRunNumbers.end(), rhs.fRunNumbers.begin(), rhs.fRunNumbers.end());
   this->fEventNos.insert(this->fEventNos.end(), rhs.fEventNos.begin(), rhs.fEventNos.end());
   this->fCutsResults.insert(this->fCutsResults.end(), rhs.fCutsResults.begin(), rhs.fCutsResults.end());
   this->fVertexStatuses.insert(this->fVertexStatuses.end(), rhs.fVertexStatuses.begin(), rhs.fVertexStatuses.end());
   this->fXVertex.insert(this->fXVertex.end(), rhs.fXVertex.begin(), rhs.fXVertex.end());
   this->fYVertex.insert(this->fYVertex.end(), rhs.fYVertex.begin(), rhs.fYVertex.end());
   this->fZVertex.insert(this->fZVertex.end(), rhs.fZVertex.begin(), rhs.fZVertex.end());
   this->fTimes.insert(this->fTimes.end(), rhs.fTimes.begin(), rhs.fTimes.end());
   this->fEventTimes.insert(this->fEventTimes.end(), rhs.fEventTimes.begin(), rhs.fEventTimes.end());
   this->fRunTimes.insert(this->fRunTimes.end(), rhs.fRunTimes.begin(), rhs.fRunTimes.end());
   this->fNumHelices.insert(this->fNumHelices.end(), rhs.fNumHelices.begin(), rhs.fNumHelices.end());
   this->fNumTracks.insert(this->fNumTracks.end(), rhs.fNumTracks.begin(), rhs.fNumTracks.end());
   return *this;
}

/// @brief Add vertex data function
/// @param runNumber 
/// @param eventNo 
/// @param cutsResult 
/// @param vertexStatus 
/// @param x 
/// @param y 
/// @param z 
/// @param t 
/// @param eventTime 
/// @param runTime 
/// @param numHelices 
/// @param numTracks 
/// @param mva
void TAPlotVertexEvents::AddVertexEvent(const int runNumber, const int eventNo, const std::vector<bool> &cutsResult,
                                        const int vertexStatus, const double x, const double y, const double z,
                                        const double t, const double eventTime, const double runTime,
                                        const int numHelices, const int numTracks, const double mva)
{
   fRunNumbers.emplace_back(runNumber);
   fEventNos.emplace_back(eventNo);
   fCutsResults.emplace_back();
   fCutsResults.back().push_back(cutsResult);
   fVertexStatuses.emplace_back(vertexStatus);
   fXVertex.emplace_back(x);
   fYVertex.emplace_back(y);
   fZVertex.emplace_back(z);
   fTimes.emplace_back(t);
   fEventTimes.emplace_back(eventTime);
   fRunTimes.emplace_back(runTime);
   fNumHelices.emplace_back(numHelices);
   fNumTracks.emplace_back(numTracks);
   fMVA.emplace_back(mva);
}

/// @brief Return the title line for the CSVLine() function
/// @return 
std::string TAPlotVertexEvents::CSVTitleLine() const
{
   std::string title = std::string("Run Number,") + "Event Number," + "Plot Time (Time axis of TAPlot)," +
                       "Detector Time (detectors internal clock)," + "OfficialTime (run time),";
   if (fCutsResults.size()) title += fCutsResults.front().title();
   title += std::string("Vertex Status,") + "X," + "Y," + "Z," + "Number of Helices," + "Number of Tracks," + "Classifier Output\n";
   return title;
}

/// @brief Return vertex information as a std::string line
/// @param i 
/// @return 
///
/// Complemented by the CSVTitleLine() function line
std::string TAPlotVertexEvents::CSVLine(const size_t i) const
{
   // This is a little fugly
   std::string line = std::to_string(fRunNumbers.at(i)) + "," + std::to_string(fEventNos.at(i)) + "," +
                      std::to_string(fTimes.at(i)) + "," + std::to_string(fEventTimes.at(i)) + "," +
                      std::to_string(fRunTimes.at(i)) + "," + fCutsResults.at(i).line() +
                      std::to_string(fVertexStatuses.at(i)) + "," + std::to_string(fXVertex.at(i)) + "," +
                      std::to_string(fYVertex.at(i)) + "," + std::to_string(fZVertex.at(i)) + "," +
                      std::to_string(fNumHelices.at(i)) + "," + std::to_string(fNumTracks.at(i)) + "," +
                      std::to_string(fMVA.at(i)) + "\n";
   return line;
}

/// @brief Get the number of events in the container
/// @return 
size_t TAPlotVertexEvents::size() const
{
   return fRunNumbers.size();
}

bool TAPlotVertexEvents::empty() const
{
   return fRunNumbers.empty();
}

void TAPlotVertexEvents::clear()
{
   fRunNumbers.clear();     // Run Number of event
   fEventNos.clear();       // Event number
   fCutsResults.clear();    // TAPlotVertexEventsPassedCuts vector of pass cut event
   fVertexStatuses.clear(); // Reconstructed vertex status
   fXVertex.clear();        // X position of reconstructed vertex
   fYVertex.clear();        // Y position of reconstructed vertex
   fZVertex.clear();        // Z position of reconstructed vertex
   fTimes.clear();          // Plot time (based off official time)
   fEventTimes.clear();     // TPC time stamp
   fRunTimes.clear();       // Official Time
   fNumHelices.clear();     // helices used for vertexing
   fNumTracks.clear();      // reconstructed (good) helices
}

/// @brief Count the number of events that pass cut number 'cut'
/// @param cut 
/// @return 
int TAPlotVertexEvents::CountPassedCuts(const int cut) const
{
   int sum = 0;

   for (const TAPlotVertexEventsPassedCuts &p : fCutsResults) {
      if (p.at(cut)) sum++;
   }
   return sum;
}
