#if BUILD_A2
#if HAVE_MIDAS
#include "midas.h"
#endif
#include "TA2PlotMicrowave.h"

TA2PlotMicrowave::TA2PlotMicrowave(bool zeroTime) : TA2PlotWithGEM(zeroTime) {}

TA2PlotMicrowave::TA2PlotMicrowave(double zMin, double zMax, bool zeroTime) : TA2PlotWithGEM(zMin, zMax, zeroTime) {}

TA2PlotMicrowave::TA2PlotMicrowave(const TA2PlotMicrowave &object) : TA2PlotWithGEM(object) {}

TA2PlotMicrowave::~TA2PlotMicrowave() {}

void TA2PlotMicrowave::LoadDataMicrowave(int verbose) {
   //for (int i=0; i<N_UWAVE_FREQS; i++) {
   //   AddGEMVariable("MicrowaveSynth\\Frequencies",i,Form("Freq_%d",i));
   //   AddGEMVariable("MicrowaveSynth\\Power",i,Form("Power_%d",i));
   //}
   TA2PlotWithGEM::LoadDataWithGEM(verbose);
   AssignMicrowaveFrequencies();
}

void TA2PlotMicrowave::FigureOutSweepTimes() {

   for (size_t i=0; i<GetTimeWindows().size(); i++) {

      const int run_number = GetTimeWindows().RunNumber(i);
      if (fUwaveStarts.count(run_number)>0) continue; // Microwave sweep times for this run number already figured out.

      TSISChannels chans(run_number);
      TSISChannel mic_start_chan = chans.GetChannel("MIC_SYNTH_STEP_START");
      TSISChannel mic_stop_chan = chans.GetChannel("MIC_SYNTH_STEP_STOP");

      std::vector<std::deque<double>> starts_in_this_window;
      std::vector<std::deque<double>> stops_in_this_window;

      const std::vector<TA2Spill>& microwave_dumps = Get_A2_Spills(run_number,{"Microwave Dump"},{-1});
      for (const TA2Spill& dump: microwave_dumps) {

         std::vector<std::pair<double,int>> mic_starts_full = GetSISTimeAndCounts( run_number, mic_start_chan, {dump.GetStartTime()}, {dump.GetStopTime()});
         std::vector<std::pair<double,int>> mic_stops_full = GetSISTimeAndCounts( run_number, mic_stop_chan, {dump.GetStartTime()}, {dump.GetStopTime()});
         std::deque<double> mic_starts;
         std::deque<double> mic_stops;
         for (size_t i = 0; i < mic_starts_full.size(); i++) {
            mic_starts.push_back(mic_starts_full.at(i).first);
            if (i < mic_stops_full.size())
               mic_stops.push_back(mic_stops_full.at(i).first);
            else
               // Add artifical stop at the end of the dump window so we can more easily pair up events
               mic_stops.push_back(dump.GetStopTime());
         }
         mic_stops.push_back(dump.GetStopTime());
         mic_stops.pop_front();
         starts_in_this_window.push_back(mic_starts);
         stops_in_this_window.push_back(mic_stops);
      }
      fUwaveStarts[run_number] = starts_in_this_window;
      fUwaveStops[run_number] = stops_in_this_window;
   }
}

void TA2PlotMicrowave::FigureOutMicrowaveFrequencies() {
   if (fUwaveStarts.size()==0) FigureOutSweepTimes();

   for (size_t i=0; i<GetTimeWindows().size(); i++) {

      const int run_number = GetTimeWindows().RunNumber(i);
      if (fUwaveFreqs.count(run_number)>0) continue; // Microwave frequencies for this run number already figured out.

      // Worlds rubbishest initializer for fUwaveFreqs and fUwavePowers
      std::vector<std::deque<double>> vec1;
      std::vector<std::deque<double>> vec2;
      for (size_t j=0; j<fUwaveStarts.at(run_number).size(); j++) {
         std::deque<double> deq1;
         vec1.push_back(deq1);
         std::deque<double> deq2;
         vec2.push_back(deq2);
      }
      fUwaveFreqs[run_number] = vec1;
      fUwavePowers[run_number] = vec2;

      // Look for microwave synth data send up until the start of the last sweep
      if (fUwaveStarts.at(run_number).at(fUwaveStarts.at(run_number).size()-1).size()==0) return;
      double last_start = fUwaveStarts.at(run_number).at(fUwaveStarts.at(run_number).size()-1).at(0);

      std::vector<std::pair<double,std::vector<double>>> all_freqs = GetFEGData(run_number, "MicrowaveSynth\\Frequencies", 0, last_start);
      std::vector<std::pair<double,std::vector<double>>> all_pows = GetFEGData(run_number, "MicrowaveSynth\\Power", 0, last_start);
      for (int f=all_freqs.size()-1; f>=0; f--) { // Iterate from last to first
         for (size_t j=0; j<fUwaveStarts.at(run_number).size(); j++) {
            if (fUwaveFreqs.at(run_number).at(j).size()>0) continue; // This sweep already has frequencies
            if (all_freqs.at(f).first > fUwaveStarts.at(run_number).at(j).at(0)) continue; // Frequency data recorded after start of sweep - too late to be correct
            // This is the right frequency sweep data (f) for these sweep times (j)
            for (size_t i_freq=1; i_freq<all_freqs.at(f).second.size()+1 and i_freq<fUwaveStarts.at(run_number).at(j).size()+1; i_freq++) { // Off by one. Also don't go past the number of sweep times.
               double freq = all_freqs.at(f).second.at(i_freq);
               double pow = all_pows.at(f).second.at(i_freq);
               if (freq==0 or pow==0) continue;
               fUwaveFreqs.at(run_number).at(j).push_back(freq);
               fUwavePowers.at(run_number).at(j).push_back(pow);
            }
         }
      }
   }
}

void TA2PlotMicrowave::AssignMicrowaveFrequencies() {
   if (fUwaveFreqs.size()==0) FigureOutMicrowaveFrequencies();

   for (size_t i=0; i<fVertexEvents.size(); i++) {
      int run_number = fVertexEvents.RunNumber(i);
      bool assigned=false;
      if (fUwaveStarts.count(run_number)<=0) {
         fFrequencies.push_back(0);
         fPowers.push_back(0);
         continue;
      }
      for (size_t i_sweep=0; i_sweep<fUwaveStarts.at(run_number).size(); i_sweep++) {
         for (size_t i_window=0; i_window<fUwaveStarts.at(run_number).at(i_sweep).size(); i_window++) {
            double window_start = fUwaveStarts.at(run_number).at(i_sweep).at(i_window);
            double window_stop = fUwaveStops.at(run_number).at(i_sweep).at(i_window);
            if (fVertexEvents.RunTime(i)<window_start or fVertexEvents.RunTime(i)>window_stop) continue; // This is very inefficient but lets get it working
            // This is the window
            double freq = fUwaveFreqs.at(run_number).at(i_sweep).at(i_window);
            double pow = fUwavePowers.at(run_number).at(i_sweep).at(i_window);
            fFrequencies.push_back(freq);
            fPowers.push_back(pow);
            assigned = true;
            break;
         }
         if (assigned) break;
      }
      if (!assigned) {
         // Not in a window
         fFrequencies.push_back(0);
         fPowers.push_back(0);
      }
   }
}

void TA2PlotMicrowave::ExportCSV(const std::string filename, const std::string options)
{
   if (IsDataStale()) LoadDataMicrowave();
   // Save vertex plots if 'v' is listed in options
   if (options.find('v') != std::string::npos)
   {
      std::string   vertexFilename     = filename + ".vertex.csv";
      std::ofstream verts;
      verts.open(vertexFilename);
      verts << fVertexEvents.CSVTitleLine().substr(0,fVertexEvents.CSVTitleLine().size()-2); // Without the newline character
      verts << "," << "Microwave Frequency," << "Microwave Power\n";
      int normal_precision = verts.precision();
      for (size_t i = 0; i < fVertexEvents.size(); i++) {
         std::string line = fVertexEvents.CSVLine(i);
         verts << line.substr(0,line.size()-2); // Without the newline character
         verts << std::setprecision( 15 );
         verts << "," << std::to_string(fFrequencies.at(i)) + "," << std::to_string(fPowers.at(i)) + "\n";
         verts << std::setprecision( normal_precision );
      }
      verts.close();
      std::cout << vertexFilename << " saved\n";
   }

   // Save timewindows if 't' is listed in options
   if (options.find('t') != std::string::npos)
   {
      std::string   timeWindowFilename = filename + ".timewindows.csv";
      std::ofstream times;
      times.open(timeWindowFilename);
      times << GetTimeWindows().CSVTitleLine();
      for (size_t i = 0; i < GetTimeWindows().size(); i++) times << GetTimeWindows().CSVLine(i);
      times.close();
      std::cout << timeWindowFilename << " saved\n";
   }

   // Save A2 AnalysisReport data if 'a' is set in options
   // Commented out since I can't access the analysis report easily from here, and does anyone use this anyway?
   /*
   if (options.find('a') != std::string::npos)
   {
      std::string   analysisReportFilename = filename + ".AnalysisReport.csv";
      std::ofstream aReport;
      aReport.open(analysisReportFilename);
      aReport << fAnalysisReports.front().CSVTitleLine();
      for (const TA2AnalysisReport &r : fAnalysisReports) aReport << r.CSVLine();
      aReport.close();
      std::cout << analysisReportFilename << " saved\n";
   }
   */
   // Save Scaler Data if 's' is set in options
   if (options.find('s') != std::string::npos)
   {
      std::string   scalerFilename = filename + ".scaler.csv";
      std::ofstream sis;
      sis.open(scalerFilename);
      sis << SISEvents.CSVTitleLine();
      for (size_t i = 0; i < SISEvents.size(); i++) sis << SISEvents.CSVLine(i);
      sis.close();
      std::cout << scalerFilename << " saved\n";
   }

}


#endif
