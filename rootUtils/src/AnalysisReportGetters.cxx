#include "AnalysisReportGetters.h"

#ifdef BUILD_A2
TA2AnalysisReport Get_A2Analysis_Report(int runNumber, bool force)
{
    TTreeReaderPointer t=Get_TA2AnalysisReport_Tree(runNumber);
    if (t->GetEntries(force)>1)
    {
        std::cout<<"Warning! More than one analysis report in run?!?! What?"<<std::endl;
    }
    TTreeReaderValue<TA2AnalysisReport> AnalysisReport(*t, "TA2AnalysisReport");
   // I assume that file IO is the slowest part of this function... 
   // so get multiple channels and multiple time windows in one pass
   if (t->Next())
   {
       TA2AnalysisReport report(*AnalysisReport);
       return report;
   }
   // There is no analysis report found! Return default constructed one
   // We could maybe think about returning a smart pointer with a null argument
   std::cerr << "No ALPHA2 analysis report found for run " << runNumber << "\n";
   return TA2AnalysisReport();
}
#endif

#ifdef BUILD_AG
TAGAnalysisReport Get_AGAnalysis_Report(int runNumber, bool force)
{
    TTreeReaderPointer t = Get_TAGAnalysisReport_Tree(runNumber);
    if (t->GetEntries(force)>1)
    {
        std::cout<<"Warning! More than one analysis report in run?!?! What?"<<std::endl;
    }
    TTreeReaderValue<TAGAnalysisReport> AnalysisReport(*t, "TAGAnalysisReport");
   // I assume that file IO is the slowest part of this function... 
   // so get multiple channels and multiple time windows in one pass
   if (t->Next())
   {
      TAGAnalysisReport report(*AnalysisReport);
      return report;
   }
   // There is no analysis report found! Return default constructed one.
   // We could maybe think about returning a smart pointer with a null argument
   std::cerr << "No ALPHAg analysis report found for run " << runNumber << "\n";
   return TAGAnalysisReport();
}

#endif
