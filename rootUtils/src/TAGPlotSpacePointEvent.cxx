#if BUILD_AG
#include "TAGPlotSpacePointEvent.h"

ClassImp(TAGPlotSpacePointEvent)


TAGPlotSpacePointEvent::TAGPlotSpacePointEvent()
{
}

TAGPlotSpacePointEvent::~TAGPlotSpacePointEvent()
{
}

TAGPlotSpacePointEvent::TAGPlotSpacePointEvent(const TAGPlotSpacePointEvent& s):
   TObject(s),
   fRunNumber(s.fRunNumber),
   fTime(s.fTime),
   fOfficialTime(s.fOfficialTime),
   fX(s.fX),
   fY(s.fY),
   fZ(s.fZ),
   fR(s.fR),
   fP(s.fP),
   fCutResults(s.fCutResults)
{
}

TAGPlotSpacePointEvent& TAGPlotSpacePointEvent::operator=(const TAGPlotSpacePointEvent& s)
{
   fRunNumber     = s.fRunNumber;
   fTime          = s.fTime;
   fOfficialTime  = s.fOfficialTime;
   fX             = s.fX;
   fY             = s.fY;
   fZ             = s.fZ;
   fR             = s.fR;
   fP             = s.fP;
   fCutResults.assign(s.fCutResults.begin(),s.fCutResults.end());
   return *this;
}

TAGPlotSpacePointEvent& TAGPlotSpacePointEvent::operator+=(const TAGPlotSpacePointEvent& s)
{
   fRunNumber.insert(fRunNumber.end(),s.fRunNumber.begin(), s.fRunNumber.end());
   fTime.insert(fTime.end(),s.fTime.begin(), s.fTime.end());
   fOfficialTime.insert(fOfficialTime.end(),s.fOfficialTime.begin(), s.fOfficialTime.end());
   fX.insert(fX.end(),s.fX.begin(), s.fX.end());
   fY.insert(fY.end(),s.fY.begin(), s.fY.end());
   fZ.insert(fZ.end(),s.fZ.begin(), s.fZ.end());
   fR.insert(fR.end(),s.fR.begin(), s.fR.end());
   fP.insert(fP.end(),s.fP.begin(), s.fP.end());
   fCutResults.insert(fCutResults.end(),s.fCutResults.begin(),s.fCutResults.end());
   return *this;
}

void TAGPlotSpacePointEvent::clear()
{
   fRunNumber.clear();
   fTime.clear();
   fOfficialTime.clear();
   fX.clear();
   fY.clear();
   fZ.clear();
   fR.clear();
   fP.clear();
   fCutResults.clear();
}

size_t TAGPlotSpacePointEvent::size() const
{
   return fRunNumber.size();
}

void TAGPlotSpacePointEvent::AddEvent(
   int runNumber,
   double time,
   double officialTime,
   double x,
   double y,
   double z,
   double r,
   double p,
   const std::vector<bool>& cuts
)
{
   fRunNumber.push_back(runNumber);
   fTime.push_back(time);
   fOfficialTime.push_back(officialTime);
   fX.push_back(x);
   fY.push_back(y);
   fZ.push_back(z);
   fR.push_back(r);
   fP.push_back(p);
   fCutResults.emplace_back();
   fCutResults.back().push_back(cuts);
}

#endif
