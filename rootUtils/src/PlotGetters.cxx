#include "PlotGetters.h"
#include "TSISChannels.h"
#include "ErrorCombination.h"

#include "TStyle.h"

//Plots

#if BUILD_AG

// Significant duplication of code between this and Plot_SIS...
TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<double>& tmin, const std::vector<double>& tmax)
{
   for (const TChronoChannel& c: channel)
      std::cout << c << std::endl;
   TCanvas* c = new TCanvas();
   AlphaColourWheel colour;
   TLegend* legend = new TLegend(0.75,0.5,0.99,0.69);

   std::vector<TH1D*> hh=Get_Summed_Chrono(runNumber, channel,tmin, tmax);
   double max_height = 0;
   double min_height = 1E99;
   for (TH1D* h: hh)
   {
      double min, max;
      h->GetMinimumAndMaximum(min, max);
      if (max > max_height)
      {
         max_height = max;
      }
      if (min < min_height)
      {
         min_height = min;
      }
   }
   if (min_height < 10 && min_height >= 0)
      min_height = 0;

   max_height*=1.1;
   
   for (size_t i=0; i<hh.size(); i++)
   {
      legend->AddEntry(hh[i]);
      hh[i]->SetLineColor(colour.GetNewColour());
      if (i ==0 )
      {
         hh[i]->GetYaxis()->SetRangeUser(min_height,max_height);
         hh[i]->Draw("HIST");
      }
      else
      {
         hh[i]->GetYaxis()->SetRangeUser(min_height,max_height);
         hh[i]->Draw("HIST SAME");
      }
   }
   //std::cout<<"min:"<< min_height <<"\tmax:"<<max_height <<std::endl;
   legend->Draw();
   c->Update();
   c->Draw();
   return c;
}

TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<std::string> & Chrono_Channel_Names, const std::vector<double>& tmin, const std::vector<double>& tmax)
{
   std::vector<TChronoChannel> chans = Get_Chrono_Channels(runNumber,Chrono_Channel_Names);
   return Plot_Summed_Chrono(runNumber, chans, tmin, tmax);
}

TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<TAGSpill>& spills)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   for (const TAGSpill& s: spills)
   {
      tmin.push_back( s.GetStartTime() );
      tmax.push_back( s.GetStopTime() );
   }
   return Plot_Summed_Chrono(runNumber, channel, tmin, tmax);
}

TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<TAGSpill>& spills)
{
   std::vector<TChronoChannel> chans = Get_Chrono_Channels(runNumber,Chrono_Channel_Names);
  return Plot_Summed_Chrono(runNumber, chans, spills);
}

TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<std::string>& description, const std::vector<int>& dumpIndex)
{
   std::vector<TAGSpill> s=Get_AG_Spills(runNumber,description,dumpIndex);
   return Plot_Summed_Chrono(runNumber, channel, s);
}

TCanvas* Plot_Summed_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<std::string>& description, const std::vector<int>& dumpIndex)
{
   std::vector<TChronoChannel> chans = Get_Chrono_Channels(runNumber, Chrono_Channel_Names);
   return Plot_Summed_Chrono( runNumber, chans, description, dumpIndex);
}




TCanvas* Plot_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<double>& tmin, const std::vector<double>& tmax, const std::vector<std::string> labels)
{
   for (const TChronoChannel& c: channel)
      std::cout << c << std::endl;
   TCanvas* c = new TCanvas();
   AlphaColourWheel colour;
   TLegend* legend = new TLegend(0.75,0.5,0.99,0.69);

   std::vector<std::vector<TH1D*>> hh=Get_Chrono(runNumber, channel,tmin, tmax,labels);
   double max_height = 0;
   double min_height = 1E99;
   for (auto times: hh)
      for (TH1D* h: times)
      {
         double min, max;
         h->GetMinimumAndMaximum(min, max);
         if (max > max_height)
         {
            max_height = max;
         }
         if (min < min_height)
         {
            min_height = min;
         }
      }
   if (min_height < 10 && min_height >= 0)
      min_height = 0;

   max_height*=1.1;
   
   for (size_t j=0; j<tmin.size(); j++)
      for (size_t i=0; i<channel.size(); i++)
      {
         legend->AddEntry(hh[i][j]);
         hh[i][j]->SetLineColor(colour.GetNewColour());
         if (i ==0 && j == 0)
         {
            hh[i][j]->GetYaxis()->SetRangeUser(min_height,max_height);
            hh[i][j]->Draw("HIST");
         }
         else
         {
            hh[i][j]->GetYaxis()->SetRangeUser(min_height,max_height);
            hh[i][j]->Draw("HIST SAME");
         }
      }
   //std::cout<<"min:"<< min_height <<"\tmax:"<<max_height <<std::endl;
   legend->Draw();
   c->Update();
   c->Draw();
   return c;
}

TCanvas* Plot_Chrono(const int runNumber, const std::vector<std::string> & Chrono_Channel_Names, const std::vector<double>& tmin, const std::vector<double>& tmax,  const std::vector<std::string> labels)
{
   std::vector<TChronoChannel> chans = Get_Chrono_Channels(runNumber,Chrono_Channel_Names);
   return Plot_Chrono(runNumber, chans, tmin, tmax, labels);
}

TCanvas* Plot_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<TAGSpill>& spills)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   std::vector<std::string> labels;
   std::map<std::string,int> repetition_count;
   for (const TAGSpill& s: spills)
   {
      tmin.push_back( s.GetStartTime() );
      tmax.push_back( s.GetStopTime() );
      if (!repetition_count.count(s.fName))
          repetition_count[s.fName] = 0;
      labels.push_back( s.fName + "[" + std::to_string(repetition_count[s.fName]) + "]" );
      repetition_count[s.fName]++;
   }
   return Plot_Chrono(runNumber, channel, tmin, tmax, labels);
}

TCanvas* Plot_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<TAGSpill>& spills)
{
   std::vector<TChronoChannel> chans = Get_Chrono_Channels(runNumber,Chrono_Channel_Names);
  return Plot_Chrono(runNumber, chans, spills);
}

TCanvas* Plot_Chrono(const int runNumber, const std::vector<TChronoChannel>& channel, const std::vector<std::string>& description, const std::vector<int>& dumpIndex)
{
   std::vector<TAGSpill> s=Get_AG_Spills(runNumber,description,dumpIndex);
   return Plot_Chrono(runNumber, channel, s);
}

TCanvas* Plot_Chrono(const int runNumber, const std::vector<std::string>& Chrono_Channel_Names, const std::vector<std::string>& description, const std::vector<int>& dumpIndex)
{
   std::vector<TChronoChannel> chans = Get_Chrono_Channels(runNumber, Chrono_Channel_Names);
   return Plot_Chrono( runNumber, chans, description, dumpIndex);
}

#endif

#ifdef BUILD_AG
void Plot_Delta_Chrono(Int_t runNumber, TChronoChannel channel, Double_t tmin, Double_t tmax)
{
  if (tmax<0.) tmax=GetAGTotalRunTime(runNumber);
  TH1D* h=Get_Delta_Chrono( runNumber, channel, tmin, tmax);  
  h->GetXaxis()->SetTitle("Time [s]");
  h->GetYaxis()->SetTitle("Counts");
  h->Draw();
  return;  
} 
#endif
#ifdef BUILD_AG
void Plot_Delta_Chrono(Int_t runNumber, TChronoChannel channel, const char* description, Int_t dumpIndex)
{
  std::vector<TAGSpill> spills = Get_AG_Spills(runNumber, {description}, {dumpIndex});
  std::vector<double> tmin;
  std::vector<double> tmax;
  for (const TAGSpill& s: spills)
  {
    tmin.push_back( s.GetStartTime() );
    tmax.push_back( s.GetStopTime() );
  }
  return Plot_Delta_Chrono(runNumber, channel, tmin.front(), tmax.front());
}
#endif
#ifdef BUILD_AG
void Plot_Delta_Chrono(Int_t runNumber, const char* ChannelName, Double_t tmin, Double_t tmax)
{
  if (tmax<0.) tmax=GetAGTotalRunTime(runNumber);
  TH1D* h=Get_Delta_Chrono( runNumber, ChannelName, tmin, tmax);
  h->Draw();
  return;  
} 
#endif
#ifdef BUILD_AG
void Plot_Delta_Chrono(Int_t runNumber, const char* ChannelName, const char* description, Int_t dumpIndex)
{
   std::vector<TAGSpill> spills = Get_AG_Spills(runNumber, {description}, {dumpIndex});
   return Plot_Delta_Chrono(runNumber, ChannelName, spills.front().GetStartTime(), spills.front().GetStopTime());
}
#endif
#ifdef BUILD_AG
void Plot_Chrono_Scintillators(Int_t runNumber, Double_t tmin, Double_t tmax)
{
  if (tmax<0.) tmax=GetAGTotalRunTime(runNumber);

  //std::vector<std::string> channels {"SiPM_A","SiPM_D","SiPM_A_OR_D","SiPM_E",
  std::vector<std::string> channels {"SIPM_BOTG_ASACUSA_TOP","SIPM_BOTG_LASER_TOP","SIPM_BOTG_TOP_OR","SIPM_LDS",
      "SIPM_BOTG_ASACUSA_BOTTOM","SIPM_BOTG_LASER_BOTTOM","SIPM_BOTG_BOTTOM_OR","SIPM_UDS"};
  TString cname = TString::Format("cSiPM_%1.3f-%1.3f",tmin,tmax);
  TCanvas* c = new TCanvas(cname.Data(),cname.Data(), 1800, 1500);
  c->Divide(2,4);
  int i=0;
  for(auto it = channels.begin(); it!=channels.end(); ++it)
    {
      TChronoChannel chan = Get_Chrono_Channel(runNumber,it->c_str());
      TH1D* h=Get_Chrono( runNumber, {chan}, {tmin}, {tmax}).front().front();
      h->GetXaxis()->SetTitle("Time [s]");
      h->GetYaxis()->SetTitle("Counts"); 
      c->cd(++i);
      h->Draw();
    }
  return;
}
#endif
#ifdef BUILD_AG
void Plot_Chrono_Scintillators(Int_t runNumber, const char* description, Int_t dumpIndex)
{
   std::vector<TAGSpill> spills = Get_AG_Spills(runNumber, {description}, {dumpIndex});
   return Plot_Chrono_Scintillators(runNumber, spills.front().GetStartTime(), spills.front().GetStopTime());
}
#endif
#ifdef BUILD_AG
TCanvas* Plot_TPC(const int runNumber, const std::vector<double> tmin, const std::vector<double> tmax, const std::string cutsType)
{
  TAGPlot* p=new TAGPlot();
  p->AddTimeGates(runNumber,tmin,tmax);
  p->LoadData();
  TString cname = TString::Format("cVTX_R%d",runNumber);
  std::cout<<"Run\tNverts\tNpass"<<std::endl;
  std::cout<<"Stat"<<cutsType<<","<<runNumber<<","<<p->GetNVerticies()<<","<<p->GetNPassedType(cutsType)<<std::endl;
  return p->DrawVertexCanvas(cname,cutsType);
}
#endif
#ifdef BUILD_AG
TCanvas* Plot_TPC(const int runNumber, const std::vector<std::string> description, const std::vector<int> dumpIndex, const std::string cuts)
{
   std::vector<TAGSpill> spills = Get_AG_Spills(runNumber, description, dumpIndex);
   std::vector<double> tmin, tmax;
   for (const TAGSpill& s: spills)
   {
      tmin.push_back(s.GetStartTime());
      tmax.push_back(s.GetStopTime());
      std::cout<<"Dump at ["<<tmin.back()<<","<<tmax.back()<<"] s   duration: "<<tmax.back()-tmin.back()<<" s"<<std::endl;
   }
   return Plot_TPC(runNumber,tmin,tmax, cuts);
}
#endif
#ifdef BUILD_AG
void Plot_Vertices_And_Tracks(const int runNumber, const std::vector<double> tmin, const std::vector<double> tmax, const std::string cuts)
{
  TAGPlotTracks* p=new TAGPlotTracks();
  p->AddTimeGates(runNumber,tmin,tmax);

  int total_number_vertices = p->GetNVertexEvents();
  double total_runtime = p->GetTotalTime();
  p->LoadData();
  std::string cname = "cVTX_";
  for (size_t i = 0; i < tmin.size(); i++)
  {
    cname += "[" + std::to_string(tmin.at(i)) + "-" + std::to_string(tmax.at(i)) + "]";
  }
  cname += "R_" + std::to_string(runNumber);
  //  std::cout<<cname<<std::endl;
  p->DrawVertexCanvas(cname.c_str(),cuts);
  cname = "cTRK_";
  for (size_t i = 0; i < tmin.size(); i++)
  {
    cname += "[" + std::to_string(tmin.at(i)) + "-" + std::to_string(tmax.at(i)) + "]";
  }
  cname += "R_" + std::to_string(runNumber);
  //  std::cout<<cname<<std::endl;
  p->DrawTrackCanvas(cname.c_str(), cuts);

  //std::cout<<"Total Number of Events: "<<total_number_events<<std::endl;
  std::cout<<"Total Number of Vertices: "<<total_number_vertices<<std::endl;
  std::cout<<"Total Runtime: "<<total_runtime<<std::endl;

  return;
}

void Plot_Vertices_And_Tracks(const int runNumber,  const std::vector<std::string> description, const std::vector<int> dumpIndex, const std::string cuts )
{
   std::vector<TAGSpill> spills = Get_AG_Spills(runNumber,description,dumpIndex);
   std::vector<double> tmin, tmax;
   for (const TAGSpill& s: spills)
   {
      tmin.push_back(s.GetStartTime());
      tmax.push_back(s.GetStopTime());
   }
   return Plot_Vertices_And_Tracks(runNumber,tmin,tmax,cuts);
}

void Plot_AGSim(const int runNumber)
{
   TAGPlotSim* p=new TAGPlotSim();
   p->LoadRun(runNumber);

   std::string cname = "cVTX_MC_";
   cname += "R_" + std::to_string(runNumber);
   //  std::cout<<cname<<std::endl;
   TCanvas* c = p->DrawSimCanvas(cname.c_str());
   std::string sname = cname+".pdf";
   c->SaveAs(sname.c_str());
   //   cname = "cTRK_MC_";
   //   cname += "R_" + std::to_string(runNumber);
   //   //  std::cout<<cname<<std::endl;
   //   p->DrawTrackCanvas(cname.c_str(), cuts);

   int total_number_vertices = p->GetNVertexEvents();
   double total_runtime = p->GetTotalTime();
   //std::cout<<"Total Number of Events: "<<total_number_events<<std::endl;
   std::cout<<"Total Number of Vertices: "<<total_number_vertices<<std::endl;
   std::cout<<"Total Runtime: "<<total_runtime<<std::endl;

   return;
}

#endif

#ifdef BUILD_AG
void Plot_FRD_Zed(std::vector<int> runNumberQWPleft, std::vector<int> runNumberQWPright, const std::string Cuts) {
   double Z_centre=-508.;
   double Z_width=400.;
   TString cname = "cFDR_Zed";
   TCanvas* c = new TCanvas(cname.Data(),cname.Data(), 1800, 1500);
   c->Divide(2,1);
   c->cd(1);
   // QWP left FDR plot
   TAGPlot a(Z_centre-Z_width,Z_centre+Z_width);
   for (int run_num: runNumberQWPleft){
      a.AddDumpGates(run_num,{"FastRampDown"},{0});
   }
   a.LoadData();
   a.FillHisto(Cuts);
   TH1D* hQWPleft = a.GetTH1D("zvtx");
   TH1D* hQWPleftCut = new TH1D("QWP left","FastRampDown QWP left;Zed position [mm];Counts",hQWPleft->GetNbinsX(),hQWPleft->GetXaxis()->GetXmin(),hQWPleft->GetXaxis()->GetXmax());
   for (int bin=0; bin<hQWPleft->GetNbinsX();bin++) {
      if (Z_centre-Z_width < hQWPleft->GetBinCenter(bin+1) and hQWPleft->GetBinCenter(bin+1) < Z_centre+Z_width) {
         for (int i=0; i<hQWPleft->GetBinContent(bin+1); i++)
            hQWPleftCut->Fill(hQWPleft->GetBinCenter(bin+1));
      }
   }
   hQWPleftCut->GetXaxis()->SetRangeUser(Z_centre-Z_width,Z_centre+Z_width);
   hQWPleftCut->Draw("hist");
   // Mixing left line
   //TAGPlot g;
   //g.AddDumpGates(runNumberQWPleft.at(runNumberQWPleft.size()-1),{"Mixing"},{-1});
   //g.AddDumpGates(runNumberQWPright.at(runNumberQWPright.size()-1),{"Mixing"},{-1});

   //if (runNumberQWPleft.size()>=3) {
   //   g.AddDumpGates(runNumberQWPleft.at(runNumberQWPleft.size()-2),{"Mixing"},{-1});
   //   g.AddDumpGates(runNumberQWPleft.at(runNumberQWPleft.size()-3),{"Mixing"},{-1});
   //}
   //if (runNumberQWPright.size()>=3) {
   //   g.AddDumpGates(runNumberQWPright.at(runNumberQWPright.size()-2),{"Mixing"},{-1});
   //   g.AddDumpGates(runNumberQWPright.at(runNumberQWPright.size()-3),{"Mixing"},{-1});
   //}
   //g.LoadData();
   //g.FillHisto(0);
   //double mixMean = g.GetTH1D("zvtx")->GetMean();
   double mixMean = -543;
   TLine* leftline = new TLine(mixMean,0,mixMean,hQWPleftCut->GetMaximum());
   leftline->SetLineColor(kRed);
   leftline->SetLineWidth(4.);
   leftline->Draw();
   // QWP right plot
   c->cd(2);
   TAGPlot b(Z_centre-Z_width,Z_centre+Z_width);
   for (int run_num: runNumberQWPright){
      b.AddDumpGates(run_num,{"FastRampDown"},{0});
   }
   b.LoadData();
   b.FillHisto(Cuts);
   TH1D* hQWPright = b.GetTH1D("zvtx");
   TH1D* hQWPrightCut = new TH1D("QWP right","FastRampDown QWP right;Zed position [mm];Counts",hQWPright->GetNbinsX(),hQWPright->GetXaxis()->GetXmin(),hQWPright->GetXaxis()->GetXmax());
   for (int bin=0; bin<hQWPright->GetNbinsX();bin++) {
      if (Z_centre-Z_width < hQWPright->GetBinCenter(bin+1) and hQWPright->GetBinCenter(bin+1) < Z_centre+Z_width) {
         for (int i=0; i<hQWPright->GetBinContent(bin+1); i++)
            hQWPrightCut->Fill(hQWPright->GetBinCenter(bin+1));
      }
   }
   hQWPrightCut->GetXaxis()->SetRangeUser(Z_centre-Z_width,Z_centre+Z_width);
   hQWPrightCut->Draw("hist");
   // Mixing right line
   TLine* rightline = new TLine(mixMean,0,mixMean,hQWPrightCut->GetMaximum());
   rightline->SetLineColor(kRed);
   rightline->SetLineWidth(4.);
   rightline->Draw();
   printf("Mixing triggers mean Z position:%.6f\n",mixMean);
   // K-S test
   printf("Kolmogorov–Smirnov test result: %.6f\n",hQWPleftCut->KolmogorovTest(hQWPrightCut));
   printf("Value = Probability of being taken from same distribution\n");
   printf("Value ~ 1 = no difference, value ~ 0 = very different\n");

   a.DrawVertexCanvas("QWPLeft",Cuts)->SaveAs("QWPLeft.png");
   a.ExportCSV("QWPLeft");

   b.DrawVertexCanvas("QWPRight.png",Cuts)->SaveAs("QWPRight.png");
   b.ExportCSV("QWPRigh");


  return;

}
#endif

//*************************************************************
// Energy Analysis
//*************************************************************
TCanvas* gc;
TH1D* gh;
#ifdef BUILD_AG
TCanvas* Plot_AG_AnyDump(Int_t runNumber, double start_time, double stop_time, Int_t binNumber, const char* dumpFile, Double_t EnergyRangeFactor, const char* Chrono_Channel_Name, const char* Dump_Name)
{
   if (stop_time<0.) stop_time = GetAGTotalRunTime(runNumber);

   double startOffset = 0.002; // dump starts two milliseconds after the start dump trigger
   double stopOffset = 0.; // dump finishes at trigger

   double dumpDuration = stop_time - start_time - startOffset - stopOffset;

   std::cout <<"Dump start: "<< start_time-startOffset << " Dump stop: " << stop_time-stopOffset << std::endl;
   std::cout<<"Dump Duration "<<"\t"<<dumpDuration<<" s"<<std::endl;

   Int_t oldBinNumber = rootUtils::GetDefaultBinNumber();
   Int_t oldTimeBinNumber = rootUtils::GetTimeBinNumber();
   rootUtils::SetDefaultBinNumber(1.e4);
   rootUtils::SetTimeBinNumber(1.e4);
   TH1D* dumpHisto = Get_Chrono( runNumber, {Get_Chrono_Channel(runNumber,Chrono_Channel_Name)}, {start_time}, {stop_time} ).front().front();
   rootUtils::SetDefaultBinNumber(oldBinNumber);
   rootUtils::SetTimeBinNumber(oldTimeBinNumber);
 
  if(!dumpHisto){Error("PlotEnergyDump","NO CB counts plot"); return 0;}
  // and the voltage ramp function of time
  TSpline5* dumpRamp = InterpolateVoltageRamp(dumpFile);
  if(!dumpRamp){Error("PlotEnergyDump","NO voltage ramp function"); return 0;}
    TH1D* dumpHisto_toPlot =Get_Chrono(runNumber,{Get_Chrono_Channel(runNumber,Chrono_Channel_Name)},{start_time+startOffset},{stop_time}).front().front(); // this is a lower resolution histo to plot

  TString htitle = "Run ";
  htitle+=runNumber;
  htitle+=" : #bar{p} energy  ";
  htitle+=Dump_Name;
  htitle+=";Energy [eV];counts";

  // energy (temperature) histogram
  Double_t RampTmin=dumpRamp->GetXmin();
  Double_t RampTmax=dumpRamp->GetXmax();
  if ( RampTmax<0.) {Error("PlotEnergyDump","Ramp invalid? Can't work out how long it was"); return 0; }
  Double_t Emin=dumpRamp->Eval(RampTmax), Emax=dumpRamp->Eval(RampTmin);
  TH1D* hEnergy = new TH1D("pbar_temperature", htitle.Data(), binNumber, Emin, Emax);
  hEnergy->SetMarkerColor(kRed);
  hEnergy->SetMarkerStyle(7);
  hEnergy->SetLineColor(kBlack);

  // calculate the energy resolution
  Double_t res = (Emax-Emin)/ (Double_t) rootUtils::GetDefaultBinNumber();
  char resolution[80];
  snprintf(resolution,80,"Energy Resolution %.1lf meV ",res*1.e3);
  std::cout << resolution << "\n";
  // map time to energy
  Double_t dt,energy,energy_max=0.;
  Int_t counts=0.;
  for(Int_t b=1; b<=dumpHisto->GetNbinsX(); ++b)
    {
      dt = (dumpHisto->GetBinCenter(b)/*-start_time*/-startOffset);//(dumpDuration);
      energy = dumpRamp->Eval(dt);
      //~ //if(energy<res) continue;
      counts = dumpHisto->GetBinContent(b);
      if(energy>energy_max && counts>10) energy_max=energy; // interesting bins only
//#if DEBUG > 1
//      std::cout<<b<<"\t"<<dt<<"\t"<<energy<<"\t"<<dumpHisto->GetBinContent(b)<<std::endl;
//#endif
      hEnergy->Fill(energy, counts);

    }

  //hEnergy->GetXaxis()->SetRangeUser(Emin,energy_max);

  // calculate the total number of counts in the intresting range
  Int_t binmin = hEnergy->FindBin(Emin),
    binmax = hEnergy->FindBin(energy_max);
  Double_t total_counts = hEnergy->Integral(binmin,binmax);
  char integral[80];
  snprintf(integral,80,"Integral %1.01f",total_counts);

  // information
  TPaveText* tt = new TPaveText(0.1,0.5,0.9,0.9);
  tt->SetFillColor(0);
  tt->AddText(resolution);
  tt->AddText(integral);

  // plotting and wait for fitting
  //  if (gLegendDetail>=1)
  //    {
      gStyle->SetOptStat(1011111);
      //    }
      //  else
      //    {
      //      gStyle->SetOptStat("ni");
      //    }
  delete gc;
  TCanvas* cEnergy = new TCanvas("AntiprotonTemperature","AntiprotonTemperature",1800,1000);
  cEnergy->Divide(2,2);
  cEnergy->cd(1);
  gPad->SetLogy(1);
  dumpHisto_toPlot->Draw("HIST");
  cEnergy->cd(2);
  
  hEnergy->GetXaxis()->SetRangeUser(0,EnergyRangeFactor*(hEnergy->GetMean()));
  hEnergy->Draw("E1 HIST");
  gPad->SetLogy(1);
  cEnergy->cd(3);
  dumpRamp->Draw();

  cEnergy->cd(4);
  tt->Draw();

  //necessary for fitting
  gc=cEnergy;
  gh=hEnergy;
  return cEnergy;
   
}

TCanvas* Plot_AG_AnyDump(Int_t runNumber,Int_t dumpIndex, Int_t binNumber, const char* dumpFile, Double_t EnergyRangeFactor, const char* Chrono_Channel_Name, const char* Dump_Name)
{
   std::vector<TAGSpill> spills = Get_AG_Spills(runNumber, {Dump_Name}, {dumpIndex});
   if (spills.empty())
      {
         std::cout<<"No \"" << Dump_Name << "\" in run..."<<std::endl;
         return NULL;
      }
   
   double start_time = spills.front().GetStartTime();
   double stop_time = spills.front().GetStopTime();
   
   return Plot_AG_AnyDump(runNumber, start_time, stop_time, binNumber, dumpFile, EnergyRangeFactor, Chrono_Channel_Name, Dump_Name);
}

TCanvas* Plot_AG_ColdDump(Int_t runNumber,Int_t dumpIndex, Int_t binNumber, const char* dumpFile, Double_t EnergyRangeFactor, const char* Chrono_Channel_Name)
{
   return Plot_AG_AnyDump( runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,Chrono_Channel_Name,"Cold Dump");
}

TCanvas* Plot_AG_CT_ColdDump(Int_t runNumber,Int_t dumpIndex, Int_t binNumber,
                          const char* dumpFile,
                          Double_t EnergyRangeFactor)
{
   std::string ramp;
   if (!dumpFile)
   {
      TAGSpill dump = Get_AG_Spills(runNumber,{"Cold Dump"},{dumpIndex}).front();
      double length = dump.GetStopTime() - dump.GetStartTime();
      if ( length < 0.6)
         ramp = "ana/macros/ColdDumpE4E5.dump";
      else if ( fabs(length - 1.) < 0.1 )
         ramp = "ana/macros/ColdDumpE4E5_1s.dump";
      else if ( fabs(length - 2.) < 0.1 )
         ramp = "ana/macros/ColdDumpE4E5_2s.dump";
      else if ( fabs(length - 4.) < 0.1 )
         ramp = "ana/macros/ColdDumpE4E5_4s.dump";
      else if (fabs(length - 10.) < 0.1 )
         ramp = "ana/macros/ColdDumpE4E5_10s.dump";
   }
   else
   {
      ramp = dumpFile;
   }
   return Plot_AG_AnyDump( runNumber, dumpIndex, binNumber, ramp.c_str(), EnergyRangeFactor,"CT_SiPM_OR","Cold Dump");
}

TCanvas* Plot_AG_RCT_Top_ColdDump(Int_t runNumber,Int_t dumpIndex, Int_t binNumber, const char* dumpFile, Double_t EnergyRangeFactor)
{
   return Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"SIPM_UDS","Cold Dump");
}

TCanvas* Plot_AG_RCT_ColdDump(Int_t runNumber,Int_t dumpIndex, Int_t binNumber, const char* dumpFile, Double_t EnergyRangeFactor)
{
   return Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"SIPM_LDS","Cold Dump");
}

TCanvas* Plot_AG_ATMPreMix_Top_ColdDump(Int_t runNumber,Int_t dumpIndex, Int_t binNumber, const char* dumpFile, Double_t EnergyRangeFactor)
{
   std::string ramp;
   if (!dumpFile)
   {
      TAGSpill dump = Get_AG_Spills(runNumber,{"Cold Dump"},{dumpIndex}).front();
      double length = dump.GetStopTime() - dump.GetStartTime();
      if ( length < 0.6)
         ramp = "ana/macros/Pbar_ramp_file_UDS_before_premix";
      else if ( fabs(length - 1.) < 0.1 )
         ramp = "ana/macros/Pbar_ramp_file_UDS_before_premix_1s.dump";
      else if ( fabs(length - 2.) < 0.1 )
         ramp = "ana/macros/Pbar_ramp_file_UDS_before_premix_2s.dump";
      else if ( fabs(length - 4.) < 0.1 )
         ramp = "ana/macros/Pbar_ramp_file_UDS_before_premix_4s.dump";
   }
   else
   {
      ramp = dumpFile;
   }
   return Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, ramp.c_str(), EnergyRangeFactor,"SIPM_UDS","Cold Dump");
}

TCanvas* Plot_AG_ATM_LeftDump(Int_t runNumber,Int_t dumpIndex, Int_t binNumber, const char* dumpFile, Double_t EnergyRangeFactor)
{
   return Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"SIPM_LDS","left dump (down)");
}

TCanvas* Plot_AG_ATM_RightDump(Int_t runNumber,Int_t dumpIndex, Int_t binNumber, const char* dumpFile, Double_t EnergyRangeFactor)
{
   return Plot_AG_AnyDump(runNumber, dumpIndex, binNumber, dumpFile, EnergyRangeFactor,"SIPM_UDS","right dump (up)");
}


#endif


#if BUILD_A2
TCanvas* Plot_A2_AnyDump(Int_t runNumber, int repetition, const char* SIS_Channel_Name, const char* Dump_Name)
{  

   std::vector<TA2Spill> spills = Get_A2_Spills(runNumber,{Dump_Name},{-1});
   std::cout << "Spills size = " << spills.size() << std::endl;
   std::vector<double> tmin;
   std::vector<double> tmax;

   std::cout << "tmin size = " << tmin.size() << std::endl;
   std::cout << "tmax size = " << tmax.size() << std::endl;

   for (TA2Spill& s: spills) {
     tmin.push_back(s.GetStartTime());
     tmax.push_back(s.GetStopTime());
   }

   std::cout << "tmin size = " << tmin.size() << std::endl;
   std::cout << "tmax size = " << tmax.size() << std::endl;
   Double_t start_time = tmin.at(repetition);
   Double_t stop_time = tmax.at(repetition);

   if (stop_time<0.) stop_time=GetA2TotalRunTime(runNumber);

   Double_t startOffset = 0.002; // dump starts two milliseconds after the start dump trigger
   Double_t stopOffset = 0.; // dump finishes at trigger
 
   Double_t dumpDuration = stop_time-start_time-startOffset-stopOffset;
 
   std::cout <<"Dump start: "<< start_time-startOffset << " Dump stop: " << stop_time-stopOffset << std::endl;
   std::cout<<"Dump Duration "<<"\t"<<dumpDuration<<" s"<<std::endl;

   TSISChannels chans(runNumber);
   TSISChannel channel = chans.GetChannel(SIS_Channel_Name);
   std::cout<<"Plotting SIS Channel "<<channel<<std::endl;
   std::vector<TSISChannel> SISChannels = {channel};
 
   std::vector<std::vector<TH1D*>> dumpHisto = Get_SIS( runNumber, SISChannels, {start_time}, {stop_time});
 
   if(!dumpHisto.at(0).at(0)){
      Error("Plot_A2_AnyDump","NO SIS counts plot"); 
      return 0;
   }
 
   TCanvas* cDump = new TCanvas("AntiprotonDump","AntiprotonDump",800,500);
   gPad->SetLogy(1);
   dumpHisto.at(0).at(0)->Draw("HIST");
   Int_t max_bin = dumpHisto.at(0).at(0)->GetMaximumBin();
   double height = dumpHisto.at(0).at(0)->GetBinContent(max_bin);
   double zeroX = dumpHisto.at(0).at(0)->GetBinCenter(max_bin);
   TLine *l = new TLine(zeroX, 0, zeroX, height);
   l->SetLineColor(kRed);
   l->SetLineWidth(2);
   l->Draw("same");

   return cDump;
}
#endif

#if BUILD_A2
TCanvas* Plot_A2_CT_HotDump(Int_t runNumber, int rep)
{
   return Plot_A2_AnyDump(runNumber, rep, "CT_SiPM_OR", "Hot Dump");
}
#endif
#if BUILD_A2

TCanvas* MultiPlotRunsAndDumps(std::vector<Int_t> runNumbers, std::string SISChannel, std::vector<std::string> description, std::vector<std::vector<int>> dumpNumbers, std::string drawOption)
{
  //Set up a vector of final histograms to save.
  std::vector<TH1D*> allHistos;

  //Loop through each run inputed.
  for(size_t i=0; i<runNumbers.size();  i++) 
  {
    Int_t run = runNumbers.at(i);

    //These 3 lines find the SIS channel of the input. 
    TSISChannels channelFinder(run);
    TSISChannel channel = channelFinder.GetChannel(SISChannel.c_str());
    std::vector<TSISChannel> channels = {channel};

    //Get the spills from the data (should only be the range selected in the dumpNumbers) 
    std::vector<TA2Spill> spills = Get_A2_Spills(run, description, {dumpNumbers.at(i)});
    //Get the SIS from the spill, then pills the first (and only histo) and adds it to allHistos
    allHistos.push_back( Get_SIS(run, channels, {spills.at(0)}).front().front() );
  }

  //Set up a nice title, and create a vector of strings for the legend(s).
  std::vector<std::string> legendStrings;
  std::string title = description.at(0);
  title+="s for the following runs and spills (RunNum/Spill): ";
  for(size_t i=0; i<runNumbers.size(); i++) 
  {
    //Turn a vector into a string - this is the best way in C++
    std::stringstream result;
    std::copy(dumpNumbers.at(i).begin(), dumpNumbers.at(i).end(), std::ostream_iterator<int>(result, " "));
    
    //Add to the long title string.
    title+="(";
    title+=std::to_string(runNumbers.at(i));
    title+="/";
    title+=result.str();
    title+=")";
    title+=", ";

    //Add each string to legend names vector.
    legendStrings.push_back("Hist " + std::to_string(i) + " = (" + std::to_string(runNumbers.at(i)) + " / [" + result.str() + "])");
  }

  //Create the final canvas, 2D histogram stack, and 2D legend.
  TCanvas *finalCanvas = new TCanvas("finalCanvas",title.c_str());
  THStack *histoStack2D = new THStack("histoStack",title.c_str());
  TLegend *legend2D = new TLegend();
  gStyle->SetPalette(kRainBow); //Select colour style, this has been the clearest I can find.

  //Loop through all histos and stack into a standard 2D stack.
  for(size_t i=0; i<allHistos.size(); i++) 
  {
    histoStack2D->Add(allHistos.at(i)); //Add each histo to the stack. 
    legend2D->AddEntry(allHistos.at(i), legendStrings.at(i).c_str() ); //Add an entry to the legend.
  }

  //If 2D draw options are selected we will enter this and draw. If not the 2D stack is used to convert to a 3D stack.
  if(drawOption == "2dstack" || drawOption == "2dnostack")
  {
    //We have to draw it first to be able to get the axis. This draw will be overwritten below. 
    histoStack2D->Draw(); //Dummy draw
    histoStack2D->GetXaxis()->SetTitle("Time (s)");
    histoStack2D->GetYaxis()->SetTitle("Counts");
    if(drawOption == "2dstack") 
      histoStack2D->Draw("pfc hist"); //Real draw
    else if (drawOption == "2dnostack")
      histoStack2D->Draw("pfc hist nostack"); //Real draw
    legend2D->Draw(); //Draw legend
  }



  //If 3D options are selected we will skip the 2D draw, go straight to the 3D conversion then the 3D draw.
  if(drawOption == "3dheat" || drawOption == "3dstack")
  {
    //Create new 3D stacks and legend.
    THStack *histoStack3D = new THStack("histoStack",title.c_str());
    TLegend *legend3D = new TLegend();
    //This function adds a z axis to the plots to allow to plot them in 3D.
    Generate3DTHStack(allHistos, histoStack3D, legend3D, legendStrings);
    histoStack3D->Draw(); //Dummy draw, again just to be able to grab axis and set titles.
    histoStack3D->GetXaxis()->SetTitle("Time (s)");
    histoStack3D->GetYaxis()->SetTitle("Run and dump - see legend");
    histoStack3D->GetHistogram()->GetZaxis()->SetTitle("Counts");
    if(drawOption == "3dheat") 
      histoStack3D->Draw("lego2"); //Real draw
    else if (drawOption == "3dstack")
      histoStack3D->Draw("pfc lego1"); //Real draw
    legend3D->Draw(); //Draw the legend
    }
  return finalCanvas;
}
#endif

void Generate3DTHStack(std::vector<TH1D*> allHistos, THStack* emptyStack, TLegend* emptyLegend, std::vector<std::string> legendStrings) {
  //Function takes the vector of relevent histos we had and generates a 3D histogram stack from it for visualisation. 
  size_t numOfHists = allHistos.size();
  gStyle->SetPalette(kRainBow); //This should match the one above but can be different if you like.
  
  for(size_t i=0; i<numOfHists; i++) 
  {
    //Loops over all relevant TH1D histograms and grabs the x data.
    TH1D* currentHist = allHistos.at(i); 
    int numXbins = currentHist->GetNbinsX();
    double minX = currentHist->GetXaxis()->GetXmin();
    double maxX = currentHist->GetXaxis()->GetXmax();
    //Creates a TH2D with the old TH1D x data and a arbitrary y axis such that each histogram is back to back.
    TH2D *newHist = new TH2D( Form("h2_%ld",i), Form("h2_%ld",i), numXbins, minX, maxX, numOfHists, 0, numOfHists);
    for (int j=1; j<=numXbins; j++) 
    {
      newHist->SetBinContent(j,i+1,currentHist->GetBinContent(j)); //Populates each histogram with its original content in x and just its number in y.
    }
    emptyStack->Add(newHist); //Add histogram to our empty stack.
    emptyLegend->AddEntry(newHist, legendStrings.at(i).c_str() ); //Adds legend entry to our old legend.
  }
}


Double_t Boltzmann_constant = 8.61733e-5; //eV/K

Double_t FitEnergyDump( Double_t Emin, Double_t Emax,TH1D* h)
{

  if (!h) h=gh;
  if(!h) {std::cout<<"Nothing to fit... exiting"<<std::endl; return 0.;}
  h->Fit("expo","QM0","",Emin,Emax);
  TF1* fEnergy = h->GetFunction("expo");
  fEnergy->SetLineColor(kBlue);

  Double_t temperature = -1./fEnergy->GetParameter(1)/Boltzmann_constant,
    error = -temperature*fEnergy->GetParError(1)/fEnergy->GetParameter(1);
  if(!gc) { std::cout<<"Nothing to draw into... "<<std::endl; return temperature;}

  gc->cd(2);
  fEnergy->Draw("same");

  char result[100];
  snprintf(result,100," ( %.2lf #pm %.2lf ) K ",temperature,error);

  TPaveText* pt = new TPaveText(0.1,0.1,0.9,0.4);
  pt->SetFillColor(0);
  pt->SetFillStyle(1001);
  pt->SetTextColor(kRed);
  pt->AddText("#bar{p} temperature");
  pt->AddText(result);
  gc->cd(4);
  pt->Draw("same");

  return temperature;
}

void SaveCanvas()
{
  //Function to save the global canvas (gc)
  TString FileName;
  std::cout << "Please input the chosen filename (no file extension needed)" << std::endl;
  std::cin >> FileName;
  TString Output = MakeAutoPlotsFolder("");
  Output+=FileName;
  SaveCanvas(Output);
}

void SaveCanvas(Int_t runNumber, const char* Description)
{
  TString Output = MakeAutoPlotsFolder("");
  Output+="R";
  Output+=runNumber;
  Output+=Description;
  SaveCanvas(Output);
}

void SaveCanvas(TString Description)
{
  TSubString extension = Description(Description.Sizeof()-5,Description.Sizeof());
  //If not a 3 char extension... add one
  if (extension[0] != '.')
     Description+=".png";
  gc->SaveAs(Description);
  std::cout << "File saved here:" << std::endl << Description << std::endl;
}

void SaveCanvas( TCanvas* iSaveCanvas, TString iDescription){
	TString Output = MakeAutoPlotsFolder("");
	Output+=iDescription;
	Output+=".png";
	iSaveCanvas->SaveAs(Output);
	
}
#if BUILD_A2
TCanvas* Plot_A2_ColdDump(Int_t runNumber,int repetition, Int_t binNumber, const char* dumpFile, Double_t EnergyRangeFactor, const char* SIS_Channel_Name)
{  

   std::vector<TA2Spill> spills = Get_A2_Spills(runNumber,{"Cold Dump"},{-1});
   std::cout << "Spills size = " << spills.size() << std::endl;
   std::vector<double> tmin;
   std::vector<double> tmax;

   std::cout << "tmin size = " << tmin.size() << std::endl;
   std::cout << "tmax size = " << tmax.size() << std::endl;

   for (TA2Spill& s: spills) {
     tmin.push_back(s.GetStartTime());
     tmax.push_back(s.GetStopTime());
   }

   std::cout << "tmin size = " << tmin.size() << std::endl;
   std::cout << "tmax size = " << tmax.size() << std::endl;
   Double_t start_time = tmin.at(repetition);
   Double_t stop_time = tmax.at(repetition);

   if (stop_time<0.) stop_time=GetA2TotalRunTime(runNumber);

   Double_t startOffset = 0.002; // dump starts two milliseconds after the start dump trigger
   Double_t stopOffset = 0.; // dump finishes at trigger
 
   Double_t dumpDuration = stop_time-start_time-startOffset-stopOffset;
 
   std::cout <<"Dump start: "<< start_time-startOffset << " Dump stop: " << stop_time-stopOffset << std::endl;
   std::cout<<"Dump Duration "<<"\t"<<dumpDuration<<" s"<<std::endl;

   Int_t oldBinNumber = rootUtils::GetDefaultBinNumber();
   Int_t oldTimeBinNumber = rootUtils::GetTimeBinNumber();
   rootUtils::SetDefaultBinNumber(1.e4);
   rootUtils::SetTimeBinNumber(1.e4);

   TSISChannels chans(runNumber);

   TSISChannel channel = chans.GetChannel(SIS_Channel_Name);
   std::cout<<"Plotting SIS Channel "<<channel<<std::endl;
   std::vector<TSISChannel> SISChannels = {channel};
 
   std::vector<std::vector<TH1D*>> dumpHisto = Get_SIS( runNumber, SISChannels, {start_time}, {stop_time});
   rootUtils::SetDefaultBinNumber(oldBinNumber);
   rootUtils::SetTimeBinNumber(oldTimeBinNumber);
   if(!dumpHisto.at(0).at(0)){
      Error("PlotEnergyDump","NO CB counts plot"); 
      return 0;
   }

    // and the voltage ramp function of time
   TSpline5* dumpRamp = InterpolateVoltageRamp(dumpFile);
   if(!dumpRamp){
      Error("PlotEnergyDump","NO voltage ramp function"); 
      return 0;
   }
     
   // this is a lower resolution histo to plot
   std::vector<std::vector<TH1D*>> dumpHisto_toPlot = Get_SIS(runNumber, SISChannels, {start_time+startOffset}, {stop_time});

   // energy (temperature) histogram
   TString htitle = "Run ";
   htitle+=runNumber;
   htitle+=" : #bar{p} energy  Cold Dump;Energy [eV];counts";
   Double_t RampTmin=dumpRamp->GetXmin();
   Double_t RampTmax=dumpRamp->GetXmax();

   if ( RampTmax<0.) {
      Error("PlotEnergyDump","Ramp invalid? Can't work out how long it was"); 
      return 0; 
   }
   Double_t Emin=dumpRamp->Eval(RampTmax), Emax=dumpRamp->Eval(RampTmin);
   TH1D* hEnergy = new TH1D("pbar_temperature", htitle.Data(), binNumber, Emin, Emax);
   hEnergy->SetMarkerColor(kRed);
   hEnergy->SetMarkerStyle(7);
   hEnergy->SetLineColor(kBlack);

   // calculate the energy resolution
   Double_t res = (Emax-Emin)/ (Double_t) rootUtils::GetDefaultBinNumber();
   char resolution[80];
   snprintf(resolution,80,"Energy Resolution %.1lf meV ",res*1.e3);
   std::cout << resolution << "\n";

   // map time to energy
   Double_t dt,energy,energy_max=0.;
   Int_t counts=0.;
   for(Int_t b=1; b<=dumpHisto.at(0).at(0)->GetNbinsX(); ++b)
   {
      dt = (dumpHisto.at(0).at(0)->GetBinCenter(b)-startOffset);
      energy = dumpRamp->Eval(dt);
      counts = dumpHisto.at(0).at(0)->GetBinContent(b);
      if(energy>energy_max && counts>10) energy_max=energy; // interesting bins only
#if DEBUG > 1
      std::cout<<b<<"\t"<<dt<<"\t"<<energy<<"\t"<<dumpHisto.at(0).at(0)->GetBinContent(b)<<std::endl;
#endif
      hEnergy->Fill(energy, counts);
   }

   // calculate the total number of counts in the intresting range
   Int_t binmin = hEnergy->FindBin(Emin),
   binmax = hEnergy->FindBin(energy_max);
   Double_t total_counts = hEnergy->Integral(binmin,binmax);
   char integral[80];
   snprintf(integral,80,"Integral %1.01f",total_counts);
   std::cout << integral << std::endl;

   delete gc;
   
   double time = RampTmin;
   double zeroX = -1;
   while(time<RampTmin || zeroX<0)
   {
      if(dumpRamp->Eval(time) < 1e-8)
         zeroX=time;
      else
         time+=0.01;
   }
   std::cout << "Voltage hits zero (< 1e-8) at " << zeroX << "s accurate to 0.01s \n";

   edouble beforeZero = (edouble)Count_SIS_Triggers(runNumber, channel, {start_time+startOffset}, {start_time+startOffset+zeroX});
   edouble totalCounts = (edouble)Count_SIS_Triggers(runNumber, channel, {start_time+startOffset}, {stop_time});
   edouble fractionCounts = beforeZero/totalCounts;

   std::cout << "Fraction before = " << fractionCounts << "\n";

   char fraction[80];
   snprintf(fraction,80,"Fraction before 0: %.3f #pm %.3f (%i/%i)",fractionCounts.value, fractionCounts.error, 
            (int)beforeZero.value, (int)totalCounts.value);


   // information
   TPaveText* tt = new TPaveText(0.1,0.5,0.9,0.9);
   tt->SetFillColor(0);
   tt->AddText(resolution);
   tt->AddText(integral);
   tt->AddText(fraction);


   TCanvas* cEnergy = new TCanvas("AntiprotonTemperature","AntiprotonTemperature",1800,1000);
   cEnergy->Divide(2,2);
   cEnergy->cd(1);
   gPad->SetLogy(1);
   dumpHisto_toPlot.at(0).at(0)->Draw("HIST");
   double height = dumpHisto_toPlot.at(0).at(0)->GetMaximum();
   TLine *l = new TLine(zeroX, 0, zeroX, height);
   l->SetLineColor(kRed);
   l->SetLineWidth(2);
   l->Draw("same");


   cEnergy->cd(2);
   hEnergy->GetXaxis()->SetRangeUser(0,EnergyRangeFactor*(hEnergy->GetMean()));
   hEnergy->Draw("E1 HIST");
   gPad->SetLogy(1);
   cEnergy->cd(3);
   dumpRamp->Draw();
   cEnergy->cd(4);
   tt->Draw();

   //necessary for fitting
   gc=cEnergy;
   gh=hEnergy;
   return cEnergy;

}
#endif

#if BUILD_A2
TCanvas* Plot_A2_CT_ColdDump(Int_t runNumber, int dump_index, Int_t binNumber, 
                          const char* dumpFile,
                          Double_t EnergyRangeFactor)
{
   std::string ramp;
   if (!dumpFile)
   {
      TA2Spill dump = Get_A2_Spills(runNumber,{"Cold Dump"},{dump_index}).front();
      double length = dump.GetStopTime() - dump.GetStartTime();
      if ( length < 0.6)
         ramp = "ana/macros/ColdDumpE4E5.dump";
      else if ( fabs(length - 1.) < 0.1 )
         ramp = "ana/macros/ColdDumpE4E5_1s.dump";
      else if ( fabs(length - 2.) < 0.1 )
         ramp = "ana/macros/ColdDumpE4E5_2s.dump";
      else if ( fabs(length - 4.) < 0.1 )
         ramp = "ana/macros/ColdDumpE4E5_4s.dump";
      else if (fabs(length - 10.) < 0.1 )
         ramp = "ana/macros/ColdDumpE4E5_10s.dump";
   }
   else
   {
      ramp = dumpFile;
   }
   return Plot_A2_ColdDump(runNumber, dump_index, binNumber, ramp.c_str(), EnergyRangeFactor, "CT_SiPM_OR");
}

TCanvas* Plot_A2_RCT_ColdDump(Int_t runNumber,int dump_index, Int_t binNumber, 
                              const char* dumpFile,
                              Double_t EnergyRangeFactor, const char* sis_chan)
{
   std::string ramp;
   if (!dumpFile)
   {
      TA2Spill dump = Get_A2_Spills(runNumber,{"Cold Dump"},{dump_index}).front();
      if ( dump.GetStopTime() - dump.GetStartTime() >1 )
         ramp = "alpha2/macros/ColdDump_E5E6_2000ms_withOffsets_20220923.dat";
      else
         ramp = "alpha2/macros/ColdDump_E5E6_500ms_withOffsets_20141105.dat";
   }
   else
      ramp = dumpFile;
   std::cout <<"ramp: " << ramp <<"\n";
   return Plot_A2_ColdDump( runNumber, dump_index, binNumber, ramp.c_str(), EnergyRangeFactor, sis_chan);
}

TCanvas* Plot_A2_PreMix_ColdDump(Int_t runNumber,int dump_index, Int_t binNumber, 
                          const char* dumpFile,
                          Double_t EnergyRangeFactor, const char* sis_chan)
{
   return Plot_A2_ColdDump( runNumber, dump_index, binNumber, dumpFile, EnergyRangeFactor, sis_chan);
}


#endif

#if BUILD_A2
TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<double> tmin, std::vector<double> tmax)
{
   TCanvas* c = new TCanvas();
   c->Update();
   c->Draw();
   AlphaColourWheel colour;
   TLegend* legend = new TLegend(0.1,0.7,0.48,0.9);

   // Get all spils in the total range of all windows
   std::vector<TA2Spill> spills = Get_A2_Spills(
      runNumber,
      *std::min_element(tmin.begin(),tmin.end()),
      *std::max_element(tmax.begin(),tmax.end())
   );
   std::vector<TLine*> spill_lines;
   std::vector<TText*> spill_text;

   std::vector<TH1D*> hh=Get_Summed_SIS(runNumber, SIS_Channel,tmin, tmax);
   double max_height = 0;
   double min_height = 1E99;
   for (TH1D* h: hh)
   {
      double min, max;
      h->GetMinimumAndMaximum(min, max);
      if (max > max_height)
      {
         max_height = max;
      }
      if (min < min_height)
      {
         min_height = min;
      }
   }
   if (min_height < 10 && min_height >= 0)
      min_height = 0;
   for (size_t i=0; i<hh.size(); i++)
   {
      legend->AddEntry(hh[i]);
      hh[i]->SetLineColor(colour.GetNewColour());
      if (i ==0 )
      {
         hh[i]->GetYaxis()->SetRangeUser(min_height,max_height);
         hh[i]->Draw("HIST");
      }
      else
      {
         hh[i]->GetYaxis()->SetRangeUser(min_height,max_height);
         hh[i]->Draw("HIST SAME");
      }

      for (const TA2Spill& s: spills) {
         const double min = tmin[i];
         const double max = tmax[i];
         const SEQUENCER_ID sqn = static_cast<SEQUENCER_ID>(s.fSeqData->fSequenceNum);
         if (!rootUtils::GetDumpMarkerDisplayMode(sqn))
            continue;


         if ( s.GetStartTime() > min && s.GetStartTime() < max)
         {
            spill_lines.emplace_back(
               new TLine(
                  s.GetStartTime() - min,
                  min_height, 
                  s.GetStartTime() - min,
                  max_height
               )
            );
            spill_lines.back()->SetLineColor(kGreen );
            spill_lines.back()->DrawClone();

            spill_text.emplace_back(
               new TText(
                  s.GetStartTime() - min,
                  min_height,
                  s.GetSanitisedName().c_str()
               )
            );
            spill_text.back()->SetTextSize(100);
            spill_text.back()->SetTextAngle(45);
            spill_text.back()->Draw();
         }

         if (s.GetStopTime() > min && s.GetStopTime() < max)
         {
            spill_lines.emplace_back(
               new TLine(
                  s.GetStopTime() - min,
                  min_height, 
                  s.GetStopTime() - min,
                  max_height
               )
            );
            spill_lines.back()->SetLineColor(kRed );
            spill_lines.back()->DrawClone();
         }
      }
   }
   //std::cout<<"min:"<< min_height <<"\tmax:"<<max_height <<std::endl;
   legend->Draw();
   c->Update();
   c->Draw();
   return c;
}

TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<double> tmin, std::vector<double> tmax)
{
   std::vector<TSISChannel> chans = GetSISChannels(runNumber, SIS_Channel_Names);
   return Plot_Summed_SIS(runNumber, chans, tmin, tmax);
}

TCanvas* Plot_SIS_on_pulse(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<std::pair<double,int>> SIS_Counts,double tstart, double tstop)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   for (auto& a: SIS_Counts)
   {
     tmin.push_back(a.first + tstart);
     tmax.push_back(a.first + tstop);
   }
   return Plot_Summed_SIS(runNumber, SIS_Channel_Names, tmin, tmax);
}

TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<TA2Spill> spills)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   for (TA2Spill& s: spills)
   {
      tmin.push_back(s.GetStartTime());
      tmax.push_back(s.GetStopTime());
   }
   return Plot_Summed_SIS(runNumber,SIS_Channel,tmin, tmax);
}

TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<TA2Spill> spills)
{
   std::vector<TSISChannel> chans = GetSISChannels(runNumber, SIS_Channel_Names);
   return Plot_Summed_SIS(runNumber, chans, spills);
}

TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TA2Spill> s=Get_A2_Spills(runNumber,description,dumpIndex);
   return Plot_Summed_SIS(runNumber, SIS_Channel, s);
}

TCanvas* Plot_Summed_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TSISChannel> chans = GetSISChannels(runNumber, SIS_Channel_Names);
   return Plot_Summed_SIS( runNumber, chans, description, dumpIndex);
}


TCanvas* Plot_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<double> tmin, std::vector<double> tmax)
{
   TCanvas* c = new TCanvas();
   c->Update();
   c->Draw();
   AlphaColourWheel colour;
   //TLegend* legend = new TLegend(0.1,0.7,0.48,0.9);
   TLegend* legend = new TLegend(0.75,0.5,0.99,0.69);

   // Get all spils in the total range of all windows
   std::vector<TA2Spill> spills = Get_A2_Spills(
      runNumber,
      *std::min_element(tmin.begin(),tmin.end()),
      *std::max_element(tmax.begin(),tmax.end())
   );
   std::vector<TLine*> spill_lines;
   std::vector<TText*> spill_text;

   std::vector<std::vector<TH1D*>> hh=Get_SIS(runNumber, SIS_Channel,tmin, tmax);
   double max_height = 0;
   double min_height = 1E99;
   for (auto times: hh)
      for (TH1D* h: times)
      {
         double min, max;
         h->GetMinimumAndMaximum(min, max);
         if (max > max_height)
         {
            max_height = max;
         }
         if (min < min_height)
         {
            min_height = min;
         }
      }
   if (min_height < 10 && min_height >= 0)
      min_height = 0;
   max_height*=1.1;
   for (size_t j=0; j<tmin.size(); j++)
      for (size_t i=0; i<SIS_Channel.size(); i++)
      {
         legend->AddEntry(hh[i][j]);
         hh[i][j]->SetLineColor(colour.GetNewColour());
         if (i ==0 && j == 0)
         {
            hh[i][j]->GetYaxis()->SetRangeUser(min_height,max_height);
            hh[i][j]->Draw("HIST");
         }
         else
         {
            hh[i][j]->GetYaxis()->SetRangeUser(min_height,max_height);
            hh[i][j]->Draw("HIST SAME");
         }

         for (const TA2Spill& s: spills) {
           const double min = tmin[j];
           const double max = tmax[j];
           const SEQUENCER_ID sqn = static_cast<SEQUENCER_ID>(s.fSeqData->fSequenceNum);
           if (!rootUtils::GetDumpMarkerDisplayMode(sqn))
              continue;
           if ( s.GetStartTime() > min && s.GetStartTime() < max)
           {
              spill_lines.emplace_back(
                 new TLine(
                    s.GetStartTime() - min,
                    min_height, 
                    s.GetStartTime() - min,
                    max_height
                 )
              );
              spill_lines.back()->SetLineColor(kGreen );
              spill_lines.back()->DrawClone();
              
              spill_text.emplace_back(
                 new TText(
                    s.GetStartTime() - min,
                    min_height,
                    s.GetSanitisedName().c_str()
                 )
              );
              //spill_text.back()->SetTextSize(100);
              spill_text.back()->SetTextAngle(45);
              spill_text.back()->Draw();
           }

           if (s.GetStopTime() > min && s.GetStopTime() < max)
           {
              spill_lines.emplace_back(
                 new TLine(
                    s.GetStopTime() - min,
                    min_height, 
                    s.GetStopTime() - min,
                    max_height
                 )
              );
              spill_lines.back()->SetLineColor(kRed );
              spill_lines.back()->DrawClone();
              
              spill_text.emplace_back(
                 new TText(
                    s.GetStartTime() - min,
                    min_height,
                    s.GetSanitisedName().c_str()
                 )
              );
              spill_text.back()->SetTextAngle(45);
              spill_text.back()->Draw();
           }
         }
      }
   //std::cout<<"min:"<< min_height <<"\tmax:"<<max_height <<std::endl;
   legend->Draw();
   c->Update();
   c->Draw();
   return c;
}

TCanvas* Plot_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<double> tmin, std::vector<double> tmax)
{
   std::vector<TSISChannel> chans = GetSISChannels(runNumber, SIS_Channel_Names);
   return Plot_SIS(runNumber, chans, tmin, tmax);
}

TCanvas* Plot_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<TA2Spill> spills)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   for (TA2Spill& s: spills)
   {
      tmin.push_back(s.GetStartTime());
      tmax.push_back(s.GetStopTime());
   }
   return Plot_SIS(runNumber,SIS_Channel,tmin, tmax);
}

TCanvas* Plot_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<TA2Spill> spills)
{
   std::vector<TSISChannel> chans = GetSISChannels(runNumber, SIS_Channel_Names);
   return Plot_SIS(runNumber, chans, spills);
}

TCanvas* Plot_SIS(Int_t runNumber, std::vector<TSISChannel> SIS_Channel, std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TA2Spill> s=Get_A2_Spills(runNumber,description,dumpIndex);
   return Plot_SIS(runNumber, SIS_Channel, s);
}

TCanvas* Plot_SIS(Int_t runNumber, std::vector<std::string> SIS_Channel_Names, std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TSISChannel> chans = GetSISChannels(runNumber, SIS_Channel_Names);
   return Plot_SIS( runNumber, chans, description, dumpIndex);
}

#endif
#if BUILD_A2
void Plot_SVD(int runNumber, std::vector<double> tmin, std::vector<double> tmax, int cutsMode)
{
   TA2Plot* Plot=new TA2Plot();
   Plot->AddTimeGates(runNumber,tmin,tmax);
   //Slow part, read all data in 1 pass over each tree so is efficient
   Plot->LoadData();
   TCanvas* c=Plot->DrawCanvas("cVTX",cutsMode);
   c->Draw();
}

void Plot_SVD(Int_t runNumber, std::vector<TA2Spill> spills, int cutsMode)
{
   std::vector<double> tmin;
   std::vector<double> tmax;
   for (auto & spill: spills)
   {
      if (spill.fScalerData)
      {
         tmin.push_back(spill.fScalerData->fStartTime);
         tmax.push_back(spill.fScalerData->fStopTime);
      }
      else
      {
         std::cout<<"Spill didn't have Scaler data!? Was there an aborted sequence?"<<std::endl;
      }
   }
   return Plot_SVD(runNumber,tmin,tmax, cutsMode);
}

void Plot_SVD(Int_t runNumber, std::vector<std::string> description, std::vector<int> dumpIndex, int cutsMode)
{
   std::vector<TA2Spill> s=Get_A2_Spills(runNumber,description,dumpIndex);
   return Plot_SVD(runNumber,s, cutsMode);
}
#endif

#if BUILD_A2
void Plot_feGEM(Int_t runNumber, std::vector<std::string> description, std::vector<int> dumpIndex, std::string category, std::string variable, int feGEMindex)
{
   
   std::string name = TAPlotFEGEMData::CombinedName(category, variable);
   std::vector<TA2Spill> s = Get_A2_Spills(runNumber,description,dumpIndex);
   std::vector<double> x,y;
   for (const TA2Spill& ss: s)
   {
      std::vector<std::pair<double,double>> data = GetFEGData(runNumber,name.c_str(), ss.GetStartTime(), ss.GetStopTime(), feGEMindex);
      for (const std::pair<double,double>& d: data)
      {
          x.emplace_back(d.first);
          y.emplace_back(d.second);
      }
      // In the future do the plotting for all data types!
   /*
   if (tree->GetBranchStatus("TStoreGEMData<double>"))
      LoadFEGEMData<double>(obj, feGEMReader, "TStoreGEMData<double>", firstTime, lastTime);
    else if (tree->GetBranchStatus("TStoreGEMData<float>"))
         LoadFEGEMData<float>(obj, feGEMReader, "TStoreGEMData<float>", firstTime, lastTime);
      else if (tree->GetBranchStatus("TStoreGEMData<bool>"))
         LoadFEGEMData<bool>(obj, feGEMReader, "TStoreGEMData<bool>", firstTime, lastTime);
      else if (tree->GetBranchStatus("TStoreGEMData<int32_t>"))
         LoadFEGEMData<int32_t>(obj, feGEMReader, "TStoreGEMData<int32_t>", firstTime, lastTime);
      else if (tree->GetBranchStatus("TStoreGEMData<uint32_t>"))
         LoadFEGEMData<uint32_t>(obj, feGEMReader, "TStoreGEMData<uint32_t>", firstTime, lastTime);
      else if (tree->GetBranchStatus("TStoreGEMData<uint16_t>"))
         LoadFEGEMData<uint16_t>(obj, feGEMReader, "TStoreGEMData<uint16_t>", firstTime, lastTime);
      else if (tree->GetBranchStatus("TStoreGEMData<char>"))
         LoadFEGEMData<char>(obj, feGEMReader, "TStoreGEMData<char>", firstTime, lastTime);
      else
         std::cout << "Warning unable to find TStoreGEMData type" << std::endl;   
   }*/
   
   }
   TCanvas c1;
   TGraph g(x.size(),x.data(),y.data());
   g.Draw();
   c1.Draw();
}
#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
