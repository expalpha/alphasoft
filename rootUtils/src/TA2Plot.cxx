#ifdef BUILD_A2

#include "TA2Plot.h"
#include <fstream>
/*! \class TA2Plot
    \brief Class for plotting vertex data in ALPHA 2

ClassImp(TA2Plot)

Child class of TAPlot, works as a Data Frame for vertex data

Example usage, plot cosmic data in run
~~~~{.cpp}
TA2Plot a;
// Add a time window to the container
a.AddTimeGate(61122,0.,GetA2TotalRunTime(61122));
// Load data into container (SVD and SIS data)
a.LoadData();
// Draw the plot (optional)
a.DrawCanvas();
// Dump out contents of container to CSV files
a.ExportCSV("R61122_entirerun");
~~~~
*/

/// @brief Constructor
/// @param zeroTime If true, the time axis is 'zeroed' allowing plotting time around a defined zero time
///
/// Each time window has its own zero time, allowing for complex plotting like events around a LyALPHA pulse
TA2Plot::TA2Plot(bool zeroTime) : TAPlot(zeroTime)
{
   fZMinCut = -99999.;
   fZMaxCut = 99999.;
}

/// @brief Constructor with z cut
/// @param zMin 
/// @param zMax 
/// @param zeroTime 
TA2Plot::TA2Plot(double zMin, double zMax, bool zeroTime) : TAPlot(zeroTime)
{
   fZMinCut = zMin;
   fZMaxCut = zMax;
}

/// Copy Constructor
TA2Plot::TA2Plot(const TA2Plot &object)
   : TAPlot(object), fSISChannels(object.fSISChannels), fTrig(object.fTrig), fTrigNobusy(object.fTrigNobusy),
     fAtomOr(object.fAtomOr), fCATStart(object.fCATStart), fCATStop(object.fCATStop), fRCTStart(object.fRCTStart),
     fRCTStop(object.fRCTStop), fATMStart(object.fATMStart), fATMStop(object.fATMStop),
     fBeamInjection(object.fBeamInjection), fBeamEjection(object.fBeamEjection), SISEvents(object.SISEvents)
{
   fZMinCut = object.fZMinCut;
   fZMaxCut = object.fZMaxCut;
}

/// @brief Deconstructor
TA2Plot::~TA2Plot() {}

/// = assignment operator
TA2Plot &TA2Plot::operator=(const TA2Plot &rhs)
{
   TAPlot::operator=(rhs);
   // Inherited TAPlot members
   // std::cout << "TA2Plot equals operator" << std::endl;
   fSISChannels   = rhs.fSISChannels;
   fTrig          = rhs.fTrig;
   fTrigNobusy    = rhs.fTrigNobusy;
   fAtomOr        = rhs.fAtomOr;
   fCATStart      = rhs.fCATStart;
   fCATStop       = rhs.fCATStop;
   fRCTStart      = rhs.fRCTStart;
   fRCTStop       = rhs.fRCTStop;
   fATMStart      = rhs.fATMStart;
   fATMStop       = rhs.fATMStop;
   fBeamInjection = rhs.fBeamInjection;
   fBeamEjection  = rhs.fBeamEjection;
   fZMinCut       = rhs.fZMinCut;
   fZMaxCut       = rhs.fZMaxCut;
   SISEvents      = rhs.SISEvents;

   return *this;
}

/// @brief += operator
/// @param rhs 
/// @return 
TA2Plot &TA2Plot::operator+=(const TA2Plot &rhs)
{
   // This calls the parent += operator first.
   TAPlot::operator+=(rhs);

   // For all these copying A is fine.
   fTrig.insert(rhs.fTrig.begin(), rhs.fTrig.end());
   fTrigNobusy.insert(rhs.fTrigNobusy.begin(), rhs.fTrigNobusy.end());
   fAtomOr.insert(rhs.fAtomOr.begin(), rhs.fAtomOr.end());
   fCATStart.insert(rhs.fCATStart.begin(), rhs.fCATStart.end());
   fCATStop.insert(rhs.fCATStop.begin(), rhs.fCATStop.end());
   fRCTStart.insert(rhs.fRCTStart.begin(), rhs.fRCTStart.end());
   fRCTStop.insert(rhs.fRCTStop.begin(), rhs.fRCTStop.end());
   fATMStart.insert(rhs.fATMStart.begin(), rhs.fATMStart.end());
   fATMStop.insert(rhs.fATMStop.begin(), rhs.fATMStop.end());
   fBeamInjection.insert(rhs.fBeamInjection.begin(), rhs.fBeamInjection.end());
   fBeamEjection.insert(rhs.fBeamEjection.begin(), rhs.fBeamEjection.end());
   fMWSynthStart.insert(rhs.fMWSynthStart.begin(), rhs.fMWSynthStart.end());
   fMWSynthStop.insert(rhs.fMWSynthStop.begin(), rhs.fMWSynthStop.end());

   // Vectors need concacting.
   fSISChannels.insert(fSISChannels.end(), rhs.fSISChannels.begin(), rhs.fSISChannels.end());

   // Object addition.
   SISEvents += rhs.SISEvents;

   return *this;
}

/// @brief Addition operator
/// @param lhs 
/// @param rhs 
/// @return 
///
/// Add the contents of lhs and rhs and return a new TA2Plot object
TA2Plot &operator+(const TA2Plot &lhs, const TA2Plot &rhs)
{
   TA2Plot *basePlot = new TA2Plot();
   *basePlot += lhs;
   *basePlot += rhs;
   return *basePlot;
}

/// @brief Set default SIS channels
/// @param runNumber 
void TA2Plot::SetSISChannels(int runNumber)
{
   TSISChannels *sisChannels = new TSISChannels(runNumber);
   fTrig.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("IO32_TRIG")));
   fTrigNobusy.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("IO32_TRIG_NOBUSY")));
   fAtomOr.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("ATOM_SiPM_OR")));
   fBeamInjection.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("SIS_AD")));
   fBeamEjection.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("SIS_AD_2")));
   fCATStart.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("SIS_PBAR_DUMP_START")));
   fCATStop.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("SIS_PBAR_DUMP_STOP")));
   fRCTStart.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("SIS_RECATCH_DUMP_START")));
   fRCTStop.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("SIS_RECATCH_DUMP_STOP")));
   fATMStart.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("SIS_ATOM_DUMP_START")));
   fATMStop.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("SIS_ATOM_DUMP_STOP")));
   fMWSynthStart.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("MIC_SYNTH_STEP_START")));
   fMWSynthStop.insert(std::pair<int, TSISChannel>(runNumber, sisChannels->GetChannel("MIC_SYNTH_STEP_STOP")));

   // Add all valid SIS channels to a list for later:
   fSISChannels.clear();
   if (fTrig.find(runNumber)->second.IsValid()) fSISChannels.push_back(fTrig.find(runNumber)->second);
   if (fTrigNobusy.find(runNumber)->second.IsValid()) fSISChannels.push_back(fTrigNobusy.find(runNumber)->second);
   if (fAtomOr.find(runNumber)->second.IsValid()) fSISChannels.push_back(fAtomOr.find(runNumber)->second);
   if (fBeamInjection.find(runNumber)->second.IsValid()) fSISChannels.push_back(fBeamInjection.find(runNumber)->second);
   if (fBeamEjection.find(runNumber)->second.IsValid()) fSISChannels.push_back(fBeamEjection.find(runNumber)->second);
   if (fCATStart.find(runNumber)->second.IsValid()) fSISChannels.push_back(fCATStart.find(runNumber)->second);
   if (fCATStop.find(runNumber)->second.IsValid()) fSISChannels.push_back(fCATStop.find(runNumber)->second);
   if (fRCTStart.find(runNumber)->second.IsValid()) fSISChannels.push_back(fRCTStart.find(runNumber)->second);
   if (fRCTStop.find(runNumber)->second.IsValid()) fSISChannels.push_back(fRCTStop.find(runNumber)->second);
   if (fATMStart.find(runNumber)->second.IsValid()) fSISChannels.push_back(fATMStart.find(runNumber)->second);
   if (fATMStop.find(runNumber)->second.IsValid()) fSISChannels.push_back(fATMStop.find(runNumber)->second);
   if (fMWSynthStart.find(runNumber)->second.IsValid()) fSISChannels.push_back(fMWSynthStart.find(runNumber)->second);
   if (fMWSynthStop.find(runNumber)->second.IsValid()) fSISChannels.push_back(fMWSynthStop.find(runNumber)->second);
   delete sisChannels;
   return;
}

/// @brief Add a single SVD event to container
/// @param SVDEvent 
///
/// Do not add if it fails a z cut
int TA2Plot::AddSVDEvent(const TSVD_QOD &SVDEvent)
{
   int events_added = 0;
   const double time = SVDEvent.GetTimeOfEvent();
   if (SVDEvent.Z() < fZMinCut) return events_added;
   if (SVDEvent.Z() >= fZMaxCut) return events_added;

   const int index = GetTimeWindows().GetValidWindowNumber(SVDEvent.GetRunNumber(),time);


   // Checks to make sure GetValidWindowNumber hasn't returned -1 (in which case it will be ignored) and
   // if not it will add the event.
   if (index >= 0) {
      events_added += AddEvent(SVDEvent, GetTimeWindows().ZeroTime(index));
   }
   // Event not added
   return events_added;
}

/// @brief Add a single SIS event to container
/// @param SISEvent 
int TA2Plot::AddSISEvent(const TSISEvent &SISEvent)
{
   int events_added = 0;
   const size_t numSISChannels = fSISChannels.size();
   const double time           = SISEvent.GetRunTime();

   // Loop over all time windows
   const int index = GetTimeWindows().GetValidWindowNumber(SISEvent.GetRunNumber(),time);
   if (index >= 0) {
      for (size_t i = 0; i < numSISChannels; i++) {
         int counts = SISEvent.GetCountsInChannel(fSISChannels[i]);
         if (counts) {
            events_added += AddEvent(SISEvent, fSISChannels[i], GetTimeWindows().ZeroTime(index));
         }
      }
   }
   return events_added;
}

/// @brief Add SVD event to container
/// @param event 
/// @param timeOffset used for 'zerotime' mode
int TA2Plot::AddEvent(const TSVD_QOD &event, const double timeOffset)
{
   const double tMinusOffset = (event.GetTimeOfEvent() - timeOffset);
   AddVertexEvent(
      event.GetRunNumber(),
      event.GetEventNumber(),
      event.GetPassedCuts(),
      event.GetVertexStatus(),
      event.X(),
      event.Y(),
      event.Z(),
      tMinusOffset,
      event.GetVF48Timestamp(),
      event.GetTimeOfEvent(),
      -1,
      event.GetNTracks(),
      event.GetRFOut()); ///Not implemented for ALPHA2 yet.
   return 1; // One event added
}

/// @brief Add SIS event to container
/// @param event 
/// @param channel 
/// @param timeOffset used for 'zerotime' mode
int TA2Plot::AddEvent(const TSISEvent &event, const TSISChannel &channel, const double timeOffset)
{
   SISEvents.AddEvent(event.GetRunNumber(), event.GetRunTime() - timeOffset, event.GetRunTime(),
                      event.GetCountsInChannel(channel), channel);
   return 1; // One event added
}

/// @brief Add dump gates to container
/// @param runNumber 
/// @param description 
/// @param dumpIndex 
///
/// Implmentation of parents pure virtual function
void TA2Plot::AddDumpGates(const int runNumber, const std::vector<std::string> description, std::vector<int> dumpIndex)
{
   std::vector<TA2Spill> spills = Get_A2_Spills(runNumber, description, dumpIndex);
   return AddDumpGates(runNumber, spills);
}

/// @brief Add time gates to container with TA2Spills
/// @param runNumber 
/// @param spills 
void TA2Plot::AddDumpGates(const int runNumber, const std::vector<TA2Spill> spills)
{
   std::vector<double> minTime;
   std::vector<double> maxTime;

   for (auto &spill : spills) {
      if (spill.fScalerData) {
         minTime.push_back(spill.fScalerData->fStartTime);
         maxTime.push_back(spill.fScalerData->fStopTime);
      } else {
         std::cout << "Spill didn't have Scaler data!? Was there an aborted sequence?" << std::endl;
      }
   }
   return TA2Plot::AddTimeGates(runNumber, minTime, maxTime);
}


/// @brief Add time gates to container with TA2Spills
/// @param spills 
///
/// If spills are from one run, it is slightly faster to call the function above: AddDumpGates(const int runNumber, const std::vector<TA2Spill> spills)
void TA2Plot::AddDumpGates(const std::vector<TA2Spill> spills)
{
   for (const TA2Spill &spill : spills) {
      if (spill.fScalerData) {
         AddTimeGate(spill.fRunNumber, spill.GetStartTime(), spill.GetStopTime());
      } else {
         std::cout << "Spill didn't have Scaler data!? Was there an aborted sequence?" << std::endl;
      }
   }
   return;
}

/// @brief Wrap add time window to detect -ve tmax and replace it with the 'End Of Run' time
void TA2Plot::AddTimeGate(const int runNumber, const double tmin, const double tmax, const double tzero)
{

   if (tmax < 0)
      return AddTimeWindow(runNumber,tmin,GetTotalRunTimeFromSIS(runNumber),tzero);
   else
      return AddTimeWindow(runNumber,tmin,tmax,tzero);
}

/// @brie Load SVD and SIS data into container for a single run
/// @param runNumber 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
void TA2Plot::LoadRun(int runNumber, double firstTime, double lastTime, int verbose)
{
   if (verbose > 0) std::cout << "Loading run " << runNumber << "\n";

   bool AlreadyHaveAnalysisReport = false;
   for (const TA2AnalysisReport &r : fAnalysisReports) {
      if (r.GetRunNumber() == runNumber) {
         AlreadyHaveAnalysisReport = true;
         break;
      }
   }
   if (!AlreadyHaveAnalysisReport) {
      fAnalysisReports.emplace_back(Get_A2Analysis_Report(runNumber));
   }

   // TTreeReaders are buffered... so this is faster than iterating over a TTree by hand
   // More performance is maybe available if we use DataFrames...
   TTreeReaderPointer SVDReader = Get_A2_SVD_Tree(runNumber);
   TTreeReaderValue<TSVD_QOD> SVDEvent(*SVDReader, "OfficialTime");
   // I assume that file IO is the slowest part of this function...
   // so get multiple channels and multiple time windows in one pass
   while (SVDReader->Next()) {
      const double t = SVDEvent->GetTimeOfEvent();
      if (t < firstTime) continue;
      if (t > lastTime) break;
      AddSVDEvent(*SVDEvent);
   }

   // TTreeReaders are buffered... so this is faster than iterating over a TTree by hand
   // More performance is maybe available if we use DataFrames...
   SetSISChannels(runNumber);

   for (int sis_module_no = 0; sis_module_no < NUM_SIS_MODULES; sis_module_no++) {
      TTreeReaderPointer SISReader = A2_SIS_Tree_Reader(runNumber, sis_module_no);
      TTreeReaderValue<TSISEvent> SISEvent(*SISReader, "TSISEvent");
      // I assume that file IO is the slowest part of this function...
      // so get multiple channels and multiple time windows in one pass
      while (SISReader->Next()) {
         const double t = SISEvent->GetRunTime();
         if (t < firstTime) continue;
         if (t > lastTime) break;
         AddSISEvent(*SISEvent);
      }
   }
}

/// @brief Configure the TA2Plot histograms
void TA2Plot::SetUpHistograms()
{
   const double XMAX(4.), YMAX(4.), RMAX(4.), ZMAX(30.);

   double minTime;
   double maxTime;
   if (kZeroTimeAxis) {
      minTime = GetBiggestTBeforeZero();
      maxTime = GetBiggestTAfterZero();
   } else {
      minTime = GetFirstTmin();
      maxTime = GetLastTmax();
   }

   AddHistogram("zvtx", new TH1D((GetTAPlotTitle() + "_zvtx").c_str(), "Z Vertex;z [cm];events",
                                 rootUtils::GetDefaultBinNumber(), -ZMAX, ZMAX));

   TH1D *rHisto = new TH1D((GetTAPlotTitle() + "_rvtx").c_str(), "R Vertex;r [cm];events",
                           rootUtils::GetDefaultBinNumber(), 0., RMAX);
   rHisto->SetMinimum(0);
   AddHistogram("rvtx", rHisto);

   TH1D *phiHisto = new TH1D((GetTAPlotTitle() + "_phivtx").c_str(), "phi Vertex;phi [rad];events",
                             rootUtils::GetDefaultBinNumber(), -TMath::Pi(), TMath::Pi());
   phiHisto->SetMinimum(0);
   AddHistogram("phivtx", phiHisto);

   TH2D *xyHisto =
      new TH2D((GetTAPlotTitle() + "_xyvtx").c_str(), "X-Y Vertex;x [cm];y [cm]", rootUtils::GetDefaultBinNumber(),
               -XMAX, XMAX, rootUtils::GetDefaultBinNumber(), -YMAX, YMAX);
   AddHistogram("xyvtx", xyHisto);

   TH2D *zrHisto = new TH2D((GetTAPlotTitle() + "_zrvtx").c_str(), "Z-R Vertex;z [cm];r [cm]",
                            rootUtils::GetDefaultBinNumber(), -ZMAX, ZMAX, rootUtils::GetDefaultBinNumber(), 0., RMAX);
   AddHistogram("zrvtx", zrHisto);

   TH2D *zphiHisto = new TH2D((GetTAPlotTitle() + "_zphivtx").c_str(), "Z-Phi Vertex;z [cm];phi [rad]",
                              rootUtils::GetDefaultBinNumber(), -ZMAX, ZMAX, rootUtils::GetDefaultBinNumber(),
                              -TMath::Pi(), TMath::Pi());
   AddHistogram("zphivtx", zphiHisto);
   std::string units;
   if (GetMaxDumpLength() < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber) {
      fTimeFactor = 1000;
      units       = "[ms]";
   } else {
      fTimeFactor = 1;
      units       = "[s]";
   }

   // SIS channels:
   TH1D *triggers     = nullptr;
   TH1D *readTriggers = nullptr;
   TH1D *atomOr       = nullptr;
   TH1D *tHisto       = nullptr;
   TH2D *ztHisto      = nullptr;
   switch (rootUtils::GetTimeAxisMode()) {
   case rootUtils::kFixedTimeBinNumber:
      triggers =
         new TH1D((GetTAPlotTitle() + "_tIO32_nobusy").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                  rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      readTriggers = new TH1D((GetTAPlotTitle() + "_tIO32").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                              rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      atomOr  = new TH1D((GetTAPlotTitle() + "_tAtomOR").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                         rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      tHisto  = new TH1D((GetTAPlotTitle() + "_tvtx").c_str(), (std::string("t Vertex;t ") + units + ";events").c_str(),
                         rootUtils::GetTimeBinNumber(), minTime * fTimeFactor, maxTime * fTimeFactor);
      ztHisto = new TH2D((GetTAPlotTitle() + "_ztvtx").c_str(), (std::string("Z-T Vertex;z [cm];t ") + units).c_str(),
                         rootUtils::GetTimeBinNumber(), -ZMAX, ZMAX, rootUtils::GetTimeBinNumber(),
                         minTime * fTimeFactor, maxTime * fTimeFactor);
      break;
   case rootUtils::kFixedTimeBinSize:
      triggers =
         new TH1D((GetTAPlotTitle() + "_tIO32_nobusy").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                  rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      readTriggers = new TH1D((GetTAPlotTitle() + "_tIO32").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                              rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      atomOr  = new TH1D((GetTAPlotTitle() + "_tAtomOR").c_str(), (std::string("t;t ") + units + ";events").c_str(),
                         rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      tHisto  = new TH1D((GetTAPlotTitle() + "_tvtx").c_str(), (std::string("t Vertex;t ") + units + ";events").c_str(),
                         rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      ztHisto = new TH2D((GetTAPlotTitle() + "_ztvtx").c_str(), (std::string("Z-T Vertex;z [cm];t ") + units).c_str(),
                         rootUtils::GetTimeBinNumber(), -ZMAX, ZMAX,
                         rootUtils::CalculateNumberOfBinsInRange(minTime, maxTime), minTime, maxTime);
      break;
   default: std::cerr << "Unknown TimeAxisMode from rootUtils\n"; break;
   }

   if (triggers) {
      triggers->SetMarkerColor(kRed);
      triggers->SetLineColor(kRed);
      triggers->SetMinimum(0);
      AddHistogram("tIO32_nobusy", triggers);
   }

   if (readTriggers) {
      readTriggers->SetMarkerColor(kViolet);
      readTriggers->SetLineColor(kBlue);
      readTriggers->SetMinimum(0);
      AddHistogram("tIO32", readTriggers);
   }

   if (atomOr) {
      atomOr->SetMarkerColor(kGreen);
      atomOr->SetLineColor(kGreen);
      atomOr->SetMinimum(0);
      AddHistogram("tAtomOR", atomOr);
   }

   if (tHisto) {
      tHisto->SetLineColor(kMagenta);
      tHisto->SetMarkerColor(kMagenta);
      tHisto->SetMinimum(0);
      AddHistogram("tvtx", tHisto);
   }
   if (ztHisto) {
      AddHistogram("ztvtx", ztHisto);
   }
   return;
}

/// @brief Fill the TA2Plot histograms
/// @param CutsMode 
void TA2Plot::FillHisto(const std::string &CutsMode)
{
   TAPlotCutsMode applyCuts(CutsMode);

   ClearHisto();
   SetUpHistograms();
   const double kMaxDumpLength = GetMaxDumpLength();

   // Fill SIS histograms
   int runNum = 0;
   for (size_t i = 0; i < SISEvents.size(); i++) {
      if (SISEvents.RunNumber(i) != runNum) {
         runNum = SISEvents.RunNumber(i);
         SetSISChannels(runNum);
      }
      double time;
      if (kZeroTimeAxis)
         time = SISEvents.Time(i);
      else
         time = SISEvents.OfficialTime(i);
      if (kMaxDumpLength < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber)
         time = time * 1000.;
      const TSISChannel channel         = SISEvents.SISChannel(i);
      const int         countsInChannel = SISEvents.Counts(i);
      const int         runNumber       = SISEvents.RunNumber(i);
      if (channel == fTrig.find(runNumber)->second)
         FillHistogram("tIO32", time, countsInChannel);
      else if (channel == fTrigNobusy.find(runNumber)->second)
         FillHistogram("tIO32_nobusy", time, countsInChannel);
      else if (channel == fAtomOr.find(runNumber)->second)
         FillHistogram("tAtomOR", time, countsInChannel);
      else if (channel == fBeamInjection.find(runNumber)->second)
         AddInjection(time);
      else if (channel == fBeamEjection.find(runNumber)->second)
         AddEjection(time);
      else if (channel == fCATStart.find(runNumber)->second || channel == fRCTStart.find(runNumber)->second ||
               channel == fATMStart.find(runNumber)->second)
         AddStopDumpMarker(time);
      else if (channel == fCATStop.find(runNumber)->second || channel == fRCTStop.find(runNumber)->second ||
               channel == fATMStop.find(runNumber)->second)
         AddStartDumpMarker(time);
      else
         std::cout << "Unconfigured SIS channel in TA2Plot" << std::endl;
   }

   // Fill Vertex Histograms
   TVector3                  vertex;
   const TAPlotVertexEvents &kVertexEvents = GetVertexEvents();
   // for (auto& vtxevent: GetVertexEvents())
   for (size_t i = 0; i < kVertexEvents.size(); i++) {
      Double_t time;
      if (kZeroTimeAxis)
         time = kVertexEvents.Times(i);
      else
         time = kVertexEvents.RunTime(i);
      if (kMaxDumpLength < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber)
         time = time * 1000.;
      vertex = fVertexEvents.VertexVector(i);
      FillHistogram("tSVD", time);
      bool PassCuts = applyCuts.ApplyCuts(fVertexEvents.CutsResults(i));
      if (!PassCuts) 
         continue;
      // Passed cut result!
      FillHistogram("tvtx", time);
      if (kVertexEvents.VertexStatus(i) <= 0) continue; // Don't draw invaid vertices
      FillHistogram("phivtx", vertex.Phi());
      FillHistogram("zphivtx", vertex.Z(), vertex.Phi());
      // FillHistogram("phitvtx",vtx.Phi(),time);
      FillHistogram("xyvtx", vertex.X(), vertex.Y());
      FillHistogram("zvtx", vertex.Z());
      FillHistogram("rvtx", vertex.Perp());
      FillHistogram("zrvtx", vertex.Z(), vertex.Perp());
      FillHistogram("ztvtx", vertex.Z(), time);
   }
   TH1D *rHisto = GetTH1D("rvtx");
   if (rHisto) {
      TH1D *rDensityHisto = (TH1D *)rHisto->Clone("radial density");
      rDensityHisto->Sumw2();
      TF1 *function = new TF1("fr", "x", -100, 100);
      rDensityHisto->Divide(function);
      rDensityHisto->Scale(rHisto->GetBinContent(rHisto->GetMaximumBin()) /
                           rDensityHisto->GetBinContent(rDensityHisto->GetMaximumBin()));
      rDensityHisto->SetMarkerColor(kRed);
      rDensityHisto->SetLineColor(kRed);
      delete function;
      AddHistogram("rdens", rDensityHisto);
   }
}

/// @brief Fill the TA2Plot histograms
/// @param CutsMode 
void TA2Plot::FillHisto(int CutsMode)
{

   ClearHisto();
   SetUpHistograms();
   const double kMaxDumpLength = GetMaxDumpLength();

   // Fill SIS histograms
   int runNum = 0;
   for (size_t i = 0; i < SISEvents.size(); i++) {
      if (SISEvents.RunNumber(i) != runNum) {
         runNum = SISEvents.RunNumber(i);
         SetSISChannels(runNum);
      }
      double time;
      if (kZeroTimeAxis)
         time = SISEvents.Time(i);
      else
         time = SISEvents.OfficialTime(i);
      if (kMaxDumpLength < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber)
         time = time * 1000.;
      const TSISChannel channel         = SISEvents.SISChannel(i);
      const int         countsInChannel = SISEvents.Counts(i);
      const int         runNumber       = SISEvents.RunNumber(i);
      if (channel == fTrig.find(runNumber)->second)
         FillHistogram("tIO32", time, countsInChannel);
      else if (channel == fTrigNobusy.find(runNumber)->second)
         FillHistogram("tIO32_nobusy", time, countsInChannel);
      else if (channel == fAtomOr.find(runNumber)->second)
         FillHistogram("tAtomOR", time, countsInChannel);
      else if (channel == fBeamInjection.find(runNumber)->second)
         AddInjection(time);
      else if (channel == fBeamEjection.find(runNumber)->second)
         AddEjection(time);
      else if (channel == fCATStart.find(runNumber)->second || channel == fRCTStart.find(runNumber)->second ||
               channel == fATMStart.find(runNumber)->second)
         AddStopDumpMarker(time);
      else if (channel == fCATStop.find(runNumber)->second || channel == fRCTStop.find(runNumber)->second ||
               channel == fATMStop.find(runNumber)->second)
         AddStartDumpMarker(time);
      else
         std::cout << "Unconfigured SIS channel in TA2Plot" << std::endl;
   }

   // Fill Vertex Histograms
   TVector3                  vertex;
   const TAPlotVertexEvents &kVertexEvents = GetVertexEvents();
   // for (auto& vtxevent: GetVertexEvents())
   for (size_t i = 0; i < kVertexEvents.size(); i++) {
      Double_t time;
      if (kZeroTimeAxis)
         time = kVertexEvents.Times(i);
      else
         time = kVertexEvents.RunTime(i);
      if (kMaxDumpLength < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber)
         time = time * 1000.;
      vertex = fVertexEvents.VertexVector(i);
      FillHistogram("tSVD", time);
      if (not kVertexEvents.CutsResults(i).at(CutsMode)) 
         continue;
      // Passed cut result!
      FillHistogram("tvtx", time);
      if (kVertexEvents.VertexStatus(i) <= 0) continue; // Don't draw invaid vertices
      FillHistogram("phivtx", vertex.Phi());
      FillHistogram("zphivtx", vertex.Z(), vertex.Phi());
      // FillHistogram("phitvtx",vtx.Phi(),time);
      FillHistogram("xyvtx", vertex.X(), vertex.Y());
      FillHistogram("zvtx", vertex.Z());
      FillHistogram("rvtx", vertex.Perp());
      FillHistogram("zrvtx", vertex.Z(), vertex.Perp());
      FillHistogram("ztvtx", vertex.Z(), time);
   }
   TH1D *rHisto = GetTH1D("rvtx");
   if (rHisto) {
      TH1D *rDensityHisto = (TH1D *)rHisto->Clone("radial density");
      rDensityHisto->Sumw2();
      TF1 *function = new TF1("fr", "x", -100, 100);
      rDensityHisto->Divide(function);
      rDensityHisto->Scale(rHisto->GetBinContent(rHisto->GetMaximumBin()) /
                           rDensityHisto->GetBinContent(rDensityHisto->GetMaximumBin()));
      rDensityHisto->SetMarkerColor(kRed);
      rDensityHisto->SetLineColor(kRed);
      delete function;
      AddHistogram("rdens", rDensityHisto);
   }
}

/// @brief Draw the TA2Plot
/// @param name   Give name to canvas and histograms (can avoid the memory leak warnings to unique names per plot)
/// @param CutsMode Apply cuts to the drawn data
/// @return 
TCanvas *TA2Plot::DrawCanvas(const char *name, int CutsMode)
{
   SetTAPlotTitle(name);
   std::cout << "TAPlot Processing time : ~" << GetApproximateProcessingTime() << "s" << std::endl;
   TCanvas *canvas = new TCanvas(name, name, 1800, 1000);
   FillHisto(CutsMode);

   // Scale factor to scale down to ms:
   if (GetMaxDumpLength() < SCALECUT && rootUtils::GetTimeAxisMode() == rootUtils::kFixedTimeBinNumber)
      SetTimeFactor(1000.);
   canvas->Divide(4, 2);

   // Canvas 1
   canvas->cd(1);
   DrawHistogram("zvtx", "HIST E1");

   // Canvas 2
   canvas->cd(2); // Z-counts (with electrodes?)4
   TVirtualPad *subPadCD2 = canvas->cd(2);
   gPad->Divide(1, 2);
   // Canvas 2 - Pad 1
   subPadCD2->cd(1);
   DrawHistogram("rvtx", "HIST E1");
   DrawHistogram("rdens", "HIST E1 SAME");
   TPaveText *radialDensityLabel = new TPaveText(0.6, 0.8, 0.90, 0.85, "NDC NB");
   radialDensityLabel->AddText("radial density [arbs]");
   radialDensityLabel->SetTextColor(kRed);
   radialDensityLabel->SetFillStyle(0);
   radialDensityLabel->SetLineStyle(0);
   radialDensityLabel->Draw();
   // Canvas 2 - Pad 2
   subPadCD2->cd(2);
   DrawHistogram("phivtx", "HIST E1");

   // Canvas 3
   canvas->cd(3); // T-counts
   TLegend *legend = NULL;
   DrawHistogram("tIO32_nobusy", "HIST");
   legend = AddLegendIntegral(legend, "Triggers: %5.0lf", "tIO32_nobusy");
   DrawHistogram("tIO32", "HIST SAME");
   legend = AddLegendIntegral(legend, "Reads: %5.0lf", "tIO32");
   DrawHistogram("tAtomOR", "HIST SAME");
   legend = AddLegendIntegral(legend, "AtomOR: %5.0f", "tAtomOR");

   if (CutsMode == 2) {
      DrawHistogram("tmva", "HIST SAME");
      legend = AddLegendIntegral(legend, "MVA: %5.0lf", "tmva");
   } else if (CutsMode == 1) {
      DrawHistogram("tvtx", "HIST SAME");
      legend = AddLegendIntegral(legend, "Pass Cuts: %5.0lf", "tvtx");
   } else if (CutsMode == 0) {
      DrawHistogram("tvtx", "HIST SAME");
      legend = AddLegendIntegral(legend, "Vertices: %5.0lf", "tvtx");
   }
   legend = DrawLines(legend, "tIO32_nobusy");

   // Canvas 4
   canvas->cd(4);
   DrawHistogram("xyvtx", "colz");

   // Canvas 5
   canvas->cd(5);
   DrawHistogram("ztvtx", "colz");

   // Canvas 6
   canvas->cd(6);
   TVirtualPad *subPadCD6 = NULL;
   if (IsGEMData() && IsLVData()) {
      subPadCD6 = canvas->cd(6);
      gPad->Divide(1, 2);
   }
   if (subPadCD6)
      subPadCD6->cd(1);

   if (IsGEMData())
   {
      std::pair<TLegend*,TMultiGraph*> gemDataMG = GetGEMGraphs();
      if (gemDataMG.first)
      {
         //Draw TMultigraph
         gemDataMG.second->Draw("AL*");
         // Draw legend
         gemDataMG.first->Draw();
      }
   }
   if (subPadCD6) 
      subPadCD6->cd(2);
   if (IsLVData())
   {
      std::pair<TLegend*,TMultiGraph*> labviewDataMG = GetLVGraphs();
      if (labviewDataMG.first)
      {
         //Draw TMultigraph
         labviewDataMG.second->Draw("AL*");
         //Draw legend
         labviewDataMG.first->Draw();
      }
   }
   // Canvas 7
   canvas->cd(7);
   DrawHistogram("tvtx", "HIST SAME");

   // Canvas 8
   canvas->cd(8);
   DrawHistogram("zphivtx", "colz");
   // Cuts applied
   if (CutsMode > 0) {
      canvas->cd(1);
      TPaveText *applyCutsLabel = new TPaveText(0., 0.95, 0.20, 1.0, "NDC NB");
      if (CutsMode > 1)
         applyCutsLabel->AddText("RF cut applied");
      else
         applyCutsLabel->AddText("Cuts applied");
      applyCutsLabel->SetTextColor(kRed);
      applyCutsLabel->SetFillColor(kWhite);
      applyCutsLabel->Draw();
   }

   // Canvas 0 - Global
   canvas->cd(0);
   TString runText = GetTAPlotTitle();
   runText += " | Run(s): ";
   runText += GetListOfRuns();
   runText += " | ";
   runText += std::to_string(GetTotalTime());
   runText += "s | ";
   if (fZMinCut != -std::numeric_limits<double>::infinity())
      runText += std::string("Z > ") + fZMinCut + " | ";
   if (fZMaxCut != std::numeric_limits<double>::infinity())
      runText += std::string("Z < ") + fZMaxCut + " | ";
   runText += rootUtils::GetDefaultBinNumber();
   runText += " bins | ";

   switch (rootUtils::GetTimeAxisMode()) {
   case rootUtils::kFixedTimeBinNumber:
      runText += rootUtils::GetTimeBinNumber();
      runText += " time bins";
      break;
   case rootUtils::kFixedTimeBinSize:
      runText += rootUtils::GetTimeBinSize();
      runText += "s time bins";
      break;
   default: std::cerr << "Unknown TimeAxisMode from rootUtils\n"; break;
   
   }
   TLatex *runsLabel = new TLatex(0., 0., runText);
   runsLabel->SetTextSize(0.016);
   runsLabel->Draw();

   std::cout << runText << std::endl;
   return canvas;
}



/// @brief Export container to CSV
/// @param filename
/// @param option select what data gets exported to csv
///
/// options (order doesn't matter):
/// "v" for vertex (filename.vertex.csv)
/// "t" for timewindows (filename.timewindows.csv)
/// "a" for AnalysisReport (filename.AnalysisReport.csv)
/// "s" for scaler, ie SIS (filename.scaler.csv
void TA2Plot::ExportCSV(const std::string filename, const std::string options)
{
   // Save Time windows and vertex data
   TAPlot::ExportCSV(filename);

   // Save A2 AnalysisReport data if 'a' is set in options
   if (options.find('a') != std::string::npos)
   {
      std::string   analysisReportFilename = filename + ".AnalysisReport.csv";
      std::ofstream aReport;
      aReport.open(analysisReportFilename);
      aReport << fAnalysisReports.front().CSVTitleLine();
      for (const TA2AnalysisReport &r : fAnalysisReports) aReport << r.CSVLine();
      aReport.close();
      std::cout << analysisReportFilename << " saved\n";
   }
   // Save Scaler Data if 's' is set in options
   if (options.find('s') != std::string::npos)
   {
      std::string   scalerFilename = filename + ".scaler.csv";
      std::ofstream sis;
      sis.open(scalerFilename);
      sis << SISEvents.CSVTitleLine();
      for (size_t i = 0; i < SISEvents.size(); i++) sis << SISEvents.CSVLine(i);
      sis.close();
      std::cout << scalerFilename << " saved\n";
   }
}

/// @brief Reset entire container and remove all vertex data (time windows untouched)
void TA2Plot::Reset()
{
   fSISChannels.clear();
   fTrig.clear();
   fTrigNobusy.clear();
   fAtomOr.clear();
   fCATStart.clear();
   fCATStop.clear();
   fRCTStart.clear();
   fRCTStop.clear();
   fATMStart.clear();
   fATMStop.clear();
   fBeamInjection.clear();
   fBeamEjection.clear();
   fAnalysisReports.clear();
   SISEvents.Clear();
}


#endif
