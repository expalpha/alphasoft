
#ifdef BUILD_A2
#include "TA2SpillGetters.h"
#include "DoubleGetters.h"


//Welp, this is broken in jupyter... why?
//----> 4 ROOT.Get_A2_Spillsa(45000,["Mixing"],[0])
//
//SystemError: vector<TA2Spill> ::Get_A2_Spillsa(int runNumber, _object* description, _object* dumpIndex) =>
//    problem in C++; program state has been reset

#ifdef HAVE_PYTHON
std::vector<TA2Spill> Get_A2_Spills(int runNumber, PyObject* description, PyObject* dumpIndex)
{
    std::vector<std::string> desc = listTupleToVector_String(description);
    std::vector<int> reps= listTupleToVector_Int( dumpIndex);
    return Get_A2_Spills(runNumber,desc, reps);
}
#endif

std::vector<TA2Spill> Get_A2_Spills(int runNumber, std::vector<std::string> description, std::vector<int> dumpIndex)
{

   // The TA2Spill Tree is small... 
   TTreeReaderPointer reader=Get_A2SpillTree(runNumber);
   TTreeReaderValue<TA2Spill> spill(*reader, "TA2Spill");

   assert(description.size()==dumpIndex.size());
   std::vector<int> match_counter(description.size());
   for (size_t i=0; i<description.size(); i++)
      match_counter[i]=0;

   std::vector<TA2Spill> spills;

   while (reader->Next())
   {
      //std::cout<<"Name:"<<spill->Name.c_str()<<std::endl;
      for (size_t i=0; i<description.size(); i++)
      {
         if (spill->IsMatchForDumpName(description[i]))
         {
            //spill->Print();
            //This TTreeReader value is odd... the dereferencing 
            //overload * is doing something special... so I need to 
            //dereference then get the pointer... then type cast it...
            //b->Print();
            if (dumpIndex.at(i)<0)
            {
               match_counter[i]++;
               //Copy spill into returned vector
               spills.push_back(*spill); //This cast is sketchy...
            }
            else if (dumpIndex.at(i)==match_counter[i]++)
               spills.push_back(*spill);
            else
               continue;
         } 
      }
   }
   for (size_t i = 0; i < match_counter.size(); i++)
   {
      if (match_counter[i] == 0)
         std::cerr << "Warning: No dumps match \"" << description[i] << "\" in run " << runNumber << "\n";
   }
      if (spills.empty())
      std::cerr << "Warning: No spills found\n";
   return spills;
}

std::vector<TA2Spill> Get_All_A2_Spills(int runNumber)
{
   return Get_A2_Spills(runNumber,{"*"},{-1});
}

std::vector<TA2Spill> Get_A2_Spills(const int runNumber, const double start_time, double stop_time)
{
   if (stop_time < 0.)
   {
      stop_time = GetA2TotalRunTime(runNumber);
   }
   
   // The TA2Spill Tree is small... 
   TTreeReaderPointer reader=Get_A2SpillTree(runNumber);
   TTreeReaderValue<TA2Spill> spill(*reader, "TA2Spill");

   std::vector<TA2Spill> spills;

   while (reader->Next())
   {
      if ( spill->GetStartTime() < start_time && spill->GetStopTime() < start_time )
         continue;
      // Future optimisation may be to change this to a break... needs testing
      if ( spill->GetStartTime() > stop_time && spill->GetStopTime() > stop_time )
         continue;
      spills.push_back(*spill);
   }
   if (spills.empty())
      std::cerr << "Warning: No spills found between " << start_time << " and " << stop_time << " in run " << runNumber << "\n";
   return spills;
}

#endif
