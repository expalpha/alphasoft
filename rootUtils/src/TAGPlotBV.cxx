#if BUILD_AG
#include "TAGPlotBV.h"
#include "TVector2.h"
#include "TProfile.h"
#include "THStack.h"

/// @brief Constructor
/// @param zeroTime
TAGPlotBV::TAGPlotBV(bool zeroTime) : TAGPlot(zeroTime) {
   fLoadStoreEvent = false;
}

/// @brief Constructor
/// @param zMin
/// @param zMax
/// @param zeroTime
TAGPlotBV::TAGPlotBV(double zMin, double zMax, bool zeroTime) : TAGPlot(zMin, zMax, zeroTime) {
   fLoadStoreEvent = false;
}

/// @brief Copy constructor
/// @param object 
TAGPlotBV::TAGPlotBV(const TAGPlotBV &object)
   : TAGPlot(object), fPlotBarEvents(object.fPlotBarEvents)
{
   fLoadStoreEvent = false;
}

/// @brief Deconstructor
TAGPlotBV::~TAGPlotBV()
{
   Reset();
}

void TAGPlotBV::Reset()
{
   TAGPlot::Reset();
   fPlotBarEvents.clear();
}

/// @brief = operator
/// @param rhs 
/// @return 
TAGPlotBV &TAGPlotBV::operator=(const TAGPlotBV &rhs)
{
   TAGPlot::operator=(rhs);
   fPlotBarEvents               = rhs.fPlotBarEvents;
   return *this;
}

/// @brief += operator
/// @param rhs 
/// @return 
TAGPlotBV &TAGPlotBV::operator+=(const TAGPlotBV &rhs)
{
   // This calls the parent += operator first.
   TAGPlot::operator+=(rhs);
   fPlotBarEvents += rhs.fPlotBarEvents;
   return *this;
}

/// @brief Addition operator
/// @param lhs
/// @param rhs
/// @return
///
/// Add the contents of the lhs and rhs and return a new TAGPlot object
TAGPlotBV &operator+(const TAGPlotBV &lhs, const TAGPlotBV &rhs)
{
   TAGPlotBV *basePlot = new TAGPlotBV;
   *basePlot += lhs;
   *basePlot += rhs;
   return *basePlot;
}

/// @brief Add TStoreEvent to container
/// @param event 
/// @param timeOffset 
void TAGPlotBV::AddEvent(const TBarEvent &barEvent, const int runNumber, const double timeOffset, const double vertexZ)
{
   fPlotBarEvents.AddEvent(runNumber, barEvent.GetRunTime()-timeOffset, barEvent.GetRunTime(),
               barEvent.GetNumberOfMatchedTracks(), barEvent.GetNumberOfUnmatchedTracks(), barEvent.GetNumberOfPileUpTracks(), barEvent, vertexZ);
   return;
}

/// @brief Load TStoreEvent and Chronobox data into container for a single run
/// @param runNumber 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
void TAGPlotBV::LoadRun(const int runNumber, const double firstTime, const double lastTime, int verbose)
{
   if (!fLoadStoreEvent) {
      TTreeReaderPointer BVTreeReader = Get_BarEvent_TreeReader(runNumber);
      if (!BVTreeReader) return;
      TTreeReaderValue<TBarEvent> BVEvent(*BVTreeReader, "TBarEvent");
      BVTreeReader = FindStartOfTree(BVTreeReader,BVEvent,0,BVTreeReader->GetEntries(),firstTime,1.0);
      while (BVTreeReader->Next()) {
         const double t = BVEvent->GetRunTime();
         if (t < firstTime) continue;
         if (t > lastTime) break;
         AddBarEvent(*BVEvent, runNumber);
      }
   }
   if (fLoadStoreEvent) {
      TTreeReaderPointer TPCTreeReader = Get_StoreEvent_TreeReader(runNumber);
      if (!TPCTreeReader) return;
      TTreeReaderValue<TStoreEvent> TPCEvent(*TPCTreeReader, "StoredEvent");
      TPCTreeReader = FindStartOfTree(TPCTreeReader,TPCEvent,0,TPCTreeReader->GetEntries(),firstTime,1.0);
      while (TPCTreeReader->Next()) {
         const double t = TPCEvent->GetTimeOfEvent();
         if (t < firstTime) continue;
         if (t > lastTime) break;
         TBarEvent* barEvent = TPCEvent->GetBarEvent();
         if (!barEvent) continue;
         double vertexZ = TPCEvent->GetVertexZ();
         AddBarEvent(*barEvent, runNumber, vertexZ);
      }

   }
   LoadChronoEvents(runNumber, firstTime, lastTime, verbose);
}

/// @brief Add TBarEvent to container
/// @param event 
///
void TAGPlotBV::AddBarEvent(const TBarEvent &event, const int runNumber, const double vertexZ)
{
   double       time = event.GetRunTime();
   const int index = GetTimeWindows().GetValidWindowNumber(runNumber,time);

   if (index >= 0) {
      AddEvent(event, runNumber, GetTimeWindows().ZeroTime(index), vertexZ);
   }
}

/// @brief Setup BV histograms
void TAGPlotBV::SetupBarHistos()
{

   ClearHisto();

   double minTime;
   double maxTime;
   if (kZeroTimeAxis) {
      minTime = GetBiggestTBeforeZero();
      maxTime = GetBiggestTAfterZero();
   } else {
      minTime = GetFirstTmin();
      maxTime = GetLastTmax();
   }

   // bar histograms
   std::vector<TH1D*> th1ds;
   th1ds.push_back(new TH1D("hZTDCmZTPC", "Z TDC minus Z TPC;Z TDC - Z TPC (m)", 200, -0.5, 0.5));
   th1ds.push_back(new TH1D("hZADCmZTPC", "Z ADC minus Z TPC;Z ADC - Z TPC (m)", 200, -0.5, 0.5));
   th1ds.push_back(new TH1D("hZADCmZTDC", "Z ADC minus Z TDC;Z ADC - Z TDC (m)", 200, -0.5, 0.5));
   th1ds.push_back(new TH1D("hTOFmDist_zbv_global", "TOF - distance/c: Z from BV, global;TOF - distance/c (ns)", 200, -5., 5.));
   th1ds.push_back(new TH1D("hTOFmDist_ztpc_global", "TOF - distance/c: Z from TPC, global;TOF - distance/c (ns)", 200, -5., 5.));
   th1ds.push_back(new TH1D("hTOFmDist_zbv_bartobar", "TOF - distance/c: Z from BV, bar-to-bar;TOF - distance/c (ns)", 200, -5., 5.));
   th1ds.push_back(new TH1D("hTOFmDist_ztpc_bartobar", "TOF - distance/c: Z from TPC, bar-to-bar;TOF - distance/c (ns)", 200, -5., 5.));
   th1ds.push_back(new TH1D("hsignificance_zbv_global", "Significance: Z from BV, global;(TOF - distance/c) / sigma for cosmics", 200, -8., 8.));
   th1ds.push_back(new TH1D("hsignificance_ztpc_global", "Significance: Z from TPC, global;(TOF - distance/c) / sigma for cosmics", 200, -8., 8.));
   th1ds.push_back(new TH1D("hsignificance_zbv_bartobar", "Significance: Z from BV, bar-to-bar;(TOF - distance/c) / sigma for cosmics", 200, -8., 8.));
   th1ds.push_back(new TH1D("hsignificance_ztpc_bartobar", "Significance: Z from TPC, bar-to-bar;(TOF - distance/c) / sigma for cosmics", 200, -8., 8.));
   th1ds.push_back(new TH1D("hPileUpCounts","Number of Pile up events; Time (s)",50,minTime,maxTime));
   th1ds.push_back(new TH1D("hTimeCounts","Number of events; Time (s)",50,minTime,maxTime));
   th1ds.push_back(new TH1D("hPileUpDeltaT","Pile Up Events: Time between hits; Delta t (ns)",50,0,3500));
   th1ds.push_back(new TH1D("hPileUpDeltaTMatched","TPC Matched Pile Up Events: Time between hits; Delta t (ns)",50,0,3500));
   th1ds.push_back(new TH1D("hNTracks", "Number of TPC tracks; N tracks",21,-0.5,20.5));
   th1ds.push_back(new TH1D("hNTracksMatched", "Number of TPC tracks matching BV; N tracks matching BV",21,-0.5,20.5));
   th1ds.push_back(new TH1D("hNTracksMatchedMain", "Number of TPC tracks matching BV main group; N tracks matching BV main group",21,-0.5,20.5));

   std::vector<TH2D*> th2ds;
   th2ds.push_back(new TH2D("hZTDCvsZTPC", "Z TDC vs TPC;Z TPC (m);Z TDC (m)", 200, -2., 2., 200, -2., 2.));
   th2ds.push_back(new TH2D("hZADCvsZTPC", "Z ADC vs TPC;Z TPC (m);Z ADC (m)", 200, -2., 2., 200, -2., 2.));
   th2ds.push_back(new TH2D("hZADCvsZTDC", "Z ADC vs TDC;Z TDC (m);Z ADC (m)", 200, -2., 2., 200, -2., 2.));
   th2ds.push_back(new TH2D("hTOFvsDist_zbv_global", "TOF vs distance/c: Z from BV, global;Distance/c (ns);TOF (ns)", 200, 0., 5., 200, 0., 5.));
   th2ds.push_back(new TH2D("hTOFvsDist_ztpc_global", "TOF vs distance/c: Z from TPC, global;Distance/c (ns);TOF (ns)", 200, 0., 5., 200, 0., 5.));
   th2ds.push_back(new TH2D("hTOFvsDist_zbv_bartobar", "TOF vs distance/c: Z from BV, bar-to-bar;Distance/c (ns);TOF (ns)", 200, 0., 5., 200, 0., 5.));
   th2ds.push_back(new TH2D("hTOFvsDist_ztpc_bartobar", "TOF vs distance/c: Z from TPC, bar-to-bar;Distance/c (ns);TOF (ns)", 200, 0., 5., 200, 0., 5.));
   th2ds.push_back(new TH2D("hAmpMinvsTOFmDist","Minimum Amplitude vs TOF minus dist/c;Minimum Amplitude;TOF - distance/c (ns)",200,0,20000,200,-5.,5.));
   th2ds.push_back(new TH2D("hCombinedAmpvsTOFmDist","Combined Amplitude vs TOF minus dist/c;Combined Amplitude;TOF - distance/c (ns)",200,10,30,200,-5.,5.));
   th2ds.push_back(new TH2D("hProjvsTime","Projection along axis vs time;Projection (m);Time (ns)",200,-1.5,1.5,200,-10.,10.));
   th2ds.push_back(new TH2D("hZProjvsTime","Z position (minus hit average) vs time;Z position (m);Time (ns)",200,-1.5,1.5,200,-10.,10.));
   th2ds.push_back(new TH2D("hTOFvsDist", "TOF vs Distance (Calibrated); Distance between hits (m);Time of Flight (ns)",100,0.1,1.5,100,0,5));
   th2ds.push_back(new TH2D("hTOFvsDist_Uncalibrated", "TOF vs Distance (Uncalibrated);Distance between hits (m);Time of Flight (ns)",100,0.1,1.5,100,0,5));

   // Investigation histograms
   th2ds.push_back(new TH2D("hADCTimeDiffvsZTPC","ADC time diff vs Z TPC;Z TPC (m);ADC Time Diff (ns)",200,-1.5,1.5,20,-50,50.));
   th2ds.push_back(new TH2D("hZTDCvsTopAmp_ZTPC0","Z TDC vs Amp Top: ZTPC=0;Amp Top;Z from TDC (m)",200,0,20000,200,-1.5,1.5));
   th2ds.push_back(new TH2D("hZTDCvsBotAmp_ZTPC0","Z TDC vs Amp Bot: ZTPC=0;Amp Bot;Z from TDC (m)",200,0,20000,200,-1.5,1.5));
   th2ds.push_back(new TH2D("hZTDCvsTopAmp_ZTPCmp5","Z TDC vs Amp Top: ZTPC=-0.5;Amp Top;Z from TDC (m)",200,0,20000,200,-1.5,1.5));
   th2ds.push_back(new TH2D("hZTDCvsBotAmp_ZTPCmp5","Z TDC vs Amp Bot: ZTPC=-0.5;Amp Bot;Z from TDC (m)",200,0,20000,200,-1.5,1.5));
   th2ds.push_back(new TH2D("hZTDCvsTopAmp_ZTPCpp5","Z TDC vs Amp Top: ZTPC=0.5;Amp Top;Z from TDC (m)",200,0,20000,200,-1.5,1.5));
   th2ds.push_back(new TH2D("hZTDCvsBotAmp_ZTPCpp5","Z TDC vs Amp Bot: ZTPC=0.5;Amp Bot;Z from TDC (m)",200,0,20000,200,-1.5,1.5));
   th2ds.push_back(new TH2D("hZTPCvsCoarse","ZTPC vs coarse time difference;Z TPC (m);Coarse time difference (ns)",200,-1.5,1.5,24,-30.,30.));
   th2ds.push_back(new TH2D("hZTPCvsFine","ZTPC vs fine time difference;Z TPC (m);Fine time difference (ns)",200,-1.5,1.5,200,-30.,30.));
   th2ds.push_back(new TH2D("hZTPCvsChanCorrected","ZTPC vs channel-corrected time difference;Z TPC (m);Channel-corrected time difference (ns)",200,-1.5,1.5,200,-30.,30.));
   th2ds.push_back(new TH2D("hZTPCvsFinal","ZTPC vs final time difference;Z TPC (m);Final time difference (ns)",200,-1.5,1.5,200,-30.,30.));
   th1ds.push_back(new TH1D("hChannelOffset","Channel offset; offset (ns)",200,-30,30));
   th1ds.push_back(new TH1D("hNTDC","Number of TDC hits in event; TDC hits",11,-0.5,10.5));
   th2ds.push_back(new TH2D("hNTDCvs","Number of TDC hits in event; TDC hits; Z TDC - Z TPC (m)",11,-0.5,10.5,200,-0.5,0.5));

   // MVA variable histograms
   if (fPlotBarEvents.fBarEvents.size()>0) {
      TBarMVAVars mva(fPlotBarEvents.fBarEvents[0]);
      mva.CalculateVars();
      mva.EnforceValueRange(9.9);
      for (std::string varname: mva.GetVarNames()) {
         th1ds.push_back(new TH1D(Form("h_%s",varname.data()), Form("%s : %s",varname.data(),varname.data()), 200, -10., 10.));
         th2ds.push_back(new TH2D(Form("h_%s_vsZed",varname.data()), Form("%s : %s : Vertex Z (mm)",varname.data(),varname.data()), 100, -10., 10.,100,-2000,2000));
      }
   }

   for (TH1D* h1: th1ds) AddHistogram(h1->GetName(), h1);
   for (TH2D* h2: th2ds) AddHistogram(h2->GetName(), h2);

}

/// @brief Fill all histograms (include TAGPlot histograms)
/// @param CutsMode 
void TAGPlotBV::FillHisto(const std::string &CutsMode)
{

   ClearHisto();
   SetUpHistograms(CutsMode);

   FillChronoHistograms();
   // Fill Vertex Histograms
   FillVertexHistograms(CutsMode);

   FillBarHisto();
}

/// @brief Fill BV metric histograms
void TAGPlotBV::FillBarHisto(std::vector<int> barIDs)
{
   std::cout << "TAGPlotBV::FillTrackHisto() Number of histos: " << fHistos.GetEntries() << std::endl;

   for (size_t i = 0; i < fPlotBarEvents.size(); i++) {

      const TBarEvent& barEvt = fPlotBarEvents.fBarEvents[i];
      for (const TBarHit& h: barEvt.GetBarHits()) {
         if (barIDs.size()>0) {
            bool goodbar = false;
            for (int barID: barIDs) {
               if (barID==h.GetBarID()) {
                  goodbar = true;
                  break;
               }
            }
            if (!goodbar) continue;
         }
         if (!h.IsComplete()) continue;
         if (!h.HasTPCHit()) continue;

         // Zed histograms
         double ztpc = h.GetZedTPC();
         double ztdc = h.GetZed();
         double zadc = h.GetADCZed();
         FillHistogram("hZTDCvsZTPC",ztpc,ztdc);
         FillHistogram("hZTDCmZTPC",ztpc-ztdc);
         FillHistogram("hZADCvsZTPC",ztpc,zadc);
         FillHistogram("hZADCmZTPC",ztpc-zadc);
         FillHistogram("hZADCvsZTDC",ztdc,zadc);
         FillHistogram("hZADCmZTDC",ztdc-zadc);

         // Investigation histograms
         double amptop = h.GetTopADCHit().GetAmpFit();
         double ampbot = h.GetBotADCHit().GetAmpFit();
         double adctimebot = h.GetBotADCHit().GetStartTime();
         double adctimetop = h.GetTopADCHit().GetStartTime();
         FillHistogram("hADCTimeDiffvsZTPC",ztpc,adctimebot-adctimetop);
         if (ztpc<0.1 and ztpc>-0.1) {
            FillHistogram("hZTDCvsTopAmp_ZTPC0",amptop,ztdc);
            FillHistogram("hZTDCvsBotAmp_ZTPC0",ampbot,ztdc);
         }
         if (ztpc<0.6 and ztpc>0.4) {
            FillHistogram("hZTDCvsTopAmp_ZTPCpp5",amptop,ztdc);
            FillHistogram("hZTDCvsBotAmp_ZTPCpp5",ampbot,ztdc);
         }
         if (ztpc<-0.4 and ztpc>-0.6) {
            FillHistogram("hZTDCvsTopAmp_ZTPCmp5",amptop,ztdc);
            FillHistogram("hZTDCvsBotAmp_ZTPCmp5",ampbot,ztdc);
         }
         double dtcoarse = (h.GetBotTDCHit().GetTime()+h.GetBotTDCHit().GetFineTime()) - (h.GetTopTDCHit().GetTime()+h.GetTopTDCHit().GetFineTime());
         double dtfine = (h.GetBotTDCHit().GetTime()) - (h.GetTopTDCHit().GetTime());
         double dtchan = (h.GetBotTDCHit().GetTimeCali()) - (h.GetTopTDCHit().GetTimeCali());
         double dtfinal = (h.GetBotEndHit().GetTime()) - (h.GetTopEndHit().GetTime());
         double dtoffset = (h.GetBotTDCHit().GetTime()-h.GetBotTDCHit().GetTimeCali()) - (h.GetTopTDCHit().GetTime()-h.GetTopTDCHit().GetTimeCali());
         FillHistogram("hZTPCvsCoarse",ztpc,dtcoarse*1e9);
         FillHistogram("hZTPCvsFine",ztpc,dtfine*1e9);
         FillHistogram("hZTPCvsChanCorrected",ztpc,dtchan*1e9);
         FillHistogram("hZTPCvsFinal",ztpc,dtfinal*1e9);
         FillHistogram("hChannelOffset",dtoffset*1e9);

         int ntdc = 0;
         for (const TBarTDCHit& h: barEvt.GetTDCHits()) {
            if (h.IsGood()) ntdc++;
         }
         FillHistogram("hNTDC",ntdc);
         FillHistogram("hNTDCvs",ntdc,ztpc-ztdc);

      }

      // Pile up
      double time = fPlotBarEvents.fTime[i];
      FillHistogram("hTimeCounts",time);
      if (barEvt.IsPileUp()) FillHistogram("hPileUpCounts",time);
      if (barEvt.IsPileUp()) {
         double avg_pileup_dt = 0;
         int n_pileup_dt = 0;
         double avg_pileup_dt_matched = 0;
         int n_pileup_dt_matched = 0;
         for (const TBarTOF& tof: barEvt.GetTOFs() ) {
            if (!tof.IsComplete()) continue;
            if (tof.IsPileUp()) {
               avg_pileup_dt+=tof.GetTOF();
               n_pileup_dt++;
            }
            if (tof.IsPileUp() and tof.GetFirstHit().HasTPCHit() and tof.GetSecondHit().HasTPCHit()) {
               avg_pileup_dt_matched+=tof.GetTOF();
               n_pileup_dt_matched++;
            }
         }
         if (n_pileup_dt>0) FillHistogram("hPileUpDeltaT",1e9*avg_pileup_dt/n_pileup_dt);
         if (n_pileup_dt_matched>0)FillHistogram("hPileUpDeltaTMatched",1e9*avg_pileup_dt_matched/n_pileup_dt_matched);
      }

      // Directionality

      if (barEvt.GetNumBarsComplete()>=3) {

         // Finds first and last hit
         double first_time = 0; double last_time = 0;
         TVector3 vfirst, vlast;
         for (const TBarHit& h: barEvt.GetBarHits()) {
            if (!h.IsComplete()) continue;
            double time = h.GetTime()*1e9 - barEvt.GetEventTDCTime()*1e9;
            if (first_time==0 or time<first_time) {
               first_time = time;
               vfirst = h.Get3Vector();
            }
            if (last_time==0 or time>last_time) {
               last_time = time;
               vlast = h.Get3Vector();
            }
         }

         // Finds main axis, i.e. the line that connects first and last hit
         TVector3 axis = vlast - vfirst;
         
         std::vector<double> aproj, atime, zproj;
         double avg_proj = 0; double avg_time = 0; double avg_z = 0;
         int n_hits = 0;
         for (const TBarHit& h: barEvt.GetBarHits()) {
            if (!h.IsComplete()) continue;
            double time = h.GetTime()*1e9 - barEvt.GetEventTDCTime()*1e9;
            TVector3 vec = h.Get3Vector();
            // Projection along main axis
            double proj = vec.Dot(axis)/(axis.Dot(axis));
            aproj.push_back(proj); avg_proj+=proj;
            atime.push_back(time); avg_time+=time;
            zproj.push_back(vec.z()); avg_z+=vec.z();
            n_hits++;
         }
         avg_proj/=n_hits; avg_time/=n_hits; avg_z/=n_hits;
         for (int i=0; i<n_hits; i++) {
            FillHistogram("hProjvsTime",aproj[i]-avg_proj,atime[i]-avg_time);
            FillHistogram("hZProjvsTime",zproj[i]-avg_z,atime[i]-avg_time);
         }

      }

      // How many tracks match BV
      FillHistogram("hNTracks",fPlotBarEvents.fNTracksMatched[i]+fPlotBarEvents.fNTracksUnmatched[i]);
      FillHistogram("hNTracksMatched",fPlotBarEvents.fNTracksMatched[i]);
      FillHistogram("hNTracksMatchedMain",fPlotBarEvents.fNTracksMatched[i]-fPlotBarEvents.fNPileUpTracks[i]);

   }

   std::cout << "TAGPlotBV::FillBarHisto() Done filling hists" << std::endl;

}

/// @brief Fill BV metric histograms
void TAGPlotBV::FillMVAHisto()
{

   std::cout << "TAGPlotBV::FillMVAHisto() Filling histograms " << std::endl;
   for (size_t i = 0; i < fPlotBarEvents.size(); i++) {

      const TBarEvent& barEvt = fPlotBarEvents.fBarEvents[i];
      TBarMVAVars mva(barEvt);
      mva.CalculateVars();
      mva.EnforceValueRange(9.9);
      double vtxZ = fPlotBarEvents.fVertexZ[i];

      for (std::string varname: mva.GetVarNames()) {
         FillHistogram(Form("h_%s",varname.data()),mva.GetVar(varname));
         FillHistogram(Form("h_%s_vsZed",varname.data()),mva.GetVar(varname),vtxZ);
      }
   }
   std::cout << "TAGPlotBV::FillMVAHisto() Done filling histograms " << std::endl;

}

/// @brief Fill BV TOF metric histograms
void TAGPlotBV::FillTOFHisto(double min_dist, std::vector<int> barIDs)
{
   std::cout << "TAGPlotBV::FillTrackHisto() Number of histos: " << fHistos.GetEntries() << std::endl;

   for (size_t i = 0; i < fPlotBarEvents.size(); i++) {

      const TBarEvent& barEvt = fPlotBarEvents.fBarEvents[i];
      for (const TBarTOF& tof: barEvt.GetTOFs()) {
         if (!tof.IsComplete()) continue;
         if (!tof.MatchedToDifferentTPC()) continue;
         if (tof.GetDistanceOverC()<min_dist*1e-9) continue;

         // Filter only bar numbers in input list
         if (barIDs.size()>0) {
            bool goodbar1 = false;
            for (int barID: barIDs) {
               if (barID==tof.GetFirstHit().GetBarID()) {
                  goodbar1 = true;
                  break;
               }
            }
            if (!goodbar1) continue;
            bool goodbar2 = false;
            for (int barID: barIDs) {
               if (barID==tof.GetSecondHit().GetBarID()) {
                  goodbar2 = true;
                  break;
               }
            }
            if (!goodbar2) continue;
         }

         // Calculate minimum amplitude
         double min_amp = 9e9;
         if (tof.GetFirstHit().GetBotADCHit().GetAmpFit()<min_amp) min_amp = tof.GetFirstHit().GetBotADCHit().GetAmpFit();
         if (tof.GetFirstHit().GetTopADCHit().GetAmpFit()<min_amp) min_amp = tof.GetFirstHit().GetTopADCHit().GetAmpFit();
         if (tof.GetSecondHit().GetBotADCHit().GetAmpFit()<min_amp) min_amp = tof.GetSecondHit().GetBotADCHit().GetAmpFit();
         if (tof.GetSecondHit().GetTopADCHit().GetAmpFit()<min_amp) min_amp = tof.GetSecondHit().GetTopADCHit().GetAmpFit();
         if (min_amp==9e9) continue;

         // TOF histograms
         FillHistogram("hTOFmDist_zbv_global",1e9*tof.GetTOFmDist(true,true));
         FillHistogram("hTOFmDist_ztpc_global",1e9*tof.GetTOFmDist(false,true));
         FillHistogram("hTOFmDist_zbv_bartobar",1e9*tof.GetTOFmDist(true,false));
         FillHistogram("hTOFmDist_ztpc_bartobar",1e9*tof.GetTOFmDist(false,false));
         FillHistogram("hsignificance_zbv_global",tof.GetSignificance(true,true));
         FillHistogram("hsignificance_ztpc_global",tof.GetSignificance(false,true));
         FillHistogram("hsignificance_zbv_bartobar",tof.GetSignificance(true,false));
         FillHistogram("hsignificance_ztpc_bartobar",tof.GetSignificance(false,false));
         FillHistogram("hTOFvsDist_zbv_global",1e9*tof.GetDistanceOverC(true),1e9*tof.GetTOF(true));
         FillHistogram("hTOFvsDist_ztpc_global",1e9*tof.GetDistanceOverC(false),1e9*tof.GetTOF(true));
         FillHistogram("hTOFvsDist_zbv_bartobar",1e9*tof.GetDistanceOverC(true),1e9*tof.GetTOF(false));
         FillHistogram("hTOFvsDist_ztpc_bartobar",1e9*tof.GetDistanceOverC(false),1e9*tof.GetTOF(false));
         FillHistogram("hAmpMinvsTOFmDist",min_amp,1e9*tof.GetTOFmDist());
         FillHistogram("hCombinedAmpvsTOFmDist",tof.GetFirstHit().GetCombinedAmplitude(),1e9*tof.GetTOFmDist());
         FillHistogram("hCombinedAmpvsTOFmDist",tof.GetSecondHit().GetCombinedAmplitude(),1e9*tof.GetTOFmDist());

//         if (tof.GetOpeningAngle()>=4) {
        if (tof.GetOpeningAngle()>=4 and barEvt.GetNumBarsComplete()<=3 and tof.GetFirstHit().GetZed()>tof.GetSecondHit().GetZed()) {
            FillHistogram("hTOFvsDist",tof.GetDistance(),1e9*tof.GetTOF());
            double raw_t2 = 0.5*(tof.GetSecondHit().GetTopTDCHit().GetTime()+tof.GetSecondHit().GetBotTDCHit().GetTime());
            double raw_t1 = 0.5*(tof.GetFirstHit().GetTopTDCHit().GetTime()+tof.GetFirstHit().GetBotTDCHit().GetTime());
            FillHistogram("hTOFvsDist_Uncalibrated",tof.GetDistance(),1e9*(raw_t2-raw_t1));
         }

      }
   }

   std::cout << "TAGPlotBV::FillTrackHisto() Done filling hists" << std::endl;

}

TCanvas* TAGPlotBV::DrawZedCanvas(TString Name, std::vector<int> barIDs)
{
   SetupBarHistos();
   FillBarHisto(barIDs);

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1800);
   ct->Divide(3,2);

   ct->cd(1);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTDCvsZTPC")))->Draw("colz");
   ct->cd(4);
   gStyle->SetOptFit(1);
   TH1D* hZTDCmZTPC = ((TH1D *)fHistos.At(fHistoPositions.at("hZTDCmZTPC")));
   TF1* fitZTDCmZTPC = new TF1("fitZTDCmZTPC","gaus(0)+gaus(3)",-2.,2.);
   fitZTDCmZTPC->SetParameters(hZTDCmZTPC->GetEntries()/30.,0,0.05,hZTDCmZTPC->GetEntries()/100.,0,0.2);
   hZTDCmZTPC->Fit(fitZTDCmZTPC,"RQ0");
   hZTDCmZTPC->Draw("hist");
   fitZTDCmZTPC->Draw("same");
   ct->cd(2);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZADCvsZTPC")))->Draw("colz");
   ct->cd(5);
   gStyle->SetOptFit(1);
   TH1D* hZADCmZTPC = ((TH1D *)fHistos.At(fHistoPositions.at("hZADCmZTPC")));
   TF1* fitZADCmZTPC = new TF1("fitZADCmZTPC","gaus(0)+gaus(3)",-2.,2.);
   fitZADCmZTPC->SetParameters(hZADCmZTPC->GetEntries()/30.,0,0.05,hZADCmZTPC->GetEntries()/100.,0,0.2);
   hZADCmZTPC->Fit(fitZADCmZTPC,"RQ0");
   hZADCmZTPC->Draw("hist");
   fitZADCmZTPC->Draw("same");
   ct->cd(3);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZADCvsZTDC")))->Draw("colz");
   ct->cd(6);
   gStyle->SetOptFit(1);
   TH1D* hZADCmZTDC = ((TH1D *)fHistos.At(fHistoPositions.at("hZADCmZTDC")));
   TF1* fitZADCmZTDC = new TF1("fitZADCmZTDC","gaus(0)+gaus(3)",-2.,2.);
   fitZADCmZTDC->SetParameters(hZADCmZTDC->GetEntries()/30.,0,0.05,hZADCmZTDC->GetEntries()/100.,0,0.2);
   hZADCmZTDC->Fit(fitZADCmZTDC,"RQ0");
   hZADCmZTDC->Draw("hist");
   fitZADCmZTDC->Draw("same");

   return ct;
}

TCanvas* TAGPlotBV::DrawInvestigationCanvas(TString Name, std::vector<int> barIDs)
{
   SetupBarHistos();
   FillBarHisto(barIDs);

   /*
   TCanvas *ct = new TCanvas(Name, Name, 2000, 1200);
   ct->Divide(4,2);

   ct->cd(2);
   ((TH2D *)fHistos.At(fHistoPositions.at("hADCTimeDiffvsZTPC")))->Draw("colz");
   ct->cd(1);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTDCvsZTPC")))->Draw("colz");
   ct->cd(3);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTDCvsTopAmp_ZTPC0")))->Draw("colz");
   ct->cd(4);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTDCvsBotAmp_ZTPC0")))->Draw("colz");
   ct->cd(5);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTDCvsTopAmp_ZTPCpp5")))->Draw("colz");
   ct->cd(6);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTDCvsBotAmp_ZTPCpp5")))->Draw("colz");
   ct->cd(7);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTDCvsTopAmp_ZTPCmp5")))->Draw("colz");
   ct->cd(8);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTDCvsBotAmp_ZTPCmp5")))->Draw("colz");
   */

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1200);
   ct->Divide(3,2);

   ct->cd(1);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTDCvsZTPC")))->Draw("colz");
   ct->cd(2);
   ((TH1D *)fHistos.At(fHistoPositions.at("hZTDCmZTPC")))->Draw("hist");
   ct->cd(3);
   ((TH2D *)fHistos.At(fHistoPositions.at("hNTDCvs")))->Draw("colz");
   ct->cd(4);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTPCvsFine")))->Draw("colz");
   ct->cd(5);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZTPCvsCoarse")))->Draw("colz");
   ct->cd(6);
   ((TH1D *)fHistos.At(fHistoPositions.at("hNTDC")))->Draw("hist");

   return ct;
}

TCanvas* TAGPlotBV::DrawMVACanvas(TString Name)
{
   SetupBarHistos();
   FillMVAHisto();

   const int num_vars = TBarMVAVars::GetNumVars();
   const int sqrt_num_vars = TMath::Floor(TMath::Sqrt(num_vars));

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1800);
   ct->Divide(sqrt_num_vars+1,sqrt_num_vars);

   for (int i=0; i<num_vars; i++) {
      ct->cd(i+1);
      std::string varname = TBarMVAVars::GetVarName(i);
      ((TH1D *)fHistos.At(fHistoPositions.at(Form("h_%s",varname.data()))))->Draw();
   }

   return ct;
}

TCanvas* TAGPlotBV::DrawMVAvsZedCanvas(TString Name)
{
   SetupBarHistos();
   FillMVAHisto();

   const int num_vars = TBarMVAVars::GetNumVars();
   const int sqrt_num_vars = TMath::Floor(TMath::Sqrt(num_vars));

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1800);
   ct->Divide(sqrt_num_vars+1,sqrt_num_vars);

   for (int i=0; i<num_vars; i++) {
      ct->cd(i+1);
      std::string varname = TBarMVAVars::GetVarName(i);
      ((TH2D *)fHistos.At(fHistoPositions.at(Form("h_%s_vsZed",varname.data()))))->Draw("colz");
   }

   return ct;
}

TCanvas* TAGPlotBV::DrawTOFCanvas(TString Name, double min_dist, std::vector<int> barIDs)
{
   SetupBarHistos();
   FillTOFHisto(min_dist, barIDs);

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1800);
   ct->Divide(4,3);
   ct->cd(1);
   ((TH2D *)fHistos.At(fHistoPositions.at("hTOFvsDist_zbv_global")))->Draw("colz");
   ct->cd(2);
   ((TH2D *)fHistos.At(fHistoPositions.at("hTOFvsDist_ztpc_global")))->Draw("colz");
   ct->cd(3);
   ((TH2D *)fHistos.At(fHistoPositions.at("hTOFvsDist_zbv_bartobar")))->Draw("colz");
   ct->cd(4);
   ((TH2D *)fHistos.At(fHistoPositions.at("hTOFvsDist_ztpc_bartobar")))->Draw("colz");
   ct->cd(5);
   gStyle->SetOptFit(1);
   TH1D* hTOFmDist_zbv_global = ((TH1D *)fHistos.At(fHistoPositions.at("hTOFmDist_zbv_global")));
   TF1* fitTOFmDist_zbv_global = new TF1("fitTOFmDist_zbv_global","gaus(0)+gaus(3)",-5.,5.);
   fitTOFmDist_zbv_global->SetParameters(hTOFmDist_zbv_global->GetEntries()/30.,0,0.3,hTOFmDist_zbv_global->GetEntries()/100.,0,0.7);
   hTOFmDist_zbv_global->Fit(fitTOFmDist_zbv_global,"RQ0");
   hTOFmDist_zbv_global->Draw("hist");
   fitTOFmDist_zbv_global->Draw("same");
   ct->cd(6);
   gStyle->SetOptFit(1);
   TH1D* hTOFmDist_ztpc_global = ((TH1D *)fHistos.At(fHistoPositions.at("hTOFmDist_ztpc_global")));
   TF1* fitTOFmDist_ztpc_global = new TF1("fitTOFmDist_ztpc_global","gaus(0)+gaus(3)",-5.,5.);
   fitTOFmDist_ztpc_global->SetParameters(hTOFmDist_ztpc_global->GetEntries()/30.,0,0.3,hTOFmDist_ztpc_global->GetEntries()/100.,0,0.7);
   hTOFmDist_ztpc_global->Fit(fitTOFmDist_ztpc_global,"RQ0");
   hTOFmDist_ztpc_global->Draw("hist");
   fitTOFmDist_ztpc_global->Draw("same");
   ct->cd(7);
   gStyle->SetOptFit(1);
   TH1D* hTOFmDist_zbv_bartobar = ((TH1D *)fHistos.At(fHistoPositions.at("hTOFmDist_zbv_bartobar")));
   TF1* fitTOFmDist_zbv_bartobar = new TF1("fitTOFmDist_zbv_bartobar","gaus(0)+gaus(3)",-5.,5.);
   fitTOFmDist_zbv_bartobar->SetParameters(hTOFmDist_zbv_bartobar->GetEntries()/30.,0,0.3,hTOFmDist_zbv_bartobar->GetEntries()/100.,0,0.7);
   hTOFmDist_zbv_bartobar->Fit(fitTOFmDist_zbv_bartobar,"RQ0");
   hTOFmDist_zbv_bartobar->Draw("hist");
   fitTOFmDist_zbv_bartobar->Draw("same");
   ct->cd(8);
   gStyle->SetOptFit(1);
   TH1D* hTOFmDist_ztpc_bartobar = ((TH1D *)fHistos.At(fHistoPositions.at("hTOFmDist_ztpc_bartobar")));
   TF1* fitTOFmDist_ztpc_bartobar = new TF1("fitTOFmDist_ztpc_bartobar","gaus(0)+gaus(3)",-5.,5.);
   fitTOFmDist_ztpc_bartobar->SetParameters(hTOFmDist_ztpc_bartobar->GetEntries()/30.,0,0.3,hTOFmDist_ztpc_bartobar->GetEntries()/100.,0,0.7);
   hTOFmDist_ztpc_bartobar->Fit(fitTOFmDist_ztpc_bartobar,"RQ0");
   hTOFmDist_ztpc_bartobar->Draw("hist");
   fitTOFmDist_ztpc_bartobar->Draw("same");
   ct->cd(9);
   ((TH2D *)fHistos.At(fHistoPositions.at("hsignificance_zbv_global")))->Draw("colz");
   ct->cd(10);
   ((TH2D *)fHistos.At(fHistoPositions.at("hsignificance_ztpc_global")))->Draw("colz");
   ct->cd(11);
   ((TH2D *)fHistos.At(fHistoPositions.at("hsignificance_zbv_bartobar")))->Draw("colz");
   ct->cd(12);
   ((TH2D *)fHistos.At(fHistoPositions.at("hsignificance_ztpc_bartobar")))->Draw("colz");

   return ct;
}

TCanvas* TAGPlotBV::DrawPileUpCanvas(TString Name)
{
   SetupBarHistos();
   FillBarHisto();

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1400);
   ct->Divide(2);
   ct->cd(1);
   TH1D* hTimeCounts = ((TH1D *)fHistos.At(fHistoPositions.at("hTimeCounts")));
   TH1D* hPileUpCounts = ((TH1D *)fHistos.At(fHistoPositions.at("hPileUpCounts")));
   hTimeCounts->SetMinimum(0);
   hPileUpCounts->SetMinimum(0);
   hTimeCounts->SetLineColor(kBlue);
   hPileUpCounts->SetLineColor(kRed);
   hTimeCounts->Draw("hist");
   hPileUpCounts->Draw("hist same");
   
   ct->cd(2);
   TH1D* hPileUpDeltaT = ((TH1D *)fHistos.At(fHistoPositions.at("hPileUpDeltaT")));
   TH1D* hPileUpDeltaTMatched = ((TH1D *)fHistos.At(fHistoPositions.at("hPileUpDeltaTMatched")));
   hPileUpDeltaT->SetLineColor(kBlue);
   hPileUpDeltaTMatched->SetLineColor(kRed);
   TF1* fitPileUpDeltaT = new TF1("fitPileUpDeltaT","expo",200,3500);
   TF1* fitPileUpDeltaTMatched = new TF1("fitPileUpDeltaT","expo",200,3500);
   hPileUpDeltaT->Fit(fitPileUpDeltaT,"R");
   hPileUpDeltaTMatched->Fit(fitPileUpDeltaTMatched,"R");
   fitPileUpDeltaT->SetLineColor(kBlue);
   fitPileUpDeltaTMatched->SetLineColor(kRed);
   hPileUpDeltaT->Draw("hist");
   hPileUpDeltaTMatched->Draw("hist same");
   fitPileUpDeltaT->Draw("same");
   fitPileUpDeltaTMatched->Draw("same");

   printf("Total number of events: %.0f\n",hTimeCounts->GetEntries());
   printf("Total number of pileup: %.0f\n",hPileUpCounts->GetEntries());
   printf("Total number of pileup matched: %.0f\n",hPileUpDeltaTMatched->GetEntries());
   
   return ct;
}

TCanvas* TAGPlotBV::DrawTrackMatchCanvas(TString Name)
{
   SetupBarHistos();
   FillBarHisto();

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1400);
   TH1D* hNTracks = ((TH1D *)fHistos.At(fHistoPositions.at("hNTracks")));
   TH1D* hNTracksMatched = ((TH1D *)fHistos.At(fHistoPositions.at("hNTracksMatched")));
   TH1D* hNTracksMatchedMain = ((TH1D *)fHistos.At(fHistoPositions.at("hNTracksMatchedMain")));
   hNTracks->SetMinimum(0);
   hNTracksMatched->SetMinimum(0);
   hNTracksMatchedMain->SetMinimum(0);
   hNTracks->SetLineColor(kBlue);
   hNTracksMatched->SetLineColor(kRed);
   hNTracksMatchedMain->SetLineColor(kGreen);
   hNTracksMatchedMain->Draw("hist");
   hNTracks->Draw("hist same");
   hNTracksMatched->Draw("hist same");
   
   printf("Total number of tracks: %.0f\n",hNTracks->GetIntegral());
   printf("Total number of tracks matched to BV: %.0f\n",hNTracksMatched->GetIntegral());
   printf("Total number of tracks matched to BV main group: %.0f\n",hNTracksMatchedMain->GetIntegral());
   
   return ct;
}

TCanvas* TAGPlotBV::DrawTOFFitCanvas(TString Name, double min_dist, std::vector<int> barIDs)
{
   SetupBarHistos();
   FillTOFHisto(min_dist, barIDs);

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1800);
   gStyle->SetOptFit(1);
   TH1D* hTOFmDist = ((TH1D *)fHistos.At(fHistoPositions.at("hTOFmDist_zbv_bartobar")));
   TF1* fit = new TF1("fit","gaus(0)+gaus(3)",-5.,5.);
   TF1* fitinner = new TF1("fitinner","gaus(0)",-5.,5.);
   TF1* fitouter = new TF1("fitouter","gaus(0)",-5.,5.);
   fit->SetParameters(hTOFmDist->GetEntries()/30.,0,0.3,hTOFmDist->GetEntries()/100.,0,0.7);
   hTOFmDist->Fit(fit,"RQ0");
   fitinner->SetParameter(0,fit->GetParameter(0));
   fitinner->SetParameter(1,fit->GetParameter(1));
   fitinner->SetParameter(2,fit->GetParameter(2));
   fitouter->SetParameter(0,fit->GetParameter(3));
   fitouter->SetParameter(1,fit->GetParameter(4));
   fitouter->SetParameter(2,fit->GetParameter(5));
   hTOFmDist->Draw("hist");
   fit->Draw("same");
   fitinner->SetLineColor(kBlue);
   fitinner->Draw("same");
   fitouter->SetLineColor(kGreen+3);
   fitouter->Draw("same");

   double inint = fitinner->Integral(-5,5);
   double outint = fitouter->Integral(-5,5);
   printf("Inner gaussian integral: %.3f\n",inint/(inint+outint));
   printf("Outer gaussian integral: %.3f\n",outint/(inint+outint));

   return ct;
}

TCanvas* TAGPlotBV::DrawTOFAmpCanvas(TString Name, double min_dist, std::vector<int> barIDs)
{
   SetupBarHistos();
   FillTOFHisto(min_dist, barIDs);

   TCanvas *ct = new TCanvas(Name, Name, 1600, 800);
   ct->Divide(2);
   ct->cd(1);
   ((TH2D *)fHistos.At(fHistoPositions.at("hAmpMinvsTOFmDist")))->Draw("colz");
   ct->cd(2);
   ((TH2D *)fHistos.At(fHistoPositions.at("hCombinedAmpvsTOFmDist")))->Draw("colz");
   return ct;
}

TCanvas* TAGPlotBV::DrawDirectionCanvas(TString Name)
{
   SetupBarHistos();
   FillMVAHisto();
   FillBarHisto();

   TCanvas *ct = new TCanvas(Name, Name, 1600, 800);
   ct->Divide(3,2);
   ct->cd(1);
   ((TH1D *)fHistos.At(fHistoPositions.at("h_directionTheta")))->Draw();
   ct->cd(2);
   ((TH1D *)fHistos.At(fHistoPositions.at("h_directionality")))->Draw();
   ct->cd(3);
   ((TH1D *)fHistos.At(fHistoPositions.at("h_zdirectionality")))->Draw();
   ct->cd(4);
   ((TH2D *)fHistos.At(fHistoPositions.at("hProjvsTime")))->Draw("colz");
   ct->cd(5);
   ((TH2D *)fHistos.At(fHistoPositions.at("hZProjvsTime")))->Draw("colz");

   return ct;
}

std::array<int,7> TAGPlotBV::GetHitTypeCounts()
{
   std::array<int,7> counts;
   counts.fill(0);
   for (size_t i = 0; i < fPlotBarEvents.size(); i++) {
      const TBarEvent& barEvt = fPlotBarEvents.fBarEvents[i];
      for (const TBarHit& h: barEvt.GetBarHits()) {
         int hit_type = h.GetHitType();
         if (hit_type==1) counts[0]++; // Complete
         if (hit_type==2 and h.HasTopEndHit()) counts[1]++; // Top plus ADC
         if (hit_type==2 and h.HasBotEndHit()) counts[2]++; // Bot plus ADC
         if (hit_type==3 and h.HasTopEndHit()) counts[3]++; // Top plus TDC
         if (hit_type==3 and h.HasBotEndHit()) counts[4]++; // Bot plus TDC
         if (hit_type==4 and h.HasTopEndHit()) counts[5]++; // Top
         if (hit_type==4 and h.HasBotEndHit()) counts[6]++; // Bot
      }
   } 
   return counts;
}

TH1D* TAGPlotBV::GetBotADC()
{
   TH1D* hBotADC = new TH1D("hBotADC","Bottom ADC Amplitude; ADC amplitude",350,0,35000);
   for (size_t i = 0; i < fPlotBarEvents.size(); i++) {
      const TBarEvent& barEvt = fPlotBarEvents.fBarEvents[i];
      for (const TBarADCHit& h: barEvt.GetADCHits()) {
         if (h.GetBarID()>=28 and h.GetBarID()<32) continue;
         if (!h.IsGood()) continue;
         if (!h.IsBottom()) continue;
         double amp = h.GetAmpFit();
         if (amp<0) continue;
         hBotADC->Fill(amp);
      }
   }
   return hBotADC;
}

TH1D* TAGPlotBV::GetTopADC()
{
   TH1D* hTopADC = new TH1D("hTopADC","Top ADC Amplitude; ADC amplitude",350,0,35000);
   for (size_t i = 0; i < fPlotBarEvents.size(); i++) {
      const TBarEvent& barEvt = fPlotBarEvents.fBarEvents[i];
      for (const TBarADCHit& h: barEvt.GetADCHits()) {
         if (h.GetBarID()>=28 and h.GetBarID()<32) continue;
         if (!h.IsGood()) continue;
         if (!h.IsTop()) continue;
         double amp = h.GetAmpFit();
         if (amp<0) continue;
         hTopADC->Fill(amp);
      }
   }
   return hTopADC;
}

TCanvas* TAGPlotBV::DrawTOFDemonstration(TString Name, bool cline)
{
   SetupBarHistos();
   FillTOFHisto();
   TCanvas *ct = new TCanvas(Name, Name,3000,2500);
   ct->Divide(2);

   ct->cd(1);
   TH2D* h1 = (TH2D *)fHistos.At(fHistoPositions.at("hTOFvsDist_Uncalibrated"));
   h1->SetStats(0);
   h1->Draw("colz");
   TF1* cLine = new TF1("cLine","pol1",0.1,1.5);
   if (cline) {
      cLine->SetParameters(0.,1./0.299792458);
      cLine->SetLineColor(kRed);
      cLine->SetLineStyle(9);
      cLine->SetLineWidth(5);
      cLine->Draw("same");
   }

   ct->cd(2);
   TH2D* h2 = (TH2D *)fHistos.At(fHistoPositions.at("hTOFvsDist"));
   h2->SetStats(0);
   h2->Draw("colz");
   if (cline) {
      cLine->Draw("same");
   }
   return ct;
}


#endif
