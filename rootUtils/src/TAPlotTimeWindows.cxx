#include "TAPlotTimeWindows.h"
#include "DoubleGetters.h"

ClassImp(TAPlotTimeWindows)


/// @brief Default constructor
TAPlotTimeWindows::TAPlotTimeWindows()
{
   fFirstTMin     = std::numeric_limits<double>::infinity();
   fLastTMax      = -std::numeric_limits<double>::infinity();
   fBiggestTBeforeZero  = 0.;
   fBiggestTAfterZero = 0;
   fMaxDumpLength = 0.;
}

/// @brief Deconstructor
TAPlotTimeWindows::~TAPlotTimeWindows()
{
}

/// @brief Copy constructor
/// @param object
TAPlotTimeWindows::TAPlotTimeWindows(const TAPlotTimeWindows &object) : TObject(object)
{
   // std::cout << "TAPlotTimeWindows copy ctor." << std::endl;
   // Deep copy
   for (size_t i = 0; i < object.fMinTime.size(); i++) {
      fRunNumber.push_back(object.fRunNumber[i]);
      fMinTime.push_back(object.fMinTime[i]);
      fMaxTime.push_back(object.fMaxTime[i]);
      fZeroTime.push_back(object.fZeroTime[i]);
   }

   fFirstTMin     = object.fFirstTMin;
   fLastTMax      = object.fLastTMax;
   fBiggestTBeforeZero  = object.fBiggestTBeforeZero;
   fBiggestTAfterZero  = object.fBiggestTAfterZero;
   fMaxDumpLength = object.fMaxDumpLength;
}

/// @brief Assignement operator
/// @param rhs
/// @return
///
/// Copies internal data containers
TAPlotTimeWindows &TAPlotTimeWindows::operator=(const TAPlotTimeWindows &rhs)
{
   // std::cout << "TAPlotTimeWindows = operator." << std::endl;
   for (size_t i = 0; i < rhs.fMinTime.size(); i++) {
      this->fRunNumber.push_back(rhs.fRunNumber[i]);
      this->fMinTime.push_back(rhs.fMinTime[i]);
      this->fMaxTime.push_back(rhs.fMaxTime[i]);
      this->fZeroTime.push_back(rhs.fZeroTime[i]);
   }

   fFirstTMin     = rhs.fFirstTMin;
   fLastTMax      = rhs.fLastTMax;
   fBiggestTBeforeZero  = rhs.fBiggestTBeforeZero;
   fBiggestTAfterZero  = rhs.fBiggestTAfterZero;
   fMaxDumpLength = rhs.fMaxDumpLength;


   return *this;
}
/// @brief + operator
/// @param rhs
/// @param lhs
/// @return
///
/// Add two TAPlotTimeWindows and return outputPlot
TAPlotTimeWindows operator+(const TAPlotTimeWindows &lhs, const TAPlotTimeWindows &rhs)
{
   // std::cout << "TAPlotTimeWindows addition operator" << std::endl;
   TAPlotTimeWindows outputPlot(lhs); // Create new from copy
   // Vectors- need concacting
   outputPlot.fRunNumber.insert(outputPlot.fRunNumber.end(), rhs.fRunNumber.begin(), rhs.fRunNumber.end());
   outputPlot.fMinTime.insert(outputPlot.fMinTime.end(), rhs.fMinTime.begin(), rhs.fMinTime.end());
   outputPlot.fMaxTime.insert(outputPlot.fMaxTime.end(), rhs.fMaxTime.begin(), rhs.fMaxTime.end());
   outputPlot.fZeroTime.insert(outputPlot.fZeroTime.end(), rhs.fZeroTime.begin(), rhs.fZeroTime.end());

   outputPlot.fFirstTMin     = (lhs.fFirstTMin < rhs.fFirstTMin) ? lhs.fFirstTMin : rhs.fFirstTMin;
   outputPlot.fLastTMax      = (lhs.fLastTMax > rhs.fLastTMax) ? lhs.fLastTMax : rhs.fLastTMax;
   outputPlot.fBiggestTBeforeZero  = (lhs.fBiggestTBeforeZero > rhs.fBiggestTBeforeZero) ? lhs.fBiggestTBeforeZero : rhs.fBiggestTBeforeZero;
   outputPlot.fBiggestTAfterZero  = (lhs.fBiggestTAfterZero > rhs.fBiggestTAfterZero) ? lhs.fBiggestTAfterZero : rhs.fBiggestTAfterZero;
   outputPlot.fMaxDumpLength = (lhs.fMaxDumpLength > rhs.fMaxDumpLength) ? lhs.fMaxDumpLength : rhs.fMaxDumpLength;

   return outputPlot;
}
/// @brief += operator
/// @param rhs
/// @return
///
/// Add the contents of rhs to this TAPlot
TAPlotTimeWindows TAPlotTimeWindows::operator+=(const TAPlotTimeWindows &rhs)
{
   // std::cout << "TAPlotTimeWindows += operator" << std::endl;
   this->fRunNumber.insert(this->fRunNumber.end(), rhs.fRunNumber.begin(), rhs.fRunNumber.end());
   this->fMinTime.insert(this->fMinTime.end(), rhs.fMinTime.begin(), rhs.fMinTime.end());
   this->fMaxTime.insert(this->fMaxTime.end(), rhs.fMaxTime.begin(), rhs.fMaxTime.end());
   this->fZeroTime.insert(this->fZeroTime.end(), rhs.fZeroTime.begin(), rhs.fZeroTime.end());

   this->fFirstTMin     = (this->fFirstTMin < rhs.fFirstTMin) ? this->fFirstTMin : rhs.fFirstTMin;
   this->fLastTMax      = (this->fLastTMax > rhs.fLastTMax) ? this->fLastTMax : rhs.fLastTMax;
   this->fBiggestTBeforeZero  = (this->fBiggestTBeforeZero > rhs.fBiggestTBeforeZero) ? this->fBiggestTBeforeZero : rhs.fBiggestTBeforeZero;
   this->fBiggestTAfterZero  = (this->fBiggestTAfterZero > rhs.fBiggestTAfterZero) ? this->fBiggestTAfterZero : rhs.fBiggestTAfterZero;
   this->fMaxDumpLength = (this->fMaxDumpLength > rhs.fMaxDumpLength) ? this->fMaxDumpLength : rhs.fMaxDumpLength;
   
   return *this;
}

/// @brief Check for overlaps when adding a time window
/// @param runNumber 
/// @param minTime 
/// @param maxTime 
/// 
/// This function only prints warnings, it does not take action. We will want to work out how to treat 
/// this undefined behaviour
void TAPlotTimeWindows::CheckForOverLap(int runNumber, double minTime, double maxTime)
{
   bool IsNewRunNumber = true;
   for (const int& run: fRunNumber)
      if (run == runNumber)
         IsNewRunNumber = false;
   // This run is new to the container, so we are good, there is no overlap
   if (IsNewRunNumber)
      return;
   const size_t nevents = fRunNumber.size();
   for (size_t i = 0; i < nevents; i++)
   {
      if (runNumber != fRunNumber[i])
         continue;
      // Is start time contained in an existing window?
      if ( fMinTime[i] <=  minTime && fMaxTime[i] >= minTime )
        std::cerr << "StartTime window overlap in index " << i <<": "<< fMinTime[i]<<" <= "<<minTime<<" <= "<<fMaxTime[i]<< "! Undefined behaviour!" <<std::endl;
      // Is stop time contained in an existing window?
      if ( fMinTime[i] <=  maxTime && fMaxTime[i] >= maxTime )
         std::cerr << "StopTime window overlap in index " << i <<": "<<fMinTime[i]<<" <= "<<maxTime<<" <= "<<fMaxTime[i]<< "! Undefined behaviour!" <<std::endl;
      // Is window encapsulating an existing window
      if ( fMinTime[i] >= minTime && fMaxTime[i] <= maxTime )
        std::cerr << "EncapsulatingTime window overlap in index " << i <<": "<<fMinTime[i]<<" <= "<<minTime<<" && "<<fMaxTime[i]<<" <= "<<maxTime<< "! Undefined behaviour!" <<std::endl;

   }
}


/// @brief Adds a time window to the TAPlot
/// @param runNumber 
/// @param minTime 
/// @param maxTime 
/// @param timeZero 
/// This function is used by TAPlot to add a time window of which data to pull.
void TAPlotTimeWindows::AddTimeWindow(int runNumber, double minTime, double maxTime, double timeZero)
{
   assert(maxTime >= 0);
   assert(maxTime >= minTime);
   assert(timeZero < maxTime);
   assert(timeZero >= minTime);

   //Update variables used for TAPlots
   if(maxTime-minTime>fMaxDumpLength) fMaxDumpLength=maxTime-minTime;
   // Find the first start window
   if(minTime<fFirstTMin) fFirstTMin=minTime; 
   // Largest time before 'zero' (-ve number)
   if (minTime - timeZero < fBiggestTBeforeZero) fBiggestTBeforeZero = minTime - timeZero;
   if (maxTime - timeZero > fBiggestTAfterZero) fBiggestTAfterZero = maxTime - timeZero;
   // Skip early if we are looking for end of run anyway
   // Find the highest value
   if (maxTime > fLastTMax) fLastTMax = maxTime;

   CheckForOverLap(runNumber,minTime,maxTime);

   //Now push back the new elements.
   fRunNumber.push_back(runNumber);
   fMinTime.push_back(minTime);
   fMaxTime.push_back(maxTime);
   fZeroTime.push_back(timeZero);
   isSorted = false; 
}

/// @brief Get the time window index for specific time
/// @param time 
/// @return 
///
/// If this class is 'const' we can't sort the internal data, the user
/// might require the intenal index order be unchanged
int TAPlotTimeWindows::GetValidWindowNumber(const int runNumber, double time) const
{
   // Check where t sits in tmin.
   const size_t tmaxSize = fMaxTime.size();
   for (size_t j = 0; j < tmaxSize; j++) {
      if (runNumber != fRunNumber[j])
         continue;
      // If inside the time window
      if (time >= fMinTime[j]) {
         const double currentTMax = fMaxTime[j];
         if (time < currentTMax || currentTMax < 0) {
            return j;
         }
      }
   }
   // If we get down here just return error.
   return -2;
}

/// @brief Pre-sort then get the time window index for specific time
/// @param time 
/// @return 
int TAPlotTimeWindows::GetValidWindowNumber(const int runNumber, const double time)
{
   // Checks whether vector is sorted and sorts if not.
   if (!isSorted) {
      SortTimeWindows();
   }
   const TAPlotTimeWindows* this_as_const = this;
   return this_as_const->GetValidWindowNumber(runNumber,time);
}

double TAPlotTimeWindows::GetTimeToNextWindow(const int runNumber, const double time) const
{
   double dt = 1E99;
   
   // Check where t sits in tmin
   const size_t tminSize = fMinTime.size();
   for (size_t j = 0; j < tminSize; j++) {
      if (runNumber != fRunNumber[j] )
         continue;
      const double min = fMinTime[j];
      if (time < min)
      {
         if ( min - time  < dt &&  min - time  > 0)
            dt = min - time;
      }
   }
   return dt;
}

std::pair<double, double> TAPlotTimeWindows::GetWindowTimeRange(const int runNumber) const
{
   double min_time =std::numeric_limits<double>::infinity();
   double max_time = 0.;
   for (size_t j = 0; j <  fMinTime.size(); j++) {
      if (runNumber != fRunNumber[j])
         continue;
      if (fMinTime[j] < min_time)
         min_time = fMinTime[j];
      if (fMaxTime[j] > max_time)
         max_time = fMaxTime[j];
   }
   return std::make_pair(min_time,max_time);
}


void TAPlotTimeWindows::SortTimeWindows()
{
   assert(fMinTime.size() == fMaxTime.size());
   const size_t minTimeSize = fMinTime.size();
   // Create vectors needed for sorting.
   std::vector<std::pair<double, double>> zipped(minTimeSize);
   std::vector<size_t>                    idx(minTimeSize);
   iota(idx.begin(), idx.end(), 0);
   // Zip tmin and index vector together.
   zipped.resize(minTimeSize);
   for (size_t i = 0; i < minTimeSize; ++i) {
      zipped[i] = std::make_pair(fMinTime[i], idx[i]);
   }
   // Sort based on first (tmin) keeping idx in the same location
   std::sort(std::begin(zipped), std::end(zipped),
             [&](const std::pair<double, double> &lhs, const std::pair<double, double> &rhs) {
                return lhs.first < rhs.first;
             });

   // Unzip back into vectors.
   for (size_t i = 0; i < minTimeSize; i++) {
      fMinTime[i] = zipped[i].first;
      idx[i]      = zipped[i].second;
   }

   fRunNumber = reorder<int>(fRunNumber, idx);
   fMaxTime   = reorder<double>(fMaxTime, idx);
   fZeroTime  = reorder<double>(fZeroTime, idx);

   isSorted = true;
}

/// @brief Get the title line of the CSV for exporting to CSV.
/// @return
std::string TAPlotTimeWindows::CSVTitleLine() const
{
   return std::string("Run Number,") + "Min Time," + "Max Time," + "Zero Time\n";
}

/// @brief Get the i-th line of the CSV for exporting to CSV.
/// @param i
/// @return
std::string TAPlotTimeWindows::CSVLine(size_t i) const
{
   // This is a little fugly
   std::string line;
   line = std::string("") + 
   std::to_string(fRunNumber.at(i)) + "," +
   std::to_string(fMinTime.at(i)) + "," +
   std::to_string(fMaxTime.at(i)) + "," +
   std::to_string(fZeroTime.at(i)) + "\n";
   return line;
}
