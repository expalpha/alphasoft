#if BUILD_A2
#include "TA2PlotAlphaEvents.h"

/// @brief Constructor
/// @param zeroTime
TA2PlotAlphaEvents::TA2PlotAlphaEvents(bool zeroTime) : TA2Plot(zeroTime) {}

/// @brief Constructor
/// @param zMin
/// @param zMax
/// @param zeroTime
TA2PlotAlphaEvents::TA2PlotAlphaEvents(double zMin, double zMax, bool zeroTime) : TA2Plot(zMin, zMax, zeroTime) {}

/// @brief Copy constructor
/// @param object 
TA2PlotAlphaEvents::TA2PlotAlphaEvents(const TA2PlotAlphaEvents &object)
   : TA2Plot(object), fHelixEvents(object.fHelixEvents)
{
}

/// @brief Deconstructor
TA2PlotAlphaEvents::~TA2PlotAlphaEvents()
{
   Reset();
}

void TA2PlotAlphaEvents::Reset()
{
   TA2Plot::Reset();
   fHelixEvents.clear();
}

/// @brief = operator
/// @param rhs 
/// @return 
TA2PlotAlphaEvents &TA2PlotAlphaEvents::operator=(const TA2PlotAlphaEvents &rhs)
{
   TA2Plot::operator=(rhs);
   fHelixEvents               = rhs.fHelixEvents;
   return *this;
}

/// @brief += operator
/// @param rhs 
/// @return 
TA2PlotAlphaEvents &TA2PlotAlphaEvents::operator+=(const TA2PlotAlphaEvents &rhs)
{
   // This calls the parent += operator first.
   TA2Plot::operator+=(rhs);
   fHelixEvents += rhs.fHelixEvents;
   return *this;
}

/// @brief Addition operator
/// @param lhs
/// @param rhs
/// @return
///
/// Add the contents of the lhs and rhs and return a new TA2Plot object
TA2PlotAlphaEvents &operator+(const TA2PlotAlphaEvents &lhs, const TA2PlotAlphaEvents &rhs)
{
   TA2PlotAlphaEvents *basePlot = new TA2PlotAlphaEvents;
   *basePlot += lhs;
   *basePlot += rhs;
   return *basePlot;
}

/// @brief Add TAlphaEvent to container
/// @param event 
/// @param timeOffset 
void TA2PlotAlphaEvents::AddAlphaEvent(const TAlphaEvent &event, const double timeOffset)
{
   ProcessHelices(event.GetRunNumber(), event.GetRunTime() - timeOffset, event.GetRunTime(),
                  event.GetHelixArray(), event.GetCutsResults());
   return;
}

/// @brief Load TAlphaEvent into container for a single run
/// @param runNumber 
/// @param firstTime 
/// @param lastTime 
/// @param verbose 
void TA2PlotAlphaEvents::LoadRun(const int runNumber, const double firstTime, const double lastTime, int verbose)
{

   TA2Plot::LoadRun(runNumber, firstTime, lastTime, verbose);

   TTreeReaderPointer AlphaEventTreeReader = Get_TAlphaEvent_Tree(runNumber);
   if (!AlphaEventTreeReader) return;
   TTreeReaderValue<TAlphaEvent> AlphaEvent(*AlphaEventTreeReader, "AlphaEvent");
   AlphaEventTreeReader = FindStartOfTree(AlphaEventTreeReader,AlphaEvent,0,AlphaEventTreeReader->GetEntries(),firstTime,1);
   while (AlphaEventTreeReader->Next()) {
      const double t = AlphaEvent->GetRunTime();
      if (t < firstTime) continue;
      if (t > lastTime) break;
      AddAlphaEvent(*AlphaEvent);
   }
}

/// @brief Setup track histograms
void TA2PlotAlphaEvents::SetupTrackHistos()
{
   // reco helices   
   TH1D *hNhel = new TH1D("hNhel", "Reconstructed Helices", 10, 0., 10.);
   AddHistogram(hNhel->GetName(), hNhel);

   TH1D *hhD = new TH1D("hhD", "Hel D;[mm]", 200, -12., 12.);
   AddHistogram(hhD->GetName(), hhD);

   TH1D *hhRc = new TH1D("hhRc", "Hel Rc;[mm]", 200, 0., 1000.);
   AddHistogram(hhRc->GetName(), hhRc);

   TH1D *hhphi = new TH1D("hhphi", "Hel phi;[degrees]", 180, 0., 360.);
   AddHistogram(hhphi->GetName(), hhphi);

   TH1D *hhlambda = new TH1D("hhlambda", "Hel lambda;[unitless]", 200, -5., 5.);
   AddHistogram(hhlambda->GetName(), hhlambda);

   TH1D *hhz0 = new TH1D("hhz0", "Hel z0;[mm]", 200, -30., 30.);
   AddHistogram(hhz0->GetName(), hhz0);

   TH1D *hhchi2 = new TH1D("hhchi2", "Helix Chi2;Helix Chi2", 200, 0., 5.);
   AddHistogram(hhchi2->GetName(), hhchi2);

}

/// @brief Fill track all histograms (include TA2Plot histograms)
/// @param CutsMode 
void TA2PlotAlphaEvents::FillHisto(const std::string &CutsMode)
{

   ClearHisto();

   // Sets up and fills normal TA2Plot histos
   TA2Plot::FillHisto(CutsMode);
   
   // Sets up and fills TA2PlotAlphaEvent histos
   SetupTrackHistos();
   FillTrackHisto(CutsMode);
}

/// @brief Fill track metric histograms
void TA2PlotAlphaEvents::FillTrackHisto(const std::string &CutsMode)
{
   TAPlotCutsMode applyCuts(CutsMode);

   std::cout << "TA2PlotAlphaEvents::FillTrackHisto() Number of histos: " << fHistos.GetEntries() << std::endl;

   for (size_t i = 0; i < fHelixEvents.size(); i++) {
      bool PassCuts = applyCuts.ApplyCuts(fHelixEvents.CutsResults(i));
      if( !PassCuts ) continue;
      FillHistogram("hhD", fHelixEvents.parD[i]);
      FillHistogram("hhRc", fHelixEvents.Curvature[i]);
      FillHistogram("hhphi", fHelixEvents.Phi[i]*180/TMath::Pi());
      FillHistogram("hhlambda", fHelixEvents.Lambda[i]);
      FillHistogram("hhz0", fHelixEvents.Z0[i]);
      FillHistogram("hhchi2", fHelixEvents.Chi2[i]);
   }

   const TAPlotVertexEvents &kVertexEvents = GetVertexEvents();
   for (size_t i = 0; i<kVertexEvents.size(); i++) {
      FillHistogram("hNhel",kVertexEvents.NumTracks(i));
   }

}

/// @brief Draw track data
/// @param Name 
/// @return 
TCanvas *TA2PlotAlphaEvents::DrawTrackCanvas(TString Name, const std::string& Cuts)
{
   SetupTrackHistos();
   FillTrackHisto(Cuts);

   TCanvas *ct = new TCanvas(Name, Name, 2000, 1800);
   ct->Divide(3, 3);

   ct->cd(1);
   ((TH1D *)fHistos.At(fHistoPositions.at("hNhel")))->Draw("hist");
   ct->cd(2);
   ((TH1D *)fHistos.At(fHistoPositions.at("hhD")))->Draw("hist");
   ct->cd(3);
   ((TH1D *)fHistos.At(fHistoPositions.at("hhRc")))->Draw("hist");
   ct->cd(4);
   ((TH1D *)fHistos.At(fHistoPositions.at("hhphi")))->Draw("hist");
   ct->cd(5);
   ((TH1D *)fHistos.At(fHistoPositions.at("hhlambda")))->Draw("hist");
   ct->cd(6);
   ((TH1D *)fHistos.At(fHistoPositions.at("hhz0")))->Draw("hist");
   ct->cd(7);
   ((TH1D *)fHistos.At(fHistoPositions.at("hhchi2")))->Draw("hist");

   return ct;
}

/// @brief Add helix data to histograms
/// @param runNumber 
/// @param time 
/// @param officialtime 
/// @param tracks 
void TA2PlotAlphaEvents::ProcessHelices(const double runNumber, const double time, const double officialtime,
                                   const std::vector<TAlphaEventHelix*> tracks, const std::vector<bool>& cuts)
{
   const int Nhelices = tracks.size();
   for (int i = 0; i < Nhelices; ++i) {
      const TAlphaEventHelix *aHelix = tracks.at(i);
      if (!aHelix or aHelix==NULL) continue;
      fHelixEvents.AddEvent(runNumber, time, officialtime, aHelix->GetChi2(), aHelix->Getfd0(), aHelix->GetR(), aHelix->Getfphi(), aHelix->Getflambda(), aHelix->Getfz0(), aHelix->GetNHits(), cuts);
      //fHelixEvents.AddEvent(runNumber, time, officialtime, aHelix->GetMomentumV().Perp(), aHelix->GetMomentumV().Z(),
      //                      aHelix->GetMomentumV().Mag(), aHelix->GetD(), aHelix->GetRc(), aHelix->GetNumberOfPoints(), cuts);

   }
}

#endif
