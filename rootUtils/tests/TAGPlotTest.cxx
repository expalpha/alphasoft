#if BUILD_AG
// Enable assert (if NDEBUG is defined, it will stop assert() functions running at all)
#undef NDEBUG
#include <cassert>

#include "TAGPlot.h"
#include "DoubleGetters.h"
int TestDoubleLoad(const int runNumber)
{
   int ncut_types;
   {
      TAGDetectorEvent qod;
      ncut_types = qod.CutNames().size();
   }
   std::cout <<"Test double load" << std::endl;
   TAGPlot a;
   a.AddTimeGate(runNumber, 0., 10.);
   a.AddTimeGate(runNumber, 20., 30.);
   a.LoadData();
   // Plot a should be the same as Plot b
   TAGPlot b;
   b.AddTimeGate(runNumber, 0., 10.);
   b.LoadData();
   b.AddTimeGate(runNumber, 20., 30.);
   b.LoadData();
   assert( a.GetTotalTime() == b.GetTotalTime() );

   assert( a.GetNVerticies() == b.GetNVerticies());
   assert( a.GetNVertexEvents() == b.GetNVertexEvents());
   for (int i = 0; i < ncut_types; i++)
      assert( a.GetNPassedType(i) == b.GetNPassedType(i) );
   assert( a.GetTotalTime() == b.GetTotalTime());
   return 0;
}

int TestTotalTime(const int runNumber)
{
   int ncut_types;
   {
      TAGDetectorEvent qod;
      ncut_types = qod.CutNames().size();
   }

   std::cout <<" Checking TAGPlot time metrics" << std::endl;
   std::cout <<"Test 1..." << std::endl;
   TAGPlot a;
   a.AddTimeGate(runNumber, 0., 10.);
   a.LoadData();
   assert( a.GetTotalTime() == 10.);

   TAGPlot b;
   b.AddTimeGate(runNumber, 20., 30.);
   b.LoadData();
   assert( a.GetTotalTime() == 10.);

   std::cout <<"Test 2...copy and +=" << std::endl;
   TAGPlot c;
   c.AddTimeGate(runNumber, 0., 10.);
   c.AddTimeGate(runNumber, 20., 30.);
   c.LoadData();
   // Plot d should be identical to plot c
   TAGPlot d(a);
   d+=b;
   assert( c.GetNVerticies() == d.GetNVerticies());
   assert( c.GetNVertexEvents()  == d.GetNVertexEvents());
   for (int i = 0; i < ncut_types; i++)
      assert( c.GetNPassedType(i) == d.GetNPassedType(i) );
   std::cout << c.GetTotalTime() << "=="<<  d.GetTotalTime() <<std::endl;
   assert( fabs( c.GetTotalTime() - d.GetTotalTime() ) < 0.001);

   std::cout <<"Test 3... overlapping time windowws" << std::endl;
   // Plot e should be identical to Plot a
   TAGPlot e;
   e.AddTimeGate(runNumber,0.,5.);
   e.AddTimeGate(runNumber,0.,10.);
   e.LoadData();
   assert( a.GetNVerticies() == e.GetNVerticies());
   assert( a.GetNVertexEvents()  == e.GetNVertexEvents());
   for (int i = 0; i < ncut_types; i++)
      assert( a.GetNPassedType(i) == e.GetNPassedType(i) );
   assert( a.GetTotalTime() == e.GetTotalTime());
   return 0;
}

int TestOnlyTotalTime(const int runNumber)
{
   std::cout <<" Checking TAGPlot time final test." << std::endl;
   std::cout <<"Test 1..." << std::endl;
   //runNumber=39993
   TAGPlot a;
   std::cout << "Test time window 0-10s  = 10s" << std::endl;
   a.AddTimeGate(runNumber, 0., 10.);
   // LoadData at every step is optional
   a.LoadData();
   assert( a.GetTotalTime() == 10.);
   std::cout << "Test time window 0-10s, 20-30s = 20s" << std::endl;
   a.AddTimeGate(runNumber, 20., 30.);
   a.LoadData();
   assert(a.GetTotalTime() > 0);
   assert ( fabs(a.GetTotalTime() - 20.) < 0.0001 );
   std::cout << "Test time window 0-10s, 5-7s, 20-30s = 20s" << std::endl;
   a.AddTimeGate(runNumber, 5., 7.);
   a.LoadData();
   assert(a.GetTotalTime() > 0);
   assert ( fabs(a.GetTotalTime() - 20.) < 0.0001 );
   std::cout << "Test time window 0-10s, 5-7s, 20-30s, 25s-37s = 27s" << std::endl;
   a.AddTimeGate(runNumber, 25., 37.);
   a.LoadData();
   assert(a.GetTotalTime() > 0);
   assert ( fabs(a.GetTotalTime() - 27.) < 0.0001 );
   std::cout << "Test time window 0-10s, 5-7s, 20-30s, 25s-37s, 38-RunEnd = Run Time - 11s" << std::endl;
   a.AddTimeGate(runNumber, 38, -1);
   a.LoadData();
   // There is an 11 second gap: 10-20s, and 37-38 seconds
   assert(a.GetTotalTime() > 0);
   assert ( fabs(a.GetTotalTime() - (GetTotalRunTimeFromChrono(runNumber)-11)) < 0.0001 );
   std::cout << "Test time window 0-10s, 5-7s, 20-30s, 25s-37s, 38-RunEnd, 0-RunEnd = Run Time" << std::endl;
   a.AddTimeGate(runNumber, 0, -1);
   a.LoadData();
   std::cout << "Total time: " << a.GetTotalTime() << "\t Total time from Chrono: " << GetTotalRunTimeFromChrono(runNumber) << std::endl;
   assert(a.GetTotalTime() > 0);
   assert ( fabs(a.GetTotalTime() - (GetTotalRunTimeFromChrono(runNumber))) < 0.0001 );
   std::cout <<"Test 38-39 seconds was already included in 38-RunEnd" << std::endl;
   // Test that 38-39 seconds got included in the `-1` time gate
   a.AddTimeGate(runNumber, 38, 39);
   a.LoadData();
   assert(a.GetTotalTime() > 0);
   assert ( fabs(a.GetTotalTime() - (GetTotalRunTimeFromChrono(runNumber))) < 0.0001 );
   
   return 0;
}


int main(int argc, char** argv)
{
   if (argc != 2)
   {
      std::cout <<"This test program expects exactly 1 input (runNumber)... test failed"<<std::endl;
      return 1;
   }
   int runNumber = atoi(argv[1]);
   std::cout <<"Using run " << runNumber <<std::endl;
   TAGPlot a;
   a.AddDumpGates(runNumber,{"Mixing"},{0});
   a.AddTimeGate(runNumber,0.,30.);
   a.LoadData();
   a.DrawVertexCanvas()->SaveAs("TAGPlotTest.png");
   a.ExportCSV("TAGPlotTest");

   TestDoubleLoad(runNumber);
   TestTotalTime(runNumber);
   TestOnlyTotalTime(runNumber);
   return 0;
}
#else
#include <iostream>
// Building without ALPHA2... so no TAGPlot... this test auto fails
int main()
{
   std::cout <<"Main build without BUILD_AG... test cannot be made\n";
   return 1;
}
#endif