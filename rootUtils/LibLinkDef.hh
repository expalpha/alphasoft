
#ifdef __CINT__
//#pragma link off all globals;
//#pragma link off all classes;
//#pragma link off all functions;

#pragma link C++ class TAPlot+;
#pragma link C++ class TAPlotScalerEvents+;
#pragma link C++ class TAPlotTimeWindows+;
#pragma link C++ class TAPlotVertexEvents+;
#pragma link C++ class TAPlotVertexEventsPassedCuts+;

#pragma link C++ class TAPlotEnvDataPlot+;
#pragma link C++ class TAPlotEnvData+;
#pragma link C++ class TAPlotFELabVIEWData+;
#pragma link C++ class TAPlotFEGEMData+;



#ifdef BUILD_AG
// Main ALPHAg plotting class
#pragma link C++ class TAGPlot+;
#pragma link C++ class TAGPlot_Filler+;
// Containers inside TAGPlot
#pragma link C++ class TAGPlotChronoPlotEvents+;
#pragma link C++ class TAGPlotSpacePointEvent+;
#pragma link C++ class TAGPlotHelixEvents+;
#pragma link C++ class TAGPlotBarEvents+;

// Child class of ALPHAg plotting class that uses TStoreEvent to load track data
#pragma link C++ class TAGPlotTracks+;
#pragma link C++ class TAGPlotBV+;

#pragma link C++ class TAGPlotSim+;

// Child class of TAGPlot (its the same, but loads data from TStoreEvent)
#pragma link C++ class TAGPlotTStoreEvent+;

#endif

#ifdef BUILD_A2
#pragma link C++ class TA2Plot+;
#pragma link C++ class TA2Plot_Filler+;
#pragma link C++ class TA2PlotSISPlotEvents+;
#pragma link C++ class TA2PlotQOD+;
#pragma link C++ class TA2PlotWithGEM+;
#pragma link C++ class TA2PlotMicrowave+;
#pragma link C++ class TA2PlotHelixEvents;
#pragma link C++ class TA2PlotAlphaEvents;

#endif

#ifdef BUILD_AG
#pragma link C++ function RootUtils+;
#endif
#ifdef BUILD_A2
#pragma link C++ function A2RootUtils+;
#endif

#pragma link C++ class HistoStacker+;

#endif
