

#Path is this file:
SOURCE="${BASH_SOURCE[0]}"
EXTRA="${1}"

function path_remove {
  echo "Removing ${1} from PATH"
  PATH=":$PATH:"
  PATH=${PATH//":"/"::"}
  PATH=${PATH//":$1:"/}
  PATH=${PATH//"::"/":"}
  PATH=${PATH#:}; PATH=${PATH%:}
}

if [[ "${EXTRA}" == "clean" ]]; then
    echo "Configuring enviroment cleanly"
    if [[ -e ${AGRELEASE} ]]; then
        echo "AGRELEASE previously set to ${AGRELEASE}"
        unset LD_LIBRARY_PATH
        path_remove ${AGRELEASE}/bin/
        path_remove ${AGRELEASE}/bin/simulation/
        path_remove ${AGRELEASE}/scripts/
    fi
    unset ROOT_INCLUDE_PATH
    unset AGRELEASE
    unset AGMIDASDATA
    unset A2DATAPATH
    unset AGOUTPUT
    unset EOS_MGM_URL
    unset MCDATA
    unset ROOTSYS
fi

# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    # if $SOURCE was a relative symlink, we need to resolve it relative
    #to the path where the symlink file was located
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
export AGRELEASE="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
#Over write data paths in 'localised profiles' based on host /domain names below
export AGMIDASDATA=${AGRELEASE}
export A2DATAPATH=${AGRELEASE}/alpha2

# It can be used to tell the ROOTUTILS to fetch an output
# rootfile somewhere different from the default location
if [[ -z ${AGOUTPUT} ]]; then
    export AGOUTPUT=${AGRELEASE}/root_output_files # this is the default location
fi

#Use EOS PUBLIC if not already set
if [ -e ${EOS_MGM_URL} ]; then
  export EOS_MGM_URL=root://eospublic.cern.ch
fi


# This MUST be set in order to create the simulation output
if [[ -z "${MCDATA}" ]]; then
    export MCDATA=${AGRELEASE}/simulation
fi


#Computer profiles

alphaBeast()
{
  #. ~/packages/root_build/bin/thisroot.sh
   . /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc9-opt/setup.sh
    echo -e " \e[34m `git status | head -1`\e[m"	
}

alphaCrunch()
{
  . /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-centos7/setup.sh
  . /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.20.06/x86_64-centos7-gcc48-opt/bin/root/thisroot.sh

}

acapra()
{
    echo -e " \e[91m Hi Andrea! \e[m"
    export PATH="$AGRELEASE/scripts/andrea":"$AGRELEASE/simulation/garfieldpp/scripts":$PATH
    echo -e " \e[34m `git status | head -1`\e[m"
}
gareth()
{
  echo "Gareth..."
  export AGMIDASDATA="$AGRELEASE/midas_data/"
}
lukasstartup()
{
    echo -e " \e[91m Hi Lukas! \e[m"
    #export AGOUTPUT="/eos/user/l/lgolino/Documents/ALPHA/root_output_files/GarethBranch_v2/"
    #export AGOUTPUT="/eos/user/l/lgolino/Documents/ALPHA/root_output_files/AGML/"
    #export AGOUTPUT="/eos/user/l/lgolino/Documents/ALPHA/root_output_files/AHRuns/"
    echo -e " \e[32m `gcc --version | head -1`\e[m"
    echo -e " \e[34m `git status | head -1`\e[m"
    echo $AGOUTPUT
}

lxplus()
{
  export AGMIDASDATA="/eos/experiment/ALPHAg/midasdata_old"
  export A2MIDASDATA="/eos/experiment/alpha/midasdata"
  if [ -z "$ROOTSYS" ]; then    
      echo "Setting EL9 lxplus/batch environment variables"
      if [ -d "/cvmfs/sft.cern.ch/lcg/views/" ]; then
	  . /cvmfs/sft.cern.ch/lcg/views/LCG_104/x86_64-el9-gcc12-opt/setup.sh
      else
	  echo "cvmfs not found! Please install and mount cvmfs"
      fi
  fi
}

condor()
{
    export AGMIDASDATA="/eos/experiment/ALPHAg/midasdata_old"
    export A2MIDASDATA="/eos/experiment/alpha/midasdata"
    if [ -z "$ROOTSYS" ]; then
	echo "Setting EL9 lxplus/batch environment variables"
	if [ -d "/cvmfs/sft.cern.ch/lcg/views/" ]; then
            . /cvmfs/sft.cern.ch/lcg/views/LCG_104/x86_64-el9-gcc12-opt/setup.sh
	else
	    echo "cvmfs not found! Please install and mount cvmfs"
	fi
    fi
}

alphasuperdaq()
{
   source /home/alpha/packages/root_v6.28.10.Linux-ubuntu22-x86_64-gcc11.4/bin/thisroot.sh
   export AGOUTPUT=${AGRELEASE}/root_output_files
}





echo "############## ALPHA Software Setup ##############"
echo "Hostname: " `hostname`
echo "Username: " `whoami`
echo "##################################################"

# ROOT (required)
if [ ${#ROOTSYS} -lt 3 ]; then
    echo "> "
    echo "> ROOT will be setup based on the host"
    echo "> or you can do it manually"
    echo "> "
else
    echo "ROOTSYS set... not over writing: $ROOTSYS"
fi

# MIDAS (optional)
if [ -z ${MIDASSYS+x} ]; then
    echo "MIDASSYS not set, continuing without MIDAS"
else
    echo "MIDASSYS set: $MIDASSYS"
fi

# Garfield++ (optional, required for simulation)
if [ `echo "${GARFIELD_HOME}" | wc -c` -gt 1 ]; then
   if [ -n "$GARFIELD_INSTALL" ]; then
       echo "GARFIELD_HOME set to ${GARFIELD_HOME}"
   else
       echo "Verify your Garfield++ installation"
   fi
else
   echo "GARFIELD_HOME not set"
fi

#Setup LD_LIBRARY_PATH
for AG_LIB_PATH in ana/obj {,build/}analib {,build/}aged {,build/}recolib {,build/}a2lib {,build/}rootUtils; do
  if echo "${LD_LIBRARY_PATH}" | grep "${AGRELEASE}/${AG_LIB_PATH}/" > /dev/null; then
    NOTHING_TO_DO=1
  else
    #echo "Adding ${AG_LIB_PATH} to LD_LIBRARY_PATH"
    export  LD_LIBRARY_PATH=${AGRELEASE}/${AG_LIB_PATH}/:${LD_LIBRARY_PATH}
  fi
done
echo "Adding $AGRELEASE/bin/lib to LD_LIBRARY_PATH"
export LD_LIBRARY_PATH="$AGRELEASE/bin/lib:"$LD_LIBRARY_PATH


#Set up Root include path
for AG_ROOT_LIB_PATH in ana/include analib/include rootUtils/include aged recolib/include a2lib/include a2lib/legacy; do
  if echo "${ROOT_INCLUDE_PATH}" | grep "${AGRELEASE}/${AG_ROOT_LIB_PATH}/" > /dev/null; then
    NOTHING_TO_DO=1
  else
#    echo "Adding ${AG_ROOT_LIB_PATH} to ROOT_INCLUDE_PATH"
    export  ROOT_INCLUDE_PATH=${AGRELEASE}/${AG_ROOT_LIB_PATH}/:${ROOT_INCLUDE_PATH}
  fi
done
echo "Adding $AGRELEASE/bin/include to ROOT_INCLUDE_PATH"
export ROOT_INCLUDE_PATH=${AGRELEASE}/bin/include:${ROOT_INCLUDE_PATH}

#Add scripts to BIN path
for AG_BIN_PATH in scripts bin bin/simulation; do
  if echo ${PATH} | grep "${AGRELEASE}/${AG_BIN_PATH}/" > /dev/null; then
    NOTHING_TO_DO=1
  else
    echo "Adding ${AGRELEASE}/${AG_BIN_PATH} to PATH"
    export  PATH=${AGRELEASE}/${AG_BIN_PATH}/:${PATH}
  fi
done

#Setup by host
case `hostname` in
alphavme*  )
  echo "alphavme detected..."
  echo "DO NOT RUN ANALYSIS ON A VME CRATE"
  exit
  ;;
alphadaq* | alphasuperdaq* )
  echo "DAQ computer detected..."
  alphasuperdaq
  echo "DO NOT RUN ANALYSIS ON DAQ!!! Just online tools"
  return
  ;;
alphacpc04* | alphacpc09*  )
  echo -e " \e[33malphacpc04 or 09 detected...\033[0m"
  if [ -z "$ROOTSYS" ]; then
    echo "Setting up local ROOT"
    alphasuperdaq
    echo "Local ROOT: ${ROOTSYS}"
  fi
  export AGMIDASDATA="/alpha/agdaq/data"
  ;;
alphacpc39* )
  echo -e " \e[33malphacpc39 detected...\033[0m"
  if [ -z "$ROOTSYS" ]; then
    echo "Setting up local ROOT"
    source $HOME/packages/root_v6.24.02/bin/thisroot.sh
    echo "Local ROOT: ${ROOTSYS}"
  fi
  export AGMIDASDATA="/alpha/agdaq/data"
  export A2MIDASDATA="/alpha/zdata/current"
;;
*.triumf.ca )
  echo -e " \e[33m alphaXXtriumf.ca or daqXX.triumf.ca  detected...\033[0m"
  export AGMIDASDATA="/daq/alpha_data0/acapra/alphag/midasdata"
  export A2MIDASDATA="/daq/alpha_data0/acapra/alpha2/midasdata"
  export MIDASDATA="/daq/alpha_agmini/data"
  if [ `whoami` = "acapra" ] ; then
      export DATADIR=/daq/alpha_data0/acapra
      export MCDATA=${DATADIR}/alphag/MCdata
      export GARFIELDPP=${DATADIR}/alphag/GPPdata
      export AGOUTPUT="/daq/alpha_data0/acapra/alphag/output"
      export A2DATAPATH="/daq/alpha_data0/acapra/alpha2/output"
      acapra
  fi
  if [ `hostname` = "alpha03.triumf.ca" ] ; then
    . $AGRELEASE/scripts/setup_triumf_alpha03.sh
    echo "alpha03.triumf.ca -- running setup_triumf_alpha03.sh"
  fi
  ;;
alphabeast* )
  echo -e " \e[33malphabeast detected...\033[0m"
  alphaBeast
  ;;
alphacrunch* )
  echo -e " \e[33malphacrunch detected...\033[0m"
  alphaCrunch
  ;;
lxplus* )
  echo -e " \e[33mlxplus detected...\033[0m"
  lxplus
  if [ `whoami` = "gwsmith" ] ; then
      export AGOUTPUT="/afs/cern.ch/work/g/gwsmith/private/root_output_files/"
  fi
  if [ `whoami` = "lgolino" ] ; then
      lukasstartup
  fi
  if [ `whoami` = "acapra" ] ; then
      export DATADIR=/afs/cern.ch/user/a/acapra/workspace
      export AGOUTPUT=$DATADIR/output
      export A2DATAPATH=$DATADIR/data
      export MCDATA=$DATADIR/mcdata
      acapra
  fi
  ;;
*.cern.ch )
  echo -e " \e[33mcern.ch detected...\033[0m"
  condor
  if [ `whoami` = "gwsmith" ] ; then
      export AGOUTPUT="/afs/cern.ch/work/g/gwsmith/private/root_output_files/"
  fi
  if [ `whoami` = "Gareth" ] ; then
      gareth
  fi
  ;;
* )
  if [ `whoami` = "lgolino" ] ; then
      lukasstartup
  fi
  if [ `whoami` = "Gareth" ] ; then
      gareth
  fi
  if [ -n "${ROOTSYS}" ]; then
    echo "$ROOTSYS seems to be set ok"
  else
    if [ `which root-config | wc -c` -gt 5 ]; then
      echo "ROOTSYS not set but root-config found... ok"
      ROOTLIBPATH=`root-config --libdir`
      echo "Adding ${ROOTLIBPATH} to LD_LIBRARY_PATH"
      export LD_LIBRARY_PATH="${ROOTLIBPATH}:${LD_LIBRARY_PATH}"
      
    else
      echo "ROOTSYS not set... Guessing settings for new computer..."
      if [ -d "/cvmfs/sft.cern.ch/lcg/views/" ]; then
        echo "cvmfs found..."
        lxplus
      else
        echo "\tFAILED TO FIND ROOT! I don't know what to do"
      fi
    fi
  fi
  ;;
esac

echo 'gcc       :' `which gcc`
echo 'g++       :' `which g++`
echo 'c++       :' `which c++`
echo 'cc        :' `which cc`
echo 'gcc ver.  :' `gcc --version | head -1`
echo "ROOTSYS   : ${ROOTSYS}"
if [ `which root-config | wc -c` -gt 5 ]; then
    echo 'ROOT ver. :' `root-config --version`
fi
if [ `which geant4-config | wc -c` -gt 5 ]; then
    echo 'Geant4 v. :' `geant4-config --version`
fi
echo "AGRELEASE : ${AGRELEASE}"
echo "AGOUTPUT  : ${AGOUTPUT}"
echo "A2DATAPATH: ${A2DATAPATH}"
