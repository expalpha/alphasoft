#!/bin/bash

case `hostname` in
alphasuperdaq* | alphagdaq*)
    echo "Good, we are on alphagdaq"
    ;;
*)
    echo "The start_daq script should be executed on alphagdaq"
    exit 1
    ;;
esac


# /zssd/home1/agdaq/pythonMidas/endrun.py
#~/andrea/agdaqscripts/endrun.py

RUNNO=${1}
if [ -z ${RUNNO} ]; then
   echo "No run number set"
   Run_number=`odbedit -c 'ls "/Runinfo/Run number"' | grep -v command | awk '{print $3}' | head -n1`
   echo ""
   echo ${Run_number}
   echo ""
   RUNNO=`echo $Run_number `
fi

echo "Processing ${RUNNO}"

sleep 3

cd ~/alphasoft
. agconfig.sh

if [ ${RUNNO} -lt 3000 ]; then
   echo "Invalid run Number"
   exit
fi

if [ ${RUNNO} -gt 15000 ]; then
   echo "Invalid run Number"
   exit
fi

cd $AGRELEASE

#echo "Waiting for analysis to finish and close the root file"
#for i in `seq 1 15`; do
#   root -b -l -q root_output_files/output0${RUNNO}.root 1> /dev/null 2> errout
#   ERRORS=`cat errout | wc -l`
#   if [ ${ERRORS} -eq 0 ]; then
#      echo "Timeout... rootfile never closed?"
#      break
#   fi
#   echo "Failed to open"
#   sleep 4
#done
#echo "Wait over"


sleep 1
#mkdir -p $AGRELEASE/AutoChronoPlots/${RUNNO}
#mkdir -p $AGRELEASE/AutoChronoPlots/${RUNNO}/VertexPlots
ssh alpha@alphasuperdaq "cd super_ALPHAg && bash -c \". agconfig.sh && ./scripts/alphagonline/EndRun.sh ${RUNNO} \""
echo `pwd`
