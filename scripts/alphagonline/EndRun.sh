#!/bin/bash

case `hostname` in
alphasuperdaq* | alphagdaq*)
    echo "Good, we are on alphagdaq"
    ;;
*)
    echo "The start_daq script should be executed on alphagdaq"
    exit 1
    ;;
esac


# /zssd/home1/agdaq/pythonMidas/endrun.py
~/andrea/agdaqscripts/endrun.py

RUNNO=${1}
if [ -z ${RUNNO} ]; then
   echo "No run number set"
   Run_number=`odbedit -c 'ls "/Runinfo/Run number"' | grep -v command | awk '{print $3}' | head -n1`
   echo ""
   echo ${Run_number}
   echo ""
   RUNNO=`echo $Run_number `
fi

echo "Processing ${RUNNO}"

sleep 3


if [ ${RUNNO} -lt 10000 ]; then
   echo "Invalid run Number (lower than 10000)"
   exit
fi

if [ ${RUNNO} -gt 31000 ]; then
   echo "Invalid run Number"
   exit
fi

cd $AGRELEASE

echo "Waiting for analysis to finish and close the root file"
for i in `seq 1 15`; do
   root -b -l -q $AGOUTPUT/output${RUNNO}.root 1> /dev/null 2> errout
   ERRORS=`cat errout | wc -l`
   if [ ${ERRORS} -eq 0 ]; then
      echo "Timeout... rootfile never closed?"
      break
   fi
   echo "Failed to open"
   sleep 4
done
echo "Wait over"

sleep 1

echo "WriteStackingSummary"
macro_main_WriteAGStackingSummary.exe ${RUNNO}   &>> logs/R${RUNNO}.EndRun.log
#echo ".L ana/macros/WriteStackingSummary.C
#WriteStackingSummary(${RUNNO})
#.q
#" | root -l -b  &>> logs/R${RUNNO}.EndRun.log
mkdir -p AutoChronoPlots/${RUNNO}/
ls -ld AutoChronoPlots/${RUNNO}/

mkdir -p AutoChronoPlots/${RUNNO}/VertexPlots/
ls -ld AutoChronoPlots/${RUNNO}/

#rsync -av --remove-source-files R${RUNNO}_StackingSummary.txt agdaq@alphagdaq:/home/alpha/public_html/AutoChronoPlots/${RUNNO}/
mv -v R${RUNNO}_StackingSummary.txt /home/alpha/public_html/AutoChronoPlots/${RUNNO}/

echo "Save FRD"

macro_main_SaveAGFRD.exe ${RUNNO}   &>> logs/R${RUNNO}.EndRun.log
#echo ".L ana/macros/SaveFRD.C
#SaveFRD(${RUNNO})
#.q
#" | root -l -b &>> logs/R${RUNNO}.EndRun.log

echo "rTPC QOD"
# rTPC QOD
root -b -l -q $AGOUTPUT/output${RUNNO}.root $AGRELEASE/ana/macros/rTPC_QOD.C &>> logs/R${RUNNO}.EndRun.log
mv rTPC_QOD_R${RUNNO}.png /home/alpha/public_html/AutoChronoPlots/${RUNNO}/

echo "BV QOD"
# BV QOD
root -b -l -q $AGOUTPUT/output${RUNNO}.root $AGRELEASE/ana/macros/BV_QOD.C &>> logs/R${RUNNO}.EndRun.log
mv BV_QOD_R${RUNNO}.png /home/alpha/public_html/AutoChronoPlots/${RUNNO}/


echo "SaveBackgroundVerts"

echo ".L ana/macros/SaveBackgroundVerts.C
SaveBackground(${RUNNO})
.q
" | root -l -b

echo "SavePbarEvent"

echo ".L ana/macros/SavePbarEvent.C
SavePbarDEvent(${RUNNO})
.q
" | root -l -b

#rsync -av --remove-source-files R${RUNNO}-*.png R${RUNNO}-*.csv agdaq@alphagdaq:/home/alpha/public_html/AutoChronoPlots/${RUNNO}/VertexPlots/
mv -v R${RUNNO}-*.png R${RUNNO}-*.csv /home/alpha/public_html/AutoChronoPlots/${RUNNO}/VertexPlots/

echo "SaveMixingVerts"
echo ".L ana/macros/SaveMixingVerts.C
SaveAllMixing(${RUNNO})
.q
" | root -l -b  &>> logs/R${RUNNO}.EndRun.log

#rsync -av --remove-source-files  AutoChronoPlots/${RUNNO}/ agdaq@alphagdaq:/home/alpha/public_html/AutoChronoPlots/${RUNNO}
mv -v R${RUNNO}-AllMixings.png /home/alpha/public_html/AutoChronoPlots/${RUNNO}/
mv -v R${RUNNO}-*.png R${RUNNO}-*.csv /home/alpha/public_html/AutoChronoPlots/${RUNNO}/VertexPlots/

echo "Generating Chrono plots..."
macro_main_SaveChronoDumps.exe ${RUNNO} &>> logs/R${RUNNO}.EndRun.log
macro_main_SaveTPCDumps.exe ${RUNNO} &>> logs/R${RUNNO}.EndRun.log
#echo ".L ana/macros/SaveAllDumps.C
#SaveAllDumps(${RUNNO})
#SaveAllDumpsTPC(${RUNNO})
#.q
#" | root -l -b &>> logs/R${RUNNO}.EndRun.log


#rsync -av --remove-source-files R${RUNNO}.log R${RUNNO}-*.png rTPC_QOD_R${RUNNO}.png agdaq@alphagdaq:/home/alpha/public_html/AutoChronoPlots/${RUNNO}/
mv -v R${RUNNO}.log R${RUNNO}-*.png /home/alpha/public_html/AutoChronoPlots/${RUNNO}/


#rm -rv AutoChronoPlots/${RUNNO}/

echo "done"


echo "Cleaning up old runs"


#cd $AGRELEASE/AutoChronoPlots

for i in `seq $(( ${RUNNO} - 200 )) $(( ${RUNNO} - 100 ))`; do
    rm -vrf $AGRELEASE/AutoChronoPlots/${i}
done

echo "Calibrating barrel detector"
# arguments are run number, maximum age of calibration file in runs, required time of cosmics, and "batch size" (loads data 1800s at a time)
root -b -l -q "ana/macros/bsc/CalibrateBarrel.C(${RUNNO},20,10800,1800)" &>> logs/R${RUNNO}.EndRun.log
echo "Done calibrating barrel detector
