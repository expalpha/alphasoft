import uproot
import matplotlib.pyplot as plt
import numpy as np
from os import environ, makedirs, path
import argparse
from math import sqrt

# ASIC 1 & 2 --> N-side
# ASIC 3 & 4 --> P-side

position_map = {"AD_inn":(-0.5,9.5),"AD_mid":(9.5,21.5),"AD_out":(21.5,35.5),
                "POS_inn":(35.5,45.5),"POS_mid":(45.5,57.5),"POS_out":(57.5,71.5)}

def get_pos(n):
    for k in position_map:
        if n > position_map[k][0] and n < position_map[k][1]: return k

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# avoid-line-color-repetition
# https://stackoverflow.com/a/52910117
colormap = plt.cm.nipy_spectral
colors = colormap(np.linspace(0,1,16))
#ax.set_prop_cycle('color', colors)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
# Per module occupancy of RawHits

def fun_si_occ(file,out_dir="./"):
    print("Modules Occupancy")
    si_occ = file["svd_qod"]["Si_Occs"]
    fig, ax = plt.subplots(figsize=(15, 8),num="siocc")
    x = si_occ.axis().edges()[:-1]
    y = si_occ.values()
    ax.errorbar(x,y,yerr=si_occ.errors(),linestyle='none',marker='o')
    ax.set_xlabel("Hybrid Number")
    # _,ymax = ax.set_ylim()
    # ax.set_ylim(0,ymax)
    ax.grid()
    ax.set_title("Occupancy per Silicon Hybrid")
    ax.axvspan(-0.5,9.5,hatch='/',color="green",alpha=0.3,label="AD inn")
    ax.axvspan(9.5,21.5,hatch='/',color="yellow",alpha=0.3,label="AD mid")
    ax.axvspan(21.5,35.5,hatch='/',color="red",alpha=0.3,label="AD out")
    ax.axvspan(35.5,45.5,hatch='\\',color="green",alpha=0.3,label="e+ inn")
    ax.axvspan(45.5,57.5,hatch='\\',color="yellow",alpha=0.3,label="e+ mid")
    ax.axvspan(57.5,71.5,hatch='\\',color="red",alpha=0.3,label="e+ out")
    ax.legend()
    fig.tight_layout()
    fig.savefig(f"{out_dir}/si_occ.png",dpi=300)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Per-strip analysis

def plot(si_num,hname,hist,ax,title="",ytitle=""):
    for VA in range(1,5):
        if f"ASIC{VA}" in hname:
            #print(hname)
            x = hist.axis().edges()[:-1]
            y = hist.values()
            ax[VA-1].errorbar(x,y,yerr=hist.errors(),linestyle='none',marker='.',label=f"Si{si_num}")
            if VA < 3: ax[VA-1].set_title(f"ASIC {VA} N-side {title}")
            else: ax[VA-1].set_title(f"ASIC {VA} P-side {title}")
            ax[VA-1].set_xlabel("Strip Number")
            ax[VA-1].set_ylabel(ytitle)
            if ytitle == "ADC": ax[VA-1].set_ylim(-200.,200.)
            ax[VA-1].grid(True)

def plot_qod_strips(dir,fignum,htag,title="",ytitle="",out_dir="./"):
    fig1 = {}
    ax1 = {}
    for pos in position_map:
        #print(pos)
        f1,a1 = plt.subplots(2,2,sharex=True,sharey=False,figsize=(20, 13),num=fignum+"_"+pos)
        fig1[pos] = f1
        ax1[pos] = a1.flatten()
        # avoid-line-color-repetition
        for a in ax1[pos]: a.set_prop_cycle('color', colors)

    for hname in dir:
        hist = dir[hname]
        hyb_num = int(hname.split("_")[2][2:])
        pos = get_pos(hyb_num)
        #print(f"Si {hyb_num} is in {pos}")
        if htag in hname:
            plot(hyb_num,hname,hist,ax1[pos],title,ytitle)

    for pos in position_map:
        ax1[pos][1].legend()
        fig1[pos].tight_layout()
        fig1[pos].savefig(f"{out_dir}/{fignum}_{pos}.png",dpi=300)
    

def fun_strp_occ(file,out_dir="./"):
    dir = file["svd_qod"]["Occupancy"]
    print("Plot Strip Occupancy")
    plot_qod_strips(dir,fignum="strpocc",htag="OCC",title="Occupancy",out_dir=out_dir)
    print("Plot Strip ADC mean")
    plot_qod_strips(dir,fignum="meanadc",htag="ADCmean",title="Mean ADC (Pedestal Subtracted)",ytitle="ADC",out_dir=out_dir)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Hybrids efficiency based on cosmic rays

def hit_efficiency(file,out_dir="./"):
    print("Hit Efficiency")
    hpocc = file["HitEfficiency"]["hphitsocc"]
    hnocc = file["HitEfficiency"]["hnhitsocc"]
    hpeff = file["HitEfficiency"]["hpeff"]
    hneff = file["HitEfficiency"]["hneff"]
    fig1, ax1 = plt.subplots(2,2,sharex=True,sharey=False,figsize=(15, 8),num="hiteff")
    ax1 = ax1.flatten()

    x = hpocc.axis().edges()[:-1]+0.5
    y = hpocc.values()
    ax1[0].plot(x,y,marker='o',linestyle='none')
    ax1[0].set_title("P Occupancy")
    ax1[0].grid(True)

    x = hnocc.axis().edges()[:-1]+0.5
    y = hnocc.values()
    ax1[1].plot(x,y,marker='o',linestyle='none')
    ax1[1].set_title("N Occupancy")
    ax1[1].grid(True)

    x = hpeff.axis().edges()[:-1]+0.5
    y = hpeff.values()
    ax1[2].plot(x,y,marker='o',linestyle='none')
    ax1[2].set_title("P Efficiency")
    ax1[2].grid(True)

    x = hneff.axis().edges()[:-1]+0.5
    y = hneff.values()
    ax1[3].plot(x,y,marker='o',linestyle='none')
    ax1[3].set_title("N Efficiency")
    ax1[3].grid(True)

    fig1.tight_layout()    
    fig1.savefig(f"{out_dir}/hit_efficiency.png",dpi=300)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def histo_report(file,out_dir="./"):
    print("Occupancy from Analysis Report")
    hNocc = file["AnalysisReport"]["HybridNSideOccupancy"]
    hPocc = file["AnalysisReport"]["HybridPSideOccupancy"]

    fig1, ax1 = plt.subplots(2,1,sharex=True,sharey=False,figsize=(15, 8),num="occupancy")
    ax1 = ax1.flatten()

    x = hNocc.axis().edges()[:-1]
    y = hNocc.values()
    ax1[0].plot(x,y,marker='o',linestyle='none')
    ax1[0].set_title("N Occupancy")
    ax1[0].grid(True)

    x = hPocc.axis().edges()[:-1]
    y = hPocc.values()
    ax1[1].plot(x,y,marker='o',linestyle='none')
    ax1[1].set_title("P Occupancy")
    ax1[1].grid(True)

    fig1.tight_layout()
    fig1.savefig(f"{out_dir}/strip_occupancy.png",dpi=300)


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# From alphaStrips

def get_firstStrip_ASIC(sil,asic):
    return sil*128*4+128*asic

def get_lastStrip_ASIC(sil,asic):
    return sil*128*4+128*asic+128


def plot_alphastrips(file,out_dir="./"):
    print("alphaStrips")
    strips = file["alphaStrip Tree"].arrays(["stripNumber","stripMean","stripRMS","stripMeanSubRMS"], library="pd")
    title = "Strip RMS"
    ytitle = "RMS [ADC]"

    fig1 = {}
    ax1 = {}
    for pos in position_map:
        f1, a1 = plt.subplots(2,2,sharex=True,sharey=True,figsize=(20,13),num="strip_rms_"+pos)
        fig1[pos] = f1
        ax1[pos] = a1.flatten()
        # avoid-line-color-repetition
        for a in ax1[pos]: a.set_prop_cycle('color', colors)

    for m in range(0,72):
        pos = get_pos(m)
        for VA in range(1,5):
            first_strip = get_firstStrip_ASIC(m,VA-1)
            last_strip = get_lastStrip_ASIC(m,VA-1)
            s = strips.iloc[first_strip:last_strip]["stripNumber"].to_numpy()-first_strip
            r = strips.iloc[first_strip:last_strip]["stripRMS"].to_numpy()
            ax1[pos][VA-1].scatter(x=s,y=r,label=f"Si{m}")
            if VA < 3:
                ax1[pos][VA-1].set_title(f"ASIC {VA} N-side {title}")
            else: 
                ax1[pos][VA-1].set_title(f"ASIC {VA} P-side {title}")
            ax1[pos][VA-1].set_xlabel("Strip Number")
            ax1[pos][VA-1].set_ylabel(ytitle)
            ax1[pos][VA-1].grid(True)

    for pos in position_map:
        ax1[pos][1].legend()
        fig1[pos].tight_layout()
        fig1[pos].savefig(f"{out_dir}/stripRMS_{pos}.png",dpi=300)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("run",help="run number",type=int)
    args = parser.parse_args()
    run_number = args.run
    print(f"Run {run_number}")

    dir1 = environ['AGRELEASE']+"/"+"AutoPlots"
    makedirs(dir1, exist_ok = True)
    dir2 = environ['AGRELEASE']+"/"+"AutoPlots/"+f"R{run_number}SVDQOD"
    makedirs(dir2, exist_ok = True)
    if path.isdir(dir2): print(f"Saving plots to {dir2}")
    else: print(f"Error creating output directory {dir2}")

    with uproot.open(f"{environ['AGOUTPUT']}/output{run_number}.root") as file:
        fun_si_occ(file,dir2)
        plt.close()
        fun_strp_occ(file,dir2)
        plt.close()
        hit_efficiency(file,dir2)
        plt.close()
        histo_report(file,dir2)
        plt.close()

    with uproot.open(f"{environ['A2DATAPATH']}/alphaStrips{run_number}offline.root") as file:
        plot_alphastrips(file,dir2)
        plt.close()

    print("DONE")
    #plt.show()
