#!/bin/bash

cd $AGRELEASE
. agconfig.sh

mkdir psr_root_output
mkdir psr_csv_files

for run_num in 70329 70330 70331 70332 70333 70334 70335 70336 70328 70377 70378 70379 70380 70381 70382 70383 70384 70385 70386 70462 70463 70464 70465 70467 70468 70469 70470 70461 70471 70466 70267 70428
do
   echo "alphaStrips.exe --mt /eos/experiment/alpha/midasdata/run${run_num}sub*.mid.lz4"
   alphaStrips.exe --mt /eos/experiment/alpha/midasdata/run${run_num}sub*.mid.lz4
   echo "alphaAnalysis.exe -D$AGRELEASE/psr_root_output/ --mt /eos/experiment/alpha/midasdata/run${run_num}sub*.mid.lz4 -- --usemva /home/alpha/gareth/ALPHA2-2023-MVA/MVA/Model/dataset/weights/A2TMVAClassification_BDT.weights.xml --mvacut 0.166 --mvacut 0.104 --mvacut 0.080"
   alphaAnalysis.exe -D$AGRELEASE/psr_root_output/ --mt /eos/experiment/alpha/midasdata/run${run_num}sub*.mid.lz4 -- --usemva /home/alpha/gareth/ALPHA2-2023-MVA/MVA/Model/dataset/weights/A2TMVAClassification_BDT.weights.xml --mvacut 0.166 --mvacut 0.104 --mvacut 0.080
# CutsType0 = has vertex, 1 = passed cuts, 2 = online mva, 3 = equal sig to pc (0.166), 4 = punzi FOM optimal (0.104), 5 = equal bkg to pc (0.080)

   echo "root analysis for run ${run_num}"
   root -b -l <<EOF
   TA2Plot a;
   rootUtils::AddPriorityROOTFilePath("$AGRELEASE/psr_root_output/");
   a.AddDumpGates(${run_num},{"Microwave Dump"},{-1});
   a.LoadData();
   a.ExportCSV("microwave${run_num}");
   .q
EOF

   mv microwave*.csv psr_csv_files

done
