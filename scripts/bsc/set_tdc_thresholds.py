#!/usr/bin/python3

import socket
import sys
import argparse
import subprocess as sp
import midas.client
import numpy as np
import time

# Mapping between tdc channels and odb entries, see elog/Detectors/6085
bot_mapping = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63]
top_mapping = [67, 66, 65, 64, 68, 70, 69, 71, 75, 74, 73, 72, 79, 78, 77, 76, 83, 82, 81, 80, 87, 86, 85, 84, 91, 90, 89, 88, 95, 94, 93, 92, 99, 98, 97, 96, 103, 102, 101, 100, 107, 106, 105, 104, 111, 110, 109, 108, 115, 114, 113, 112, 119, 118, 117, 116, 123, 122, 121, 120, 127, 126, 125, 124]

# Thresholds to set, see elog/Detectors/6086
thr_by_bar = [0.0065, 0.0072, 0.0090, 0.0045, 0.0057, 0.0065, 0.0045, 0.0065, 0.0065, 0.0035, 0.0090, 0.0075, 0.0085, 0.0072, 0.0047, 0.0107, 0.0052, 0.0070, 0.0057, 0.0070, 0.0083, 0.0045, 0.0125, 0.0052, 0.0070, 0.0035, 0.0075, 0.0090, 0.0050, 0.0062, 0.0075, 0.0065, 0.0052, 0.0083, 0.0062, 0.0057, 0.0130, 0.0052, 0.0052, 0.0085, 0.0065, 0.0075, 0.0075, 0.0070, 0.0065, 0.0075, 0.0075, 0.0070, 0.0045, 0.0070, 0.0020, 0.0065, 0.0083, 0.0057, 0.0125, 0.0062, 0.0045, 0.0095, 0.0123, 0.0052, 0.0067, 0.0085, 0.0062, 0.0062, 0.0045, 0.0045, 0.0083, 0.0062, 0.0055, 0.0052, 0.0075, 0.0045, 0.0097, 0.0057, 0.0140, 0.0070, 0.0097, 0.0065, 0.0075, 0.0075, 0.0065, 0.0083, 0.0085, 0.0057, 0.0090, 0.0118, 0.0065, 0.0075, 0.0057, 0.0052, 0.0050, 0.0090, 0.0040, 0.0070, 0.0090, 0.0040, 0.0083, 0.0083, 0.0070, 0.0032, 0.0052, 0.0052, 0.0090, 0.0057, 0.0062, 0.0083, 0.0083, 0.0123, 0.0075, 0.0102, 0.0090, 0.0045, 0.0085, 0.0070, 0.0085, 0.0065, 0.0042, 0.0040, 0.0070, 0.0040, 0.0083, 0.0057, 0.0040, 0.0075, 0.0040, 0.0032, 0.0075, 0.0040]


def setsingleASDthr(client,channel,thr,end):
    print(f'Settting new threshold {thr} V at {end} ch{channel}')

    # set new thr
    key='/Equipment/BVlv'+end+'/Settings/asd_dac_setpoint['+str(channel)+']'
    client.odb_set(key,thr)
    #------------------------------------------------------


def setThr(client):
      print("Setting thresholds!")
      for ch in range(64):
         setsingleASDthr(client,ch,thr_by_bar[bot_mapping[ch]],'bot')
         setsingleASDthr(client,ch,thr_by_bar[top_mapping[ch]],'top')
      print("Thresholds set! Woohoo!")

   #-------------------------------------------------------------


if __name__=='__main__':

    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    elif socket.gethostname() == 'daq16.triumf.ca':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())


    parser=argparse.ArgumentParser()
    parser.add_argument("-r","--run",action="store_true",help="Start a data acquisition for 60s after the threshold has changed")



    args=parser.parse_args()

    client = midas.client.MidasClient('TDCmapping')

    state = client.odb_get("/Runinfo/State")
    if state == midas.STATE_RUNNING:
        print("The experiment is currently running")
        if args.run:
            print("Stop the run first")
            client.disconnect()

    key='/Equipment/BVlvtop/Settings/asd_dac_setcommon'
    readback = client.odb_get(key)
    if readback == 1:
        print("Disabling DAC common setpoint")
        client.odb_set(key,0)

    key='/Equipment/BVlvbot/Settings/asd_dac_setcommon'
    readback = client.odb_get(key)
    if readback == 1:
        print("Disabling DAC common setpoint")
        client.odb_set(key,0)

    # Set the thresholds
    setThr(client)

    client.disconnect()
