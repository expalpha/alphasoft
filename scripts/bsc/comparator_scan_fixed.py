#!/usr/bin/python3

import socket
import sys
import argparse
import subprocess as sp
import midas.client
import numpy as np
import time

def _start_run(client,comment):
    print("Start Run")
    change_comment_command="set '/Experiment/Edit on start/Comment' '" + comment + "'"
    sp.run(["odbedit", "-c", change_comment_command])
    key='/Experiment/Edit on start/PulserEnable'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/BscPulserEnable'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/TrigAwMLU'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/TrigBscGrandOr'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/TrigBscMult'
    client.odb_set(key,1)
    key='/Experiment/Edit on start/TrigPulser'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/Enable_ADC'
    client.odb_set(key,1)
    key='/Experiment/Edit on start/Enable_PWB'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/Enable_TDC'
    client.odb_set(key,1)

    time.sleep(5)
    sp.run(["odbedit", "-c", "start"], stdout=sp.PIPE)

def _stop_run():
    print("Stop Run")
    sp.run(["odbedit", "-c", "stop"], stdout=sp.PIPE)


def setASDthr(client,thr,end):
    print(f'Settting new threshold {thr} V at {end}')

    # set new thr
    key='/Equipment/BVlv'+end+'/Settings/asd_dac_common_setpoint'
    client.odb_set(key,thr)

    # make sure that thr changed
    key='/Equipment/BVlv'+end+'/Settings/asd_dac_setpoint'
    #client.odb_set(key,[t]*64)

    while client.odb_get(key+'[0]') != thr:
        time.sleep(1)

    # Read the value back
    readback = client.odb_get(key)
    print(f"value of {key} is {readback}\t{len(readback)}")
    #------------------------------------------------------


def scanASDthr(client, thr_max, step, run):
    print(f'Starting the scan from {thr_max:1.3f} to step in {step:1.3f} steps')
    for t in np.arange(0.,thr_max+step,step)[::-1]:

        setASDthr(client,t,"top")
        setASDthr(client,t,"bot")
        run_comment="BV Threshold scan. Thr = " + str(t) + ' V, TrigAwMLU'

        if run:
            print(f"Taking run at threshold {t}")
            _start_run(client,run_comment)
            time.sleep(300)
            _stop_run()
            time.sleep(10)

    # after scan, set reasonable threshold
    setASDthr(client,0.025,"top")
    setASDthr(client,0.025,"bot")


if __name__=='__main__':

    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    elif socket.gethostname() == 'daq16.triumf.ca':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())


    parser=argparse.ArgumentParser()
    parser.add_argument("thr", type=float, default=0.3,help="Set maximum value for scan or fixed value when there is no scan (see below)")
    parser.add_argument("-s","--step",type=float,default=0.01,help="Step size in the scan")
    parser.add_argument("-n","--noscan",action="store_true",help="Do not scan, just set a fixed value")
    parser.add_argument("-r","--run",action="store_true",help="Start a data acquisition for 300s after the threshold has changed")



    args=parser.parse_args()

    client = midas.client.MidasClient('ASDcomparatorScan')

    state = client.odb_get("/Runinfo/State")
    if state == midas.STATE_RUNNING:
        print("The experiment is currently running")
        if args.run:
            print("Stop the run first")
            client.disconnect()

    key='/Equipment/BVlvtop/Settings/asd_dac_setcommon'
    readback = client.odb_get(key)
    if readback == 0:
        print("Enabling DAC common setpoint")
        client.odb_set(key,1)

    key='/Equipment/BVlvbot/Settings/asd_dac_setcommon'
    readback = client.odb_get(key)
    if readback == 0:
        print("Enabling DAC common setpoint")
        client.odb_set(key,1)

    if args.noscan:
        setASDthr(client,args.thr,"top")
        setASDthr(client,args.thr,"bot")
    else:
        key="/Logger/Write data"
        client.odb_set(key,1)
        scanASDthr(client,args.thr,args.step,args.run)


    client.disconnect()
