#!/usr/bin/python3

import socket
import sys
import argparse
import subprocess as sp
import midas.client
import numpy as np
import time

def _start_run(client,comment):
    print("Start Run")
    change_comment_command="set '/Experiment/Edit on start/Comment' '" + comment + "'"
    sp.run(["odbedit", "-c", change_comment_command])
    key='/Experiment/Edit on start/PulserEnable'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/BscPulserEnable'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/TrigAwMLU'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/TrigBscGrandOr'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/TrigBscMult'
    client.odb_set(key,1)
    key='/Experiment/Edit on start/TrigPulser'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/Enable_ADC'
    client.odb_set(key,1)
    key='/Experiment/Edit on start/Enable_PWB'
    client.odb_set(key,0)
    key='/Experiment/Edit on start/Enable_TDC'
    client.odb_set(key,1)

    time.sleep(5)
    sp.run(["odbedit", "-c", "start"], stdout=sp.PIPE)

def _stop_run():
    print("Stop Run")
    sp.run(["odbedit", "-c", "stop"], stdout=sp.PIPE)


def setsingleASDthr(client,channel,thr,end):
    print(f'Settting new threshold {thr} V at {end} ch{channel}')

    # set new thr
    key='/Equipment/BVlv'+end+'/Settings/asd_dac_setpoint['+str(channel)+']'
    client.odb_set(key,thr)

    # make sure that thr changed
    key='/Equipment/BVlv'+end+'/Settings/asd_dac_setpoint'
    #client.odb_set(key,[t]*64)

    while client.odb_get(key+'['+str(channel)+']') != thr:
        time.sleep(1)

    # Read the value back
    readback = client.odb_get(key)
    print(f"value of {key} is {readback}\t{len(readback)}")
    #------------------------------------------------------


def scanDACmapping(client, run):
   for end in ['bot','top']:
      print(f'Starting the scan for {end}')
      # Get common threshold
      key = '/Equipment/BVlv'+end+'/Settings/asd_dac_common_setpoint'
      common_setpoint = client.odb_get(key)
      print(f'Common setpoint is {common_setpoint}')

      # For each channel
      for ch in range(64):
         # Set threshold high, will see few counts
         setsingleASDthr(client,ch,1.,end)

         # Start 5 minute run
         run_comment = f'TDC mapping scan, {end} {ch}'
         if run:
            _start_run(client,run_comment)
            time.sleep(60)
            _stop_run()
            time.sleep(10)
         else:
            print(f'Would be running, instead waiting 5s')
            time.sleep(5)

         # Set threshold back to common
         setsingleASDthr(client,ch,common_setpoint,end)
   #-------------------------------------------------------------


if __name__=='__main__':

    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    elif socket.gethostname() == 'daq16.triumf.ca':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())


    parser=argparse.ArgumentParser()
    parser.add_argument("-r","--run",action="store_true",help="Start a data acquisition for 60s after the threshold has changed")



    args=parser.parse_args()

    client = midas.client.MidasClient('TDCmapping')

    state = client.odb_get("/Runinfo/State")
    if state == midas.STATE_RUNNING:
        print("The experiment is currently running")
        if args.run:
            print("Stop the run first")
            client.disconnect()

    key='/Equipment/BVlvtop/Settings/asd_dac_setcommon'
    readback = client.odb_get(key)
    top_was_common = False
    if readback == 1:
        print("Disabling DAC common setpoint")
        top_was_common = True
        client.odb_set(key,0)

    key='/Equipment/BVlvbot/Settings/asd_dac_setcommon'
    readback = client.odb_get(key)
    bot_was_common = False
    if readback == 1:
        print("Disabling DAC common setpoint")
        bot_was_common = True
        client.odb_set(key,0)

    key="/Logger/Write data"
    client.odb_set(key,1)
    scanDACmapping(client,args.run)

    if top_was_common:
       key='/Equipment/BVlvtop/Settings/asd_dac_setcommon'
       print("Re-enabling DAC common setpoint")
       client.odb_set(key,1)

    if bot_was_common:
       key='/Equipment/BVlvbot/Settings/asd_dac_setcommon'
       print("Re-enabling DAC common setpoint")
       client.odb_set(key,1)

    client.disconnect()
