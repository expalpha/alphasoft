#!/bin/env python3

import plotly.express as px
import pandas as pd
import re
import argparse

def makeDF(filename, ignore=[]):
    in_table = False
    in_header = False
    modules = []
    tmax = []
    tmean = []
    tsum = []
    with open(filename) as f:
        lines = f.readlines()
    for line in lines :
        if(in_table):
            if("---------------------" in line):
                in_table = False
                break
            # print(line[:24].strip())
            # print(line[24:].strip())
            data = line.strip().split('\t')
            if(data[0].strip() in ignore):
                continue
            modules.append(data[0].strip())
            t_max = 0.0
            t_mean = 0.0
            t_sum = 0.0
            # print(data, len(data))
            try:
                if(data[4] != '-'):
                    t_max += float(data[4])
                    t_mean += float(data[5])/float(data[1])
                    t_sum += float(data[5])
            except ValueError:
                print("ValueError1 line: ", data, data[1], data[4], data[5])
            try:
                if(data[8] != '-'):
                    t_max += float(data[9])
                    t_mean += float(data[10])/float(data[6])
                    t_sum += float(data[10])
            except ValueError:
                print("ValueError2 line: ", data, data[6], data[9], data[10])
            tmax.append(t_max)
            tmean.append(t_mean*1000.)
            tsum.append(t_sum)
        elif("Module average processing time" in line):
            in_header = True
        elif(in_header):
            if("---------------------" in line):
                in_table = True
                in_header = False
    return pd.DataFrame({'file': filename.split('.')[0], 'module': modules, 'tmax': tmax, 'tmean': tmean, 'tsum': tsum})

def collapseDF(df, total=False):
    ddfout = pd.DataFrame()
    match = re.compile(r"(.+\(\D*)\d+/\d+\)")
    modgroups = []
    for m in df.module:
        mm = match.match(m)
        if(mm):
            module = mm.group(1).strip()
            if(module[-1]=="("):
                module = module[:-1]
            modgroups.append(module)
    modgroups = list(dict.fromkeys(modgroups))
    to_drop = []
    for m in modgroups:
        # print(">> >> >>", m)
        match = df.module.str.contains(m, regex=False)
        ddf = df[match]
        # print([i for i, x in enumerate(match) if x])
        to_drop = to_drop + [i for i, x in enumerate(match) if x]
        if("(" in m and ")" not in m):
            m += ")"
        # print(m)
        # print(ddf)
        if(total):
            mn = ddf.sum(axis=0,numeric_only=True)
        else:
            mn = ddf.mean(axis=0,numeric_only=True)

        ddf = pd.DataFrame({'file': ddf.file[0:1], 'module': m, 'tmax': mn.tmax, 'tmean': mn.tmean, 'tsum': mn.tsum})
        # print("==========================================")
        # print(ddf)
        ddfout = ddfout.append(ddf).sort_index()
    df = df.drop(to_drop)
    return df.append(ddfout)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--total_cpu", "-t", action="store_true", help="display total CPU time instead of average event")
    parser.add_argument("--pie", "-p", action="store_true", help="display pie chart instead of bar chart")
    parser.add_argument("--ignore", "-i", nargs="*", default=[], help="modules to skip when plotting (e.g. Handle Sequencer)")
    parser.add_argument("--files", "-f", nargs="+", help="analyzer logs containing profiler table")
    args = parser.parse_args()
    print(args)

    if(args.pie and len(args.files) > 1):
        args.pie = False
        print("Currently pie chart is only supported for single input file")

    if(not len(args.ignore) and not args.total_cpu):
        args.ignore = ["Handle Sequencer","Handle Dumps","AnalysisReport"]
        print("Ignoring rare events", args.ignore)

    dfout = pd.DataFrame()
    for f in args.files:
        df = makeDF(f, args.ignore)
        df = collapseDF(df, args.total_cpu)
        dfout = dfout.append(df, ignore_index=True)

    if(args.total_cpu):
        if(args.pie):
            fig = px.pie(dfout, values="tsum", names="module", title="Total CPU time", labels = {"tsum":"total CPU time in s"})
        else:
            fig = px.bar(dfout, x="file", y="tsum", color="module", title="Total CPU time", labels = {"tsum":"total CPU time in s"})
            fig.update_layout(legend_traceorder="reversed")
    else:
        if(args.pie):
            fig = px.pie(dfout, values="tmean", names="module", title="Mean time per event", labels = {"tmean":"mean time per event in ms"})
        else:
            fig = px.bar(dfout, x="file", y="tmean", color="module", title="Mean time per event", labels = {"tmean":"mean time per event in ms"})
            fig.update_layout(legend_traceorder="reversed")

    fig.show()
