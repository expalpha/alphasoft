#!/bin/bash

set -x

#time jagana.exe -O${AGOUTPUT}/output07060.root --mt $AGMIDASDATA/run07060sub00*.mid.lz4 -- --bscdiag --recoff --Bfield 1 --anasettings $AGRELEASE/ana/cern2021_2.json &> RunLogs/R7060.log

#for i in $(seq -f "%f" 0.0 0.2 4.2)
for i in $(seq -f "%f" 3.8 0.2 4.2)
do
   time jagana.exe -O${AGOUTPUT}/output07060tw${i}.root --mt $AGMIDASDATA/run07060sub000.mid.lz4 -- --twA ${i} --bscdiag --recoff --Bfield 1 --bscDontWriteOffsetFile --anasettings $AGRELEASE/ana/cern2021_2.json &> RunLogs/R7060.log
done
