#!/bin/bash

case `hostname` in
alphasuperdaq*)
    echo "Good, we are on alphasuperdaq"
    ;;
*)
    echo "The start_daq script should be executed on alphadaq"
    exit 1
    ;;
esac

RUNNO=${1}
if [ -z ${RUNNO} ]; then
   echo "No run number set"
   Run_number=`odbedit -c 'ls "/Runinfo/Run number"' | grep -v command | awk '{print $3}' | head -n1`
   echo "\n"
   echo ${Run_number}
   echo "\n"
   RUNNO=`echo $Run_number `
fi


sleep 3

cd ~/alphasoft
. agconfig.sh

if [ ${RUNNO} -lt 55000 ]; then
   echo "Invalid run Number"
   exit
fi

if [ ${RUNNO} -gt 100000 ]; then
   echo "Invalid run Number"
   #exit
fi

cd $AGRELEASE

#Wait for file to open (60 second timeout)
ROOT_FILE_NAME=${AGOUTPUT}/output${RUNNO}.root
for i in `seq 1 600`; do
   # Check timeout ...
   if [ ${i} -gt 60 ]; then
      echo "Could not open file after ${i} seconds. Did the analyzer crash?"
      exit 1
   fi

   root.exe -b -l -q -e "TFile *f = TFile::Open(\"${ROOT_FILE_NAME}\"); if ((!f) || f->IsZombie() || f->TestBit(TFile::kRecovered)) { cout << \"There is a problem with the file: $filename_to_check\n\"; exit(1); }" > /dev/null
   if [ $? -ne 0 ]; then
      echo "${ROOT_FILE_NAME} has error, analysis is probably still running"
      sleep 1
      continue;
   else
      break;
   fi

 done

 
mkdir -p AutoSISPlots/${RUNNO}/
macro_main_WriteA2StackSummary.exe ${RUNNO}
#echo ".L alpha2/macros/WriteA2StackSummary.C
#WriteStackingSummaryA2(${RUNNO})
# .q
#" | root -l -b 
cat AutoSISPlots/${RUNNO}/R${RUNNO}_A2StackingSummary.txt

macro_main_MicrowaveDumps.exe ${RUNNO} > AutoSISPlots/${RUNNO}/MicrowaveAnalysis.log
#echo ".L alpha2/macros/MicrowaveDumps.C
#MicrowaveDumps(${RUNNO})
#.q
#" | root -l -b > AutoSISPlots/${RUNNO}/MicrowaveAnalysis.log

macro_main_LyAlpha.exe ${RUNNO} > AutoSISPlots/${RUNNO}/LyAlphaAnalysis.log
#echo ".L alpha2/macros/LyAlpha.C
#LyAlphaTable(${RUNNO})
#PlotLyAlphaScan(${RUNNO})
#.q
#" | root -l -b > AutoSISPlots/${RUNNO}/LyAlphaAnalysis.log

mkdir -p AutoSISPlots/${RUNNO}/LyAlpha
mv -v R${RUNNO}_Freq*.png AutoSISPlots/${RUNNO}/LyAlpha/
mv -v R${RUNNO}_Freq*.csv AutoSISPlots/${RUNNO}/LyAlpha/
mv -v R${RUNNO}_Spectrum.png AutoSISPlots/${RUNNO}/LyAlpha/
mv -v R${RUNNO}_Cooling_Freq15* AutoSISPlots/${RUNNO}/LyAlpha/
mv -v R${RUNNO}_LyA_summed* AutoSISPlots/${RUNNO}/LyAlpha/

macro_main_SaveFRD.exe ${RUNNO} > AutoSISPlots/${RUNNO}/R${RUNNO}_FRD_PassCutsEvents.log

echo ".L alpha2/macros/SubtractADPulses.C
if ( Get_A2_Spills(${RUNNO},{\"Lifetime\"},{-1}).size() > 0 ) {
   TCanvas c;
   SubtractADPulses(${RUNNO},\"CT_SiPM_OR\",\"SIS_AD\",10,{0.,13.},\"Lifetime\",0,-1,200,2000);
   c.SaveAs(\"AutoSISPlots/${RUNNO}/LifetimeFit.png\");
}
.q
" | root -l -b &> AutoSISPlots/${RUNNO}/LifetimeFit.log



echo "if ( Get_A2_Spills(${RUNNO},{\"MW 486 Frq*\"},{-1}).size() > 0 ) {
   .L alpha2/macros/Plot_2024_486_Cooled_Lineshape.C
   Plot_486_Light_And_Dark_Lineshape_Auto_Anal(${RUNNO});
}
.q
" | root -l -b &> AutoSISPlots/${RUNNO}/486Analysis.log

# Run QOD checks
echo "PrintA2QODChecks(${RUNNO})
.q
" | root -l -b > AutoSISPlots/${RUNNO}/QODChecks.log

echo "Generating SIS plots..."
macro_main_PlotMixingShapes.exe ${RUNNO} &
macro_main_SaveAllSVDDumps.exe ${RUNNO} &> AutoSISPlots/${RUNNO}/R${RUNNO}_SVDDumps.log &
macro_main_SaveAllMixings.exe ${RUNNO} &> AutoSISPlots/${RUNNO}/R${RUNNO}_Mixings.log &
macro_main_SaveAllColdDumps.exe ${RUNNO} &> AutoSISPlots/${RUNNO}/R${RUNNO}_ColdDumps.log &
macro_main_SaveAllDumps.exe ${RUNNO} &> AutoSISPlots/${RUNNO}/R${RUNNO}.log &
macro_main_PBarBudgetTool.exe ${RUNNO} &> AutoSISPlots/${RUNNO}/R${RUNNO}_budget.log &
macro_main_SaveLossesForDumps.exe ${RUNNO} &> AutoSISPlots/${RUNNO}/R${RUNNO}_LossesForDumps.log &
wait

echo "QOD plots"
root -b -l -q ${AGRELEASE}/root_output_files/output${RUNNO}.root ${AGRELEASE}/alpha2/macros/ALPHA2_QOD.C &>> R${RUNNO}.EndRun.log
echo "Done"


echo "Cleaning up old runs"


cd $AGRELEASE/AutoSISPlots

for i in `seq $(( ${RUNNO} - 200 )) $(( ${RUNNO} - 100 ))`; do
rm -vrf $AGRELEASE/AutoSISPlots/${i}
done


echo `pwd`
