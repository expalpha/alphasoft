#!/bin/bash
# usage: ./make_h5_files.sh /path/to/midas/files/ /output/path/ min_file_num max_file_num is_sim
# 1: path to midas files
# 2: path to move output h5 files to
# 3: minimum file number
# 4: maximum file number
# 5: is simulation? 1 = simulation, 0 = data

for run in $(seq $3 $4)
do
   if [ $5 -eq 1 ]; then
      filename="$1/sim_${run}.mid"
   else
      filename="$1/run${run}sub*.mid.lz4"
   fi
   echo "file is $filename"
   if [ ! -f $filename ]; then
      echo "File not found!"
      continue
   fi

   echo "# # # # #"
   echo "# # # # #"
   echo "Analyzing $filename"
   echo "# # # # #"
   echo "# # # # #"

   spacepoint_dumper.exe --mt $filename -O/dev/null  -- --anasettings $AGRELEASE/ana/jagana_2022.json

   echo "# # # # #"
   echo "# # # # #"
   echo "Moving output to $2"
   echo "# # # # #"
   echo "# # # # #"

   echo "Moving vertices-1.h5  to $2/vertices-${run}.h5"
   mv $AGRELEASE/vertices-1.h5 $2/vertices-${run}.h5
   echo "Moving spacepoints-1.h5  to $2/spacepoints-${run}.h5"
   mv $AGRELEASE/spacepoints-1.h5 $2/spacepoints-${run}.h5


done


