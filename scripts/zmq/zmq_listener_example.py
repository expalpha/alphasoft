"""Listen to ZQM messages from the ZMQ publisher example."""

import argparse

import zmq


def main(server_ip="localhost", server_port=5556):
    """
    Listen to ZMQ messages from the ZMQ publisher example.

    Parameters
    ----------
    server_ip : str
        The IP address of the server.
    server_port : int
        The port of the server.
    """

    context = zmq.Context()

    #  Socket to talk to server
    print("Connecting to zmq spill log server...")
    socket = context.socket(zmq.SUB)
    socket.connect(f"tcp://{server_ip}:{server_port}")

    socket.setsockopt_string(zmq.SUBSCRIBE, "")
    while True:
        message = socket.recv_string()
        print("Received message: %s" % message)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--server_ip", type=str, default="localhost")
    parser.add_argument("--server_port", type=int, default=5556)
    args = parser.parse_args()
    main(args.server_ip, args.server_port)

