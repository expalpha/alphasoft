#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(FOLDERNAME) $(label) $(source)
export EOS_MGM_URL=root://eosproject.cern.ch

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."


export runNum=`awk -F "," 'NR==1 { print $1 }' mva_input.txt`
export FOLDERNAME=`awk -F "," 'NR==1 { print $2 }' mva_input.txt`
export label=`awk -F "," 'NR==1 { print $3 }' mva_input.txt`
export source=`awk -F "," 'NR==1 { print $4 }' mva_input.txt`

echo "Source = $source."
echo "FOLDERNAME = $FOLDERNAME."
echo "label = $label."

# Activate environment
cd $source
source agconfig.sh
cd $AGRELEASE




# ------------------------------------------------------------------------------
# Run
echo "Signal merger running..."
macro_main_merger.exe -- --listfile ./MVA/${FOLDERNAME}/mixingList --mixing --location ./MVA/${FOLDERNAME}/dumperoutputs &>> MVA/${FOLDERNAME}/outputs/mixingMerger.log
echo "Cosmic merger running..."
macro_main_merger.exe -- --listfile ./MVA/${FOLDERNAME}/cosmicList --cosmic --location ./MVA/${FOLDERNAME}/dumperoutputs &>> MVA/${FOLDERNAME}/outputs/cosmicMerger.log
echo "Moving root files to  MVA/${FOLDERNAME}"
#touch testfile.txt
rsync -av --remove-source-files mvaSignal.root MVA/${FOLDERNAME}
rsync -av --remove-source-files mvaBackground.root MVA/${FOLDERNAME}
echo "Done with all. Files plus output log saved to MVA/${FOLDERNAME}"


# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
