#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(FOLDERNAME) $(label) $(source) $(dl)
# $(1)         $(2)       $(3)      $(4)         $(5)      $(6)     $(7)
export EOS_MGM_URL=root://eosproject.cern.ch

#BELOW ARE EXAMPLES - Please change to your folders.


##CHANGE THESE 2 LINES:
export repo_location=/afs/cern.ch/work/l/lgolino/private/alphasofts/A2ML
#export program=kagana.exe




# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

# Activate environment
cd $repo_location
source agconfig.sh
cd $AGRELEASE

#Debug strings
#which root
#which alphagonline.exe
#echo $PATH

# Make output directory
#export output_directory=/afs/cern.ch/work/l/lgolino/private/alphasofts/AGML/root_output_files/
#mkdir -p $output_directory
#mkdir -p ./root_output_files/$6

export AGOUTPUT=/eos/user/l/lgolino/Documents/ALPHA/root_output_files/AllMixingData/
mkdir -p $AGOUTPUT

# ------------------------------------------------------------------------------
# Run

##WARNING: CHANGE THESE LINES TO MATCH YOUR RUN...


echo "Trying to run alphaStrips.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt"
alphaStrips.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt
echo "Trying to run alphaAnalysis.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt"
alphaAnalysis.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt
macro_main_eventlist_generator.exe --$5 --runnumber $3
rm /eos/user/l/lgolino/Documents/ALPHA/root_output_files/AllMixingData/*$3*



# Move *.root to EOS (advised)
#rsync -av --remove-source-files root_output_files/output0$3.root $output_directory



# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
