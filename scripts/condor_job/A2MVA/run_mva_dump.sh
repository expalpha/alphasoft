#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(FOLDERNAME) $(label) $(source)
export EOS_MGM_URL=root://eosproject.cern.ch

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

function eosfail () {
   exit 20
}

test -d /eos/experiment/alpha/midasdata/ || eosfail

export run=$3
export FOLDERNAME=$4
export label=$5
export source=$6
export dl=$7

echo "Source = $source"
echo "FOLDERNAME = $FOLDERNAME"
echo "label = $label"

# Activate environment
cd $source
source agconfig.sh
cd $AGRELEASE

export AGOUTPUT=/eos/user/l/lgolino/Documents/ALPHA/root_output_files/A2ML/
mkdir -p $AGOUTPUT

#Run alphaStrips - delete this file after...
alphaStrips.exe /eos/experiment/alpha/midasdata/run${run}sub* -D$AGOUTPUT --mt -- --lite 


# ------------------------------------------------------------------------------
# Run
if [ $label = 'mixing' ]
then
    echo "Signal run ${run} running ... "
    #jagana.exe /eos/experiment/alpha/midasdata/run${run}sub* -- --lite --Bfield 1 --anasettings ana/cern2021_2.json &>> MVA/${FOLDERNAME}/outputs/M${run}_jagana.log
    #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --mixing &>> MVA/${FOLDERNAME}/outputs/M${run}_eventlist.log
    #mv eventlist0${run}.list MVA/${FOLDERNAME}
    if [ $dl == 0 ]
    then
        a2dumper.exe /eos/experiment/alpha/midasdata/run${run}sub* -O/dev/null --mt -- --lite --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel mixing &>> MVA/${FOLDERNAME}/outputs/a2dumper/M${run}mixing_a2dumper.log
    fi
    if [ $dl == 1 ]
    then
        a2dldumper.exe /eos/experiment/alpha/midasdata/run${run}sub* -O/dev/null --mt -- --lite --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel mixing &>> MVA/${FOLDERNAME}/outputs/a2dumper/M${run}mixing_a2dldumper.log
    fi
    mv dumperoutputmixing${run}.root MVA/${FOLDERNAME}/dumperoutputs
    echo "run ${run} complete."
fi

if [ $label = 'cosmic' ]
then
    echo "Background run ${run} running ... "
    #jagana.exe /eos/experiment/alpha/midasdata/run${run}sub* -- --lite --Bfield 1 --anasettings ana/cern2021_2.json &>> MVA/${FOLDERNAME}/outputs/M${run}_jagana.log
    #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --cosmic &>> MVA/${FOLDERNAME}/outputs/M${run}_eventlist.log
    #mv eventlist0${run}.list MVA/${FOLDERNAME}
    if [ $dl == 0 ]
    then
        a2dumper.exe /eos/experiment/alpha/midasdata/run${run}sub* -O/dev/null --mt -- --lite --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel cosmic &>> MVA/${FOLDERNAME}/outputs/a2dumper/M${run}cosmic_a2dumper.log
    fi
    if [ $dl == 1 ]
    then
        a2dldumper.exe /eos/experiment/alpha/midasdata/run${run}sub* -O/dev/null --mt -- --lite --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel cosmic &>> MVA/${FOLDERNAME}/outputs/a2dumper/M${run}cosmic_a2dldumper.log
    fi
    mv dumperoutputcosmic${run}.root MVA/${FOLDERNAME}/dumperoutputs
    echo "run ${run} complete."
fi

#IMPORTANT: Change this destination location if you are not using -O/dev/null option!!!
#rsync -av --remove-source-files root_output_files/output0${run}.root /eos/user/l/lgolino/Documents/ALPHA/root_output_files/MVA/


#Delete the file afterwards.
echo "Done with ${run} - deleting the strips file."
rm $AGOUTPUT/alphaStrips${run}offline.root


# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
