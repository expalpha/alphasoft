#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum)
export EOS_MGM_URL=root://eosproject.cern.ch


#BELOW ARE EXAMPLES - Please change to your folders.
##CHANGE THESE 2 LINES:
#The location of the repo where you want to run the analysis.
export repo_location=/afs/cern.ch/work/l/lgolino/private/alphasofts/A2ML
#Folder to dump output to, unless you have enough space on your afs this should be somewhere on EOS.
export AGOUTPUT=/eos/user/l/lgolino/Documents/ALPHA/root_output_files/AllMixingData/



#This checks whether we can connect to EOS or not, if not it will exit and try another node a maximum of 3 times (this number is configured in the .sub file)
function eosfail () {
   exit 20
}

test -d /eos/experiment/alpha/midasdata/ || eosfail




# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

# Activate environment
cd $repo_location
source agconfig.sh
cd $AGRELEASE

#Just making sure this folder exists...
mkdir -p $AGOUTPUT



# ------------------------------------------------------------------------------
# Run

##WARNING: CHANGE THESE LINES TO MATCH YOUR NEEDS...
if [ $5 -lt 0 ]
then
echo "Trying to run alphaStrips.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt"
alphaStrips.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt
echo "Trying to run alphaAnalysis.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt"
alphaAnalysis.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt
else
echo "Trying to run alphaStrips.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt--usetimerange $4 $5"
alphaStrips.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt--usetimerange $4 $5
echo "Trying to run alphaAnalysis.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt--usetimerange $4 $5"
alphaAnalysis.exe /eos/experiment/alpha/midasdata/run$3sub* -D$AGOUTPUT --mt--usetimerange $4 $5
fi



# Move *.root to EOS (advised) - depreciated, now we just pipe straight there.
#rsync -av --remove-source-files root_output_files/output0$3.root $output_directory

#End run
echo "Finished job $1.$2 on $(date)..."
# ------------------------------------------------------------------------------
