#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum)
export EOS_MGM_URL=root://eosproject.cern.ch




# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

function eosfail () {
   exit 20
}

test -d /eos/experiment/ALPHAg/midasdata_old/ || eosfail

export run=$3
export FOLDERNAME=$4
export label=$5
export source=$6
export dl=$7

echo "Source = $source"
echo "FOLDERNAME = $FOLDERNAME"
echo "label = $label"

# Activate environment
cd $source
source agconfig.sh
cd $AGRELEASE

export AGOUTPUT=/eos/user/l/lgolino/Documents/ALPHA/root_output_files/2024/AGMVAWithTOF
mkdir -p $AGOUTPUT

# ------------------------------------------------------------------------------
# Run

echo " .L ana/macros/bsc/CalibrateBarrel.C
CalibrateBarrel(${run},20,10800)"

echo " .L ana/macros/bsc/CalibrateBarrel.C
CalibrateBarrel(${run},20,10800)" | root -l -b &>> MVA/${FOLDERNAME}/outputs/calib/M${run}_bv_calib.log



# Move *.root to EOS (advised)
#rm -r ${output_directory}



# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
