#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum)
export EOS_MGM_URL=root://eosproject.cern.ch

#BELOW ARE EXAMPLES - Please change to your folders.


##CHANGE THESE 2 LINES:
export repo_location=/afs/cern.ch/work/l/lgolino/private/alphasofts/AGML
export program=kagana.exe




# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

# Activate environment
cd $repo_location
source agconfig.sh
cd $AGRELEASE

#Debug strings
#which root
#which alphagonline.exe
#echo $PATH

# Make output directory
#export output_directory=/afs/cern.ch/work/l/lgolino/private/alphasofts/AGML/root_output_files/
#mkdir -p $output_directory
mkdir -p ./root_output_files/$6

# ------------------------------------------------------------------------------
# Run

##WARNING: CHANGE THESE LINES TO MATCH YOUR RUN...

if [ $5 -lt 0 ]
then
echo "$program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -- --Bfield 1 --lite --anasettings ana/jagana_2022.json --usemva Run54_Ballgown"
$program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D./root_output_files/$6 --mt -- --Bfield 1 --lite --anasettings ana/jagana_2022.json --usemva $repo_location/MVA/$6/dataset/weights/AGTMVAClassification_BDT.weights.xml --eventlist MVA/eventlists/AGMVA/9782.list
else
echo "Trying to run $program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -- --anasettings ana/cern2021_2.json --usetimerange $4 $5"
$program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D./root_output_files/$6 --mt -- --Bfield 1 --lite --anasettings ana/jagana_2022.json --usemva $repo_location/MVA/$6/dataset/weights/AGTMVAClassification_BDT.weights.xml --usetimerange $4 $5
fi


# Move *.root to EOS (advised)
#rsync -av --remove-source-files root_output_files/output0$3.root $output_directory



# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
