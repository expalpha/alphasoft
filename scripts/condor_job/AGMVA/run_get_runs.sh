#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(FOLDERNAME) $(label) $(source)
export EOS_MGM_URL=root://eosproject.cern.ch

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

#!/bin/bash

# Input and output file
input_file="mva_input.txt"
output_file="alphagonlineruns.txt"

# Temporary file to store unique numbers
temp_file=$(mktemp)

# Clear the output file if it exists
> "$output_file"

# Read the first column from the input file, get unique values
awk -F, '{print $1}' "$input_file" | sort -n | uniq | while read number; do
    # For each number, generate the preceding 20 numbers
    for ((i=20; i>=0; i--)); do     #WARNING I HAVE REDUCED TO 3 FOR TESTING
    #for ((i=3; i>=0; i--)); do
        echo "$((number - i))"
    done
done | sort -n | uniq > "$temp_file"

# Write the unique numbers to the output file
mv "$temp_file" "$output_file"

echo "Unique preceding numbers written to $output_file"




# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
