#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum)
export EOS_MGM_URL=root://eosproject.cern.ch

#BELOW ARE EXAMPLES - Please change to your folders.


# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

export location=$3
export name=$4
export dl=$5

echo "location = $location."
echo "name = $name."
echo "dl? = $name."

# Activate environment
cd $location
cd ../../
source agconfig.sh
cd $location
mkdir -p outputs

if [ $dl == 0 ]
then
    root -b -l -q ./AGTMVAClassification.C\(\"$name\"\) &>> ./outputs/modelClassification.log
fi
if [ $dl == 1 ]
then
    root -b -l -q ./AGTMVARegression.C\(\"$name\"\) &>> ./outputs/modelRegression.log
fi


# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
