#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum)
export EOS_MGM_URL=root://eosproject.cern.ch

#BELOW ARE EXAMPLES - Please change to your folders.


# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

export runNum=`awk -F "," 'NR == 1 { print $1 }' mva_input.txt`
export FOLDERNAME=`awk -F "," 'NR == 1 { print $2 }' mva_input.txt`
export label=`awk -F "," 'NR == 1 { print $3 }' mva_input.txt`
export source=`awk -F "," 'NR == 1 { print $4 }' mva_input.txt`
export dl=`awk -F "," 'NR == 1 { print $5 }' mva_input.txt`

echo "Source = $source."
echo "FOLDERNAME = $FOLDERNAME."
echo "label = $label."
echo "dl = $dl."

# Activate environment
cd $source
source agconfig.sh
cd $AGRELEASE
cd  MVA/${FOLDERNAME}

#IMPORTANT: Change the option here for your model. For example for a NN I would change the line below to:
#root -l ./AGTMVAClassification.C\(\"ANN\"\) &>> ./outputs/modelTraining.log
#root -b -l -q ./AGTMVAClassification.C\(\"BDT\"\) &>> ./outputs/modelTraining.log
#root -b -l -q ./AGTMVARegression.C\(\"MLP\"\) &>> ./outputs/modelTraining.log

if [ $dl == 0 ]
then
    root -b -l -q ./AGTMVAClassification.C\(\"BDT\"\) &>> ./outputs/modelClassification.log
fi
if [ $dl == 1 ]
then
    root -b -l -q ./AGTMVARegression.C\(\"MLP\"\) &>> ./outputs/modelRegression.log
fi

# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
