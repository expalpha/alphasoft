#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(FOLDERNAME) $(label) $(source)
export EOS_MGM_URL=root://eosproject.cern.ch

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

function eosfail () {
   exit 20
}

test -d /eos/experiment/ALPHAg/midasdata_old/ || eosfail

export run=$3

export runNum=`awk -F "," 'NR==1 { print $1 }' mva_input.txt`
export FOLDERNAME=`awk -F "," 'NR==1 { print $2 }' mva_input.txt`
export label=`awk -F "," 'NR==1 { print $3 }' mva_input.txt`
export source=`awk -F "," 'NR==1 { print $4 }' mva_input.txt`

echo "Source = $source."
echo "FOLDERNAME = $FOLDERNAME."
echo "label = $label."


# Activate environment
cd $source
source agconfig.sh
cd $AGRELEASE

export AGOUTPUT=/eos/user/l/lgolino/Documents/ALPHA/root_output_files/2024/AGMVAWithTOF
mkdir -p $AGOUTPUT

# ------------------------------------------------------------------------------
# Run
echo "Run ${run} running ... "
agana.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub* -D$AGOUTPUT --mt -- --lite --bscnocali --anasettings ana/cern2024_0.json &>> MVA/${FOLDERNAME}/outputs/agana/M${run}_agana.log
echo "run ${run} complete."


# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
