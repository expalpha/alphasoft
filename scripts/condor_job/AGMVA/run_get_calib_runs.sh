#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(FOLDERNAME) $(label) $(source)
export EOS_MGM_URL=root://eosproject.cern.ch

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

#!/bin/bash

export runNum=`awk -F "," 'NR==1 { print $1 }' mva_input.txt`
export FOLDERNAME=`awk -F "," 'NR==1 { print $2 }' mva_input.txt`
export label=`awk -F "," 'NR==1 { print $3 }' mva_input.txt`
export source=`awk -F "," 'NR==1 { print $4 }' mva_input.txt`

#scp mva_input.txt $source
# Activate environment
cd $source
source agconfig.sh
cd $AGRELEASE

export AGOUTPUT=/eos/user/l/lgolino/Documents/ALPHA/root_output_files/2024/AGMVAWithTOF
mkdir -p $AGOUTPUT


macro_main_ListBSCRuns.exe --input ./scripts/condor_job/DAGs/AGMVADump_$FOLDERNAME/mva_input.txt &>> MVA/${FOLDERNAME}/outputs/M${run}_get_calib.log

mv values_for_agana.txt ./scripts/condor_job/DAGs/AGMVADump_$FOLDERNAME/

# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
