#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(FOLDERNAME) $(label) $(source)
export EOS_MGM_URL=root://eosproject.cern.ch

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

function eosfail () {
   exit 20
}

test -d /eos/experiment/ALPHAg/midasdata_old/ || eosfail

export run=$3
export FOLDERNAME=$4
export label=$5
export source=$6
export dl=$7

echo "Source = $source"
echo "FOLDERNAME = $FOLDERNAME"
echo "label = $label"

# Activate environment
cd $source
source agconfig.sh
cd $AGRELEASE




# ------------------------------------------------------------------------------
# Run
if [ $label = 'mixing' ]
then
    echo "Signal run ${run} running ... "
    #jagana.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub* -- --lite --Bfield 1 --anasettings ana/cern2021_2.json &>> MVA/${FOLDERNAME}/outputs/M${run}_jagana.log
    #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --mixing &>> MVA/${FOLDERNAME}/outputs/M${run}_eventlist.log
    #mv eventlist0${run}.list MVA/${FOLDERNAME}
    if [ $dl == 0 ]
    then
        agdumper.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub* -O/dev/null --mt -- --lite --Bfield 1 --anasettings ana/cern2024_0.json --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel mixing &>> MVA/${FOLDERNAME}/outputs/agdumper/M${run}_agdumper.log
    fi
    if [ $dl == 1 ]
    then
        agdldumper.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub* -O/dev/null --mt -- --lite --Bfield 1 --anasettings ana/cern2024_0.json --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel mixing &>> MVA/${FOLDERNAME}/outputs/agdumper/M${run}_agdldumper.log
    fi
    mv dumperoutputmixing${run}.root MVA/${FOLDERNAME}/dumperoutputs
    echo "run ${run} complete."
fi

if [ $label = 'cosmic' ]
then
    echo "Background run ${run} running ... "
    #jagana.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub* -- --lite --Bfield 1 --anasettings ana/cern2021_2.json &>> MVA/${FOLDERNAME}/outputs/M${run}_jagana.log
    #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --cosmic &>> MVA/${FOLDERNAME}/outputs/M${run}_eventlist.log
    #mv eventlist0${run}.list MVA/${FOLDERNAME}
    if [ $dl == 0 ]
    then
        agdumper.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub* -O/dev/null --mt -- --lite --Bfield 1 --anasettings ana/cern2024_0.json --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel cosmic --dumperreqcali &>> MVA/${FOLDERNAME}/outputs/agdumper/M${run}_agdumper.log
    fi
    if [ $dl == 1 ]
    then
        agdldumper.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub* -O/dev/null --mt -- --lite --Bfield 1 --anasettings ana/cern2024_0.json --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel cosmic --dumperreqcali &>> MVA/${FOLDERNAME}/outputs/agdumper/M${run}_agdldumper.log
    fi
    mv dumperoutputcosmic${run}.root MVA/${FOLDERNAME}/dumperoutputs
    echo "run ${run} complete."
fi

#IMPORTANT: Change this destination location if you are not using -O/dev/null option!!!
#rsync -av --remove-source-files root_output_files/output0${run}.root /eos/user/l/lgolino/Documents/ALPHA/root_output_files/MVA/



# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
