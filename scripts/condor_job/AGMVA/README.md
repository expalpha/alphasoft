## How to run me ##

# Normal Runs Batch Mode #

Either copy and paste this whole directory (condor_job) to a suitable location on your lxplus or run in here.  

1. Go to run_analysis.sh and change the repo_location, output_directory, and program to the correct options.
    i) repo_location is where you would like to run the analysis. Likely you want the repo you're in right now, in which case your repo_location would be `$AGRELEASE`. Note: Use the full directory link (the exact output of pwd or $AGRELEASE).
    ii) output_directory is where the root_output_files will be copied to. It is suggested to have a separate EOS location for this as your AFS might fill up pretty quickly (100Gb vs 1TB). Again use the full directory name.
    iii) program is choice of jagana.exe or agana.exe

2. Update the list of runs, starttimes, and endtimes in input.txt. It will run them all in parallel on different nodes. If you want to do the whole run set endtime to -1 for that run number. (See input_example.txt for an example where some are using time range and others are doing the whole run). Note: if endtime is -1, starttime is irrelevant.

3. In analysis.sub update the number of cores and memory per node (don't go crazy).

4. Update JobFlavour in analysis.sub:

espresso     = 20 minutes
microcentury = 1 hour
longlunch    = 2 hours
workday      = 8 hours
tomorrow     = 1 day
testmatch    = 3 days
nextweek     = 1 week

Note: Your job will auto end after this time, it doesn't mean the node will finish your job within this time. Although they will try to prioritise espresso jobs first. Don't send 20 agana's to an espresso job and expect it to finish. 
Excessive use will drain your "priority" and therefore how soon your jobs finish. To view your priority: run `condor_userprio | grep USERNAME` (higher is worse). It will decay with some half-life (~7 hours) back to its default value of 50,000.

5. Run `condor_submit analysis.sub`

6. Either go get a coffee, or go to sleep. Depending on your job it might take a while... 

You can use `condor_q` or `condor_q -nobatch` to look at your jobs.
You can use `condor_rm` to remove a bad job.
You can view files in outputs/log, outputs/error, and outputs/out relating to your job for updates. Unfortunately the "out" folder (and hence agana output) is only populated after your job is finished.
Test your jobs once or twice before submitting many. Don't waste all your priority and time on a job that wasn't configured correctly. Take the time to run 1 or 2 runs before you submit a big batch.

# DAG Job (for MVA dump) #

If you plan on running tha MVA dump it is recommended to read the MVA/README.md, and run the AGMVADump.sh found in the top directory. 

If you want to build a DAG: decide the order you want yur jobs to run and create a .dag file that defines which node (job) is the child of which others like so:

```
JOB  A  A.sub
JOB  B  B.sub
JOB  C  C.sub
JOB  D  D.sub

Parent A Child B C
Parent B C Child D
```

Then run condor_submit_dag yourfile.dag. You must have the referenced matching submit files configured correctly in the same directory when you submit the DAG. 



More infos:

# Info multiple arguments: http://chtc.cs.wisc.edu/multiple-jobs.shtml#args
# Info job flavours: https://batchdocs.web.cern.ch/local/submit.html#job-flavours
# Info on DAGs: https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html