!/bin/bash
# $(ClusterId) $(ProcId) $(seed) $(runnum)
export EOS_MGM_URL=root://eosproject.cern.ch

#BELOW ARE EXAMPLES - Please change to your folders.


##CHANGE THESE 3 LINES:
export repo_location=/afs/cern.ch/user/a/acapra/alphasim
export output_location=$repo_location/root_output_files
#export program=agana.exe
export program=spacepoint_dumper.exe

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

# Activate environment
cd $repo_location
source agconfig.sh
export MCDATA=/afs/cern.ch/user/a/acapra/CERNbox/agg4_allover
cd $AGRELEASE
ls -l $MCDATA/mc$4.mid

# ------------------------------------------------------------------------------
# Run

echo "Trying to run $program --mt -O$output_location/output$4.root $MCDATA/mc$4.mid -- --savefile $repo_location/spacepoints/spacepoints$4.h5 --savefile2 $repo_location/vertices/vertices$4.h5"
echo "Seed: $3"
$program --mt -O$output_location/output$4.root $MCDATA/mc$4.mid -- --savefile $repo_location/spacepoints/spacepoints$4.h5 --savefile2 $repo_location/vertices/vertices$4.h5

#rsync -av --remove-source-files $repo_location/spacepoints/vertices$4.h5 $MCDATA/vertices/
#rsync -av --remove-source-files $repo_location/spacepoints/spacepoints$4.h5 $MCDATA/spacepoints/
#rsync -av --remove-source-files $output_location/output$4.root $MCDATA/output/


# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
