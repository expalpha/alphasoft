#!/bin/bash
# $(ClusterId) $(ProcId) $(seed) $(runnum)
export EOS_MGM_URL=root://eosproject.cern.ch

#BELOW ARE EXAMPLES - Please change to your folders.


##CHANGE THESE 3 LINES:
export repo_location=/afs/cern.ch/user/a/acapra/alphasim
export output_location=/afs/cern.ch/work/a/acapra/mcdata
export program=agg4

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

# Activate environment
cd $repo_location
source agconfig.sh
export MCDATA=$output_location
cd $AGRELEASE

# ------------------------------------------------------------------------------
# Run

#echo "Trying to 1 run $program simulation/agg4/AllOver.mac $3 $4"
#$program simulation/agg4/AllOver.mac $3 $4
echo "Trying to 1 run $program simulation/agg4/WideRLongZ.mac $3 $4"
$program simulation/agg4/WideRLongZ.mac $3 $4


# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
