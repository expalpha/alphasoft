#!/bin/bash

INIT=90150
N=50
i=1

SEED=$((RANDOM*RANDOM))
echo "${SEED},${INIT}" > seed_list.txt

while [ $i -lt $N ]; do
    RUN=$((INIT+i))
    SEED=$((RANDOM*RANDOM))
    echo "${SEED},${RUN}" >> seed_list.txt
    i=$((i+1))
done

cat seed_list.txt
