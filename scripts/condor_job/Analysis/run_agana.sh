#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(FOLDERNAME) $(label) $(source)
export EOS_MGM_URL=root://eosproject.cern.ch

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

function eosfail () {
   exit 20
}

test -d /eos/experiment/ALPHAg/midasdata_old/ || eosfail

export run=$3

export repo_location=/afs/cern.ch/work/g/gwsmith/private/alphasoft

# Activate environment
cd $repo_location
source agconfig.sh
cd $AGRELEASE

export AGOUTPUT=/eos/user/g/gwsmith/Documents/Sep_2024_root_output/
mkdir -p $AGOUTPUT

# ------------------------------------------------------------------------------
# Run
echo "Run ${run} running ... "
agana.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub* -D$AGOUTPUT --mt -- --lite --bscnocali &>> scripts/condor_job/Analysis/outputs/M${run}_agana.log
echo "run ${run} complete."


# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
