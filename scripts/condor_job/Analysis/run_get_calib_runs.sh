#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(FOLDERNAME) $(label) $(source)
export EOS_MGM_URL=root://eosproject.cern.ch

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

#!/bin/bash

export repo_location=/afs/cern.ch/work/g/gwsmith/private/alphasoft

# Activate environment
cd $repo_location
source agconfig.sh
cd $AGRELEASE

export AGOUTPUT=/eos/user/g/gwsmith/Documents/Sep_2024_root_output/
mkdir -p $AGOUTPUT


macro_main_ListBSCRuns.exe --input ./scripts/condor_job/Analysis/analysis_input_gravity.txt &>> scripts/condor_job/Analysis/outputs/M${run}_get_calib.log

mv values_for_agana.txt ./scripts/condor_job/Analysis/

# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
