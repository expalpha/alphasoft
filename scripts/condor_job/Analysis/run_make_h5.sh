#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(starttime) $(endtime)
export EOS_MGM_URL=root://eosproject.cern.ch

#BELOW ARE EXAMPLES - Please change to your folders.


##CHANGE THESE 3 LINES:
export repo_location=/afs/cern.ch/work/g/gwsmith/private/alphasoft
export output_location=/afs/cern.ch/work/g/gwsmith/private/root_output_files
export program=spacepoint_dumper.exe

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

# Activate environment
cd $repo_location
source agconfig.sh
cd $AGRELEASE

# ------------------------------------------------------------------------------
# Run

if [ $5 -lt 0 ]
then
echo "Trying to run $program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location -- --lite --cut 6"
$program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location --mt -- --lite --cut 6
else
echo "Trying to run $program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location -- --lite --usetimerange $4 $5 --cut 6"
$program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location --mt -- --lite --usetimerange $4 $5 --cut 6
fi

rsync -av --remove-source-files vertices$3.h5 $output_location
rsync -av --remove-source-files spacepoints$3.h5 $output_location

# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
