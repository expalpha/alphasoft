#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum) $(starttime) $(endtime) $(calibration)
export EOS_MGM_URL=root://eosproject.cern.ch

#BELOW ARE EXAMPLES - Please change to your folders.


##CHANGE THESE 3 LINES:
export repo_location=/afs/cern.ch/work/g/gwsmith/private/alphasoft
export output_location=/afs/cern.ch/work/g/gwsmith/private/root_output_files
export program=agana.exe

# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

# Activate environment
cd $repo_location
source agconfig.sh
cd $AGRELEASE

# ------------------------------------------------------------------------------
# Run

if [ $5 -lt 0 ]
then

if [ $6 -lt 0 ]
then
echo "Trying to run $program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location -- --lite"
$program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location --mt -- --lite
else
echo "Trying to run $program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location -- --lite --bsccalifile \"/eos/user/l/lgolino/Documents/ALPHA/root_output_files/2024/AGMVAWithTOF/BarrelCalibration0${6}.root\" "
$program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location --mt -- --lite --bsccalifile "/eos/user/l/lgolino/Documents/ALPHA/root_output_files/2024/AGMVAWithTOF/BarrelCalibration0${6}.root"
fi

else
if [ $6 -lt 0 ]
then
echo "Trying to run $program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location -- --lite --usetimerange $4 $5"
$program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location --mt -- --lite --usetimerange $4 $5
else
echo "Trying to run $program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location -- --lite --usetimerange $4 $5 --bsccalifile \"/eos/user/l/lgolino/Documents/ALPHA/root_output_files/2024/AGMVAWithTOF/BarrelCalibration0${6}.root\""
$program /eos/experiment/ALPHAg/midasdata_old/run0$3sub0*.lz4 -D$output_location --mt -- --lite --usetimerange $4 $5 --bsccalifile "/eos/user/l/lgolino/Documents/ALPHA/root_output_files/2024/AGMVAWithTOF/BarrelCalibration0${6}.root"
fi

fi

# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
