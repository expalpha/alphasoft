#!/bin/bash
# $(ClusterId) $(ProcId) $(runnum)
export EOS_MGM_URL=root://eosproject.cern.ch




# ------------------------------------------------------------------------------
echo "Starting job $1.$2 on $(date)..."

function eosfail () {
   exit 20
}

test -d /eos/experiment/ALPHAg/midasdata_old/ || eosfail

export run=$3
export repo_location=/afs/cern.ch/work/g/gwsmith/private/alphasoft

# Activate environment
cd $repo_location
source agconfig.sh
cd $AGRELEASE

export AGOUTPUT=/eos/user/g/gwsmith/Documents/Sep_2024_root_output/
mkdir -p $AGOUTPUT

# ------------------------------------------------------------------------------
# Run

echo " .L ana/macros/bsc/CalibrateBarrel.C
CalibrateBarrel(${run},20,10800)"

echo " .L ana/macros/bsc/CalibrateBarrel.C
CalibrateBarrel(${run},20,10800)" | root -l -b &>> scripts/condor_job/Analysis/outputs/M${run}_bv_calib.log



# Move *.root to EOS (advised)
#rm -r ${output_directory}



# ------------------------------------------------------------------------------
echo "Finished job $1.$2 on $(date)..."
