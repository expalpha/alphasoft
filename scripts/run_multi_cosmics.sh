#!/bin/bash

#runs=("11" "12" "13" "14" "15" "16" "17" "18" "19" "20")

#runs=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20")

runs=("00" "01" "02" "03" "04" "05" "06" "07" "08" "09")
for i in "${runs[@]}"; do
 echo "Running MC gen " $i
 agg4 simulation/agg4/ecomug_cosmics.mac > /dev/null
 mv -v simulation/outAgTPC_det_AWtime16ns_PADtime16ns_B1.00T_Q30_cosmics.root simulation/cosmics_ecomug_hsphere_B0T_50000_run${i}.root
done 

