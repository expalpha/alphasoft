
message(STATUS "Logging build time git info")
message(STATUS ${SOURCE_PATH})
# Find Git
find_package(Git REQUIRED)

# this command must generate: ${CMAKE_CURRENT_BINARY_DIR}/header.h
execute_process(
    COMMAND ${GIT_EXECUTABLE} log -1 --format=%at 
    #WORKING_DIRECTORY ${BUILD_SOURCE_DIR} 
    OUTPUT_VARIABLE BUILD_TIME_GIT_DATE
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
execute_process(
    COMMAND ${GIT_EXECUTABLE}  log -1 --format=%h 
    #WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} 
    OUTPUT_VARIABLE BUILD_TIME_GIT_REVISION
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
execute_process(
    COMMAND ${GIT_EXECUTABLE}  log -1 --format=%H 
    #WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} 
    OUTPUT_VARIABLE BUILD_TIME_GIT_REVISION_FULL
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
execute_process(
    COMMAND ${GIT_EXECUTABLE}  rev-parse --abbrev-ref HEAD 
    #WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} 
    OUTPUT_VARIABLE BUILD_TIME_GIT_BRANCH
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )

execute_process(
    COMMAND ${GIT_EXECUTABLE}  diff --shortstat 
    #WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} 
    OUTPUT_VARIABLE BUILD_TIME_GIT_DIFF_SHORT_STAT
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )


#PACKAGE_GIT_HISTORY_DATES=`git log -${TRACK_HISTORY_DEPTH} --format="      %at,"`
#PACKAGE_GIT_HISTORY_HASHS=`git log -${TRACK_HISTORY_DEPTH} --format="%h\\\"`

execute_process(COMMAND ${GIT_EXECUTABLE}  log --format="%at" 
                WORKING_DIRECTORY  ${CMAKE_SOURCE_DIR} 
                OUTPUT_VARIABLE PACKAGE_GIT_HISTORY_DATES
                #ECHO_OUTPUT_VARIABLE
                ERROR_QUIET
                OUTPUT_STRIP_TRAILING_WHITESPACE)
string(REPLACE "\n" "," PACKAGE_GIT_HISTORY_DATES ${PACKAGE_GIT_HISTORY_DATES})
separate_arguments(PACKAGE_GIT_HISTORY_DATES UNIX_COMMAND ${PACKAGE_GIT_HISTORY_DATES})

execute_process(COMMAND ${GIT_EXECUTABLE}  log --format="%h"
                WORKING_DIRECTORY  ${CMAKE_SOURCE_DIR} 
                OUTPUT_VARIABLE PACKAGE_GIT_HISTORY_HASHS
                #ECHO_OUTPUT_VARIABLE
                ERROR_QUIET
                OUTPUT_STRIP_TRAILING_WHITESPACE)
string(REPLACE "\n" "," PACKAGE_GIT_HISTORY_HASHS ${PACKAGE_GIT_HISTORY_HASHS})

string(TIMESTAMP BUILD_TIME_TIMESTAMP "%s" )

configure_file(${SOURCE_PATH}/GitInfo.h.in GitInfo.h @ONLY)
configure_file(${SOURCE_PATH}/GitInfo.build_time.h.in GitInfo.build_time.h @ONLY)
configure_file(${SOURCE_PATH}/GitInfo.build_time.cxx.in GitInfo.build_time.cxx @ONLY)

configure_file(${SOURCE_PATH}/GitInfo.commit_timestamps.cxx.in GitInfo.commit_timestamps.cxx @ONLY)
configure_file(${SOURCE_PATH}/GitInfo.commit_hashes.cxx.in GitInfo.commit_hashes.cxx @ONLY)

#TRACK_HISTORY_DEPTH=20000
#PACKAGE_GIT_HISTORY_DATES=`git log -${TRACK_HISTORY_DEPTH} --format="      %at,"`
#PACKAGE_GIT_HISTORY_HASHS=`git log -${TRACK_HISTORY_DEPTH} --format="%h\\\"`
