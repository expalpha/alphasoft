# this command must generate: ${CMAKE_CURRENT_BINARY_DIR}/header.h
execute_process(
    COMMAND ${GIT_EXECUTABLE}  log -1 --format=%at 
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} 
    OUTPUT_VARIABLE CMAKE_TIME_GIT_DATE
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
execute_process(
    COMMAND ${GIT_EXECUTABLE}  log -1 --format=%h 
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} 
    OUTPUT_VARIABLE CMAKE_TIME_GIT_REVISION
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
execute_process(
    COMMAND ${GIT_EXECUTABLE}  log -1 --format=%H 
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} 
    OUTPUT_VARIABLE CMAKE_TIME_GIT_REVISION_FULL
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
execute_process(
    COMMAND ${GIT_EXECUTABLE}  rev-parse --abbrev-ref HEAD 
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} 
    OUTPUT_VARIABLE CMAKE_TIME_GIT_BRANCH
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
execute_process(
    COMMAND ${GIT_EXECUTABLE}  diff --shortstat 
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} 
    OUTPUT_VARIABLE CMAKE_TIME_GIT_DIFF_SHORT_STAT
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )

if (NOT CMAKE_TIME_GIT_DIFF_SHORT_STAT)
   set(CMAKE_TIME_GIT_DIFF_SHORT_STAT "no diff")
endif()
string(TIMESTAMP CMAKE_TIME_TIMESTAMP "%s" )
configure_file(GitInfo.cmake_time.h.in GitInfo.cmake_time.h @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/GitInfo.cmake_time.h DESTINATION include)

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_INCLUDE_CURRENT_DIR})
add_library(gitinfo SHARED GitInfo.build_time.cxx GitInfo.commit_timestamps.cxx GitInfo.commit_hashes.cxx)
target_include_directories(gitinfo PUBLIC ${CMAKE_CURRENT_BINARY_DIR})
install(TARGETS gitinfo DESTINATION lib)
add_custom_command(OUTPUT 
   GitInfo.build_time.cxx
   GitInfo.build_time.h
   GitInfo.h
   GitInfo.commit_timestamps.cxx
   GitInfo.commit_hashes.cxx
   ... # force this to be triggered every rebuild
   COMMAND ${CMAKE_COMMAND} -DSOURCE_PATH=${CMAKE_CURRENT_SOURCE_DIR} -P ${CMAKE_CURRENT_SOURCE_DIR}/build_time_gitinfo.cmake
   #WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/GitInfo.h ${CMAKE_CURRENT_BINARY_DIR}/GitInfo.build_time.h DESTINATION include)


#setup.sh/py/C files for SWAN notebooks
if(PRE_CONFIG)
   message("PRE_CONFIG: ${PRE_CONFIG} being added to ${CMAKE_INSTALL_PREFIX}/setup.{sh,py,C}")
endif()
set(AGRELEASE $ENV{AGRELEASE})
set(BIN_PATH ${CMAKE_INSTALL_PREFIX})
set(LIB_PATH ${CMAKE_INSTALL_PREFIX}/lib)
set(INC_PATH ${CMAKE_INSTALL_PREFIX}/include)
configure_file(setup.sh.in ${CMAKE_CURRENT_BINARY_DIR}/setup.sh @ONLY)
configure_file(setup.py.in ${CMAKE_CURRENT_BINARY_DIR}/setup.py @ONLY)
configure_file(setup.C.in ${CMAKE_CURRENT_BINARY_DIR}/setup.C @ONLY)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/setup.sh ${CMAKE_CURRENT_BINARY_DIR}/setup.py ${CMAKE_CURRENT_BINARY_DIR}/setup.C DESTINATION .)

