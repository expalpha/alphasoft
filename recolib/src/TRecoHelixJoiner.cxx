#include "TRecoHelixJoiner.hh"

int TRecoHelixJoiner::JoinHelix(std::vector<TFitHelix> &HelixArray, std::vector<HelixAndIDs> &fCombinedHelixArray, const int thread_no, const int total_threads)
{


   int n = 0;

   if (HelixArray.size()==0) return n;
   const float slice_size = HelixArray.size() / (float)total_threads;
   const int eff_thread_no = ((thread_no -1 + HelixArray.at(0).GetNumberOfPoints())%total_threads)+1;
   // If we use thread_number, then the first and second threads will do most of the work.
   // Use the number of points in the first helix as a pseudorandom number to split the load more equally.
   const unsigned int start = floor(slice_size*(eff_thread_no - 1));
   unsigned int stop = floor( slice_size * eff_thread_no );
   //I am the last thread
   if (eff_thread_no == total_threads)
      stop = HelixArray.size();

   if (thread_no == 1) {
      fCombinedHelixArray.clear();
      fCombinedHelixArray.reserve((HelixArray.size()*HelixArray.size()-HelixArray.size())/2);
   }


   // Step 1: For events with few tracks, lets combine each combination of two tracks and try to fit them as one track. i.e. cosmic finder
   if (HelixArray.size()>=2 and (int)HelixArray.size()<=fMaxTrackJoin) {

      for (unsigned int it = start; it < stop; ++it)
      {

         if (it==HelixArray.size()-1) continue;

         for (unsigned int jt=it+1; jt<HelixArray.size(); jt++) {

            TTrack t;
            for (const TSpacePoint& p: *HelixArray[it].GetPointsArray()) t.AddPoint(p);
            for (const TSpacePoint& p: *HelixArray[jt].GetPointsArray()) t.AddPoint(p);

            TFitHelix helix(t);
            helix.SetChi2ZCut(fHelChi2ZCut);
            helix.SetChi2RCut(fHelChi2RCut);
            helix.SetChi2RMin(fHelChi2RMin);
            helix.SetChi2ZMin(fHelChi2ZMin);
            helix.SetDCut(fHelDcut);
            helix.SetcCut(fHelCcut);
            helix.SetPointsCut(fPointsCut);
            helix.SetPointMaxRad(fPointMaxRad);
            helix.SetRFiterations(fHelRFiterations);
            helix.SetZFiterations(fHelZFiterations);
            helix.SetHelPreferStraight(fHelPreferStraight);
            helix.SetHelSmallCurvature(fHelSmallCurvature);
            helix.FitM2();
            if (helix.GetStatR() > 0 && helix.GetStatZ() > 0) helix.CalculateResiduals();

            helix.IsGood();
            double pt = helix.Momentum();
            helix.SetTrackNum(it+1);
            if (fTrace) {
         helix.Print();
         std::cout << "Reco::FitHelix()  hel # " << n << " p_T = " << pt
            << " MeV/c in B = " << helix.GetMagneticField() << " T" << std::endl;
            }

            // Calculates chi2 improvement for each half-track separately
            RadFuncFCN rf1(&HelixArray[it]);
            RadFuncFCN rf2(&HelixArray[jt]);
            const double chi2r_f1 = rf1({helix.GetRc(),helix.GetPhi0(),helix.GetD()});
            const double chi2r_f2 = rf2({helix.GetRc(),helix.GetPhi0(),helix.GetD()});
            const double chi2r_old1 = HelixArray[it].GetRchi2();
            const double chi2r_old2 = HelixArray[jt].GetRchi2();
            ZedFuncFCN zf1(&HelixArray[it]);
            ZedFuncFCN zf2(&HelixArray[jt]);
            const double chi2z_f1 = zf1.test({helix.GetLambda(),helix.GetZ0()},&helix);
            const double chi2z_f2 = zf2.test({helix.GetLambda(),helix.GetZ0()},&helix);
            const double chi2z_old1 = HelixArray[it].GetZchi2();
            const double chi2z_old2 = HelixArray[jt].GetZchi2();

            bool Rfit1_improved = (chi2r_f1 < fJoinRImprovement*(chi2r_old1));
            bool Zfit1_improved = (chi2z_f1 < fJoinZImprovement*(chi2z_old1));
            bool Rfit2_improved = (chi2r_f2 < fJoinRImprovement*(chi2r_old2));
            bool Zfit2_improved = (chi2z_f2 < fJoinZImprovement*(chi2z_old2));

            if (Rfit1_improved and Zfit1_improved and Rfit2_improved and Zfit2_improved) {
               HelixAndIDs helid;
               helid.hel = helix;
               helid.ids = std::pair<int,int>(it,jt);
               fCombinedHelixArray.push_back(helid);
            }

         }

      }

      if (thread_no==total_threads) {
         std::sort(fCombinedHelixArray.begin(), fCombinedHelixArray.end(), [](const HelixAndIDs &a, const HelixAndIDs &b) { return a.hel.GetNumberOfPoints() > b.hel.GetNumberOfPoints(); });

         std::vector<int> used_track_nums;
         unsigned int i=0;
         while (i<fCombinedHelixArray.size()) {
            if (std::find(used_track_nums.begin(), used_track_nums.end(), fCombinedHelixArray[i].ids.first) != used_track_nums.end()
                  or std::find(used_track_nums.begin(), used_track_nums.end(), fCombinedHelixArray[i].ids.second) != used_track_nums.end())
            { // At least one constituent track already combined, discard this combination
               fCombinedHelixArray.erase(fCombinedHelixArray.begin() + i);
            }
            else {
               used_track_nums.push_back(fCombinedHelixArray[i].ids.first);
               used_track_nums.push_back(fCombinedHelixArray[i].ids.second);
               i++;
            }
         }

         // Now add all joined and unjoined helices
         std::vector<TFitHelix> final_hels;
         for (const HelixAndIDs& helix: fCombinedHelixArray) {
            final_hels.push_back(helix.hel);
            n++;
         }
         for (unsigned int i=0; i<HelixArray.size(); i++) {
            const TFitHelix& hel = HelixArray[i];
            if (std::find(used_track_nums.begin(), used_track_nums.end(), i) == used_track_nums.end()) {
               final_hels.push_back(hel);
            }
         }
         
         // Replace the helix array, clear the temporary array
         HelixArray = final_hels;
         fCombinedHelixArray.clear();

      }

   }

   // Step 2: Lets look for small Rc tracks, which have two or more other small Rc tracks in the same vertical column. i.e. cyclotron motion electron finder
   if (HelixArray.size() > 5 and eff_thread_no==total_threads) {
      std::vector<std::vector<unsigned int>> stack_ids;
      for (unsigned int i=0; i<HelixArray.size(); i++) {
         const TFitHelix& hel = HelixArray[i];
         if (TMath::Abs(hel.GetRc()) > fHelSmallCurvature) continue;
         const double u0 = TMath::Cos(hel.GetPhi0());
         const double v0 = TMath::Sin(hel.GetPhi0());
         const double Rc = hel.GetRc();
         const double D = hel.GetD();
         const double xc = (Rc+D)*-1*v0; // Centre of helix
         const double yc = (Rc+D)*u0;
         int count_stacked_small = 1;
         std::vector<unsigned int> stacked_ids = {i};
         for (unsigned int j=0; j<HelixArray.size(); j++) {
            if (i==j) continue;
            const TFitHelix& helj = HelixArray[j];
            int c_points_stacked = 0;
            for (const TSpacePoint& p: *helj.GetPointsArray()) {
               if (TMath::Sqrt((p.GetX()-xc)*(p.GetX()-xc) + (p.GetY()-yc)*(p.GetY()-yc)) < 1.5*TMath::Abs(Rc)+30) c_points_stacked++;
            }
            const double f_stacked = 1.0*c_points_stacked/helj.GetNumberOfPoints();
            if (f_stacked>0.9) {
               stacked_ids.push_back(j);
               if (TMath::Abs(helj.GetRc()) < fHelSmallCurvature) count_stacked_small++;
            }
            else {
            }
         }
         if ((int)stacked_ids.size() >= fMegaJoinMinTracks and count_stacked_small >= fMegaJoinMinSmallTracks) {
            stack_ids.push_back(stacked_ids);
         }
      }

      std::sort(stack_ids.begin(), stack_ids.end(), [](const std::vector<unsigned int> &a, const std::vector<unsigned int> &b) { return a.size() > b.size(); }); // Biggest stacks first
      std::vector<int> used_ids;
      std::vector<TFitHelix> hels;
      for (const std::vector<unsigned int>& stack: stack_ids) {
         bool isgood = true;
         for (unsigned int id: stack) {
            if (std::find(used_ids.begin(), used_ids.end(), id) != used_ids.end()) {
               isgood = false;
               break;
            }
         }
         if (isgood) {
            // Time to make a mega helix
            if (fTrace) {
               printf("Making a mega helix with tracks:");
               for (int id: stack) printf("%d, ",id);
               printf("\n");
            }
            for (int id: stack) used_ids.push_back(id);

            TTrack t;
            for (unsigned int id: stack) {
               for (const TSpacePoint& p: *HelixArray[id].GetPointsArray()) t.AddPoint(p);
            }


            TFitHelix helix(t);
            helix.SetChi2ZCut(fHelChi2ZCut);
            helix.SetChi2RCut(fHelChi2RCut);
            helix.SetChi2RMin(fHelChi2RMin);
            helix.SetChi2ZMin(fHelChi2ZMin);
            helix.SetDCut(fHelDcut);
            helix.SetcCut(fHelCcut);
            helix.SetPointsCut(fPointsCut);
            helix.SetPointMaxRad(fPointMaxRad);
            helix.SetRFiterations(fHelRFiterations);
            helix.SetZFiterations(fHelZFiterations);
            // Curvy initialization only, do not fit. These mega helices don't fit well.
            // The initialization should have small Rc, and so they will be excluded from fitting.
            helix.FitM2(false,false,false,true,true);
            if (helix.GetStatR() > 0 && helix.GetStatZ() > 0) helix.CalculateResiduals();

            helix.IsGood();
            helix.SetTrackNum(stack[0]+1);
            if (fTrace) {
         helix.Print();
            }
            ++n;
            hels.push_back(helix);
         }
      }

      // Now add all unjoined helices
      for (unsigned int i=0; i<HelixArray.size(); i++) {
         const TFitHelix& hel = HelixArray[i];
         if (std::find(used_ids.begin(), used_ids.end(), i) == used_ids.end()) {
            hels.push_back(hel);
         }
      }
      
      // Replace the helix array, clear the temporary array
      HelixArray = hels;

   }


   return n;
}
