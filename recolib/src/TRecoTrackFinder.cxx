#include "TRecoTrackFinder.hh"

TRecoTrackFinder::TRecoTrackFinder(AnaSettings* ana_settings, bool trace ): fTrace(trace)
{
   fNspacepointsCut = ana_settings->GetInt("RecoModule","NpointsCut");
   fPointMaxRad = ana_settings->GetInt("RecoModule","PointMaxRad");
   fPointsDistCut = ana_settings->GetDouble("RecoModule","PointsDistCut");
   fMaxIncreseAdapt = ana_settings->GetDouble("RecoModule","MaxIncreseAdapt");
   fSeedRadCut = ana_settings->GetDouble("RecoModule","SeedRadCut");
   fAdaptiveness = ana_settings->GetDouble("RecoModule","Adaptiveness");
   fTrackIterations = ana_settings->GetInt("RecoModule","TrackIterations");
   fTrackLengthCut = ana_settings->GetDouble("RecoModule","TrackLengthCut");
   fFinderChoice = ana_settings->GetInt("RecoModule","TrackFinderMethod");
   fTouchDistanceXY = ana_settings->GetDouble("RecoModule","TouchDistanceXY");
   fTouchDistanceZ = ana_settings->GetDouble("RecoModule","TouchDistanceZ");
   fLastPointRadCut = ana_settings->GetDouble("RecoModule","LastPointRadCut");

}

    


int TRecoTrackFinder::FindTracks( std::vector<TSpacePoint>& SortedPoints , std::vector<track_t>& TrackVector)
{

   switch(finderChoice(fFinderChoice))
      {
      case exhaustive:
         pattrec = new ExhaustiveFinder( &SortedPoints, fTouchDistanceXY, fTouchDistanceZ);
         break;
      case iterative:
         pattrec = new IterativeFinder( &SortedPoints, fMaxIncreseAdapt, fLastPointRadCut, fAdaptiveness, fTrackIterations, fTrackLengthCut);
         break;
      case adaptive:
         pattrec = new AdaptiveFinder( &SortedPoints, fMaxIncreseAdapt, fLastPointRadCut);
         break;
      case base:
         pattrec = new TracksFinder(  &SortedPoints ); 
         break;
      default:
         pattrec = new IterativeFinder(  &SortedPoints , fMaxIncreseAdapt,fLastPointRadCut, fAdaptiveness, fTrackIterations, fTrackLengthCut);
         break;
      }
   
   if( fTrace ) 
      pattrec->SetDebug();

   pattrec->SetPointsDistCut(fPointsDistCut);
   pattrec->SetPointMaxRadCut(fPointMaxRad);
   pattrec->SetNpointsCut(fNspacepointsCut);
   pattrec->SetSeedRadCut(fSeedRadCut);

   int stat = pattrec->RecTracks(TrackVector);
   if( fTrace ) 
      std::cout<<"Reco::FindTracks status: "<<stat<<std::endl;
   int tk,npc,rc,lc;
   pattrec->GetReasons(tk,npc,rc,lc);
   track_not_advancing += tk;
   points_cut += npc;
   rad_cut += rc;
   length_cut += lc;

   delete pattrec;
   return stat;
}

int TRecoTrackFinder::FindTrivialTrack(const std::vector<TSpacePoint> SortedPoints , std::vector<track_t>& TrackVector)
{
   int Npoints = SortedPoints.size(); 
   
   //Initialise the one track
   track_t atrack;

   //Push back every point to this track
   for(int i=0; i<Npoints; ++i)
   {
      atrack.push_back(i);
   }

   //Push back trivial track to the TrackVector
   TrackVector.push_back( atrack );

   if( Npoints<=0 )
   {
      return -1;
   }
   else
      return 1;
}


int TRecoTrackFinder::FindUsedTrivialTrack(const std::vector<track_t>& TrackVectorFromReco , std::vector<track_t>& myUsedTrackVector)
{
   //Initialise the one track
   track_t atrack;

   //Push back everything from previous track vector.
   int Npoints = 0;
   for(track_t track: TrackVectorFromReco)
   {
      for(int point: track)
      {
         atrack.push_back(point);
         Npoints++;
      }
   }

   //Push back trivial track to the TrackVector
   myUsedTrackVector.push_back( atrack );

   if( Npoints<=0 )
   {
      return -1;
   }
   else
      return 1;
}




void TRecoTrackFinder::PrintPattRec()
{
   std::cout<<"Reco:: pattrec failed\ttrack not advanving: "<<track_not_advancing
            <<"\tpoints cut: "<<points_cut
            <<"\tlength cut: "<<length_cut
            <<"\tradius cut: "<<rad_cut<<std::endl;
}