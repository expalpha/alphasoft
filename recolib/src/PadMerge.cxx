#include "PadMerge.hh"

#include "TH1D.h"
#include "TSpectrum.h"
#include "TF1.h"
#include "TCanvas.h"

#include <Math/MinimizerOptions.h>

#include "fitSignals.hh"

#include <chrono>
#include <thread>       // std::thread
#include <functional>   // std::ref

//Null static pointers to histograms (histograms static so can be shared 
//between PadMerge instances in multithreaded mode)
//TH1D* PadMerge::hsigCoarse=NULL;
//TH1D* PadMerge::hsig=NULL;
TH1D* PadMerge::hcognpeaks=NULL;
// TH2D* PadMerge::hcognpeaksrms=NULL;
// TH2D* PadMerge::hcognpeakswidth=NULL;
TH1D* PadMerge::hNblobSigs=NULL;
TH1D* PadMerge::hcogsigma=NULL;
TH1D* PadMerge::hcogerr=NULL;
TH2D* PadMerge::hcogpadssigma=NULL;
TH2D* PadMerge::hcogpadsamp=NULL;
TH2D* PadMerge::hcogpadsint=NULL;
TH2D* PadMerge::hcogpadsampamp=NULL;
TH1D* PadMerge::hcogpadsdmaxmean=NULL;
TH1D* PadMerge::hcogpadsdfitpos=NULL;
TH2D* PadMerge::hcogpadsblob=NULL;
TH1D* PadMerge::htimecog=NULL;
TH1D* PadMerge::htimeblobs=NULL;
TH1D* PadMerge::htimefit=NULL;
TH1D* PadMerge::hNpadSigs=NULL;
TGraph* PadMerge::hgNpadSigs=NULL;

PadMerge::PadMerge(const AnaSettings* ana_set, bool trace):
   fTrace(trace),
   fDebug(false),
   ana_settings(ana_set),
   fCoincTime(     ana_settings->GetDouble("MatchModule","coincTime")),
   maxPadGroups(   ana_settings->GetInt("PadMergeModule","maxPadGroups")),
   padsNmin(       ana_settings->GetInt("PadMergeModule","padsNmin")),
   padSigma(       ana_settings->GetDouble("PadMergeModule","padSigma")),
   padSigmaD(      ana_settings->GetDouble("PadMergeModule","padSigmaD")),
   padFitErrThres( ana_settings->GetDouble("PadMergeModule","padFitErrThres")),

   // FIXME: If I put these 3 here in the initializers, the constructor fails, never reaches the output in line 73
   //  padCogDoFit(ana_settings->GetBool("PadMergeModule", "padCogDoFit")),
   //  padCogUseMean(ana_settings->GetBool("PadMergeModule", "padCogUseMean")),
   //  padCogFixSigma(ana_settings->GetBool("PadMergeModule", "padCogFixSigma")),

   // FIXME: Pretty sure these three settings are unused
   // use_mean_on_spectrum(ana_settings->GetBool("PadMergeModule","use_mean_on_spectrum")),
   // spectrum_mean_multiplyer(ana_settings->GetDouble("PadMergeModule","spectrum_mean_multiplyer")),
   // spectrum_cut(   ana_settings->GetDouble("PadMergeModule","spectrum_cut")),
   spectrum_width_min(ana_settings->GetDouble("PadMergeModule","spectrum_width_min")),
   grassCut(       ana_settings->GetDouble("PadMergeModule","grassCut")),
   goodDist(       ana_settings->GetDouble("PadMergeModule","goodDist")),
   fNpadsCut(ana_settings->GetInt("PadMergeModule","NpadsCut"))
{
   //std::cout<<"PadMerge::Loading AnaSettings from json"<<std::endl;

   // FIXME: here instead of in line 49 because that somehow crashes the constructor...
   padCogDoFit = ana_settings->GetBool("PadMergeModule", "padCogDoFit");
   padCogUseMean = ana_settings->GetBool("PadMergeModule", "padCogUseMean");
   padCogFixSigma = ana_settings->GetBool("PadMergeModule", "padCogFixSigma");
   // std::cout << "XXXXXXXX Booleans from anasettings:" << std::endl;
   // std::cout << "XXXXXXXX padCogDoFit = " << int(padCogDoFit) << std::endl;
   // std::cout << "XXXXXXXX padCogUseMean = " << int(padCogUseMean) << std::endl;
   // std::cout << "XXXXXXXX padCogFixSigma = " << int(padCogFixSigma) << std::endl;

   TString CentreOfGravity=ana_settings->GetString("PadMergeModule","CentreOfGravityMethod");
   if ( CentreOfGravity.EqualTo("CentreOfGravity") )
      { 
         std::cout<<"PadMerge::PadMerge this CoG is NOT THREAD SAFE"<<std::endl;
         CentreOfGravityFunction=1; 
      }
   if ( CentreOfGravity.EqualTo("CentreOfGravity_blobs") ) CentreOfGravityFunction=2;

   if ( CentreOfGravityFunction <= 0 )
      {
         std::cout<<"PadMerge::PadMerge(json) No valid CentreOfGravityMethod function in json"<<std::endl;
         exit(1);
      }
   else if(fTrace)
      std::cout<<"PadMerge::PadMerge(json) Using CentreOfGravity case "<<CentreOfGravityFunction<<": "<<CentreOfGravity<<std::endl;
}

PadMerge::~PadMerge()
{
   if(hgNpadSigs) hgNpadSigs->Write("gNpadSigs");
   std::cout << "Blobs with edge: " << edge << ", blobs without edge: " << noedge << std::endl;
}

void PadMerge::Init()
{
   assert(CentreOfGravityFunction>=0); //CentreOfGravityFunction not set!
   if(fDebug) std::cout<<"PadMerge::Init!"<<std::endl;
   ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
}

void PadMerge::Setup(TFile* OutputFile)
{
   if( diagnostic )
      {
         if( OutputFile )
            { 
               OutputFile->cd(); // select correct ROOT directory
               int error_level_save = gErrorIgnoreLevel;
               gErrorIgnoreLevel = kFatal;
               if( !gDirectory->cd("padmatch") )
                  gDirectory->mkdir("padmatch")->cd();
               else 
                  gDirectory->cd("padmatch");
               gErrorIgnoreLevel = error_level_save;
            }
         else
            gFile->cd();

         if (!hcognpeaks)
            hcognpeaks = new TH1D("hcognpeaks","cCombPads CoG - Number of Avals",int(maxPadGroups+1.),
                                  0.,maxPadGroups+1.);
         // if (!hcognpeaksrms)
         //   hcognpeaksrms = new TH2D("hcognpeaksrms","CombPads CoG - Number of Avals vs RMS", 500, 0., 50,int(maxPadGroups+1.),
         // 			       0.,maxPadGroups+1.);
         // if (!hcognpeakswidth)
         //   hcognpeakswidth = new TH2D("hcognpeakswidth","CombPads CoG - Number of Avals vs width", 20, 0., 20,int(maxPadGroups+1.),
         // 				 0.,maxPadGroups+1.);
         if (!hNblobSigs)
            hNblobSigs = new TH1D("hNblobSigs","Number of pad signals per blob",40,0.,40.);
         if (!hcogsigma)
            hcogsigma = new TH1D("hcogsigma","CombPads CoG - Sigma Charge Induced;[mm]",700,0.,70.);
         if (!hcogerr)
            hcogerr = new TH1D("hcogerr","CombPads CoG - Error on Mean;[mm]",2000,0.,20.);

         if (!hcogpadssigma)
            hcogpadssigma = new TH2D("hcogpadssigma","CombPads CoG - Pad Index Vs. Sigma Charge Induced;pad index;#sigma [mm]",32*576,0.,32.*576.,1000,0.,140.);
         if (!hcogpadsamp)
            hcogpadsamp = new TH2D("hcogpadsamp","CombPads CoG - Pad Index Vs. Amplitude Charge Induced;pad index;Amplitude [a.u.]",32*576,0.,32.*576.,1000,0.,4000.);
         if (!hcogpadsint)
            hcogpadsint = new TH2D("hcogpadsint","CombPads CoG - Pad Index Vs. Integral Charge Induced;pad index;Tot. Charge [a.u.]",32*576,0.,32.*576.,1000,0.,10000.);
         if (!hcogpadsampamp)
            hcogpadsampamp = new TH2D("hcogpadsampamp","CombPads CoG - Gaussian fit amplitude Vs. Max. Signal height;max. height;Gauss Amplitude",1000,0.,4000.,1000,0.,4000.);
         if (!hcogpadsdmaxmean)
            hcogpadsdmaxmean = new TH1D("hcogpadsdmaxmean","Difference between blobfinding max and mean;dz [mm]",200,-10.,10.);
         if (!hcogpadsdfitpos)
            hcogpadsdfitpos = new TH1D("hcogpadsdfitpos","Difference between gaussian fit and blobfinding;dz [mm]",200,-100.,100.);
         //  hsig = new TH1D("hpadRowSig","sigma of pad combination fit",1000,0,50);
         if (!hcogpadsblob)
            hcogpadsblob = new TH2D("hcogpadsblob","Relative pad signal heights;height;pad",11,-5.5,5.5,110,0.,1.1);
         if (!htimecog)
            htimecog = new TH1D("htimecog","Timing of Cog;Time [us]",1000,0.,10000.);
         if (!htimeblobs)
            htimeblobs = new TH1D("htimeblobs","Timing of Blob Finding;Time [us]",1000,0.,10000.);
         if (!htimefit)
            htimefit = new TH1D("htimefit","Timing of Fit;Time [us]",1000,0.,10000.);
         if (!hNpadSigs)
            hNpadSigs = new TH1D("hNpadSigs","Number of pad signals per event",1000,0.,100000.);
         if (!hgNpadSigs)
            hgNpadSigs = new TGraph;
      }
}

std::pair<std::set<short>,std::vector< std::vector<ALPHAg::TPadSignal> >> PadMerge::PartitionBySector(
   const std::vector<ALPHAg::TPadSignal>& padsignals) const
{
   std::vector< std::vector<ALPHAg::TPadSignal> > pad_bysec;
   pad_bysec.resize(32);

   std::set<short> secs;

   for (const auto& pad: padsignals)
      {
         //ipd->print();
         secs.insert( pad.sec );
         pad_bysec.at(pad.sec).emplace_back(pad);
      }
   return {secs,pad_bysec};
}

std::vector< std::vector<ALPHAg::TPadSignal> > PadMerge::PartitionByTime(
   const std::vector<ALPHAg::TPadSignal>& sig ) const
{
   if( fDebug ) std::cout<<"PadMerge::PartitionByTime  "<<sig.size()<<std::endl;
   const std::multiset<ALPHAg::TPadSignal, ALPHAg::signal::timeorder> sig_bytime(sig.begin(),
                                                                                 sig.end());
   double temp=-999999.;
   std::vector< std::vector<ALPHAg::TPadSignal> > pad_bytime;
   pad_bytime.reserve(sig.size());
   for( const ALPHAg::TPadSignal& asig: sig_bytime )
      {
         if( fDebug ) asig.print();
         //      if( asig.t > temp ) // 
         if( (asig.t - temp) > fCoincTime )
            {
               temp = asig.t;
               pad_bytime.emplace_back();
               pad_bytime.back().emplace_back(asig);
            }
         else
         {
            pad_bytime.back().emplace_back( asig );
         }
      }
#define MERGEMETHOD 1
 
#if MERGEMETHOD==1
   // merge coincident signals in the same pad into one average signal
   for(auto &tvec: pad_bytime){
      for(auto it = tvec.begin(); it != tvec.end(); it++){
         int n = 1;
         
         // for(auto itt = tvec.rbegin(); itt != it; itt--){
         //    if(itt->idx == it->idx){
         //       it->height += itt->height;
         //       it->t += itt->t;
         //       it->errh = it->errh*it->errh + itt->errh*itt->errh;
         //       tvec.erase(itt);
         //       n++;
         //    }
         // }
         std::reverse_iterator<std::vector<ALPHAg::TPadSignal>::iterator> rit(std::next(it));
         auto itt = tvec.rbegin();
         while(itt != rit){
            if(itt->idx == it->idx) {
               it->height += itt->height;
               it->t += itt->t;
               it->errh = it->errh*it->errh + itt->errh*itt->errh;
               // It is UB to not reassign itt after erase
               auto temp = tvec.erase(std::next(itt).base());
               itt = std::reverse_iterator<std::vector<ALPHAg::TPadSignal>::iterator>(temp);
               n++;
			} else {
               itt++;
            }
         }
         
         if(n > 1){
            it->errh = sqrt(it->errh);
            it->t /= double(n);
            it->height /= double(n);
         }
      }
   }
#endif

#if MERGEMETHOD==2
   // Merge method 2 here is a re-write of the older merge method 1... I think the propgation of errors 
   // is wrong in the older version. Its also less clearly written

   // Testing with Valgrind, method 2 is ~2x faster, however the number of hits changes causing a increase in total number 
   // of hits to process 
   // Method 1: ~149,000 CPU cycles per PartitionByTime call
   // Method 2: ~92,000 CPU cycles per PartitionByTime call


   // merge coincident signals in the same pad into one average signal
   for(auto &tvec: pad_bytime){
      if (tvec.size() <= 1)
         continue;

      // Hold pads with unique index's
      std::vector<ALPHAg::TPadSignal> index_summed;
      // Track number of entries added to each index
      std::vector<int> n;
      // We need to average together hits from the same index
      for (const ALPHAg::TPadSignal& pad: tvec)
      {
         bool already_in_summed_vector = false;
         for (size_t i =0; i < index_summed.size(); i++)
         {
            ALPHAg::TPadSignal& summed = index_summed[i];
            // We only want to merge time bins for pads with the same index
            if (pad.idx != summed.idx) continue;
            summed.t += pad.t;
            summed.height += pad.height;
            summed.errh += pad.errh * pad.errh;
            ++n[i];
            already_in_summed_vector = true;
            break;
         }
         // The pad index was not already found (and added to)... create a new one
         if (!already_in_summed_vector)
         {
            // Copy this pad into the container for summing
            index_summed.emplace_back(pad);
            // Errors are getting added in quadrature
            index_summed.back().errh = index_summed.back().errh * index_summed.back().errh;
            n.emplace_back(1);
         }
      }
      // Loop over the rest of the pads
      for (size_t i =0; i < index_summed.size(); i++)
      {
         ALPHAg::TPadSignal& pad = index_summed[i];
         const double denominator = n[i];
         pad.t /= denominator;
         pad.height /= denominator;
         pad.errh = sqrt(pad.errh);
      
      }
      // Replace the original tvec with the summed version
      tvec.clear();
      tvec = std::move(index_summed);
   }
#endif


   if( fDebug ) std::cout<<"PadMerge::PartitionByTime # of time partitions: "<<pad_bytime.size()<<std::endl;
   return pad_bytime;
}

std::vector<std::vector<ALPHAg::TPadSignal>> PadMerge::CombPads(const std::vector<ALPHAg::TPadSignal>& padsignals)
{
   if( fTrace )
      std::cout<<"PadMerge::CombPads!"<<std::endl;

   if( diagnostic ){
      hNpadSigs->Fill(padsignals.size());
      int ip = hgNpadSigs->GetN();
      hgNpadSigs->SetPoint(ip,ip,padsignals.size());
   }
   std::vector< std::vector<ALPHAg::TPadSignal> > comb;
   if( int(padsignals.size()) > fNpadsCut )
      {
         if( fTrace )
            std::cout<<"PadMerge::CombPads number of pads signals "<<padsignals.size()
                     <<" exceeds its limit "<<fNpadsCut<<std::endl;
         comb.resize(0);
         return comb;
      }

   // combine pads in the same column only
   std::vector< std::vector<ALPHAg::TPadSignal> > pad_bysec;
   std::set<short> secs;
   std::tie(secs, pad_bysec) = PartitionBySector( padsignals ) ;
  
   if( fTrace )
      std::cout<<"PadMerge::CombPads # of secs: "<<secs.size()<<std::endl;

   for (const short& sector: secs)
      {
         if( sector < 0 || sector > 31 ) continue;
         if( fDebug )
            std::cout<<"PadMerge::CombPads sec: "<<sector
                     <<" = sector: "<<pad_bysec[sector].at(0).sec
                     <<" size: "<<pad_bysec[sector].size()<<std::endl;
         // combine pads in the same time slice only
         const std::vector< std::vector<ALPHAg::TPadSignal> > pad_bytime = PartitionByTime( pad_bysec[sector] );
         for( const auto &p: pad_bytime )
            {
               if( p.size() < size_t(padsNmin) ) continue;
               if( p.begin()->t < 0. ) continue;
               comb.emplace_back( std::move( p ));
            }
      }
   secs.clear();
   pad_bysec.clear();
   if( fTrace )
      std::cout<<"PadMerge::CombPads # of teeth: "<<comb.size()<<std::endl;
   return comb;
}

std::vector<ALPHAg::TPadSignal> PadMerge::CombineAPad(
   const std::vector< std::vector<ALPHAg::TPadSignal> >& comb,
   std::vector<ALPHAg::TPadSignal>& CombinedPads,
   const size_t PadNo)
{

   if (PadNo > comb.size())
      return CombinedPads;

   switch(CentreOfGravityFunction)
      {
      case 0: 
         {
            std::cout<<""<<std::endl;
            break;
         }
      case 1:
         {
            CentreOfGravity(comb.at(PadNo),CombinedPads);
            break;
         }
      case 2:
         {
            CentreOfGravity_blobs(comb.at(PadNo), CombinedPads);
            break;
         }
      }
   return CombinedPads;
}


std::vector<ALPHAg::TPadSignal> PadMerge::CombinePads(const std::vector< std::vector<ALPHAg::TPadSignal> >& comb)
{

  
   std::vector<ALPHAg::TPadSignal> CombinedPads;
   if( comb.empty() ) return CombinedPads;

   if( fTrace ) 
      {
         std::cout<<"PadMerge::CombinePads comb size: "<<comb.size()<<"\t";
         std::cout<<"Using CentreOfGravityFunction: "<<CentreOfGravityFunction<<std::endl;
         std::cout<<"PadMerge::CombinePads sssigv: ";
         if( fDebug ) {
            for( auto sigv=comb.begin(); sigv!=comb.end(); ++sigv )
               {
                  std::cout<<sigv->size()<<" ";
               }
         }
         std::cout<<"\n";
      }
  
   switch(CentreOfGravityFunction) {
   case 0: 
      {
         std::cout<<""<<std::endl;
         break;
      }
   case 1: {
      for( auto sigv=comb.begin(); sigv!=comb.end(); ++sigv )
         {
            auto start = std::chrono::high_resolution_clock::now();
            CentreOfGravity(*sigv,CombinedPads);
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start); 
            if( fTrace ) 
               std::cout << "PadMerge::CombinePads Time taken CentreOfGravity: "
                         << duration.count() << " us" << std::endl; 
            if( diagnostic ) htimecog->Fill(duration.count());
         }
      break;}
   case 2: {
      auto cogstart = std::chrono::high_resolution_clock::now();
      for (const auto& combpads: comb)
         {
            PadMerge::CentreOfGravity_blobs(combpads, CombinedPads);
         }   
      auto cogstop = std::chrono::high_resolution_clock::now();
      auto cogdura = std::chrono::duration_cast<std::chrono::microseconds>(cogstop-cogstart);
      if( diagnostic ) htimecog->Fill(cogdura.count());
      if( fTrace ) {
         std::cout<<"PadMerge::CombinePads Time taken CentreOfGravity_blobs: "
                  << cogdura.count() << " us" << std::endl; 
      }
      break;}
   }
   return CombinedPads;
}

std::vector<ALPHAg::TPadSignal> PadMerge::CombinePads(const std::vector<ALPHAg::TPadSignal>& padsignals)
{
   const std::vector< std::vector<ALPHAg::TPadSignal> > comb = CombPads( padsignals );
   return CombinePads(comb);
}

void PadMerge::CentreOfGravity(const  std::vector<ALPHAg::TPadSignal> &vsig, std::vector<ALPHAg::TPadSignal>& CombinedPads )
{
   if(!vsig.size()) return;

   //Root's fitting routines are often not thread safe, lock globally
   manalzer_global_mtx->lock();
   double time = vsig.begin()->t;
   short col = vsig.begin()->sec;
   TString hname = TString::Format("hhhhh_%d_%1.0f",col,time);
   //      std::cout<<hname<<std::endl;
   auto start = std::chrono::high_resolution_clock::now();
   TH1D* hh = new TH1D(hname.Data(),"",int(ALPHAg::_padrow),-ALPHAg::_halflength,ALPHAg::_halflength);
   for( auto& s: vsig )
      {
         // s.print();
         double z = ( double(s.idx) + 0.5 ) * ALPHAg::_padpitch - ALPHAg::_halflength;
         //hh->Fill(s.idx,s.height);
         hh->SetBinContent(hh->GetXaxis()->FindBin(z),s.height);
      }

   // exploit wizard avalanche centroid (peak)
   TSpectrum spec(maxPadGroups);
   int error_level_save = gErrorIgnoreLevel;
   gErrorIgnoreLevel = kFatal;
   spec.Search(hh,1,"nodraw");
   auto stop = std::chrono::high_resolution_clock::now();
   auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start); 
   int nfound = spec.GetNPeaks();

   if( diagnostic )
      {
         htimeblobs->Fill(duration.count());
         hcognpeaks ->Fill(nfound);
      }

   gErrorIgnoreLevel = error_level_save;

   if( fDebug )
      std::cout<<"PadMerge::CentreOfGravity nfound: "<<nfound<<" @ t: "<<time<<std::endl;
   if( nfound > 1 && hh->GetRMS() < spectrum_width_min )
      {
         nfound = 1;
         if( fDebug )
            std::cout<<"\tRMS is small: "<<hh->GetRMS()<<" set nfound to 1"<<std::endl;
      }

   std::vector<double> peakx(nfound);
   std::vector<double> peaky(nfound);

   for(int i = 0; i < nfound; ++i)
      {
         peakx[i]=spec.GetPositionX()[i];
         peaky[i]=spec.GetPositionY()[i];
         TString ffname = TString::Format("fffff_%d_%1.0f_%d",col,time,i);
         TF1* ff = new TF1(ffname.Data(),"gaus(0)",peakx[i]-10.*padSigma,peakx[i]+10.*padSigma);
         // initialize gaussians with peak finding wizard
         ff->SetParameter(0,peaky[i]);
         ff->SetParameter(1,peakx[i]);
         ff->SetParameter(2,padSigma);

         start = std::chrono::high_resolution_clock::now();
         int r = hh->Fit(ff,"B0NQ");
         stop = std::chrono::high_resolution_clock::now();
         duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
         if( diagnostic ) htimefit->Fill(duration.count());
#ifdef RESCUE_FIT
         bool stat=true;
#endif
         if( r==0 ) // it's good
            {
               // make sure that the fit is not crazy...
               double sigma = ff->GetParameter(2);
               double err = ff->GetParError(1);
               double pos = ff->GetParameter(1);
               double zix = ( pos + ALPHAg::_halflength ) / ALPHAg::_padpitch - 0.5;
               int row = (zix - floor(zix)) < 0.5 ? int(floor(zix)):int(ceil(zix));
               double amp = ff->GetParameter(0);
               double eamp = ff->GetParError(0);
        
               if( diagnostic )
                  {
                     hcogsigma->Fill(sigma);
                     hcogerr->Fill(err);
                     int index = pmap.index(col,row);
                     hcogpadssigma->Fill(double(index),sigma);
                     hcogpadsamp->Fill(double(index),amp);
                     double totq = ff->Integral(pos-10.*sigma,pos+10.*sigma);
                     hcogpadsint->Fill(double(index),totq);
                     hcogpadsampamp->Fill(peaky[i],amp);
                  }
               if( err < padFitErrThres &&
                   fabs(sigma-padSigma)/padSigma < padSigmaD )
                  //if( err < padFitErrThres && sigma > 0. )
                  {
                     // create new signal with combined pads
                     CombinedPads.emplace_back( ALPHAg::electrode( col, row), time, amp, eamp, pos, err );
                     if( fDebug )
                        std::cout<<"Combination Found! s: "<<col
                                 <<" i: "<<row
                                 <<" t: "<<time
                                 <<" a: "<<amp
                                 <<" z: "<<pos
                                 <<" err: "<<err<<std::endl;
                  }
               else // fit is crazy
                  {
                     if( fTrace )
                        std::cout<<"Combination NOT found... position error: "<<err
                                 <<" or sigma: "<<sigma<<std::endl;
#ifdef RESCUE_FIT
                     stat=false;
#endif
                  }
            }// fit is valid
         else
            {
               if( fTrace )
                  std::cout<<"\tFit Not valid with status: "<<r<<std::endl;
#ifdef RESCUE_FIT
               stat=false;
#endif
            }
         delete ff;

#ifdef RESCUE_FIT
         if( !stat )
            {
               int b0 = hh->FindBin(peakx[i]);
               int bmin = b0-5, bmax=b0+5;
               if( bmin < 1 ) bmin=1;
               if( bmax > int(_padrow) ) bmax=int(_padrow);
               double zcoord=0.,tot=0.;
               for( int ib=bmin; ib<=bmax; ++ib )
                  {
                     double bc = hh->GetBinContent(ib);
                     zcoord += bc*hh->GetBinCenter(ib);
                     tot += bc;
                  }
               if( tot > 0. )
                  {
                     //double amp = tot/11.;
                     double amp = tot;
                     double pos = zcoord/tot;
                     double zix = ( pos + _halflength ) / _padpitch - 0.5;
                     int index = (zix - floor(zix)) < 0.5 ? int(floor(zix)):int(ceil(zix));
        
                     // create new signal with combined pads
                     CombinedPads->emplace_back( col, index, time, amp, sqrt(amp), pos, zed_err );
        
                     if( fDebug )
                        std::cout<<"at last Found! s: "<<col
                                 <<" i: "<<index
                                 <<" t: "<<time
                                 <<" a: "<<amp
                                 <<" z: "<<pos<<std::endl;
                     stat=true;
                  }
               else
                  {
                     if( fTrace )
                        std::cout<<"Failed last combination resort"<<std::endl;
                  }
            }
#endif
      } // wizard peak finding failed
   delete hh;
   if( fTrace )
      std::cout<<"-------------------------------"<<std::endl;
   manalzer_global_mtx->unlock();
}


// TH1-independent method to find peaks in pad charge distribution
std::vector<std::pair<double, double> > PadMerge::FindBlobs(const std::vector<ALPHAg::TPadSignal> &sigs,
                                                            int ifirst, int ilast)
{
   if(ilast < 0) ilast = int(sigs.size());
   std::vector<ALPHAg::TPadSignal>::const_iterator first = std::next(sigs.begin(),ifirst);
   std::vector<ALPHAg::TPadSignal>::const_iterator last = std::next(sigs.begin(),ilast);
   std::vector<std::pair<double, double> > blobs;

   ALPHAg::signal::heightorder sigcmp_h;
   auto maxit = std::max_element(first, last, sigcmp_h);
   double maxpos = maxit->z;
   double max = maxit->height;
   auto max_left = first;
   while(max_left->height != max) {
      ++max_left;
   }
   auto max_right = std::prev(last);
   while(max_right->height != max) {
      --max_right;
   }

   if(max_left == first && first != sigs.begin()){ // if maximum is at left edge of dist,
      auto previt = std::prev(max_left);           // and there is another high signal nearby,
      if(previt->height >= max && abs(max_left->z - previt->z) <= 2.*ALPHAg::_padpitch) // don't count
         return blobs;
   }
   if(std::next(max_right) == last && last != sigs.end()){   // if maximum is at right edge of dist,
      auto nextit = std::next(max_right);           // and there is another high signal nearby,
      if(nextit->height >= max && abs(nextit->z - max_right->z) <= 2.*ALPHAg::_padpitch) // don't count
         return blobs;
      // ilast = std::min(sigs.size(), ilast+5); // expand range and try again
      // return FindBlobs(sigs, ifirst, ilast, cumulBins);
   }

   double blobwidth = 5.;
   double minRMS = 2.;
   int padmask = 4;

   double mean,rms;
   SignalsStatistics(first, last, mean, rms);

   // select range for blob weighted mean
   bool foundEdge = false;

   auto leftit = max_left;
   int nsigs = 0;
   while(leftit != first){
      if(diagnostic){
         hcogpadsblob->Fill(leftit->idx - maxit->idx, leftit->height/max);
      }
      if(leftit != maxit && leftit->idx == maxit->idx){
         maxit->print();
         leftit->print();
      }
      nsigs++;
      if(leftit->height < max){
         auto it2 = leftit;
         it2--;
         if(it2->height <= leftit->height){
            foundEdge = true;
            break;
         }
      }
      leftit--;
   }
   
   auto rightit = max_right;
   while(std::next(rightit) != last){
      if(diagnostic){
         hcogpadsblob->Fill(rightit->idx - maxit->idx, rightit->height/max);
      }
      nsigs++;
      if(rightit->height < max){
         auto it2 = rightit;
         it2++;
         if(it2->height <= rightit->height){
            foundEdge = true;
            break;
         }
      }
      rightit++;
   }

   if(diagnostic){
      hNblobSigs->Fill(nsigs);
   }
   if(foundEdge) edge++;
   else noedge++;
   
   double blobmean,blobrms;
   SignalsStatistics(leftit, std::next(rightit), blobmean, blobrms);

   if(rms < blobwidth && abs(maxpos-mean) < blobwidth){ // small width, search no further peaks
      if(rms > minRMS){  // width is not too small for real peak
         double z_pos;
         if(padCogUseMean || max_left != max_right) {
            z_pos = blobmean;
         } else {
            z_pos = maxpos;
         }
         blobs.emplace_back(z_pos, max);
         if( diagnostic )
            hcogpadsdmaxmean->Fill(blobmean - maxpos);
      }
   } else {                  // large width, save this peak, then search for more
      double z_pos;
      if(padCogUseMean || max_left != max_right) {
         z_pos = blobmean;
      } else {
         z_pos = maxpos;
      }
      blobs.emplace_back(z_pos, max);
      if( diagnostic )
         hcogpadsdmaxmean->Fill(blobmean - maxpos);

      int cutbin = max_left-sigs.begin()-padmask;
      std::vector<std::pair<double, double> > subblobs;
      if(cutbin-ifirst > padsNmin){ // search left of found peak
         subblobs = FindBlobs(sigs, ifirst, cutbin);
         blobs.insert(blobs.end(), subblobs.begin(), subblobs.end());
      }

      cutbin = max_right-sigs.begin()+padmask + 1;
      if(ilast-cutbin > padsNmin){ // search right of found peak
         subblobs = FindBlobs(sigs, cutbin, ilast);
         blobs.insert(blobs.end(), subblobs.begin(), subblobs.end());
      }
   }
   return blobs;
}


void PadMerge::CentreOfGravity_blobs( const std::vector<ALPHAg::TPadSignal>& vsig, std::vector<ALPHAg::TPadSignal>& CombinedPads )
{
   //Unused
   //int nPositions=0;
   if(int(vsig.size()) < padsNmin) return;
   double time = vsig.begin()->t;
   short col = vsig.begin()->sec;

   std::vector<ALPHAg::TPadSignal> vsig_sorted(vsig);
   ALPHAg::signal::indexorder sigcmp_z;
   auto start = std::chrono::high_resolution_clock::now();
   std::sort(vsig_sorted.begin(), vsig_sorted.end(), sigcmp_z);
   std::vector<std::pair<double, double> > blobs = FindBlobs(vsig_sorted, 0, -1);

   int nfound = blobs.size();
   if( fTrace )
      std::cout<<"PadMergeModule::CentreOfGravity_blobs nfound: "<<nfound<<" @ t: "<<time<<" in sec: "<<col<<std::endl;

   std::vector<double> peakx, peaky; // initiliaze CoG fit
   // cut grass
   for(int i = 0; i < nfound; ++i)
      {
         bool grass(false);
         for(int j = 0; j < i; j++)
            {
               if(abs(blobs[i].first-blobs[j].first) < goodDist)
                  if(blobs[i].second < grassCut*blobs[j].second)
                     grass = true;
            }
         if(!grass)
            {
               if( fDebug ) 
                  std::cout << blobs[i].first << '\t';
               peakx.push_back(blobs[i].first);
               peaky.push_back(blobs[i].second);
            }
         else
            {
               if( fDebug ) std::cout << "OOOO cut grass peak at " << blobs[i].first << std::endl;
            }
      }
   if( fDebug ) std::cout << "\n";

   auto stop = std::chrono::high_resolution_clock::now();
   auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start); 
 
   nfound=int(peakx.size());
   if( fDebug )
      std::cout<<"PadMerge::PadMergeModule::CentreOfGravity_blobs nfound after grass cut: "<<nfound<<" @ t: "<<time<<" in sec: "<<col<<std::endl;
   // assert(int(peakx.size())==nfound);

   if(diagnostic)
      {
         htimeblobs->Fill(duration.count());
         hcognpeaks->Fill(nfound);
      }

   if (padCogDoFit) {
      fitSignals ffs( vsig_sorted, nfound );
      for(int i = 0; i < nfound; ++i)
         {
            ffs.SetStart(3*i,peaky[i]);
            ffs.SetStart(1+3*i,peakx[i]);
            ffs.SetStart(2+3*i,padSigma);
         }

      ffs.FixSigma(padCogFixSigma);

      //  const std::vector<double> init = ffs.GetStart();
      // std::cout<<"init: ";
      // for( auto it=init.begin(); it!=init.end(); ++it)
      //   std::cout<<*it<<", ";
      // std::cout<<"\n";

      start = std::chrono::high_resolution_clock::now();
      ffs.Fit();
      stop = std::chrono::high_resolution_clock::now();
      duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
      if( diagnostic ) htimefit->Fill(duration.count());
      int r = ffs.GetStat();
      if( r==1 ) // it's good
         {
            if( fTrace ) ffs.Print();
            for(int i = 0; i < nfound; ++i)
               {
                  double amp = ffs.GetAmplitude(i);
                  double amp_err = ffs.GetAmplitudeError(i);

                  double pos = ffs.GetMean(i);
                  double zix = ( pos + ALPHAg::_halflength ) / ALPHAg::_padpitch - 0.5;
                  int row = (zix - floor(zix)) < 0.5 ? int(floor(zix)):int(ceil(zix));

                  double err = ffs.GetMeanError(i);
                  double sigma = ffs.GetSigma(i);

                  double dx = 1e99;
                  // for(unsigned int j = 0; j < peakx.size(); j++){
                  //   double ddx = pos - peakx[j];
                  //   if(fabs(ddx) < fabs(dx))
                  //     dx = ddx;
                  // }
                  dx = pos - peakx[i];

                  if( diagnostic )
                     {
                        hcogsigma->Fill(sigma);
                        hcogerr->Fill(err);
                        int index = pmap.index(col,row);
                        hcogpadssigma->Fill(double(index),sigma);
                        hcogpadsamp->Fill(double(index),amp);
                        // double totq = ff->Integral(pos-10.*sigma,pos+10.*sigma);
                        double totq = sqrt(2.*M_PI)*sigma*amp;
                        hcogpadsint->Fill(double(index),totq);
                        hcogpadsampamp->Fill(peaky[i],amp);
                        hcogpadsdfitpos->Fill(dx);
                     }

                  if( err < padFitErrThres &&
                      fabs(sigma-padSigma)/padSigma < padSigmaD )
                     {
                        if( fabs(pos) < ALPHAg::_halflength )
                           {
                              // create new signal with combined pads
                              CombinedPads.emplace_back( ALPHAg::electrode(col, row), time, amp, amp_err, pos, err );
                              //signal pad_cog( col, row, time, amp, amp_err, pos, err );
                              //padcog.push_back(pad_cog);
                              //Unused
                              //++nPositions;
                              if( fDebug )
                                 std::cout<<"CoG_blobs Combination Found! s: "<<col
                                          <<" i: "<<row
                                          <<" t: "<<time
                                          <<" a: "<<amp
                                          <<" z: "<<pos
                                          <<" err: "<<err<<std::endl;
                           }
                        else
                           {
                              if( fDebug )
                                 std::cout<<"CoG_blobs Bad Combination Found! (z outside TPC) s: "<<col
                                          <<" i: "<<row
                                          <<" t: "<<time
                                          <<" a: "<<amp
                                          <<" z: "<<pos
                                          <<" err: "<<err<<std::endl;
                           }
                     }
                  else // fit is crazy
                     {
                        if( fTrace )
                           std::cout<<"Combination NOT found... position error: "<<err
                                    <<" or sigma: "<<sigma<<std::endl;
                     }
               } // loop over blobs and their fit
         }// fit is valid
      else
         {
            if( fTrace )
               std::cout<<"\tFit Not valid with status: "<<r<<std::endl;
         }
   }
   else
      {
         for (unsigned int i = 0; i < peakx.size(); i++) {
            double zix = (peakx[i] + ALPHAg::_halflength) / ALPHAg::_padpitch - 0.5;
            int    row = (zix - floor(zix)) < 0.5 ? int(floor(zix)) : int(ceil(zix));
            // create new signal with combined pads
            CombinedPads.emplace_back(ALPHAg::electrode(col, row), time, peaky[i], ALPHAg::kUnknown, peakx[i], zed_err);
            //Unused
            //++nPositions;
         }
      }

   if( fTrace )
      std::cout<<"-------------------------------"<<std::endl;

   //  return nPositions;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
