// Tracks finder class implementation
// for ALPHA-g TPC analysis
// Author: A.Capra, G.Smith
// Date: Sep. 2016, Oct. 2022

#include "TPCconstants.hh"
#include "IterativeFinder.hh"
#include <iostream>
#include "TSeqCollection.h"

IterativeFinder::IterativeFinder(const std::vector<TSpacePoint>* points, const double maxIncrease, const double LastPointRadCut,
                                 const double Adaptiveness, const int TrackIterataions, const double TrackLengthCut):
   TracksFinder(points),
   fLastPointRadCut(LastPointRadCut),
   fPointsRadCut(4.),
   fPointsPhiCut( ALPHAg::_anodepitch*2. ),
   fPointsZedCut( ALPHAg::_padpitch*1.1 ),
   fMaxIncreseAdapt(maxIncrease),
   fAdaptiveness(Adaptiveness),
   fTrackIterations(TrackIterataions),
   fTrackLengthCut(TrackLengthCut)
{
   // No inherent reason why these parameters should be the same as in base class
   fSeedRadCut = 150.;
   fPointsDistCut = 8.1;
   fPointMaxRad = ALPHAg::_fwradius-1;
   fSmallRad = ALPHAg::_cathradius;
   fNpointsCut = 7;
   if( debug )
      std::cout<<"IterativeFinder::IterativeFinder ctor!"<<std::endl;
}

//==============================================================================================
int IterativeFinder::RecTracks(std::vector<track_t>& TrackVector)
{
   int Npoints = fPointsArray.size();
   if( Npoints<=0 )
      return -1;
   if( debug )
      std::cout<<"IterativeFinder::IterativeFinder() # of points: "<<Npoints<<std::endl;

   // Pattern Recognition algorithm
   int i_track = 0;
   hit_used.clear();
   hit_used = std::vector<bool>(Npoints,false);

   for(int i=0; i<Npoints; ++i)
      {
         TSpacePoint* point=fPointsArray[i];
         if (!point) continue;
         // spacepoints in the proportional region and "near" the fw (r=174mm) are messy
         // thus I include spacepoints up to r=173mm
         // spacepoints in the proportional region and "near" the fw (r=174mm) are messy
         if( !point->IsGood(ALPHAg::_cathradius, fPointMaxRad) )  
            {
#if BUILD_EXCLUSION_LIST
               fExclusionList.push_back(fPointsArray[i]);
#endif
               fPointsArray[i]=NULL;
               continue;
            }

         // do not start a track far from the anode
         if( point->GetR() < fSeedRadCut && TrackVector.size() > 0 ) {
            break;
         }

         track_t vector_points;
         vector_points.clear();
         vector_points.push_front(i);

         std::vector<bool> hit_used_save = hit_used;
         hit_used.at(i) = true;

         int gapidx = NextPoint( point, i , Npoints, fPointsDistCut, vector_points );
         TSpacePoint* LastPoint =  fPointsArray.at( gapidx );

         gapidx = Search(LastPoint, gapidx, Npoints, vector_points);
         LastPoint = fPointsArray.at( gapidx );
         // Gareth test
         for (int rep = 0; rep<fTrackIterations; rep++) {
            int n_points = vector_points.size();
            for (int p=0; p<n_points; p+=1) { // Check for missed points starting from every point. Can maybe start every third point?
               int start_index = vector_points.at(p);
               TSpacePoint* NewStartPoint = fPointsArray.at(start_index);
               gapidx = Search(NewStartPoint, vector_points.at(p), Npoints, vector_points);
               // This makes the track finding better at identifying tracks which curve backwards.
               // However, those tracks are fit badly, so this hurts the overall vertex efficiency until they are fit properly.
               //NewStartPoint = fPointsArray.at(start_index);
               //firstidx = SearchBack(NewStartPoint, vector_points.at(p), Npoints, vector_points);
            }
            double dist = point->Distance(LastPoint);
            //if (fPointsArray[firstidx]->Distance(LastPoint) > dist) {
            //   point = fPointsArray[firstidx];
            //   dist = fPointsArray[firstidx]->Distance(LastPoint);
            //}
            if (point->Distance(fPointsArray[gapidx]) > dist) {
               LastPoint = fPointsArray[gapidx];
            }
            if ((int)vector_points.size()==n_points) {
               break;
            }
         }
         // Gareth test end
         if( int(vector_points.size()) < fNpointsCut )
            {
               ++points_cut;
               hit_used = hit_used_save;
               for (auto& it: vector_points) {
                  fPointsArray[it]->SetTrackFinderStatus(failed_points_cut);
               }
               continue;
            }
         else if( LastPoint->GetR() > fLastPointRadCut )
            {
               ++rad_cut;
               hit_used = hit_used_save;
               for (auto& it: vector_points) {
                  fPointsArray[it]->SetTrackFinderStatus(failed_rad_cut);
               }
               continue;
            }
         else if( point->Distance(LastPoint) < fTrackLengthCut )
            {
               ++length_cut;
               hit_used = hit_used_save;
               for (auto& it: vector_points) {
                  fPointsArray[it]->SetTrackFinderStatus(failed_length_cut);
               }
               continue;
            }
         else
            {
               i_track++;
               //printf("----> Number of iterations ======== %d\n",n_iter);

               TrackVector.push_back( vector_points );
               for(auto& it: vector_points)
                  {
#if BUILD_EXCLUSION_LIST
                     fExclusionList.push_back(fPointsArray[it]);
#endif
                     fPointsArray[it]->SetRecoTrackID(i_track);
                     fPointsArray[it]->SetTrackFinderStatus(passed);
                     fPointsArray[it]=NULL;
                  }
               ++fNtracks;
            }
      }//i loop
   if( fNtracks != int(TrackVector.size()) )
      std::cerr<<"IterativeFinder::IterativeFinder(): Number of found tracks "<<fNtracks
               <<" does not match the number of entries "<<TrackVector.size()<<std::endl;
   else if( debug )
      {
         std::cout<<"IterativeFinder::IterativeFinder(): Number of found tracks "<<fNtracks<<std::endl;
         std::cout<<"IterativeFinder::IterativeFinder() -- Reasons: Track Not Advancing "<<track_not_advancing
                  <<" Points Cut: ("<<fNpointsCut<<"): "<<points_cut
                  <<" Length Cut: ("<<fTrackLengthCut<<"): "<<points_cut
                  <<" Radius Cut ("<<fLastPointRadCut<<" mm): "<<rad_cut<<std::endl;
      }

   return fNtracks;
}

int IterativeFinder::NextPoint(const TSpacePoint* SeedPoint, const int index, const int Npoints, double& distcut, track_t& atrack) 
{
   int LastIndex = index;
   for(int j = index+1; j < Npoints; ++j)
      {
         if (hit_used.at(j)) continue;
         const TSpacePoint* NextPoint = fPointsArray[j];
         if (!NextPoint) continue;
         if( SeedPoint->Distance( NextPoint ) <= distcut )
            {
               SeedPoint = NextPoint;
               atrack.push_back(j);
               LastIndex = j;
               distcut = fPointsDistCut;
               hit_used.at(j) = true;
               //return LastIndex;
               //Just an idea: but right now I change the results
               /*if( int(atrack.size()) > fNpointsCut )
                 {
                 //Track already has more points than the points cut... abort
                 return LastIndex;
                 }*/
            }
      }// j loop
   return LastIndex;
}

int IterativeFinder::NextPointBack(const TSpacePoint* SeedPoint, const int index, double& distcut, track_t& atrack) 
{
   int LastIndex = index;
   for(int j = index-1; j >=0 ; --j)
      {
         if (hit_used.at(j)) continue;
         const TSpacePoint* NextPoint = fPointsArray[j];
         if (!NextPoint) continue;
         if( SeedPoint->Distance( NextPoint ) <= distcut )
            {
               SeedPoint = NextPoint;
               atrack.push_back(j);
               LastIndex = j;
               distcut = fPointsDistCut;
               hit_used.at(j) = true;
               //return LastIndex;
               //Just an idea: but right now I change the results
               /*if( int(atrack.size()) > fNpointsCut )
                 {
                 //Track already has more points than the points cut... abort
                 return LastIndex;
                 }*/
            }
      }// j loop
   return LastIndex;
}

int IterativeFinder::Search(const TSpacePoint* LastPoint, int gapidx, const int Npoints, track_t& atrack) 
{
   double AdaptDistCut = fPointsDistCut*fAdaptiveness;
   while( LastPoint->GetR() > fSmallRad )
      {
         // LastPoint->Print("rphi");
         // std::cout<<"AdaptDistCut: "<<AdaptDistCut<<" mm"<<std::endl;
         if( AdaptDistCut > fMaxIncreseAdapt ) break;
         //gapidx = NextPoint( LastPoint, gapidx ,Npoints, AdaptDistCut, vector_points );
         gapidx = NextPoint( LastPoint, gapidx ,Npoints, AdaptDistCut, atrack );
         LastPoint = fPointsArray.at( gapidx );
         AdaptDistCut*=fAdaptiveness;
      }
   return gapidx;

}

int IterativeFinder::SearchBack(const TSpacePoint* LastPoint, int gapidx, track_t& atrack) 
{
   double AdaptDistCut = fPointsDistCut*fAdaptiveness;
   while( LastPoint->GetR() > fSmallRad )
      {
         // LastPoint->Print("rphi");
         // std::cout<<"AdaptDistCut: "<<AdaptDistCut<<" mm"<<std::endl;
         if( AdaptDistCut > fMaxIncreseAdapt ) break;
         //gapidx = NextPoint( LastPoint, gapidx ,Npoints, AdaptDistCut, vector_points );
         gapidx = NextPointBack( LastPoint, gapidx , AdaptDistCut, atrack );
         LastPoint = fPointsArray.at( gapidx );
         AdaptDistCut*=fAdaptiveness;
      }
   return gapidx;

}

int IterativeFinder::NextPoint(const int index,
                              double radcut, double phicut, double zedcut,
                              track_t& atrack) const
{
   const TSpacePoint* SeedPoint = fPointsArray.at( index );

   int LastIndex = index;
   int Npoints = fPointsArray.size();
   for(int j = index+1; j < Npoints; ++j)
      {
         const TSpacePoint* NextPoint = fPointsArray[j];
         if (!NextPoint) continue;
         if( SeedPoint->MeasureRad( NextPoint ) <= radcut &&
             SeedPoint->MeasurePhi( NextPoint ) <= phicut &&
             SeedPoint->MeasureZed( NextPoint ) <= zedcut )
            {
               SeedPoint = NextPoint;
               atrack.push_back(j);
               LastIndex = j;
               radcut = fPointsRadCut;
               phicut = fPointsPhiCut;
               zedcut = fPointsZedCut;
            }
      }// j loop

   return LastIndex;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
