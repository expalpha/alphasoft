// Helix class implementation
// for ALPHA-g TPC analysis
// Author: A.Capra
// Date: May 2014

#include "TFitHelix.hh"
#include "TSpacePoint.hh"

#include "TStoreHelix.hh"

#include <iostream>
#include <iomanip>

#include <TMath.h>
#include <TMinuit.h>

#include <TMatrixDSym.h>
#include <TMatrixD.h>

#include"HelixFCN.hh"
#include"Minuit2/FunctionMinimum.h"
#include"Minuit2/MnMigrad.h"


bool kDcut = true;
bool kccut = true;
bool kpcut = false;

TFitHelix::TFitHelix():TTrack(),
           fc(ALPHAg::kUnknown),fRc(ALPHAg::kUnknown),
           fphi0(ALPHAg::kUnknown),fD(ALPHAg::kUnknown),
           flambda(ALPHAg::kUnknown),fz0(ALPHAg::kUnknown),
           fa(ALPHAg::kUnknown),
           fx0(ALPHAg::kUnknown),fy0(ALPHAg::kUnknown),
           ferr2c(ALPHAg::kUnknown),ferr2Rc(ALPHAg::kUnknown),
           ferr2phi0(ALPHAg::kUnknown),ferr2D(ALPHAg::kUnknown),
           ferr2lambda(ALPHAg::kUnknown),ferr2z0(ALPHAg::kUnknown),
           fDoubleBranched(false),fBranch(0),fBeta(0.),fTrackNum(-1),
           fchi2R(0.),fStatR(-1),
           fchi2Z(0.),fStatZ(-1),
           fChi2RCut(15.),fChi2ZCut(8.),
           fChi2RMin(1.),fChi2ZMin(0.1),
           fcCut(16.e-3),fDCut(40.),
           fpCut(15.),
           fRFiterations(500), fZFiterations(500),
           fHelPreferStraight(1.1), fHelSmallCurvature(30.)
{  
  fPointsCut = 10;
  fMomentum.SetXYZ(0.0,0.0,0.0);
  fMomentumError.SetXYZ(0.0,0.0,0.0);
  fBarHitsFirstBranch = {};
  fBarHitsSecondBranch = {};
  fIsPileup = false;
}

TFitHelix::TFitHelix(const TTrack& atrack):TTrack(atrack),
             fc(ALPHAg::kUnknown),fRc(ALPHAg::kUnknown),
             fphi0(ALPHAg::kUnknown),fD(ALPHAg::kUnknown),
             flambda(ALPHAg::kUnknown),fz0(ALPHAg::kUnknown),
             fa(ALPHAg::kUnknown),
             fx0(ALPHAg::kUnknown),fy0(ALPHAg::kUnknown),
             ferr2c(ALPHAg::kUnknown),ferr2Rc(ALPHAg::kUnknown),
             ferr2phi0(ALPHAg::kUnknown),ferr2D(ALPHAg::kUnknown),
             ferr2lambda(ALPHAg::kUnknown),ferr2z0(ALPHAg::kUnknown),
             fDoubleBranched(false),fBranch(0),fBeta(0.),fTrackNum(-1),
             fchi2R(0.),fStatR(-1),
             fchi2Z(0.),fStatZ(-1),
             fChi2RCut(15.),fChi2ZCut(8.),
             fChi2RMin(1.),fChi2ZMin(0.1),
             fcCut(16.e-3),fDCut(40.),
             fpCut(15.),
             fRFiterations(500), fZFiterations(500),
             fHelPreferStraight(1.1), fHelSmallCurvature(30.)
{
  fPointsCut = 10;
  fMomentum.SetXYZ(0.0,0.0,0.0);
  fMomentumError.SetXYZ(0.0,0.0,0.0);
  fBarHitsFirstBranch = {};
  fBarHitsSecondBranch = {};
  fIsPileup = false;
}


TFitHelix::TFitHelix(TObjArray* points):TTrack(points),
          fc(ALPHAg::kUnknown),fRc(ALPHAg::kUnknown),
          fphi0(ALPHAg::kUnknown),fD(ALPHAg::kUnknown),
          flambda(ALPHAg::kUnknown),fz0(ALPHAg::kUnknown),
          fa(ALPHAg::kUnknown),
          fx0(ALPHAg::kUnknown),fy0(ALPHAg::kUnknown),
          ferr2c(ALPHAg::kUnknown),ferr2Rc(ALPHAg::kUnknown),
          ferr2phi0(ALPHAg::kUnknown),ferr2D(ALPHAg::kUnknown),
          ferr2lambda(ALPHAg::kUnknown),ferr2z0(ALPHAg::kUnknown),
          fDoubleBranched(false),fBranch(0),fBeta(0.),fTrackNum(-1),
          fchi2R(0.),fStatR(-1),
          fchi2Z(0.),fStatZ(-1),
          fChi2RCut(7.),fChi2ZCut(4.),
          fChi2RMin(0.1),fChi2ZMin(0.1),
          fcCut(0.001),fDCut(40.),
          fpCut(15.),
          fRFiterations(500), fZFiterations(500),
          fHelPreferStraight(1.1), fHelSmallCurvature(30.)
{ 
  fPointsCut = 10;
  fMomentum.SetXYZ(0.0,0.0,0.0);
  fMomentumError.SetXYZ(0.0,0.0,0.0);
  fBarHitsFirstBranch = {};
  fBarHitsSecondBranch = {};
  fIsPileup = false;
}

TFitHelix::TFitHelix(const TStoreHelix& h):TTrack(h.GetSpacePoints()),
             fc(h.GetC()), fRc(h.GetRc()), 
             fphi0(h.GetPhi0()), fD(h.GetD()),
             flambda(h.GetLambda()), fz0(h.GetZ0()),
             fx0( h.GetX0() ), fy0( h.GetY0() ),
             ferr2c(h.GetErrC()), ferr2Rc(h.GetErrRc()), 
             ferr2phi0(h.GetErrPhi0()), ferr2D(h.GetErrD()),
             ferr2lambda(h.GetErrLambda()), ferr2z0(h.GetErrZ0()),
             fDoubleBranched(h.IsDoubleBranched()), fBranch( h.GetBranch() ), fBeta( h.GetFBeta() ), fTrackNum(h.GetTrackNumber()),
             fMomentum(h.GetMomentumV()), fMomentumError(h.GetMomentumVerror()),
             fBarHitsFirstBranch(h.GetBarHitsFirstBranch()), fBarHitsSecondBranch(h.GetBarHitsSecondBranch()), fIsPileup(h.IsPileup()),
             fchi2R(h.GetRchi2()), fchi2Z(h.GetZchi2())
{
  SetStatus( h.GetStatus() );
  
  SetResidual( h.GetResidual() );
  std::vector<double> res = h.GetResidualsVector();
  SetResidualsVector( res );
  SetResidualsSquared( h.GetResidualsSquared() );
}

TFitHelix::TFitHelix( const TFitHelix& right ):TTrack(right), 
                 fc(right.fc), fRc(right.fRc),
                 fphi0(right.fphi0), fD(right.fD),
                 flambda(right.flambda),fz0(right.fz0),
                 fa(right.fa),
                 fx0(right.fx0), fy0(right.fy0),
                 ferr2c(right.ferr2c), ferr2Rc(right.ferr2Rc),
                 ferr2phi0(right.ferr2phi0), ferr2D(right.ferr2D),
                 ferr2lambda(right.ferr2lambda), ferr2z0(right.ferr2z0),
                 fDoubleBranched(right.fDoubleBranched), fBranch(right.fBranch), fBeta(right.fBeta), fTrackNum(right.fTrackNum),
                 fMomentum(right.fMomentum), fMomentumError(right.fMomentumError),
                 fBarHitsFirstBranch(right.fBarHitsFirstBranch), fBarHitsSecondBranch(right.fBarHitsSecondBranch), fIsPileup(right.fIsPileup),
                 fchi2R(right.fchi2R), fStatR(right.fStatR),fchi2Z(right.fchi2Z), fStatZ(right.fStatZ)
{ }

TFitHelix& TFitHelix::operator=( const TFitHelix& right )
{
  fPoints     = right.fPoints;
  fNpoints    = right.fNpoints;
  fStatus     = right.fStatus;
  fParticle   = right.fParticle;
  fResiduals2 = right.fResiduals2;
  fResidual   = right.fResidual;
  fResiduals  = right.fResiduals;
  #if USE_MAPS
  fResidualsRadii = right.fResidualsRadii;
  fResidualsXY = right.fResidualsXY;
  #endif
  fPoint      = right.fPoint;
  fc = right.fc; fRc = right.fRc;
  fphi0 = right.fphi0; fD = right.fD;
  flambda = right.flambda;fz0 = right.fz0;
  fa = right.fa;
  fx0 = right.fx0; fy0 = right.fy0;
  ferr2c = right.ferr2c; ferr2Rc = right.ferr2Rc;
  ferr2phi0 = right.ferr2phi0; ferr2D = right.ferr2D;
  ferr2lambda = right.ferr2lambda; ferr2z0 = right.ferr2z0;
  fDoubleBranched = right.fDoubleBranched; fBranch = right.fBranch; fBeta = right.fBeta; fTrackNum = right.fTrackNum;
  fMomentum = right.fMomentum; fMomentumError = right.fMomentumError;
  fBarHitsFirstBranch = right.fBarHitsFirstBranch; fBarHitsSecondBranch = right.fBarHitsSecondBranch; fIsPileup = right.fIsPileup;
  fchi2R = right.fchi2R; fStatR = right.fStatR;fchi2Z = right.fchi2Z; fStatZ = right.fStatZ;
  return *this;
}

void TFitHelix::Clear(Option_t *)
{
  fPoints.clear();
  if (fPoint) delete fPoint;
  fResiduals.clear();
}

TFitHelix::~TFitHelix()
{
  fPoints.clear();
  fResiduals.clear();
}


void TFitHelix::FitM2(bool fit, bool i1, bool i1b, bool i2, bool i2b)
{
  if(fNpoints<=fNpar) return;
  fStatus=0;

  int r_steps = (fit) ? fRFiterations : 1;
  int z_steps = fZFiterations;

  // Set starting values for parameters
  double* vstart = new double[fNpar];

  double chi2_1 = -99; double stat_1 = -99; ROOT::Minuit2::MnUserParameterState rf1_state;
  double chi2_1b = -99; double stat_1b = -99; ROOT::Minuit2::MnUserParameterState rf1b_state;
  double chi2_2 = -99; double stat_2 = -99; ROOT::Minuit2::MnUserParameterState rf2_state;
  double chi2_2b = -99; double stat_2b = -99; ROOT::Minuit2::MnUserParameterState rf2b_state;

  std::vector<double> r_lower_bound = {-1.0e6, -TMath::Pi(),0};
  std::vector<double> r_upper_bound = {1.0e6, TMath::Pi(), 200.};

  if (i1) {

    Initialization_Straight(vstart);

    //Minuit2 fitting for R FIT 1
    const std::vector<double> init_rfit = {vstart[0], vstart[1], vstart[2]};
    const std::vector<double> init_rerr = {vstart[0]*0.005, 0.001,vstart[2]*0.005};
    std::vector<double> r_lower_bound = {-1.0e6, -TMath::Pi(),0};
    std::vector<double> r_upper_bound = {1.0e6, TMath::Pi(), 200.};
    ROOT::Minuit2::MnUserParameters r_upar;
    r_upar.Add("fRc", init_rfit[0], init_rerr[0], r_lower_bound[0], r_upper_bound[0]);
    r_upar.Add("fphi0", init_rfit[1], init_rerr[1], r_lower_bound[1], r_upper_bound[1]);
    r_upar.Add("fD", init_rfit[2], init_rerr[2], r_lower_bound[2], r_upper_bound[2]);

    RadFuncFCN rfit1_fcn(this);
    ROOT::Minuit2::MnMigrad rfit1_minimizer(rfit1_fcn,r_upar);
    ROOT::Minuit2::FunctionMinimum rfit1_min = rfit1_minimizer(r_steps,0.1);
    rf1_state = rfit1_min.UserState();

    if (!std::isnan(chi2_1)) {
      chi2_1 = rf1_state.Fval();
      stat_1 = rf1_state.CovarianceStatus();
    }

  }

  if (i1b) {

    // Set starting values for parameters
    Initialization_Straight(vstart);
    vstart[0] *= -1;

    //Minuit2 fitting for R FIT 1b
    const std::vector<double> init_rfit1b = {vstart[0], vstart[1], vstart[2]};
    const std::vector<double> init_rerr1b = {vstart[0]*0.005, 0.001,vstart[2]*0.005};
    ROOT::Minuit2::MnUserParameters rb_upar;
    rb_upar.Add("fRc", init_rfit1b[0], init_rerr1b[0], r_lower_bound[0], r_upper_bound[0]);
    rb_upar.Add("fphi0", init_rfit1b[1], init_rerr1b[1], r_lower_bound[1], r_upper_bound[1]);
    rb_upar.Add("fD", init_rfit1b[2], init_rerr1b[2], r_lower_bound[2], r_upper_bound[2]);

    RadFuncFCN rfit1b_fcn(this);
    ROOT::Minuit2::MnMigrad rfit1b_minimizer(rfit1b_fcn,rb_upar);
    ROOT::Minuit2::FunctionMinimum rfit1b_min = rfit1b_minimizer(r_steps,0.1);
    rf1b_state = rfit1b_min.UserState();

    if (!std::isnan(chi2_1b)) {
      chi2_1b = rf1b_state.Fval();
      stat_1b = rf1b_state.CovarianceStatus();
    }

  }

  if (i2) {

    // Set starting values for parameters
    Initialization_Curved(vstart, true);

    //Minuit2 fitting for R FIT 2
    const std::vector<double> init_rfit2 = {vstart[0], vstart[1], vstart[2]};
    const std::vector<double> init_rerr2 = {vstart[0]*0.005, 0.001,vstart[2]*0.005};
    ROOT::Minuit2::MnUserParameters r2_upar;
    r2_upar.Add("fRc", init_rfit2[0], init_rerr2[0], r_lower_bound[0], r_upper_bound[0]);
    r2_upar.Add("fphi0", init_rfit2[1], init_rerr2[1], r_lower_bound[1], r_upper_bound[1]);
    r2_upar.Add("fD", init_rfit2[2], init_rerr2[2], r_lower_bound[2], r_upper_bound[2]);

    RadFuncFCN rfit2_fcn(this);
    ROOT::Minuit2::MnMigrad rfit12minimizer(rfit2_fcn,r2_upar);
    ROOT::Minuit2::FunctionMinimum rfit2_min = rfit12minimizer(r_steps,0.1);
    rf2_state = rfit2_min.UserState();

    if (!std::isnan(chi2_2)) {
      chi2_2 = rf2_state.Fval();
      stat_2 = rf2_state.CovarianceStatus();
    }
  }

  if (i2b) {

    // Set starting values for parameters
    Initialization_Curved(vstart, false);

    //Minuit2 fitting for R FIT 2
    const std::vector<double> init_rfit2b = {vstart[0], vstart[1], vstart[2]};
    const std::vector<double> init_rerr2b = {vstart[0]*0.005, 0.001,vstart[2]*0.005};
    ROOT::Minuit2::MnUserParameters r2_upar;
    r2_upar.Add("fRc", init_rfit2b[0], init_rerr2b[0], r_lower_bound[0], r_upper_bound[0]);
    r2_upar.Add("fphi0", init_rfit2b[1], init_rerr2b[1], r_lower_bound[1], r_upper_bound[1]);
    r2_upar.Add("fD", init_rfit2b[2], init_rerr2b[2], r_lower_bound[2], r_upper_bound[2]);

    RadFuncFCN rfit2b_fcn(this);
    ROOT::Minuit2::MnMigrad rfit12bminimizer(rfit2b_fcn,r2_upar);
    ROOT::Minuit2::FunctionMinimum rfit2b_min = rfit12bminimizer(r_steps,0.1);
    rf2b_state = rfit2b_min.UserState();

    if (!std::isnan(chi2_2b)) {
      chi2_2b = rf2b_state.Fval();
      stat_2b = rf2b_state.CovarianceStatus();
    }
  }

  double errR,errphi0,errD;
  double pri_1 = (stat_1>0) ? chi2_1 : 9999999999;
  double pri_1b = (stat_1b>0) ? chi2_1b : 9999999999;
  double pri_2 = (stat_2>0) ? chi2_2 : 9999999999;
  double pri_2b = (stat_2b>0) ? chi2_2b : 9999999999;
  if (rf2_state.Value(0) < fHelSmallCurvature) pri_2 *= 0.0001; // Mega prioritize curved tracks if they are seeded with Rc < 30.
  if (rf2b_state.Value(0) < fHelSmallCurvature) pri_2b *= 0.0001;

  const double prefer_straight = fHelPreferStraight;
  pri_2 *= prefer_straight;
  pri_2b *= prefer_straight;

  // ======== R FIT 1 or 2 ? ========
  if(pri_1<pri_1b and pri_1<pri_2 and pri_1<pri_2b) {
    fRc = rf1_state.Value(0);
    errR = rf1_state.Error(0);
    fphi0 = rf1_state.Value(1);
    errphi0 = rf1_state.Error(1);
    fD = rf1_state.Value(2);
    errD = rf1_state.Error(2);
    fStatR = stat_1;
    fchi2R = chi2_1;
    fBranch = 1; // figure out after z fit
    fDoubleBranched = false; // figure out after z fit
  }
  else if(pri_1b<pri_1 and pri_1b<pri_2 and pri_1b<pri_2b) {
    fRc = rf1b_state.Value(0);
    errR = rf1b_state.Error(0);
    fphi0 = rf1b_state.Value(1);
    errphi0 = rf1b_state.Error(1);
    fD = rf1b_state.Value(2);
    errD = rf1b_state.Error(2);
    fStatR = stat_1b;
    fchi2R = chi2_1b;
    fBranch = 1; // figure out after z fit
    fDoubleBranched = false; // figure out after z fit
  }
  else if (pri_2<pri_1 and pri_2<pri_1b and pri_2<pri_2b) {
    fRc = rf2_state.Value(0);
    errR = rf2_state.Error(0);
    fphi0 = rf2_state.Value(1);
    errphi0 = rf2_state.Error(1);
    fD = rf2_state.Value(2);
    errD = rf2_state.Error(2);
    fStatR = stat_2;
    fchi2R = chi2_2;
    fBranch = 1; // figure out after z fit
    fDoubleBranched = false; // figure out after z fit
  }
  else {
    fRc = rf2b_state.Value(0);
    errR = rf2b_state.Error(0);
    fphi0 = rf2b_state.Value(1);
    errphi0 = rf2b_state.Error(1);
    fD = rf2b_state.Value(2);
    errD = rf2b_state.Error(2);
    fStatR = stat_2b;
    fchi2R = chi2_2b;
    fBranch = 1; // figure out after z fit
    fDoubleBranched = false; // figure out after z fit
  }

  if (fRc==0) {
    printf("Warning: zero curvature helix!\n");
    fRc = 1;
    errR = 1;
  }
  fc = 0.5/fRc;
  ferr2Rc = errR*errR;
  ferr2c = 4.*TMath::Power(fc,4.)*ferr2Rc;
  ferr2phi0 = errphi0*errphi0;
  ferr2D = errD*errD;
  fx0=-fD*TMath::Sin(fphi0);
  fy0=fD*TMath::Cos(fphi0);

  fa=-0.299792458*TMath::Sign(1.,fc)*fB;

  // Figures out which branch, and if double branched
  int count_plus=0;
  int count_minus=0;
  for (const TSpacePoint& sp: fPoints) {
    if (GetDirection(sp.GetX(),sp.GetY(),fRc,fphi0,fD) < 0) count_plus++;
    else count_minus++;
  }
  double fraction_plus = count_plus/(1.0*count_plus+count_minus);
  if (fraction_plus>0.95) {
    fBranch = +1;
    fDoubleBranched = false;
  }
  else if (fraction_plus<0.05) {
    fBranch = -1;
    fDoubleBranched = false;
  }
  else {
    fBranch = +1; // should be irrelevant when fDoubleBranched = true
    fDoubleBranched = true;
  }

  //Minuit2 fitting for Z
  Initialization_Z(vstart);

  //Minuit2 fitting for Z
  std::vector<double> init_zfit = {vstart[3], vstart[4]};
  std::vector<double> init_zerr = {0.001+0.01*vstart[3], 1.0};
  std::vector<double> lower_bound = {-20., -30000.};
  std::vector<double> upper_bound = {20., 30000.};
  if (vstart[3]>=0) lower_bound[0] = 0.1;
  else upper_bound[0] = -0.1;

  ZedFuncFCN zfit_fcn(this);
  ROOT::Minuit2::MnUserParameters upar;
  upar.Add("fLambda", init_zfit[0], init_zerr[0], lower_bound[0], upper_bound[0]);
  upar.Add("fz0", init_zfit[1], init_zerr[1], lower_bound[1], upper_bound[1]);
  
  //ROOT::Minuit2::MnMigrad zfit_minimizer(zfit_fcn,init_zfit, init_zerr);
  ROOT::Minuit2::MnMigrad zfit_minimizer(zfit_fcn,upar);
  ROOT::Minuit2::FunctionMinimum zfit_min = zfit_minimizer(z_steps,0.1);
  ROOT::Minuit2::MnUserParameterState zf_state = zfit_min.UserState();
  
  fStatZ = zf_state.CovarianceStatus();
  fchi2Z = zf_state.Fval();

  double errlambda, errz0;
  flambda = zf_state.Value(0);
  errlambda = zf_state.Error(0);
  fz0 = zf_state.Value(1);
  errz0 = zf_state.Error(1);

  ferr2lambda = errlambda*errlambda;
  ferr2z0 = errz0*errz0;

  if (std::isnan(fphi0) or std::isnan(fD) or std::isnan(flambda) or std::isnan(fz0) or std::isnan(fchi2R) or std::isnan(fchi2Z) or std::isnan(fRc))
  {
    // Should stop the code crashing when someone tries to do something (i.e. mva dump) with failed fit helix.
    fphi0 = ALPHAg::kUnknown; fD = ALPHAg::kUnknown; flambda = ALPHAg::kUnknown; 
    fx0 = ALPHAg::kUnknown; fy0 = ALPHAg::kUnknown; fz0 = ALPHAg::kUnknown; 
    fchi2R = ALPHAg::kUnknown; fchi2Z = ALPHAg::kUnknown; fc = ALPHAg::kUnknown; 
    fRc = ALPHAg::kUnknown;
    
  }

  delete[] vstart;

}

//==============================================================================================
// use analytical straight line through first and last spacepoint
// to initialize helix canonical form
void TFitHelix::Initialization_Straight(double* Ipar)
{
  const int npoints = GetPointsArray()->size();
  const int inner_outer_range = 10;

  // Inner point
  double x1 = 0; double y1 = 0; double z1 = 0;
  int n = 0; int i = npoints - 1.;
  while (i > 2.*npoints / 3.)
  {
     const TSpacePoint& point = GetPointsArray()->at(i);
     x1 += point.GetX();
     y1 += point.GetY();
     z1 += point.GetZ();
     ++n; --i;
     if (n >= inner_outer_range)
        break;
  }
  x1 /= n;  y1 /= n;  z1 /= n;

  // Outer point
  double x2 = 0; double y2 = 0; double z2 = 0;
  n =0; i = 0;
  while (i < npoints / 3.)
  {
     const TSpacePoint& point = GetPointsArray()->at(i);
     x2 += point.GetX();
     y2 += point.GetY();
     z2 += point.GetZ();
     ++n; ++i;
     if (n >= inner_outer_range)
        break;
  }
  x2 /= n;  y2 /= n;  z2 /= n;

  //parametrizations
  const double dx = x1-x2, dy = y1-y2, dz=z1-z2,
     vr2=dx*dx+dy*dy;
  const double t = -(x2*dx+y2*dy)/vr2; // intersection with z-axis
  const double x0=dx*t+x2, y0=dy*t+y2, z0=dz*t+z2;

  const double l = -1*dz/TMath::Sqrt(vr2);
  double p0 = TMath::ATan2(y0,x0) - TMath::Pi()/2.;
  if (p0<-1*TMath::Pi()) p0+=2.*TMath::Pi();
  const double rc = 1e4; // Straight initialization
  const double D = TMath::Sqrt(x0*x0+y0*y0);

  Ipar[0]=rc;
  Ipar[1]=p0;
  Ipar[2]=D;
  Ipar[3]=l;
  Ipar[4]=z0;
  
}

//==============================================================================================
// use Menger curvature
// to initialize helix canonical form
void TFitHelix::Initialization_Curved(double* Ipar, bool sort_by_z = true)
{

  // Determine Z spread of spacepoints in track
  double min_z=0;
  double max_z=0;
  for (const TSpacePoint& p: *GetPointsArray()) {
    if (min_z==0 or p.GetZ()<min_z) min_z = p.GetZ();
    if (max_z==0 or p.GetZ()>max_z) max_z = p.GetZ();
  }

  std::vector<TSpacePoint> sorted_points = *GetPointsArray(); // Default sorting is by radius
  if (sort_by_z) {
    // Sort by z instead if the z spread is enough
    std::sort(sorted_points.begin(), sorted_points.end(), [](const TSpacePoint& p1, const TSpacePoint& p2)
      { 
        return p1.GetZ() < p2.GetZ(); 
      });

  }

  const int npoints = sorted_points.size();
  const int inner_outer_range = 10;
  const int middle_range = 4;

  
  // Inner point
  double x1 = 0; double y1 = 0; double z1 = 0;
  int n = 0; int i = npoints - 1.;
  while (i >= 2.*npoints / 3.)
  {
     const TSpacePoint& point = sorted_points.at(i);
     x1 += point.GetX();
     y1 += point.GetY();
     z1 += point.GetZ();
     ++n; --i;
     if (n >= inner_outer_range)
        break;
  }
  x1 /= n;  y1 /= n;  z1 /= n;
  const double phi1 = TMath::ATan2(y1,x1);

  //middle point
  double x1p5 = 0; double y1p5 = 0; double z1p5 = 0;
  n=0;
  i = ((npoints-1)/2.)-middle_range/2.;
  while (i <= (2.*npoints/3.) ) {
       if (i < npoints/3.){
          ++i;
          continue;
       }
     const TSpacePoint& point = sorted_points.at(i);
     x1p5 += point.GetX();
     y1p5 += point.GetY();
     z1p5 += point.GetZ();
     ++n;
     if (n >= inner_outer_range)
        break;
  }
  x1p5 /= n; y1p5 /= n; z1p5 /= n;

  // Outer point
  double x2 = 0; double y2 = 0;// double z2 = 0;
  n =0; i = 0;
  while (i <= npoints / 3.)
  {
     const TSpacePoint& point = sorted_points.at(i);
     x2 += point.GetX();
     y2 += point.GetY();
//     z2 += point.GetZ();
     ++n; ++i;
     if (n >= inner_outer_range)
        break;
  }
  x2 /= n;  y2 /= n;//  z2 /= n;

  //Menger curvature  
  const double A121p5 = fabs((x1*(y1p5 - y2) + x1p5*(y2 - y1) + x2*(y1 - y1p5))/2.);
  const double d21p5 = TMath::Sqrt((x1p5 - x2)*(x1p5 - x2) + (y1p5 - y2)*(y1p5 - y2));
  const double d1p51 = TMath::Sqrt((x1 - x1p5)*(x1 - x1p5) + (y1 - y1p5)*(y1 - y1p5));
  const double d21 = TMath::Sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
  const double kappa = 4.*A121p5/(d21p5*d1p51*d21);
  const double radius = 1./kappa;

  // Find centre of circle three points on which our three points lie
  const double centre11p5_x = (x1+x1p5)/2;
  const double centre11p5_y = (y1+y1p5)/2;
  const double centre1p52_x = (x2+x1p5)/2;
  const double centre1p52_y = (y2+y1p5)/2;
  const double tanslope11p5 = (x1-x1p5)/(y1p5-y1);
  const double tanslope1p52 = (x1p5-x2)/(y2-y1p5);

  const double ta = (centre1p52_y - centre11p5_y + (centre11p5_x - centre1p52_x) * tanslope1p52) / (tanslope11p5 - tanslope1p52);
  const double centre_x = centre11p5_x + ta;
  const double centre_y = centre11p5_y + tanslope11p5 * ta;

  // Find closest point on circle to z axis
  const double centre_r = TMath::Sqrt(centre_x*centre_x + centre_y*centre_y);
  const double closest_x = centre_x - radius * centre_x/centre_r;
  const double closest_y = centre_y - radius * centre_y/centre_r;

  const double x0 = closest_x, y0 = closest_y;

  //parametrizations
  const double dx = x1-x1p5, dy = y1-y1p5, dz=z1-z1p5,
     vr2=dx*dx+dy*dy;
  const double t = -(x1p5*dx+y1p5*dy)/vr2; // intersection with z-axis
  //const double x0=dx*t+x1p5, y0=dy*t+y1p5;
  const double z0=dz*t+z1p5;

  double r0 = TMath::Sqrt(x0*x0+y0*y0),
     l = -1*dz/TMath::Sqrt(vr2),
     phi0=TMath::ATan2(y0,x0); 

  const double r1 = TMath::Sqrt(x1*x1+y1*y1);
  const double r1p5 = TMath::Sqrt(x1p5*x1p5+y1p5*y1p5);
  if (r1p5<r1) l*=-1;

  double p0, rc, D, 
     a = phi1 - phi0; // signed difference between angles [-180,180]
  if (a<-1.*TMath::Pi()) a+=2.*TMath::Pi();
  if (a>TMath::Pi()) a-=2.*TMath::Pi();

  if ( centre_x*x0<0 and centre_y*y0<0) {
    rc = -1*(1./kappa);
  }
  else {
    rc = (1./kappa);
  }
  
  p0 = TMath::ATan2(y0,x0) - TMath::Pi()/2.;
  if (p0<-1*TMath::Pi()) p0+=2.*TMath::Pi();
  D = r0;

  Ipar[0]=rc;
  Ipar[1]=p0;
  Ipar[2]=D;
  Ipar[3]=l;
  Ipar[4]=z0;

}

void TFitHelix::Initialization_Z(double* Ipar)
{
  // Determine Z spread of spacepoints in track
  double min_z=0;
  double max_z=0;
  for (const TSpacePoint& p: *GetPointsArray()) {
    if (min_z==0 or p.GetZ()<min_z) min_z = p.GetZ();
    if (max_z==0 or p.GetZ()>max_z) max_z = p.GetZ();
  }

  // Sort by distance to closest point
  std::vector<TSpacePoint> sorted_points = *GetPointsArray(); // Default sorting is by radius
  std::sort(sorted_points.begin(), sorted_points.end(), [&](const TSpacePoint& p1, const TSpacePoint& p2)
    { 
      const double x0 = -1*fD*TMath::Sin(fphi0);
      const double y0 = fD*TMath::Cos(fphi0);
      return TMath::Sqrt((p1.GetX()-x0)*(p1.GetX()-x0) + (p1.GetY()-y0)*(p1.GetY()-y0)) < TMath::Sqrt((p2.GetX()-x0)*(p2.GetX()-x0) + (p2.GetY()-y0)*(p2.GetY()-y0)); 
    });

  int npoints = sorted_points.size();

  TSpacePoint f_point_l;
  TSpacePoint c_point_l;
  TSpacePoint f_point_r;
  TSpacePoint c_point_r;
  int n_l = 0;
  int n_r = 0;

  for (int i=0; i<npoints; i++) {
    const TSpacePoint& p = sorted_points.at(i);
    if (GetDirection(p.GetX(),p.GetY(),fRc,fphi0,fD) < 0) {
      if (n_l==0) c_point_l = p;
      n_l++;
    }
    else {
      if (n_r==0) c_point_r = p;
      n_r++;
    }
  }

  for (int i=npoints-1; i>0; i--) {
    const TSpacePoint& p = sorted_points.at(i);
    if (GetDirection(p.GetX(),p.GetY(),fRc,fphi0,fD) < 0) {
      f_point_l = p;
      break;
    }
  }

  for (int i=npoints-1; i>0; i--) {
    const TSpacePoint& p = sorted_points.at(i);
    if (GetDirection(p.GetX(),p.GetY(),fRc,fphi0,fD) > 0) {
      f_point_r = p;
      break;
    }
  }

  double lambda_l, lambda_r, c_theta_l, c_theta_r, f_theta_l, f_theta_r;

  if (n_l>5) {
    c_theta_l = GetTheta(c_point_l.GetX(),c_point_l.GetY(),fRc,fphi0,fD);
    f_theta_l = GetTheta(f_point_l.GetX(),f_point_l.GetY(),fRc,fphi0,fD);
    const double delta_theta = f_theta_l - c_theta_l;
    const double delta_sperp = delta_theta*TMath::Abs(fRc);
    const double delta_z = f_point_l.GetZ()  - c_point_l.GetZ();
    lambda_l = delta_z/delta_sperp;
  }
  if (n_r>5) {
    c_theta_r = GetTheta(c_point_r.GetX(),c_point_r.GetY(),fRc,fphi0,fD);
    f_theta_r = GetTheta(f_point_r.GetX(),f_point_r.GetY(),fRc,fphi0,fD);
    const double delta_theta = c_theta_r - f_theta_r;
    const double delta_sperp = delta_theta*TMath::Abs(fRc);
    const double delta_z = f_point_r.GetZ()  - c_point_r.GetZ();
    lambda_r = delta_z/delta_sperp;
  }
  double lambda;
  double z0;
  if (n_l>5 and n_r>5) {
    lambda = (lambda_l+lambda_r)/2.;
    z0 = c_point_l.GetZ() - (c_theta_l*TMath::Abs(fRc))*lambda;
  }
  else if (n_l>5) {
    lambda = lambda_l;
    z0 = c_point_l.GetZ() - (c_theta_l*TMath::Abs(fRc))*lambda;
  }
  else if (n_r>5) {
    lambda = lambda_r;
    z0 = c_point_r.GetZ() + (c_theta_r*TMath::Abs(fRc))*lambda;
  }
  else {
    lambda = 0.01;
    z0 = sorted_points.at(0).GetZ();
  }


  Ipar[3]=lambda;
  Ipar[4]=z0;

}

//==============================================================================================
// internal helix parameter
inline double TFitHelix::GetBeta(const double r2,const double Rc,const double D) const
{
   const double num = r2-D*D;
   if (num<0) return 0.;
   const double den = 1.+D/Rc,
   arg=num/den,
   beta = TMath::Sqrt(arg)*0.5/Rc;
   if (beta>1) return 1;
   if (beta<-1) return -1;
   return beta;
}

inline double TFitHelix::GetArcLength(const double r2, const double Rc, const double D) const
{
  return TMath::ASin( GetBeta(r2,Rc,D) ) * 2. * Rc;
}

inline double TFitHelix::GetArcLength_(const double r2, const double Rc, const double D) const
{
  return ( -1* TMath::ASin( GetBeta(r2,Rc,D) ) ) * 2. * Rc;
}

double TFitHelix::GetTheta(const double x1, const double y1, const double _Rc, const double _phi0, const double _D)
{
  const std::array<double,2> cp1 = GetClosestToPoint(x1,y1,_Rc,_phi0,_D);
  const TVector2 p1(cp1[0],cp1[1]);
  const double x0 = -1*_D*TMath::Sin(_phi0);
  const double y0 = _D*TMath::Cos(_phi0);
  const double c2 = (x0-p1.X())*(x0-p1.X()) + (y0-p1.Y())*(y0-p1.Y());
  const double theta = TMath::ACos(1 - c2/(2*_Rc*_Rc));
  return theta;
}

double TFitHelix::GetTheta(const double z1, const double _Rc, const double _lambda, const double _z0)
{
  const double twist_height = 2*TMath::Pi()*TMath::Abs(_Rc)*_lambda;
  const double num_twists = (_lambda!=0) ? (z1-_z0)/twist_height : 0;
  double theta = num_twists*(2*TMath::Pi());
  while (theta>TMath::Pi()) theta -= 2*TMath::Pi();
  while (theta<-1*TMath::Pi()) theta += 2*TMath::Pi();
  return theta;
}

// FitHelix Axial and FitVertex::FindSeed and FitVertex::Improve
double TFitHelix::GetArcLength(const double r2) const
{
  if(fBranch==1)
    return GetArcLength(r2,fRc,fD);
  else if(fBranch==-1)
    return GetArcLength_(r2,fRc,fD);
  else
    return 0;
}

// Trying std::array<double,2> instead of TVector2 for performance reasons.
// https://root.cern.ch/doc/master/classTVector2.html says TVector2 is slow and deprecated.
// Indeed, this gave a 50% speed improvement
std::array<double,2> TFitHelix::GetClosestToPoint(const double x0, const double y0, const double _Rc, const double _phi0, const double _D)
{
  const double u0 = TMath::Cos(_phi0);
	const double v0 = TMath::Sin(_phi0);
	const double Rc = TMath::Abs(_Rc);
	const double D = TMath::Abs(_D);
	const double xc = (_Rc+D)*-1*v0; // Centre of helix
	const double yc = (_Rc+D)*u0;

  const double dxc = x0 - xc; // Distance from centre to spacepoint
  const double dyc = y0 - yc;
  const double dc = TMath::Sqrt(dxc*dxc+dyc*dyc);
  const double cpx = xc + Rc*(dxc/dc); // Closest point on helix
  const double cpy = yc + Rc*(dyc/dc);
  std::array<double,2> cp = {cpx,cpy};

  return cp;
}

TVector3 TFitHelix::GetClosestToPoint(const double x1, const double y1, const double z1, const double _Rc, const double _phi0, const double _D, const double _lambda, const double _z0)
{
  const std::array<double,2> XY = GetClosestToPoint(x1,y1,_Rc,_phi0,_D);
  const TVector2 closestXY(XY[0],XY[1]);
  const TVector2 p0(_D*-1*TMath::Sin(_phi0),_D*TMath::Cos(_phi0));
  double theta = GetTheta(x1,y1,_Rc,_phi0,_D);

  if (closestXY.DeltaPhi(p0)>0) theta*=-1;
  const double signed_sperp = theta*TMath::Abs(_Rc);
  const double one_possible_z = _z0 + _lambda*signed_sperp;

  const double dz = z1-one_possible_z;
  const double twist_height = 2*TMath::Pi()*TMath::Abs(_Rc)*_lambda;
  const double num_twists = (_lambda!=0) ? dz/twist_height : 0;
  int int_num_twists = (int)std::round(num_twists);
  const int max_twists = 5;
  if (int_num_twists>max_twists) int_num_twists = max_twists;
  if (int_num_twists<-1*max_twists) int_num_twists = -1*max_twists;
  const double best_z = one_possible_z + int_num_twists*twist_height;

  return TVector3(closestXY.X(),closestXY.Y(),best_z);
}

TVector3 TFitHelix::GetClosestToPoint(const double x1, const double y1, const double z1) const {
  const TVector3 closest = GetClosestToPoint(x1,y1,z1,fRc,fphi0,fD,flambda,fz0);
  if (GetDirection(closest.X(),closest.Y(),fRc,fphi0,fD)*fBranch < 0 or fDoubleBranched) return closest;
  else if (GetTheta(x1,y1,fRc,fphi0,fD)<=TMath::Pi()/2.) return TVector3(fx0,fy0,fz0);
  else {
    const double x2 = (2*fRc+fD)*-1*TMath::Sin(fphi0);
    const double y2 = (2*fRc+fD)*TMath::Cos(fphi0);
    const double z2 = fz0 + TMath::Pi()*TMath::Abs(fRc)*flambda;
    return TVector3(x2,y2,z2);
  }
}

int TFitHelix::GetDirection(const double x1, const double y1, const double _Rc, const double _phi0, const double _D) {
  const std::array<double,2> cp1 = GetClosestToPoint(x1,y1,_Rc,_phi0,_D);
  const TVector2 p1(cp1[0],cp1[1]);
  const TVector2 p0(_D*-1*TMath::Sin(_phi0),_D*TMath::Cos(_phi0));
  const double delta_phi = p1.DeltaPhi(p0);
  if (delta_phi>=0) return 1;
  else return -1;

}

TVector2 TFitHelix::GetPointAtZ( const double z1, const double _Rc, const double _phi0, const double _D, const double _lambda, const double _z0) {
  const double theta = GetTheta(z1, _Rc, _lambda, _z0);
  const double beta = TMath::Sin(TMath::Abs(theta)/2.);
  const double u0 = TMath::Cos(_phi0);
  const double v0 = TMath::Sin(_phi0);
  const double x0 = -1*_D*v0;
  const double y0 = _D*u0;
  const double Rc = TMath::Abs(_Rc);
  //printf("x0 %.1f y0 %.1f xc %.1f yc %.1f theta %.5f xx %.3f yy %.3f\n",x0, y0, x0-v0*_Rc*2*beta*beta, y0+u0*_Rc*2*beta*beta, theta, u0*_Rc*2*beta*TMath::Sqrt(1-beta*beta),v0*_Rc*2*beta*TMath::Sqrt(1-beta*beta));
  if (theta>0) return TVector2(x0 + u0*Rc*2*beta*TMath::Sqrt(1-beta*beta) - v0*Rc*2*beta*beta, y0 + v0*Rc*2*beta*TMath::Sqrt(1-beta*beta) + u0*Rc*2*beta*beta);
  else return TVector2(x0 - u0*Rc*2*beta*TMath::Sqrt(1-beta*beta) - v0*Rc*2*beta*beta, y0 - v0*Rc*2*beta*TMath::Sqrt(1-beta*beta) + u0*Rc*2*beta*beta);
}

//==============================================================================================
// FitHelix Radial for +1 Branch
inline Vector2 TFitHelix::Evaluate(const double r2,const double Rc,const double phi,const double D) const
{
  const double u0 = TMath::Cos(phi),
    v0 = TMath::Sin(phi);
  return Evaluate( r2, Rc, u0, v0, D);
}

// FitHelix Radial for +1 Branch
Vector2 TFitHelix::Evaluate(const double r2,const double Rc,const double u0,const double v0,const double D) const
{
  const double x0 = -D*v0,
    y0 = D*u0,
    beta = GetBeta(r2, TMath::Abs(Rc), D);
  const double beta2 = beta*beta;
  return { x0 + u0 * beta * TMath::Sqrt(1.-beta2) * 2. * Rc - v0 * beta2 * 2. * Rc,
        y0 + v0 * beta * TMath::Sqrt(1.-beta2) * 2. * Rc + u0 * beta2 * 2. * Rc};
}

// FitHelix Radial for -1 Branch
inline Vector2 TFitHelix::Evaluate_(const double r2,const double Rc,const double phi,const double D) const
{
  const double u0 = TMath::Cos(phi),
    v0 = TMath::Sin(phi);
  return Evaluate_( r2, Rc, u0, v0, D);
}
// FitHelix Radial for -1 Branch
Vector2 TFitHelix::Evaluate_(const double r2,const double Rc,const double u0,const double v0, const double D) const
{
  const double x0 = -D*v0,
    y0 = D*u0,
    beta = GetBeta(r2, Rc, D);
  const double beta2 = beta*beta;
  return { x0 - u0 * beta * TMath::Sqrt(1.-beta2) * 2. * Rc - v0 * beta2 * 2. * Rc,
        y0 - v0 * beta * TMath::Sqrt(1.-beta2) * 2. * Rc + u0 * beta2 * 2. * Rc};
}

// FitHelix Axial
double TFitHelix::Evaluate(const double s, const double l, const double z0) const
{
  return z0 + l * s;
}

//===============================================================================================

// Draw routine
TVector3 TFitHelix::Evaluate(const double r2) const
{
  const double max_r2 = (fRc>0) ? (fD+2*fRc)*(fD+2*fRc) : ((-1*fRc>fD) ? fRc*fRc : fD*fD);
  const double min_r2 = (fRc>0) ? fD*fD : 0;
  if (r2<min_r2 or r2>max_r2) {return TVector3(ALPHAg::kUnknown, ALPHAg::kUnknown, ALPHAg::kUnknown);}
  const double s= GetArcLength(r2);
  Vector2 r;
  if (fBranch==+1) {r=Evaluate(r2, fRc, fphi0, fD);}
  else {r=Evaluate_(r2, fRc, fphi0, fD);}
  return TVector3( r.X, r.Y, Evaluate(s,flambda,fz0) );
}

// FitVertex
TVector3 TFitHelix::EvaluateErrors2(const double r2) const 
{
  const double max_r2 = (fRc>0) ? (fD+2*fRc)*(fD+2*fRc) : ((-1*fRc>fD) ? fRc*fRc : fD*fD);
  const double min_r2 = (fRc>0) ? fD*fD : 0;
  if (r2<min_r2 or r2>max_r2) return TVector3(ALPHAg::kUnknown, ALPHAg::kUnknown, ALPHAg::kUnknown);
  const double beta = GetBeta(r2,fRc,fD),
    beta2 = beta*beta,
    bb = beta*TMath::Sqrt(1.-beta2),
    cp=TMath::Cos(fphi0),
    sp=TMath::Sin(fphi0),
    c2=fc*fc,
    eps=(double) fBranch;

  const double dxdc = -eps*bb*cp/c2 +beta2*sp/c2 ,
    dxdphi = -fD*cp -beta2*cp/fc -eps*bb*sp/fc,
    dxdD = -sp,
    dydc = -eps*bb*sp/c2 -beta2*cp/c2,
    dydphi = -fD*sp -beta2*sp/fc +eps*bb*cp/fc,
    dydD = cp,
    dzdl = GetArcLength(r2);

  return TVector3(dxdc*dxdc*ferr2c + dxdphi*dxdphi*ferr2phi0 + dxdD*dxdD*ferr2D,
      dydc*dydc*ferr2c + dydphi*dydphi*ferr2phi0 + dydD*dydD*ferr2D,
      dzdl*dzdl*ferr2lambda + ferr2z0);
}

// FitVertex
TVector3 TFitHelix::GetPosition(const double s) const
{
  const double rho=2.*fc,
    cp=TMath::Cos(fphi0),sp=TMath::Sin(fphi0),
    crs=TMath::Cos(rho*s),srs=TMath::Sin(rho*s);

  return TVector3(fx0+cp*srs/rho-sp*(1.-crs)/rho,
       fy0+sp*srs/rho+cp*(1.-crs)/rho,
       fz0+flambda*s);
}

// FitVertex::EvaluateMeanPointError2
TVector3 TFitHelix::GetError2(double s) const
{
  const double rho=2.*fc,
    cp=TMath::Cos(fphi0),sp=TMath::Sin(fphi0),
    crs=TMath::Cos(rho*s),srs=TMath::Sin(rho*s);

  const double dxdc = s*crs*cp/fc - cp*srs/rho/fc + (1.-crs)*sp/rho/fc - s*srs*sp/fc,
    dxdp = -fy0 - (1.-crs)*cp/rho -srs*sp/rho,
    dxdD = -sp,
    dydc = -(1.-crs)*cp/rho/fc + s*cp*srs/fc + s*crs*sp/fc - srs*sp/rho/fc,
    dydp = fx0 + cp*srs/rho - (1.-crs)*sp/rho,
    dydD = cp;
  return TVector3(dxdc*dxdc*ferr2c + dxdp*dxdp*ferr2phi0 + dxdD*dxdD*ferr2D,
      dydc*dydc*ferr2c + dydp*dydp*ferr2phi0 + dydD*dydD*ferr2D,
      s*s*ferr2lambda + ferr2z0);
}

double TFitHelix::CalculateResiduals() {
  fResiduals2=0.;
  fResidual.SetXYZ(0.,0.,0.);
  fResiduals.clear();
  #if USE_MAPS
  fResidualsRadii.clear();
  fResidualsPhi.clear();
  fResidualsXY.clear();
  fRResidualsXY.clear();
  fZResidualsXY.clear();
  #endif
  int npoints=fPoints.size();

  for(int i=0; i<npoints; ++i)
    {
      const TSpacePoint& aPoint = fPoints.at(i);
      TVector3 p(aPoint.GetX(),
		    aPoint.GetY(),
		    aPoint.GetZ());
      const double r = aPoint.GetR();

      TVector3 res( p-GetClosestToPoint(aPoint.GetX(),aPoint.GetY(),aPoint.GetZ(),fRc,fphi0,fD,flambda,fz0) );

      fResidual += res; 

      const double resmag = res.Mag();
      fResiduals.push_back( resmag );
      const double res_r = res.Perp();
      const double res_z = res.z();

      #if USE_MAPS
      fResidualsRadii.insert( std::pair<double,double>( r, resmag ) );
      fResidualsPhi.insert( std::pair<double,double>( aPoint.GetPhi(), resmag ) );
      fResidualsXY.insert( std::pair<std::pair<double,double>,double>
			   (std::pair<double,double>( aPoint.GetX(),
						      aPoint.GetY()),
			    resmag ) ); 
      fRResidualsXY.insert( std::pair<std::pair<double,double>,double>
			   (std::pair<double,double>( aPoint.GetX(),
						      aPoint.GetY()),
			    res_r ) ); 
      fZResidualsXY.insert( std::pair<std::pair<double,double>,double>
			   (std::pair<double,double>( aPoint.GetX(),
						      aPoint.GetY()),
			    res_z ) ); 
      #endif
      fResiduals2 += res.Mag2();

      const TVector2 pointatz = GetPointAtZ(p.z(),fRc,fphi0,fD,flambda,fz0);

      //printf("point at (%.1f, %.1f, %.1f) dz %.1f point at z (%.1f, %.1f) dx %.1f dy %.1f\n",p.X(),p.Y(),p.Z(),res.z(),pointatz.X(),pointatz.Y(), pointatz.X()-p.X(), pointatz.Y()-p.Y());

    }
  return fResiduals2;

}

//==============================================================================================
double TFitHelix::Momentum()
{
  if( fc == 0. || fRc == 0. ) 
    {
      std::cerr<<"TFitHelix::Momentum() Error curvature is 0"<<std::endl;
      return -1.;
    }
  double coeff = fa*fRc,
    px=coeff*TMath::Cos(fphi0), // MeV/c
    py=coeff*TMath::Sin(fphi0),
    pz=coeff*flambda;
  //  std::cout<<"TFitHelix::Momentum() coeff (a/2c=a*Rc) is "<<coeff<<std::endl;
  fMomentum.SetXYZ(px,py,pz);
  double pT = fMomentum.Perp();
  double errc = TMath::Sqrt(ferr2c), errphi0 = TMath::Sqrt(ferr2phi0), errlambda = TMath::Sqrt(ferr2lambda);
  fMomentumError.SetXYZ(-px*errc/fc-py*errphi0,-py*errc/fc+px*errphi0,-pz*errc/fc+pT*errlambda);
  return pT;
}

double TFitHelix::GetApproxPathLength() const
{
  TVector3 r1(Evaluate(ALPHAg::_cathradius*ALPHAg::_cathradius));
  TVector3 r2(Evaluate(ALPHAg::_trapradius*ALPHAg::_trapradius));
  return TMath::Abs(r1.Mag()-r2.Mag());
}

double TFitHelix::VarMS() const
{
  // Multiple scattering causes the particle to scatter in the two planes
  // perpendicular to its path without losing energy. The distribution of
  // each angle is gaussian with std.dev.
  //          0.0141         L
  // sigma = -------- sqrt( --- )
  //          p beta         X
  // where p is in GeV/c, beta its velocity, L path length and X radiation length

  double p2 = fMomentum.Mag2()*1.e-6, // (GeV/c)^2
     E2 = ALPHAg::_ChargedPionMass*ALPHAg::_ChargedPionMass+fMomentum.Mag2(),
    beta2 = fMomentum.Mag2()/E2,
    L=GetApproxPathLength();

  // return sigma^2 variance
  return 1.9881e-4*L/p2/beta2/ALPHAg::_RadiationLength;
}

void TFitHelix::AddMSerror()
{
  TMatrixDSym W(4);
  double weights[]={
    1.,  0.,  0.5,   0.,
    0.,  1.,  0.,    0.5,
    0.5, 0.,  1./3., 0.,
    0.,  0.5, 0.,    1./3.};
  W.Use(4,weights);

  double r=ALPHAg::_trapradius,
    r2=r*r,
    s=GetArcLength(r2),
    D2=fD*fD,
    cD=fc*fD,
    rho=2.*fc,
    lambda2=flambda*flambda,
    sintheta=1./TMath::Sqrt(1.+lambda2),
    costheta=flambda*sintheta;

  if(fMomentum.Mag()==0.0)
    Momentum();

  double pT=fMomentum.Perp(),
    HL = VarMS(),
    p=fMomentum.Mag(),
    T=pT*(1.+2.*cD),
    T2=T*T,
    pz=fMomentum.Z(),
    sina=r*fc-fD*(1.+cD)/r,
    cosa=TMath::Sqrt( (1.-D2/r2)*((1.+cD)*(1.+cD)-r2*fc*fc) );
  if( TMath::IsNaN(cosa) ) cosa=0.;
  else if(fBranch==-1) cosa*=-1.;

  TMatrixD M(5,4);
  double MSeffect[]={
    0.,                               0.5*fa*flambda/pT,         0.,                     0.,
    p*pT*(1.-rho*r*sina)/T2,          fa*flambda*pT*r*cosa/T2,   fa*p*r*cosa/T2,         fa*pz*sintheta*(1.-rho*r*sina)/T2,
    -p*r*cosa/T,                      -0.5*fa*flambda*(r2-D2)/T, p*(1.-rho*r*sina)/T,    fa*r*costheta*cosa/T,
    0.,                               -p*p/pT/pT,                0.,                     0.,
    -flambda*p*pT*(fD+fc*(D2+r2))/T2, s+lambda2*pT*pT*r*cosa/T2, fa*flambda*p*r*cosa/T2, sintheta*(-T2+pz*pz*(1.-rho*r*sina))/T2
  };

  M.Use(5,4,MSeffect);

  TMatrixD A(W,TMatrixD::kMultTranspose,M); // A = W*M^T
  TMatrixD V(M,TMatrixD::kMult,A); // V = M*A
  V*=HL; // V = (HL) M*W*M^T

  TMatrixDDiag Vdiag(V);
    ferr2c     +=Vdiag(0);
  ferr2Rc     = 4.*TMath::Power(fRc,4.)*ferr2c;
    ferr2phi0  +=Vdiag(1);
    ferr2D     +=Vdiag(2);
    ferr2lambda+=Vdiag(3);
    ferr2z0    +=Vdiag(4);
}

//==============================================================================================
int TFitHelix::TubeIntersection( TVector3& pos1,  TVector3& pos2, const double radius) const
{
  if( TMath::Abs(fD) < radius )
    {
      double beta = GetBeta(radius*radius,fRc,fD),
  s1, s2;
      if( fBranch == 1 )
  {
    s1 = TMath::ASin(beta)*2.*fRc;
    s2 = TMath::ASin(-beta)*2.*fRc;
  }
      else if( fBranch == -1 )
  {
    s1 = (TMath::Pi()-TMath::ASin(beta))*2.*fRc;
    s2 = (TMath::Pi()-TMath::ASin(-beta))*2.*fRc;
  }
      else
  {
    std::cerr<<"TFitHelix::TubeIntersection FAIL type: Unknown Branch Type "<<fBranch<<std::endl;
    return -1;
  }
      pos1 = GetPosition(s1);
      if( !TMath::AreEqualRel( pos1.Perp(), radius, 1.e-4) )
  {
    double err = TMath::Abs(pos1.Perp()-radius),
      avg = 0.5*(TMath::Abs(pos1.Perp())+TMath::Abs(radius));
    double rel = err/avg;
    std::cerr<<"TFitHelix::TubeIntersection FAIL type: position 1 radius: "<<pos1.Perp()<<" differs from parameter: "<<radius<<" by "<<rel<<std::endl;
  }
      pos2 = GetPosition(s2);
      if( !TMath::AreEqualRel( pos2.Perp(), radius, 1.e-4) )
  {
    double err = TMath::Abs(pos2.Perp()-radius),
      avg = 0.5*(TMath::Abs(pos2.Perp())+TMath::Abs(radius));
    double rel = err/avg;
    std::cerr<<"TFitHelix::TubeIntersection FAIL type: position 2 radius: "<<pos2.Perp()<<" differs from parameter: "<<radius<<" by "<<rel<<std::endl;
  }
      return 1;
    }
  else
    {
      pos1.SetXYZ(fx0,fy0,fz0);
      pos2.SetXYZ(fx0,fy0,fz0);
      return 0;
    }
}


static TMinuit* hel2pnt=0;
void Hel2PntFunc(int&, double*, double& chi2, double* p, int)
{
  TFitHelix* fitObj = (TFitHelix*) hel2pnt->GetObjectFit();
  const TVector3* pnt  = fitObj->GetPoint();
  const TVector3 h  = fitObj->GetPosition( p[0] );
  //  TVector3 e2 = fitObj->EvaluateErrors2( h.Perp2() );
  const TVector3 e2 = fitObj->GetError2(p[0]);
  const double tx=pnt->X()-h.X(), ty=pnt->Y()-h.Y(), tz=pnt->Z()-h.Z();
  chi2 = tx*tx/e2.X() + ty*ty/e2.Y() + tz*tz/e2.Z() ;
  return;
}

//==============================================================================================
bool TFitHelix::IsGood()
{
  // make sure that the fit succeeded
  if( fStatR <= 0 ) fStatus=-2;
  else if( fStatZ <= 0 ) fStatus=-3;
  // do not search for the vertex with bad helices
  else if( (fchi2R/(double) GetRDoF()) <=fChi2RMin)  fStatus=-14;
  else if( (fchi2R/(double) GetRDoF()) > fChi2RCut ) fStatus=-4;
  else if( (fchi2Z/(double) GetZDoF()) <=fChi2ZMin ) fStatus=-15;
  else if( (fchi2Z/(double) GetZDoF()) > fChi2ZCut ) fStatus=-5;
  else if( TMath::Abs(fD)  > fDCut && kDcut )        fStatus=-6;
  else if( TMath::Abs(fc)  > fcCut && kccut )        fStatus=-8;
  else if( fMomentum.Perp() < fpCut && kpcut )       fStatus=-9;
  else if( fNpoints < fPointsCut )                   fStatus=-11;
  else fStatus=1;

  //  std::cout<<"TFitHelix::Status = "<<fStatus<<std::endl;

  return fStatus>0;
}

bool TFitHelix::IsGoodChiSquare()
{
  // make sure that the fit succeeded
  if( fStatR <= 0 ) fStatus=-2;
  else if( fStatZ <= 0 ) fStatus=-3;
  //else if( (fchi2R/(double) GetRDoF()) <=fChi2RMin)  fStatus=-14;
  else if( (fchi2R/(double) GetRDoF()) > fChi2RCut ) fStatus=-4;
  //else if( (fchi2Z/(double) GetZDoF()) <=fChi2ZMin ) fStatus=-15;
  else if( (fchi2Z/(double) GetZDoF()) > fChi2ZCut ) fStatus=-5; 
  else if( fNpoints < fPointsCut )                   fStatus=-11;
  else fStatus=4;

  //  std::cout<<"TFitHelix::Status = "<<fStatus<<std::endl;

  return fStatus>0?true:false;
}
void TFitHelix::Reason()
{
  std::cout<<"  TFitHelix::Reason() Status: "<<GetStatus()<<"\t";
  double chi2R = fchi2R/(double) GetRDoF(), chi2Z = fchi2Z/(double) GetZDoF();
  switch( fStatus )
   {
   case -2:
     std::cout<<"R Fit Cov. Matrix stat: "<<fStatR
        <<"\tZ Fit Cov. Matrix stat: "<<fStatZ<<std::endl;
     break;
   case -3:
     std::cout<<"R Fit Cov. Matrix stat: "<<fStatR
        <<"\tZ Fit Cov. Matrix stat: "<<fStatZ<<std::endl;
     break;
   case -14:
     std::cout<<"R Fit chi^2: "<<chi2R
        <<"\tZ Fit chi^2: "<<chi2Z<<std::endl;
     break;
   case -4:
     std::cout<<"R Fit chi^2: "<<chi2R
        <<"\tZ Fit chi^2: "<<chi2Z<<std::endl;
     break;
  case -15:
    std::cout<<"R Fit chi^2: "<<chi2R
        <<"\tZ Fit chi^2: "<<chi2Z<<std::endl;
     break;
  case -5:
     std::cout<<"R Fit chi^2: "<<chi2R
        <<"\tZ Fit chi^2: "<<chi2Z<<std::endl;
     break;
   case -6:
       std::cout<<"D = "<<fD<<" mm [cut:"<<fDCut<<"]"<<std::endl;
       break;
   case -8:
       std::cout<<"c = "<<fc<<" mm^-1 [cut:"<<fcCut<<"]"<<std::endl;
       break;
   case -9:
       std::cout<<"pT = "<<fMomentum.Perp()<<" MeV [cut:"<<fpCut<<"]"<<std::endl;
       break;
   case -11:
     std::cout<<"Too few Points..."<<std::endl;
     break;
   default:
     std::cout<<"\n";
   }
}

bool TFitHelix::IsDuplicated(const TFitHelix* right, const double cut) const
{
  if( TMath::AreEqualAbs(fMomentum.Phi(),right->GetMomentumV().Phi(),cut) &&
      TMath::AreEqualAbs(fMomentum.Theta(),right->GetMomentumV().Theta(),cut) )
    return true;
  else
    return false;
}

int TFitHelix::Compare(const TObject* aHelix) const
{
  if(TMath::Abs(fc) < TMath::Abs(((TFitHelix*)aHelix)->fc)) return -1;
  else if(TMath::Abs(fc) > TMath::Abs(((TFitHelix*)aHelix)->fc)) return 1;
  else return 0;
}

bool SortMomentum(const TFitHelix& aHelix, const TFitHelix& bHelix)
{
  return (TMath::Abs(aHelix.GetC()) > TMath::Abs(bHelix.GetC()));
}

//==============================================================================================
void TFitHelix::Print(Option_t*) const
{
  std::cout<<" *** TFitHelix ***"<<std::endl;
  std::cout<<"# of points: "<<fNpoints<<std::endl;
  std::cout<<" ("<<std::setw(5)<<std::left<<GetX0()
     <<", "<<std::setw(5)<<std::left<<GetY0()
     <<", "<<std::setw(5)<<std::left<<fz0<<")\n"
     <<" Rc = "<<std::setw(5)<<std::left<<fRc
     <<" Phi0 = "<<std::setw(5)<<std::left<<fphi0
     <<"    D = "<<std::setw(5)<<std::left<<fD
     <<"    L = "<<std::setw(5)<<std::left<<flambda
     <<std::endl;
  std::cout<<" a = "<<fa<<std::endl;
  std::cout<<" c = "<<fc<<std::endl;
  std::cout<<" Branch : "<<fBranch<<std::endl;
  std::cout<<" Radial Chi2 = "<<fchi2R
     <<"\t ndf = "<<GetRDoF()
     <<"\t cov stat = "<<fStatR
     <<std::endl;
  std::cout<<"  Axial Chi2 = "<<fchi2Z
     <<"\t ndf = "<<GetZDoF()
     <<"\t cov stat = "<<fStatZ
     <<std::endl;
  if(fMomentum.Mag()!=0.0)
    {
      std::cout<<" Momentum = ("
         <<std::setw(5)<<std::left<<fMomentum.X()<<", "
         <<std::setw(5)<<std::left<<fMomentum.Y()<<", "
         <<std::setw(5)<<std::left<<fMomentum.Z()<<") MeV/c"<<std::endl;
      std::cout<<" |p| = "<<fMomentum.Mag()
         <<" MeV/c\t pT = "<<fMomentum.Perp()<<" MeV/c"<<std::endl;
    }
  if(fResidual.Mag()!=0.0)
    std::cout<<"  Residual = ("
       <<std::setw(5)<<std::left<<fResidual.X()
       <<", "<<std::setw(5)<<std::left<<fResidual.Y()
       <<", "<<std::setw(5)<<std::left<<fResidual.Z()<<") mm"<<std::endl;
  if(fResiduals2!=0.0) 
    std::cout<<"  Residuals Squared = "<<fResiduals2<<" mm^2"<<std::endl;
  if(fParticle!=0)
    std::cout<<"PDG code "<<fParticle<<std::endl;
  std::cout<<" Status: "<<GetStatus()<<std::endl;
  std::cout<<"--------------------------------------------------------------------------"<<std::endl;

}

ClassImp(TFitHelix)

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
