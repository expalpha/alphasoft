#include "TRecoHelixFitter.hh"

int TRecoHelixFitter::FitHelix(const std::vector<TTrack> TracksArray, std::vector<TFitHelix> &HelixArray, const int thread_no, const int total_threads) const
{

   if (thread_no == 1)
   {
      HelixArray.clear();
      HelixArray.reserve(TracksArray.size());
   }

   int n = HelixArray.size();
   if (TracksArray.size()==0) return 0;

   const float slice_size = TracksArray.size() / (float)total_threads;
   const int eff_thread_no = ((thread_no -1 + TracksArray.at(0).GetNumberOfPoints())%total_threads)+1;
   // If we use thread_number, then the first and second threads will do most of the work.
   // Use the number of points in the first helix as a pseudorandom number to split the load more equally.
   const int start = floor(slice_size*(eff_thread_no - 1));
   int stop = floor( slice_size * eff_thread_no );
   //I am the last thread
   if (eff_thread_no == total_threads)
      stop = TracksArray.size();

   for (int it = start; it < stop; ++it)
   {
      HelixArray.emplace_back(TracksArray[it]);// Copies TTrack
      TFitHelix& helix = HelixArray.back();
      helix.SetChi2ZCut(fHelChi2ZCut);
      helix.SetChi2RCut(fHelChi2RCut);
      helix.SetChi2RMin(fHelChi2RMin);
      helix.SetChi2ZMin(fHelChi2ZMin);
      helix.SetDCut(fHelDcut);
      helix.SetcCut(fHelCcut);
      helix.SetPointsCut(fPointsCut);
      helix.SetPointMaxRad(fPointMaxRad);
      helix.SetRFiterations(fHelRFiterations);
      helix.SetZFiterations(fHelZFiterations);
      helix.SetHelPreferStraight(fHelPreferStraight);
      helix.SetHelSmallCurvature(fHelSmallCurvature);
      helix.FitM2();
      if (helix.GetStatR() > 0 && helix.GetStatZ() > 0) helix.CalculateResiduals();

      helix.IsGood();
      double pt = helix.Momentum();
      //printf("Final z0 = %.0f\n",helix.GetZ0());
      helix.SetTrackNum(it+1);
      if (fTrace) {
	helix.Print();
	std::cout << "Reco::FitHelix()  hel # " << n << " p_T = " << pt
		  << " MeV/c in B = " << helix.GetMagneticField() << " T" << std::endl;
      }
      ++n;
   }

   if (n != (int)HelixArray.size())
      std::cerr << "Reco::FitHelix() ERROR number of lines " << n << " differs from array size " << HelixArray.size()
                << std::endl;
   return n;
}
