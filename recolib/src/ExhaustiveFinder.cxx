// Tracks finder class implementation
// for ALPHA-g TPC analysis
// Author: A.Capra, G.Smith
// Date: Sep. 2016, Oct. 2022

#include "TPCconstants.hh"
#include "ExhaustiveFinder.hh"
#include <iostream>
#include "TSeqCollection.h"

ExhaustiveFinder::ExhaustiveFinder(const std::vector<TSpacePoint>* points, const double TouchDistanceXY, const double TouchDistanceZ):
   TracksFinder(points),
   fTouchDistanceXY(TouchDistanceXY),
   fTouchDistanceZ(TouchDistanceZ)
{
   if( debug )
      std::cout<<"ExhaustiveFinder::ExhaustiveFinder ctor!"<<std::endl;
}

//==============================================================================================

int ExhaustiveFinder::MapConnections()
{

   // Clear connection map
   fConnected.clear();
   fHitGood.clear();
   int Npoints = (int)fPointsArray.size();
   if (Npoints<=0)
      return -1;
   fHitGood = std::vector<bool>(Npoints,false);
   for (int ip=0; ip<Npoints; ip++) {
      fConnected.push_back(std::vector<bool>(Npoints,false));
   }


   // Double loop over good spacepoints. O(N^2) but I doubt this is the bottleneck.
   for (int ip=0; ip<Npoints; ip++) {
      TSpacePoint* p = fPointsArray[ip];
      if (!p) continue;
      if( !p->IsGood(ALPHAg::_cathradius, fPointMaxRad) )  continue;
      fHitGood[ip] = true;

      double px = p->GetX();
      double py = p->GetY();
      double pz = p->GetZ();

      for (int jp=ip; jp<Npoints; jp++) { // Start at first iterator, use symmetry to do half the work
         TSpacePoint* q = fPointsArray[jp];
         if (!q) continue;
         if( !q->IsGood(ALPHAg::_cathradius, fPointMaxRad) )  continue;

         double qx = q->GetX();
         double qy = q->GetY();
         double qz = q->GetZ();

         // Fill connection map
         double distXY = TMath::Sqrt((px-qx)*(px-qx) + (py-qy)*(py-qy));
         double distZ = TMath::Abs(pz-qz);

         if (distXY < fTouchDistanceXY and distZ < fTouchDistanceZ) {
            fConnected[ip][jp] = true;
            fConnected[jp][ip] = true;
         }

      }
   }   

   return 0;

}

int ExhaustiveFinder::AddConnectedPoints(track_t& track, int i_point, int Npoints)
{
   track.push_back(i_point);
   fHitUsed[i_point] = true;
   int n_added = 1;
   for (int j=0; j<Npoints; j++) {
      if (!fHitGood[j]) continue;
      if (fHitUsed[j]) continue;
      if (!fConnected[i_point][j]) continue;
      n_added += AddConnectedPoints(track, j, Npoints);
   }
   return n_added;
}

int ExhaustiveFinder::RecTracks(std::vector<track_t>& TrackVector)
{
   int Npoints = fPointsArray.size();
   if( Npoints<=0 )
      return -1;
   if( debug )
      std::cout<<"ExhaustiveFinder::ExhaustiveFinder() # of points: "<<Npoints<<std::endl;

   // Fill the connection map
   int map_status = MapConnections();
   if (debug)
      std::cout<<"ExhaustiveFinder::MapConnections() finished with status: "<<map_status<<std::endl;

   // Pattern Recognition algorithm
   int i_track = 0;
   fHitUsed.clear();
   fHitUsed = std::vector<bool>(Npoints,false);

   for(int i=0; i<Npoints; ++i)
      {

         if (!fHitGood[i]) continue;
         if (fHitUsed[i]) continue;

         track_t vector_points;
         vector_points.clear();

         int n_added = AddConnectedPoints(vector_points,i,Npoints);
         if (debug)
            std::cout<<"ExhaustiveFinder::RecTracks() found track with "<<n_added<<" points."<<std::endl;

         if( int(vector_points.size()) < fNpointsCut )
            {
               ++points_cut;
               for (auto& it: vector_points) {
                  fPointsArray[it]->SetTrackFinderStatus(failed_points_cut);
               }
               continue;
            }
         else
            {
               i_track++;

               TrackVector.push_back( vector_points );
               for(auto& it: vector_points)
                  {
                     fPointsArray[it]->SetRecoTrackID(i_track);
                     fPointsArray[it]->SetTrackFinderStatus(passed);
                  }
               ++fNtracks;
            }
      }//i loop
   if( fNtracks != int(TrackVector.size()) )
      std::cerr<<"ExhaustiveFinder::ExhaustiveFinder(): Number of found tracks "<<fNtracks
               <<" does not match the number of entries "<<TrackVector.size()<<std::endl;
   else if( debug )
      {
         std::cout<<"ExhaustiveFinder::ExhaustiveFinder(): Number of found tracks "<<fNtracks<<std::endl;
         std::cout<<"ExhaustiveFinder::ExhaustiveFinder() -- Reasons: Track Not Advancing "<<track_not_advancing
                  <<" Points Cut: ("<<fNpointsCut<<"): "<<points_cut<<std::endl;
      }

   return fNtracks;
}


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
