#include "TRecoVertexFitter.hh"

vertexstatus_t TRecoVertexFitter::RecVertex(std::vector<TFitHelix>& HelixArray, TFitVertex* Vertex, const int thread_no, const int thread_count)
{
   // sort the helices by |c|, so that lowest |c| (highest momentum) is first
   if (thread_no == 1)
   {
      std::sort(HelixArray.begin(),HelixArray.end(),SortMomentum);
      Vertex->SetChi2Cut( fVtxChi2Cut );
      Vertex->SetFitIterations( (unsigned int)fVtxFitIterations );
      for (TFitHelix& hel: HelixArray)
      {
         if( hel.IsGood() )
         {
            if (fRequireBV and !hel.HasBarHit()) continue;
            if (fExcludePileupTracks and hel.IsPileup()) continue;
            Vertex->AddHelix(&hel);
         }
      }
   }
   if( fTrace )
      std::cout<<"Reco::RecVertex(  )   # helices: "<<HelixArray.size()<<"   # good helices: "<<Vertex->GetNumberOfAddedHelix()<<std::endl;
   // reconstruct the vertex
   vertexstatus_t sv = kNoGoodHelices;

   if (Vertex->GetNumberOfAddedHelix())
   {
     sv = Vertex->Calculate(thread_no,thread_count);
     if( fTrace )
        std::cout<<"Reco::RecVertex(  )   # used helices: "<<Vertex->GetNumberOfHelices()<<std::endl;
   }
   return sv;
}
