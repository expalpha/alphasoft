// Cosmic Track class implementation
// for ALPHA-g TPC analysis
// Author: A.Capra 
// Date: May 2019

#include "TCosmic.hh"
#include "TPCconstants.hh"
#include "TFitVertex.hh"

#include <TVector3.h>
#include <TMinuit.h>
#include"Minuit2/FunctionMinimum.h"
#include"Minuit2/VariableMetricMinimizer.h"
#include"CosmicFCN.hh"

#include <iostream>
#include <algorithm>
#include <stdio.h>

TCosmic::TCosmic():TFitLine(),fMagneticField(0.),
                   fDCA(ALPHAg::kUnknown),fCosAngle(ALPHAg::kUnknown),fAngle(ALPHAg::kUnknown)
{
   fvstart = new double[9];
   for(int n=0; n<9; ++n) fvstart[n]=ALPHAg::kUnknown;
}

TCosmic::TCosmic(const TCosmic &object):TFitLine(object),fMagneticField(object.fMagneticField),
                   fDCA(object.fDCA),fCosAngle(object.fCosAngle),fAngle(object.fAngle)
                   {
                     fvstart = new double[9];
                     for(int n=0; n<9; ++n) fvstart[n]=object.fvstart[n];


                     /*for(int i=0; i<object.fResiduals.size();i++)
                     {
                        fResiduals.at(i) = object.fResiduals.at(i);
                     }*/
                     //fvstart=object.fvstart;
                   }

// TCosmic::TCosmic(const TFitHelix& t1,const TFitHelix& t2, double b):fMagneticField(b)
// {
//    fvstart = new double[9];
//    AddAllPoints(t1.GetPointsArray(),t2.GetPointsArray());
//    CalculateHelDCA(t1,t2); // defined here
// }

// TCosmic::TCosmic(const TFitLine& t1, const TFitLine& t2):fMagneticField(0.)
// {
//    fvstart = new double[9];
//    AddAllPoints(t1.GetPointsArray(),t2.GetPointsArray());
//    fDCA = t1.Distance(t2);// defined in TFitLine
//    fCosAngle = t1.CosAngle(t2);
//    fAngle = t1.Angle(t2);
// }

TCosmic::TCosmic(const TStoreHelix& t1, const TStoreHelix& t2, double b):fMagneticField(b)
{
   fvstart = new double[9];
   AddAllPoints( t1.GetSpacePoints(), t2.GetSpacePoints() );
   CalculateHelDCA(t1,t2); // defined here
}

TCosmic::TCosmic(const TStoreLine& t1 ,const TStoreLine& t2):fMagneticField(0.)
{
   fvstart = new double[9];
   AddAllPoints( t1.GetSpacePoints(), t2.GetSpacePoints() );
   fDCA = LineDistance(t1,t2); // defined here
   fCosAngle = t1.GetDirection()->Dot( *(t2.GetDirection()) );
   fAngle = t1.GetDirection()->Angle( *(t2.GetDirection()) );
}

TCosmic::~TCosmic()
{
   delete[] fvstart;
   fPoints.clear();
   fResiduals.clear();
}

int TCosmic::AddAllPoints(const TObjArray* pcol1, const TObjArray* pcol2)
{
   int np1 = pcol1->GetEntriesFast(),
      np2 = pcol2->GetEntriesFast();
   for(int i=0; i<np1; ++i)
      {
         TSpacePoint* ap = (TSpacePoint*) pcol1->At(i);
         AddPoint( *ap );
      }
   for(int i=0; i<np2; ++i)
      {
         TSpacePoint* ap = (TSpacePoint*) pcol2->At(i);
         AddPoint( *ap );
      }
   return fNpoints;
}

int TCosmic::AddAllPoints(const std::vector<TSpacePoint>* pcol1, 
			  const std::vector<TSpacePoint>* pcol2)
{ 
   for(auto p: *pcol1) AddPoint( p );
   for(auto p: *pcol2) AddPoint( p );
   return fNpoints;
}

void TCosmic::FitM2()
{
     if(fNpoints<=fNpar) return;
  fStatus=0;

  // Set starting values for parameters
  //static double vstart[fNpar*3];
  Initialization();

  std::vector<double> init_params = {fvstart[0],fvstart[1],fvstart[2],fvstart[3],fvstart[4],fvstart[5]};
  std::vector<double> init_errors(6,0.0001);
  
  CosmicFCN the_fcn(this);
  ROOT::Minuit2::VariableMetricMinimizer the_minimizer;
  ROOT::Minuit2::FunctionMinimum min = the_minimizer.Minimize(the_fcn, init_params, init_errors);

  ROOT::Minuit2::MnUserParameterState the_state = min.UserState();

  fchi2 = the_state.Fval();
  fStat = the_state.CovarianceStatus();
  fux = the_state.Value(0);
  fuy = the_state.Value(1);
  fuz = the_state.Value(2);
  fx0 = the_state.Value(3);
  fy0 = the_state.Value(4);
  fz0 = the_state.Value(5);

  double mod = TMath::Sqrt(fux*fux+fuy*fuy+fuz*fuz);
   if( mod == 0.) 
   {
      std::cerr<<"TCosmic::Fit() NULL SLOPE: error!"<<std::endl;
      return;
   }
   else if( mod == 1. )
   {
      std::cout<<"TCosmic::Fit() UNIT SLOPE: warning!"<<std::endl;
   }
   else 
   {
      //Maybe the error in these parameters should be propagated after
      //normalization
      fux/=mod;
      fuy/=mod;
      fuz/=mod;
   }

  fr0 = sqrt( fx0*fx0 + fy0*fy0 );

  ferr2ux = std::pow(the_state.Error(0),2);
  ferr2uy = std::pow(the_state.Error(1),2);
  ferr2uz = std::pow(the_state.Error(2),2);
  ferr2x0 = std::pow(the_state.Error(3),2);
  ferr2y0 = std::pow(the_state.Error(4),2);
  ferr2z0 = std::pow(the_state.Error(5),2);

}
void TCosmic::Initialization()
{
   double mod,dx,dy,dz,mx,my,mz,x0,y0,z0;
   mx=my=mz=x0=y0=z0=0.;
   int npoints=fPoints.size();
   //std::cout<<"TCosmic::Initialization npoints: "<<npoints<<std::endl;
   for(int i=0;i<npoints-1;i+=2)
      {
         //     std::cout<<"TCosmic::Initialization   "<<i<<std::endl;
         const TSpacePoint& PointOne = fPoints.at(i);
         double x1 = PointOne.GetX(),
            y1 = PointOne.GetY(),
            z1 = PointOne.GetZ();
         x0+=x1; y0+=y1; z0+=z1;
         //      PointOne->Print();

         const TSpacePoint& PointTwo = fPoints.at(i+1);
         double x2 = PointTwo.GetX(),
            y2 = PointTwo.GetY(),
            z2 = PointTwo.GetZ();
         x0+=x2; y0+=y2; z0+=z2;
         //      PointTwo->Print();

         dx = x2-x1; dy = y2-y1; dz=z2-z1;
         //      std::cout<<"TCosmic::Initialization (dx,dy,dz) = ("<<dx<<","<<dy<<","<<dz<<") mm"<<std::endl;
         mod=TMath::Sqrt(dx*dx+dy*dy+dz*dz);
         if( mod == 0. ) continue;
         //      std::cout<<"TCosmic::Initialization mod: "<<mod<<std::endl;
         dx/=mod; dy/=mod; dz/=mod;
         //      std::cout<<"TCosmic::Initialization (dx,dy,dz)/mod = ("<<dx<<","<<dy<<","<<dz<<") mm"<<std::endl;
         if( TMath::IsNaN(dx) || TMath::IsNaN(dy) || TMath::IsNaN(dz) ) continue;
         mx+=dx; my+=dy; mz+=dz;
      } 
   //  std::cout<<"TCosmic::Initialization (mx,my,mz) = ("<<mx<<","<<my<<","<<mz<<") mm"<<std::endl;
   double N = TMath::Floor( double(fPoints.size())*0.5 );
   //  std::cout<<"TCosmic::Initialization N: "<<N<<std::endl;
   mx/=N; my/=N; mz/=N;
   mod=TMath::Sqrt(mx*mx+my*my+mz*mz);
   //  std::cout<<"TCosmic::Initialization mag: "<<mod<<std::endl;
   mx/=mod; my/=mod; mz/=mod;
   //  std::cout<<"TCosmic::Initialization (mx,my,mz)/mag = ("<<mx<<","<<my<<","<<mz<<") mm"<<std::endl;
   fvstart[0]=mx;
   fvstart[1]=my;
   fvstart[2]=mz;

   fvstart[3] = fPoints.front().GetX();
   fvstart[4] = fPoints.front().GetY();
   fvstart[5] = fPoints.front().GetZ();
}

int TCosmic::CalculateHelDCA(const TStoreHelix& hi, const TStoreHelix& hj)
{
   TFitHelix* hel1 = new TFitHelix(hi);
   hel1->SetMagneticField(fMagneticField);
   TFitHelix* hel2 = new TFitHelix(hj);
   hel2->SetMagneticField(fMagneticField);
   int stat = CalculateHelDCA(*hel1,*hel2);
   delete hel1;
   delete hel2;
   return stat;
}

int TCosmic::CalculateHelDCA(const TFitHelix& hel1, const TFitHelix& hel2)
{
   TFitVertex* c = new TFitVertex(-1);
   c->AddHelix( (TFitHelix*) &hel1 );
   c->AddHelix( (TFitHelix*) &hel2 );
   int stat = c->FindDCA();
   if( stat > 0 )
      {
         //fDCA = c->GetNewChi2(); //mis-name since I'm re-using the vertexing algorithm
         fDCA = c->GetMeanVertex()->Mag();
         TVector3 p1 = hel1.GetMomentumV();
         TVector3 p2 = hel2.GetMomentumV();
         fCosAngle = p1.Unit().Dot(p1.Unit());
         fAngle = p1.Unit().Angle(p1.Unit());
      }
   else
      {
         fDCA = fCosAngle = fAngle = ALPHAg::kUnknown;
      }

   delete c;
   return stat;
}

double TCosmic::LineDistance(const TStoreLine& l0, const TStoreLine& l1)
{
   TVector3 u0 = *(l0.GetDirection());
   TVector3 u1 = *(l1.GetDirection());
   TVector3 p0 = *(l0.GetPoint());
   TVector3 p1 = *(l1.GetPoint());
  
   TVector3 n0 = u0.Cross( u1 ); // normal to lines
   TVector3 c =  p1 - p0;
   if( n0.Mag() == 0. ) return -1.;
  
   TVector3 n1 = n0.Cross( u1 ); // normal to plane formed by n0 and line1
  
   double tau = c.Dot( n1 ) / u0.Dot( n1 ); // intersection between
   TVector3 q0 = tau * u0 + p0;             // plane and line0
  
   double t1 = ( (q0-p0).Cross(n0) ).Dot( u0.Cross(n0) ) / ( u0.Cross(n0) ).Mag2();
   TVector3 q1 = t1 * u0 + p0;
  
   double t2 = ( (q0-p1).Cross(n0) ).Dot( u1.Cross(n0) ) / ( u1.Cross(n0) ).Mag2();
   TVector3 q2 = t2*u1+p1;
  
   TVector3 Q = q2 - q1;
  
   return Q.Mag();
}

ClassImp(TCosmic)

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
