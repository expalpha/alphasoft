#include"HelixFCN.hh"
#include<cassert>

HelixFCN::HelixFCN(TFitHelix* a_track) : error_def(1) {
        track = a_track;
	points = track->GetPointsArray();
}

double HelixFCN::Up() const {
	return error_def;
}

RadFuncFCN::RadFuncFCN(TFitHelix* a_track) : HelixFCN(a_track) {
}

double RadFuncFCN::operator()(const std::vector<double>& p) const {
	assert(p.size() == 3);

	double chi2 = 0;

	for(const auto& apnt : *points) {

		const std::array<double,2> closest = TFitHelix::GetClosestToPoint(apnt.GetX(),apnt.GetY(),p[0],p[1],p[2]);	

		const double dx = apnt.GetX() - closest[0]; // Distance from closest point to spacepoint
		const double dy = apnt.GetY() - closest[1];

		double tx = dx / apnt.GetErrX();
		double ty = dy / apnt.GetErrY();
		double d2 = tx * tx + ty * ty;

		chi2 += d2;

	}
	
	return chi2;
}

ZedFuncFCN::ZedFuncFCN(TFitHelix* a_track) : HelixFCN(a_track) {
}

double ZedFuncFCN::operator()(const std::vector<double>& p) const {
	assert(p.size() == 2);

	double chi2 = 0;
	double Rc = track->GetRc();
	double phi0 = track->GetPhi0();
	double D = track->GetD();

	for(const auto& apnt : *points) {
		const TVector3 closest = TFitHelix::GetClosestToPoint(apnt.GetX(),apnt.GetY(),apnt.GetZ(),Rc,phi0,D,p[0],p[1]);
		const double tz = (apnt.GetZ() - closest.Z()) / apnt.GetErrZ();
		const double d2 = tz * tz;
		chi2 += d2;
	}
	return chi2;
}

double ZedFuncFCN::test(const std::vector<double>& p, TFitHelix* testHelix) const {
	assert(p.size() == 2);

	double chi2 = 0;

	for(const auto& apnt : *points) {
		const TVector3 closest = TFitHelix::GetClosestToPoint(apnt.GetX(),apnt.GetY(),apnt.GetZ(),testHelix->GetRc(),testHelix->GetPhi0(),testHelix->GetD(),p[0],p[1]);
		const double tz = (apnt.GetZ() - closest.Z()) / apnt.GetErrZ();
		const double d2 = tz * tz;
		chi2 += d2;
	}
	return chi2;

}


