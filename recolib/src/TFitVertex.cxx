// Vertex class implementation
// for ALPHA-g TPC analysis
// Author: A.Capra 
// Date: May 2014

#include "TFitVertex.hh"

#include <iostream>
#include <iomanip>
#include <string>

#include <TMath.h>
#include <TMinuit.h>

#include "TPCconstants.hh"

#include "VertexFCN.hh"
#include"Minuit2/FunctionMinimum.h"
#include"Minuit2/MnMigrad.h"

TFitVertex::TFitVertex():fID(-1),fHelixArray(0),fNhelices(0),
            fchi2(-1.),
            fNumberOfUsedHelices(0),fHelixStack(0),
            fChi2Cut(17.), fFitIterations(50),
            fInit0(0),fSeed0Index(-1),fSeed0Par(0.),fSeed0Point(0.,0.,0.),
            fInit1(0),fSeed1Index(-1),fSeed1Par(0.),fSeed1Point(0.,0.,0.),
            fSeedchi2(9999999.),
            fNewChi2(-1),
            fNewSeed0Par(0.),fNewSeed1Par(0.),fDCA(ALPHAg::kLargeNegativeUnknown)
{ 
  fVertex.SetXYZ(-999.,-999.,-999.);
  fVertexError2.SetXYZ(-999.,-999.,-999.);  
  fNewVertex.SetXYZ(-999.,-999.,-999.);
  fNewVertexError2.SetXYZ(-999.,-999.,-999.);  
  fMeanVertex.SetXYZ(-999.,-999.,-999.);
  fMeanVertexError2.SetXYZ(-999.,-999.,-999.);
}

TFitVertex::TFitVertex(int i):fID(i),fHelixArray(0),fNhelices(0),
            fchi2(-1.),
            fNumberOfUsedHelices(0),fHelixStack(0),
            fChi2Cut(17.),
            fInit0(0),fSeed0Index(-1),fSeed0Par(0.),fSeed0Point(0.,0.,0.),
            fInit1(0),fSeed1Index(-1),fSeed1Par(0.),fSeed1Point(0.,0.,0.),
            fSeedchi2(9999999.),
            fNewChi2(-1),
            fNewSeed0Par(0.),fNewSeed1Par(0.),fDCA(ALPHAg::kLargeNegativeUnknown)
{ 
  fVertex.SetXYZ(-999.,-999.,-999.);
  fVertexError2.SetXYZ(-999.,-999.,-999.);  
  fNewVertex.SetXYZ(-999.,-999.,-999.);
  fNewVertexError2.SetXYZ(-999.,-999.,-999.);  
  fMeanVertex.SetXYZ(-999.,-999.,-999.);
  fMeanVertexError2.SetXYZ(-999.,-999.,-999.);
}

void TFitVertex::Clear(Option_t*)
{ 
  fHelixArray.clear();
  fHelixStack.clear();
}


TFitVertex::~TFitVertex()
{ 
  Clear();
}

int TFitVertex::AddHelix(TFitHelix* anHelix)
{
  // MS error added in quadrature
  anHelix->AddMSerror();

  fHelixArray.push_back(anHelix);
  ++fNhelices;
  return fNhelices;
}

// main function for vertexing
vertexstatus_t TFitVertex::Calculate(const int thread_no, const int thread_count)
{
  if (fNhelices == 0) return kNoGoodHelices;
  if (fNhelices < 2) return kOnlyOneGoodHelix;
  vertexstatus_t val = kUninitialized;
    // ============= FIRST PASS =============
   // find the minimum distance 
   // between a pair of helices

   // First thread does first pass
   if (thread_no == 1)
      val = FirstPass();
   if ( (thread_no == 1 && thread_count > 1) || // I have more than 1 thread... safe work for another thread
                                   (val < 0))  // Or I failed a fit
      return val;

  // ============= SECOND PASS =============
  // minimize the distance of the minimum-distance-pair
  // to a point, called NewVertex
  if ( (thread_no == 1 && thread_count == 1) ||  // First thread of ONE always works
                            ( thread_no == 2) )  // Second thread does the work
     val = SecondPass();
  
  if (thread_no == 2 && thread_count > 2) // I have 3 threads.... 
      return val;

  // ============= THIRD PASS =============
  // if there are more than 2 helices, improve
  // vertex by finding a new one with more helices
  
  if (val!=kChi2failed) val = ThirdPass(); // If we got this far, I am the last thread no matter what


  //Calculate DCA
  FindDCA();


  // return code is
  //  0: only one good helix
  // -1: failed to find minimum-distance-pair
  //  1: only two good helices
  //  2: didn't improve vertex (more than 2 helices)
  //  3: vertex improved
  return val;
}

vertexstatus_t TFitVertex::FirstPass()
{
  // ============= FIRST PASS =============
  // find the minimum distance 
  // between a pair of helices
  
  fchi2 = FindSeed(); 

  if(fSeed0Index<0||fSeed1Index<0) return kFailToFindPair;
  fNumberOfUsedHelices=2;
  // the SeedVertex is the mean point
  // on the segment joining the minimum-distance-pair
  fVertex = EvaluateMeanPoint();
  fVertexError2 = EvaluateMeanPointError2();

  return kOnlyTwoGoodHelices;
}

vertexstatus_t TFitVertex::ThirdPass()
{
  vertexstatus_t val=kOnlyTwoGoodHelices;
  // ============= THIRD PASS =============
  // if there are more than 2 helices, improve
  // vertex by finding a new one with more helices
  if(fNhelices>2) val=Improve();

  if(fNumberOfUsedHelices != (int)fHelixStack.size())
    std::cerr<<"Improve Error ("<<fNumberOfUsedHelices <<"!=" << fHelixStack.size() << ")"<<std::endl;

  // notify the helix in the stack whether it has been used for 
  // the seed (2)
  // the improvement (3)
  // ( all the helices outside the stack have status 1 )
  AssignHelixStatus();

  // return code is
  //  0: only one good helix
  // -1: failed to find minimum-distance-pair
  //  1: only two good helices
  //  2: didn't improve vertex (more than 2 helices)
  //  3: vertex improved
  return val;
}

double TFitVertex::FindSeed()
{
  for (int i0=0; i0<fNhelices; i0++) {
    fInit0 = fHelixArray.at(i0);
    const double x0_0 = fInit0->GetX0(); const double y0_0 = fInit0->GetY0(); const double z0_0 = fInit0->GetZ0();
    for (int i1=0; i1<fNhelices; i1++) {
      if (i1==i0) continue;
      fInit1 = fHelixArray.at(i1);
      const double x0_1 = fInit1->GetX0(); const double y0_1 = fInit1->GetY0(); const double z0_1 = fInit1->GetZ0();
      const double twist_height = 2*TMath::Pi()*TMath::Abs(fInit1->GetRc())*fInit1->GetLambda();
      const double num_twists = (fInit1->GetLambda()!=0) ? (z0_0-z0_1)/twist_height : 0;
      int int_num_twists = (int)std::round(num_twists);
      const double best_z0_1 = z0_1 + int_num_twists*twist_height;
      const double twist_height_0 = 2*TMath::Pi()*TMath::Abs(fInit0->GetRc())*fInit0->GetLambda();
      const double num_twists_0 = (fInit0->GetLambda()!=0) ? (best_z0_1-z0_0)/twist_height_0 : 0;
      int int_num_twists_0 = (int)std::round(num_twists_0);
      const double best_z0_0 = z0_0 + int_num_twists_0*twist_height_0;
      const double dist = TMath::Sqrt((x0_0-x0_1)*(x0_0-x0_1)+(y0_0-y0_1)*(y0_0-y0_1)+(best_z0_0-best_z0_1)*(best_z0_0-best_z0_1));
      if (dist<fSeedchi2) {
        fSeedchi2 = dist;
        fSeed0Index = i0; fSeed0Point = TVector3(x0_0,y0_0,z0_0);
        fSeed1Index = i1; fSeed1Point = TVector3(x0_1,y0_1,best_z0_1);;
      }
    }
  }
  return fSeedchi2;
}

vertexstatus_t TFitVertex::SecondPass()
{
  // ============= SECOND PASS =============
  // minimize the distance of the minimum-distance-pair
  // to a point, called NewVertex
  fHelixStack.clear();
  fHelixStack.push_back( fHelixArray.at( fSeed0Index ));
  fHelixStack.push_back( fHelixArray.at( fSeed1Index ));

  fchi2 = Recalculate();
  if (fchi2/2. > fChi2Cut) return kChi2failed;
  else return kOnlyTwoGoodHelices;

}

double TFitVertex::FindMinDistance(double& s0, double& s1)
{

  std::vector<double> init_dfit = {s0,s1};
  std::vector<double> init_derr(2, 0.01);
  
  MinDistFCN fitd_fcn(this);
  ROOT::Minuit2::MnMigrad fitd_minimizer(fitd_fcn,init_dfit,init_derr);
  ROOT::Minuit2::FunctionMinimum fitd_min = fitd_minimizer(fFitIterations, 0.1);
  ROOT::Minuit2::MnUserParameterState fitd_state = fitd_min.UserState();

  double chi2 = fitd_state.Fval();

  s0 = fitd_state.Value(0);
  s1 = fitd_state.Value(1);

  // degrees of freedom is ndf=3H-2
  // for H=2, ndf=4
  return 0.25*chi2;
}

TVector3 TFitVertex::EvaluateMeanPoint()
{
  return EvaluateMeanPoint( 
          fSeed0Point,
          TVector3(1.,1.,1.),
          fSeed1Point,
          TVector3(1.,1.,1.)
        );
}

TVector3 TFitVertex::EvaluateMeanPoint(TVector3 p0, TVector3 e0, 
               TVector3 p1, TVector3 e1)
{
  TVector3 zero(0.,0.,0.);
  if(e0!=zero && e1!=zero)
    {
      double px=p0.X()/e0.X() + p1.X()/e1.X(),
  py=p0.Y()/e0.Y() + p1.Y()/e1.Y(),
  pz=p0.Z()/e0.Z() + p1.Z()/e1.Z();
      fMeanVertex.SetXYZ(px/(1./e0.X() + 1./e1.X()),
       py/(1./e0.Y() + 1./e1.Y()),
       pz/(1./e0.Z() + 1./e1.Z()));
    }
  else fMeanVertex = 0.5*(p0+p1);
  return fMeanVertex;
}

TVector3 TFitVertex::EvaluateMeanPointError2()
{
  /*const TVector3& e0 = fHelixArray.at(fSeed0Index)->GetError2(fSeed0Par);
  const TVector3& e1 = fHelixArray.at(fSeed1Index)->GetError2(fSeed1Par);
  fMeanVertexError2 = (e0+e1)*0.25;
  return fMeanVertexError2;*/
  
  // This was broken by Gareth's 2024 update... Sorry eh.
  return TVector3(1.,1.,1.);
}

double TFitVertex::Recalculate()
{
  std::vector<double> init_vfit = {fVertex.X(),fVertex.Y(),fVertex.Z()};
  std::vector<double> init_verr(3, 0.01);

  VertGFuncFCN fitvtx_fcn(this);

  ROOT::Minuit2::MnMigrad fitvtx_minimizer(fitvtx_fcn,init_vfit,init_verr);
  ROOT::Minuit2::FunctionMinimum fitvtx_min = fitvtx_minimizer(fFitIterations, 0.1);
  ROOT::Minuit2::MnUserParameterState fitvtx_state = fitvtx_min.UserState();

  double chi2 = fitvtx_state.Fval();

  double vx,vy,vz,ex,ey,ez;
  vx = fitvtx_state.Value(0);
  ex = fitvtx_state.Error(0);
  vy = fitvtx_state.Value(1);
  ey = fitvtx_state.Error(1);
  vz = fitvtx_state.Value(2);
  ez = fitvtx_state.Error(2);
  fVertex.SetXYZ(vx,vy,vz);
  fVertexError2.SetXYZ(ex*ex,ey*ey,ez*ez);
  
  // store the NewVertex and the NewChi2
  fNewChi2=chi2;
  fNewVertex.SetXYZ(vx,vy,vz);
  fNewVertexError2.SetXYZ(ex*ex,ey*ey,ez*ez);

  // degrees of freedom is ndf=3H-5
  // for H=2, ndf=1
  return fNewChi2; 
}

vertexstatus_t TFitVertex::Improve()
{
  int nhels=fHelixArray.size();
  TVector3 vertex_save = fVertex;
  TVector3 err_save = fVertexError2;
  for(int n=0; n<nhels; ++n)
    {
      if(n==fSeed0Index || n==fSeed1Index) continue;
      fHelixStack.push_back( fHelixArray.at(n) );

      double new_chi2 = Recalculate(); 
      if(new_chi2/fHelixStack.size() < fChi2Cut ) {
        ++fNumberOfUsedHelices;
        fchi2 = new_chi2;
        vertex_save = fVertex;
        err_save = fVertexError2;
      } else {
        fHelixStack.pop_back();
        fVertex = vertex_save;
        fVertexError2 = err_save;
      }
    }
  
  if(fNumberOfUsedHelices>2) return kVertexImproved;
  else return kDidntImproveHelix;
}

double TFitVertex::FindNewVertex(double* ipar, double* iparerr)
{
  std::vector<double> init_vnewfit;
  std::vector<double> init_vnewerr;

  int mpar = 3+fHelixStack.size();

  for(int i=0; i<mpar; ++i)
    {
      init_vnewfit.push_back(ipar[i]);
      init_vnewerr.push_back(0.01);
    }

  VertFuncFCN fitnewvtx_fcn(this);

  ROOT::Minuit2::MnMigrad fitnewvtx_minimizer(fitnewvtx_fcn,init_vnewfit,init_vnewerr);
  ROOT::Minuit2::FunctionMinimum fitnewvtx_min = fitnewvtx_minimizer(fFitIterations, 0.1);
  ROOT::Minuit2::MnUserParameterState fitnewvtx_state = fitnewvtx_min.UserState();

  double chi2 = fitnewvtx_state.Fval();
  
  for(int i=0; i<mpar; ++i)
  {
    ipar[i] = fitnewvtx_state.Value(i);
    iparerr[i] = fitnewvtx_state.Error(i);
  }
  

  double ndf = 3.*(double) fHelixStack.size() - (double) mpar;
  return chi2/ndf; 
}

void TFitVertex::AssignHelixStatus()
{
  int nhelstack=fHelixStack.size();
  for(int h=0; h<nhelstack; ++h)
    {
      if(h==0 || h==1) 
        fHelixStack.at(h)->SetStatus(2);
      else
        fHelixStack.at(h)->SetStatus(3);
    }
}

int TFitVertex::FindDCA()
{
  if(fNhelices<2) return 0;
  
  if(fSeed0Index<0||fSeed1Index<0) return -1;
  TVector3 meanpoint = EvaluateMeanPoint();
  TVector3 p0 = fHelixArray.at(fSeed0Index)->GetClosestToPointIncludingBackwards(meanpoint.X(),meanpoint.Y(),meanpoint.Z());
  TVector3 p1 = fHelixArray.at(fSeed1Index)->GetClosestToPointIncludingBackwards(meanpoint.X(),meanpoint.Y(),meanpoint.Z());
  p0 = fHelixArray.at(fSeed0Index)->GetClosestToPointIncludingBackwards(p1.X(),p1.Y(),p1.Z());
  p1 = fHelixArray.at(fSeed1Index)->GetClosestToPointIncludingBackwards(p0.X(),p0.Y(),p0.Z());
  p0 = fHelixArray.at(fSeed0Index)->GetClosestToPointIncludingBackwards(p1.X(),p1.Y(),p1.Z());
  p1 = fHelixArray.at(fSeed1Index)->GetClosestToPointIncludingBackwards(p0.X(),p0.Y(),p0.Z());
  fMeanVertex = p0 - p1;
  fDCA = fMeanVertex.Mag();

  return 1;
}

void TFitVertex::Print(Option_t* opt) const
{
  std::cout<<"TFitVertex:: # of Used Helices: "<<fNumberOfUsedHelices<<", ";
  if( !strcmp(opt,"rphi") )
    {
      std::cout<<"(r,phi,z) = ("
         <<std::setw(5)<<std::left<<fVertex.Perp()<<", "
         <<std::setw(5)<<std::left<<fVertex.Phi()<<", "
         <<std::setw(5)<<std::left<<fVertex.Z()<<"), ";
    }
  else if( !strcmp(opt,"xy") )
  {
      std::cout<<"(x,y,z) = ("
         <<std::setw(5)<<std::left<<fVertex.X()<<", "
         <<std::setw(5)<<std::left<<fVertex.Y()<<", "
         <<std::setw(5)<<std::left<<fVertex.Z()<<"), ";
    }
  else std::cout<<"Unknown coordinate system, "<<opt<<std::endl;
  std::cout<<"Normalized chi^2 = "<<fchi2<<std::endl;
}

void TFitVertex::Reset()
{
  fVertex.SetXYZ(-999.,-999.,-999.);
  fVertexError2.SetXYZ(-999.,-999.,-999.);
  fHelixArray.clear();
  fID=-1;
  fNhelices=-1;
  fchi2=-999.;
  fInit0=0;
  fInit1=0;
  fSeed0Index=-1;
  fSeed1Index=-1;
  fMeanVertex.SetXYZ(-999.,-999.,-999.);
  fMeanVertexError2.SetXYZ(-999.,-999.,-999.);
  fNumberOfUsedHelices=-1;
  fHelixStack.clear();
  fNewChi2=-999.;
  fNewVertex.SetXYZ(-999.,-999.,-999.);
  fNewVertexError2.SetXYZ(-999.,-999.,-999.);
  fNewSeed0Par=-999.;
  fNewSeed1Par=-999.;
  fChi2Cut=0.;
  fFitIterations=50;
}

ClassImp(TFitVertex)

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
