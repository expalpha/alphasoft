#include "Match.hh"

#include "TH1D.h"
#include "TSpectrum.h"
#include "TF1.h"
#include "TCanvas.h"

#include <Math/MinimizerOptions.h>

#include "fitSignals.hh"

#include <chrono>
#include <thread>       // std::thread
#include <functional>   // std::ref

/* PadMerge
//Null static pointers to histograms (histograms static so can be shared 
//between Match instances in multithreaded mode)
//TH1D* Match::hsigCoarse=NULL;
//TH1D* Match::hsig=NULL;
TH1D* Match::hcognpeaks=NULL;
// TH2D* Match::hcognpeaksrms=NULL;
// TH2D* Match::hcognpeakswidth=NULL;
TH1D* Match::hcogsigma=NULL;
TH1D* Match::hcogerr=NULL;
TH2D* Match::hcogpadssigma=NULL;
TH2D* Match::hcogpadsamp=NULL;
TH2D* Match::hcogpadsint=NULL;
TH2D* Match::hcogpadsampamp=NULL;
TH1D* Match::hcogpadsdmaxmean=NULL;
TH1D* Match::hcogpadsdfitpos=NULL;
TH1D* Match::htimecog=NULL;
TH1D* Match::htimeblobs=NULL;
TH1D* Match::htimefit=NULL;
*/


std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> > Match::MatchElectrodes(std::vector<ALPHAg::TWireSignal>& awsignals, std::vector<ALPHAg::TPadSignal>& CombinedPads )
{
  std::multiset<ALPHAg::TWireSignal, ALPHAg::signal::timeorder> aw_bytime(awsignals.begin(),
						     awsignals.end());
  std::multiset<ALPHAg::TPadSignal, ALPHAg::signal::timeorder> pad_bytime(CombinedPads.begin(),
						      CombinedPads.end());

  std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> > spacepoints;
  int Nmatch=0;
  for( auto iaw=aw_bytime.begin(); iaw!=aw_bytime.end(); ++iaw )
    {
      if( iaw->t < 0. ) continue;
      short sector = short(iaw->idx/8);
      short secwire = short(iaw->idx%8);
      if( fTrace )
	std::cout<<"Match::Match aw: "<<iaw->idx
		 <<" t: "<<iaw->t<<" pad sector: "<<sector<<std::endl;
      for( auto ipd=pad_bytime.begin(); ipd!=pad_bytime.end(); ++ipd )
	{
	  if( ipd->t < 0. ) continue;
	  bool tmatch=false;
	  bool pmatch=false;

          bool ampCut = (charge_dist_scale==0);

	  double delta = fabs( iaw->t - ipd->t );
	  if( delta < fCoincTime ) tmatch=true;

	  if( sector == ipd->sec ) pmatch=true;

          if( !ampCut ){
              ampCut = (ipd->height > charge_dist_scale*padThr*relCharge[secwire]);
          }

	  if( tmatch && pmatch && ampCut )
	    {
	      spacepoints.push_back( std::make_pair(*iaw,*ipd) );
	      //pad_bytime.erase( ipd );
	      ++Nmatch;
	      if( fTrace )
		std::cout<<"\t"<<Nmatch<<")  pad col: "<<ipd->sec<<" pad row: "<<ipd->idx
			 <<"\tpad err: "<<ipd->errz<<std::endl;
	    }
	}
    }
  if( fTrace )
  //std::cout<<"Match::MatchElectrodes Number of Matches: "<<Nmatch<<std::endl;
  std::cout<<"Match::MatchElectrodes "<<Nmatch<<" found"<<std::endl;
  if( int(spacepoints.size()) != Nmatch )
    std::cerr<<"Match::MatchElectrodes ERROR: number of matches differs from number of spacepoints: "<<spacepoints.size()<<std::endl;
  return spacepoints;
}


std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> >  Match::FakePads(std::vector<ALPHAg::TWireSignal>& awsignals)
{
  std::multiset<ALPHAg::TWireSignal, ALPHAg::signal::timeorder> aw_bytime(awsignals.begin(),
						     awsignals.end());
  std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> > spacepoints;
  int Nmatch=0;
  for( auto iaw=aw_bytime.begin(); iaw!=aw_bytime.end(); ++iaw )
    {
      if( iaw->t < 0. ) continue;
      short sector = short(iaw->idx/8);
      //signal fake_pad( sector, 288, iaw->t, 1., 0.0 );
      //signal fake_pad( sector, 288, iaw->t, 1., 0.0, kUnknown);
      ALPHAg::TPadSignal fake_pad( ALPHAg::electrode( sector, 288), iaw->t, 1., 0.);
      spacepoints.push_back( std::make_pair(*iaw,fake_pad) );
      ++Nmatch;
    }
  if( int(spacepoints.size()) != Nmatch )
    std::cerr<<"Match::FakePads ERROR: number of matches differs from number of spacepoints: "<<spacepoints.size()<<std::endl;
  if( fTrace )
    std::cout<<"Match::FakePads Number of Matches: "<<Nmatch<<std::endl;
  return spacepoints;
}

void Match::SortPointsAW(  const std::pair<double,int>& pos,
			   std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>& vec,
			   std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>,std::greater<int>>& spaw )
{
  for(auto& s: vec)
    {
      if( 1 )
	std::cout<<"\ttime: "<<pos.first
		 <<" row: "<<pos.second
		 <<" aw: "<<s->first.idx
		 <<" amp: "<<s->first.height
		 <<"   ("<<s->first.t<<", "<<s->second.idx<<")"<<std::endl;
      spaw[s->first.idx].push_back( s );
    }// vector of sp with same time and row
}
void Match::SortPointsAW(  std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>& vec,
			   std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>,std::greater<int>>& spaw )
//void Match::SortPointsAW(  const std::pair<double,int>& pos,
//			   std::vector<std::pair<ALPHAg::signal,ALPHAg::signal>*>& vec,
//			   std::map<int,std::vector<std::pair<ALPHAg::signal,ALPHAg::signal>*>>& spaw )
{
  for(auto& s: vec)
    {
      spaw[s->first.idx].push_back( s );
    }// vector of sp with same time and row
}

void Match::CombPointsAW(std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>,std::greater<int>>& spaw,
			 std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>>& merger)
{
  int m=-1, aw = spaw.begin()->first, q=0;
  for( auto& msp: spaw )
    {
      if( fTrace )
	std::cout<<"Match::CombPointsAW: "<<msp.first<<std::endl;
      for( auto &s: msp.second )
	{
	  if( abs(s->first.idx-aw) <= 1 )
	    {
	      merger[q].push_back( s );
	      ++m;
	    }
	  else
	    {
	      ++q;
	      merger[q].push_back( s );
	      m=0;
	    }
	  if( fTrace )
	    std::cout<<"\t"<<m
		     <<" aw: "<<s->first.idx
		     <<" amp: "<<s->first.height
		     <<" phi: "<<s->first.phi
		     <<"   ("<<s->first.t<<", "<<s->second.idx<<", "
		     << ALPHAg::_anodepitch * ( double(s->first.idx) + 0.5 )
		     <<") {"
		     <<s->first.idx%8<<", "<<s->first.idx/8<<", "<<s->second.sec<<"}"
		     <<std::endl;
	  aw = s->first.idx;
	}// vector of sp with same time and row and decreasing aw number
    }// map of sp sorted by increasing aw number
}
void Match::CombPointsAW(std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>>& spaw,
			 std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>>& merger)
{
  int m=-1, aw = spaw.begin()->first, q=0;
  // std::cout<<"Match::CombPoints() anode: "<<aw
  //          <<" pos: "<<_anodepitch * ( double(aw) + 0.5 )<<std::endl;
  for( auto& msp: spaw )
    {
      for( auto &s: msp.second )
	{
	  if( abs(s->first.idx-aw) <= 1 )
	    {
	      merger[q].push_back( s );
	      ++m;
	    }
	  else
	    {
	      ++q;
	      merger[q].push_back( s );
	      m=0;
	    }
	  if( 0 )
	    std::cout<<"\t"<<m
		     <<" aw: "<<s->first.idx
		     <<" amp: "<<s->first.height
		     <<" phi: "<<s->first.phi
		     <<"   ("<<s->first.t<<", "<<s->second.idx<<", "
		     << ALPHAg::_anodepitch * ( double(s->first.idx) + 0.5 )
		     <<") "
		     <<std::endl;
	  aw = s->first.idx;
	}// vector of sp with same time and row and increasing aw number
    }// map of sp sorted by increasing aw number
}

uint Match::MergePoints(std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>>& merger,
			std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>>& merged,
			uint& number_of_merged)
{
  uint np = 0;
  for( auto &mmm: merger )
    {
      double pos=0.,amp=0.;
      double maxA=amp, amp2=amp*amp;
      if( fTrace )
	std::cout<<"==="<<mmm.first<<std::endl;
      np+=mmm.second.size();
      uint j=0, idx=j;
      int wire=-1;
      for( auto &p: mmm.second )
	{
	  double A = p->first.height,
	    pphi = p->first.phi;
	  if( 0 )
	    std::cout<<" aw: "<<p->first.idx
		     <<" amp: "<<p->first.height
		     <<" phi: "<<p->first.phi
		     <<"   ("<<p->first.t<<", "<<p->second.idx<<", "
		     << ALPHAg::_anodepitch * ( double(p->first.idx) + 0.5 )
		     <<") "<<std::endl;
	  amp += A;
	  amp2 += (A*A);
	  pos += (pphi*A);
	  if( A > maxA )
	    {
	      idx = j;
	      maxA = A;
	      wire = p->first.idx;
	    }
	  ++number_of_merged;
	  ++j;
	}
      double phi = pos/amp,
	err = phi_err*sqrt(amp2)/amp,
	H = amp/double(mmm.second.size());
      if( fTrace )
	std::cout<<"\tpnt: "<<phi<<" +/- "<<err
		 <<" A: "<<H<<" # "<<mmm.second.size()
		 <<" wire: "<<wire<<" maxA: "<<maxA
		 <<std::endl;
      for( uint i=0; i<mmm.second.size(); ++i )
	{
	  if( i == idx )
	    {
	      mmm.second.at(i)->first.height = H;
	      mmm.second.at(i)->first.phi = phi;
	      mmm.second.at(i)->first.errphi = err;
	      merged.push_back( *mmm.second.at(i) );
	      --number_of_merged;
	    }
	}
    }
  return np;
}

std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> > Match::CombPoints(std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> >& spacepoints)
{
  if( fTrace )
    std::cout<<"Match::CombPoints() spacepoints size: "<<spacepoints.size()<<std::endl;

  // sort sp by row and time
  std::map<std::pair<double,int>,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>> combsp;
  for(auto &sp: spacepoints)
    {
      double time = sp.first.t;
      int row = sp.second.idx;
      std::pair<double,int> spid(time,row);
      combsp[spid].push_back( &sp );
    }

  if( fTrace )
    std::cout<<"Match::CombPoints() comb size: "<<combsp.size()<<std::endl;
  uint n=0;
  std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>> merged;
  uint m=0;
  for(auto &k: combsp)
    {
      n+=k.second.size();
      if( k.second.size() > 1 )
	{
	  if( fTrace )
	    std::cout<<"Match::CombPoints() vec size: "<<k.second.size()
		     <<"\ttime: "<<k.first.first
		     <<"ns row: "<<k.first.second<<std::endl;

	  // sort sp by decreasing aw number
	  std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>,std::greater<int>> spaw;
	  //                  SortPointsAW( k.first, k.second, spaw );
	  SortPointsAW( k.second, spaw );

	  std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>> merger;
	  CombPointsAW(spaw,merger);
	  if( 0 )
	    std::cout<<"Match::CombPoints() merger size: "<<merger.size()<<std::endl;

	  uint np = MergePoints( merger, merged, m );
	  if( np != k.second.size() )
	    std::cerr<<"Match::CombPoints() ERROR tot merger size: "<<np
		     <<" vec size: "<<k.second.size()<<std::endl;
	}// more than 1 sp at the same time in the same row
      else
	{
	  merged.push_back( *k.second.at(0) );
	}
    }// map of sp sorted by row and time

  if( n != spacepoints.size() )
    std::cerr<<"Match::CombPoints() ERROR total comb size: "<<n
	     <<"spacepoints size: "<<spacepoints.size()<<std::endl;
  if( (n-merged.size()) != m )
    std::cerr<<"Match::CombPoints() ERROR spacepoints merged diff size: "<<n-merged.size()
	     <<"\t"<<m<<std::endl;

  spacepoints.assign( merged.begin(), merged.end() );
  if( fDebug ) {
    std::cout<<"Match::CombPoints() spacepoints merged size: "<<merged.size()<<" (diff: "<<m<<")"<<std::endl;
    std::cout<<"Match::CombPoints() spacepoints size (after merge): "<<spacepoints.size()<<std::endl;
  }
  if( fTrace )
    std::cout<<"Match::CombPoints() "<<spacepoints.size()<<" found"<<std::endl;
  return spacepoints;
}
