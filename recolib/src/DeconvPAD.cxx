#include <fstream>
#include <numeric>
#include <cmath>

#include <TDirectory.h>

#include "DeconvPAD.h"

#ifdef BUILD_AG_SIM
#include "TWaveform.hh"
#endif

// pads
TH1D* DeconvPAD::hAvgRMSPad=0;

DeconvPAD::DeconvPAD( double pwb, double pad, unsigned int maxhits): fTrace(false), fDiagnostic(false), fAged(false),
                                                                     fPADbinsize(16),
                                                                     fPWBmax(pow(2.,12.)),
                                                                     fPWBrange(fPWBmax*0.5-1.),
                                                                     fPWBdelay(0.), // to be guessed
                                                                     pedestal_length(100),fScale(-1.), // values fixed by DAQ
                                                                     thePadBin(6),
                                                                     fPWBThres(pwb),
                                                                     fPWBpeak(pad),
                                                                     fPADmaxHits(maxhits),
                                                                     isalpha16(false)
{
   Setup();
}

DeconvPAD::DeconvPAD(const std::string& json, const std::vector<std::string>& args):
                                fTrace(false), fDiagnostic(false), fAged(false),
                                ana_settings(new AnaSettings(json.c_str(), args)),
                                fPADbinsize(16),
                                fPWBmax(pow(2.,12.)),
                                fPWBrange(fPWBmax*0.5-1.),
                                fPWBdelay(0.), // to be guessed
                                pedestal_length(100),fScale(-1.), // values fixed by DAQ
                                thePadBin(6),
                                fPWBThres(ana_settings->GetDouble("DeconvModule","PWBthr")),
                                fPWBpeak(ana_settings->GetDouble("DeconvModule","PADthr")),
                                fPADmaxHits(ana_settings->GetInt("DeconvModule","PADMaxHits")),
                                isalpha16(false)
{
   Setup();
}

DeconvPAD::DeconvPAD(AnaSettings* s):fTrace(false), fDiagnostic(false), fAged(false),
                               ana_settings(s),
                               fPADbinsize(16),
                               fPWBmax(pow(2.,12.)),
                               fPWBrange(fPWBmax*0.5-1.),
                               fPWBdelay(0.), // to be guessed
                               pedestal_length(100),fScale(-1.), // values fixed by DAQ
                               thePadBin(6),
                               fPWBThres(ana_settings->GetDouble("DeconvModule","PWBthr")),
                               fPWBpeak(ana_settings->GetDouble("DeconvModule","PADthr")),
                               fPADmaxHits(ana_settings->GetInt("DeconvModule","PADMaxHits")),
                               isalpha16(false)
{
   //   Setup();
}

DeconvPAD::~DeconvPAD()
{
}

void DeconvPAD::Setup()
{
   SetupPWBs(0,0);
}

void DeconvPAD::SetupPWBs(TFile* fout, int run, bool norm, bool diag)
{
   int s = ReadPADResponseFile(fPADbinsize);
   std::cout<<"Deconv::SetupADCs() Response status: "<<s<<std::endl;
   assert(s>0);
   if( norm )
      {
         s = ReadPWBRescaleFile();
         std::cout<<"DeconvPAD::SetupPWBs() Response status: "<<s<<std::endl;
         assert(s>0);
      }
   else
      fPwbRescale.assign(32*576,1.0);

   if( diag && 0 )
      {
         fDiagnostic = true;
         if(fout) {
            fout->cd();
            if( !gDirectory->cd("paddiag") )
               gDirectory->mkdir("paddiag")->cd();
            else
               gDirectory->cd("paddiag");

            if( !hAvgRMSPad )
               hAvgRMSPad = new TH1D("hAvgRMSPad","Average Deconv Remainder Pad",500,0.,5000.);
         }
      }

   // by run settings
   if( run == 0 )
      fPWBdelay=52.;
   else if( run == 2246 || run == 2247 || run == 2248 || run == 2249 || run == 2251 )
      fPWBdelay = -50.;
   else if( run == 2272 || run ==  2273 || run == 2274 )
      fPWBdelay = 136.;
   else if( run >= 3870 && run < 900000 )
      fPWBdelay = 0.;

   if( run == 3169 || run == 3209 || run == 3226 || run == 3241 ||
       run == 3249 || run == 3250 || run == 3251 ||
       run == 3253 || run == 3254 || run == 3255 ||
       run == 3260 || run == 3263 || run == 3265 ||
       run == 3875 || run == 3866 || run == 3859 || run == 3855) // TrigBscMult
      fPWBdelay = -100.;

    if( run == 10419 || run == 10421 || run == 10422 || run == 10423 ) // Ext. Trigger
      fPWBdelay = -400.;

   if( run > 904100 && run <= 904400 )
      fPWBdelay = -112.;
   else if( run > 904400 ) fPWBdelay = -64.;
   if( run == (int)4294967295) fPWBdelay = 0.; // Simulation MIDAS file.
 
   std::vector<std::string> mask_files = ana_settings->GetStringVector("DeconvModule","PADmask");
   int pad_index;
   for(auto& f: mask_files)
      {
         std::string fname(getenv("AGRELEASE"));
         fname += "/ana/pads_masking/";
         fname += f;
         std::cout<<"DeconvPAD::SetupPWBs Reading Mask file: "<<fname<<std::endl;
         std::ifstream fin(fname.c_str());
         while(1)
            {
               fin>>pad_index;
               if( !fin.good() ) break;
               fPadIdxMask.push_back(pad_index);
            }
         fin.close();
      }
}

void DeconvPAD::Reset()
{

}

#ifdef BUILD_AG_SIM

int DeconvPAD::FindPadTimes(TClonesArray* PADsignals, std::vector<ALPHAg::TPadSignal>& signals)
{
   int Nentries = PADsignals->GetEntries();

   // prepare vector with wf to manipulate
   std::vector<ALPHAg::wfholder> PadWaves;
   PadWaves.reserve( Nentries );
   // clear/initialize "output" vectors
   std::vector<ALPHAg::electrode> PadIndex;
   PadIndex.reserve( Nentries );
   PadIndex.reserve( Nentries );

   std::string delimiter = "_";

   // find intresting channels
   unsigned int index=0; //ALPHAg::wfholder index
   for( int j=0; j<Nentries; ++j )
      {
         TWaveform* w = (TWaveform*) PADsignals->ConstructedAt(j);
         std::vector<int> data(w->GetWaveform());
         std::string wname = w->GetElectrode();

         size_t pos = wname.find(delimiter);
         std::string p = wname.substr(0, pos);
         if( p != "p" )
            std::cerr<<"Deconv Error: Wrong Electrode? "<<p<<std::endl;
         wname = wname.erase(0, pos + delimiter.length());

         pos = wname.find(delimiter);
         short col = std::stoi( wname.substr(0, pos) );
         assert(col<32&&col>=0);
         //std::cout<<"DeconvPAD::FindPadTimes() col: "<<col<<std::endl;
         wname = wname.erase(0, pos + delimiter.length());

         pos = wname.find(delimiter);
         int row = std::stoi( wname.substr(0, pos) );
         //std::cout<<"DeconvPAD::FindPadTimes() row: "<<row<<std::endl;
         assert(row<576&&row>=0);

         int coli = int(col);
         int pad_index = pmap.index(coli,row);
         assert(!std::isnan(pad_index));
         // CREATE electrode
         ALPHAg::electrode el(col,row);

         if( data.size() == 0 ) continue;

         // nothing dumb happens
         if( (int)data.size() < 410 + pedestal_length )
            {
               std::cerr<<"DeconvPAD::FindPadTimes ERROR wf samples: "
                        <<data.size()<<std::endl;
               continue;
            }
         // mask hot pads
         if( MaskPads(pad_index) ) continue;

         double ped = CalculatePedestal(data);
         double peak_h = GetPeakHeight(data,pad_index,ped);

         // CREATE WAVEFORM
         ALPHAg::wfholder waveform( index,
                                          std::next(data.begin(),pedestal_length),
                                          data.end());

         // Signal amplitude < thres is considered uninteresting
         if(peak_h > fPWBThres)
            {
               if(fTrace)
                  std::cout<<"\tsignal above threshold ch: "<<j<<" index: "<<index<<std::endl;

               // SUBTRACT PEDESTAL
               waveform.massage(ped,fPwbRescale.at(pad_index));

               // fill vector with wf to manipulate
               PadWaves.emplace_back( waveform );

               // STORE electrode
               PadIndex.push_back( el );

               ++index;
            }// max > thres
      }// channels

   // ============== DECONVOLUTION ==============
   Deconvolution(PadWaves,PadIndex,signals, 1, 1);   // no MT for now
   int nsig=signals.size();
   std::cout<<"DeconvPAD::FindPadTimes "<<nsig<<" found"<<std::endl;
   // ===========================================

   return nsig;
}
#endif

size_t DeconvPAD::BuildWFContainer(
    const FeamEvent* padSignals,
    std::vector<ALPHAg::wfholder>& PadWaves,
    std::vector<ALPHAg::electrode>& PadIndex,
    std::vector<ALPHAg::wf_ref>& feamwaveforms,  // to use in aged display
    std::vector<ALPHAg::TPadSignal>& PwbPeaks // waveform max
) const
{
   const std::vector<FeamChannel*> channels = padSignals->hits;
   const size_t n_chans = channels.size();
   PadIndex.clear();
   if(n_chans > fPADmaxHits){
      return -1;
   }
   // prepare vector with wf to manipulate
   PadWaves.reserve( n_chans );
   // clear/initialize "output" vectors
   PadIndex.clear();
   PadIndex.reserve( n_chans );

   if( fDiagnostic )
   {
      PwbPeaks.reserve( n_chans );
   }

   if( fAged )
   {
      feamwaveforms.reserve( n_chans );
   }

   // find intresting channels
   unsigned int index=0; //wfholder index
   for(unsigned int i = 0; i < n_chans; ++i)
      {
         const FeamChannel* ch = channels[i];
         if( !(ch->sca_chan>0) ) continue;
         short col = (ch->pwb_column * MAX_FEAM_PAD_COL + ch->pad_col + 1) % 32;

         if( col<0 || col >=32 )
            {
               std::cout<<"DeconvPAD::FindPadTimes() col: "<<col
                        <<" pwb column: "<<ch->pwb_column
                        <<" pad col: "<<ch->pad_col
                        <<" PAD SEC ERROR"<<std::endl;
            }
         const int row = ch->pwb_ring * MAX_FEAM_PAD_ROWS + ch->pad_row;
         if( row<0 || row>576 )
            {
               std::cout<<"DeconvPAD::FindPadTimes() row: "<<row
                        <<" pwb ring: "<<ch->pwb_ring
                        <<" pad row: "<<ch->pad_row
                        <<" PAD ROW EROOR"<<std::endl;
            }
         const int pad_index = pmap.index(col,row);
         assert(!std::isnan(pad_index));
         if( pad_index < 0 || pad_index >= (int)ALPHAg::_padchan )
            {
               std::cout<<"DeconvPAD::FindPadTimes() index: "<<pad_index
                        <<" col: "<<col
                        <<" row: "<<row
                        <<" PAD INDEX ERROR"<<std::endl;
               continue;
            }

         // mask hot pads
         if( MaskPads(pad_index) ) 
            {
               if (fTrace) std::cout<<"DeconvPAD::FindPadTimes(const FeamEvent*) MaskPad sec: "<<col<<", row:"<<row<<std::endl;
               continue;
            }

         if( ch->adc_samples.size() < 510 )
            {
               std::cerr<<"DeconvPAD::FindPadTimes ch: "<<i<<"\tpad: "<<pad_index
                        <<"\tERROR # of adc samples = "<<ch->adc_samples.size()
                        <<std::endl;
               continue;
            }

         const double ped = CalculatePedestal(ch->adc_samples);
         const double peak_h = GetPeakHeight(ch->adc_samples,pad_index,ped);
          
         if( fDiagnostic )
            {
               const double peak_t = GetPeakTime(ch->adc_samples);
               PwbPeaks.emplace_back(ALPHAg::electrode(col,row),peak_t,peak_h,0.);
            }


         // Signal amplitude < thres is considered uninteresting
         if(peak_h > fPWBThres)
            {
               if(fTrace)
                  std::cout<<"\tsignal above threshold ch: "<<i<<" index: "<<index<<" p.h.: "<<peak_h<<std::endl;

               // fill vector with wf to manipulate
               PadWaves.emplace_back( index,
                                          std::next(ch->adc_samples.begin(),pedestal_length),
                                          ch->adc_samples.end());

               // CREATE WAVEFORM
               ALPHAg::wfholder& waveform = PadWaves.back();

               // SUBTRACT PEDESTAL
               waveform.massage(ped,fPwbRescale[pad_index]);

               // STORE electrode
               PadIndex.emplace_back( col,row );

               if( fAged )
                  feamwaveforms.emplace_back(ALPHAg::electrode(col,row),new std::vector<double>(waveform.h));

               ++index;
            }// max > thres
      }// channels
   return n_chans;
   }

void DeconvPAD::Deconvolution(
    std::vector<ALPHAg::wfholder>& subtracted,
    const std::vector<ALPHAg::electrode> &fElectrodeIndex,
    std::vector<ALPHAg::TPadSignal>& signals, const int thread_no = 1, const int total_threads = 1) const
{
   if(subtracted.empty())
      return;

   const size_t nsamples = subtracted.back().h.size();
   assert(nsamples >= thePadBin);

   if (thread_no == 1)
   {
      signals.clear();
      signals.reserve(nsamples - thePadBin);
   }
   assert(nsamples < 1000);
   if( fTrace )
      std::cout<<"DeconvPAD::Deconvolution Subtracted Size: "<<subtracted.size()
               <<"\t# samples: "<<nsamples<<"\ttheBin: "<<thePadBin<<std::endl;
   if (thread_no == 1 && total_threads == 1)
      return DeconvolutionByRange( subtracted, fElectrodeIndex, signals, thePadBin,nsamples);
   else
   {
      const float slice_size = (nsamples - thePadBin) / (float)total_threads;
      const int start = floor(slice_size*(thread_no - 1) + thePadBin);
      int stop = floor( slice_size * thread_no + thePadBin );
      //I am the last thread
      if (thread_no == total_threads)
         stop = nsamples;
      assert (start <= stop);
      return DeconvolutionByRange( subtracted, fElectrodeIndex, signals, start ,stop);
   }
}

void DeconvPAD::DeconvolutionByRange(
    std::vector<ALPHAg::wfholder>& subtracted,
    const std::vector<ALPHAg::electrode> &fElectrodeIndex,
    std::vector<ALPHAg::TPadSignal>& signals, const int start, const int stop) const
{
   //std::cout<<"DECONV:"<< start <<" -> "<< stop <<std::endl;
   for(int b = start; b < stop; ++b)// b is the current bin of interest
      {
         // For each bin, order waveforms by size,
         // i.e., start working on largest first
         const std::vector<ALPHAg::wfholder*> histset = wforder( subtracted, b );
         // std::cout<<"DeconvPAD::Deconvolution bin of interest: "<<b
         //          <<" workable wf: "<<histset.size()<<std::endl;
         // this is useful to split deconv into the "Subtract" method
         // map ordered wf to corresponding electrode
         for (auto const it : histset)
            {
               const unsigned int i = it->index;
               const std::vector<double>& wf=it->h;
               const ALPHAg::electrode &anElectrode = fElectrodeIndex[i];
               // number of "electrons"
               const double ne = anElectrode.gain * fScale * wf[b] / fPadResponse[thePadBin];
               if( ne >= fPWBpeak )
                  {
                     // loop over all bins for subtraction
                     SubtractPAD(it,b,ne,fElectrodeIndex);
                     if( int( b - thePadBin) >= 0)
                        {
                           // time in ns of the bin b centre
                           const double t = ( double(b - thePadBin) + 0.5 ) * double(fPADbinsize) + fPWBdelay;
                           signals.emplace_back(anElectrode,t,ne,GetNeErr(ne,it->val));
                        }
                  }// if deconvolution threshold Avalanche Size
            }// loop set of ordered waveforms
      }// loop bin of interest
   return;
}

void DeconvPAD::LogDeconvRemaineder( std::vector<ALPHAg::wfholder>& PadWaves )
{

   if( fDiagnostic )
   {
      // prepare control variable (deconv remainder) vector
      // resRMS_p.clear();
      // resRMS_p.reserve( PadWaves.size() );
      double mr=0.,r=0.;
      // calculate remainder of deconvolution
      for(const auto& s: PadWaves)
      {
         r+=sqrt( std::inner_product(s.h.begin(), s.h.end(), s.h.begin(), 0.)
                     / static_cast<double>(s.h.size()) );
         // resRMS_p.push_back( sqrt(
         //                          std::inner_product(s->h->begin(), s->h->end(), s->h->begin(), 0.)
         //                          / static_cast<double>(s->h->size()) )
         //                     );
         ++mr;
      }
      if( mr != 0. )
         r /= mr;
      hAvgRMSPad->Fill(r);
   }
}

void DeconvPAD::SubtractPAD(ALPHAg::wfholder* hist1,
                         const int b,
                         const double ne,
                         const std::vector<ALPHAg::electrode> &fElectrodeIndex) const
{
   const unsigned int i1 = hist1->index;
   const ALPHAg::electrode& wire1 = fElectrodeIndex[i1]; // mis-name for pads

   const int respsize = fPadResponse.size();

   const double collectivegain = ne/fScale/wire1.gain;

   std::vector<double> &wf1 = hist1->h;
   const int wf1size = wf1.size();

   const int start = b - thePadBin;
   const int range = std::min(wf1size - start, respsize );

   int respBin = 0;
   for(int bb = start; bb < start + range; ++bb)
      {
         // Remove signal tail for waveform we're currently working on
         wf1[bb] -= collectivegain * fPadResponse[respBin];
         ++respBin;
      }// bin loop: subtraction
}

std::vector<ALPHAg::wfholder*> DeconvPAD::wforder(std::vector<ALPHAg::wfholder> & subtracted, const unsigned b) const
{
   // For each bin, order waveforms by size,
   // i.e., start working on largest first

   const size_t size = subtracted.size();
   std::vector<ALPHAg::wfholder*> histset(size,nullptr);
   //   std::cout<<"DeconvPAD::wforder subtracted size: "<<size<<" @ bin = "<<b<<std::endl;
   for(size_t i=0; i<size;++i)
      {
         //         std::cout<<"wf# "<<i;
         ALPHAg::wfholder& mh=subtracted[i];
         // std::cout<<"\twf index: "<<mh->index;
         // std::cout<<"\twf size: "<<mh->h->size();
         //std::cout<<"\twf bin: "<<b<<std::endl;
         mh.val = fScale*mh.h[b];
         //         std::cout<<"\twf val: "<<mh->val<<std::endl;
         histset[i] = &mh;
      }
   std::sort(histset.begin(), histset.end(),wf_comparator);
   return histset;
}


int DeconvPAD::ReadResponseFile(const int padbin)
{
   return ReadPADResponseFile( padbin );
}

int DeconvPAD::ReadPADResponseFile( const int padbin )
{
   std::string filename(getenv("AGRELEASE"));
   filename+="/ana/padResponse_deconv.dat";
   std::cout << "DeconvPADModule:: Reading in response file (pad) " << filename << std::endl;
   std::ifstream respFile(filename.c_str());
   if( !respFile.good() ) return 0;
   double binedge, val;
   std::vector<double> resp;
   while(1)
      {
         respFile >> binedge >> val;
         if( !respFile.good() ) break;
         resp.push_back(val);
      }
   respFile.close();
   fPadResponse=Rebin(resp, padbin);

   double frac = 0.1;
   double max = *std::max_element(fPadResponse.begin(), fPadResponse.end());
   double thres = frac*max;
   for(unsigned b=0; b<fPadResponse.size(); ++b)
      {
         if( fPadResponse[b] > thres )
            thePadBin=b;
      }
   if( fTrace )
      std::cout<<"DeconvPADModule::ReadResponseFile pad max: "<<max<<"\tpad bin: "<<thePadBin<<std::endl;

   return int(fPadResponse.size());
}

std::vector<double> DeconvPAD::Rebin(const std::vector<double> &in, int binsize, double ped)
{
   if( fTrace )
      std::cout<<"DeconvPAD::Rebin "<<binsize<<std::endl;
   if(binsize == 1) return in;
   std::vector<double> result;
   result.reserve(in.size()/binsize);
   for(unsigned int i = 0; i < in.size(); i++)
      {
         if(i / binsize == result.size()-1)
            result.back() += double(in[i])-ped;
         else if(i / binsize == result.size())
            result.push_back(double(in[i])-ped);
      }
   if(result.size()*binsize > in.size())
      {
         result.pop_back();
         if( fTrace )
            std::cout << "DeconvPAD::Rebin: Cannot rebin without rest, dropping final "
                      << in.size() % result.size() << std::endl;
      }
   return result;
}

int DeconvPAD::ReadRescaleFile()
{
   return ReadPWBRescaleFile();
}

int DeconvPAD::ReadPWBRescaleFile()
{
   std::string basepath(getenv("AGRELEASE"));
   std::ifstream fpwbres(basepath+"/ana/PwbRescale.dat");
   double rescale_factor;
   while(1)
      {
         fpwbres>>rescale_factor;
         if( !fpwbres.good() ) break;
         fPwbRescale.push_back(rescale_factor);
      }
   fpwbres.close();

   if( fPwbRescale.size() == 32*576 )
      std::cout<<"DeconvAWModule BeginRun PWB rescaling factors OK"<<std::endl;
   else
      std::cout<<"DeconvAWModule BeginRun PWB rescaling factors NOT ok (size: "
               <<fPwbRescale.size()<<")"<<std::endl;
   return int(fPwbRescale.size());
}

void DeconvPAD::PrintPWBsettings()
{
   std::cout<<"-------------------------"<<std::endl;
   std::cout<<"Deconv Settings"<<std::endl;
   std::cout<<" PWB max: "<<fPWBmax<<std::endl;
   std::cout<<" PWB range: "<<fPWBrange<<std::endl;
   std::cout<<" PWB time bin: "<<fPADbinsize<<" ns"<<std::endl;
   std::cout<<" PWB delay: "<<fPWBdelay<<" ns"<<std::endl;
   std::cout<<" PWB thresh: "<<fPWBThres<<std::endl;
   std::cout<<" PAD thresh: "<<fPWBpeak<<std::endl;
   std::cout<<"-------------------------"<<std::endl;
   std::cout<<"Masked Electrodes"<<std::endl;
   std::cout<<"PAD: ";
   for(auto it=fPadIdxMask.begin(); it!=fPadIdxMask.end(); ++it)
       std::cout<<*it<<",";
   std::cout<<"\n"<<std::endl;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
