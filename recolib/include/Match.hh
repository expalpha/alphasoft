#ifndef __MATCH__
#define __MATCH__ 1

#include <vector>
#include <set>
#include "TH1D.h"
#include "TH2D.h"

#include "SignalsType.hh"
#include "AnaSettings.hh"
#include "TFile.h"

#include <mutex>          // std::mutex

class Match
{
private:
   bool fTrace;
   bool fDebug;
   bool fMT;
   bool diagnostic;

   std::mutex* manalzer_global_mtx;

   const AnaSettings* ana_settings;
   const double fCoincTime; // ns

   const double charge_dist_scale;  // set to zero to not use, other value gets multiplied by padThr
   const double padThr;               // needed for wire-dependent pad threshold

   const double phi_err = ALPHAg::_anodepitch*ALPHAg::_sq12;

   double relCharge[8] = {1., 1.33687717, 1.50890722, 1.56355571, 1.56355571, 1.50890722, 1.33687717, 1.};



   //bool diagnostic;

   void SortPointsAW(  const std::pair<double,int>& pos,
                       std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>& vec,
                       std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>,std::greater<int>>& spaw );
   void SortPointsAW(  std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>& vec,
                       std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>,std::greater<int>>& spaw );

   //  void SortPointsAW(  const std::pair<double,int>& pos,
   //		      std::vector<std::pair<signal,signal>*>& vec,
   //		      std::map<int,std::vector<std::pair<signal,signal>*>>& spaw );
   void CombPointsAW(std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>,std::greater<int>>& spaw,
                     std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>>& merger);
   void CombPointsAW(std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>>& spaw,
                     std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>>& merger);

   uint MergePoints(std::map<int,std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>*>>& merger,
                    std::vector<std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>>& merged,
                    uint& number_of_merged);

public:
   Match(const AnaSettings* ana_set, bool mt=false):
      fTrace(false),
      fDebug(false),
      fMT(mt),
      ana_settings(ana_set),
      fCoincTime(     ana_settings->GetDouble("MatchModule","coincTime")),
      charge_dist_scale(ana_settings->GetDouble("MatchModule","pad_charge_dist_scale")),
      padThr(         ana_settings->GetDouble("DeconvModule","PADthr"))// This DeconvModule setting is also needed here, for wire-dependent threshold
   {}

   Match(const std::string& json, const std::vector<std::string>& args): Match(new AnaSettings(json.c_str(), args))  {}
   // ~Match();
   void SetGlobalLockVariable(std::mutex* _manalyzerLock)
   {
      manalzer_global_mtx=_manalyzerLock;
   }
   // void Init();
   // void Setup(TFile* OutputFile);

   void MatchElectrodes(std::vector<ALPHAg::TWireSignal>& awsignals);
   std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> > MatchElectrodes(std::vector<ALPHAg::TWireSignal>& awsignals,
                                                                                    std::vector<ALPHAg::TPadSignal>& padsignals);
   std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> >  FakePads(std::vector<ALPHAg::TWireSignal>& awsignals);

   std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> > CombPoints(std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> >& spacepoints);

   void SetTrace(bool t) { fTrace=t; }
   void SetDebug(bool d) { fDebug=d; }
   void SetDiagnostic(bool d) { diagnostic=d; }
   void SetMultiThread(bool m) { fMT=m; }
};




#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
