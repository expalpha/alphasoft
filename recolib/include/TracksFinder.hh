// Tracks finder class definition
// for ALPHA-g TPC analysis
// Author: A.Capra
// Date: Sep. 2016

#ifndef __TFINDER__
#define __TFINDER__ 1
#include "TSpacePoint.hh"
#include <vector>
#include <list>
#include <deque>

typedef std::deque<int> track_t;
enum finderChoice { base, adaptive, iterative, exhaustive };

#define BUILD_EXCLUSION_LIST 0

class TracksFinder
{
protected:
   std::vector<TSpacePoint*> fPointsArray;
   int fNtracks;

   double fPointMaxRad;
   double fSeedRadCut;
   double fPointsDistCut;
   double fSmallRad;
   int fNpointsCut;
   int fFinderChoice;

   // Reasons for failing:
   int track_not_advancing;
   int points_cut;
   int rad_cut;
   int length_cut;

   bool debug;

private:
#if BUILD_EXCLUSION_LIST
   //Do we care about keeping a list of excluded TSpacePoints if we don't use a map anymore?
   //I am guessing not so putting it behind a pre-compiler if statement and turning it off:
   std::vector<TSpacePoint*> fExclusionList;
#endif

public:
   TracksFinder(const std::vector<TSpacePoint>*);
   virtual ~TracksFinder();

   inline void SetSeedRadCut(double cut)    { fSeedRadCut=cut; }
   inline double GetSeedRadCut() const      { return fSeedRadCut; }
   inline double GetPointMaxRadCut() const      { return fPointMaxRad; }
   inline void SetPointMaxRadCut(double cut) { fPointMaxRad=cut; }
   inline void SetPointsDistCut(double cut) { fPointsDistCut=cut; }
   inline double GetPointsDistCut() const   { return fPointsDistCut; }
   inline void SetSmallRadCut(double cut)   { fSmallRad=cut; }
   inline double GetSmallRadCut() const     { return fSmallRad; }
   inline void SetNpointsCut(int cut)       { fNpointsCut=cut; }
   inline int GetNpointsCut() const         { return fNpointsCut; }
   inline void SetFinderChoice(int c)       { fFinderChoice=c; }
   inline int GetFinderChoice() const         { return fFinderChoice; }

   virtual void Clear();

   virtual int RecTracks(std::vector<track_t>& fTrackVector);

   inline void GetReasons(int& t, int& n, int& r, int& l) { t=track_not_advancing; n=points_cut; r=rad_cut; l=length_cut;}

   virtual inline void SetDebug(bool d=true) { debug = d; } 
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
