// Vertex class definition
// for ALPHA-g TPC analysis
// Author: A.Capra 
// Date: May 2014

#ifndef __TFITVERTEX__
#define __TFITVERTEX__ 1

#include <TObject.h>
#include <TVector3.h>
#include <vector>

#include "TFitHelix.hh"

typedef enum vertexStatus { 
  kChi2failed = -4,
  kUninitialized = -3,
  kNoGoodHelices = -2,
  kFailToFindPair = -1,
  kOnlyOneGoodHelix = 0,
  kOnlyTwoGoodHelices = 1,
  kDidntImproveHelix = 2,
  kVertexImproved = 3
} vertexstatus_t;

class TFitVertex : public TObject
{
private:
  int fID;
  std::vector<TFitHelix*> fHelixArray;
  int fNhelices;

  double fchi2;     // vertex chi^2
  TVector3 fVertex; // vertex position
  TVector3 fVertexError2;

  int fNumberOfUsedHelices;
  std::vector<TFitHelix*> fHelixStack;

  double fChi2Cut;
  unsigned int fFitIterations;

  TFitHelix* fInit0; // seed helix
  int fSeed0Index;
  double fSeed0Par; // arc length param.
  TVector3 fSeed0Point; // closest point to other helix
  TFitHelix* fInit1;
  int fSeed1Index;
  double fSeed1Par; // arc length param.
  TVector3 fSeed1Point; // closest point to other helix

  double fSeedchi2; // stage 1 chi2
  TVector3 fMeanVertex; // stage 1 position
  TVector3 fMeanVertexError2;

  double fNewChi2; // stage 2 chi2
  TVector3 fNewVertex; // stage 2 vertex
  TVector3 fNewVertexError2;  

  double fNewSeed0Par; // arc length param. helix new vertex
  double fNewSeed1Par;

  double fDCA; // stage 2 chi2
  // stage 1
  vertexstatus_t FirstPass();
  double FindMinDistance(double& s0, double& s1);
  TVector3 EvaluateMeanPoint();
  TVector3 EvaluateMeanPoint(TVector3 p0, TVector3 e0, 
			     TVector3 p1, TVector3 e1);
  TVector3 EvaluateMeanPointError2();
  
  // stage 3
  double FindNewVertex(double* p, double* e);
  
  // new method stage 1
  double FindSeed();

  // new method stage 2
  vertexstatus_t SecondPass();
  double Recalculate();

  // new method stage 3
  vertexstatus_t ThirdPass();
  vertexstatus_t Improve();

  void AssignHelixStatus();

public:
  TFitVertex();
  TFitVertex(int id);
  ~TFitVertex();

  int AddHelix(TFitHelix*);
  inline const std::vector<TFitHelix*>* GetHelixArray()  {return &fHelixArray;}
  inline int GetNumberOfAddedHelix() const {return fNhelices;}

  void SetID(int id) { fID = id; }

  inline void SetChi2Cut(double cut) {fChi2Cut=cut;}
  inline double GetChi2Cut() {return fChi2Cut;}
  inline void SetFitIterations(unsigned int n) {fFitIterations=n;}
  inline unsigned int GetFitIterations() {return fFitIterations;}

  // main function to reconstruct the vertex
  vertexstatus_t Calculate(const int thread_no = 1, const int thread_count = 1);

  inline TFitHelix* GetInit0() const {return fInit0;}
  inline TFitHelix* GetInit1() const {return fInit1;}

  inline const std::vector<TFitHelix*>* GetHelixStack() const {return &fHelixStack;}
  inline int GetNumberOfHelices() const         {return fNumberOfUsedHelices;}

  int FindDCA();
  inline void SetDCA(double dca) {fDCA=dca;}
  inline double GetDCA() {return fDCA;}

  inline double GetRadius()    const {return fVertex.Perp();}
  inline double GetAzimuth()   const {return fVertex.Phi();}
  inline double GetElevation() const {return fVertex.Z();}

  inline double GetChi2()     const {return fchi2;}
  inline double GetSeedChi2() const {return fSeedchi2;}
  inline double GetNewChi2()  const {return fNewChi2;}

  inline const TVector3* GetVertex()           const {return &fVertex;}
  inline const TVector3* GetVertexError2()     const {return &fVertexError2;}    
  inline const TVector3* GetNewVertex()        const {return &fNewVertex;}
  inline const TVector3* GetNewVertexError2()  const {return &fNewVertexError2;}
  inline const TVector3* GetMeanVertex()       const {return &fMeanVertex;}
  inline const TVector3* GetMeanVertexError2() const {return &fMeanVertexError2;}

  virtual void Print(Option_t *option="rphi") const;
  virtual void Clear(Option_t *option="");
  virtual void Reset();

  ClassDef(TFitVertex,2)
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
