// Tracks finder class definition
// for ALPHA-g TPC analysis
// Author: A.Capra, G.Smith
// Date: Sep. 2016, Oct. 2022

#ifndef __EXFINDER__
#define __EXFINDER__ 1
#include "TSpacePoint.hh"
#include "TracksFinder.hh"
#include <TVector3.h>
#include <vector>
#include <list>
#include <set>


class ExhaustiveFinder: public TracksFinder
{
private:

   std::vector<bool> fHitGood;
   std::vector<bool> fHitUsed;
   std::vector<std::vector<bool>> fConnected;
   const double fTouchDistanceXY;
   const double fTouchDistanceZ;

public:
   ExhaustiveFinder(const std::vector<TSpacePoint>*, const double TouchDistanceXY, const double TouchDistanceZ);
   ~ExhaustiveFinder(){};
   
   inline double GetTouchDistanceXY() const {return fTouchDistanceXY;}
   inline double GetTouchDistanceZ() const {return fTouchDistanceZ;}

   virtual int RecTracks(std::vector<track_t>&);
   int MapConnections();
   int AddConnectedPoints(track_t& track, int i_point, int Npoints);

};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
