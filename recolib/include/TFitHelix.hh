// Helix class definition
// for ALPHA-g TPC analysis
// Author: A.Capra
// Date: May 2014

#ifndef __TFITHELIX__
#define __TFITHELIX__ 1

#include <TObject.h>
#include <TObjArray.h>
#include <TMath.h>
#include <TVector3.h>
#include <TMinuit.h>

#include "TPCconstants.hh"
#include "TTrack.hh"

struct Vector2 {
  double X;
  double Y;
};


class TStoreHelix;

class TSpacePoint;
class TFitHelix : public TTrack
{
private:
  double fc;
  double fRc;
  double fphi0;
  double fD;

  double flambda;
  double fz0;

  double fa;
  double fx0;
  double fy0;

  double ferr2c;
  double ferr2Rc;
  double ferr2phi0;
  double ferr2D;

  double ferr2lambda;
  double ferr2z0;

  bool fDoubleBranched;

  int fBranch;
  double fBeta;
  double fTrackNum;

  TVector3 fMomentum;  // MeV/c
  TVector3 fMomentumError;

  std::vector<int> fBarHitsFirstBranch;
  std::vector<int> fBarHitsSecondBranch;
  bool fIsPileup;

  static const int fNpar=5;

  static const int fRNpar=3;
  double fchi2R;
  int fStatR;

  static const int fZNpar=2;
  double fchi2Z;
  int fStatZ;

  double fChi2RCut;
  double fChi2ZCut;
  double fChi2RMin;
  double fChi2ZMin;
  double fcCut;
  double fDCut;
  double fpCut;
  int fRFiterations;
  int fZFiterations;
  double fHelPreferStraight;
  double fHelSmallCurvature;

  // parameters initialization
  void Initialization_Straight(double* Ipar);
  void Initialization_Curved(double* Ipar, bool sort_by_z);
  void Initialization_Z(double* Ipar);

  // utilities to calculate internal helix parameters
  double GetBeta      ( const double r2, const double Rc, const double D ) const;

  double GetArcLength ( const double r2, const double Rc, const double D ) const;
  double GetArcLength_( const double r2, const double Rc, const double D ) const;
  static double GetTheta( const double x1, const double y1, const double _Rc, const double _phi0, const double _D );
  static double GetTheta( const double z1, const double _Rc, const double _lambda, const double _z0);

  // Multiple Scattering angle variance
  double VarMS() const;

public:
  TFitHelix();
  //  TFitHelix(double B=0);
  TFitHelix(const TTrack& atrack);
  TFitHelix(TObjArray*);
  TFitHelix( const TFitHelix& right );
  TFitHelix(const TStoreHelix&);

  TFitHelix& operator=( const TFitHelix& right );
  
  ~TFitHelix();

  inline double GetC() const       {return fc;}
  inline void SetC(double c)       {fc=c;}
  inline double GetRc() const      {return fRc;}
  inline void SetRc(double rc)     {fRc=rc;}
  inline double GetPhi0() const    {return fphi0;}
  inline void SetPhi0(double phi0) {fphi0=phi0;}
  inline double GetD() const       {return fD;}
  inline void SetD(double d)       {fD=d;}

  inline double GetLambda() const {return flambda;}
  inline void SetLambda(double l) {flambda=l;}
  inline double GetZ0() const     {return fz0;}
  inline void SetZ0(double z)     {fz0=z;}

  inline double GetA() const       {return fa;}
  inline void SetA(double a)       {fa=a;}

  inline double GetErrC() const       {return ferr2c;}
  inline void SetErrC(double c)       {ferr2c=c;}
  inline double GetErrRc() const       {return ferr2Rc;}
  inline void SetErrRc(double rc)      {ferr2Rc=rc;}
  inline double GetErrPhi0() const    {return ferr2phi0;}
  inline void SetErrPhi0(double phi0) {ferr2phi0=phi0;}
  inline double GetErrD() const       {return ferr2D;}
  inline void SetErrD(double d)       {ferr2D=d;}

  inline double GetErrLambda() const {return ferr2lambda;}
  inline void SetErrLambda(double l) {ferr2lambda=l;}
  inline double GetErrZ0() const     {return ferr2z0;}
  inline void SetErrZ0(double z)     {ferr2z0=z;}

  inline double GetRchi2() const {return fchi2R;}
  inline void SetRchi2(double rchi2) {fchi2R = rchi2;}
  inline int GetRDoF() const     {return 2*GetNumberOfPoints() - fRNpar;}
  inline int GetStatR() const    {return fStatR;}

  inline double GetZchi2() const {return fchi2Z;}
  inline void SetZchi2(double zchi2) {fchi2Z = zchi2;}
  inline int GetZDoF() const     {return GetNumberOfPoints() - fZNpar;}
  inline int GetStatZ() const    {return fStatZ;}

  inline void SetXY0()
  {
    //#ifdef _GNU_SOURCE
    //  sincos(TMath::RadToDeg()*fphi0,&fx0,&fy0);
    //  fx0=-fD*fx0;
    //  fy0=fD*fy0;
    //#else
      fx0=-fD*TMath::Sin(fphi0); fy0=fD*TMath::Cos(fphi0);
    //#endif
  }
  inline double GetX0() const {return fx0;}
  inline void SetX0(double x) {fx0 = x;}
  inline double GetY0() const {return fy0;}
  inline void SetY0(double y) {fy0 = y;}

  inline void SetDoubleBranched(bool d) { fDoubleBranched = d;}
  inline bool IsDoubleBranched() const { return fDoubleBranched;}

  inline void SetBranch(int br) { fBranch = br;}
  inline int GetBranch() const {return fBranch;}
  inline double GetFBeta() const { return fBeta;}
  inline int GetTrackNum() const {return fTrackNum;}
  inline void SetTrackNum(int tr) {fTrackNum=tr;}

  inline void SetChi2RCut(double cut) {fChi2RCut=cut;}
  inline double GetChi2RCut() const   {return fChi2RCut;}
  inline void SetChi2ZCut(double cut) {fChi2ZCut=cut;}
  inline double GetChi2ZCut() const   {return fChi2ZCut;}
  inline void SetChi2RMin(double min) {fChi2RMin=min;}
  inline double GetChi2RMin() const   {return fChi2RMin;}
  inline void SetChi2ZMin(double min) {fChi2ZMin=min;}
  inline double GetChi2ZMin() const   {return fChi2ZMin;}
  inline void SetRFiterations(int n_iter) {fRFiterations = n_iter;}
  inline double GetRFiterations() const {return fRFiterations;}
  inline void SetZFiterations(int n_iter) {fZFiterations = n_iter;}
  inline double GetZFiterations() const {return fZFiterations;}
  inline void SetHelPreferStraight(double p) {fHelPreferStraight = p;}
  inline double GetHelPreferStraight() const {return fHelPreferStraight;}
  inline void SetHelSmallCurvature(double p) {fHelSmallCurvature = p;}
  inline double FetHelSmallCurvature() const {return fHelSmallCurvature;}
 
  inline void SetcCut(double cut) {fcCut=cut;}
  inline double GetcCut() const   {return fcCut;}
  inline void SetDCut(double cut) {fDCut=cut;}
  inline double GetDCut() const   {return fDCut;}

  inline void SetMomentumCut(double cut) {fpCut=cut;}
  inline double GetMomentumCut() const   {return fpCut;}

  inline TVector3 GetMomentumV() const      {return fMomentum;}// MeV/c
  inline TVector3 GetMomentumVerror() const {return fMomentumError;}

  inline void SetMomentumV(double px, double py, double pz) {fMomentum.SetX(px); fMomentum.SetY(py); fMomentum.SetZ(pz);}

  inline int GetNBarHitsFirstBranch() const {return fBarHitsFirstBranch.size();}
  inline int GetNBarHitsSecondBranch() const {return fBarHitsSecondBranch.size();}
  inline std::vector<int> GetBarHitsFirstBranch() const {return fBarHitsFirstBranch;}
  inline std::vector<int> GetBarHitsSecondBranch() const {return fBarHitsSecondBranch;}
  inline int GetBarHitFirstBranch(int i) const {return fBarHitsFirstBranch.at(i);}
  inline int GetBarHitSecondBranch(int i) const {return fBarHitsSecondBranch.at(i);}
  inline void AddBarHitFirstBranch(int barHitNum) {fBarHitsFirstBranch.push_back(barHitNum); }
  inline void AddBarHitSecondBranch(int barHitNum) {fBarHitsSecondBranch.push_back(barHitNum); }
  inline void SetIsPileup(bool isPileup) {fIsPileup=isPileup;}
  inline bool IsPileup() const {return fIsPileup;}
  inline bool HasBarHit() const {return (fBarHitsFirstBranch.size()+fBarHitsSecondBranch.size())>0;}
  double CalculateResiduals();

  static std::array<double,2> GetClosestToPoint( const double x1, const double y1, const double Rc, const double phi0, const double D);
  static TVector3 GetClosestToPoint( const double x1, const double y1, const double z1, const double Rc, const double phi0, const double D, const double lambda, const double z0);
  TVector3 GetClosestToPoint( const double x1, const double y1, const double z1) const;
  TVector3 GetClosestToPointIncludingBackwards( const double x1, const double y1, const double z1) const { return GetClosestToPoint(x1,y1,z1,fRc,fphi0,fD,flambda,fz0); }
  static int GetDirection(const double x0, const double y0, const double Rc, const double phi0, const double D);
  int GetDirection(const double x0, const double y0) const { return GetDirection(x0, y0, fRc, fphi0, fD); }
  static TVector2 GetPointAtZ( const double z1, const double Rc, const double phi0, const double D, const double lambda, const double z0);


  // LS fit to helix canonical form

  virtual void FitM2(bool fit=true, bool i1=true, bool i1b=true, bool i2=true, bool i2b=true);
  void RadialFit(double* Ipar);
  void AxialFit(double* Ipar);

  // Evaluate the function for fitting
  Vector2 Evaluate ( const double r2, const double Rc, const double phi, const double D ) const; // +1 branch
  Vector2 Evaluate ( const double r2, const double Rc, const double u0, const double v0, const double D ) const; // +1 branch
  Vector2 Evaluate_( const double r2, const double Rc, const double phi, const double D )  const; // -1 branch
  Vector2 Evaluate_( const double r2, const double Rc, const double u0, const double v0, const double D ) const; // -1 branch

  double Evaluate   ( const double s,  const double l, const double z0 ) const;            // axial fit

  // unused but useful
  // +1 branch
  TVector3 Evaluate ( const double r2, const double Rc, const double phi, const double D, const double l, const double z0  ) const;
  // -1 branch 
  TVector3 Evaluate_( const double r2, const double Rc, const double phi, const double D, const double l, const double z0  ) const;
  // +1 branch, beta +ve root

  // Radial arclength parameter for fitting/vertexing
  double GetArcLength ( const double r2 ) const;

  // Evaluate the function for plotting
  TVector3 Evaluate( const double r2 ) const;
  // Evaluate errors for vertexing
  TVector3 EvaluateErrors2( const double r2 ) const;

  // Evaluate function and errors for vertexing
  TVector3 GetPosition(const double s) const;
  TVector3 GetError2(const double s) const;

  double Momentum(); // returns pT in MeV/c

  virtual double GetApproxPathLength() const;
  void AddMSerror();

  int TubeIntersection(TVector3&, TVector3&, 
		       const double radius = ALPHAg::_trapradius) const;

  virtual bool IsGood();
  bool IsGoodChiSquare();
  void Reason();
  bool IsDuplicated(const TFitHelix*,const double) const;

  virtual void Print(Option_t *option="") const;
  virtual void Clear(Option_t *option="");

  // for sorting helix arrays from lowest c first to highest c last
  inline bool IsSortable() const { return true; }
  int Compare(const TObject*) const;

  ClassDef(TFitHelix,6)
};

bool SortMomentum(const TFitHelix& aHelix, const TFitHelix& bHelix);

struct HelixAndIDs {
   TFitHelix hel;
   std::pair<int,int> ids;
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
