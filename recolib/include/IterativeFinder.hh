// Tracks finder class definition
// for ALPHA-g TPC analysis
// Author: A.Capra, G.Smith
// Date: Sep. 2016, Oct. 2022

#ifndef __ITFINDER__
#define __ITFINDER__ 1
#include "TSpacePoint.hh"
#include "TracksFinder.hh"
#include <vector>
#include <list>
#include <set>


class IterativeFinder: public TracksFinder
{
private:
   const double fLastPointRadCut;
   const double fPointsRadCut;
   const double fPointsPhiCut;
   const double fPointsZedCut;
   const double fMaxIncreseAdapt;
   const double fAdaptiveness;
   const int fTrackIterations;
   const double fTrackLengthCut;

   std::vector<bool> hit_used;

public:
   IterativeFinder(const std::vector<TSpacePoint>*, const double MaxIncrease, const double LastPointRadCut, const double Adaptiveness, const int fTrackIterations, const double fTrackLengthCut );
   ~IterativeFinder(){};

   inline double GetMaxIncreseAdapt() const {return fMaxIncreseAdapt;}
   inline double GetLastPointRadCut() const { return fLastPointRadCut; }
   inline double GetAdaptiveNess() const { return fAdaptiveness; }
   inline double GetTrackIterations() const { return fTrackIterations; }
   inline double GetTrackLengthCut() const { return fTrackLengthCut; }

   virtual int RecTracks(std::vector<track_t>&);

   int NextPoint( const TSpacePoint*, const int, const int, double&, track_t&) ;
   int NextPointBack( const TSpacePoint*, const int, double&, track_t&) ;
   int Search( const TSpacePoint*, int, const int, track_t&) ;
   int SearchBack( const TSpacePoint*, int, track_t&) ;
   int NextPoint( const int, double, double, double, track_t&) const;
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
