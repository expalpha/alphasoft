#ifndef _AnaSettings_
#define _AnaSettings_

#include "json.hpp"
#include <fstream>
#include <string>
#include "TString.h"
#include "TObjString.h"
#include <sstream>
#include <iostream>

using json = nlohmann::json;
class AnaSettings
{
 private:

   TString filename;
   json settings;
 public: 
   AnaSettings(const char* filename, const std::vector<std::string> &args);
   static void Help();
   virtual ~AnaSettings();
   std::string removeComments(std::string prgm); //Convert hjson to json in memory
   
   bool HasVar(char* module, const char* var) const;
   
   double GetDouble(const char* Module, const char* Variable) const;
   void SetDouble(const char* Module, const char* Variable, const double value);
   int GetInt(const char* Module, const char* Variable) const;
   void SetInt(const char* Module, const char* Variable, const int value);
   bool GetBool(const char* Module, const char* Variable) const;
   void SetBool(const char* Module, const char* Variable, const bool value);
   std::string GetString(const char* Module, const char* Variable) const;
   void SetString(const char* Module, const char* Variable, const std::string value);
   std::vector<int> GetIntVector(const char* Module, const char* Variable) const;
   std::vector<std::string> GetStringVector(const char* Module, const char* Variable) const;
   virtual void Print();

   const json* GetSettings() const { return &settings; }
   TObjString GetSettingsString();

   TString GetFilename() { return filename; }
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
