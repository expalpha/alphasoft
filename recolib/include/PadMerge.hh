#ifndef __PADMERGE__
#define __PADMERGE__ 1

#include <vector>
#include <set>
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"

#include "SignalsType.hh"
#include "AnaSettings.hh"
#include "TFile.h"

#include <mutex>          // std::mutex

class PadMerge
{
private:
   const bool fTrace;
   const bool fDebug;
   bool diagnostic;
   bool padCogDoFit;
   bool padCogUseMean;
   bool padCogFixSigma;

   int edge = 0;
   int noedge = 0;
   std::mutex* manalzer_global_mtx;

   const AnaSettings* ana_settings;
   const double fCoincTime; // ns

   const int maxPadGroups; // max. number of separate groups of pads coincident with single wire signal
   const int padsNmin;     // minimum number of coincident pad hits to attempt reconstructing a point
   const double padSigma; // width of single avalanche charge distribution = 2*(pad-aw)/2.34
   const double padSigmaD; // max. rel. deviation of fitted sigma from padSigma
   const double padFitErrThres; // max. accepted error on pad gaussian fit mean
   // const bool use_mean_on_spectrum;
   // const double spectrum_mean_multiplyer; //if use_mean_on_spectrum is true, this is used.
   // const double spectrum_cut;              //if use_mean_on_spectrum is false, this is used.
   const double spectrum_width_min;

   const double grassCut;       // don't consider peaks smaller than grassCut factor of a
   const double goodDist;       // neighbouring peak, if that peak is closer than goodDist

   const int fNpadsCut; // if the number of reconstructed pads is too large quit

   const double zed_err = ALPHAg::_padpitch*ALPHAg::_sq12;
   int CentreOfGravityFunction = -1; //One day we can maybe make this const :)

   std::pair<std::set<short>,std::vector< std::vector<ALPHAg::TPadSignal> >> PartitionBySector(
      const std::vector<ALPHAg::TPadSignal>& padsignals) const;
   std::vector< std::vector<ALPHAg::TPadSignal> > PartitionByTime(
      const std::vector<ALPHAg::TPadSignal>& sig ) const;

   void CentreOfGravity(
      const std::vector<ALPHAg::TPadSignal> &vsig,
      std::vector<ALPHAg::TPadSignal>& combpads ); // #1
   void CentreOfGravity_blobs(
      const std::vector<ALPHAg::TPadSignal> &vsig,
      std::vector<ALPHAg::TPadSignal>& combpads); // #2

   std::vector<std::pair<double, double> > FindBlobs(TH1D *h);

   std::vector<std::pair<double, double> > FindBlobs(const std::vector<ALPHAg::TPadSignal> &sigs,
                                                     int ifirst, int ilast);
   //Use static to share these histograms between all instances of Match
   // static TH1D *hsigCoarse;
   // static TH1D *hsig;
   static TH1D* hcognpeaks;
   // static TH2D* hcognpeaksrms;
   // static TH2D* hcognpeakswidth;
   static TH1D* hNblobSigs;
   static TH1D* hcogsigma;
   static TH1D* hcogerr;
   static TH2D* hcogpadssigma;
   static TH2D* hcogpadsamp;
   static TH2D* hcogpadsint;
   static TH2D* hcogpadsampamp;
   static TH1D* hcogpadsdmaxmean;
   static TH1D* hcogpadsdfitpos;
   static TH2D* hcogpadsblob;

   static TH1D* htimecog;
   static TH1D* htimeblobs;
   static TH1D* htimefit;
   static TH1D* hNpadSigs;
   static TGraph* hgNpadSigs;

   ALPHAg::padmap pmap;

public:
   PadMerge(const AnaSettings* ana_settings, bool mt=false);
   PadMerge(const std::string& json, const std::vector<std::string>& args): PadMerge(new AnaSettings(json.c_str(),args))  {}
   ~PadMerge();
   void SetGlobalLockVariable(std::mutex* _manalyzerLock)
   {
      manalzer_global_mtx=_manalyzerLock;
   }
   void Init();
   void Setup(TFile* OutputFile);

   std::vector<std::vector<ALPHAg::TPadSignal>> CombPads(const std::vector<ALPHAg::TPadSignal>& padsignals);
   std::vector<ALPHAg::TPadSignal>  CombinePads(const std::vector<ALPHAg::TPadSignal>& padsignals);
   std::vector<ALPHAg::TPadSignal> CombinePads(
      const std::vector< std::vector<ALPHAg::TPadSignal> > &comb); // this is the used now  -- AC 27-08-2020
   std::vector<ALPHAg::TPadSignal> CombineAPad(
      const std::vector< std::vector<ALPHAg::TPadSignal> > &comb,
      std::vector<ALPHAg::TPadSignal>& CombinedPads,
      const size_t PadNo); //this is the replacement for CombinePads -- Joe 12-10-2020

   void SetDiagnostic(bool d) { diagnostic=d; }

};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
