#ifndef __RECOTRACKFINDER__
#define __RECOTRACKFINDER__

#include <vector>
#include "TracksFinder.hh"
#include "AdaptiveFinder.hh"
#include "IterativeFinder.hh"
#include "ExhaustiveFinder.hh"
#include <iostream>
#include <string>
#include "AnaSettings.hh"
class TRecoTrackFinder
{
   private:
   TracksFinder *pattrec;

   // AdaptiveFinder & IterativeFinder
   double fMaxIncreseAdapt;
   double fLastPointRadCut;
   double fAdaptiveness;
   int fTrackIterations;
   double fTrackLengthCut;

   // ExhaustiveFinder
   double fTouchDistanceXY;
   double fTouchDistanceZ;

   // general TracksFinder parameters, also used by other finders
   // unsigned fNhitsCut; No used...
   int fFinderChoice;
   unsigned fNspacepointsCut;
   double fPointMaxRad;
   double fPointsDistCut;
   double fSeedRadCut;
   // Reasons for pattrec to fail:
   int track_not_advancing;
   int points_cut;
   int rad_cut;
   int length_cut;

   bool fTrace;
   public:
   
      //TRecoTrackFinder(std::string json, bool trace = false);
      TRecoTrackFinder(AnaSettings* ana_set,  bool trace = false);
      int FindTracks( std::vector<TSpacePoint>& PointsArray , std::vector<track_t>& TrackVector);
      int FindTrivialTrack(const std::vector<TSpacePoint> PointsArray , std::vector<track_t>& TrackVector);
      int FindUsedTrivialTrack(const std::vector<track_t>& TrackVector , std::vector<track_t>& myTrackVector); 
      void PrintPattRec();


};





#endif
