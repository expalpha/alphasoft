#ifndef __TTRACK_BUILDER__
#define __TTRACK_BUILDER__

#include <vector>
#include "SignalsType.hh"
#include "TSpacePoint.hh"
#include "TracksFinder.hh"
#include "TTrack.hh"
#include <assert.h>

//Class to convert std::vector<track_t> into std::vector<TTrack>
class TTrackBuilder
{
   private:
      double fMagneticField;
      std::string fLocation;
      const bool fTrace;
   public:
      TTrackBuilder(double B, std::string location, bool trace);
      void BuildTracks( const std::vector<track_t> track_vector, const std::vector<TSpacePoint> PointsArray, std::vector<TTrack>& TTrackArray );
     ~TTrackBuilder() {}
};

#endif
