#ifndef __RECOVERTEXFITTER__
#define __RECOVERTEXFITTER__

#include "TFitHelix.hh"

#include "TFitVertex.hh"
#include <iostream>

class TRecoVertexFitter
{
   private:
      const double fVtxChi2Cut;
      const int fVtxFitIterations;
      const bool fRequireBV;
      const bool fExcludePileupTracks;
      const bool fTrace;

   public:
      TRecoVertexFitter(double chi2cut, int fit_iterations, bool requireBV, bool excludePileupTracks, bool trace): fVtxChi2Cut(chi2cut), fVtxFitIterations(fit_iterations), fRequireBV(requireBV), fExcludePileupTracks(excludePileupTracks), fTrace(trace)
      {
      }
      vertexstatus_t RecVertex(
         std::vector<TFitHelix>& HelixArray, 
         TFitVertex* Vertex, 
         const int thread_no = 1, 
         const int thread_count = 1);
};

#endif