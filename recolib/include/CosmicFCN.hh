// Minimization function for TCosmic in M2. Class derived from FCNBase. Operator() calculates the
// value to be minimized.
// for ALPHA-g MVA analysis
// Author: L GOLINO
// Date: May 2023

#ifndef LINEFCN_HH
#define LINEFCN_HH

#include"Minuit2/FCNBase.h"
#include"TCosmic.hh"
#include"TSpacePoint.hh"
#include<vector>

class CosmicFCN : public ROOT::Minuit2::FCNBase {
	private:
		TCosmic* track;
		const std::vector<TSpacePoint>* points; 
		double error_def;
	public:
		CosmicFCN(TCosmic* a_track);

		double Up() const;
		double operator()(const std::vector<double>& params) const;
};

#endif
