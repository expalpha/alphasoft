// TPC Electron Drift
// uses tabulated data from Garfield calculation 
// to determine for each primary ionization the 
// drift time and the Lorentz angle
//------------------------------------------------
// Author: A.Capra   Nov 2014
//------------------------------------------------

#ifndef __ELECTRONDRIFT__
#define __ELECTRONDRIFT__ 1

#include "Math/Interpolator.h"
#include<map>

// A half-open range bounded inclusively below and exclusively above.
struct Range {
  double start;
  double end;
  
  Range(double left, double right) : start(left), end(right) {}
  bool contains(double x) const {
    return start <= x && x < end;
  }
  bool operator<(const Range& other) const {
    return start < other.start && end < other.end;
  }
};

class ElectronDrift
{
private:
  std::map<Range, ROOT::Math::Interpolator> finterpol_rtime;
  std::map<Range, ROOT::Math::Interpolator> finterpol_rphi;

  double* fAnodeSignal;
  double* fPadSignal;

  static const int fTimeBins = 10000;

  double* fInductionPads;
  double* fInductionAnodes;

  static ElectronDrift* fElectronDrift;

public:
  ElectronDrift();
  ~ElectronDrift();

  double GetTime(double r, double z);
  double GetAzimuth(double r, double z);

  double GetAnodeSignal(int t) const;
  double GetPadSignal(int t) const;
  inline double* GetAnodeSignal() const {return fAnodeSignal;}
  inline double* GetPadSignal() const {return fPadSignal;}

  inline double GetAnodeInduction(int i) const 
  { if(i<3) return fInductionAnodes[i]; 
    else return 0.;
  }
  inline double GetPadInduction(int i) const 
  { if(i<6) return fInductionPads[i];
    else return 0.;
  }

  static ElectronDrift* ElectronDriftInstance();

};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
