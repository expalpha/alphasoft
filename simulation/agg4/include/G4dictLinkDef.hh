#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class  TDigi+;
#pragma link C++ class  TScintDigi+;
#pragma link C++ class  TBSCHit+;
#pragma link C++ class  TTPCelement+;
#pragma link C++ class  TAnode+;
#pragma link C++ class  TPads+;

#endif
