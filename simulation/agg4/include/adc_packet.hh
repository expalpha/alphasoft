#ifndef ADC_PACKET_HH
#define ADC_PACKET_HH

#include<array>
#include<vector>
#include <string>
#include <cstdint>

struct Alpha16Board {
	int name;
	std::array<uint8_t, 6> mac;
	uint8_t module_id;
};

// Map of all known boards
// Index represents the preamp: T0, T1, ..., T15.
constexpr std::array<Alpha16Board, 16> alpha16_boards = {{
	{9, {216, 128, 57, 104, 55, 76}, 0},
	{9, {216, 128, 57, 104, 55, 76}, 0},
    {10, {216, 128, 57, 104, 170, 37}, 1},
    {10, {216, 128, 57, 104, 170, 37}, 1},
    {11, {216, 128, 57, 104, 172, 127}, 2},
    {11, {216, 128, 57, 104, 172, 127}, 2},
    {12, {216, 128, 57, 104, 79, 167}, 3},
    {12, {216, 128, 57, 104, 79, 167}, 3},
    {13, {216, 128, 57, 104, 202, 166}, 4},
    {13, {216, 128, 57, 104, 202, 166}, 4},
    {14, {216, 128, 57, 104, 142, 130}, 5},
    {14, {216, 128, 57, 104, 142, 130}, 5},
    {18, {216, 128, 57, 104, 142, 82}, 6},
    {18, {216, 128, 57, 104, 142, 82}, 6},
    {16, {216, 128, 57, 104, 111, 162}, 7},
    {16, {216, 128, 57, 104, 111, 162}, 7},
}};

// Map of channel_id
// Index represents the wire number within a preamp
// This is valid only for revision 1.1
constexpr std::array<uint8_t, 32> adc_channels = {
   2, 8, 1, 9,
   0, 10, 3, 11,
   4, 12, 5, 13,
   6, 14, 7, 15,
   16, 24, 17, 25,
   18, 26, 19, 27,
   20, 28, 21, 29,
   22, 30, 23, 31
};

// This is the data in the MIDAS bank.
// It contains a single waveform from a single ADC channel
class AdcPacket {
	private:
		static constexpr uint8_t PACKET_TYPE = 0x01;
		static constexpr uint8_t PACKET_VERSION = 0x03;
		uint16_t accepted_trigger;
		Alpha16Board alpha16_board;
		uint8_t channel_id;
		static constexpr uint16_t REQUESTED_SAMPLES = 0x0200;
		static constexpr uint64_t EVENT_TIMESTAMP = 0x0000000000000000;
		static constexpr uint32_t TRIGGER_OFFSET = 0x00000000;
		static constexpr uint32_t BUILD_TIMESTAMP = 0x5FBADC61;
		std::vector<int16_t> waveform;
		uint16_t data_suppression_footer;

		uint16_t suppression_baseline;
	public:
		AdcPacket(Alpha16Board board, uint8_t channel, std::vector<int> data, uint16_t accepted_trig);
		std::vector<uint8_t> to_bytes() const;
		std::string bank_name() const;
};

#endif
