#ifndef TRG_PACKET_HH
#define TRG_PACKET_HH

#include <vector>
#include<cstdint>

// Data in the MIDAS bank
class TrgPacket {
	private:
		uint32_t udp_counter;
		static constexpr uint32_t TIMESTAMP = 0x00000000;
		uint32_t output_counter;
		uint32_t input_counter;
		static constexpr uint32_t PULSER_COUNTER = 0x00000000;
		static constexpr uint32_t TRIGGER_BITMAP = 0x00000000;
		static constexpr uint32_t NIM_BITMAP = 0x00000000;
		static constexpr uint32_t ESATA_BITMAP = 0x00000000;
		static constexpr bool SATISFIED_MLU = true;
		static constexpr uint16_t AW_PROMPT = 0x0000;
		uint32_t drift_veto_counter;
		uint32_t scaledown_counter;
		static constexpr uint8_t AW_MULTIPLICITY = 0x00;
		static constexpr uint16_t AW_BUS = 0x0000;
		static constexpr uint64_t BSC64_BUS = 0x0000000000000000;
		static constexpr uint8_t BSC64_MULTIPLICITY = 0x00;
		static constexpr uint8_t COINCIDENCE_LATCH = 0x00;
		static constexpr uint32_t FIRMWARE_REVISION = 0x00000000;
	public:
		// Currently I am only writing a simplified TRG packet. Most of the
		// fields are just set to zero, hence the minimal constructor possible
		// to have a valid packet.
		TrgPacket(uint32_t output_counter);
		std::vector<uint8_t> to_bytes() const;
};

#endif
