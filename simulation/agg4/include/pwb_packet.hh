#ifndef PWB_PACKET_HH
#define PWB_PACKET_HH

#include<iostream>
#include<array>
#include<vector>
#include<utility>
#include<algorithm>
#include <cstdint>

// A Chunk in the Message Chunk Protocol.
// This is what actually goes into a MIDAS data bank. Multiple chunks form a
// single PWB packet.
class Chunk {
	private:
		uint32_t device_id;
		static constexpr uint32_t PACKET_SEQUENCE = 0;
		static constexpr uint16_t CHANNEL_SEQUENCE = 0;
		uint8_t channel_id;
		uint8_t flags;
		uint16_t chunk_id;
		std::vector<uint8_t> payload;
	
	public:
		Chunk(uint32_t device_id, uint8_t channel_id, bool last_chunk, uint16_t chunk_id, std::vector<uint8_t> payload);
		// Binary format of the chunk.
		std::vector<uint8_t> to_bytes() const;
};

struct PadwingBoard {
	int name;
	std::array<uint8_t, 6> mac;

	std::string midas_bank_name() const;
};

// This is the packet that is sent from an AFTER chip in a padwing board.
// It is NOT the MIDAS data banks directly (needs to be split up into chunks)
class PwbPacket {
	private:
		static constexpr uint8_t PACKET_VERSION = 0x02;
		uint8_t after_id;
		static constexpr uint8_t COMPRESSION_TYPE = 0x00;
		static constexpr uint8_t TRIGGER_SOURCE = 0x00;
		std::array<uint8_t, 6> mac;
		static constexpr uint16_t TRIGGER_DELAY = 0x0000;
		static constexpr uint64_t TRIGGER_TIMESTAMP = 0x000000000000;
		static constexpr uint16_t SCA_LAST_CELL = 0x01ff;
		static constexpr uint16_t REQUESTED_SAMPLES = 0x01fe;
		// There are 79 channels.
		// Byte 0: [7..0] = channels 8..1
		// Byte 1: [15..8] = channels 16..9
		// ...
		// Byte 8: [71..64] = channels 72..65
		// Byte 9: [78..72] = channels 79..73
		std::array<uint8_t, 10> sent_channels_mask;
		uint32_t event_counter;
		static constexpr uint16_t FIFO_MAX_DEPTH = 0x0032;
		static constexpr uint8_t EVENT_DESCRIPTOR_WRITE_DEPTH = 0x00;
		static constexpr uint8_t EVENT_DESCRIPTOR_READ_DEPTH = 0x00;
		/// Each waveform sample is a 2 bytes signed integer
		/// The `waveform_data` vector contains the samples for all channels
		/// that are in the `sent_channel_mask` in order.
		/// All waveforms are of the same length (`requested_samples`).
		std::vector<int16_t> waveform_data;

		// Internally keep track of the channels sent just because working with
		// the mask is more annoying.
		std::vector<int> sent_channels;
	public:
		PwbPacket(uint8_t after, std::array<uint8_t, 6> mac_address,
				// First element of each pair is the channel number
				// Second element is the waveform data for that channel
				std::vector<std::pair<int, std::vector<int>>> waveforms,
				uint32_t e_count);
		// Binary format of the packet
		std::vector<uint8_t> to_bytes() const;
		// Chunks i.e. data in MIDAS banks
		std::vector<Chunk> to_chunks() const;
};

// Map of all known boards.
// First index is column, second index is row.
constexpr std::array<std::array<PadwingBoard, 8>, 8> pwb_boards = {{
	{{
	{12, {236, 41, 34, 206, 84, 2}},
	{13, {236, 40, 159, 252, 84, 2}},
	{14, {236, 41, 44, 52, 84, 2}},
	{2, {236, 40, 136, 108, 84, 2}},
	{11, {236, 40, 197, 187, 84, 2}},
	{17, {236, 40, 153, 39, 84, 2}},
	{18, {236, 40, 228, 87, 84, 2}},
	{19, {236, 40, 116, 173, 84, 2}}
	}}, {{
	{20, {236, 40, 219, 80, 84, 2}},
	{21, {236, 40, 221, 26, 84, 2}},
	{22, {236, 40, 113, 70, 84, 2}},
	{23, {236, 41, 39, 253, 84, 2}},
	{24, {236, 40, 226, 191, 84, 2}},
	{25, {236, 40, 212, 176, 84, 2}},
	{26, {236, 40, 188, 31, 84, 2}},
	{27, {236, 40, 252, 239, 84, 2}}
	}}, {{
	{46, {236, 40, 160, 64, 84, 2}},
	{29, {236, 40, 108, 189, 84, 2}},
	{8, {236, 40, 253, 139, 84, 2}},
	{77, {236, 41, 17, 29, 84, 2}},
	{10, {236, 40, 248, 75, 84, 2}},
	{33, {236, 40, 255, 150, 84, 2}},
	{34, {236, 40, 226, 52, 84, 2}},
	{35, {236, 40, 137, 30, 84, 2}}
	}}, {{
	{36, {236, 40, 165, 153, 84, 2}},
	{37, {236, 41, 43, 61, 84, 2}},
	{1, {236, 40, 250, 162, 84, 2}},
	{39, {236, 41, 43, 253, 84, 2}},
	{76, {236, 40, 244, 136, 84, 2}},
	{41, {236, 40, 187, 198, 84, 2}},
	{42, {236, 41, 41, 188, 84, 2}},
	{40, {236, 40, 198, 81, 84, 2}}
	}}, {{
	{44, {236, 40, 218, 198, 84, 2}},
	{49, {236, 40, 156, 87, 84, 2}},
	{7, {236, 40, 116, 164, 84, 2}},
	{78, {236, 41, 37, 14, 84, 2}},
	{3, {236, 40, 226, 49, 84, 2}},
	{4, {236, 41, 12, 121, 84, 2}},
	{45, {236, 41, 24, 143, 84, 2}},
	{15, {236, 40, 219, 60, 84, 2}}
	}}, {{
	{52, {236, 41, 24, 28, 84, 2}},
	{53, {236, 40, 183, 208, 84, 2}},
	{54, {236, 40, 113, 62, 84, 2}},
	{55, {236, 40, 255, 172, 84, 2}},
	{56, {236, 40, 135, 152, 84, 2}},
	{57, {236, 40, 128, 45, 84, 2}},
	{58, {236, 41, 42, 70, 84, 2}},
	{5, {236, 40, 211, 69, 84, 2}}
	}}, {{
	{60, {236, 40, 243, 36, 84, 2}},
	{0, {236, 40, 255, 135, 84, 2}},
	{6, {236, 40, 218, 6, 84, 2}},
	{63, {236, 40, 108, 234, 84, 2}},
	{64, {236, 40, 110, 20, 84, 2}},
	{65, {236, 40, 215, 15, 84, 2}},
	{66, {236, 40, 197, 199, 84, 2}},
	{67, {236, 40, 183, 38, 84, 2}}
	}}, {{
	{68, {236, 40, 211, 91, 84, 2}},
	{69, {236, 40, 224, 249, 84, 2}},
	{70, {236, 40, 248, 99, 84, 2}},
	{71, {236, 40, 129, 16, 84, 2}},
	{72, {236, 40, 241, 249, 84, 2}},
	{73, {236, 40, 113, 64, 84, 2}},
	{74, {236, 40, 252, 14, 84, 2}},
	{75, {236, 41, 39, 26, 84, 2}}
	}}
}};

#endif
