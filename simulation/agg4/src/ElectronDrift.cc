// TPC Electron Drift
// uses tabulated data from Garfield calculation 
// to determine for each primary ionization the 
// drift time and the Lorentz angle
//------------------------------------------------
// Author: A.Capra   Nov 2014
//------------------------------------------------

#include "ElectronDrift.hh"
#include <TMath.h>
#include <TString.h>
#include <vector>
#include <fstream>
#include <iostream>

extern double gMagneticField;
extern double gQuencherFraction;

ElectronDrift* ElectronDrift::fElectronDrift=0;

ElectronDrift::ElectronDrift()
{
  double start = 0.0;
  double end = 700.0;
  while(start <= 1155.0) {
    TString electrondrift_name = gMagneticField == 0.0 ?
      TString::Format("%s/simulation/common/driftTables/QF%2.0f/ElectronDrift_B0.00T.dat",
	    getenv("AGRELEASE"),
		gQuencherFraction*1.e2) :
	  TString::Format("%s/simulation/common/driftTables/QF%2.0f/inv_STR_B%1.2fT_z%1.0fmm.dat",
        getenv("AGRELEASE"),
		gQuencherFraction*1.e2,
		gMagneticField,
		start);
	
	  std::ifstream electrondrift(electrondrift_name.Data());
	  double r,t,w;
	  std::vector<double> radpos; // cm
	  std::vector<double> drift; // ns
	  std::vector<double> wire; // rad
	  while(1) {
	    electrondrift>>r>>t>>w;
		if( !electrondrift.good() ) break;
		radpos.push_back(r);
		drift.push_back(t);
		wire.push_back(w);
	  }
	  electrondrift.close();

	  // Copy assignment of ROOT's interpolator is private.
	  finterpol_rtime.emplace(std::piecewise_construct,
			  std::forward_as_tuple(start, end),
			  std::forward_as_tuple(radpos, drift));
	  finterpol_rphi.emplace(std::piecewise_construct,
			  std::forward_as_tuple(start, end),
			  std::forward_as_tuple(radpos, wire));

	  start = end;
	  end += 5.0;
  }

  fAnodeSignal = new double[fTimeBins];
  TString awresponse_name = TString::Format("%s/simulation/common/response/anodeResponse.dat",
					    getenv("AGRELEASE"));
  std::ifstream awresponse(awresponse_name.Data());
  int bin; double V;
  while(1)
    {
      awresponse>>bin>>V;
      if( !awresponse.good() ) break;
      fAnodeSignal[bin] = V;
    }
  awresponse.close();

  fPadSignal = new double[fTimeBins];
  TString padresponse_name = TString::Format("%s/simulation/common/response/padResponse_pRO.dat",
					     getenv("AGRELEASE"));
  std::ifstream padresponse(padresponse_name.Data());
  while(1)
    {
      padresponse>>bin>>V;
      if( !padresponse.good() ) break;
      fPadSignal[bin] = V;
    }
  padresponse.close();

  fInductionPads = new double[6];
  fInductionPads[0] = 0.89; fInductionPads[1] = 0.63; 
  fInductionPads[2] = 0.36; fInductionPads[3] = 0.16; 
  fInductionPads[4] = 0.06; fInductionPads[5] = 0.02;

  fInductionAnodes = new double[3];
  fInductionAnodes[0] = -0.13; 
  fInductionAnodes[1] = -0.04;
  fInductionAnodes[2] = -0.01;
}

ElectronDrift::~ElectronDrift()
{
  delete[] fAnodeSignal;
  delete[] fPadSignal;
  delete[] fInductionPads;
  delete[] fInductionAnodes;
}

double ElectronDrift::GetTime(double r, double z)
{
  // parameters in mm
  // returns ns 
  for (const auto& entry : finterpol_rtime) {
    if (entry.first.contains(abs(z))) {
      return entry.second.Eval(r * 0.1);
    }
  }
  return finterpol_rtime.rbegin()->second.Eval(r * 0.1);
}

double ElectronDrift::GetAzimuth(double r, double z)
{  
  // parameters in mm
  // returns rad
  for (const auto& entry : finterpol_rphi) {
    if (entry.first.contains(abs(z))) {
      return entry.second.Eval(r * 0.1);
    }
  }
  return finterpol_rphi.rbegin()->second.Eval(r * 0.1);
}

ElectronDrift* ElectronDrift::ElectronDriftInstance()
{
  if(!fElectronDrift) fElectronDrift=new ElectronDrift();
  return fElectronDrift;
}

double ElectronDrift::GetAnodeSignal(int t) const
{
  if(t<fTimeBins) 
    return fAnodeSignal[t];
  else
    return 0.;
}

double ElectronDrift::GetPadSignal(int t) const
{
  if(t<fTimeBins) 
    return fPadSignal[t];
  else
    return 0.;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
