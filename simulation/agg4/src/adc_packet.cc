#include "adc_packet.hh"
#include <cstdint>
#include <string>
// Number of waveform samples that survive data suppression for a given waveform
int num_samples(const std::vector<int>& data, int baseline) {
	unsigned last_index = 0;
	for(size_t i = 0; i < data.size(); ++i) {
		if(abs(data[i] - baseline) >= 1500) {
			last_index = i;
		}
	}

	if(last_index > 63) {
		// keep_more fixed
		last_index += 23;
		if(last_index > data.size() - 1) {
			last_index = data.size() - 1;
		}
	} else {
		last_index = -1;
	}

	return last_index + 1;
}

AdcPacket::AdcPacket(Alpha16Board board, uint8_t channel, std::vector<int> data, uint16_t accepted_trig) :
  accepted_trigger(accepted_trig), alpha16_board(board), channel_id(channel), waveform() {
		/// Set max/min overflow values
		for (size_t i = 0; i < data.size(); ++i) {
			if (data[i] > 32764) {
				data[i] = 32764;
			} else if (data[i] < -32768) {
				data[i] = -32768;
			}
		}
		// The baseline is the average of the first 64 samples, with the
		// appropriate rounding.
		int32_t num = 0;
		if (data.size() >= 64) {
			for (int i = 0; i < 64; i++) {
				num += data[i];
			}
		}
		int16_t d = num / 64;
		if (num % 64 < 0) {
			suppression_baseline = d - 1;
		} else {
			suppression_baseline = d;
		}

		int samples = num_samples(data, suppression_baseline);
		if(samples > REQUESTED_SAMPLES - 2) {
			samples = REQUESTED_SAMPLES - 2;
		}

		for (int i = 0; i < samples; i++) {
			waveform.push_back(data[i]);
		}

		if(waveform.size() > 0) {
			data_suppression_footer = (samples + 1)/2 + 1;
			// keep_bit
			data_suppression_footer |= 1 << 12;
		} else {
			data_suppression_footer = 0;
		}
		// suppression_enabled
		data_suppression_footer |= 1 << 13;
	}

// The binary format is (all multi-byte fields are big-endian):
// 0x00: 1 byte. Packet type, fixed to 0x01.
// 0x01: 1 byte. Packet version, fixed to 0x03.
// 0x02: 2 bytes. Accepted trigger, fixed to 0x0000.
// 0x04: 1 byte. Module ID, obtained from Alpha16Board::module_id.
// 0x05: 1 byte. Channel ID.
// 0x06: 2 bytes. Requested samples, fixed to 0x0200.
// 0x08: 4 bytes. Event timestamp (LSW), fixed to 0x00000000.
// 0x0C: 2 bytes. Fixed to 0x0000.
// 0x0E: 6 bytes. MAC address, obtained from Alpha16Board::mac.
// 0x14: 4 bytes. Event timestamp (MSW), fixed to 0x00000000.
// 0x18: 4 bytes. Trigger offset, fixed to 0x00000000.
// 0x1C: 4 bytes. Build timestamp, fixed to 0x5FBADC61.
// 0x20: n bytes. Waveform data.
//      With data suppression disabled, fixed to 510 samples of 16-bit data.
//      Truncate, or pad with `suppression_baseline` if less than 510 samples.
// 0x20 + n: 2 bytes. Data suppression footer, fixed to 0x1100.
//      Suppression disabled
//      Keep bit set to 1
//      Keep last fixed to 256
// 0x22 + n: 2 bytes. Suppression baseline.
std::vector<uint8_t> AdcPacket::to_bytes() const {
	std::vector<uint8_t> bytes;
	bytes.push_back(PACKET_TYPE);
	bytes.push_back(PACKET_VERSION);
	bytes.push_back((accepted_trigger >> 8) & 0xFF);
	bytes.push_back(accepted_trigger & 0xFF);
	bytes.push_back(alpha16_board.module_id);
	// The 128 sets the MSB (indicating TPC aw channel)
	bytes.push_back(channel_id + 128);
	bytes.push_back((REQUESTED_SAMPLES >> 8) & 0xFF);
	bytes.push_back(REQUESTED_SAMPLES & 0xFF);
	bytes.push_back((EVENT_TIMESTAMP >> 24) & 0xFF);
	bytes.push_back((EVENT_TIMESTAMP >> 16) & 0xFF);
	bytes.push_back((EVENT_TIMESTAMP >> 8) & 0xFF);
	bytes.push_back(EVENT_TIMESTAMP & 0xFF);
	if(waveform.size() > 0) {
		bytes.push_back(0x00);
		bytes.push_back(0x00);
		for (int i = 0; i < 6; i++) {
			bytes.push_back(alpha16_board.mac[i]);
		}
		bytes.push_back((EVENT_TIMESTAMP >> 56) & 0xFF);
		bytes.push_back((EVENT_TIMESTAMP >> 48) & 0xFF);
		bytes.push_back((EVENT_TIMESTAMP >> 40) & 0xFF);
		bytes.push_back((EVENT_TIMESTAMP >> 32) & 0xFF);
		bytes.push_back((TRIGGER_OFFSET >> 24) & 0xFF);
		bytes.push_back((TRIGGER_OFFSET >> 16) & 0xFF);
		bytes.push_back((TRIGGER_OFFSET >> 8) & 0xFF);
		bytes.push_back(TRIGGER_OFFSET & 0xFF);
		bytes.push_back((BUILD_TIMESTAMP >> 24) & 0xFF);
		bytes.push_back((BUILD_TIMESTAMP >> 16) & 0xFF);
		bytes.push_back((BUILD_TIMESTAMP >> 8) & 0xFF);
		bytes.push_back(BUILD_TIMESTAMP & 0xFF);
		for (size_t i = 0; i < waveform.size(); i++) {
			bytes.push_back((waveform[i] >> 8) & 0xFF);
			bytes.push_back(waveform[i] & 0xFF);
		}
	}
	bytes.push_back((data_suppression_footer >> 8) & 0xFF);
	bytes.push_back(data_suppression_footer & 0xFF);
	bytes.push_back((suppression_baseline >> 8) & 0xFF);
	bytes.push_back(suppression_baseline & 0xFF);
	return bytes;
}

// The format of the bank name for TPC anode wire data is:
// CXXY
// Where:
// XX is the board_name, e.g. 09, 10, etc.
// Y is channel_id with radix 32, i.e. 0, 1, ..., 9, A, ..., V.
std::string AdcPacket::bank_name() const {
	std::string name = "C";
	// Board name with leading zeros if necessary.
	if (alpha16_board.name < 10) {
		name += "0";
	}
	name += std::to_string(alpha16_board.name);
	name += "0123456789ABCDEFGHIJKLMNOPQRSTUV"[channel_id];

	return name;
}
