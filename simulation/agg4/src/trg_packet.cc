#include "trg_packet.hh"

TrgPacket::TrgPacket(uint32_t counter) : udp_counter(counter), output_counter(counter), input_counter(counter), drift_veto_counter(counter), scaledown_counter(counter) {
}

// Function to push back a field into a vector as little endian
void push_back_u32(std::vector<uint8_t>& vec, uint32_t value) {
  vec.push_back(value & 0xFF);
  vec.push_back((value >> 8) & 0xFF);
  vec.push_back((value >> 16) & 0xFF);
  vec.push_back((value >> 24) & 0xFF);
}

// All multi-byte fields are little-endian
std::vector<uint8_t> TrgPacket::to_bytes() const {
	std::vector<uint8_t> bytes;
	// I am only supporting a specific TRG packet version that is 80 bytes long.
	bytes.reserve(80);
	push_back_u32(bytes, udp_counter);
	push_back_u32(bytes, 0x80000000 | output_counter);
	push_back_u32(bytes, TIMESTAMP);
	push_back_u32(bytes, output_counter);
	push_back_u32(bytes, input_counter);
	push_back_u32(bytes, PULSER_COUNTER);
	push_back_u32(bytes, TRIGGER_BITMAP);
	push_back_u32(bytes, NIM_BITMAP);
	push_back_u32(bytes, ESATA_BITMAP);

	uint32_t aw_prompt = AW_PROMPT;
	if (SATISFIED_MLU) {
		aw_prompt |= 0x80000000;
	}
	push_back_u32(bytes, aw_prompt);

	push_back_u32(bytes, drift_veto_counter);
	push_back_u32(bytes, scaledown_counter);
	push_back_u32(bytes, 0x00000000);

	uint32_t aw_mult = AW_MULTIPLICITY;
	uint32_t aw_bus = AW_BUS;
	push_back_u32(bytes, (aw_mult << 16) | aw_bus);

	push_back_u32(bytes, BSC64_BUS & 0xFFFFFFFF);
	push_back_u32(bytes, (BSC64_BUS >> 32) & 0xFFFFFFFF);
	push_back_u32(bytes, BSC64_MULTIPLICITY);
	push_back_u32(bytes, COINCIDENCE_LATCH);
	push_back_u32(bytes, FIRMWARE_REVISION);
	push_back_u32(bytes, 0xE0000000 | output_counter);

	return bytes;
}
