//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// BabcockField implementation
//
// Created: J.Apostolakis, 12.11.1998
// Adapter for ALPHA-g: A. Capra 17.02.2023
// --------------------------------------------------------------------

#include "BabcockField.hh"
#include "G4SystemOfUnits.hh"

BabcockField::BabcockField(): G4MagneticField()
{
  int l = ReadMap();
  G4cout<<"BabcockField() read "<<l<<" lines"<<G4endl;
  if( l<0 ) G4cerr<<" EROOR "<<G4endl;
  else if( l==0 ) G4cerr<<" NO MAP FILE"<<G4endl;
}

BabcockField::~BabcockField()
{
}

BabcockField::BabcockField(const BabcockField& r): G4MagneticField(r)
// To allow extension to joint EM & g field
{
  fBfieldMap = r.fBfieldMap;
}

BabcockField& BabcockField::operator = (const BabcockField& p)
{
  if (&p == this) return *this;
  G4Field::operator=(p);
  return *this;
}

int BabcockField::ReadMap()
{
  std::string fname(getenv("AGRELEASE"));
  fname += "/simulation/common/fieldmaps/Babcock_Field_Map.csv";
  std::ifstream fileMap(fname.c_str());
  if( fileMap.is_open() ) G4cout<<"Reading map file: "<<fname<<G4endl;
  else { G4cerr<<"Cannot find file: "<<fname<<G4endl; return -1;}
  std::string dummy;
  std::getline(fileMap,dummy); // skip header
  //std::cout<<dummy<<std::endl;
  std::getline(fileMap,dummy); // skip header
  //std::cout<<dummy<<std::endl;

  int nlines = 0;
  std::string line;
  int r,z; // mm
  double Br,Bz; // Tesla
  while( std::getline(fileMap, line) ) // read file line-by-line
    {
      std::istringstream s_line(line);
      std::vector<std::string> fields;
      std::string cell;
      // read csv fields
      while( getline(s_line, cell,',') ) fields.push_back( cell );
      r = std::stoi( fields[0] )*10; // cm -> mm
      z = std::stoi( fields[1] )*10; // cm -> mm
      Br = std::stod( fields[2] ); // Gauss
      Bz = std::stod( fields[3] ); // Gauss
      // create map entry 
      fBfieldMap[r][z] = {Br*1e-4,Bz*1e-4}; // Tesla
      ++nlines;
    }
  fileMap.close();

  // std::vector<double> test = fBfieldMap[110][400];
  // std::cout<<"Test Map: (110,400) -> ("<<test[0]<<","<<test[1]<<")"<<std::endl;
  // std::vector<double> test2 = fBfieldMap[120][400];
  // std::cout<<"Test Map: (120,400) -> ("<<test2[0]<<","<<test2[1]<<")"<<std::endl;
    
  return nlines;
}

// Return as Bfield[0], [1], [2] the magnetic field x, y & z components
void  BabcockField::GetFieldValue( const G4double* Point, G4double *Bfield ) const
{
  // cart -> polar
  G4double r = sqrt(Point[0]*Point[0]+Point[1]*Point[1]);
  G4double phi = atan2(Point[1],Point[0]);
  G4double z = Point[2];

  // if outside the map, assume no field (the map is pretty big)
  if( r > 2000. || z < -1800. || z > 1800. ) // these are mm
    {
      Bfield[0] = Bfield[1] = Bfield[2] = 0.0;
      return;
    }

  // find the 4 points in the map around the current Point
  int rup = ceil(r*0.1)*10;
  int rdw = floor(r*0.1)*10;
  int zup = ceil(fabs(z*0.1))*10;
  int zdw = floor(fabs(z*0.1))*10;

  // interpolation weights
  double rup_w = 10.-fabs(double(rup)-r);
  double rdw_w = 10.-fabs(double(rdw)-r);
  double zup_w = 10.-fabs(double(zup)-z);
  double zdw_w = 10.-fabs(double(zdw)-z);
  double total_weights = rup_w*zup_w+rup_w*zdw_w+rdw_w*zup_w+rdw_w*zdw_w;
 
  // map query
  std::vector<G4double> B1 = fBfieldMap.at(rup).at(zup);
  std::vector<G4double> B2 = fBfieldMap.at(rup).at(zdw);
  std::vector<G4double> B3 = fBfieldMap.at(rdw).at(zup);
  std::vector<G4double> B4 = fBfieldMap.at(rdw).at(zdw);
  
  // atrox interpolation
  // G4double    Br = 0.25*(B1[0]+B2[0]+B3[0]+B4[0]);
  // G4double    Bz = 0.25*(B1[1]+B2[1]+B3[1]+B4[1]);
  G4double Br = (rup_w*zup_w*B1[0]+rup_w*zdw_w*B2[0]+rdw_w*zup_w*B3[0]+rdw_w*zdw_w*B4[0])/total_weights;
  G4double Bz = (rup_w*zup_w*B1[1]+rup_w*zdw_w*B2[1]+rdw_w*zup_w*B3[1]+rdw_w*zdw_w*B4[1])/total_weights;

  // polar -> cart
  Bfield[0] = Br*cos(phi)*tesla;
  Bfield[1] = Br*sin(phi)*tesla;
  Bfield[2] = Bz*tesla;

  // G4cout<<Point[0]<<","<<Point[1]<<","<<Point[2]<<" --> "
  // 	<<Bfield[0]/tesla<<","<<Bfield[1]/tesla<<","<<Bfield[2]/tesla<<G4endl;
}

