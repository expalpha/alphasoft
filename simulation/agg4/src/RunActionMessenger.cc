//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id$
//
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "RunActionMessenger.hh"
#include "RunAction.hh"

#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithAString.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunActionMessenger::RunActionMessenger(RunAction* Run):fRunAction(Run)
{ 
  RunCmd = new G4UIcmdWithAnInteger("/run/setRunNumber",this);
  RunCmd->SetGuidance("Set Run Number");
  RunCmd->SetParameterName("Run",false);
  RunCmd->SetRange("Run Number>=0.");
  RunCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  TagCmd = new G4UIcmdWithAString("/AGTPC/setRunName",this);
  TagCmd->SetGuidance("Add a tag to the rootfile name");
  TagCmd->SetParameterName("Tag",true);
  //  TagCmd->SetRange("Run Number>=0.");
  TagCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  TrigCmd = new G4UIcmdWithAString("/AGTPC/trigger",this);
  TrigCmd->SetGuidance("Select detector trigger");
  TrigCmd->SetParameterName("Trigger",true);
  TrigCmd->SetCandidates("all_events mlu2");
  TrigCmd->SetDefaultValue("all_events");
  TrigCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  Adc32ThresCmd = new G4UIcmdWithAnInteger("/AGTPC/adc32_threshold",this);
  Adc32ThresCmd->SetGuidance("Set ADC32 threshold");
  Adc32ThresCmd->SetParameterName("ADC32Threshold",true);
  Adc32ThresCmd->SetRange("ADC32Threshold<0");
  Adc32ThresCmd->SetDefaultValue(-8192);
  Adc32ThresCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

RunActionMessenger::~RunActionMessenger()
{
  delete RunCmd;
  delete TagCmd;
  delete TrigCmd;
  delete Adc32ThresCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunActionMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{ 
  if( command == RunCmd ) {}
    //    fRunAction->SetRunNumber( RunCmd->GetNewIntValue(newValue) );

  if( command == TagCmd ) {
    fRunAction->SetRunName( newValue );
  }

  if( command == TrigCmd ) {
	  fRunAction->SetTrigger( newValue );
  }
  
  if( command == Adc32ThresCmd ) {
	  fRunAction->SetAdc32Threshold( Adc32ThresCmd->GetNewIntValue(newValue) );
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
