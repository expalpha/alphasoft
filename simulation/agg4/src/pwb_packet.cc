#include "pwb_packet.hh"

uint32_t crc32c(const uint8_t *data, size_t length) {
	uint32_t crc = 0xffffffff;
	for (size_t i = 0; i < length; i++) {
		crc ^= data[i];
		for (int j = 0; j < 8; j++) {
			crc = (crc >> 1) ^ (0x82f63b78 & (-(crc & 1)));
		}
	}
	return ~crc;
}

Chunk::Chunk(uint32_t device, uint8_t channel, bool is_last, uint16_t id, std::vector<uint8_t> data) : 
	device_id(device), channel_id(channel), chunk_id(id), payload(data) {
	flags = is_last ? 0x01 : 0x00;
}

// The binary format is (all multi-byte fields are little-endian):
// 0x00: 4 bytes. Device ID (first 4 bytes of mac address as little-endian u32).
// 0x04: 4 bytes. Packet sequence, fixed value 0x00000000.
// 0x08: 2 bytes. Channel sequence, fixed value 0x0000.
// 0x0a: 1 byte.  Channel ID. 0 = A, 1 = B, 2 = C, 3 = D.
// 0x0b: 1 byte. Flags. 0x01 = last chunk in packet.
// 0x0c: 2 bytes. Chunk ID. 0 = first chunk, 1 = second chunk, etc.
// 0x0e: 2 bytes. Chunk length. Number of bytes of the unpadded payload.
// 0x10: 4 bytes. Header CRC32-C.
// 0x14: n bytes. Payload 32-bit aligned.
// 0x14+n: 4 bytes. Payload CRC32-C (including padding).
std::vector<uint8_t> Chunk::to_bytes() const {
	std::vector<uint8_t> bytes;
	bytes.reserve(20 + payload.size() + 4);
	bytes.push_back(device_id & 0xff);
	bytes.push_back((device_id >> 8) & 0xff);
	bytes.push_back((device_id >> 16) & 0xff);
	bytes.push_back((device_id >> 24) & 0xff);
	bytes.push_back(PACKET_SEQUENCE & 0xff);
	bytes.push_back((PACKET_SEQUENCE >> 8) & 0xff);
	bytes.push_back((PACKET_SEQUENCE >> 16) & 0xff);
	bytes.push_back((PACKET_SEQUENCE >> 24) & 0xff);
	bytes.push_back(CHANNEL_SEQUENCE & 0xff);
	bytes.push_back((CHANNEL_SEQUENCE >> 8) & 0xff);
	bytes.push_back(channel_id);
	bytes.push_back(flags);
	bytes.push_back(chunk_id & 0xff);
	bytes.push_back((chunk_id >> 8) & 0xff);
	bytes.push_back(payload.size() & 0xff);
	bytes.push_back((payload.size() >> 8) & 0xff);
	
	// Header CRC32-C
	uint32_t header_crc = ~crc32c(bytes.data(), bytes.size());
	bytes.push_back(header_crc & 0xff);
	bytes.push_back((header_crc >> 8) & 0xff);
	bytes.push_back((header_crc >> 16) & 0xff);
	bytes.push_back((header_crc >> 24) & 0xff);
	
	bytes.insert(bytes.end(), payload.begin(), payload.end());
	
	// Pad to 32-bit boundary
	while (bytes.size() % 4 != 0) {
		bytes.push_back(0);
	}
	
	// Payload CRC32-C
	uint32_t payload_crc = ~crc32c(bytes.data() + 0x14, bytes.size() - 0x14);
	bytes.push_back(payload_crc & 0xff);
	bytes.push_back((payload_crc >> 8) & 0xff);
	bytes.push_back((payload_crc >> 16) & 0xff);
	bytes.push_back((payload_crc >> 24) & 0xff);
	
	return bytes;
}

// A PWB bank name is a string of the form "PCXX" where XX is the board name
// with leading zeros if necessary.
std::string PadwingBoard::midas_bank_name() const {
	std::string bank_name = "PC";
	if (name < 10) {
		bank_name += "0";
	}
	bank_name += std::to_string(name);
	return bank_name;
}

PwbPacket::PwbPacket(uint8_t after, std::array<uint8_t, 6> mac_address,
				std::vector<std::pair<int, std::vector<int>>> waveforms,
				uint32_t e_count) : 
	after_id(after), mac(mac_address), sent_channels_mask(), event_counter(e_count), waveform_data(), sent_channels() {
	// Sort the waveforms by channel number
	std::sort(waveforms.begin(), waveforms.end(),
			[](const std::pair<int, std::vector<int>> &a,
				const std::pair<int, std::vector<int>> &b) {
				return a.first < b.first;
			});
	// Set the sent channels mask
	for (auto &w : waveforms) {
		int channel = w.first;
		sent_channels_mask[(channel - 1) / 8] |= 1 << ((channel - 1) % 8);
		sent_channels.push_back(channel);
	}
	// Copy the waveform data
	// If a waveform is shorter than the `REQUESTED_SAMPLES`, pad it with zeros
	// If a waveform is longer than the `REQUESTED_SAMPLES`, truncate it
	for (auto &w : waveforms) {
		auto &data = w.second;
		int n = std::min((int)REQUESTED_SAMPLES, (int)data.size());
		for (int i = 0; i < n; i++) {
			// Pad waveform saturation
			int value = data[i];
			if(value > 2047) {
				value = 2047;
			} else if(value < -2048) {
				value = -2048;
			}
			waveform_data.push_back(value);
		}
		for (int i = n; i < REQUESTED_SAMPLES; i++) {
			waveform_data.push_back(0);
		}
	}
}

// The binary format is (all multi-byte fields are little endian):
// 0x00: 1 byte. Packet version, fixed value 0x02.
// 0x01: 1 byte. AFTER chip ID. 65 = A, 66 = B, 67 = C, 68 = D.
// 0x02: 1 byte. Compression type, fixed value 0x00 = raw.
// 0x03: 1 byte. Trigger source, fixed value 0x00 = external.
// 0x04: 6 bytes. MAC address of the padwing board.
// 0x0a: 2 bytes. Trigger delay, fixed value 0x0000.
// 0x0c: 6 bytes. Trigger timestamp, fixed value 0x000000000000.
// 0x12: 2 bytes. Fixed value 0x0000.
// 0x14: 2 bytes. SCA last cell, fixed value 0x01ff.
// 0x16: 2 bytes. Requested number of samples, fixed value 0x01fe.
// 0x18: 10 bytes. Sent channels mask.
// 0x22: 10 bytes. Channels over threshold mask (same as sent channel mask).
// 0x2c: 4 bytes. Event counter, fixed value 0x00000000.
// 0x30: 2 bytes. FIFO max depth, fixed value 0x0032.
// 0x32: 1 byte. Event descriptor write depth, fixed value 0x00.
// 0x33: 1 byte. Event descriptor read depth, fixed value 0x00.
// 0x34: ...  Waveform data.
std::vector<uint8_t> PwbPacket::to_bytes() const {
	std::vector<uint8_t> bytes;
	// Reserve space for the packet
	// 52 bytes for the header
	// If REQUESTED_SAMPLES is even:
	// (4 + 2 * `REQUESTED_SAMPLES`) * `sent_channels.size()` for the waveform data
	// If REQUESTED_SAMPLES is odd:
	// Extra 2 padding bytes per waveform
	// 4 bytes End of Packet marker
	int channel_size = 4 + 2 * (REQUESTED_SAMPLES + (REQUESTED_SAMPLES % 2));
	bytes.reserve(52 + channel_size * sent_channels.size() + 4);

	bytes.push_back(PACKET_VERSION);
	bytes.push_back(after_id);
	bytes.push_back(COMPRESSION_TYPE);
	bytes.push_back(TRIGGER_SOURCE);
	bytes.insert(bytes.end(), mac.begin(), mac.end());
	bytes.push_back(TRIGGER_DELAY & 0xff);
	bytes.push_back((TRIGGER_DELAY >> 8) & 0xff);
	bytes.push_back(TRIGGER_TIMESTAMP & 0xff);
	bytes.push_back((TRIGGER_TIMESTAMP >> 8) & 0xff);
	bytes.push_back((TRIGGER_TIMESTAMP >> 16) & 0xff);
	bytes.push_back((TRIGGER_TIMESTAMP >> 24) & 0xff);
	bytes.push_back((TRIGGER_TIMESTAMP >> 32) & 0xff);
	bytes.push_back((TRIGGER_TIMESTAMP >> 40) & 0xff);
	bytes.push_back(0x00);
	bytes.push_back(0x00);
	bytes.push_back(SCA_LAST_CELL & 0xff);
	bytes.push_back((SCA_LAST_CELL >> 8) & 0xff);
	bytes.push_back(REQUESTED_SAMPLES & 0xff);
	bytes.push_back((REQUESTED_SAMPLES >> 8) & 0xff);
	bytes.insert(bytes.end(), sent_channels_mask.begin(), sent_channels_mask.end());
	bytes.insert(bytes.end(), sent_channels_mask.begin(), sent_channels_mask.end());
	bytes.push_back(event_counter & 0xff);
	bytes.push_back((event_counter >> 8) & 0xff);
	bytes.push_back((event_counter >> 16) & 0xff);
	bytes.push_back((event_counter >> 24) & 0xff);
	bytes.push_back(FIFO_MAX_DEPTH & 0xff);
	bytes.push_back((FIFO_MAX_DEPTH >> 8) & 0xff);
	bytes.push_back(EVENT_DESCRIPTOR_WRITE_DEPTH);
	bytes.push_back(EVENT_DESCRIPTOR_READ_DEPTH);
	// The format of the waveform data is:
	// 1. The first 2 bytes are the channel number
	// 2. The next 2 bytes are the number of samples (REQUESTED_SAMPLES)
	// 3. The next 2 * REQUESTED_SAMPLES bytes are the waveform data
	// 4. If REQUESTED_SAMPLES is odd, there is a padding sample
	// 5. Repeat 1-4 for each channel
	for (size_t i = 0; i < sent_channels.size(); i++) {
		int channel = sent_channels[i];
		bytes.push_back(channel & 0xff);
		bytes.push_back((channel >> 8) & 0xff);
		bytes.push_back(REQUESTED_SAMPLES & 0xff);
		bytes.push_back((REQUESTED_SAMPLES >> 8) & 0xff);
		for (int j = 0; j < REQUESTED_SAMPLES; j++) {
			int sample = waveform_data[i * REQUESTED_SAMPLES + j];
			bytes.push_back(sample & 0xff);
			bytes.push_back((sample >> 8) & 0xff);
		}
		if (REQUESTED_SAMPLES % 2 == 1) {
			bytes.push_back(0);
			bytes.push_back(0);
		}
	}
	// Finally there is a 4 bytes End Of Data marker (0xCCCCCCCC)
	bytes.push_back(0xcc);
	bytes.push_back(0xcc);
	bytes.push_back(0xcc);
	bytes.push_back(0xcc);
	return bytes;
}

// Maximum Chunk size is actually ~65k bytes, but for some reason the Chunks in
// the MIDAS file are maximum 1448 bytes. Maybe a restriction on the hardware
// side? I don't know; I will just stick to the same stuff I see in the MIDAS
// files.
constexpr unsigned long int CHUNK_SIZE = 1448;

std::vector<Chunk> PwbPacket::to_chunks() const {
	std::vector<Chunk> chunks;

	// The device_id is the first 4 bytes of the MAC address as a little-endian
	// integer
	uint32_t device_id = mac[0] | (mac[1] << 8) | (mac[2] << 16) | (mac[3] << 24);
	// The channel_id is just the AFTER. A -> 0, B -> 1, C -> 2, D -> 3
	uint8_t channel_id = after_id - 'A';

	auto bytes = to_bytes();
	// Split the bytes into chunks of maximum size `CHUNK_SIZE`
	// All chunks except the last one have the same size
	// The last chunk has the remaining bytes
	for (size_t i = 0; i < bytes.size(); i += CHUNK_SIZE) {
		int n = std::min(CHUNK_SIZE, (int)bytes.size() - i);
		std::vector<uint8_t> payload(bytes.begin() + i, bytes.begin() + i + n);
		bool is_last = i + n == bytes.size();
		// The chunk id is the index of the chunk in the packet
		// The first chunk has number 0
		// The last chunk has number `chunks.size()`
		// The chunk number is used to reconstruct the packet from the chunks
		// in the correct order
		chunks.push_back(Chunk(device_id, channel_id, is_last, chunks.size(),payload));
	}
	return chunks;
}
