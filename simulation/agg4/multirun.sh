#!/bin/bash

if [ ! -d "$AGRELEASE/RunLogs" ]; then
    mkdir -p $AGRELEASE/RunLogs
fi

# DEFAULT SETTINGS
# pad time
echo "16" > settings.dat
# anode time
echo "16" >> settings.dat
# magnetic field
echo "1" >> settings.dat
# quencher fraction
echo "0.3" >> settings.dat
# material
echo "1" >> settings.dat
# prototype
echo "0" >> settings.dat

#gedit settings.dat &> /dev/null &

NAME=""

ySEED=28115
RUNNO=1
for SEED in 10081985 18061985 3092016 26092019 28091956 18031956 20051985 23061990 17122013 30112015 12345678 23456789 34567890 45678901 56789012 67890123 78901234 89012345 90123456 10987654 98765432 87654321 76543210 65432109 54321098 43210987 32109876 21098765; do
    echo "/control/verbose 2" > run${RUNNO}_${SEED}.mac
    echo "/AGTPC/setZcenter -0.55255 m" >> run${RUNNO}_${SEED}.mac
    echo "/AGTPC/setRunName multirun${RUNNO}_seed${SEED}${NAME}" >> run${RUNNO}_${SEED}.mac
    echo "/random/setSeeds ${SEED} ${ySEED}" >> run${RUNNO}_${SEED}.mac
    echo "/run/beamOn 3000" >> run${RUNNO}_${SEED}.mac

    echo "@@@ SEED   ${SEED}   ${ySEED}" > $AGRELEASE/RunLogs/multiAGTPCrun${RUNNO}_${SEED}.log
    echo "@@@ Run # ${RUNNO}" >> $AGRELEASE/RunLogs/multiAGTPCrun${RUNNO}_${SEED}.log
    echo `hostname` >> $AGRELEASE/RunLogs/multiAGTPCrun${RUNNO}_${SEED}.log
    echo `pwd` >> $AGRELEASE/RunLogs/multiAGTPCrun${RUNNO}_${SEED}.log
    { time agg4 run${RUNNO}_${SEED}.mac ; } >> $AGRELEASE/RunLogs/multiAGTPCrun${RUNNO}_${SEED}.log 2>&1 &

    gedit $AGRELEASE/RunLogs/multiAGTPCrun${RUNNO}_${SEED}.log &> /dev/null &
    RUNNO=$((RUNNO+1))
done

