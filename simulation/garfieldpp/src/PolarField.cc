#include <iostream>

#include "Garfield/MediumMagboltz.hh"
#include "Garfield/ComponentAnalyticField.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/ViewField.hh"

#include "Helpers.hh"

#include <TApplication.h>
//#include <TFile.h>

using namespace std;
using namespace Garfield;

int main(int argc, char * argv[])
{
  // double CathodeVoltage=-4000.,// V
  //   AnodeVoltage = 3200.,
  //   FieldVoltage = -100.;
  // double Bfield = 1.;//T

  // if( argc == 3 )
  //   {
  //     CathodeVoltage = atof(argv[1]);
  //     AnodeVoltage   = atof(argv[2]);
  //   }
  // else if( argc == 4 )
  //   {
  //     CathodeVoltage = atof(argv[1]);
  //     AnodeVoltage   = atof(argv[2]);
  //     FieldVoltage   = atof(argv[3]);
//    }
  cout<<"============================="<<endl;
  cout<<"\t"<<argv[0]<<endl;
  cout<<"============================="<<endl;

  TApplication app("PolarField", &argc, argv);

  //  TString fname = TString::Format("chamberfield_Cathode%4.0fV_Anode%4.0fV_Field%3.0fV.root",
  //				  CathodeVoltage,AnodeVoltage,FieldVoltage);
  //  TFile* fout = TFile::Open(fname.Data(),"RECREATE");

  // Create the medium
  MediumMagboltz* gas = new MediumMagboltz();
  // Gas file created with other software
  TString gasfile = TString::Format("%s/simulation/common/gas_files/ar_70_co2_30_725Torr_20E200000_4B1.10.gas",
				    getenv("AGRELEASE"));
  if( LoadGas(gas,gasfile.Data()) )
    cout<<gasfile<<endl;
  else
    return -47;

  TString garfdata = TString::Format("%s/Data/IonMobility_Ar+_Ar.txt",getenv("GARFIELD_HOME"));
  gas->LoadIonMobility(garfdata.Data());

  // Define the cell layout.
  constexpr double lZ = 115.2;
  ComponentAnalyticField cmp;
  Sensor sensor;
  sensor.AddComponent(&cmp);

  cmp.SetMedium(gas);
  cmp.SetMagneticField(0., 0., 1.);
  cmp.SetPolarCoordinates();

  // Outer wall.
  constexpr double rRO = 19.;
  cmp.AddPlaneR(rRO, 0, "ro");
  cmp.AddReadout("ro");
  // sensor.AddElectrode(&cmp, "ro");

  // Inner wall.
  constexpr double rCathode = 10.925;
  constexpr double vCathode = -4000.;
  cmp.AddPlaneR(rCathode, vCathode, "c");
  constexpr int nWires = 256;
  // Phi periodicity.
  const double sphi = 360. / double(nWires);

  // Field wires.
  // Radius [cm]
  constexpr double rFW = 17.4;
  // Diameter [cm]
  constexpr double dFW = 152.e-4;
  // Potential [V]
  constexpr double vFW = -110.;
  // Tension [g]
  constexpr double tFW = 120.;

  // Anode wires.
  constexpr double rAW = 18.2;
  constexpr double dAW = 30.e-4;
  constexpr double vAW = 3200.;
  constexpr double tAW = 40.;

  cmp.AddWire(rFW, 0, dFW, vFW, "f", 2 * lZ, tFW);
  cmp.AddWire(rAW, 0.5 * sphi, dAW, vAW, "a", 2 * lZ, tAW, 19.25);
  cmp.AddReadout("a");
  cmp.SetPeriodicityPhi(sphi);
  //cmp.PrintCell();
  sensor.AddElectrode(&cmp,"a");

  cout<<"*** Plot Electric Field Magnitude contours @ wires ***"<<endl;
  // area
  double areaX1 = 10., areaX2 = rRO,
     areaY1 = -1., areaY2 = 8.;
     //    areaZ1 = -1., areaZ2 = 1.;
  // Plot Electric Field Magnitude contours @ wires
  TString c5name = TString::Format("Efield_wires_Cathode%4.0fV_Anode%4.0fV_Field%3.0fV",
				   vCathode,vAW,vFW);
  TCanvas c5(c5name.Data(),c5name.Data(), 1200, 1300);
  ViewField viewEw;
  viewEw.SetSensor(&sensor);
  viewEw.SetElectricFieldRange(0.,1e4);
  viewEw.SetArea(areaX1,areaY1,areaX2,areaY2);
  viewEw.SetCanvas(&c5);
  //viewEw.PlotContour("e");
  viewEw.Plot("emag","colz");

  c5.SaveAs(".png");
  c5.SaveAs(".C");

  app.Run(true);

  return 0;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
