# Applications Index

 - `DriftGas.exe` produce gas file with MagBoltz

 - `ChamberField.exe` display electric field 
	
 - `PlotMedium.exe` plot properties of selected gas

 - `ChamberDrift.exe` calculate STR
 
 - `ElectronAvalanche.exe` model electronics response to avalanche
 
 - `TrackElectronAvalanche.exe` simulate the passage of a charged pion and save the rTPC response
 
 - `PolarDrift.exe` calculate STR using polar coordinates


# Scripts Index

Located in `$AGRELEASE/simulation/garfieldpp/scripts`

 - `str_batch.py` parallel execution of ChamberDrift.exe
 
 - `FitSTR.py` display and save STR for B=0T

 - `FitSTR_B1.py` display and save STR for B=1T
 
 - `FitSTR_map.py` display and save STR for mapped B
 
 - `runTrack.sh` sample
 
 - `str_batch_polar.py` parallel execution of PolarDrift.exe
