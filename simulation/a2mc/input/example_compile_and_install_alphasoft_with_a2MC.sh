#!/bin/bash

####################################################################################
# ALPHASOFT CMAKE CONFIGURATION AND INSTALLATION SCRIPT FOR A2MC
####################################################################################

#____________
# DEFINITIONS

PACKAGES=/home/user/local 
VGMDIR=$PACKAGES/vgm.5.1/install/lib/VGM-5.1.0/
G4VMCDIR=$PACKAGES/geant4_vmc.6.3.p2/install/lib/Geant4VMC-6.3.2/
VMCDIR=$PACKAGES/vmc.2.0/install/lib/VMC-2.0.0/
#__________________
# COMPILE AND BUILD
rm -rf build
rm -rf bin
mkdir build
cd build

cmake -DBUILD_A2_SIM=ON -DVGM_DIR=$VGMDIR -DGeant4VMC_DIR=$G4VMCDIR -DVMC_DIR=$VMCDIR ..
cmake --build . --target install -- -j4
#_____________
# BACK TO HOME
cd $AGRELEASE
pwd
echo "INSTALLATION COMPLETED" 
echo "MC data are saved to $MCDATA/root"
