TOPDIR=~/packages/simulation
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$TOPDIR/geant4_vmc_install/lib 
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$TOPDIR/vgm_install/lib 
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$TOPDIR/vmc_install/lib 
A2MC=~/a2mc
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$A2MC/install/lib
