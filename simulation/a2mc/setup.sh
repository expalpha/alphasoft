####################################################################################
# A2MC SETUP SCRIPT
####################################################################################

echo "Setting up .... "
echo -e "\t cp input/cmake_config_and_install.sh ./"
cp input/cmake_config_and_install.sh ./
echo -e "\t ==> PLEASE MODIFY ./cmake_config_and_install.sh to match your local installation"
echo -e "\t ==> then type \"source cmake_config_and_install.sh\" to build and install a2mc"
