///< ##############################################
///< Developed for the Alpha experiment [Nov. 2020]
///< lukas.golino@cern.ch
///< ##############################################

#include "a2mcPanelDIGI.h"

ClassImp(a2mcPanelDIGI)

using namespace std;

//_____________________________________________________________________________
a2mcPanelDIGI::a2mcPanelDIGI() 
  : fPanelID(-1),
    fEventID(-1),
    fEnergy(0.)
{
/// Default constructor
}

//_____________________________________________________________________________
a2mcPanelDIGI::~a2mcPanelDIGI() 
{
/// Destructor
}
//_____________________________________________________________________________
void a2mcPanelDIGI::Print(const Option_t* /*opt*/) const
{
/// Printing

  cout << "  Panel number: " << fPanelID 
       << "  panel released energy:  " << fEnergy
       << "  event ID:  " << fEventID
       << endl;
}

