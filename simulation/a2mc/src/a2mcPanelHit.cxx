///< ##############################################
///< Developed for the Alpha experiment [Aug. 2022]
///< lukas.golino@cern.ch
///< ##############################################

#include "a2mcPanelHit.h"

ClassImp(a2mcPanelHit)

using namespace std;

//_____________________________________________________________________________
a2mcPanelHit::a2mcPanelHit() 
  : fTrackID(-1),
    fPdgCode(-1),
    fMotherID(-1),
    fEvent(-1),
    fPanelID(-1),
    fEdep(0.)
{
/// Default constructor
}
//_____________________________________________________________________________
a2mcPanelHit::~a2mcPanelHit() 
{
/// Destructor
}
//_____________________________________________________________________________
void a2mcPanelHit::Print(const Option_t* /*opt*/) const
{
/// Printing

  cout << "  HIT| trackID: " << fTrackID 
       << "  Panel:  " << fPanelID
       << "  energy deposit (keV): " << fEdep * 1.0e06
       << "  position (cm): (" 
       << fPosX << ", " << fPosY << ", " << fPosZ << ")"
       << endl;
}

