///< ##############################################
///< Developed for the Alpha experiment [Aug. 2022]
///< lukas.golino@cern.ch
///< ##############################################

#include "a2mcPanelSD.h"
#include "a2mcRootManager.h"

ClassImp(a2mcPanelSD)

using namespace std;

//_____________________________________________________________________________
a2mcPanelSD::a2mcPanelSD(const char* name)
    : TNamed(name, ""),
    fHitCollection(0),
    fDIGICollection(0),
    fVerboseLevel(0)
{
    /// Standard constructor
    /// \param name  The hits collection name

    fHitCollection = new TClonesArray("a2mcPanelHit");
    fDIGICollection = new TClonesArray("a2mcPanelDIGI");


}

//_____________________________________________________________________________
    a2mcPanelSD::a2mcPanelSD()
: TNamed(),
    fHitCollection(0),
    fDIGICollection(0),
    fVerboseLevel(0)
{
    /// Default constructor
}

//_____________________________________________________________________________
a2mcPanelSD::~a2mcPanelSD()
{
    /// Destructor
}

//
// -----------------------------------> PUBLIC FUNCTIONS
//
//_____________________________________________________________________________
void a2mcPanelSD::Initialize()
{
    ///< Register hits collection in the Root manager;
    Register();
    ///< fSensitiveID stores the ID number of the silicon module volumes. 
    ///< The VMC can recover them with the gMC->VolId functions. The name 
    ///< of the volume is the required input. The names have been stored 
    ///< in a2mcApparatus in the silNameIDMap variable.
    ///< fSensitiveID is used in a2mcPanelSD::ProcessHits()
    //Scintillating panels
    fSensitiveID.push_back(gMC->VolId("CTOuterPanel"));
    fSensitiveID.push_back(gMC->VolId("CTInnerPanel"));
    fSensitiveID.push_back(gMC->VolId("ICPanel"));
    fSensitiveID.push_back(gMC->VolId("ATStkLargePanel"));
    fSensitiveID.push_back(gMC->VolId("ATStkSmallPanel"));
    fSensitiveID.push_back(gMC->VolId("ATPanel"));
    fSensitiveID.push_back(gMC->VolId("ICPanel"));

    std::cout << gMC->VolId("silMod") << "\n";
    std::cout << gMC->VolId("CTOuterPanel") << "\n";
    std::cout << gMC->VolId("CTInnerPanel") << "\n";
    std::cout << gMC->VolId("ICPanel") << "\n";
    std::cout << gMC->VolId("ATStkLargePanel") << "\n";
    std::cout << gMC->VolId("ATStkSmallPanel") << "\n";
    std::cout << gMC->VolId("ATPanel") << "\n";
    std::cout << gMC->VolId("MCP") << "\n";
    std::cout << gMC->VolId("silPCB") << "\n";
    
    fPanelNameIDMap = a2mcApparatus::Instance()->GetPanelNameIDMap(); 

    map<std::string, int>::iterator it = fPanelNameIDMap.begin();
    int count = 0;
    while(it != fPanelNameIDMap.end()) {
        std::string vol_name = it->first;
        std::cout << "VolName = " << vol_name << "\n";
        while(count <= it->second)
        {
            hasPanelHits.push_back(false); ///< Creating an entry for each module
            DigiID.push_back(-1); ///< Creating an entry for each module
            count++;
        }
        ++it;
    }

}
//_____________________________________________________________________________
void a2mcPanelSD::Register()
{
    /// Register the hits collection in the Root manager.

    a2mcRootManager::Instance()->Register("PanelHits", "TClonesArray", &fHitCollection);
    a2mcRootManager::Instance()->Register("PanelDIGI", "TClonesArray", &fDIGICollection);
}

//_____________________________________________________________________________
void a2mcPanelSD::BeginOfEvent()
{
}

int a2mcPanelSD::GetIDFromMap(const char* volname, int volID)
{
    std::string name = volname;
    name += "_";
    name += std::to_string(volID);
    //int ID = fPanelNameIDMap.find(name);
    int ID = -1;
        
    map<std::string, int>::iterator it = fPanelNameIDMap.find(name);
    if(it!=fPanelNameIDMap.end()) {
        ID = it->second;
    } else {
        cout << "a2mcPanelSD::GetIDFromMap --> Could not find panelID for name " << name << endl;
        return -1;
    }
    //std::cout << "finding name = " << name << ". got: " << ID << "\n";
    return ID;
}

//_____________________________________________________________________________
Bool_t a2mcPanelSD::ProcessHits()
{
    /*std::cout << gMC->VolId("silMod") << "\n";
    std::cout << gMC->VolId("CTOuterPanel") << "\n";
    std::cout << gMC->VolId("CTInnerPanel") << "\n";
    std::cout << gMC->VolId("ICPanel") << "\n";
    std::cout << gMC->VolId("ATStkSmallPanel") << "\n";
    std::cout << gMC->VolId("ATPanel") << "\n";
    std::cout << gMC->VolId("ATPMT_Stk") << "\n";
    std::cout << gMC->VolId("MCP") << "\n";
    std::cout << gMC->VolId("silPCB") << "\n";*/
    /// Create hits (in stepping).
    Int_t copyNo;
    Int_t volID                 = gMC->CurrentVolID(copyNo); ///< volID [MC internal numbering]
    const char* volName         = gMC->CurrentVolName(); ///< volID [MC internal numbering]
    /*std::cout << "Sens volume size = " << fSensitiveID.size() << "...\n";
    for(UInt_t i=0; i<fSensitiveID.size(); i++) {
    std::cout << "They are: " <<fSensitiveID[i]<< ":" << panelNameIDMap[fSensitiveID[i]] << "...\n";
    }*/

    //std::cout << "Got ourselves a hit on volume = " << volID << std::endl;
    Bool_t isPanel=false;
    for(UInt_t i=0; i<fSensitiveID.size(); i++) {
        if(volID==fSensitiveID[i]) {
            isPanel = true;
            break;
        }
    }
    Double_t edep_thrs = 0.;

    //std::cout << "isPanel = " << isPanel << std::endl;
    //std::cout << "TrackCharge = " << gMC->TrackCharge() << std::endl;
    //std::cout << "Edep = " << gMC->Edep() << std::endl;
    //std::cout << "edep_thrs = " << edep_thrs << std::endl;

    //std::cout << "Checking the criteria...\n";
    if(!isPanel)                    {/*std::cout << "failed on panel\n";*/ return false;} ///< We are not in one of the scint panel volumes
    if(gMC->TrackCharge() == 0.)    {/*std::cout << "failed on neutral\n";*/ return false;} ///< Not considering "hits" from neutral tracks
    if(gMC->Edep()<=edep_thrs)      {/*std::cout << "failed on threshold\n";*/ return false;} ///< Cutting on energy deposited (single hit)
    //std::cout << "Passed...\n";

    //std::cout << "We have a hit on vold id:" << volID << "...\n";
    //std::cout << "We have a hit on copyno:" << copyNo << "...\n";
    //std::cout << "you hit: " << gMC->VolName(volID) << "\n";
    ///< OTHER POSSIBLE SELECTIONS
//    if((int)part->GetMother(0)!=-1) return false;      ///< ONLY "HITS" RELEASED BY THE PRIMARY MUON 
//    if(gMC->TrackStep()==0.)        return false; ///< No step length (probably unused)
//    if(!gMC->IsTrackEntering()&&!gMC->IsTrackExiting()) return false; ///< Only "IN" and "OUT" hits

    ///< is indeed the copy number of the silPCB mount volume (the silicon module is hosted inside such volume)
    Int_t panelID = GetIDFromMap(volName, copyNo);
    // gMC->CurrentVolOffID(1,panelID); ///< Copy number of the mother volume
    if(panelID == -1)    return false; ///< Something went wrong

    // Track ID
    Int_t trackID = gMC->GetStack()->GetCurrentTrackNumber();
    TParticle* part = gMC->GetStack()->GetCurrentTrack();

    TLorentzVector pos;
    gMC->TrackPosition(pos);
    
//  Transforming global (world/master) position into local (fiber reference system)
    Double_t top_pos[3], loc_pos[3];
    top_pos[0] = pos.X(); top_pos[1] = pos.Y(); top_pos[2] = pos.Z(); 
    gMC->Gmtod(top_pos, loc_pos, 1);
    ///< Checking that the hit is in the "active part" of the silicon
    //if(n_strip==-1||p_strip==-1) return false; ///< return only if one of the two is -1 or need BOTH????
    TLorentzVector mom;
    gMC->TrackMomentum(mom);
    Double_t edep = gMC->Edep();
    if (edep==0.) return false;

    Double_t step = 0.;
    step = gMC->TrackStep();

    ///< Checking if "THIS TRACK" already released a hit in "THIS MODULE" and in "THESE STRIPS"
    bool createHit = true;
    Int_t updateHitID = -1;
    if(hasPanelHits[panelID]) { ///< "THIS MODULE" has already some hits
       for(int j=0; j<GetHitCollectionSize();j++) {
            a2mcPanelHit* theHit = GetHit(j);
            if(theHit->GetPanelID() != panelID)     continue; ///< Not the same module
            Int_t hitTrack = theHit->GetTrackID();
            if(trackID==hitTrack) { ///< Same module, same track -> merge the hits
                createHit = false;
                updateHitID = j; // This previous hit was released in this layer by the same track  
            }
        }
    }
    if(createHit) { ///< CREATE A NEW HIT
        a2mcPanelHit* newHit = AddHit();
        newHit->SetEventNumber  (gMC->CurrentEvent());
        newHit->SetTrackID(trackID);
        newHit->SetPdgCode(part->GetPdgCode());
        newHit->SetMotherID(part->GetMother(0)); // Get First Mother
        newHit->SetPos (TVector3(pos.X(), pos.Y(), pos.Z()));
        newHit->SetMom (TVector3(mom.X(), mom.Y(), mom.Z()));
        newHit->SetEdep(gMC->Edep());
        newHit->SetPanelID(panelID);
        hasPanelHits[panelID] = true;
    } else { ///< UPDATE AN EXISTING HIT (SAME MODULE, SAME TRACK)
        a2mcPanelHit* theHit = GetHit(updateHitID);
        ///< double check
        if(theHit->GetPanelID()!=panelID||theHit->GetTrackID()!=trackID) {
            std::cout << "a2mcPanelSD::ProcessHits -> check hit update procedure " << std::endl;
            std::cout << "theHit->GetPanelID() " << theHit->GetPanelID() << " panelID " << panelID << std::endl;
            std::cout << "theHit->GetTrackID() " << theHit->GetTrackID() << " trackID " << trackID << std::endl;
        }
        ///< Update energy (leave the other values unchanged)
        theHit->SetEdep(theHit->GetEdep()+gMC->Edep());
    }
    return true;
}

//_____________________________________________________________________________
//Off for now
void a2mcPanelSD::Digitalize()
{
     // Digitalize the hits collection

    Int_t nofHits = GetHitCollectionSize();


    //==================
    //Delete all this
    /*bool panel4 = false;
    bool panel5 = false;
    for(int j=0; j<nofHits;j++) 
    {
        a2mcPanelHit* theHit = GetHit(j);
        Int_t panelID = theHit->GetPanelID();
        if(panelID == 4)
        {
            panel4=true;
        }
        if(panelID == 5)
        {
            panel5=true;
        }
    }
    if(panel4 && panel5)
    {
        std::cout << "\n\n\n WE GOT THAT DOUBLE BOYS...\n\n" <<std::endl;
    }
    if(panel4)
    {
        std::cout << "Panel 4 hit.." <<std::endl;
    }
    if(panel5)
    {
        std::cout << "Panel 5 hit.." <<std::endl;
    }*/
    //==================



    // std::cout << "Attempting to DIGI" << std::endl;
    // if(nofHits>1) std::cout << "Number of hits " << nofHits << std::endl;
    for(int j=0; j<nofHits;j++) 
    {
        a2mcPanelHit* theHit = GetHit(j);
        Int_t panelID = theHit->GetPanelID();
        Double_t energy = theHit->GetEdep();
        // std::cout << " \t Hit edep " <<  energy << std::endl;
        //Double_t ePMT1  = theHit->GetEPMT1(); //Off for now but at some point we would like this. 
        //Double_t ePMT2  = theHit->GetEPMT2();

        bool createDIGI = true;
        if(DigiID[panelID]!= -1) createDIGI = false;

        if(createDIGI) 
        { // NEW DIGI
            a2mcPanelDIGI* newDIGI = AddDIGI();
            newDIGI->SetEventID(theHit->GetEventNumber());
            newDIGI->SetPanelID(panelID);
            newDIGI->SetEnergy(energy);
            //newDIGI->SetEnePMT1(ePMT1);
            //newDIGI->SetEnePMT2(ePMT2);
            DigiID[panelID] = GetDIGICollectionSize()-1;
        } 
        else 
        { // Updating DIGI info
            a2mcPanelDIGI* theDIGI = GetDIGI(DigiID[panelID]);
            Double_t e = theDIGI->GetEnergy();
            theDIGI->SetEnergy(e+energy);
            //Double_t e1 = theDIGI->GetEnePMT1();
            //Double_t e2 = theDIGI->GetEnePMT2();
            //theDIGI->SetEnePMT1(e1+ePMT1);
            //theDIGI->SetEnePMT2(e2+ePMT2);
        }
    }
    if(nofHits>1) {
        //std::cout << "Number of DIGI " << GetDIGICollectionSize() << std::endl;
        for(unsigned int i=0; i<(unsigned int)GetDIGICollectionSize(); i++) {
            a2mcPanelDIGI* theDIGI = GetDIGI(i);
            //std::cout << " \t DIGI edep " << theDIGI->GetEnergy() << std::endl;
        }
    }


}

//_____________________________________________________________________________
void a2mcPanelSD::EndOfEvent()
{
    /// Print hits collection (if verbose)
    /// and delete hits afterwards.

    if (fVerboseLevel>0)  Print();

    // Reset hits collection
    if(fHitCollection) fHitCollection->Delete();  
    // Reset hits collection
    if(fDIGICollection) fDIGICollection->Delete();  
    // Reset DIGIID
    for(int i=0; i<DigiID.size(); i++)
    {
        DigiID[i] = -1;
    }
}
//
// -----------------------------------> PRIVATE FUNCTIONS
//
//_____________________________________________________________________________
a2mcPanelHit* a2mcPanelSD::AddHit()
{
    /// Create a new hit in the TClonesArray.
    /// \return  The new hit

    TClonesArray& ref = *fHitCollection;
    Int_t size = ref.GetEntriesFast();
    return new(ref[size]) a2mcPanelHit();
}
//_____________________________________________________________________________
a2mcPanelHit* a2mcPanelSD::GetHit(Int_t i)
{
    /// Get the a2mcSilHit from the SilSD Hits collection
    //  \return  The hit

    return (a2mcPanelHit*)fHitCollection->At(i);    

}
//_____________________________________________________________________________
a2mcPanelDIGI* a2mcPanelSD::AddDIGI()
{
    // Create a new digit in the TClonesArray.
    //  \return  The new digit

    TClonesArray& ref = *fDIGICollection;
    Int_t size = ref.GetEntriesFast();

    return new(ref[size]) a2mcPanelDIGI();    

}
//_____________________________________________________________________________
a2mcPanelDIGI* a2mcPanelSD::GetDIGI(Int_t i)
{
    /// Get the a2mcDIGI from the SilSD DIGI collection
    //  \return  The DIGI

    return (a2mcPanelDIGI*)fDIGICollection->At(i);    

}
//_____________________________________________________________________________
Int_t a2mcPanelSD::GetHitCollectionSize()
{
    /// Return Hits Collection size
    TClonesArray& ref = *fHitCollection;
    Int_t size = ref.GetEntriesFast();
    return size;
}
//_____________________________________________________________________________
Int_t a2mcPanelSD::GetDIGICollectionSize()
{
    /// Return DIGI Collection size
    TClonesArray& ref = *fDIGICollection;
    Int_t size = ref.GetEntriesFast();
    return size;
}
// PRINT FUNCTION
//_____________________________________________________________________________
void a2mcPanelSD::Print(const Option_t* /*option*/) const
{
    /// Print the hits collection and the DIGI collection.

    Int_t nofHits = fHitCollection->GetEntriesFast();
    Int_t nofDigi = fDIGICollection->GetEntriesFast();

    cout << "\n-------->Hits Collection: in this event there are " << nofHits 
            << " hits in the Panels: " << endl;

    for (Int_t i=0; i<nofHits; i++) (*fHitCollection)[i]->Print();          
    for (Int_t i=0; i<nofDigi; i++) (*fDIGICollection)[i]->Print();          
}
