#ifndef a2mcPanelSD_H
#define a2mcPanelSD_H

#include <TNamed.h>
#include <TClonesArray.h>
#include <TParticle.h>

#include "a2mcPanelHit.h"
#include "a2mcPanelDIGI.h"
#include "a2mcApparatus.h"

class a2mcPanelHit;
class a2mcPanelDIGI; //Turned on.
class a2mcApparatus;

class a2mcPanelSD : public TNamed
{
    public:
        a2mcPanelSD(const char* name);
        a2mcPanelSD();
        virtual ~a2mcPanelSD();

       // -------> PUBLIC FUNCTIONS
        void    Initialize();
        void    Register();
        void    BeginOfEvent();
        int GetIDFromMap(const char* volname, int volID);
        Bool_t  ProcessHits();
        void    EndOfEvent();
        virtual void  Print(const Option_t* option = 0) const;
        void    Digitalize();

        // -------> SET METHODS
        void SetVerboseLevel(Int_t level);

    private:
        // -------> PRIVATE FUNCTIONS
        void        IDToPanelMod(Int_t, Int_t&, Int_t&);
        //Not needed since we don't have SVD panels. 
        //Double_t    GetnPos(Int_t);
        //Double_t    GetpPos(Int_t);
        //Int_t       ReturnPStrip(Int_t, Double_t);
        //Int_t       ReturnNStrip(Int_t, Double_t);

        a2mcPanelHit* AddHit();             // Add the hit to the collection
        a2mcPanelHit* GetHit(Int_t);        // Get the hit from collection

        Int_t GetHitCollectionSize();   // Get the hits collection size
        
        a2mcPanelDIGI* AddDIGI();      // Add the digit to the collection
        a2mcPanelDIGI* GetDIGI(Int_t); // Get the digit from the DIGI collection
        Int_t GetDIGICollectionSize();  // Get the DIGI collection size



        // -------> PRIVATE VARIABLES
        // REMEMBER TO CHANGE SIZE IF MORE SilS
        TClonesArray*               fHitCollection;   //  Hits collection    
        std::vector<Int_t>          fSensitiveID;
        Int_t                       fVerboseLevel;    // Verbosity level
        std::map<std::string, int>  fPanelNameIDMap;
        std::vector<bool>           hasPanelHits;
        std::vector<int>            DigiID;
        TClonesArray*               fDIGICollection;  //  Digits collection
        
        ClassDef(a2mcPanelSD,1)
};

/// Set verbose level
/// \param level The new verbose level value
inline void a2mcPanelSD::SetVerboseLevel(Int_t level) 
{ fVerboseLevel = level; }


#endif //a2mcPanelSD_H

