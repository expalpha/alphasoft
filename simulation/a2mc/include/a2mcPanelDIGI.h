#ifndef a2mc_PanelDIGI_H
#define a2mc_PanelDIGI_H

#include <iostream>
#include <TObject.h>

class a2mcPanelDIGI : public TObject
{
    public:
        a2mcPanelDIGI();
        virtual ~a2mcPanelDIGI();

        // -------> PUBLIC FUNCTIONS
        virtual void Print(const Option_t* option = "") const;

        // -------> SET METHODS

        // Set event ID 
        void SetEventID(Int_t id)   { fEventID = id; }; 

        // Set panel ID 
        void SetPanelID(Int_t id)   { fPanelID = id; };  

        // Set energy 
        void SetEnergy(Double_t e)  { fEnergy = e; };

        // -------> GET METHODS

        // Get panel ID
        Int_t GetPanelID() { return fPanelID; };
 
        // Get event ID
        Int_t GetEventID() { return fEventID; };

        // Get energy
        Double_t GetEnergy()   	{ return fEnergy; };


        


        // -------> PRIVATE VARIABLES
    private:
        // For the Silillators: panel ID = Silillator number
        Int_t      fEventID;     // Event ID 
        Int_t      fPanelID;     // panel ID 
        Double_t   fEnergy;     // Energy released in the panel

        ClassDef(a2mcPanelDIGI,1) //a2mcPanelDIGI  
};

#endif //a2mcPanelDIGI_H


