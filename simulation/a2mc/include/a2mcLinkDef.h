///< a2mcLinkDef.h
///< The CINT link definitions for a2mc classes

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ class  a2mcMessenger+;
#pragma link C++ class  a2mcVirtualMC+;
#pragma link C++ class  a2mcStack+;
#pragma link C++ class  a2mcMCTrack+;
#pragma link C++ class  a2mcSettings+;
#pragma link C++ class  a2mcApparatus+;
#pragma link C++ class  a2mcFieldConstant+;
#pragma link C++ class  a2mcFieldFromMap+;
#pragma link C++ class  a2mcPrimary+;
#pragma link C++ class  a2mcGenerator+;
#pragma link C++ class  a2mcHit+;
#pragma link C++ class  a2mcSilHit+;
#pragma link C++ class  a2mcSilDIGI+;
#pragma link C++ class  a2mcSilSD+;
#pragma link C++ class  a2mcPanelHit+;
#pragma link C++ class  a2mcPanelDIGI+;
#pragma link C++ class  a2mcPanelSD+;
#pragma link C++ class  a2mcRootManager+;


#pragma link C++ enum   FileMode;

#endif

