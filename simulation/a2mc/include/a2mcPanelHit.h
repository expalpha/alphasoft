#ifndef a2mc_PanelHIT_H
#define a2mc_PanelHIT_H

#include <iostream>

#include <TObject.h>
#include <TVector3.h>

class a2mcPanelHit : public TObject
{
  public:
    a2mcPanelHit();
    virtual ~a2mcPanelHit();

    ///< general methods
    virtual void Print(const Option_t* option = "") const;

    ///< set methods
    void SetTrackID(Int_t track)        {fTrackID   = track; };
    void SetPdgCode(Int_t pdg)          {fPdgCode   = pdg; };
    void SetMotherID(Int_t mid)         {fMotherID  = mid; };
    void SetEventNumber(Int_t ievt)     {fEvent     = ievt; };
    void SetPanelID(Int_t panel)        {fPanelID     = panel; };
    void SetEdep(Double_t de)           {fEdep      = de; };
    void SetPos(TVector3 xyz)           {fPosX      = xyz.X(); fPosY = xyz.Y(); fPosZ = xyz.Z();};
    void SetMom(TVector3 xyz)           {fMomX      = xyz.X(); fMomY = xyz.Y(); fMomZ = xyz.Z();};
      
    ///< get methods    
    Int_t GetTrackID()      {return fTrackID;};
    Int_t GetPdgCode()      {return fPdgCode;};
    Int_t GetEventNumber()  {return fEvent;};
    Int_t GetPanelID()        {return fPanelID;};


    Double_t GetEdep()      {return fEdep;};

  private:
    Int_t       fTrackID;   ///< Track Id
    Int_t       fPdgCode;   ///< Particle PDG Code
    Int_t       fMotherID;  ///< Particle mother ID (-1 = primary, 0 = secondary, etc..)
    Int_t       fEvent;     ///< Event Number
    Int_t       fPanelID;     ///< Which panel?
    Double_t    fEdep;      ///< Energy deposit
    Double_t    fPosX;      ///< Hit coordinates (at the entrance of the detector)
    Double_t    fPosY;
    Double_t    fPosZ;
    Double_t    fMomX;      ///< Track momentum when releasing the hit
    Double_t    fMomY;
    Double_t    fMomZ;
    
  ClassDef(a2mcPanelHit,1)
};

#endif //a2mc_SilHIT_H


