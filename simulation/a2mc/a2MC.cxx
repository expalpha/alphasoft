///< ##############################################
///< Based on the VMC framework by I. Hřivnáčová 
///< https://vmc-project.github.io/
///< Developed for the Alpha experiment [Nov. 2020]
///< germano.bonomi@cern.ch
///< ##############################################

#include "TG4RunConfiguration.h"
#include "TGeant4.h"
#include "TThread.h"

#include "a2mcVirtualMC.h"
#include "a2mcUtility.h"
#include "a2mcSettings.h"

int main(int argc, char** argv) {
    TThread::Initialize();
    gInterpreter->SetProcessLineLock(false);
  
    Utility utils(argc, argv);
    string iniFile  = utils.GetIniFile();
    a2mcSettings a2mcConf;
    a2mcConf.init(iniFile);
    if(!a2mcConf.isValid()) return 0;
// Check if the "root" and "output" folder already exist otherwise create them
    if(!utils.checkDir(getenv("MCDATA"),"root"))  {
        cout << "Creating root subdirectory " << endl;
	TString cmd = TString::Format("mkdir -p %s/root",getenv("MCDATA"));
        gSystem->Exec(cmd.Data());
    }
    if(!utils.checkDir(getenv("MCDATA"),"output"))  {
        cout << "Creating output subdirectory " << endl;
	TString cmd = TString::Format("mkdir -p %s/output",getenv("MCDATA"));
        gSystem->Exec(cmd.Data());
    }
  
// seed generator
    TDatime dt;
    UInt_t curtime=dt.Get();
    UInt_t procid=gSystem->GetPid();
    UInt_t runSeed=curtime-procid;
    gRandom->SetSeed(runSeed);
//    gRandom->SetSeed(0);
    ///< Creating the Virtual MC object
    a2mcVirtualMC* virtualMC = new a2mcVirtualMC("a2mcVirtualMC", "The Alpha2 MC", utils.GetRun(), a2mcConf, utils.GetRunTime(), runSeed);
  
    ostringstream sgeo;
    sgeo << "geomRootToGeant4";
    TG4RunConfiguration* runConfiguration = new TG4RunConfiguration(sgeo.str().c_str(), "FTFP_BERT_TRV","stepLimiter+specialCuts+specialControls",false,false);
    TGeant4* geant4 = new TGeant4("TGeant4", "The Geant4 Monte Carlo", runConfiguration, argc, argv);
    TString g4macro = TString::Format("%s/simulation/a2mc/g4config.in",getenv("AGRELEASE"));
    geant4->ProcessGeantMacro(g4macro.Data());
  
    virtualMC->InitMC("");
    virtualMC->RunMC(utils.GetNEvents());
  
    delete virtualMC;
    return 0;
}
