#include "TMVADumper.h"

#include "TSiliconEvent.h"
#include "TAlphaEvent.h"
#include "SiMod.h"


class TA2MVADumper: public TMVADumper
{
    public:
   /// @brief Default constructor. Creates branches for each variable names in the dumper class.
   /// @param tree TTree to dump the data to.
    TA2MVADumper(TTree* tree): TMVADumper(tree)
    {
        
    }
   /// @brief Update the variables before dumping to root file.
   /// @param siliconEvent TSiliconEvent for obtaining the event information.
   /// @param alphaEvent TAlphaEvent also needed for some dumpers.    
   /// @param currentEventNumber EventNumber to know whether to dump or not.    
   /// @return Success or not.
    virtual bool UpdateVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent , int currentEventNumber) = 0;
};

/// @brief Simple class that writes out the Event ID number and Run Number
class TA2MVAEventIDDumper: public TA2MVADumper
{
   public:
      int fRunNumber;
      int fEventID;

   /// <inheritdoc />
   TA2MVAEventIDDumper(TTree* tree): TA2MVADumper(tree)
   {
      fTree->Branch("RunNumber", &fRunNumber, "RunNumber/I");
      fTree->Branch("EventID", &fEventID, "RunNumber/I");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("RunNumber", &fRunNumber);      
      reader->AddVariable("EventID", &fEventID);
   }

   /// <inheritdoc />
   bool UpdateVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent , int currentEventNumber)
   {
      std::ignore = alphaEvent;
      // Safely handle case where we have the wrong Event ID
      if ( currentEventNumber != siliconEvent->GetVF48NEvent() )
      {
         return false;
      }
      // Update class members
      fEventID = siliconEvent->GetVF48NEvent();
      fRunNumber = siliconEvent->GetRunNumber();
      // Report success!
      return true;
   }
   
   //
   

};

/// @brief Dumps the classic hyper-parameters used for example in the 2018 papers.
class TA2MVAClassicDumper: public TA2MVADumper
{
   public:
      float nhits,residual,r,S0rawPerp,S0axisrawZ,phi_S0axisraw,nCT,nGT,tracksdca,curvemin,curvemean,lambdamin,lambdamean,curvesign,phi;
   
   /// <inheritdoc />
   TA2MVAClassicDumper(TTree* tree): TA2MVADumper(tree)
   {
      fTree->Branch("nhits", &nhits, "nhits/F");
      fTree->Branch("residual", &residual, "residual/F");
      fTree->Branch("r", &r, "r/F");
      fTree->Branch("S0rawPerp", &S0rawPerp, "S0rawPerp/F");
      fTree->Branch("S0axisrawZ", &S0axisrawZ, "S0axisrawZ/F");
      fTree->Branch("phi_S0axisraw", &phi_S0axisraw, "phi_S0axisraw/F");
      fTree->Branch("nCT", &nCT, "nCT/F");
      fTree->Branch("nGT", &nGT, "nGT/F");
      fTree->Branch("tracksdca", &tracksdca, "tracksdca/F");
      fTree->Branch("curvemin", &curvemin, "curvemin/F");
      fTree->Branch("curvemean", &curvemean, "curvemean/F");
      fTree->Branch("lambdamin", &lambdamin, "lambdamin/F");
      fTree->Branch("lambdamean", &lambdamean, "lambdamean/F");
      fTree->Branch("curvesign", &curvesign, "curvesign/F");
      fTree->Branch("phi", &phi, "phi/F");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("nhits", &nhits);      
      reader->AddVariable("residual", &residual);
      reader->AddVariable("r", &r);
      reader->AddVariable("S0rawPerp", &S0rawPerp);
      reader->AddVariable("S0axisrawZ", &S0axisrawZ);
      reader->AddVariable("phi_S0axisraw", &phi_S0axisraw);
      reader->AddVariable("nCT", &nCT);
      reader->AddVariable("nGT", &nGT);
      reader->AddVariable("tracksdca", &tracksdca);
      reader->AddVariable("curvemin", &curvemin);
      reader->AddVariable("curvemean", &curvemean);
      reader->AddVariable("lambdamin", &lambdamin);
      reader->AddVariable("lambdamean", &lambdamean);
      reader->AddVariable("curvesign", &curvesign);
      reader->AddVariable("phi", &phi);
   }

   /// <inheritdoc />
   bool UpdateVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent , int currentEventNumber)
   {
      if ( currentEventNumber != siliconEvent->GetVF48NEvent() )
      {
         //std::cout<<currentEventNumber <<" != "<< siliconEvent->GetVF48NEvent() <<std::endl;
         return false;
      }
      //std::cout<<"\t"<<currentEventNumber <<" == "<< siliconEvent->GetVF48NEvent()<< "  :)" <<std::endl;
      this->nhits=alphaEvent->GetNHits();
      this->residual = siliconEvent->GetResidual();
      TVector3* vtx = siliconEvent->GetVertex();
      this->r = vtx->Perp();
      this->phi = vtx->Phi();
      
      this->tracksdca=alphaEvent->GetVertex()->GetDCA();     
      std::vector<double> velxraw;
      std::vector<double> velyraw;
      std::vector<double> velzraw;

      Int_t nAT =  alphaEvent->GetNHelices(); // all tracks
      this->nCT = 0;
      Int_t nraw = 0;
  
      //Unused
      //Double_t AT_MeanHitSig=0; //Average hit significance
      //Unused
      //Double_t CT_MeanHitSig=0; //Average hit significance
      //Unused
      //Double_t AT_SumHitSig=0;
      //Unused
      //Double_t CT_SumHitSig=0;
      //Unused
      //Int_t AT_HitSigCounter=0;
      //Unused
      //Int_t CT_HitSigCounter=0;
      for (int i = 0; i< nAT ; ++i)
      {
         TAlphaEventHelix* aehlx = alphaEvent->GetHelix(i);
         if (!aehlx) continue;
         // unused
         //Double_t fc = aehlx->Getfc();
         Double_t fphi0 = aehlx->Getfphi();
         Double_t fLambda = aehlx->Getflambda();
         //Double_t s=0.; // calculates velx,y,z at POCA
         // special case for s = 0
         Int_t HelixHits=aehlx->GetNHits();
         for (int j=0; j<HelixHits; j++)
         {
            //Unused
            //TAlphaEventHit* aehlx_hit=aehlx->GetHit(j);
            //Unused
            //AT_SumHitSig+=aehlx_hit->GetHitSignifance();
            //Unused
            //AT_HitSigCounter++;
            //delete aehlx_hit;
         }
         // select good helices, after removal of duplicates

         // This seems to be true all the time... please check - Joe
         if (aehlx->GetHelixStatus()==1)
         {
            for (int j=0; j<HelixHits; j++)
            {
               //Unused
               //TAlphaEventHit* aehlx_hit=aehlx->GetHit(j);
               //Unused
               //CT_SumHitSig+=aehlx_hit->GetHitSignifance();
               //Unused
               //CT_HitSigCounter++;
               //delete aehlx_hit;
            }
            ++nraw; // == ntracks
            velxraw.push_back( - TMath::Sin(fphi0));
            velyraw.push_back( TMath::Cos(fphi0)) ;
            velzraw.push_back( fLambda );
            this->nCT++;
         }
      }
      std::vector<double> velx;
      std::vector<double> vely;
      std::vector<double> velz;
      // alpha event part
      TAlphaEventVertex* aevtx = alphaEvent->GetVertex();
      Int_t nGTL = aevtx->GetNHelices();// tracks with vertex
      this->nGT = 0;
      this->curvemin=9999.;
      this->curvemean=0.;
      this->lambdamin=9999.;
      this->lambdamean=0;
      this->curvesign=0;
      for (int i = 0; i< nGTL ; ++i)
      {
         TAlphaEventHelix* aehlx = aevtx->GetHelix(i);
         //if(aehlx->GetHelixStatus()<0) continue;
         Double_t fc = aehlx->Getfc();
         Double_t fphi0 = aehlx->Getfphi();
         Double_t fLambda = aehlx->Getflambda();

         //Unused
         //Double_t s=0.; // calculates velx,y,z at POCA
         // special case for s = 0
         velx.push_back( - TMath::Sin(fphi0) );
         vely.push_back( TMath::Cos(fphi0) ) ;
         velz.push_back( fLambda );

         // select good helices, after removal of duplicates
         if (aehlx->GetHelixStatus()==1)
         {
            this->nGT++;
            this->curvemin= fabs(fc)>this->curvemin? this->curvemin:fabs(fc);
            this->lambdamin= fabs(fLambda)>this->lambdamin? this->lambdamin:fabs(fLambda);
            this->curvemean+=fabs(fc);
            this->lambdamean+=fabs(fLambda);
            this->curvesign+=(fc>0)?1:-1;
         }
      }
      if(this->nGT>0){
         this->lambdamean/=this->nGT;
         this->curvemean/=this->nGT;
      }

      //Unused
      //Double_t S0rawl1 = -99.;
      //Unused
      //Double_t S0rawl2 = -99.;
      //Unused
      //Double_t S0rawl3 = -99.;
      //Unused in online_mva
      Double_t S0axisrawX = -99.;
      //Unused in online_mva
      Double_t S0axisrawY = -99.;
      this->S0axisrawZ = -99.;

      if(nraw>0)
      {
         TVector3* S0axisraw;
         TVector3* S0valuesraw;
         sphericity(velxraw, velyraw, velzraw, 0, &S0axisraw, &S0valuesraw); // generalizedspher.h
         this->S0rawPerp = S0valuesraw->Perp();
         //Unused
         //S0rawl1 = S0valuesraw->X();
         //Unused
         //S0rawl2 = S0valuesraw->Y();
         //Unused
         //S0rawl3 = S0valuesraw->Z();

         //Unused in online_mva
         S0axisrawX = S0axisraw->X();
         //Unused
         S0axisrawY = S0axisraw->Y();
         this->S0axisrawZ = S0axisraw->Z();
         this->phi_S0axisraw = TMath::ACos(S0axisrawY/TMath::Sqrt(S0axisrawX*S0axisrawX+S0axisrawY*S0axisrawY));
         delete S0axisraw;
         delete S0valuesraw;
      }
      return true;
   }


};

/// @brief Classic X,Y,Z position dumper.
class TA2MVAXYZ: public TA2MVADumper
{
   public:
      float fX, fY, fZ;
   
   /// <inheritdoc />
   TA2MVAXYZ(TTree* tree): TA2MVADumper(tree)
   {
      fTree->Branch("X", &fX, "X/F");
      fTree->Branch("Y", &fY, "Y/F");
      fTree->Branch("Z", &fZ, "Z/F");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("X", &fX);
      reader->AddVariable("Y", &fY);
      reader->AddVariable("Z", &fZ);
   }

   /// <inheritdoc />
   bool UpdateVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent , int currentEventNumber)
   {
      std::ignore = alphaEvent;
      if ( currentEventNumber != siliconEvent->GetVF48NEvent() )
         return false;
      TVector3* vtx = siliconEvent->GetVertex();
      this->fX = vtx->X();
      this->fY = vtx->Y();
      this->fZ = vtx->Z();
      return true;
   }

   /// <inheritdoc />
   void PrintEvent()
   {
      std::cout << "==============================================================" << std::endl;
      std::cout << "===============Printing a TA2MVAXYZ event================" << std::endl;
      std::cout << "X = "                                      << fX << "\n"; 
      std::cout << "Y = "                                      << fY << "\n"; 
      std::cout << "Z = "                                       << fZ << "\n";
      std::cout << "================================================================" << std::endl;

   }

};

/// @brief Copy this as a template for your own implementation
class TA2MVATestDumper: public TA2MVADumper
{
   public:   
      float fX;

   /// <inheritdoc />
   TA2MVATestDumper(TTree* tree): TA2MVADumper(tree)
   {
      fTree->Branch("X", &fX, "X/F");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("X", &fX);      
   }

   /// <inheritdoc />
   bool UpdateVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent , int currentEventNumber)
   {
      std::ignore = alphaEvent; //Use if you need

      if ( currentEventNumber != siliconEvent->GetVF48NEvent() )
         return false;

      TVector3* vtx = siliconEvent->GetVertex();
      this->fX = vtx->X();
      return true;
   }
};


/// @brief Dumping silicon data
class TA2MVASiliconHitDumper: public TA2MVADumper
{
   public:   
      const int fNumASICs = 4;
      const int fNumChannels = 128;
      const int fNumSil = 72;
      //Remember this vector is in blocks of 512, each one a different silicon module
      //Each 512 chunk of this vector is 4x128, the signal on each ASIC.
      //For example signal on channel 10, of ASIC 2, on silicon module 12
      //Would give (i,j,k) = (12,2,10) = 6410
      //F(x, y, z) = z + 128*y + 128×4*x where x = 12, y = 2, z = 10 = 6410
      //Everything counts from 0 so F(0,0,0) = 0
      std::vector<double>* fSiliconModules;

   /// <inheritdoc />
   TA2MVASiliconHitDumper(TTree* tree): TA2MVADumper(tree)
   {
      fSiliconModules = new std::vector<double>(fNumSil*fNumASICs*fNumChannels, 0.);
      fTree->Branch("fSiliconModules", "std::vector<double>", &fSiliconModules);
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      
      char typefloat = 'F';
      reader->DataInfo().AddVariablesArray("fSiliconModules", fNumSil*fNumASICs*fNumChannels, "fSiliconModules", "nHits", -999., 999., typefloat, false, &fSiliconModules);
   }

   /// <inheritdoc />
   bool UpdateVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent , int currentEventNumber)
   {
      std::ignore = alphaEvent; //Use if you need

      if ( currentEventNumber != siliconEvent->GetVF48NEvent() )
         return false;

      int ijk=0;
      for( int i = 0; i < fNumSil; i++ ) // Loop through the height.
      {
         TAlphaEventSil* sil = alphaEvent->GetSilByNumber(i, true);
         bool silHit = false;
         if(sil)
            silHit = true;
         else
            silHit = false;
         for( int j = 0; j < fNumASICs; j++ ) // Loop through the rows.
         {
            for( int k = 0; k < fNumChannels; k++ ) // Loop through the columns.
            {
               ijk = k + fNumChannels * j + fNumChannels * fNumASICs * i;
               if(silHit)
                  fSiliconModules->at(ijk) = alphaEvent->GetSilByNumber(i)->GetASIC(j, k);
               else
                  fSiliconModules->at(ijk) = 0.;
            }
         }
      }

      return true;
   }
};


/// @brief Dumping silicon data
class TA2MVASiliconRMSDumper: public TA2MVADumper
{
   public:   
      const int fNumSil = 72;
      const int fNumASICs = 4;
      const int fNumChannels = 128;
      //Remember this vector is in blocks of 512, each one a different silicon module
      //Each 512 chunk of this vector is 4x128, the signal on each ASIC.
      //For example signal on channel 10, of ASIC 2, on silicon module 12
      //Would give (i,j,k) = (12,2,10) = 6410
      //F(x, y, z) = z + 128*y + 128×4*x where x = 12, y = 2, z = 10 = 6410
      //Everything counts from 0 so F(0,0,0) = 0
      std::vector<double>* fSiliconModulesRMS;

   /// <inheritdoc />
   TA2MVASiliconRMSDumper(TTree* tree): TA2MVADumper(tree)
   {
      fSiliconModulesRMS = new std::vector<double>(fNumSil*fNumASICs*fNumChannels, 0.);
      fTree->Branch("fSiliconModulesRMS", "std::vector<double>", &fSiliconModulesRMS);
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      
      char typefloat = 'F';
      reader->DataInfo().AddVariablesArray("fSiliconModulesRMS", fNumSil*fNumASICs*fNumChannels, "fSiliconModulesRMS", "RMS", -999., 999., typefloat, false, &fSiliconModulesRMS);
   }

   /// <inheritdoc />
   bool UpdateVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent , int currentEventNumber)
   {
      std::ignore = alphaEvent; //Use if you need

      if ( currentEventNumber != siliconEvent->GetVF48NEvent() )
         return false;

      int ijk=0;
      for( int i = 0; i < fNumSil; i++ ) // Loop through the height.
      {
         TAlphaEventSil* sil = alphaEvent->GetSilByNumber(i, true);
         bool silHit = false;
         if(sil)
            silHit = true;
         else
            silHit = false;
         for( int j = 0; j < fNumASICs; j++ ) // Loop through the rows.
         {
            for( int k = 0; k < fNumChannels; k++ ) // Loop through the columns.
            {
               ijk = k + fNumChannels * j + fNumChannels * fNumASICs * i;
               if(silHit)
                  fSiliconModulesRMS->at(ijk) = alphaEvent->GetSilByNumber(i)->GetRMS(j, k);
               else
                  fSiliconModulesRMS->at(ijk) = 0.;
            }
         }
      }

      return true;
   }
};


/// @brief Copy this as a template for your own implementation
class TA2MVAStatusDumper: public TA2MVADumper
{
   public:   
      float fvStatus;

   /// <inheritdoc />
   TA2MVAStatusDumper(TTree* tree): TA2MVADumper(tree)
   {
      fTree->Branch("vStatus", &fvStatus, "vStatus/F");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("vStatus", &fvStatus);      
   }

   /// <inheritdoc />
   bool UpdateVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent , int currentEventNumber)
   {
      std::ignore = alphaEvent; //Use if you need

      if ( currentEventNumber != siliconEvent->GetVF48NEvent() )
         return false;

      this->fvStatus = siliconEvent->GetVertexType();
      return true;
   }
};
