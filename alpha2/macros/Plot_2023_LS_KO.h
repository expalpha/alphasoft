
#include "TA2Plot.h"
#include "TA2Plot_Filler.h"

#include "TA2SpillGetters.h"
#include "TA2Spill.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TAPlotVertexEvents.h"

#include <vector>
#include <utility> //std::pair

std::vector<TA2Plot*> Plot_243_Light_And_Dark_Lineshape(int runNumber, bool DrawVertices, bool zeroTime = true);
double GetRep(double time, const std::vector<TA2Spill>& LaserSpills, int& lastRep, bool& IsLight);
