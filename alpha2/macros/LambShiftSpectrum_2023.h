//
// Macro for plotting/extracting data from the lamb shift experiments done in 2023.
// Each run was done with a different config, the struct "LambShiftExperimentConfig" is per run, and can only be found from reading the datalog
// The main function here is PlotLambShiftSpectrum() that will generate a lineshape and extract statistics for your run. Just add the config to the GetLambShiftConfig function.
//
// L GOLINO, JTK McKENNA
//

#include "GetPCErrors.C"
#include "ErrorCombination.h"


/// Structure compiling the experimental config for the lamb shift.
struct LambShiftExperimentConfig
{
    int fNumberOfFreqs; //INCLUDING off res [0]
    std::vector<double> fFreqOffset;
    std::vector<double> fFreqOffseterrors;
    double fZCut;
    bool fZeroTime;
    int fFirstLegitDump; //This is the first dump you actually want (counting from 0)
    int fLastLegitDump; //This is the last dump you actually want (counting from 0)
};


//Function to a return a LambShiftExperimentConfig from runNumber
LambShiftExperimentConfig GetLambShiftConfig(int runNumber, bool isCState = false)
{
    LambShiftExperimentConfig config;
    if(runNumber == 68465)
    {
        config.fFirstLegitDump = 10;
        config.fLastLegitDump = -1; //This gets all
        config.fNumberOfFreqs = 6;
        config.fFreqOffset = {-680, -150, -75, 0, 75, 150};
        for(auto freq: config.fFreqOffset) config.fFreqOffseterrors.push_back(0);
        config.fZCut = 30;
        config.fZeroTime = true;
    }
    else if(runNumber == 68475 )
    {
        config.fFirstLegitDump = 16;
        config.fLastLegitDump = -1; //This gets all
        config.fNumberOfFreqs = 8;
        config.fFreqOffset = {-680, -150, -75, -75/2, 0, 75, 150, 300};
        for(auto freq: config.fFreqOffset) config.fFreqOffseterrors.push_back(0);
        config.fZCut = 30;
        config.fZeroTime = true;
    }
    else if(runNumber == 68481 )
    {
        config.fFirstLegitDump = 16;
        config.fLastLegitDump = -1; //This gets all
        config.fNumberOfFreqs = 11;
        config.fFreqOffset =  {-683.56, -150, -75, -37.5, -18.75, 0, 37.5, 75, 150, 300, 683.56};
        for(auto freq: config.fFreqOffset) config.fFreqOffseterrors.push_back(0);
        config.fZCut = 30;
        config.fZeroTime = true;
    }
    else if(runNumber == 68489 )
    {
        if (isCState)
        {
           config.fFirstLegitDump = 0;
           config.fLastLegitDump = -1; //This gets all
        }
        else
        {
            config.fFirstLegitDump = 39;
            config.fLastLegitDump = -1; //This gets all
        }
        config.fNumberOfFreqs = 11;
        config.fFreqOffset =  {-683.56, -150, -75, -37.5, -18.75, 0, 37.5, 75, 150, 300, 683.56};
        for(auto freq: config.fFreqOffset) config.fFreqOffseterrors.push_back(0);
        config.fZCut = 30;
        config.fZeroTime = true;
    }
    else if(runNumber == 68498 )
    {
        if (isCState)
        {
            config.fFirstLegitDump = 0;
            config.fLastLegitDump = -1; //This gets all
            config.fFreqOffset =  {-376, -270, -150, -75, 0, 75, 142, 991};
        }
        else
        {
            config.fFirstLegitDump = 4;
            config.fLastLegitDump = 203;
            config.fFreqOffset =  {-199, -150, -75, 0, 75, 150, 230, 334.5};
        }
        config.fNumberOfFreqs = 8;
        for(auto freq: config.fFreqOffset) config.fFreqOffseterrors.push_back(0);
        config.fZCut = 30;
        config.fZeroTime = true;
    }
    else if(runNumber == 68506 )
    {
        config.fFirstLegitDump = 93;
        config.fLastLegitDump = 179; //This gets all
        config.fNumberOfFreqs = 11;
        config.fFreqOffset =  {-683.56, -150, -75, -37.5, -18.75, 0, 37.5, 75, 150, 300, 683.56};
        for(auto freq: config.fFreqOffset) config.fFreqOffseterrors.push_back(0);
        config.fZCut = 30;
        config.fZeroTime = true;
    }
    else
    {
        std::cout << "We dont have the variables for this run, please add them to the GetLambShiftConfig and re-run\n";
        config.fNumberOfFreqs = -1;
        config.fFreqOffset = {-1};
        config.fFreqOffseterrors = {-1};
        config.fZCut = -1;
        config.fZeroTime = -1;
        config.fFirstLegitDump = -1;
        config.fLastLegitDump = -1; 

    }
    return config;
}


TA2Plot* PlotLambShiftSpectrum(int runNumber, bool isCState = false )
{

    std::cout << "============================================\n";
    std::cout << "Begin diagnostic output\n";
    std::cout << "============================================\n";

    //Setup constant variables to be used throughout that are different per run:
    LambShiftExperimentConfig currentConfig = GetLambShiftConfig(runNumber, isCState);
    //Variables set up. If the run isnt added just return an empty TA2Plot (fail)
    if(currentConfig.fNumberOfFreqs < 0)
    {
        std::cout << "We dont have the variables for this run, please add them to the GetLambShiftConfig and re-run\n";
        return new TA2Plot();
    }

    //Initialise vector of TA2Plots
    TA2Plot* plots[currentConfig.fNumberOfFreqs];
    //A TA2Plot to just add everything
    TA2Plot* totalPlot;
    //And a TA2Plot for the MW power off
    TA2Plot* powerOffPlot;

    //Initalise the actual objects
    totalPlot=new TA2Plot(-currentConfig.fZCut,currentConfig.fZCut,currentConfig.fZeroTime);
    powerOffPlot=new TA2Plot(-currentConfig.fZCut,currentConfig.fZCut,currentConfig.fZeroTime);
    for (int i=0; i<currentConfig.fNumberOfFreqs; i++)
    {
         plots[i]=new TA2Plot(-currentConfig.fZCut,currentConfig.fZCut,currentConfig.fZeroTime);
    }
    
    if (isCState)
    {
       if (Get_A2_Spills(runNumber, {"c MW*"},{-1}).empty())
       {
         std::cout <<" No C state experiment in this run" << std::endl;
         return new TA2Plot();
       }
    }

    //Add correct dumps to each TA2Plot
    if (isCState)
      powerOffPlot->AddDumpGates(runNumber, {"c MW Power Off"}, {-1}); //Just add all these to one TA2Plot
    else
      powerOffPlot->AddDumpGates(runNumber, {"MW Power Off"}, {-1}); //Just add all these to one TA2Plot
    for(int i=0; i<currentConfig.fNumberOfFreqs; i++)
    {

        //Off res has a different name so do this, it will be plots[0]
        std::string name;
        if(i==0)
          if (isCState)
            name = "c MW OffResonance Dump";
          else
            name = "MW OffResonance Dump";
        else
          if (isCState)
            name = std::string("c MW Frq ") + std::to_string(i) + " Dump";
          else
            name = std::string("MW Frq ") + std::to_string(i) + " Dump";

        //Get the spills:
        std::cout << "Getting spills: " << name << "\n";
        std::vector<TA2Spill> spills = Get_A2_Spills(runNumber,{name},{-1});
        std::cout << "got spills\n";
        
        
        std::cout << "spills \n";
        std::cout << "============================================\n";
        std::cout << "spills = [";

        //Add spills to the TA2Plot ONLY if its less than the number of tests. Note the variable 'numberOfTestDumps' is the index of the first dump you want
        for(int j=0; j<spills.size(); j++)
        {
            if(j>=currentConfig.fFirstLegitDump && (currentConfig.fLastLegitDump<0 || j<=currentConfig.fLastLegitDump)) //If fLastLegitDump < 0, it takes them all, otherwise stop at lld.
            {
                totalPlot->AddDumpGates(runNumber, {spills.at(j)});
                plots[i]->AddDumpGates(runNumber, {spills.at(j)});
            }
        }
        std::cout << "]\n";
        std::cout << "============================================\n";
        //Maybe you want to do some other manipulation with the spills?
        //for(auto spill: spills)
        //    totalPlot->AddTimeGate(runNumber, spill.GetStartTime()-1, spill.GetStopTime());

        //Done.
        std::cout << "added spills\n";
    }

    //Use TA2Plot_Filler to load the data into the array of plots
    TA2Plot_Filler filler;
    for(int i=0; i<currentConfig.fNumberOfFreqs; i++)
    {
        filler.BookPlot(plots[i]);
    }
    filler.BookPlot(totalPlot);
    filler.BookPlot(powerOffPlot);
    filler.LoadData();
    //Data is now loaded.



    //Begin analysing the counts and bg subbing them.
    std::vector<edouble> rawCounts;
    std::vector<double> rawCountsdouble;
    std::vector<double> rawCountserror;
    std::vector<edouble> times;
    std::vector<edouble> bgsubbedCounts;
    std::vector<double> bgsubbedCountsdouble;
    std::vector<double> bgsubbedCountserror;

    //The functions are a little slow, so we hardcode these numbers.
    //edouble PCEff = GetPCEff(); //This is the function that generated the number below, It's from alpha2/macros/GetPCErrors.C.
    edouble PCEff(0.677548,0.0013503); 
    //edouble PCCR = GetPCFalseAcc(); //This is the function that generated the number below, It's from alpha2/macros/GetPCErrors.C.
    edouble PCCR(0.0513146,0.0010429); 

    edouble rawCountsSum(0,0);
    edouble bgSubbedCountsSum(0,0);

    for(int i=0; i<currentConfig.fNumberOfFreqs; i++)
    {
        //Push back the edoubles
        rawCounts.push_back((edouble)plots[i]->GetNPassedType(1)); 
        std::cout << plots[i]->GetTotalTime() << "\n";
        edouble time(plots[i]->GetTotalTime(),0);
        times.push_back(time);
        bgsubbedCounts.push_back( (rawCounts.at(i) - PCCR*times.at(i))/PCEff );

        //Push back the doubles, this makes the graph easier
        rawCountsdouble.push_back(rawCounts.at(i).value);
        rawCountserror.push_back(rawCounts.at(i).error);
        bgsubbedCountsdouble.push_back(bgsubbedCounts.at(i).value);
        bgsubbedCountserror.push_back(bgsubbedCounts.at(i).error);

        //For the totals...
        rawCountsSum=rawCountsSum+rawCounts.at(i);
        bgSubbedCountsSum=bgSubbedCountsSum+bgsubbedCounts.at(i);
    }
    std::cout << "============================================\n";
    std::cout << "Macro complete.... printing results:\n";
    std::cout << "============================================\n\n";


    //Print to table
    std::cout << "============================================\n";
    std::cout << "Results table\n";
    std::cout << "============================================\n";
    std::cout << "index,";
    for(auto freq: currentConfig.fFreqOffset) {std::cout << std::to_string(freq) << ",";}
    std::cout << "\n";
    std::cout << "time,";
    for(auto time: times) {std::cout << time  << ",";}
    std::cout << "\n";
    std::cout << "Counts,";
    for(auto count: rawCounts) {std::cout << count  << ",";}
    std::cout << "\n";
    std::cout << "bgSubbed,";
    for(auto count: bgsubbedCounts) {std::cout << count  << ",";}
    std::cout << "\n";
    std::cout << "rawCountsSum = " << rawCountsSum << "\n";
    std::cout << "bgSubbedCountsSum = " << bgSubbedCountsSum << "\n";
    std::cout << "MW Power off = " << (edouble)powerOffPlot->GetNPassedType(1) << "\n";
    std::cout << "============================================\n";


    //Print in python friendly format
    std::cout << "\n============================================\n";
    std::cout << "For Python \n";
    std::cout << "============================================\n";
    std::cout << "freqs = [";
    for(auto freq: currentConfig.fFreqOffset) {std::cout << freq  << ",";}
    std::cout << "]\n";
    std::cout << "rawCounts = [";
    for(auto count: rawCounts) {std::cout << count.value  << ",";}
    std::cout << "]\n";
    std::cout << "bgSubbed = [";
    for(auto count: bgsubbedCounts) {std::cout << count.value  << ",";}
    std::cout << "]\n";
    std::cout << "rawCountserr = [";
    for(auto count: rawCounts) {std::cout << count.error  << ",";}
    std::cout << "]\n";
    std::cout << "bgSubbederr = [";
    for(auto count: bgsubbedCounts) {std::cout << count.error  << ",";}
    std::cout << "]\n";
    std::cout << "plot_lamb(freqs, rawCounts, bgSubbed, rawCountserr, bgSubbederr)\n";
    std::cout << "============================================\n";

    //print in excel friendly format
    std::cout << "\n============================================\n";
    std::cout << "For Excel \n";
    std::cout << "============================================\n";
    std::cout << "index,";
    for(auto freq: currentConfig.fFreqOffset) {std::cout << freq  << ",";}
    std::cout << "\n";
    std::cout << "rawCounts,";
    for(auto count: rawCounts) {std::cout << count.value  << ",";}
    std::cout << "\n";
    std::cout << "bgSubbed = ";
    for(auto count: bgsubbedCounts) {std::cout << count.value  << ",";}
    std::cout << "\n";
    std::cout << "rawCountserr = ";
    for(auto count: rawCounts) {std::cout << count.error  << ",";}
    std::cout << "\n";
    std::cout << "bgSubbederr = ";
    for(auto count: bgsubbedCounts) {std::cout << count.error  << ",";}
    std::cout << "\n";
    std::cout << "============================================\n";

    //Setup canvas and plot the lineshape.
    TString canvasTitle="R";
    canvasTitle+=runNumber;
    gStyle->SetGridStyle(1);
    TCanvas* c=new TCanvas(canvasTitle);
    c->Divide(1, 2);
    c->cd(1);
    TGraphErrors* rawCountsGraph = new TGraphErrors(currentConfig.fNumberOfFreqs, currentConfig.fFreqOffset.data(),rawCountsdouble.data(), currentConfig.fFreqOffseterrors.data(), rawCountserror.data());
    rawCountsGraph->GetYaxis()->SetRange(0, rawCountsGraph->GetYaxis()->GetXmax());
    rawCountsGraph->SetTitle(canvasTitle+" RawCounts");
    rawCountsGraph->Draw("AP*");
    c->cd(2);
    TGraphErrors* bgSubbedCountsGraph=new TGraphErrors(currentConfig.fNumberOfFreqs,currentConfig.fFreqOffset.data(),bgsubbedCountsdouble.data(), currentConfig.fFreqOffseterrors.data(), bgsubbedCountserror.data());
    bgSubbedCountsGraph->GetYaxis()->SetRange(0, bgSubbedCountsGraph->GetYaxis()->GetXmax());
    bgSubbedCountsGraph->SetTitle(canvasTitle+" Efficiency Corrected and BG subbed");
    bgSubbedCountsGraph->Draw("AP*");

    //Save the canvas under 'title'
    TString title="R";
    title+=runNumber;
    if (isCState)
       title += "_C";
    else
       title += "_D";
    title+="_LambShiftSpectrum";
    gPad->RedrawAxis("g");
    c->Update();
    gPad->RedrawAxis("g");
    c->Draw();
    gPad->RedrawAxis("g");
    c->SaveAs(title + ".png");

    //return rawCountsGraph;
    //Still just returns empty boi, until you find something smarter to return this is fine.
    return new TA2Plot();    
}

void GetLambShift68459()
{
    int runNumber = 68459;
    std::vector<TA2Spill> OffPower = Get_A2_Spills(runNumber, {"MW Power Off"}, {-1});
    std::vector<TA2Spill> OffResonance = Get_A2_Spills(runNumber, {"MW OffResonance Dump"}, {-1});
    std::vector<TA2Spill> MW2PF = Get_A2_Spills(runNumber, {"MW 2PF Dump"}, {-1});
    std::vector<TA2Spill> MW2PA = Get_A2_Spills(runNumber, {"MW 2PA Dump"}, {-1});

    TA2Plot OffPower_OnLaser;
    TA2Plot OffResonance_OnLaser;
    TA2Plot MW2PF_OnLaser;
    TA2Plot MW2PA_OnLaser;
    TA2Plot OffPower_OffLaser;
    TA2Plot OffResonance_OffLaser;
    TA2Plot MW2PF_OffLaser;
    TA2Plot MW2PA_OffLaser;

    for(int i=0; i<OffPower.size(); i++)
    {
        //std::cout << "i = " << i << "\n";
        if(i%2==0)//==0
        {
            //std::cout << "Adding to ON \n";
            OffPower_OnLaser.AddDumpGates(runNumber, {OffPower.at(i)});
        }
        if(i%2==1)//==1
        {
            //std::cout << "Adding to OFF \n";
            OffPower_OffLaser.AddDumpGates(runNumber, {OffPower.at(i)});
        }
    }
    for(int i=0; i<OffResonance.size(); i++)
    {
        if(i%2==0)//==0
        {
            OffResonance_OnLaser.AddDumpGates(runNumber, {OffResonance.at(i)});
        }
        if(i%2==1)//==1
        {
            OffResonance_OffLaser.AddDumpGates(runNumber, {OffResonance.at(i)});
        }
    }
    for(int i=0; i<MW2PF.size(); i++)
    {
        if(i%2==0)//==0
        {
            MW2PF_OnLaser.AddDumpGates(runNumber, {MW2PF.at(i)});
        }
        if(i%2==1)//==1
        {
            MW2PF_OffLaser.AddDumpGates(runNumber, {MW2PF.at(i)});
        }
    }
    for(int i=0; i<MW2PA.size(); i++)
    {
        if(i%2==0)//==0
        {
            MW2PA_OnLaser.AddDumpGates(runNumber, {MW2PA.at(i)});
        }
        if(i%2==1)//==1
        {
            MW2PA_OffLaser.AddDumpGates(runNumber, {MW2PA.at(i)});
        }
    }
    TA2Plot total;
    total.AddDumpGates(runNumber, OffPower);
    total.AddDumpGates(runNumber, OffResonance);
    total.AddDumpGates(runNumber, MW2PF);
    total.AddDumpGates(runNumber, MW2PA);

    TA2Plot_Filler filler;

    filler.BookPlot(&OffPower_OnLaser);
    filler.BookPlot(&OffResonance_OnLaser);
    filler.BookPlot(&MW2PF_OnLaser);
    filler.BookPlot(&MW2PA_OnLaser);
    filler.BookPlot(&OffPower_OffLaser);
    filler.BookPlot(&OffResonance_OffLaser);
    filler.BookPlot(&MW2PF_OffLaser);
    filler.BookPlot(&MW2PA_OffLaser);
    filler.BookPlot(&total);

    filler.LoadData();

    int sum = 0;
    sum+=OffPower_OnLaser.GetNPassedType(1);
    sum+=OffResonance_OnLaser.GetNPassedType(1);
    sum+=MW2PF_OnLaser.GetNPassedType(1);
    sum+=MW2PA_OnLaser.GetNPassedType(1);
    sum+=OffPower_OffLaser.GetNPassedType(1);
    sum+=OffResonance_OffLaser.GetNPassedType(1);
    sum+=MW2PF_OffLaser.GetNPassedType(1);
    sum+=MW2PA_OffLaser.GetNPassedType(1);

    std::cout << "OffPower_OnLaser = " << (edouble)OffPower_OnLaser.GetNPassedType(1) << "\n";
    std::cout << "OffResonance_OnLaser = " << (edouble)OffResonance_OnLaser.GetNPassedType(1) << "\n";
    std::cout << "MW2PF_OnLaser = " << (edouble)MW2PF_OnLaser.GetNPassedType(1) << "\n";
    std::cout << "MW2PA_OnLaser = " << (edouble)MW2PA_OnLaser.GetNPassedType(1) << "\n";
    std::cout << "OffPower_OffLaser = " << (edouble)OffPower_OffLaser.GetNPassedType(1) << "\n";
    std::cout << "OffResonance_OffLaser = " << (edouble)OffResonance_OffLaser.GetNPassedType(1) << "\n";
    std::cout << "MW2PF_OffLaser = " << (edouble)MW2PF_OffLaser.GetNPassedType(1) << "\n";
    std::cout << "MW2PA_OffLaser = " << (edouble)MW2PA_OffLaser.GetNPassedType(1) << "\n";
    std::cout << "TOTAL = " << total.GetNPassedType(1) << "\n"; 
    std::cout << "TOTALFromMe = " << sum << "\n"; 
    
    edouble PCEff(0.68,0.05);
    edouble PCCR(0.045,0);
    edouble hundred(100,0);
    edouble OffPower_OnLaser_counts = ((edouble)OffPower_OnLaser.GetNPassedType(1));
    edouble OffResonance_OnLaser_counts = ((edouble)OffResonance_OnLaser.GetNPassedType(1));
    edouble MW2PF_OnLaser_counts = ((edouble)MW2PF_OnLaser.GetNPassedType(1));
    edouble MW2PA_OnLaser_counts = ((edouble)MW2PA_OnLaser.GetNPassedType(1));
    edouble OffPower_OffLaser_counts = ((edouble)OffPower_OffLaser.GetNPassedType(1));
    edouble OffResonance_OffLaser_counts = ((edouble)OffResonance_OffLaser.GetNPassedType(1));
    edouble MW2PF_OffLaser_counts = ((edouble)MW2PF_OffLaser.GetNPassedType(1));
    edouble MW2PA_OffLaser_counts = ((edouble)MW2PA_OffLaser.GetNPassedType(1));

    edouble OffPower_OnLaser_time = edouble(OffPower_OnLaser.GetTotalTime(), 0);
    edouble OffResonance_OnLaser_time = edouble(OffResonance_OnLaser.GetTotalTime(), 0);
    edouble MW2PF_OnLaser_time = edouble(MW2PF_OnLaser.GetTotalTime(), 0);
    edouble MW2PA_OnLaser_time = edouble(MW2PA_OnLaser.GetTotalTime(), 0);
    edouble OffPower_OffLaser_time = edouble(OffPower_OffLaser.GetTotalTime(), 0);
    edouble OffResonance_OffLaser_time = edouble(OffResonance_OffLaser.GetTotalTime(), 0);
    edouble MW2PF_OffLaser_time = edouble(MW2PF_OffLaser.GetTotalTime(), 0);
    edouble MW2PA_OffLaser_time = edouble(MW2PA_OffLaser.GetTotalTime(), 0);


    std::cout << "OffPower_OnLaser total time = " << OffPower_OnLaser_time << "\n";
    std::cout << "OffResonance_OnLaser total time = " << OffResonance_OnLaser_time << "\n";
    std::cout << "MW2PF_OnLaser total time = " << MW2PF_OnLaser_time << "\n";
    std::cout << "MW2PA_OnLaser total time = " << MW2PA_OnLaser_time << "\n";
    std::cout << "OffPower_OffLaser total time = " << OffPower_OffLaser_time << "\n";
    std::cout << "OffResonance_OffLaser total time = " << OffResonance_OffLaser_time << "\n";
    std::cout << "MW2PF_OffLaser total time = " << MW2PF_OffLaser_time << "\n";
    std::cout << "MW2PA_OffLaser total time = " << MW2PA_OffLaser_time << "\n";

    edouble OffPower_OnLaser_expectedHbar = (OffPower_OnLaser_counts - (PCCR * OffPower_OnLaser_time)) /PCEff;
    edouble OffResonance_OnLaser_expectedHbar = (OffResonance_OnLaser_counts - (PCCR * OffResonance_OnLaser_time)) /PCEff;
    edouble MW2PF_OnLaser_expectedHbar = (MW2PF_OnLaser_counts - (PCCR * MW2PF_OnLaser_time)) /PCEff;
    edouble MW2PA_OnLaser_expectedHbar = (MW2PA_OnLaser_counts - (PCCR * MW2PA_OnLaser_time)) /PCEff;
    edouble OffPower_OffLaser_expectedHbar = (OffPower_OffLaser_counts - (PCCR * OffPower_OffLaser_time)) /PCEff;
    edouble OffResonance_OffLaser_expectedHbar = (OffResonance_OffLaser_counts - (PCCR * OffResonance_OffLaser_time)) /PCEff;
    edouble MW2PF_OffLaser_expectedHbar = (MW2PF_OffLaser_counts - (PCCR * MW2PF_OffLaser_time)) /PCEff;
    edouble MW2PA_OffLaser_expectedHbar = (MW2PA_OffLaser_counts - (PCCR * MW2PA_OffLaser_time)) /PCEff;

    edouble sume(0);
    sume=sume+OffPower_OnLaser_expectedHbar;
    sume=sume+OffResonance_OnLaser_expectedHbar;
    sume=sume+MW2PF_OnLaser_expectedHbar;
    sume=sume+MW2PA_OnLaser_expectedHbar;
    sume=sume+OffPower_OffLaser_expectedHbar;
    sume=sume+OffResonance_OffLaser_expectedHbar;
    sume=sume+MW2PF_OffLaser_expectedHbar;
    sume=sume+MW2PA_OffLaser_expectedHbar;
    std::cout << "BG REJECTION \n\n";
    std::cout << "OffPower_OnLaser = " <<OffPower_OnLaser_expectedHbar << "\n";
    std::cout << "OffResonance_OnLaser = " <<OffResonance_OnLaser_expectedHbar << "\n";
    std::cout << "MW2PF_OnLaser = " <<MW2PF_OnLaser_expectedHbar << "\n";
    std::cout << "MW2PA_OnLaser = " <<MW2PA_OnLaser_expectedHbar << "\n";
    std::cout << "OffPower_OffLaser = " <<OffPower_OffLaser_expectedHbar << "\n";
    std::cout << "OffResonance_OffLaser = " <<OffResonance_OffLaser_expectedHbar << "\n";
    std::cout << "MW2PF_OffLaser = " <<MW2PF_OffLaser_expectedHbar << "\n";
    std::cout << "MW2PA_OffLaser = " <<MW2PA_OffLaser_expectedHbar << "\n"; 
    std::cout << "TOTAL = " << sume << "\n"; 


    std::cout << "\n============================================\n";
    std::cout << "For Python \n";
    std::cout << "============================================\n";
    std::cout << "freqs = [-700, -680, -480, 0]\n";
    std::cout << "onLaserRaw = [";
    for(auto count: {OffPower_OnLaser_counts, OffResonance_OnLaser_counts, MW2PA_OnLaser_counts, MW2PF_OnLaser_counts}) {std::cout << count.value  << ",";}
    std::cout << "]\n";
    std::cout << "onLaserRawerr = [";
    for(auto count: {OffPower_OnLaser_counts, OffResonance_OnLaser_counts, MW2PA_OnLaser_counts, MW2PF_OnLaser_counts}) {std::cout << count.error  << ",";}
    std::cout << "]\n";
    std::cout << "offLaserRaw = [";
    for(auto count: {OffPower_OffLaser_counts, OffResonance_OffLaser_counts, MW2PA_OffLaser_counts, MW2PF_OffLaser_counts}) {std::cout << count.value  << ",";}
    std::cout << "]\n";
    std::cout << "offLaserRawerr = [";
    for(auto count: {OffPower_OffLaser_counts, OffResonance_OffLaser_counts, MW2PA_OffLaser_counts, MW2PF_OffLaser_counts}) {std::cout << count.error  << ",";}
    std::cout << "]\n";
    std::cout << "#BG subbed\n";
    std::cout << "bgsubonLaserRaw = [";
    for(auto count: {OffPower_OnLaser_expectedHbar, OffResonance_OnLaser_expectedHbar, MW2PA_OnLaser_expectedHbar, MW2PF_OnLaser_expectedHbar}) {std::cout << count.value  << ",";}
    std::cout << "]\n";
    std::cout << "bgsubonLaserRawerr = [";
    for(auto count: {OffPower_OnLaser_expectedHbar, OffResonance_OnLaser_expectedHbar, MW2PA_OnLaser_expectedHbar, MW2PF_OnLaser_expectedHbar}) {std::cout << count.error  << ",";}
    std::cout << "]\n";
    std::cout << "bgsuboffLaserRaw = [";
    for(auto count: {OffPower_OffLaser_expectedHbar, OffResonance_OffLaser_expectedHbar, MW2PA_OffLaser_expectedHbar, MW2PF_OffLaser_expectedHbar}) {std::cout << count.value  << ",";}
    std::cout << "]\n";
    std::cout << "bgsuboffLaserRawerr = [";
    for(auto count: {OffPower_OffLaser_expectedHbar, OffResonance_OffLaser_expectedHbar, MW2PA_OffLaser_expectedHbar, MW2PF_OffLaser_expectedHbar}) {std::cout << count.error  << ",";}
    std::cout << "]\n";
    std::cout << "plot_lamb(freqs, onLaserRaw, bgsubonLaserRaw, onLaserRawerr, bgsubonLaserRawerr)\n";
    std::cout << "plot_lamb(freqs, offLaserRaw, bgsuboffLaserRaw, offLaserRawerr, bgsuboffLaserRawerr)\n";
    std::cout << "============================================\n";
}

void GetLambShift68452()
{
    int runNumber = 68452;
    std::vector<TA2Spill> OffResonance_spills = Get_A2_Spills(runNumber, {"MW OffResonance Dump"}, {-1});
    std::vector<TA2Spill> MW2PF_spills = Get_A2_Spills(runNumber, {"MW 2PF Dump"}, {-1});
    std::vector<TA2Spill> MW2PA_spills = Get_A2_Spills(runNumber, {"MW 2PA Dump"}, {-1});

    TA2Plot OffResonance_plot;
    TA2Plot MW2PF_plot;
    TA2Plot MW2PA_plot;

    OffResonance_plot.AddDumpGates(runNumber, OffResonance_spills);
    MW2PF_plot.AddDumpGates(runNumber, MW2PF_spills);
    MW2PA_plot.AddDumpGates(runNumber, MW2PA_spills);

    TA2Plot total;
    total.AddDumpGates(runNumber, OffResonance_spills);
    total.AddDumpGates(runNumber, MW2PF_spills);
    total.AddDumpGates(runNumber, MW2PA_spills);

    TA2Plot_Filler filler;

    filler.BookPlot(&OffResonance_plot);
    filler.BookPlot(&MW2PF_plot);
    filler.BookPlot(&MW2PA_plot);
    filler.BookPlot(&total);
    filler.LoadData();

    int sum = 0;
    sum+=OffResonance_plot.GetNPassedType(1);
    sum+=MW2PF_plot.GetNPassedType(1);
    sum+=MW2PA_plot.GetNPassedType(1);

    std::cout << "OffResonance_plot = " << (edouble)OffResonance_plot.GetNPassedType(1) << "\n";
    std::cout << "MW2PF_plot = " << (edouble)MW2PF_plot.GetNPassedType(1) << "\n";
    std::cout << "MW2PA_plot = " << (edouble)MW2PA_plot.GetNPassedType(1) << "\n";
    std::cout << "TOTAL = " << total.GetNPassedType(1) << "\n"; 
    std::cout << "TOTALFromMe = " << sum << "\n"; 
    
    edouble PCEff(0.68,0.05);
    edouble PCCR(0.045,0);
    edouble hundred(100,0);
    edouble OffResonance_plot_counts = ((edouble)OffResonance_plot.GetNPassedType(1));
    edouble MW2PF_plot_counts = ((edouble)MW2PF_plot.GetNPassedType(1));
    edouble MW2PA_plot_counts = ((edouble)MW2PA_plot.GetNPassedType(1));

    edouble OffResonance_plot_time = edouble(OffResonance_plot.GetTotalTime(), 0);
    edouble MW2PF_plot_time = edouble(MW2PF_plot.GetTotalTime(), 0);
    edouble MW2PA_plot_time = edouble(MW2PA_plot.GetTotalTime(), 0);


    std::cout << "OffResonance_plot total time = " << OffResonance_plot_time << "\n";
    std::cout << "MW2PF_plot total time = " << MW2PF_plot_time << "\n";
    std::cout << "MW2PA_plot total time = " << MW2PA_plot_time << "\n";


    edouble OffResonance_plot_expectedHbar = (OffResonance_plot_counts - (PCCR * OffResonance_plot_time)) /PCEff;
    edouble MW2PF_plot_expectedHbar = (MW2PF_plot_counts - (PCCR * MW2PF_plot_time)) /PCEff;
    edouble MW2PA_plot_expectedHbar = (MW2PA_plot_counts - (PCCR * MW2PA_plot_time)) /PCEff;
    
    edouble sume(0);
    sume=sume+OffResonance_plot_expectedHbar;
    sume=sume+MW2PF_plot_expectedHbar;
    sume=sume+MW2PA_plot_expectedHbar;

    std::cout << "BG REJECTION \n\n";
    std::cout << "OffResonance_expectedHbar = " <<OffResonance_plot_expectedHbar << "\n";
    std::cout << "MW2PF_expectedHbar = " <<MW2PF_plot_expectedHbar << "\n";
    std::cout << "MW2PA_expectedHbar = " <<MW2PA_plot_expectedHbar << "\n";
    std::cout << "TOTAL = " << sume << "\n"; 

    std::cout << "\n============================================\n";
    std::cout << "For Python \n";
    std::cout << "============================================\n";
    std::cout << "freqs = [-680, -480, 0]\n";
    std::cout << "onLaserRaw = [";
    for(auto count: {OffResonance_plot_counts, MW2PA_plot_counts, MW2PF_plot_counts}) {std::cout << count.value  << ",";}
    std::cout << "]\n";
    std::cout << "onLaserRawerr = [";
    for(auto count: {OffResonance_plot_counts, MW2PA_plot_counts, MW2PF_plot_counts}) {std::cout << count.error  << ",";}
    std::cout << "]\n";
    std::cout << "#BG subbed\n";
    std::cout << "bgsubonLaserRaw = [";
    for(auto count: {OffResonance_plot_expectedHbar, MW2PA_plot_expectedHbar, MW2PF_plot_expectedHbar}) {std::cout << count.value  << ",";}
    std::cout << "]\n";
    std::cout << "bgsubonLaserRawerr = [";
    for(auto count: {OffResonance_plot_expectedHbar, MW2PA_plot_expectedHbar, MW2PF_plot_expectedHbar}) {std::cout << count.error  << ",";}
    std::cout << "]\n";
    std::cout << "plot_lamb(freqs, onLaserRaw, bgsubonLaserRaw, onLaserRawerr, bgsubonLaserRawerr)\n";
    std::cout << "============================================\n";
    
}

void GetPython()
{
    //This has got to be one of the dumbest things I've ever thought of :)
    //If you want the python to print the lineshape, call this...
    const char essay[] = "import pandas as pd\n" 
    "import matplotlib.pyplot as plt"    "import numpy as np\n" 
    "from scipy.optimize import curve_fit\n" 
    "#To make plots looks like latex\n" 
    "plt.rcParams.update({\n" 
    "    \"pgf.texsystem\": \"pdflatex\",\n" 
    "    \"text.usetex\": True,\n" 
    "    \'font.size\' : 30,\n" 
    "})\n" 
    "plt.rcParams[\"font.family\"] = \"Times New Roman\"\n" 
    "plt.rcParams[\'axes.facecolor\']=\'white\'\n" 
    "def plot_lamb(freqs, raw, bg, rawerr, bgerr):\n" 
    "fig = plt.figure(figsize=(15, 10), dpi=80)\n" 
    "plt.suptitle(\"Passed Cuts\")\n" 
    "fig.patch.set_facecolor('lightgrey')\n" 
    "eb = plt.errorbar(freqs, raw, yerr=rawerr, linestyle='', marker='o', color='b', capsize=6)\n" 
    "plt.ylabel('Raw counts')\n" 
    "plt.xlabel('Detuning from 2PF (MHz)')\n" 
    "plt.grid()\n" 
    "plt.ylim(ymin=0)\n" 
    "print(\"\\n\")\n" 
    "fig = plt.figure(figsize=(15, 10), dpi=80)\n" 
    "plt.suptitle(\"Background subtracted and efficiency corrected\")\n" 
    "fig.patch.set_facecolor('lightgrey')\n" 
    "eb = plt.errorbar(freqs, bg, yerr=bgerr, linestyle='', marker='o', color='b', capsize=6)\n" 
    "plt.ylabel('Atoms')\n" 
    "plt.xlabel('Detuning from 2PF (MHz)')\n" 
    "plt.grid()\n" 
    "plt.ylim(ymin=0)\n" 
    "print(\" \\n \")\n" 
    "";

    std::cout << essay;
}
