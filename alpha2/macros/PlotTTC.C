// ====================================================================
//
// macro for the analysis of TA trigger events
//
// ====================================================================


// 1 FRC = 16 trigger lines, i.e., 4 si modules x 4 asics.
// 1 TTC = 256 lines, i.e., 16 FRCs.

const int nttc = 2;
const int nta = nttc*256;


TTree* XGet_TTC_Tree( Int_t run_number )
{
  //load file
  TFile* root_file = Get_File( run_number );

  TTree* ttc_tree = NULL;
  ttc_tree = (TTree*) root_file->Get("TTCEvents");
  if( ttc_tree==NULL ){
    Error("Get_TTC_Tree","\033[31mTTC Tree for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    ttc_tree ->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }
  return ttc_tree;
}


TH1D* Plot_TA_Events( Int_t runNumber, Double_t start_time=0., Double_t end_time=99999. ) {
  TH1D* TA_hist = new TH1D("TAs","TAs",nta, 0, nta );

  TTree* ttctree = XGet_TTC_Tree( runNumber );
  TTCEvent* ttcEvent = new TTCEvent(); 
  ttctree->SetBranchAddress("TTCEvent", &ttcEvent );
  
  Int_t TAs[16];

  for( Int_t i=0; i<ttctree->GetEntries(); i++ ){
    ttctree->GetEntry(i);

    std::cout << "TTCEvent time = " << ttcEvent->GetRunTime() << "\n";
    
    if( ttcEvent->GetRunTime() < start_time ) continue;
    if( ttcEvent->GetRunTime() > end_time ) continue;

    int ifpga = ttcEvent->GetFPGA();
    
    // loop over banks
    for( Int_t BankNumber=0; BankNumber <8; BankNumber++ ) {
        uint16_t latch = ttcEvent->GetLatch( BankNumber );

	uint16_t imask = 0x9999;

	if (ifpga==0) {
	  if (BankNumber==2)
	    imask = 0xFF99;
	  if (BankNumber==6)
	    imask = 0xFF99;
	}

	if (ifpga==2) {
	  if (BankNumber==3)
	    imask = 0xFF99;
	  if (BankNumber==7)
	    imask = 0xFF99;
	}

	latch &= ~imask;
        
        uint16_t compare = 1;
        int counter = 0 ;
        do{
          if ( latch & compare  ) {
            TAs[counter] = 1  ;
          }
          else     TAs[counter] = 0 ;
          
          compare = compare << 1 ;
          
          //printf("Latch = %04x, Counter = %d, TA = %d \n", latch, counter, TAs[counter] );
                
          counter++ ;
        }while(counter < 16 ) ;
        
            
        Int_t TAnumber;
        for( Int_t TA=0; TA<16; TA++ ){
          TAnumber = TA + 16*BankNumber + 128*ifpga; //ttcEvent->GetFPGA();
          if( TAs[TA]==1 ) TA_hist->Fill( TA_hist->GetBinCenter(TAnumber+1) );               
        }
        
      }//loop over banks
    
  }//loop over TTC tree entries
    
  return TA_hist;
}

Bool_t TA_Mask_Run2010( Int_t TAnumber )
{
  Bool_t doMask = kFALSE;
  
  // TA banks that are not connected :
  // FPGA 0 bank 7 - TA inputs 0-15        == TAnumbers 112-127
  // FPGA 1 bank 7 - TA inputs 12,13,14,15 == TAnumbers 252,253,254,255 

  if( TAnumber >=112 && TAnumber <=127 ) doMask = kTRUE;
  if( TAnumber >=252 && TAnumber <=255 ) doMask = kTRUE;
  
  return doMask;
}

Double_t Get_ASIC_Number( Int_t TAnumber )
{
  // ASIC number defined as;
  //  ASIC for p-side 1 = Module + 0.
  //  ASIC for p-side 2 = Module + 0.5

  Double_t ASIC_number(0);


  return ASIC_number;
}


TGraph* Plot_SiMod_TA_Events( Int_t runNumber, Double_t start_time=0., Double_t end_time=99999. ) {
  TH1D* TA_hist = Plot_TA_Events( runNumber, start_time, end_time );
  
  // VA number 
  // TA number % 4 = 1 -> VA #1  (n-side)
  // TA number % 4 = 2 -> VA #2  (p-side)
  // TA number % 4 = 3 -> VA #3  (p-side)
  // TA number % 4 = 0 -> VA #4  (n-side)

  Double_t SImod[nta];
  Double_t TAcounts[nta];
  
  Int_t index(0);
 
  // check this 
  for( Int_t TAnumber=0; TAnumber<nta; TAnumber++ ){
    
    Double_t content = (Double_t) TA_hist->GetBinContent(TAnumber + 1);
    //    if( content <= 0 ) continue;

    // determine Si module number
    TVF48SiMap vfmap("./a2lib/maps/vf48_map.00015");

    Double_t simod = (Double_t) vfmap.GetSil( TAnumber );

    if (simod<0) continue;

        //cout << TAnumber << " " << simod << " " << content << endl;

    
    simod+=0.15*(TAnumber%4);
    
    SImod[index]=simod;
    TAcounts[index]=content;
    index++;

  }

  TGraph* TAcounts_gr = new TGraph( index, SImod, TAcounts );
  TAcounts_gr->SetTitle("TA trigger counts");
  TAcounts_gr->GetXaxis()->SetTitle("Si module #");
  TAcounts_gr->SetMinimum(0.);
  TAcounts_gr->SetMarkerStyle(2);
  TAcounts_gr->SetMarkerColor(4);
  
  return TAcounts_gr;
}



