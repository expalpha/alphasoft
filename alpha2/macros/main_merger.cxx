//Std Lib
#include <vector>
//Root stuff
#include "TChain.h"
#include "TTree.h"
#include "TBranch.h"
#include "TFile.h"
//Our stuff
#include "TA2Plot.h"
#include "TA2Plot_Filler.h"

int main(int argc, char* argv[])
{
    std::vector<int> runNumbers;
    std::string runlist;
    std::string location = ".";
    std::vector<std::string> args;
    std::string name;
    enum ERunType
    {
        kMixing,
        kCosmic,
        kInvalidOption
    };
    ERunType runType = kInvalidOption;

    for (int i=0; i<argc; i++) 
    {
        args.push_back(argv[i]);
    }

    for (unsigned int i=1; i<args.size(); i++) // loop over the commandline options
    { 
        if(args[i] == "--mixing")
        {
            runType = kMixing;
            name = "mixing";
        }
        if(args[i] == "--cosmic")
        {
            runType = kCosmic;
            name = "cosmic";
        }
        if(args[i] == "--listfile")
            runlist = args[i+1];
        if(args[i] == "--location")
            location = args[i+1]; 
    }

    std::cout << "Location chosen is " << location << "\n";

    std::ifstream myFile(runlist);
    std::string line;
    if (myFile.is_open())
    {
        while ( std::getline(myFile, line) )
        {
            int runNumber;
            std::cout << line << std::endl;
            std::sscanf(line.c_str(), "%d", &runNumber);
            runNumbers.push_back(runNumber);
        }
        myFile.close();
    }

    //Create files and trees here
    TChain* chain = nullptr;
    switch(runType)
    {
        case kMixing:
            chain = new TChain("mixing");
            break;
        case kCosmic:
            chain = new TChain("cosmic");
            break;
        case kInvalidOption:
            std::cout << "Invalid option chosen" << std::endl;
            exit(1);
            break;
    }

    for(size_t i = 0; i<runNumbers.size(); i++)
    {
        std::string filename = location;
        filename += "/dumperoutput";
        filename += name;
        filename += std::to_string(runNumbers[i]);
        filename += ".root";
        std::cout << filename << "\n";
        if(gSystem->AccessPathName(filename.c_str()))
        {
            std::cout << "Dumper file: " << filename << " does not exist.\n";
        }
        else
        {
            chain->Add(filename.c_str());
        }
    }

    const auto nEntries = chain->GetEntries();

    TFile *outFile = nullptr;
    TTree *trainTree, *validTree, *testTree;
    trainTree = validTree = testTree = nullptr;
    switch(runType)
    {
        case(kMixing):
            outFile = new TFile("mvaSignal.root", "RECREATE");
            trainTree = chain->CloneTree(0);
            trainTree->SetName("trainSignal");
            validTree = chain->CloneTree(0);
            validTree->SetName("validationSignal");
            testTree = chain->CloneTree(0);
            testTree->SetName("testSignal");
            break;
        case(kCosmic):
            outFile = new TFile("mvaBackground.root", "RECREATE");
            trainTree = chain->CloneTree(0);
            trainTree->SetName("trainBackground");
            validTree = chain->CloneTree(0);
            validTree->SetName("validationBackground");
            testTree = chain->CloneTree(0);
            testTree->SetName("testBackground");
            break;
        case(kInvalidOption):
            outFile = nullptr;
            trainTree = validTree = testTree = nullptr;
            break;
    }

    if( !trainTree || !validTree || !testTree || !outFile )
    {
        std::cerr << "Merger: Null pointers to TTrees or to TFile because of invalid option" << std::endl;
        return EXIT_FAILURE;
    }

    for (int i = 0; i < nEntries; i++) 
    {
        chain->GetEntry(i);

        if(i%10000 == 0)
            printf("Merging event %i/%lli \n", i, nEntries);
            
        if (i%3 == 0)
            trainTree->Fill();
        if (i%3 == 1)
            validTree->Fill();
        if (i%3 == 2)
            testTree->Fill();

    }

    outFile->cd();
    outFile->Write();
    outFile->Close();

    return EXIT_SUCCESS;
}