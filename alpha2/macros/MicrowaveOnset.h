
// Set this to 0 when we have finalised the analysis! Never before!
#define DATA_BLINDED 1

std::map<int,double> magnet_reset_time = {
  /* { 1, 1673190840 }, */
  /* { 2, 1675872600 }, */
  /* { 3, 1678284060 }, */
  /* { 4, 1683543360 } */
  { 1, 1715809395 },
  { 2, 1716115895 },
  { 3, 1716492615 }
};

std::map<int,std::vector<int>> trian_run_map = {
  //{ 1,				
   /*    { */
   /*       67532, */
   /*       67533, */
   /*       67534, */
   /*       67535, */
   /*       67536, */
   /*       67537, */
   /*       67538, */
   /*       67539 */
   /*    } */
   /* }, */
   /* { 2, */
   /*    { */
   /*       67559, */
   /*       67560, */
   /*       67562, */
   /*       67563, */
   /*       67564, */
   /*       67566, */
   /*       67567, */
   /*       67568, */
   /*       67569, */
   /*       67570, */
   /*       67571, */
   /*       67572, */
   /*       67573, */
   /*       67574, */
   /*       67576, */
   /*       67577, */
   /*       67579, */
   /*       67580, */
   /*       67582, */
   /*       67583, */
   /*       67584 */
   /*    } */
   /* }, */
   /* { 3, */
   /*    { */
   /*       67590, */
   /*       67591, */
   /*       67594, */
   /*       67596, */
   /*       67597, */
   /*       67600, */
   /*       67601, */
   /*       67602, */
   /*       67603, */
   /*       67604, */
   /*       67605, */
   /*       67606, */
   /*       67607, */
   /*       67608, */
   /*       67609, */
   /*       67610, */
   /*       67611, */
   /*       67612 */
   /*    } */
   /* }, */
   /* {4, */
   /*    { */
   /*       67644, */
   /*       67645, */
   /*       67646, */
   /*       67647, */
   /*       67648, */
   /*       67649, */
   /*       67650, */
   /*       67651, */
   /*       67653, */
   /*       67654, */
   /*       67655, */
   /*       67656, */
   /*       67657, */
   /*       67658, */
   /*       67659, */
   /*       67660, */
   /*       67661, */
   /*       67662, */
   /*       67663, */
   /*       67664, */
   /*       67665 */
   /*    } */
   /* }, */
   /* {5, */
   /*    { */
   /*       69385, */
   /*       69386, */
   /*       69387, */
   /*       69388, */
   /*       69389, */
   /*       69390 */
   /*    } */
  {1, 
   {
     70329,
     70330,
     70331,
     70332,
     70333,
     70334,
     70335,
     70336
   }
  },
  {2,
   {
     70377,
     70378,
     70379,
     70380,
     70381,
     70382,
     70383,
     70384
   }
  },
  {3,
   {
     70462,
     70463,
     70464,
     70465,
     70467,
     70468,
     70469,
     70470
   }
   /* } */
  }
};
int GetStartTimeOfRun(const int runNumber)
{
   for (const auto& pair: trian_run_map)
   {
      for (const int& run: pair.second)
         if (run == runNumber)
            return magnet_reset_time[pair.first];
   }
   return -1;
}

std::map<int,std::vector<int>> GetSeriesLists()
{
   std::srand(16);
#if !DATA_BLINDED
   return trian_run_map;
#endif
   std::map<int,std::vector<int>> shorted_map(trian_run_map);
   int shortest_list = -1;
   size_t shortest_list_length = 9999;
   for (const auto& pair: trian_run_map)
   {
      if (pair.second.size() < shortest_list_length)
      {
         shortest_list = pair.first;
         shortest_list_length = pair.second.size();
      }
   }
   
   // Make all series the same number of runs (so we can't immediately tell which is which)
   for (auto& pair: shorted_map)
   {
      std::vector<int>& run_list = pair.second;
      while (run_list.size() > shortest_list_length)
      {
         int remove_element = rand() % run_list.size();
         std::cout << "Removing " << remove_element << "\n";
         // Remove random run
         run_list.erase( 
            run_list.begin() + remove_element
         );
      }
   }
   
   std::vector<int> new_order;
   while (new_order.size() < shorted_map.size())
   {
      const int random_number = rand() % shorted_map.size() + 1;
      if (std::count(new_order.begin(), new_order.end(),random_number))
         continue;
      new_order.emplace_back(random_number);
   }
   std::cout <<"Order:";
   // Convert shorted_map to shuffled_map
   std::map<int,std::vector<int>> shuffled_map;
   for (int i = 0; i < new_order.size(); i++)
   {
      shuffled_map.insert( { i + 1, shorted_map[new_order[i]] } );
      std::cout << new_order[i] << "\n";
   }
   return trian_run_map;//shuffled_map;
}

double GetRandomOffset()
{
   std::srand(42);
   const double approximate_splitting = 1.42;
   const double random_number = static_cast<double> ( rand()) / static_cast<double>(RAND_MAX);
   // Random offset at ~20% level of the splitting
   const double random_offset = approximate_splitting * 0.2 * random_number + 0.1;
   return random_offset;
}

