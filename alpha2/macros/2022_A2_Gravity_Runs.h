#include <map>
#include <vector>

// This file will be replaced by the one in ana/macros/2022_Gravity_Runs.h when we merge ALPHA2 and ALPHAg branches

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <random>


int myrandom (int i) { return std::rand()%i;}
std::map<double, std::vector<std::vector<int>>> ShuffleMap (std::map<double, std::vector<std::vector<int>>> mymap)
{
    // Set fixed seed for randomness
    srand(42);
    //map<int,string> m;
    std::vector<double> v;

    for(auto i: mymap)
    {
        //std::cout << i.first << ":" << i.second.at(0) << std::endl;
        v.push_back(i.first);
    }
    std::random_shuffle(v.begin(), v.end(), myrandom);
    std::vector<double>::iterator it=v.begin();
    std::cout << std::endl;
    for(auto& i:mymap)
    {
        std::vector<std::vector<int>> ts=i.second;
        i.second=mymap[*it];
        mymap[*it]=ts;
        it++;
    }
    for(auto i: mymap)
    {
        //std::cout << i.first << ":" << i.second.at(0) << std::endl;
    }
    return mymap;
}




std::map<double, std::vector<std::vector<int>>> GetA2Map(bool shuffle = false)
{

  std::map<double, std::vector<std::vector<int>>> mymap = {
                                                            {{1},{
                                                               {66001},
                                                               {66008},
                                                               {66045},
                                                               {66055},
                                                               {66089},
                                                               {66100},
                                                               {66253}
                                                            }},
                                                            {{-1},{
                                                               {66000},
                                                               {66011},
                                                               {66030},
                                                               {66046},
                                                               {66056},
                                                               {66091},
                                                               {66101},
                                                               {66250}
                                                            }},
                                                            {{0},{
                                                               {65999},
                                                               {66024},
                                                               {66031},
                                                               {66047},
                                                               {66087},
                                                               {66096},
                                                               {66104}
                                                            }},
                                                            {{2},{
                                                               {65998},
                                                               {66044},
                                                               {66092},
                                                               {66097},
                                                               {66102},
                                                               {66243},
                                                               {66257}
                                                            }},
                                                            {{-2},{
                                                               {65997},
                                                               {66032},
                                                               {66054},
                                                               {66094},
                                                               {66099},
                                                               {66103},
                                                               {66248}
                                                            }},
                                                            {{-10},{

                                                            }},
                                                            {{10},{

                                                            }},
                                                            {{-10.1},{
                                                               {66245},
                                                               {66249},
                                                               {66254},
                                                               {66260},
                                                               {66262},
                                                               {66265},
                                                               {66266}
                                                            }},
                                                            {{10.1},{
                                                               {66247},
                                                               {66251},
                                                               {66258},
                                                               {66261},
                                                               {66264},
                                                               {66267}
                                                            }},
                                                            {{3},{
                                                               {66208},
                                                               {66217},
                                                               {66228},
                                                               {66232},
                                                               {66236},
                                                               {66240},
                                                               {66259}
                                                            }},
                                                            {{-3},{
                                                               {66207},
                                                               {66211},
                                                               {66227},
                                                               {66231},
                                                               {66235},
                                                               {66239},
                                                               {66246}
                                                            }},
                                                            {{0.5},{
                                                               {66210},
                                                               {66226},
                                                               {66230},
                                                               {66234},
                                                               {66238},
                                                               {66242},
                                                               {66263}
                                                            }},
                                                            {{-0.5},{
                                                               {66209},
                                                               {66218},
                                                               {66229},
                                                               {66233},
                                                               {66237},
                                                               {66241},
                                                               {66244}
                                                            }},
                                                            {{1.5},{
                                                               {66268},
                                                               {66271},
                                                               {66273},
                                                               {66275},
                                                               {66277},
                                                               {66281}
                                                            }},
                                                            {{-1.5},{
                                                               {66269},
                                                               {66272},
                                                               {66274},
                                                               {66276},
                                                               {66278},
                                                               {66282},
                                                               {66283}
                                                            }}
                                                         };

    if(shuffle)
    {
        mymap = ShuffleMap(mymap);
    }
    return mymap;
}


std::map<double, std::vector<std::vector<int>>> GetA2SlowMap(bool shuffle = false)
{
  std::map<double, std::vector<std::vector<int>>> mymap = {
                                                            {{0},{
                                                               {66290},
                                                               {66293},
                                                               {66297},
                                                               {66300},
                                                               {66305},
                                                               {66309}
                                                            }},
                                                            {{-1},{
                                                               {66295},
                                                               {66296},
                                                               {66299},
                                                               {66303},
                                                               {66308},
                                                               {66311}
                                                            }},
                                                            {{-2},{
                                                               {66292},
                                                               {66294},
                                                               {66298},
                                                               {66302},
                                                               {66306},
                                                               {66310}
                                                            }},
                                                            // 10g runs are fast ramps used for calibration
                                                            {{-10},{

                                                             }},
                                                            {{10},{

                                                            }}
                                                         };
    if(shuffle)
    {
        mymap = ShuffleMap(mymap);
        mymap = ShuffleMap(mymap);
        mymap = ShuffleMap(mymap);
    }
    return mymap;
}


