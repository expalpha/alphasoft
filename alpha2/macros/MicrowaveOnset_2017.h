
// Set this to 0 when we have finalised the analysis! Never before!
#define DATA_BLINDED 1

std::map<int,double> magnet_reset_time = {
  { 1, 1509869000 }
};

std::map<int,std::vector<int>> trian_run_map = {
   { 1,
      {
         54560,
         54561,
         54562,
         54582,
         54583,
         54584,
         54585,
         54586,
         54587,
         54588,
         54589,
         54629,
         54630,
         54631,
         54632,
         54633,
         54634,
         54635,
         54636
      }
   }
};

int GetStartTimeOfRun(const int runNumber)
{
   for (const auto& pair: trian_run_map)
   {
      for (const int& run: pair.second)
         if (run == runNumber)
            return magnet_reset_time[pair.first];
   }
   return -1;
}

std::map<int,std::vector<int>> GetSeriesLists()
{
   std::srand(16);
#if !DATA_BLINDED
   return trian_run_map;
#endif
   std::map<int,std::vector<int>> shorted_map(trian_run_map);
   int shortest_list = -1;
   size_t shortest_list_length = 9999;
   for (const auto& pair: trian_run_map)
   {
      if (pair.second.size() < shortest_list_length)
      {
         shortest_list = pair.first;
         shortest_list_length = pair.second.size();
      }
   }
   
   // Make all series the same number of runs (so we can't immediately tell which is which)
   for (auto& pair: shorted_map)
   {
      std::vector<int>& run_list = pair.second;
      while (run_list.size() > shortest_list_length)
      {
         int remove_element = rand() % run_list.size();
         std::cout << "Removing " << remove_element << "\n";
         // Remove random run
         run_list.erase( 
            run_list.begin() + remove_element
         );
      }
   }
   
   std::vector<int> new_order;
   while (new_order.size() < shorted_map.size())
   {
      const int random_number = rand() % shorted_map.size() + 1;
      if (std::count(new_order.begin(), new_order.end(),random_number))
         continue;
      new_order.emplace_back(random_number);
   }
   std::cout <<"Order:";
   // Convert shorted_map to shuffled_map
   std::map<int,std::vector<int>> shuffled_map;
   for (int i = 0; i < new_order.size(); i++)
   {
      shuffled_map.insert( { i + 1, shorted_map[new_order[i]] } );
      std::cout << new_order[i] << "\n";
   }
   return shuffled_map;
}

double GetRandomOffset()
{
   std::srand(42);
   const double approximate_splitting = 1.42;
   const double random_number = static_cast<double> ( rand()) / static_cast<double>(RAND_MAX);
   // Random offset at ~20% level of the splitting
   const double random_offset = approximate_splitting * 0.2 * random_number + 0.1;
   return random_offset;
}

