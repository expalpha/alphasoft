//
// main_eventlist_generator.cxx
//
// Macro for dumping eventlists for MVA training in A2.
// L GOLINO
//

//Std Lib
#include <vector>
//Root stuff
#include "TChain.h"
#include "TTree.h"
#include "TBranch.h"
#include "TFile.h"
//Our stuff
#include "TA2Plot.h"
#include "TA2Plot_Filler.h"
#include "PlotGetters.h"

//R__LOAD_LIBRARY($ROOTSYS/test/libEvent.so)

TA2Plot DumpSaturationWindows(int runNumber, std::string filename, double step = 0.05, double threshold = 4, double filter = 20, double min_rate = 500, int skip = 2)
{
    //Init histos
    TH1D* trig_recieved_hist;
    TH1D* trig_out_hist;

    //Set time bin
    rootUtils::SetTimeBinSize(step);

    //First get all the mixing spills
    std::vector<TA2Spill> spills = Get_A2_Spills(runNumber, {"Mixing"}, {-1});
    std::vector<double> dump_starts;
    std::vector<double> dump_stops;

    //Outut plot
    TA2Plot output;

    //double cumulativeTime = 0;
    for(size_t i=0; i<spills.size(); i=i+skip) //Note: i=i+2 - we completely omit 50% of mixings as backup testing data
    {
        //For each spill, first get the whole trigger histograms with bins set to step size.
        double dumpstart = floor(spills.at(i).GetStartTime());
        double dumpstop = ceil(spills.at(i).GetStopTime());

        //Push back to vectors
        dump_starts.push_back(dumpstart);
        dump_stops.push_back(dumpstop);
    }

    TSISChannels chan(runNumber);

    //Get chrono hists
    std::vector<std::vector<TH1D*>> hists = Get_SIS(
       runNumber,
       {
           chan.GetChannel("IO32_TRIG"), //reads
           chan.GetChannel("IO32_TRIG_NOBUSY"), //triggers
       },
       dump_starts,
       dump_stops
    ); 

    int windows_in_taplot = 0;
    for(size_t i=0; i<hists.at(0).size(); i++)
    {
        trig_recieved_hist = hists.at(0).at(i); //triggers recieved: ie- reads
        trig_out_hist = hists.at(1).at(i); //triggers out of the detector: ie- triggers

        double dumpstart = dump_starts.at(i);
        //double dumpstop = dump_stops.at(i);

        //For each bin loop:
        for(int j=1;j<=trig_recieved_hist->GetNbinsX(); j++)
        {   
            //Get bin contents to compare whether detector is saturated or not.
            double trig_recieved = double(trig_recieved_hist->GetBinContent(j));
            double trig_out = double(trig_out_hist->GetBinContent(j));

            std::cout << "trig_recieved = " << trig_recieved << " vs " << trig_out << " = trig_out??" << (trig_out > threshold*trig_recieved) << std::endl;

            //If saturated above threshold add window to TA2Plot
            if(
               trig_out > threshold*trig_recieved && 
               trig_out < filter * trig_recieved &&
               trig_out / step > min_rate
            )
            {
                windows_in_taplot++;
                const double start_time = dumpstart + trig_out_hist->GetBinLowEdge(j);
                const double stop_time = start_time + trig_out_hist->GetBinWidth(j);
                std::cout << "Adding time window: [" << dumpstart+(j*step) << "," << dumpstart+((j+1)*step) << "]\n";
                std::cout << "Adding time window: [" << start_time << "," << stop_time << "]\n";
                std::cout << "Total time: " <<output.GetTotalTime() << " (" << windows_in_taplot << " windows)\n";


            
                output.AddTimeGate(
                   runNumber,
                   start_time,
                   stop_time
                 );
            }
        }
        delete trig_recieved_hist;
        delete trig_out_hist;
    }

    
    output.LoadData();
    std::string imagename = "debugpngs/eventlistcanvases/eventlist_mixing_";
    imagename += std::to_string(runNumber);
    imagename += "_ta2plot.png";
    output.DrawVertexCanvas("",0)->SaveAs(imagename.c_str());
    output.WriteEventList(filename, false, -1);
    return output;
}


int main(int argc, char* argv[])
{
    enum ERunType
    {
        kMixing,
        kCosmic,
        kUnassigned
    };

    std::vector<std::string> args;
    int runNumber = -1;
    double threshold = 2;
    double step = 0.05;
    bool fGlobal = false;
    ERunType runType = kUnassigned;

    for (int i=0; i<argc; i++) 
    {
        args.push_back(argv[i]);
    }

    for (unsigned int i=1; i<args.size(); i++) // loop over the commandline options
    { 
        if(args[i] == "--mixing")
            runType = kMixing;
        if(args[i] == "--cosmic")
            runType = kCosmic;
        if(args[i] == "--runnumber")
            runNumber = stoi(args[i+1]);
        if(args[i] == "--threshold")
            threshold = stof(args[i+1]);
        if(args[i] == "--step")
            step = stof(args[i+1]);
        if(args[i] == "--global")
            fGlobal = true;
    }

    
    std::string filename = "MVA/eventlists/A2MVA/";
    if(fGlobal)
    {
        filename += "eventlistfull";
    }
    else
    {
        filename += "eventlist";
        filename += std::to_string(runNumber);
    }


    //Ifndef find out which this run is.
    if(runType == kUnassigned)
    {
        std::vector<TA2Spill> mixingSpills = Get_A2_Spills(runNumber, {"Mixing"}, {-1});
        std::vector<TA2Spill> pbartransfersSpills = Get_A2_Spills(runNumber, {"Pbar Transfer FSTLNE"}, {-1});
        std::vector<TA2Spill> positrontransfersSpills = Get_A2_Spills(runNumber, {"Positron Transfer"}, {-1});
        if(mixingSpills.size() > 0)
        {
            runType = kMixing;
            std::cout << runNumber << ":mixingList\n";
        }
        else if(pbartransfersSpills.size()==0 && positrontransfersSpills.size()==0)
        {
            runType = kCosmic;
            std::cout << runNumber << ":cosmicList\n";
        }
        else
        {
            std::cout << "This run cannot be used, it has no mixings, but pbar or e- transfers.\n";
            return 0;
        }
    }

    //LMG - Switch/case only taking Mixing if Mixing.
    TA2Plot plot;
    switch(runType)
    {
        case kMixing:
        {
            DumpSaturationWindows(runNumber, filename, step, threshold);
            break;
        }
        case kCosmic:
        {
            Double_t finalTime = GetA2TotalRunTime(runNumber);
            plot.AddTimeGate(runNumber,0,finalTime);
            plot.LoadData();
            plot.WriteEventList(filename, false, -1);
            std::string imagename = "debugpngs/eventlistcanvases/eventlist_cosmic_";
            imagename += std::to_string(runNumber);
            imagename += "_ta2plot.png";
            plot.DrawVertexCanvas("",0)->SaveAs(imagename.c_str());
            break;
        }
        case kUnassigned:
        {
            std::cout << "Incorrect input given, please define a type for the data with --mixing or --cosmic\n";
            break;
        }
    }

    return EXIT_SUCCESS;
}
