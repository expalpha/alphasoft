#include <iostream>
#include <iomanip>
#include "TStyle.h"

#include "TA2SpillGetters.h"
#include "TSISChannels.h"

#include "TA2Plot.h"
#include "TA2Plot_Filler.h"

#include "PairGetters.h"

bool debug=true;

void WriteStackingSummaryA2(const int runNumber)
{
   std::ostringstream oss;
   oss << "AutoSISPlots/" << runNumber << "/R" << runNumber << "_A2StackingSummary.txt";
   std::string filename = oss.str();
   std::ofstream output(filename);

   std::vector<TA2Spill> ct_holds = Get_A2_Spills(runNumber, {"Hold"}, {-1});
   std::vector<TA2Spill> ct_hots = Get_A2_Spills(runNumber, {"Hot Dump"}, {-1});
   std::vector<TA2Spill> ct_tenths = Get_A2_Spills(runNumber, {"Tenth Dump"}, {-1});
   std::vector<TA2Spill> rct_holds = Get_A2_Spills(runNumber, {"Hold Dump"}, {-1});
   std::vector<TA2Spill> rct_hots = Get_A2_Spills(runNumber, {"RCT Hot Dump"}, {-1});
   std::vector<TA2Spill> mixings = Get_A2_Spills(runNumber, {"Mixing"}, {-1});
   std::vector<TA2Spill> rights = Get_A2_Spills(runNumber, {"Right Dump"}, {-1});
   std::vector<TA2Spill> lefts = Get_A2_Spills(runNumber, {"Left Dump"}, {-1});

   TSISChannels chans(runNumber);
   TSISChannel CTSISChannel = chans.GetChannel("CT_SiPM_OR");
   TSISChannel ATUSSISChannel = chans.GetChannel("SiPM_I");
   TSISChannel ATDSSISChannel = chans.GetChannel("SiPM_J");
   TSISChannel IO32SISChannel = chans.GetChannel("IO32_TRIG_NOBUSY");

   std::vector<double> LNE0s;
   std::vector<double> LNE0_times;
   double prev_time = std::numeric_limits<double>::infinity();
   double prev_lne0 = std::numeric_limits<double>::infinity();

   TTreeReaderPointer reader = Get_feGEM_Tree(runNumber, "ADandELENA", "LNEAPULB0030");
   if(reader)
   {
      TTreeReaderValue<TStoreGEMData<double>> event(*reader, "TStoreGEMData<double>");
      while(reader->Next()) {
      double time = event->GetRunTime();
      double lne0 = event->GetData().at(0);
      if(LNE0s.size() < ct_hots.size()) {
         const auto hot_dump = ct_hots.at(LNE0s.size());
         double dump_time = hot_dump.GetStartTime();

         if(prev_time < dump_time && time > dump_time) {
            LNE0s.push_back(prev_lne0);
            LNE0_times.push_back(prev_time);
         }
         }
      prev_time = time;
      prev_lne0 = lne0;
      }
      if(LNE0s.size() < ct_hots.size() && prev_time < ct_hots.at(LNE0s.size()).GetStartTime()) {
         LNE0s.push_back(prev_lne0);
         LNE0_times.push_back(prev_time);
      }
   }
   else
   {
      std::cout << "Warning: No LNE0 tree in root file, showing N/As instead\n";
   }

   // Summary table
   int w = 11;
   std::vector<std::vector<TA2Spill>> cols{ ct_holds, ct_hots, ct_tenths,
	   rct_holds, rct_hots, mixings,
	   rights, lefts };
   std::vector<TSISChannel> col_chans { CTSISChannel, CTSISChannel, CTSISChannel,
	   IO32SISChannel, ATUSSISChannel, IO32SISChannel,
	   ATDSSISChannel, ATUSSISChannel };
   std::vector<int> sum(cols.size(), 0);

   if(mixings.size() > 0) {
	   output << "Stacking summary:\n";
	   output << std::setw(3) << "#" << std::setw(w) << "LNE0" << std::setw(w) 
		   << "CT_Hold" << std::setw(w) << "CT_Hot" << std::setw(w) << "CT_Tenth"
		   << std::setw(w) << "RCT_Hold" << std::setw(w) << "RCT_Hot" << std::setw(w)
		   << "Mixing" << std::setw(w) << "Right" << std::setw(w) << "Left" << std::endl;
   }

   for(size_t i = 0; i < mixings.size(); ++i) {
	   output << std::setw(3) << i;
	   if(i < LNE0s.size()) {
		   output << std::setw(w) << LNE0s.at(i);
	   } else {
		   output << std::setw(w) << "N/A";
	   }

	   for(size_t j = 0; j < cols.size(); ++j) {
		   if(i < cols.at(j).size() && col_chans.at(j).IsValid()) {
			   int counts = cols.at(j).at(i).fScalerData->fDetectorCounts.at(col_chans.at(j).toInt());
			   output << std::setw(w) << counts;

			   sum.at(j) += counts;
		   } else {
			   output << std::setw(w) << "N/A";
		   }
	   }
	   output << std::endl;
   }
   output << std::string(w * (cols.size() + 1) + 3, '-') << std::endl;
   
   //If more than one mixing add the average column.
   if(mixings.size()>0)
   {
      output << std::setw(3) << "avg" << std::setw(w) << "";
      for(size_t i = 0; i < sum.size(); ++i) {
         output << std::setw(w) << sum.at(i) / mixings.size();
      }
   }
   output << std::endl;

   // Cold dumps summary
   std::vector<TA2Spill> colds = Get_A2_Spills(runNumber, {"Cold Dump"}, {-1});

   if(colds.size() > 0 && LNE0s.size() > 0) {
	   output << "\n\nCold dumps:\n";
	   output << std::setw(3) << "#" << std::setw(w) << "LNE0" << std::setw(w)
		   << "CT_SiOr" << std::setw(w) << "ATDS" << std::endl;

	   for(const auto& dump : colds) {
		   for(size_t i = LNE0s.size() - 1; i < LNE0s.size(); --i) {
			   if(LNE0_times.at(i) < dump.GetStartTime()) {
				   output << std::setw(3) << i << std::setw(w) << LNE0s.at(i);
				   break;
			   }
		   }

		   std::vector<int> counts = dump.fScalerData->fDetectorCounts;
		   if(CTSISChannel.IsValid()) {
		       output << std::setw(w) << counts.at(CTSISChannel.toInt());
		   } else {
		       output << std::setw(w) << "N/A";
		   }
		   if(ATDSSISChannel.IsValid()) {
			   output << std::setw(w) << counts.at(ATDSSISChannel.toInt()) << std::endl;
		   } else {
		       output << std::setw(w) << "N/A" << std::endl;
		   }
	   }
   }


   // Lifetime analysis
   std::vector<TA2Spill> lifetimeSpills = Get_A2_Spills(runNumber, {"Lifetime"}, {-1});
   if(lifetimeSpills.size() > 0)
   {
      TA2AnalysisReport report = Get_A2Analysis_Report(runNumber);
      double lifetime_start = report.GetRunStartTime() + lifetimeSpills.at(0).GetStartTime();

      output << "\n\nLifetime calculation:\n";
      std::vector<TA2Spill> coldDumpSpills =  Get_A2_Spills(runNumber,{"Cold Dump"},{-1});
      std::vector<TA2Spill> tenthDumpSpills =  Get_A2_Spills(runNumber,{"Tenth Dump"},{-1});
      if(coldDumpSpills.size()>=2 && tenthDumpSpills.size()>=2)
      {
         double lifetimeDumpLength = lifetimeSpills.at(0).GetStopTime() - lifetimeSpills.at(0).GetStartTime();
         TSISChannel channel = GetSISChannel(runNumber, "CT_SiPM_OR");

         double coldDump0 = Count_SIS_Triggers(runNumber, channel, {coldDumpSpills.at(0).GetStartTime()}, {coldDumpSpills.at(0).GetStopTime()});
         double FifthDump0 = Count_SIS_Triggers(runNumber, channel, {tenthDumpSpills.at(0).GetStartTime()}, {tenthDumpSpills.at(0).GetStopTime()});
         double coldDump1 = Count_SIS_Triggers(runNumber, channel, {coldDumpSpills.at(1).GetStartTime()}, {coldDumpSpills.at(1).GetStopTime()});
         double FifthDump1 = Count_SIS_Triggers(runNumber, channel, {tenthDumpSpills.at(1).GetStartTime()}, {tenthDumpSpills.at(1).GetStopTime()});
         
         output << "Lifetime = time / ln[(coldDump0/tenthDump0) / (coldDump1/tenthDump1)] \n";
         output << "Lifetime = " << lifetimeDumpLength << " / ln[(" << coldDump0 << "/" << FifthDump0 << ") / (" << coldDump1 << "/" << FifthDump1 << ")] \n";
         output << "Lifetime = " << lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) )) << "s";
         double lifetime_s = lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) ));
         output << " = " << lifetime_s/60 << "m";
         output << " = " << lifetime_s/(60*60) << "h\n\n";

         output << "delta = (lifetime**2/time)*sqrt[1/coldDump0 + 1/tenthDump0 + 1/coldDump1+1/tenthDump1] \n";
         output << "delta = (" << lifetime_s << "**2/" << lifetimeDumpLength << ")*sqrt[1/" << coldDump0 << " + 1/" << FifthDump0 << " + 1/" << coldDump1 << "+1/" << FifthDump1 << "] \n";
         output << "delta = " << ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1)) << "s";
         double delta_s = ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1));
         output << " = " << delta_s/60 << "m";
         output << " = " << delta_s/(60*60) << "h\n";

         TDatime datetimelifetime(lifetime_start);
         std::string datetimeSQL(datetimelifetime.AsSQLString());

         output << "\nLifetime @ " << datetimeSQL << " = " << lifetime_s/60 << "m +/- " << delta_s/60 << "m \n";
      }
      else
      {
         output << "Lifetime dump found, but missing 2 cold dumps and 2 tenth dumps. Not doing anything. If you used fifth dumps, this won't work.\n";
      }
   }
}

int main(int argc, char* argv[])
{
   gStyle->SetOptStat(1011111);
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 35000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 100000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }
   WriteStackingSummaryA2(runNumber);
   return 0;
}
