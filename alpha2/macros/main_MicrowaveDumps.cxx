#include <vector>
#include <iomanip>
#include "TA2SpillGetters.h"
#include "TA2Plot.h"
#include "TA2Plot_Filler.h"
#include "TSISChannels.h"
#include "PairGetters.h"
#include "TStyle.h"


bool verb = false;
void MicrowaveDumps(const int runNumber, const int dumpNo){
   


   
   std::vector<TA2Spill> microwave_dump = Get_A2_Spills(runNumber,{"Microwave Dump"},{dumpNo});
   if (microwave_dump.empty())
   {
      std::cout <<"No microwave dump!" <<std::endl;
      return;
   }

   const double start_time = microwave_dump.front().GetStartTime();
   const double stop_time = microwave_dump.front().GetStopTime();
   std::cout<<"Start: "<<start_time<<" Stop: "<<stop_time<<std::endl;
   //return;
   
   // Get data labels from feGEM
   std::vector<std::pair<double,std::vector<double>>> freq = GetFEGData(runNumber,"MicrowaveSynth\\Frequencies",0.,stop_time);
   std::vector<std::pair<double,std::vector<double>>> dwell = GetFEGData(runNumber,"MicrowaveSynth\\DwellTime",0.,stop_time);
   std::vector<std::pair<double,std::vector<double>>> power  = GetFEGData(runNumber,"MicrowaveSynth\\Power",0.,stop_time);
  
   TSISChannels chans(runNumber);
   TSISChannel mic_start_chan = chans.GetChannel("MIC_SYNTH_STEP_START");
   TSISChannel mic_stop_chan = chans.GetChannel("MIC_SYNTH_STEP_STOP");
   TSISChannel SVD_TRIG = chans.GetChannel("IO32_TRIG_NOBUSY");
   TSISChannel SVD_READ = chans.GetChannel("IO32_TRIG");

   std::vector<std::pair<double,int>> mic_starts_full = GetSISTimeAndCounts( runNumber, mic_start_chan, {start_time}, {stop_time});
   std::vector<std::pair<double,int>> mic_stops_full = GetSISTimeAndCounts( runNumber, mic_stop_chan, {start_time}, {stop_time});

   std::deque<double> mic_starts;
   std::deque<double> mic_stops;
   for (size_t i = 0; i < mic_starts_full.size(); i++) {
      mic_starts.push_back(mic_starts_full.at(i).first);
      if (i < mic_stops_full.size())
         mic_stops.push_back(mic_stops_full.at(i).first);
      else
         // Add artifical stop at the end of the dump window so we can more easily pair up events
         mic_stops.push_back(stop_time);
   }
   std::cout <<"Adding extra stop at end of dump: " << stop_time << "\n";
   mic_stops.push_back(stop_time);
   std::cout <<"Remove unused stop at start of dump: " << mic_stops.front() << "\n";
   mic_stops.pop_front();

   std::vector<TA2Plot*> windows;
   std::vector<TA2Plot*> inter_windows;
   
   std::cout << "------------------------------------------\n";
   std::cout << mic_starts.size() << " starts, "<< mic_stops.size() << " stops in dump\n";
   std::cout << "------------------------------------------\n";
   if( verb ) std::cout << "window\tstart\tstop\tduration\n";
   for ( size_t i = 0; i <  mic_starts.size(); i++)
     {
       double duration = mic_stops.at(i) - mic_starts.at(i);
       if( verb ) std::cout << i << "\t" << mic_starts.at(i) << "\t" << mic_stops.at(i) << "\t" << duration <<"\n";
     }
   
   TA2Plot_Filler f;
   for (size_t i = 0; i < mic_starts.size(); i++)
   {
      if (mic_stops.at(i) <  mic_starts.at(i))
      {
         std::cout 
               << "Something broken with your alignment of starts and stops in sweep " 
               << i << "! stop time < start time" << std::endl;
         continue;
      }

      TA2Plot* p = new TA2Plot();
      p->AddTimeGate(
         runNumber,
         mic_starts.at(i),
         mic_stops.at(i)
      );
      f.BookPlot(p);
      windows.push_back(p);

      if (i != 0)
      {
         if (mic_starts.at(i) < mic_stops.at(i-1) )
         {
            std::cout 
                     << "Something broken with your alignment of starts and stops in the window between "
                     << i - 1 << " and " << i << "!" << std::endl;
            continue;
         }
         TA2Plot* q = new TA2Plot();
         
         q->AddTimeGate(
            runNumber,
            mic_stops.at(i-1),
            mic_starts.at(i)
         );
         inter_windows.push_back(q);
         f.BookPlot(q);
      }
   }
   f.LoadData();

   std::vector<int> trig_per_sweep;
   std::vector<int> inter_trig_per_sweep;
   std::vector<int> read_per_sweep;
   std::vector<int> inter_read_per_sweep;
   for (size_t i = 0; i < mic_starts.size(); i++)
   {
      int trig_sum  = Count_SIS_Triggers(runNumber, SVD_TRIG, {mic_starts.at(i)}, {mic_stops.at(i)});
      trig_per_sweep.emplace_back(trig_sum);

      int read_sum  = Count_SIS_Triggers(runNumber, SVD_READ, {mic_starts.at(i)}, {mic_stops.at(i)});
      read_per_sweep.emplace_back(read_sum);
      
      if (i ==0)
         continue;

      trig_sum  = Count_SIS_Triggers(runNumber, SVD_TRIG, {mic_stops.at(i - 1)}, {mic_starts.at(i)});
      inter_trig_per_sweep.emplace_back(trig_sum);
      
      read_sum  = Count_SIS_Triggers(runNumber, SVD_READ, {mic_stops.at(i - 1)}, {mic_starts.at(i)});
      inter_read_per_sweep.emplace_back(read_sum);
   }

   uint32_t runStartTime = Get_A2Analysis_Report(runNumber).GetRunStartTime();

   double total_exposure_time = 0;
   int precision = std::cout.precision();
   std::cout << "------------------------------------------\n";
   std::cout << "window\tfreq\tpower\tunixtime start\t runtime start\tstop\tduration\tTrig\tRead\tPass\tMVA\n";
  for ( size_t i = 0; i < mic_starts.size(); i++)
   {
      std::cout << i << "\t";

      std::cout << std::setprecision( precision + 5);
      if (i + 1 < freq.back().second.size())
         std::cout << freq.back().second.at(i + 1) << "\t"; // The zeroth entry is before sweep
      else
         std::cout << "?\t";
      if (i + 1 < power.back().second.size())
         std::cout << power.back().second.at(i + 1) << "\t"; // The zeroth entry is before sweep
      else
         std::cout << "?\t";
      std::cout << std::setprecision( precision);

      // Unix time start
      std::cout << std::to_string(runStartTime + mic_starts.at(i)) << "\t";
      // Run time start     Run time stop      
      std::cout << mic_starts.at(i) << "\t" << mic_stops.at(i) << "\t";
      
      std::cout << std::setprecision( precision - 3)
                << mic_stops.at(i) - mic_starts.at(i) << "\t"
                << std::setprecision( precision );


      total_exposure_time += mic_stops.at(i) - mic_starts.at(i);

      std::cout << trig_per_sweep.at(i) << "\t" << read_per_sweep.at(i) << "\t";
      std::cout << windows.at(i)->GetNPassedType(1) << "\t";
      std::cout << windows.at(i)->GetNPassedType(2) << "\n";
   }
   std::cout << "==========================================\n";
   std::cout << "sum" << "\t" << /*mic_starts.front() <<*/ "\t" << /*mic_stops.back() <<*/ "\t";
   std::cout << total_exposure_time << "\t";
   std::cout << std::accumulate(trig_per_sweep.begin(), trig_per_sweep.end(), 0) << "\t";
   std::cout << std::accumulate(read_per_sweep.begin(), read_per_sweep.end(), 0) << "\t";
   std::cout << std::accumulate(windows.begin(),
                                windows.end(),
                                0,
                                [](int sum, const TA2Plot* elem){ return sum + elem->GetNPassedType(1);}) << "\t";
   std::cout << std::accumulate(windows.begin(),
                                windows.end(),
                                0,
                                [](int sum, const TA2Plot* elem){ return sum + elem->GetNPassedType(2);}) << "\t";
   std::cout <<"\n\n\n\n";
   std::cout << "------------------------------------------\n";
   double total_inter_window = 0;
   std::cout << "interwindow\tstart\tstop\tduration\tTrig\tRead\tPass\tMVA\n";
   for ( 
      size_t i = 1;
      i < mic_starts.size() && i < mic_stops.size();
      i++)
   {
      std::cout << i - 1 << " - " << i << "\t" << mic_stops.at(i - 1) << "\t" << mic_starts.at(i) << "\t";
      std::cout << std::setprecision( precision - 3)
                << mic_starts.at(i) - mic_stops.at(i - 1) << "\t"
                << std::setprecision( precision );
      total_inter_window += mic_starts.at(i) - mic_stops.at(i - 1);
      if (inter_windows.size() > i - 1)
      {
         std::cout << inter_trig_per_sweep.at(i - 1) << "\t" << inter_read_per_sweep.at(i - 1) << "\t";
         std::cout << inter_windows.at(i-1)->GetNPassedType(1) << "\t";
         std::cout << inter_windows.at(i-1)->GetNPassedType(2) << std::endl;
      }
      else
      {
         std::cout << "fail\tfail" << std::endl;
      }
   }
   std::cout << "==========================================\n";
   std::cout << "sum" << "\t" /*<< mic_stops.front()*/ << "\t" << /*mic_starts.back() <<*/ "\t";
   std::cout << total_inter_window << "\t";
   std::cout << std::accumulate(inter_trig_per_sweep.begin(),inter_trig_per_sweep.end(),0) << "\t";
   std::cout << std::accumulate(inter_read_per_sweep.begin(),inter_read_per_sweep.end(),0) << "\t";
   std::cout << std::accumulate(inter_windows.begin(),
                                inter_windows.end(),
                                0,
                                [](int sum, const TA2Plot* elem){ return sum + elem->GetNPassedType(1);}) << "\t";
   std::cout << std::accumulate(inter_windows.begin(),
                                inter_windows.end(),
                                0,
                                [](int sum, const TA2Plot* elem){ return sum + elem->GetNPassedType(2);}) << "\t";
   std::cout << "\n\n" << std::endl;

}

void MicrowaveDumps(const int runNumber)
{
   std::vector<TA2Spill> microwave_dump = Get_A2_Spills(runNumber,{"Microwave Dump"},{-1});
   if (microwave_dump.empty())
     std::cout <<"No microwave dump found in run " <<runNumber<<std::endl;
   else
     std::cout << microwave_dump.size() << " microwave dumps found in run "<<runNumber<<"\n";
   
   for (size_t i = 0; i < microwave_dump.size(); i++)
   {
      std::cout << "=====================================================================\n";
      std::cout << "                   Microwave Dump " << i << "\n";
      std::cout << "=====================================================================\n";
      MicrowaveDumps(runNumber,i);
   }
}



int main(int argc, char* argv[])
{
   gStyle->SetOptStat(1011111);
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 35000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 100000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }
   MicrowaveDumps(runNumber);
   return 0;
}
