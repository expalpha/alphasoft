

#include "PlotGetters.h"
#include "TSISChannels.h"
#include "TA2Plot_Filler.h"

#include <vector>


void SaveAllMixings(int runNumber)
{
   std::vector<TA2Spill> mixings = Get_A2_Spills(runNumber,{"Mixing"},{-1});

   if(mixings.size()<1)
   {
      std::cout << "No mixing dumps in this run...\n";
      return;
   }


   std::string folderIO32 = "AutoSISPlots/";
         gSystem->mkdir(folderIO32.c_str());
         folderIO32 += std::to_string(runNumber) + "/";
         gSystem->mkdir(folderIO32.c_str());
         folderIO32 += "IO32_TRIG_NOBUSY/";
         gSystem->mkdir(folderIO32.c_str());

   std::string nameAllIO32 = folderIO32 + "R";
   nameAllIO32 += std::to_string(runNumber);
   nameAllIO32 += "_IO32_AllMixings.png";
   Plot_SIS(runNumber, {"IO32_TRIG_NOBUSY"}, {"Mixing"}, {-1})->SaveAs(nameAllIO32.c_str());
   
   std::string nameAllIO32_pdf = folderIO32 + "R";
   nameAllIO32_pdf += std::to_string(runNumber);
   nameAllIO32_pdf += "_IO32_AllMixings.pdf";
   Plot_SIS(runNumber, {"IO32_TRIG_NOBUSY"}, {"Mixing"}, {-1})->SaveAs(nameAllIO32_pdf.c_str());


      std::string folderATMOR = "AutoSISPlots/";
         gSystem->mkdir(folderATMOR.c_str());
         folderATMOR += std::to_string(runNumber) + "/";
         gSystem->mkdir(folderATMOR.c_str());
         folderATMOR += "ATOM_SiPM_OR//";
         gSystem->mkdir(folderATMOR.c_str());

   std::string nameAllSiPMATMOR = folderATMOR + "R";
   nameAllSiPMATMOR += std::to_string(runNumber);
   nameAllSiPMATMOR += "_ATM_OR_AllMixings.png";
   Plot_SIS(runNumber, {"ATOM_SiPM_OR"}, {"Mixing"}, {-1})->SaveAs(nameAllSiPMATMOR.c_str());


}


int main(int argc, char* argv[])
{
   gStyle->SetOptStat(1011111);
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 35000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 100000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }
   
   SaveAllMixings(runNumber);

   return 0;
}
