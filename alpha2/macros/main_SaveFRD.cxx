#include "TStyle.h"

#include "TA2Plot.h"

void SaveFRD(const int runNumber)
{
   auto FRDs = Get_A2_Spills(runNumber,{"*RampDown"},{-1});
   if (FRDs.empty()) {
     std::cout << "No FRD in this run" <<std::endl;
     return;
   }
   for (const TA2Spill& s: FRDs) {
      std::cout << " --------------------" << s.fName << " --------------------\n";
      TA2Plot a;
      a.AddDumpGates({s});
      a.LoadData();
      std::cout << a.GetVertexEvents().CSVTitleLine() << "\n";
      for (size_t i = 0; i < a.GetVertexEvents().size(); ++i)
         std::cout << a.GetVertexEvents().CSVLine(i) << "\n";
   }
}



int main(int argc, char* argv[])
{
   gStyle->SetOptStat(1011111);
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 35000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 100000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }
   SaveFRD(runNumber);
   return 0;
}
