
#include "TA2Plot.h"
#include "TA2Plot_Filler.h"
#include "TA2Spill.h"
#include "TA2SpillGetters.h"
#include "TAPlotFEGEMData.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TAPlotVertexEvents.h"
#include "TFile.h"
#include "TH2D.h"
#include <vector>
#include <utility> //std::pair

void Plot_A2_Power(int runNumber, bool DrawVertices =false, bool zeroTime = true);

