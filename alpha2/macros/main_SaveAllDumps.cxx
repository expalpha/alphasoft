
#include "PlotGetters.h"
#include "TSISChannels.h"
#include "TA2Plot_Filler.h"

#include <vector>



double GetEndOfFinalMixing(const int runNumber)
{
   std::vector<TA2Spill> mixings = Get_A2_Spills(runNumber,{"Mixing"},{-1});
   if (mixings.size())
      return mixings.back().GetStopTime();
   return -1;
}

void SaveAllDumps(int runNumber, bool after_mixing)
{
   
   const double final_mixing = GetEndOfFinalMixing(runNumber);
   
   std::vector<TA2Spill> all_spills = Get_A2_Spills(runNumber,{"*"},{-1});
   std::vector<TA2Spill> all;
   // Filter spills that start after mixing
   if (after_mixing)
   {
      std::cout << "Plotting after mixing (" << final_mixing << "s)...";
      for (const TA2Spill& s: all_spills)
      {
         if (s.GetStartTime() > final_mixing)
         {
            all.emplace_back(s);
         }
      }
      std::cout << " found " << all.size() << " dumps\n";
   }
   else // Filter spills that start before mixing
   {
      std::cout << "Plotting before mixing (" << final_mixing << "s)...";
      for (const TA2Spill& s: all_spills)
      {
         if (s.GetStartTime() > final_mixing)
            continue;
         all.emplace_back(s);
      }
      std::cout << " found " << all.size() << " dumps\n";
   }
   if (all.empty())
      return;

   //Get this list from the root file! (or ODB)
   /*std::vector<std::string> detector_channels = {
         "SIS_PMT_CATCH_OR",
         "SIS_PMT_CATCH_AND",
         "CT_SiPM1",
         "CT_SiPM2",
         "CT_SiPM_OR",
         "CT_SiPM_AND",
         "PMT_11",
         "AT_SiPM_Stick"};*/
   std::vector<std::string> detector_channels = {
         "CT_SiPM_OR",
         "ATOM_SiPM_OR",
         "IO32_TRIG_NOBUSY",
         "PMT_11",
         //"CT_SiPM_AND",
         //"ATOM_SiPM_AND",
         "SiPM_H",
         "SiPM_I",
         "SiPM_J"};
   std::vector<TSISChannel> chans;
   for (std::string channel_name: detector_channels)
   {
      TSISChannels* sisch = new TSISChannels(runNumber);
      chans.push_back(sisch->GetChannel(channel_name.c_str()));
      delete sisch;
   }

   std::vector<std::vector<TH1D*>> Histos = Get_SIS(runNumber,chans,all);
   std::cout<<Histos.size() <<"\t"<<Histos.at(0).size()<<std::endl;
   
   //All sis channels
   for (size_t i = 0; i< detector_channels.size(); i++)
   {
      std::string channel_name = detector_channels.at(i);
      
      std::map<std::string,int> dumpIndex_counter;
    
      TSISChannels* sisch = new TSISChannels(runNumber);
      TSISChannel ch = sisch->GetChannel(channel_name.c_str());
      delete sisch;

      std::cout<<channel_name <<"\tis sis channel\t"<<ch<<std::endl;
      if (!ch.IsValid())
         continue;
      
      for (size_t j = 0; j < all.size(); j++)
      {
         TA2Spill s = all.at(j);
         if (s.GetStartTime() == s.GetStopTime())
         {
            std::cout<<s.fName << " has no time duration (its a pulse...) skipping plot" <<std::endl;
            continue;
         }
         if (!s.fScalerData)
         {
            std::cout<<s.fName << " has no scaler data (its an information dump...) skipping plot" <<std::endl;
            continue;
         }
         if (!s.fScalerData->fDetectorCounts.at(ch.toInt()))
         {
            std::cout<<s.fName << " has no counts in channel " << ch<<" ... skipping plot" <<std::endl;
            continue;
         }
         std::string dump_name = s.GetSequenceName() + "_" + s.fName + "_" + std::to_string(dumpIndex_counter[s.GetSequenceName() + "_" + s.fName]++);
         // Remove quote marks... they upset uploading to elog
         dump_name.erase(std::remove(dump_name.begin(), dump_name.end(), '"'), dump_name.end());
         std::cout << "dump_name:"<< channel_name << "/"<< dump_name <<std::endl;

         TCanvas* c = new TCanvas();
         Histos.at(i).at(j)->Draw("HIST");

         std::string folder = "AutoSISPlots/";
         gSystem->mkdir(folder.c_str());
         folder += std::to_string(runNumber) + "/";
         gSystem->mkdir(folder.c_str());
         folder += std::string(channel_name.c_str()) + "/";
         gSystem->mkdir(folder.c_str());
         
         std::string filename = folder + "R" + std::to_string(runNumber) + std::string("_") + dump_name + ".png";
         c->Draw();
         c->SaveAs(filename.c_str());
         c->Close();
         delete c;
      }
   }
   // Save extra cherry-picked
   std::string all_right_name = "AutoSISPlots/" + std::to_string(runNumber)
	   + "/SiPM_J/R" + std::to_string(runNumber) + "_all_RightDump_SiPM_J.png";
   Plot_SIS(runNumber, {"SiPM_J"}, {"Right Dump"}, {-1})->SaveAs(all_right_name.c_str());

   std::string all_left_name = "AutoSISPlots/" + std::to_string(runNumber)
	   + "/SiPM_I/R" + std::to_string(runNumber) + "_all_LeftDump_SiPM_I.png";
   Plot_SIS(runNumber, {"SiPM_I"}, {"Left Dump"}, {-1})->SaveAs(all_left_name.c_str());
}

void SaveAllDumps(int runNumber)
{
   SaveAllDumps(runNumber, true);
   SaveAllDumps(runNumber, false);
}



int main(int argc, char* argv[])
{
   gStyle->SetOptStat(1011111);
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 35000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 100000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }

   SaveAllDumps(runNumber);

   return 0;
}


