
#include "PlotGetters.h"
#include "TSISChannels.h"
#include "TA2Plot_Filler.h"

#include <vector>

void SaveColdDumps(int runNumber)
{
   std::vector<TA2Spill> colddumps = Get_A2_Spills(runNumber,{"Cold Dump"},{-1});

   if(colddumps.size()<1)
   {
      std::cout << "No cold dumps found, not plotting them.\n";
      return;
   }

   std::string folder = "AutoSISPlots/";
         gSystem->mkdir(folder.c_str());
         folder += std::to_string(runNumber) + "/";
         gSystem->mkdir(folder.c_str());
         folder += "ColdDumps/";
         gSystem->mkdir(folder.c_str());
         
   for(size_t i=0; i<colddumps.size(); i++)
   {
      std::string filename = folder + "R" + std::to_string(runNumber) + std::string("_") + std::to_string(i) + ".png";
      if(colddumps.at(i).GetSequenceName()=="cat")
      {
         TCanvas* c = Plot_A2_CT_ColdDump(runNumber, i);
         c->SaveAs(filename.c_str());
         c->Close();
         //delete c;
      }
      if(colddumps.at(i).GetSequenceName()=="atm")
      {
         TCanvas* c = Plot_A2_PreMix_ColdDump(runNumber, i, 1000);
         c->SaveAs(filename.c_str());
         c->Close();
         //delete c;
      }
      if(colddumps.at(i).GetSequenceName()=="rct")
      {
         std::cout << "No dump file yet.\n";
      }
      
   }

   return;
}
int main(int argc, char* argv[])
{
   gStyle->SetOptStat(1011111);
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 35000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 100000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }
   
   SaveColdDumps(runNumber);
   return 0;
}
