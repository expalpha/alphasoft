#include "TA2Plot.h"
#include "TA2Spill.h"
#include "FileGetters.h"

#include <vector>
#include <iostream>

#include "TSISChannels.h"

#include "TA2SpillGetters.h"


void DumpMixingData(int runNumber, const char*);
