
#include "TA2SpillGetters.h"
#include "TA2Plot.h"
#include "TA2Plot_Filler.h"


double GetEndOfFinalMixing(const int runNumber)
{
   std::vector<TA2Spill> mixings = Get_A2_Spills(runNumber,{"Mixing"},{-1});
   if (mixings.size())
      return mixings.back().GetStopTime();
   return -1;
}

void PBarBudget(const int runNumber)
{
   const double final_mixing = GetEndOfFinalMixing(runNumber);
   
   std::vector<TA2Spill> all_spills = Get_A2_Spills(runNumber,{"*"},{-1});
   std::vector<TA2Spill> rct_spills;
   std::vector<TA2Spill> atm_spills;
   
   for (const TA2Spill& s: all_spills)
   {
      if (s.GetStartTime() > final_mixing)
      {
         if (s.GetSequenceName() == "atm" )
            atm_spills.emplace_back(s);
         if (s.GetSequenceName() == "rct" )
            rct_spills.emplace_back(s);
      }
   }
   
   std::vector<std::string> rct_dump_names;
   std::vector<TA2Plot*> rct_counts;
   std::vector<std::string> atm_dump_names;
   std::vector<TA2Plot*> atm_counts;
   
   double last_stop_time = -1.;
   for (const auto& s: rct_spills)
   {
      if (last_stop_time > 0.)
      {
          TA2Plot* p = new TA2Plot();
          rct_dump_names.emplace_back("interdump gap");
          p->AddTimeGate(runNumber,last_stop_time,s.GetStartTime());
          rct_counts.emplace_back(p);
      }
      rct_dump_names.emplace_back(s.GetSanitisedName());
      TA2Plot* p = new TA2Plot();
      p->AddTimeGate(runNumber,s.GetStartTime(),s.GetStopTime());
      rct_counts.emplace_back(p);
      last_stop_time = s.GetStopTime();
   }

   last_stop_time = -1.;
   for (const auto& s: atm_spills)
   {
      if (last_stop_time > 0.)
      {
          TA2Plot* p = new TA2Plot();
          atm_dump_names.emplace_back("interdump gap");
          p->AddTimeGate(runNumber,last_stop_time,s.GetStartTime());
          atm_counts.emplace_back(p);
      }
      atm_dump_names.emplace_back(s.GetSanitisedName());
      TA2Plot* p = new TA2Plot();
      p->AddTimeGate(runNumber,s.GetStartTime(),s.GetStopTime());
      atm_counts.emplace_back(p);
      last_stop_time = s.GetStopTime();
   }
   TA2Plot_Filler filler;
   for (TA2Plot* p: rct_counts)
      filler.BookPlot(p);
   for (TA2Plot* p: atm_counts)
      filler.BookPlot(p);
   filler.LoadData();
   
   
   std::string folder = "AutoSISPlots/";
   gSystem->mkdir(folder.c_str());
   folder += std::to_string(runNumber) + "/";
   gSystem->mkdir(folder.c_str());

   std::ofstream myfile;
   std::string filename = folder + "R";
   filename += std::to_string(runNumber);
   filename += "_rctbudget.csv";
   myfile.open (filename.c_str());
   myfile << "DumpName,TotalTime,NVertexEvents,NVertex,NPassedCut,NMVA,BackgroundSubVerts,BackgroundSubPassedCuts,BackgroundSubMVA\n";


   for (size_t i = 0; i < rct_counts.size(); i++ )
   {
      TA2Plot* p = rct_counts.at(i);
      myfile << rct_dump_names.at(i) << ",";
      myfile << p->GetTotalTime() << ",";
      myfile << p->GetNVertexEvents() << ",";
      for (int i = 0; i < 3; i++)
      {
         myfile << p->GetNPassedType(i) << ",";
      }
      std::vector<double> background_rates = { 7., 45.E-3, 5.E-3};
      for (int i = 0; i < 3; i++)
      {
         myfile << p->GetNPassedType(i) - background_rates.at(i) * p->GetTotalTime()<< ",";
      }
      myfile << "\n";
   }
   myfile.close();
   
   
   filename = folder + "R";
   filename += std::to_string(runNumber);
   filename += "_atmbudget.csv";
   myfile.open (filename.c_str());
   myfile << "DumpName,TotalTime,NVertexEvents,NVertex,NPassedCut,NMVA,BackgroundSubVerts,BackgroundSubPassedCuts,BackgroundSubMVA\n";
   for (size_t i = 0; i < atm_counts.size(); i++ )
   {
      TA2Plot* p = atm_counts.at(i);
      myfile << atm_dump_names.at(i) << ",";
      myfile << p->GetTotalTime() << ",";
      myfile << p->GetNVertexEvents() << ",";
      for (int i = 0; i < 3; i++)
      {
         myfile << p->GetNPassedType(i) << ",";
      }
      std::vector<double> background_rates = { 7., 45.E-3, 5.E-3};
      for (int i = 0; i < 3; i++)
      {
         myfile << p->GetNPassedType(i) - background_rates.at(i) * p->GetTotalTime()<< ",";
      }
      
      myfile << "\n";
   }
   myfile.close();
   
   

}


int main(int argc, char* argv[])
{
   gStyle->SetOptStat(1011111);
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 35000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 100000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }

   PBarBudget(runNumber);

   return 0;
}

