
#include "TA2Plot.h"
#include "TA2Plot_Filler.h"

#include "TA2SpillGetters.h"
#include "TA2Spill.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TAPlotVertexEvents.h"

#include <vector>
#include <utility> //std::pair

std::vector<TA2Plot*> Plot_Lamb_LS_KO(int runNumber, bool IsCState=false, bool DrawVertices=false, bool zeroTime = true);
double GetRep(double time, const std::vector<TA2Spill>& MWSpills, int& lastRep, bool& IsLight);

double GetRep1(double time, const std::vector<TA2Spill>& MWSpills, int& lastRep, bool& IsLight);
