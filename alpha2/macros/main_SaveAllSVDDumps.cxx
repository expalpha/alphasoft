
#include "PlotGetters.h"
#include "TSISChannels.h"
#include "TA2Plot_Filler.h"

#include <vector>
double GetEndOfFinalMixing(const int runNumber)
{
   std::vector<TA2Spill> mixings = Get_A2_Spills(runNumber,{"Mixing"},{-1});
   if (mixings.size())
      return mixings.back().GetStopTime();
   return -1;
}

void SaveAllSVDDumps(int runNumber, bool SaveEmpty, bool after_mixing)
{
   const double final_mixing = GetEndOfFinalMixing(runNumber);
   
   std::vector<TA2Spill> all_spills = Get_A2_Spills(runNumber,{"*"},{-1});
   std::vector<TA2Spill> all;
   // Filter spills that start after mixing
   if (after_mixing)
   {
      for (const TA2Spill& s: all_spills)
      {
         if (s.GetStartTime() > final_mixing)
         {
            all.emplace_back(s);
         }
      }
   }
   else // Filter spills that start before mixing
   {
      for (const TA2Spill& s: all_spills)
      {
         if (s.GetStartTime() > final_mixing)
            continue;
         all.emplace_back(s);
      }
   }
   if (all.empty())
      return;

   std::vector<TA2Plot*> plots;
   std::vector<std::string> plot_names;
   TA2Plot_Filler DataLoader;
   
   
   std::map<std::string,int> repetition_counter;
   if (all.empty())
   {
      double svd = GetTotalRunTimeFromSVD(runNumber);
      double sis = GetTotalRunTimeFromSIS(runNumber);
      double tmax;
      if ( sis > svd)
         tmax = sis;
      else
         tmax = svd;
      TA2Plot* a = new TA2Plot();
      a->AddTimeGate(runNumber, 0, tmax);
      plots.push_back(a);
      DataLoader.BookPlot(a);
   }
   else
   {
      for (const TA2Spill& s: all)
      {
         if (s.GetSequenceName() != "atm" && s.GetSequenceName() != "rct")
            continue;
         if (s.GetStartTime() == s.GetStopTime())
         {
            std::cout<<s.fName << " has no time duration (its a pulse...) skipping plot" <<std::endl;
            continue;
         }
         if (!s.fScalerData)
         {
            std::cout<<s.fName << " has no scaler data (its an information dump...) skipping plot" <<std::endl;
            continue;
         }

         std::string dump_name = s.GetSequenceName() + "_" + s.fName + "_" + std::to_string(repetition_counter[s.GetSequenceName() + "_" + s.fName]++);
         // Remove quote marks... they upset uploading to elog
         dump_name.erase(std::remove(dump_name.begin(), dump_name.end(), '"'), dump_name.end());
         std::cout << "dump_name:"<< dump_name <<std::endl;

         TA2Plot* a = new TA2Plot();
         a->AddDumpGates(runNumber, { s });
         DataLoader.BookPlot(a);

         plots.push_back(a);
         plot_names.push_back(dump_name);
      }
   }
   DataLoader.LoadData();
   
   std::string folder = "AutoSISPlots/";
   gSystem->mkdir(folder.c_str());
   folder += std::to_string(runNumber) + "/";
   gSystem->mkdir(folder.c_str());
   folder += "SVD/";
   gSystem->mkdir(folder.c_str());
      
   if (all.empty())
   {
      std::cout <<"No dumps found... Save as:";
      std::string filename = folder + "R" + std::to_string(runNumber) + std::string("_EntireRun.png");
      std::cout <<filename<<std::endl;
      plots.front()->DrawCanvas()->SaveAs(filename.c_str());
      std::cout<<"Done"<<std::endl;
   }
   else
   {
      for (size_t j = 0; j < plots.size(); j++)
      {   
         std::string filename = folder + "R" + std::to_string(runNumber) + std::string("_") + plot_names.at(j) + ".png";
         if (SaveEmpty)
         {
            plots.at(j)->DrawCanvas()->SaveAs(filename.c_str());
         }
         else
         {
            if (plots.at(j)->GetVertexEvents().CountPassedCuts(1))
               plots.at(j)->DrawCanvas()->SaveAs(filename.c_str());
         }
      }
   }
}


TCanvas* SVD_QOD(int runNumber)
{
   TH1I* NOcc = (TH1I*)Get_File(runNumber)->Get("svd_qod/Si_Occs_nside");
   TH1I* POcc = (TH1I*)Get_File(runNumber)->Get("svd_qod/Si_Occs_pside");
   TH1I* BothOcc = (TH1I*)Get_File(runNumber)->Get("svd_qod/Si_Occs");


   TCanvas *canvas = new TCanvas("SVD_QOD", "SVD_QOD", 1800, 1000);

   canvas->Divide(2, 2);

   // Canvas 1
   canvas->cd(1);
   NOcc->Draw("hist");

   // Canvas 2
   canvas->cd(2); // Z-counts (with electrodes?)4
   POcc->Draw("hist");

   // Canvas 3
   canvas->cd(3); // T-counts
   BothOcc->Draw("hist");

   // Canvas 4
   canvas->cd(4);



    std::string folder = "AutoSISPlots/";
    gSystem->mkdir(folder.c_str());
    folder += std::to_string(runNumber) + "/";
    gSystem->mkdir(folder.c_str());
    folder += "SVD_QOD/";
    gSystem->mkdir(folder.c_str());
    std::string filename = folder + "SVD_QOD_Plot_";
    filename+=std::to_string(runNumber);
    filename+=".png";

    canvas->SaveAs(filename.c_str());

   return canvas;
}

void SaveAllSVDDumps(int runNumber, bool SaveEmpty = false)
{
   SaveAllSVDDumps(runNumber,SaveEmpty,true);
   SaveAllSVDDumps(runNumber,SaveEmpty,false);
}


int main(int argc, char* argv[])
{
   gStyle->SetOptStat(1011111);
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 35000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 100000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }

   SaveAllSVDDumps(runNumber);
   SVD_QOD(runNumber);

   return 0;
}
