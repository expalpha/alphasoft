#include <iostream>
#include <iomanip>
#include "TStyle.h"

#include "TA2SpillGetters.h"
#include "TSISChannels.h"

#include "TA2Plot.h"
#include "TA2Plot_Filler.h"

#include "PairGetters.h"

bool debug=true;

void SaveLossesForDump(const int runNumber, const std::string spillName)
{

   std::vector<TA2Spill> spills = Get_A2_Spills(runNumber, {spillName}, {-1});
   std::vector<int> passCuts;
   TH1I* hPassCuts = new TH1I("hPassCuts",Form("R%d Passed Cuts During [%s];%s Dump Number;Passed Cuts",runNumber,spillName.c_str(),spillName.c_str()),spills.size(),0,spills.size());

   for (unsigned int i=0; i<spills.size(); i++) {
      passCuts.push_back(spills[i].fScalerData->fPassCuts);
      hPassCuts->Fill(i,spills[i].fScalerData->fPassCuts);
   }

   TCanvas* c = new TCanvas();
   hPassCuts->SetMinimum(0);
   hPassCuts->SetStats(0);
   hPassCuts->Draw("HIST");

   std::string folder = "AutoSISPlots/";
   gSystem->mkdir(folder.c_str());
   folder += std::to_string(runNumber) + "/";
   gSystem->mkdir(folder.c_str());
   folder += "PassCutsByDump/";
   gSystem->mkdir(folder.c_str());
   
   std::string filename = folder + "R" + std::to_string(runNumber) + std::string("_PassCutsByDump_") + spillName + ".png";
   c->Draw();
   c->SaveAs(filename.c_str());
   c->Close();
   delete c;
   delete hPassCuts;
}

void SaveLossesForDumps(const int runNumber)
{
   std::vector<TA2Spill> all_spills = Get_A2_Spills(runNumber,{"*"},{-1});
   std::vector<std::string> all_spill_names;
   for (const TA2Spill& spill: all_spills) {
      bool used=false;
      for (const std::string& name: all_spill_names) {
         if (name==spill.fName.substr(1, spill.fName.size() - 2)) {
            used = true;
            break;
         }
      }
      if (!used) {
         all_spill_names.push_back(spill.fName.substr(1, spill.fName.size() - 2));
      }
   }
   for (const std::string& name: all_spill_names) {
      SaveLossesForDump(runNumber,name);
   }
}

int main(int argc, char* argv[])
{
   gStyle->SetOptStat(1011111);
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 35000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 100000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }
   SaveLossesForDumps(runNumber);
   return 0;
}
