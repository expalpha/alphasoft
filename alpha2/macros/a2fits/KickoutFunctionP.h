#ifndef MN_KickoutFunctionP_H_
#define MN_KickoutFunctionP_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom.h>
//#include <iostream>
namespace ROOT { 
  namespace Minuit2 {
    class KickoutFunctionp {

    public:
      KickoutFunctionp(double N0, double lamd, double lamc, double lamg, double bkg, double leak, double gammad,double gammac, double rncd,const std::vector<double>&LightTimes,const std::vector<double>& DarkTimes,const std::vector<double>&PowerD,const std::vector<double>&PowerC)        
      {
        the_N0=N0;
        the_lamd=lamd;
        the_lamc=lamc;
        the_lamg=lamg;
        the_bkg=bkg;
        the_leak=leak;
        the_gammad=gammad;
        the_gammac=gammac;
        the_rncd=rncd;
        // std::cout<<"KickoutFunctionp init ";
        TheFcn fFcn;
        NRep=fFcn.GetNRep();
        // std::cout<<"NRep= "<<NRep<<std::endl;
        NRep=(NRep>299)?299:NRep;
        N_d[0]=the_N0;
        N_c[0]=the_N0*the_rncd ;
        ndd[0]=0.;
        ncc[0]=0.;
        ndark[0]=0.;
        double ldmpd,ldmpc; //Events remaining at end of laser pulse.
        int rep;
        for (int irep=1;irep<=2*NRep;irep++) {
          // here are the recurrence defs of N_d(irep), N_c(irep), ndd(irep/2+1) ncc(irep/2) 
          if((irep-1)%4<2){
            rep= irep/2+1;
            //           std::cout<<"rep "<<rep;
            ldmpd=N_d[irep-1]*exp(-(the_lamd*PowerD[rep]*(pow(rep,the_gammad)-pow(rep-1, the_gammad))+the_lamg)*LightTimes[irep-1]);
            // std::cout<<" ldmpd="<<ldmpd;
            ldmpc=N_c[irep-1]*exp(-the_lamg*LightTimes[irep-1]);
            // std::cout<<" ldmpc="<<ldmpc;
            ndd[rep]=N_d[irep-1]-ldmpd+N_c[irep-1]-ldmpc;
             // std::cout<<" ndd="<<ndd[rep];
           N_d[irep] =ldmpd*exp(-the_lamg*DarkTimes[irep-1]);
           // std::cout<<" N_d="<<N_d[irep]<<std::endl;
            N_c[irep]=ldmpc*exp(-the_lamg*DarkTimes[irep-1]);
            ndark[irep] =N_d[irep-1] -N_d[irep]+N_c[irep-1]-N_c[irep]-ndd[rep] +the_bkg*DarkTimes[irep-1] ;
            ndark[irep]=ndark[irep]+ndd[rep]*the_leak; //leak correction
            ndd[rep]=ndd[rep]*(1-the_leak)+the_bkg*LightTimes[irep-1];
            //std::cout<<irep<<", "<<N_c[irep]<<"; ";  
          }
          else {
            rep=irep/2;
	    ldmpc=N_c[irep-1]*exp(-(the_lamc*PowerC[rep]*(pow(rep,the_gammac)-pow(rep-1, the_gammac))+the_lamg)*LightTimes[irep-1]);
          ldmpd=N_d[irep-1]*exp(-the_lamg*LightTimes[irep-1]);
            ncc[rep]=N_c[irep-1]-ldmpc+N_d[irep-1]-ldmpd;
            N_d[irep] =ldmpd*exp(-the_lamg*DarkTimes[irep-1]);
            N_c[irep]=ldmpc*exp(-the_lamg*DarkTimes[irep-1]);
            ndark[irep] =N_d[irep-1] -N_d[irep]+N_c[irep-1]-N_c[irep]-ncc[rep]  +the_bkg*DarkTimes[irep-1];                 
            ndark[irep]=ndark[irep]+ncc[rep]*the_leak; //leak correction
            ncc[rep]=ncc[rep]*(1-the_leak)+the_bkg*LightTimes[irep-1];
            //std::cout<<irep<<", "<<N_c[irep]<<"; ";  
          }
          //std::cout<<std::endl;
        } 
      }
        ~KickoutFunctionp(){}

        double N0() const {return the_N0;}
        double lamd() const {return the_lamd;}
        double lamc() const {return the_lamc; }
        double lamg() const {return the_lamg;}
        double bkg() const {return the_bkg;}
        double leak() const {return the_leak;}
        double fndd(int rep) const {return ndd[rep];}
        double fndd(double x) const {return ndd[int(x)];}
        double fndd_(double* rep, double*) /*const*/ {
          return ndd[int(rep[0]+0.5)];
        }
        double fncc(int rep) const {return ncc[rep];}
        double fncc(double x) const {return ncc[int(x)];}
        double fncc_(double* rep, double*) /*const*/ {
          return ncc[int(rep[0]+1.5)];
        }
        double fndark(int rep) const {return ndark[rep];}
        double fndark(double x) const {return ndark[int(x)];}
        double fndark_(double* rep, double* ) {return ndark[int(rep[0]+0.5)];}
        double fN_d(int rep) const {return N_d[2*rep];}
      double fN_d_(double* rep, double*)  {return N_d[int(2*rep[0]+0.5)];}
        double fN_c(int rep) const {return N_c[2*rep];}
        double fN_c_(double* rep, double*)  {return N_c[int(2*rep[0]+0.5)];}
     double frncd() const {return the_rncd;}
        //      double operator()(double x) const {return {n_dd[int(x))]};     
      private:
        double the_N0;
        double the_lamd;
        double the_lamc;
        double the_lamg;
        double the_bkg;
        double the_leak;
        double the_gammad;
        double the_gammac;
        double the_rncd;
      //      std::vector<double>the_darkfrac;
      double N_d[600]{0.};
      double N_c[600]{0.};
      double ndd[300]{0.};
      double ncc[300]{0.};
      double ndark[600]{0.};
        int NRep=260;
      };
    }
  }
#endif // MN_KickoutFunctionP_H_
