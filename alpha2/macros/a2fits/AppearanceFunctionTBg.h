#ifndef MN_AppearanceFunction_H_
#define MN_AppearanceFunction_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom3.h>

  namespace ROOT { 
  namespace Minuit2 {
    class AppearanceFunction {
    public:
      AppearanceFunction(double peak,double dmatch, double gammar, double gammab,double amp, double bkg, double sigma) :
        the_peak(peak), the_dmatch(dmatch), the_gammar(gammar),the_gammab(gammab), the_amp(amp), the_bkg(bkg), the_sigma(sigma) {}
      ~AppearanceFunction() {}
      double c() const {return the_peak+the_offset;}
      double dm() const {return the_dmatch; }
      double gr() const {return the_gammar;}
      double gb() const {return the_gammab;}
      double bg() const {return the_bkg;}
      double sg() const {return the_sigma;}
      double alpha3() const {return dm()/gb();}
//      double a3() const {return pow(alpha3(), alpha3())*exp((-dm()-sg()*sg()/(2*gb()))/gb());}
      //TRandom gRand= TRandom(571950); //sim
      // TRandom gRand= TRandom(57195);
      // TRandom gRand= TRandom(57208);
      // TRandom gRand=TRandom(572080); //sim
      //TRandom gRand=TRandom(57181); 
      TRandom gRand=TRandom(68871);
      // TRandom gRand=TRandom(571810); //sim
     double operator()(double x) const {
//       return(the_amp/2*(exp((x-c()-sg()*sg()/(2*gr()))/gr())*(1+TMath::Erf((c()-x-sg()*sg()/gr())/(sqrt(2)*sg())))+(((x<dm()+c()))?(exp((c()-x-sg()*sg()/(2*gb()))/gb())):(a3()*pow((x-c()/gb()),-alpha3())))*(1+TMath::Erf((x-c()-sg()*sg()/gb())/(sqrt(2)*sg()))))+bg());
       return(the_amp/2*(exp((x-c()+sg()*sg()/(2*gr()))/gr())*(1+TMath::Erf((c()-x-sg()*sg()/gr())/(sqrt(2)*sg())))+(((x<dm()+c()))?(exp((c()-x+sg()*sg()/(2*gb()))/gb())):(pow(alpha3(), alpha3())*exp((-dm()+sg()*sg()/(2*gb()))/gb())*pow((x-c())/gb(),-alpha3())))*(1+TMath::Erf((x-c()-sg()*sg()/gb())/(sqrt(2)*sg()))))+bg());     }
    private:
      double the_peak;
      double the_dmatch;// use for dmatch
      double the_gammar;
      double the_gammab;
      double the_amp;
      double the_bkg;
      double the_sigma;
      double the_offset=gRand.Uniform(-5.,5.);
      //      double the_offset=0.;
    };
  }
}
#endif // MN_AppearanceFunction_H_
