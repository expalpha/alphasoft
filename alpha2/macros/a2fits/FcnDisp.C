// @(#)root/minuit2:$Id$
// Template Authors: M. Winkler, F. James, L. Moneta, A. Zsenei   2003-2005
// Art Olin 2024 likelihood to lineshape & kickout projections
// prob with CHI==1
/**********************************************************************
 *                                                                    *
 * Copyright (c) 2005 LCG ROOT Math team,  CERN/PH-SFT                *
 *                                                                    *
 **********************************************************************/
//
#include "FcnLSKO.h"
#include "DisFunction.h"
#include <cassert>
#define CHI 0
double 	ROOT::TheFCN::operator() (const double*  par)  const {
      //std::cout<<"Starting minimization \n";
      DisFunction Dis(par[0],par[1],par[2],par[3],par[4],par[5],par[6],par[7],par[8],par[9],par[10],par[11],par[12],&_hlighttimes,&_hdarktimes,&_hpowersq,_freqs);
      //std::cout<<"lamd\t"<<kickoutd.lamd()<<" lamc\t"<<kickoutd.lamc()<<" N0\t"<<kickoutd.N0()<<" N_d0\t"<<kickoutd.fN_d(0)<<std::endl;
      TH2D* hfit=Dis.getfit();
      TH1D* hfitfreq= hfit->ProjectionX("hfitfreq",1,NRep);
      TH1D* hfitko=hfit->ProjectionY("hfitko",1,NFreq);
      TH2D* hdarkfit=Dis.getdarkfit();
      TH1D* hdarkfitfreq= hdarkfit->ProjectionX("hdarkfitfreq",1,NRep);
      TH1D* hdarkfitko=hdarkfit->ProjectionY("hdarkfitko",1,NFreq);
      Double_t cu,fu,cuc, fuc, cudark(0), fudark(0);
      Double_t loglikfreq(0),loglikko(0),loglikdfreq(0),loglikdko(0);
      Double_t gtestfreq(0),gtestko(0),gtestdfreq(0),gtestdko(0);
      for (int ifreq=1;ifreq<NFreq+1;ifreq++) {
        cu=hdatafreq.GetBinContent(ifreq);
        fu=hfitfreq->GetBinContent(ifreq);
#if CHI==0
        if (fu < 1.e-9) fu = 1.e-9;
        loglikfreq-=TMath::Log(TMath::PoissonI(cu,fu));
 #endif
  #if CHI==1
        cuc=hdatafreq.GetBinError(ifreq);
                 if (cuc>0.0001) 
          gtestfreq+=(cu-fu)*(cu-fu)/(cuc*cuc);
        //gtestfreq+=(cu>0)?2*cu*TMath::Log(cu/fu):0.;
#endif
      }
      //     loglikfreq=hfitfreq->Chi2Test(*hdatafreq,"UWCHI2");
      for ( int irep=1;irep<NRep+1;irep++) {
        cu=hdatako.GetBinContent(irep);
	     fu=hfitko->GetBinContent(irep);
#if CHI==0
        if (fu < 1.e-9) fu = 1.e-9;
        loglikko-=TMath::Log(TMath::PoissonI(cu,fu));
   #endif
  #if CHI==1
        cuc=hdatako.GetBinError(irep);
            if (cuc>0.0001) 
          gtestko+=(cu-fu)*(cu-fu)/(cuc*cuc);
          //        gtestko+=(cu>0)?2*cu*TMath::Log(cu/fu):0.;
   #endif      
      }  
      if(SIM==0) {
        for (int ifreq=1;ifreq<NFreq+1;ifreq++) {
          cu=hdarkdatafreq.GetBinContent(ifreq);
          fu=hdarkfitfreq->GetBinContent(ifreq);
  #if CHI==0
        if (fu < 1.e-9) fu = 1.e-9;
          loglikdfreq-=TMath::Log(TMath::PoissonI(cu,fu));
  #endif
  #if CHI==1
           cuc=hdarkdatafreq.GetBinError(ifreq);
            if (cuc>0.0001) 
          gtestdfreq+=(cu-fu)*(cu-fu)/(cuc*cuc);       
            //         gtestdfreq+=(cu>0)?2*cu*TMath::Log(cu/fu):0.;
#endif
        }
        for ( int irep=1;irep<NRep+1;irep++) {
          cu=hdarkdatako.GetBinContent(irep);
          fu=hdarkfitko->GetBinContent(irep);
 #if CHI==0
          if (fu < 1.e-9) fu = 1.e-9;
          loglikdko-=TMath::Log(TMath::PoissonI(cu,fu));
#endif
 #if CHI==1
          cuc=hdarkdatako.GetBinError(irep);
            if (cuc>0.0001) 
          gtestdko+=(cu-fu)*(cu-fu)/(cuc*cuc);       
          //         gtestdko+=(cu>0)?2*cu*TMath::Log(cu/fu):0.;
 #endif
        }
      }
      hfit->Delete();
      hdarkfit->Delete();
 #if CHI==0
      return loglikfreq + loglikko+(loglikdfreq+loglikdko);
#endif
 #if CHI==1
      return gtestfreq+gtestko +gtestdfreq+gtestdko;
#endif      
   }
