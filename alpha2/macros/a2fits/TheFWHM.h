//This class instantiates an lineshape width minimizer
// Calculates sqr(lsko.lineshape(p1)-0.5*lsko.lineshape(0)) + sqr(lsko.lineshape(p2)-0.5*lsko.lineshape(0)) 
#ifndef MN_TheFWHM_H_
#define MN_TheFWHM_H_
#include <TROOT.h>
#include <iostream>
#include <math.h>
#include <TMath.h>
namespace ROOT {
  class TheFWHM {
    Double_t  _gammar;
    Double_t _gammab;
    Double_t _sigma;
    Double_t _dmatch;
    //  double theErrorDef;
  public:
    TheFWHM( const Double_t  gammar, const Double_t gammab,
             const Double_t sigma, const Double_t dmatch) : _gammar(gammar),_gammab(gammab),_sigma(sigma),_dmatch(dmatch)
    {           }
  
    TheFWHM() {}
    ~TheFWHM(){}
    virtual double operator()(const double* par) const;
    double DoEval(const double* x)
    {
      return this->operator()(x); // not elegant
    }
     double alpha3() const {return _dmatch/_gammab;}
  double lineshape(double x)  const {
    return(0.5*(exp((x+_sigma*_sigma/(2*_gammar))/_gammar)*(1+TMath::Erf((-x-_sigma*_sigma/_gammar)/(sqrt(2)*_sigma)))+( (x<_dmatch)?(exp((-x+_sigma*_sigma/(2*_gammab))/_gammab)):(pow(alpha3(), alpha3())*exp((-_dmatch+_sigma*_sigma/(2*_gammab))/_gammab)*pow(x/_gammab,-alpha3()))) *(1+TMath::Erf((x-_sigma*_sigma/_gammab)/(sqrt(2)*_sigma)) ) ));     };
   
  private:
  };
}
#endif //MN_TheFWHM_H_
