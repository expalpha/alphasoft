#ifndef MN_TheFcn_H_
#define MN_TheFcn_H_
#include "Minuit2/FCNBase.h"
#include <vector>
namespace ROOT {
  namespace Minuit2 {
    class TheFcn : public  FCNBase {
    public:
      TheFcn(const std::vector<double>& measd,
             const std::vector<double>& measc,
             const std::vector<double>& measdark,
             const std::vector<double>& mvard,
             const std::vector<double>& mvarc,
             const std::vector<double>& mvardark,
             const std::vector<double>& darkfrac):
        theMeasd(measd),theMeasc(measc),theMeasdark(measdark),theMvard(mvard),
        theMvarc(mvarc), theMvardark(mvardark), thedarkfrac(darkfrac), theErrorDef(0.5) {}
      TheFcn(){}

      ~TheFcn() {}
      void SetNSets(int ns)  {NSets=ns;}
      void SetNRep(int nrep)  {NRep=nrep;}
      int GetNRep() const {return NRep;} 
      //void SetDarkfrac(double *dfrac) {darkfrac[0]=dfrac[0];darkfrac[1]=dfrac[1];}
      double GetDarkfrac( int i) const {return thedarkfrac.at(i);}
      // double GetGamma() {return gamma;}
      virtual double Up() const {return theErrorDef;}
      virtual double operator()(const std::vector<double>&) const;
      std::vector<double> Measd() const {return theMeasd;}
      std::vector<double> Measc() const {return theMeasc;}
      std::vector<double> Mvard() const {return theMvard;}
      std::vector<double> Mvarc() const {return theMvarc;}
      std::vector<double> Measdark() const {return theMeasdark;}
      std::vector<double> Mvardark() const {return theMvardark;}
      void setErrorDef(double def) {theErrorDef = def;}
    private:
      std::vector<double> theMeasd;
      std::vector<double> theMeasc;
      std::vector<double> theMvard;
      std::vector<double> theMvarc;
      std::vector<double> theMeasdark;
      std::vector<double> theMvardark;
      std::vector<double> thedarkfrac;
       double theErrorDef;
	bool SIM=true;   //for sim files
      int NSets=2;
      // int NRep=299;
      //int NRep=258;   //for the fit 65700
      //int NRep=99;   //for the fit 57181
      //int NRep=99;    //for sim37
      //int NRep=79;    //for sim36rd
      int NRep=34;   //sim41 
    };
  }
}
#endif //MN_TheFcn_H_
