#ifndef Math_KolmoSpill_H_
#define Math_KolmoSpill_H_
#include "TAxis.h"
#include <TSystem.h>
#include <TF1.h>
#include <TH1D.h>
#include "Math/GoFTest.h"

Double_t KolmoSpill(const TH1 *h1, const TH1 *h2, Option_t *option) ;
#endif // Math_KolmoSpill_H_
