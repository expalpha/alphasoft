#ifndef MN_DisFunction_H_
#define MN_DisFunction_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom3.h>
#include <TH2D.h>
#include <iostream>
namespace ROOT { 
    class DisFunction {
      double _N0, _lamd,  _lamg, _lamg1, _bkg, _leak,  _gammad,_peak,
        _gammar,_gammab, _sigma,_rncd, _dmatch;
      double N_d, N_c;
  TRandom gRand=TRandom(68871);
      double _offset=gRand.Uniform(-5.,5.);
     public:
double c() const {return _peak+_offset;}
double alpha3() const {return _dmatch/_gammab;}
      double lineshape(double x) {
        /* return(0.5*(exp((x-c()+_sigma*_sigma/(2*_gammar))/_gammar)*(1+TMath::Erf((c()-x-_sigma*_sigma/_gammar)/(sqrt(2)*_sigma)))+( (x<_dmatch+c())?(exp((c()-x+_sigma*_sigma/(2*_gammab))/_gammab)):(pow(alpha3(), alpha3())*exp((-_dmatch+_sigma*_sigma/(2*_gammab))/_gammab)*pow((x-c())/_gammab,-alpha3())))*(1+TMath::Erf((x-c()-_sigma*_sigma/_gammab)/(sqrt(2)*_sigma)) ) )); */
        return(1.0-_dmatch*TMath::Gaus(x,c(),_sigma));
      }
      TH2D* hDisfit,  *hDarkfit;
      DisFunction(double N0, double lamd, double lamg, double lamg1, double bkg, double leak,  double gammad,double peak, double gammar,double gammab, double sigma, double rncd, double dmatch, const TH2D* lighttimes, const TH2D* darktimes, const TH2D* powersq,const Double_t* Freq) :  _N0(N0),_lamd(lamd), _lamg(lamg)  , _lamg1(lamg1), _bkg(bkg) ,  _leak(leak), _gammad(gammad), _peak(peak), _gammab(gammab), _gammar(gammar), _sigma(sigma), _rncd(rncd),_dmatch(dmatch)
      {
        // std::cout<<"DisFunction init ";
       const Int_t NFreq=lighttimes->GetNbinsX();
      const Int_t NRep=lighttimes->GetNbinsY();
      Int_t NSpills=NFreq*NRep;
      hDisfit=new TH2D("hDisfit","lineshape/kickout fit", NFreq,-.5,NFreq-0.5,NRep,0.5,NRep+0.5);
      hDarkfit=new TH2D("hDarkfit","Dark lineshape/kickout fit",NFreq, -.5,NFreq-0.5,NRep,0.5,NRep+0.5);
      N_d=(_N0);
      N_c=(_N0*_rncd); 
        double N_dlast(_N0),N_clast(N_c);
        double ldmpd,ldmpc,ddcnt, darkcnt; //Events remaining at end of laser pulse and annihilations in the light or dark period
        double tau=lighttimes->GetBinContent(1,1) ;
        int freqbin(0),repbin(1);
        ldmpd=N_d*exp(-(_lamd*_gammad*lineshape(Freq[0])*powersq->GetBinContent (1,1)+_lamg+_lamg1)*tau) ;
        ldmpc=N_c*exp(-(_lamg+_lamg1)*tau);
        ddcnt=N_d -ldmpd+N_c   -ldmpc;
      N_d   =ldmpd*exp(-_lamg*darktimes->GetBinContent(1,1) )  ;
      N_c  =ldmpc*exp(-_lamg*darktimes->GetBinContent(1,1) ) ;
      darkcnt=N_dlast-N_d+N_clast-N_c-ddcnt;
      darkcnt+=ddcnt*_leak+_bkg*darktimes->GetBinContent(1,1);
      ddcnt=ddcnt*(1-_leak)+_bkg*tau ;
    hDisfit->SetBinContent(1,1,ddcnt);
    hDarkfit->SetBinContent(1,1,darkcnt);                      
    hDisfit->SetBinError(1,1,0.0);
    hDarkfit->SetBinError(1,1,0.0);   
     for (int ispill=1;ispill<NSpills;ispill++) {
      // here are the recurrence defs of N_d(ispill), N_c(ispill),dd(irep/2+1) ncc(irep/2) 
      repbin=ispill/NFreq+1;
      freqbin = repbin%2?(ispill%NFreq+1): (NFreq -ispill%NFreq);
      //      ldmpd=N_d*exp(-(_lamd*(pow(ispill,_gammad)-pow(ispill-1,_gammad))*lineshape(Freq[freqbin-1])*powersq->GetBinContent (freqbin,repbin)+_lamg)*lighttimes->GetBinContent(freqbin,repbin)  ) ;
      tau=lighttimes->GetBinContent(freqbin,repbin);
      ldmpd=N_d*exp(-(_lamd*_gammad*pow(ispill*tau,_gammad-1)*lineshape(Freq[freqbin-1])*powersq->GetBinContent (freqbin,repbin)+_lamg+_lamg1)*tau  ) ;
      ldmpc=N_c*exp(-(_lamg+_lamg1)*tau);
      ddcnt=N_d-ldmpd+N_c-ldmpc;
      N_dlast=N_d;
      N_clast=N_c;
      N_d=ldmpd*exp(-_lamg*darktimes->GetBinContent(freqbin,repbin));
      N_c=ldmpc*exp(-_lamg*darktimes->GetBinContent( freqbin,repbin)) ;       
      darkcnt=N_dlast-N_d+N_clast-N_c-ddcnt;
     darkcnt+=ddcnt*_leak+bkg*darktimes->GetBinContent(freqbin,repbin) ;
      ddcnt=ddcnt*(1-_leak)+_bkg*tau  ;
      hDisfit->SetBinContent(freqbin, repbin,ddcnt);
      hDarkfit->SetBinContent(freqbin, repbin,darkcnt);   
       hDisfit->SetBinError(freqbin, repbin,0.0);
      hDarkfit->SetBinError(freqbin, repbin,0.0);   
   }
//std::cout<<std::endl;
      }
~DisFunction(){}

double N0() const {return _N0;}
double lamd() const {return _lamd;}
double lamg() const {return _lamg;}
double lamg1() const {return _lamg1;}
double bkg() const {return _bkg;}
double leak() const {return _leak;}
double rncd() const {return _rncd;}
double peak() const {return _peak;}
double gammar()  const {return _gammar;}
double gammab() const { return _gammab;}
double sigma() const { return _sigma;}
double remaind() const {return N_d;}
double remainc() const {return N_c;}
TH2D* getfit() const {return hDisfit; }
TH2D* getdarkfit() const {return hDarkfit; }
    

    private:

    };
  }
#endif // MN_DisFunction_H_
