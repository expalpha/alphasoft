#ifndef MN_TheFMIN_H_
#define MN_TheFMIN_H_
#include <vector>
#include <TROOT.h>
#include <TH1D.h>
#include <TH2D.h>
#include <iostream>
namespace ROOT {

     class TheFCN {
    TH2D _hdata;
      TH2D _hdarkdata;
      TH2D  _hlighttimes;
      TH2D _hdarktimes;
      TH2D  _hpowersq;
      const Double_t* _freqs;
      int SIM;
      Int_t NFreq,NRep;
      TH1D hdatafreq,hdatako,hdarkdatafreq,hdarkdatako;
  public:
       TheFCN(const TH2D& hdata,
             const TH2D& hdarkdata,
             const TH2D& hlighttimes,
             const TH2D& hdarktimes,
             const TH2D& powersq,
             const Double_t* freqs,
             const int sim ):
         SIM(sim){
      //     std::cout<<"SIM= "<<sim;
      double NEntries=hdata.GetEntries();
      // std::cout<<" NEntries "<< NEntries<<std::endl;
      _hdata=(TH2D)hdata;
       NEntries=_hdata.GetEntries();
       //      std::cout<<" NEntries1 "<< NEntries<<std::endl;
       _hdarkdata=(TH2D)hdarkdata;
      _hlighttimes=(TH2D)hlighttimes;
      _hdarktimes=(TH2D)hdarktimes;
      _hpowersq=(TH2D)powersq;
      _freqs=freqs;
     NFreq=_hdata.GetNbinsX();
      NRep=_hdata.GetNbinsY();
       std::cout<<"NREP: "<<NRep<<" NFREQ: "<<NFreq<<std::endl;
      TH1D* _hdatafreq= _hdata.ProjectionX("hdatafreq",1,NRep);
      TH1D*  _hdatako=_hdata.ProjectionY("hdatako",1,NFreq);
      TH1D* _hdarkdatafreq=_hdarkdata.ProjectionX("hdarkdatafreq",1,NRep);
      TH1D* _hdarkdatako=_hdarkdata.ProjectionY("hdarkdatako",1,NFreq);
      hdatafreq =TH1D( *_hdatafreq);
      hdatako =  TH1D( *_hdatako);
      hdarkdatafreq = TH1D( *_hdarkdatafreq);
      hdarkdatako =TH1D(  *_hdarkdatako);
      }
    
      TheFCN(){}

      ~TheFCN() {}
      void SetNRep(int nrep)  {NRep=nrep;}
      int GetNRep() const {return NRep;} 
       virtual double operator()(const double* par) const;
    double DoEval(const double* x)
   {
      return this->operator()(x); // not elegant
   }
   private:


    };

  
}
#endif //MN_TheFMIN_H_
