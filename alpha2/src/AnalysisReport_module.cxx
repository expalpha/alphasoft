//
// AnalysisReport_module.cxx
//
// AnalysisReport of all modules
//
// Joseph McKenna
//


#include <stdio.h>
#include <iomanip>

#include "manalyzer.h"
#include "midasio.h"


#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"

#ifdef BUILD_A2
#include "A2Flow.h"
#endif

#include "AnalysisFlow.h"

#include "TA2AnalysisReport.h"

//I am intentionally global, external modules test this
bool TimeModules=true;
#ifdef BUILD_A2
class A2DumpSummary
{
public:
   std::string DumpName;
   int PassedCuts;   double PassedCuts_sqsum;
   int fVerticies;    double Verticies_sqsum;
   int fVF48Events;   double VF48Events_sqsum;
   double time;
   int TotalCount;
   A2DumpSummary(const char* name)
   {
      DumpName=name;
      PassedCuts      =0;
      PassedCuts_sqsum=0;
      fVerticies       =0;
      Verticies_sqsum =0;
      fVF48Events      =0;
      VF48Events_sqsum=0;
      time=0.;
      TotalCount=0;
   }
   void Fill(TA2Spill* s)
   {
      if (!s->fScalerData)
      {
         std::cout<<"Error: Spill has no scaler data to fill!"<<std::endl;
         return;
      }
      //std::cout<<"Adding spill to list"<<std::endl;
      PassedCuts       += s->fScalerData->fPassCuts;
      PassedCuts_sqsum += s->fScalerData->fPassCuts * s->fScalerData->fPassCuts;

      fVerticies        += s->fScalerData->fVerticies;
      Verticies_sqsum  += s->fScalerData->fVerticies * s->fScalerData->fVerticies;

      fVF48Events       += s->fScalerData->VertexEvents;
      VF48Events_sqsum += s->fScalerData->VertexEvents * s->fScalerData->VertexEvents;

      time             += s->fScalerData->fStopTime-s->fScalerData->fStartTime;
      TotalCount++;
   }
   double calc_stdev(double sq_sum, int scaler)
   {
      double mean= (double) scaler / (double) TotalCount;
      double variance = sq_sum / TotalCount - mean * mean;
      return sqrt(variance);
   }
   /*void Print()
   {
      printf("DUMP SUMMARY:%s\t DumpCount: %d  \t fVF48Events: %d ( %f )\tVerticies: %d ( %f )\t PassedCuts: %d ( %f )\t TotalTime: %f\t\n",
                   DumpName.c_str(),
                   TotalCount,
                   fVF48Events, calc_stdev(VF48Events_sqsum, fVF48Events),
                   fVerticies, calc_stdev(Verticies_sqsum, fVerticies), 
                   PassedCuts, calc_stdev(PassedCuts_sqsum, PassedCuts),
                   time);
   }*/
   void Print()
    {
      std::streamsize ss = std::cout.precision();
       std::cout<<"DUMP SUMMARY: "<< DumpName.c_str() << "\t";
       std::cout<<"DumpCount: "   << TotalCount << "\t";
       std::cout<<"fVF48Events: "  << fVF48Events << "\t";
       std::cout<<"fVerticies: "   << fVerticies << " (" << std::setprecision(3) << 100.*fVerticies/fVF48Events << "% / "  << fVerticies/time <<"Hz)\t";
       std::cout<<"PassedCuts: "  << PassedCuts << " (" << std::setprecision(3) << 100.*PassedCuts/fVF48Events << "% / " << PassedCuts/time <<"Hz)\t";
       std::cout<<"TotalTime: "   << std::setprecision(ss) << time << std::endl;
    }
};
#endif
#ifdef BUILD_A2
class A2DumpSummaryList
{
public:
   std::vector<A2DumpSummary*> list;
   A2DumpSummaryList() {}
   ~A2DumpSummaryList()
   {
      const int size=list.size();
      for (int i=0; i<size; i++)
         delete list[i];
      list.clear();
   }
   
   void TrackDump(const char* d)
   {
      list.push_back(new A2DumpSummary(d));
      return;
   }
   void Fill(TA2Spill* s)
   {
      //std::cout<<"Filling list "<<s->Name.c_str()<<std::endl;
      const int size=list.size();
      if (!size) return;
      for (int i=0; i<size; i++)
      {
         //std::cout<<list[i]->DumpName.c_str()<<"vs"<<s->Name.c_str()<<std::endl;
         if (strcmp(list[i]->DumpName.c_str(),s->fName.c_str())==0)
            list[i]->Fill(s);
      }
      return;
   }
   void Print()
   {
      const int size=list.size();
      for (int i=0; i<size; i++)
         list[i]->Print();
   }
};
#endif



class AnalysisReportFlags
{
public:
   bool fPrint = false;
   TA2AnalysisReport* AnalysisReport = NULL;
   double last_event_ts=-1.; //Results from reco module
#ifdef BUILD_A2
   A2DumpSummaryList DumpLogs;
#endif
  
};

   
class AnalysisReportModule: public TARunObject
{
public:

   bool fTrace = false;
   bool fVersionReported = false;

   AnalysisReportFlags* fFlags;

   AnalysisReportModule(TARunInfo* runinfo, AnalysisReportFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="AnalysisReport";
#endif
      if (fTrace)
         printf("AnalysisReportModule::ctor!\n");
      fFlags->AnalysisReport = NULL;
      if (!getenv("AGRELEASE"))
      {
         std::cerr<<"AGRELEASE not set! Did you mean to 'source agconfig.sh'?"<<std::endl;
         exit(1);
      }
   }

   ~AnalysisReportModule()
   {
      if (fTrace)
         printf("AnalysisReportModule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if (fFlags->fPrint)
         printf("AnalysisReportModule::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      gDirectory->mkdir("AnalysisReport")->cd();
      fFlags->AnalysisReport = new TA2AnalysisReport(runinfo->fRunNo);
      uint32_t midas_start_time = -1 ;
      #ifdef INCLUDE_VirtualOdb_H
      midas_start_time = runinfo->fOdb->odbReadUint32("/Runinfo/Start time binary", 0, 0);
      #endif
      #ifdef INCLUDE_MVODB_H
      runinfo->fOdb->RU32("Runinfo/Start time binary",(uint32_t*) &midas_start_time);
      #endif
      fFlags->AnalysisReport->SetStartTime(midas_start_time);


   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("AnalysisReportModule::EndRun, run %d\n", runinfo->fRunNo);
      uint32_t midas_stop_time = -1;
      #ifdef INCLUDE_VirtualOdb_H
      midas_stop_time = runinfo->fOdb->odbReadUint32("/Runinfo/Stop time binary", 0, 0);
      #endif
      #ifdef INCLUDE_MVODB_H
      runinfo->fOdb->RU32("Runinfo/Stop time binary",(uint32_t*) &midas_stop_time);
      #endif
      runinfo->fRoot->fOutputFile->cd("AnalysisReport");
      fFlags->AnalysisReport->SetStopTime(midas_stop_time);
      //Fill internal containers with histogram data
      fFlags->AnalysisReport->Flush();

      //Do the tree writing... I only have one report... so 
      TTree* t=new TTree("AnalysisReport","AnalysisReport");
      t->Branch("TA2AnalysisReport","TA2AnalysisReport",&fFlags->AnalysisReport,32000,0);
      t->Fill();
      t->Write();
      delete t;
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("AnalysisReportModule::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("AnalysisReportModule::ResumeRun, run %d\n", runinfo->fRunNo);
   }

   TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow)
   {
      if (fTrace)
         printf("AnalysisReportModule::Analyze, run %d, event %p, flags %p, flow %p\n", runinfo->fRunNo, (void*) event, (void*) flags, (void*) flow);
      if (!fVersionReported)
      {
         fVersionReported = true;
         const std::string VersionLine = fFlags->AnalysisReport->GetVersionLine();
         TInfoSpill* versionSpill = new TInfoSpill(fFlags->AnalysisReport->GetRunNumber(), fFlags->AnalysisReport->GetRunStartTime(), fFlags->AnalysisReport->GetRunStartTime(), VersionLine.c_str());
         TInfoSpillFlow* f = new TInfoSpillFlow(flow);
         f->spill_events.push_back(versionSpill);
         return f;
      }
#ifdef HAVE_MANALYZER_PROFILER
         *flags |= TAFlag_SKIP_PROFILE;
#endif
      return flow;
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      if (fTrace)
         printf("AnalysisReportModule::AnalyzeFlowEvent, run %d\n", runinfo->fRunNo);
      //Clocks unfold backwards... 
      if (!flow)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags |= TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }   

      std::vector<TAFlowEvent*> flowArray;
      int FlowEvents=0;
      TAFlowEvent* f = flow;
      while (f) 
      {
         flowArray.push_back(f);
         f=f->fNext;
         FlowEvents++;
      }
      for (int ii=FlowEvents-1; ii>=0; ii--)
      {
         f=flowArray[ii];
#ifdef BUILD_A2
         SilEventFlow* SilFlow = dynamic_cast<SilEventFlow*>(f);
         if(SilFlow)
         {
            TSiliconEvent* se=SilFlow->silevent;
            fFlags->AnalysisReport->FillSVD(
                se->GetNsideNRawHits(),
                se->GetPsideNRawHits(),
                //SVD_N_Clusters->Fill(se->GetNNClusters());
                //SVD_P_Clusters->Fill(se->GetNPClusters());
                se->GetNRawHits(),
                se->GetNHits(),
                se->GetNTracks(),
                se->GetNVertices(),
                se->GetPassedCuts(),
                se->GetVF48Timestamp()
                );
            //This is not proper occupancy... its a hit counters TODO JOE
            for (int i = 0; i < nSil; i++)
            {
               const TSiliconModule* m = se->GetSiliconModule(i);
               if (!m)
                  continue;
               if (!m->IsAHitModule())
                  continue;
               for (int j = 0; j < 4; j++)
               {
                  const TSiliconVA* v = m->GetASIC(j);
                  if (!v) continue;
                  if (v->IsAHitOR()) // see TSiliconVA L.220 and 242
                  {
                     if (v->IsAPSide())
                        fFlags->AnalysisReport->FillHybridPSideOccupancy(i);
                     else
                        fFlags->AnalysisReport->FillHybridNSideOccupancy(i);
                  }
               }
            }
         }
         const A2SpillFlow* SpillFlow= dynamic_cast<A2SpillFlow*>(f);
         if (SpillFlow)
         {
            for (size_t i=0; i<SpillFlow->spill_events.size(); i++)
            {
               TA2Spill* s=SpillFlow->spill_events.at(i);
               //s->Print();
               fFlags->DumpLogs.Fill(s);
            }
            continue;
         }
#endif
      }
      return flow;
   }
};

class AnalysisReportModuleFactory: public TAFactory
{
public:
   AnalysisReportFlags fFlags;
   void Usage()
   {
      printf("AnalysisReportModuleFactory::Help!\n");
      printf("\t--notime \tTurn off AnalysisReport module processing time calculation and summary\n");
      printf("\t--summarise NNN MMM OOO \t\tPrint SVD summary for NNN, MMM and OOO dumps (no limit on dumps to track)\n");
  
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("AnalysisReportModuleFactory::Init!\n");
      

      for (unsigned i=0; i<args.size(); i++) {
         //Ok, lets support both proper and american spellings
         if (args[i] == "--summarise" || args[i] == "--summarize")
         {
#ifdef BUILD_A2
             while (args[i+1].c_str()[0]!='-')
             {
                char dump[80];
                snprintf(dump,80,"\"%s\"",args[++i].c_str());
                fFlags.DumpLogs.TrackDump(dump);
                if (i+1==args.size()) break;
             }
#else
             std::cerr<<"--summarise feature only available for ALPHA2"<<std::endl;
#endif

         }
      }
   }

   void Finish()
   {
#ifdef BUILD_A2
      fFlags.DumpLogs.Print();
#endif
      if (fFlags.AnalysisReport)
      {
         fFlags.AnalysisReport->Print();
         fFlags.AnalysisReport->QODChecks();
      }
      //delete fFlags.AnalysisReport;
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("AnalysisReportModule::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new AnalysisReportModule(runinfo,&fFlags);
   }
};

static TARegister tar(new AnalysisReportModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
