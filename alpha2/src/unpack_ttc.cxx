//
// Module to unpack the TTC triggers in A2, and save to root file.
//
// L GOLINO
//

#include <stdio.h>

#include "manalyzer.h"
#include "midasio.h"
#include "TTree.h"
#include "A2Flow.h"
#include <sstream>
#include <vector>
#include <iterator>
#include "unistd.h"
#include "TTCEvent.h"
#include <iomanip>

#include <iostream>
#include <map>

#define TTC_TA_inputs 512
#define NUM_FPGA TTC_TA_inputs/128


class TTCUnpackerFlags
{
public:
   bool fPrint = false;
   bool fFakeRealtime = false;
};

class TTCUnpackerWriter
{
   private:
      TTree* fTree;
      TBranch* fDataBranch;
      TTCEvent* fEvent = nullptr;
      int fRunNo;
   

   private:
   public:
   TTCUnpackerWriter()
   {
      fEvent = new TTCEvent();
   }
   TTCUnpackerWriter(TARunInfo* runInfo) : fTree(nullptr), fDataBranch(nullptr), fEvent(nullptr)
   {
      fRunNo=runInfo->fRunNo;
   }
   void BranchTree(TARunInfo* runInfo)
   {
      runInfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      fTree = new TTree("TTCEvents","TTCEvents");
      fDataBranch = fTree->Branch("TTCEvent",&fEvent);
      SetRunNumber(runInfo->fRunNo);
   }
   void FillTree(TARunInfo* /* runInfo */)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      fTree->Fill();
   }
   void WriteFile()
   {
      fTree->Write();
   }
   void SetRunNumber(int runNumber)    { fRunNo = runNumber;}
   void SetEvent(TTCEvent* evt)      { fEvent = evt;    }

};

class TTCUnpacker: public TARunObject
{
private:
   TTCUnpackerWriter* writer;
   uint32_t fTTCEventNoverflow[4]={0};
   Bool_t   fTTCReadyToOverflow[4]={kFALSE};
   int32_t  fTTCEventID=0;
   TTCEvent fEvent;
   int EventNo;
   double EventTime;
   int fRunNo;
   uint32_t fInitialEventTime;

public:
   TTCUnpackerFlags* fFlags;
   bool fTrace = false;
   std::vector<TTree*> fTrees;
   const double kUnixTimeOffset = 2082844800;
   
   TTCUnpacker(TARunInfo* runinfo, TTCUnpackerFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="TTCUnpacker Module";
#endif
      if (fTrace)
         printf("TTCUnpacker::ctor!\n");
   }

   ~TTCUnpacker()
   {
      if (fTrace)
         printf("TTCUnpacker::dtor!\n");
   }

   void BeginRun(TARunInfo* runInfo)
   {
      runInfo->fRoot->fOutputFile->cd();
      if (fTrace)
         printf("TTCUnpacker::BeginRun, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
      writer = new TTCUnpackerWriter(runInfo);
      writer->BranchTree(runInfo);

    }

   void EndRun(TARunInfo* runInfo)
   {
      if (fTrace)
         printf("TTCUnpacker::EndRun, run %d\n", runInfo->fRunNo);
      writer->WriteFile();
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /* runInfo */, TAFlags* /* flags */, TAFlowEvent* flow)
  {
      VF48EventFlow* fe=flow->Find<VF48EventFlow>();
      if(fe)
      {
         if(fe->vf48event)
         {

            EventNo = fe->vf48event->eventNo;
            EventTime = fe->vf48event->timestamp;
            if (fTrace)
            {
               std::cout << "FOUND VF48 EVENT\n";
               std::cout << "fe->vf48event->eventNo = " << fe->vf48event->eventNo << "\n"; 
               std::cout << "fe->vf48event->timestamp = " << fe->vf48event->timestamp << "\n"; 
               //std::cout << "\n\n\n\n";
            }
         }
         else
         {
            if (fTrace) std::cout << "Not found VF48 EVENT\n";
         }
      }
      else
      {
         if (fTrace) std::cout << "Not found flow \n";
      }
     /*{
         //TTCUnpackerEvent* flowEvent = flow->Find<TTCUnpackerEvent>();
         if(flowEvent == 0x0)
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags |= TAFlag_SKIP_PROFILE;
#endif
            //printf("DEBUG: TTCUnpacker::AnalyzeFlowEvent has recieved a standard  TAFlowEvent. Returning flow and not analysing this event.\n");
            return flow;
         }
         if (fTrace)
            printf("TTCUnpacker::AnalyzeFlowEvent, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());

         fTreeWriter.SaveToTree(runInfo, flowEvent);
      }*/
      return flow; 
   }


   TTCEvent* HandleTTC(int sizettc, uint16_t* ptrttc, int ifpga)
   {
      const int kBufsize = 100;
      static int wptr;
      static uint16_t buf[kBufsize];
      static uint32_t xtime;
      static uint32_t xevent;

      int size = sizettc;
      const uint16_t* ptr = ptrttc;

      if (0)  //Prints the raw TTC event
      {
         printf("FPGA %d, size %d, data 0x", ifpga, size);
         for (int i = 0; i < size; i++)
            printf(" %04x", ptr[i]);
         printf("\n");
      }

      for (int i = 0; i < size; i++)
      {
         buf[wptr++] = ptr[i];
         if (ptr[i] == 0xaaaa) 
         {
            int j = 0;
            for (; j < wptr; j++)
               if (buf[j] == 0x5555)
                     break;
   
            j++;
            
            TTCEvent* ttcEvent = new TTCEvent(); 

            // I assume sequential TTCEvents with non missing
            // if  (buf[ifpga][j+0]==65535) fTTCReadyToOverflow[ifpga]=kTRUE;
            // if (buf[ifpga][j+0]==0 && fTTCReadyToOverflow[ifpga])
            // {
            //     cout << "Run:"<< gRunNumber<<"Overflowing "<<ifpga <<endl;
            //     fTTCEventNoverflow[ifpga]++;
            //     fTTCReadyToOverflow[ifpga]=kFALSE;
            // }

            // I may be a future bugfix, if there are missig TTC events, this *should* catch overflows properly
            if  (buf[j+0] > 60000 && buf[j+0] < 62000 && !fTTCReadyToOverflow[ifpga]) 
               fTTCReadyToOverflow[ifpga] = kTRUE;
            if (buf[j+0] > 0 && buf[j+0] < 1000 && fTTCReadyToOverflow[ifpga])
            {
               std::cout << "Overflowing " << ifpga << std::endl;
               fTTCEventNoverflow[ifpga]++;
               fTTCReadyToOverflow[ifpga] = kFALSE;
            }

            ttcEvent->SetTTCCounter(((65536) * (fTTCEventNoverflow[ifpga])) + ((uint32_t)buf[j+0]));
            //cout<<ttcEvent->GetTTCCounter() <<endl;
            ttcEvent->SetTTCTimestamp(buf[j+1] | (buf[j+2] << 16));

            for (int k = 0; k < 8; k++) 
               ttcEvent->SetLatch(buf[j+4+k], k);

            ttcEvent->SetMult(buf[j+12]);
            ttcEvent->SetMult2(buf[j+13]);

            if (/*xevent[ifpga]==0 ||*/ ttcEvent->GetTTCCounter() < xevent)
               xevent = ttcEvent->GetTTCCounter();

            ttcEvent->SetTTCNEvent(ttcEvent->GetTTCCounter() - xevent);

            if (xtime == 0 || ttcEvent->GetTTCTimestamp() < xtime)
               xtime = ttcEvent->GetTTCTimestamp();

            ttcEvent->SetEventTime((ttcEvent->GetTTCTimestamp() - xtime) / 20.0e6); // convert 20 MHz clock into seconds.
            ttcEvent->SetFPGA(ifpga);
            ttcEvent->SetID(fTTCEventID);
            fTTCEventID++;
            //fTTCEvents->Add(ttcEvent);

            wptr = 0;
            //ttcEvent->Print();
            return ttcEvent;
         } 
      }
      return new TTCEvent();
   }


   TAFlowEvent* Analyze(TARunInfo* runInfo, TMEvent* midasEvent, TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      /*if(midasEvent->event_id != 12)
      {  //No work done... skip profiler
#ifdef HAVE_MANALYZER_PROFILER
         *flags |= TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }*/
      if (fTrace)
         printf("TTCUnpacker::AnalyzeFlowEvent, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
      
      midasEvent->FindAllBanks();

      void* ptrttc;
      int sizettc;
      int ifpga;

      bool haveTTCData = false;
      for (int i=0; i<NUM_FPGA; i++) 
      {
         char bankname[5];
         bankname[0] = 'T';
         bankname[1] = 'E';
         bankname[2] = 'L';
         bankname[3] = '0' + i;
         bankname[4] = 0;
         if (midasEvent->FindBank(bankname))
         {
            //std::cout << "FOUND BANK DATA. EVENT ID = " << midasEvent->event_id << " in bank: " << bankname << "\n";
            haveTTCData = true;
            //break;
         }
         TMBank* ttcBank = midasEvent->FindBank(bankname); 
         if (!ttcBank) continue;
         int size=ttcBank->data_size/2;
         uint16_t* rawMIDASData = (uint16_t*)midasEvent->GetBankData(ttcBank);
         ptrttc = rawMIDASData;
         sizettc = size;
         ifpga = i;

         TTCEvent* event = nullptr;

         if ( haveTTCData ){ // Don't bother with TTC when searching for si events
            event = HandleTTC(sizettc,(uint16_t*) ptrttc, ifpga); 
            if (fTrace)
               std::cout << "Built TTC event. EventNo: " << event->GetTTCNEvent() << ". "
                         << "EventID: " << event->GetID() <<". "
                         << "TTCCounter: " << event->GetTTCCounter() <<". "
                         << "TTCTimestamp: " << event->GetTTCTimestamp() <<". "
                         << "RunTime: " << event->GetRunTime() <<". "
                         << "TSRunTime: " << event->GetTSRunTime() <<". "
                         << "EventTime: " << event->GetEventTime() <<". "
                         << "ExptTime: " << event->GetExptTime() <<". "
                         << "EventTime: " << event->GetEventTime() <<". "
                         << "VertexCounter: " << event->GetVertexCounter() <<". "
                         << "LabVIEWCounter: " << event->GetLabVIEWCounter() <<". "
                         << "SISCounter: " << event->GetSISCounter() <<". \n";
            //std::cout << "Print TTC event:\n";
            if(event->GetTTCNEvent() == (uint32_t)EventNo)
               event->SetRunTime(EventTime);
            else
               if (fTrace) std::cout << "Warning, couldn't catch event time from the flow\n";
            //event->Print();
         }

         if(event)
         {
            writer->SetEvent(event);
            //Writes the tree.
            writer->FillTree(runInfo);
         }
      }
      
      return flow;
   }

   void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event)
   {
       if (fTrace)
          printf("TTCUnpacker::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", 
                 runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
   }
};

class TTCUnpackerFactory: public TAFactory
{
public:
   TTCUnpackerFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("TTCUnpackerFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
         if (args[i] == "--fakerealtime")
            fFlags.fFakeRealtime = true;
      }
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("TTCUnpackerFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("TTCUnpackerFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new TTCUnpacker(runinfo, &fFlags);
   }
};

static TARegister tar(new TTCUnpackerFactory);
