//
// A2 offline MVA application
//
// L GOLINO
//


//Turning this whole mother off
#include <stdio.h>

#include "manalyzer.h"
#include "midasio.h"

#include "A2Flow.h"

#include "TApplication.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"

#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/RReader.hxx"

class OfflineA2MVAFlags
{
public:
   bool fPrint;
   bool fUseMVA;
   std::string fXMLModel;

   OfflineA2MVAFlags():fPrint(false),fUseMVA(false)
   {
      fXMLModel = getenv("AGRELEASE");
      fXMLModel += "/MVA/A2OnlineMVA/dataset/weights/A2TMVAClassification_BDTF.weights.xml";
   }
};


class OfflineA2MVA: public TARunObject
{
private:
   //ReadBDTF* r;
   std::string fMethodName;
   TString gdir;
   double grfcut;

   TMVA::Reader *reader;
   float nhits,residual,r,S0rawPerp,S0axisrawZ,phi_S0axisraw,nCT,nGT,tracksdca,curvemin;
   float curvemean,lambdamin,lambdamean,curvesign,phi, vStatus;
   float fX, fY, fZ;
   //vector ;
   
   std::vector<std::string> input_vars;
   std::vector<double>      input_vals;
  //TString gVarList="nhits,residual,r,S0rawPerp,S0axisrawZ,phi_S0axisraw,nCT,nGT,tracksdca,curvemin,curvemean,lambdamin,lambdamean,curvesign,";
  
public:
   OfflineA2MVAFlags* fFlags;
   bool fTrace = false;
   
   
   OfflineA2MVA(TARunInfo* runinfo, OfflineA2MVAFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef MANALYZER_PROFILER
      ModuleName="Online MVA Module";
#endif
      if (fTrace)
         printf("OnlineMVA::ctor!\n");
      
      //~4mHz Background (42% efficiency)
      grfcut=0.398139;
      //45mHz Background (72% efficiency)
      //grfcut=0.230254;
      //100mHz Background (78% efficiency)
      //grfcut=0.163; 
   }

   ~OfflineA2MVA()
   {
      if (fTrace)
         printf("OnlineMVA::dtor!\n");
      delete reader;
   }
   
   
   void BeginRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("OnlineMVA::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());

      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory

      reader = new TMVA::Reader();

      TMVA::Experimental::RReader* rreader = new TMVA::Experimental::RReader(fFlags->fXMLModel);
      for(std::string variable: rreader->GetVariableNames())
      {
         std::cout << "Adding variable " << variable << " to the TMVA::Reader\n";
         if(variable == "X") reader->AddVariable("X", &fX);
         if(variable == "Y") reader->AddVariable("Y", &fY);
         if(variable == "Z") reader->AddVariable("Z", &fZ);
         if(variable == "nhits") reader->AddVariable("nhits", &nhits);      
         if(variable == "residual") reader->AddVariable("residual", &residual);
         if(variable == "r") reader->AddVariable("r", &r);
         if(variable == "S0rawPerp") reader->AddVariable("S0rawPerp", &S0rawPerp);
         if(variable == "S0axisrawZ") reader->AddVariable("S0axisrawZ", &S0axisrawZ);
         if(variable == "phi_S0axisraw") reader->AddVariable("phi_S0axisraw", &phi_S0axisraw);
         if(variable == "nCT") reader->AddVariable("nCT", &nCT);
         if(variable == "nGT") reader->AddVariable("nGT", &nGT);
         if(variable == "tracksdca") reader->AddVariable("tracksdca", &tracksdca);
         if(variable == "curvemin") reader->AddVariable("curvemin", &curvemin);
         if(variable == "curvemean") reader->AddVariable("curvemean", &curvemean);
         if(variable == "lambdamin") reader->AddVariable("lambdamin", &lambdamin);
         if(variable == "lambdamean") reader->AddVariable("lambdamean", &lambdamean);
         if(variable == "curvesign") reader->AddVariable("curvesign", &curvesign);
         if(variable == "phi") reader->AddVariable("phi", &phi);
         if(variable == "vStatus") reader->AddVariable("vStatus", &vStatus);
         //These are not implemented correctly yet.
         //if(variable == "phi") dataloader->AddVariablesArray("fSiliconModules", fNumSil*fNumASICs*fNumChannels, 'F', 0., 999.);
         //if(variable == "phi") dataloader->AddVariablesArray("fSiliconModulesRMS", fNumSil*fNumASICs*fNumChannels, 'F', 0., 999.);
      }

      std::string foldername = fFlags->fXMLModel;
      std::string methodname = foldername.substr( foldername.find_last_of("_") + 1, 10);
      methodname = methodname.substr( 0, methodname.find_first_of("."));
      fMethodName = methodname;

      reader->BookMVA( methodname.c_str(),  foldername.c_str() );
   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("OnlineMVA::EndRun, run %d\n", runinfo->fRunNo);
   }
   
   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("OnlineMVA::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }
  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /* runinfo */, TAFlags* /* flags */, TAFlowEvent* flow)
   {
      A2OnlineMVAFlow* dumper_flow=flow->Find<A2OnlineMVAFlow>();
      if (!dumper_flow)
      {
#ifdef MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      OnlineMVAStruct* OnlineVars=dumper_flow->dumper_event;

      //    "phi_S0axisraw", "S0axisrawZ", "S0rawPerp", "residual", "nhits", "phi", "", "nCT", "nGT"
      nCT               = OnlineVars->nCT;
      nGT               = OnlineVars->nGT;
      nhits             = OnlineVars->nhits;
      residual          = OnlineVars->residual;
      r                 = OnlineVars->r;
      S0rawPerp         = OnlineVars->S0rawPerp;
      S0axisrawZ        = OnlineVars->S0axisrawZ;
      phi_S0axisraw     = OnlineVars->phi_S0axisraw;
      nCT               = OnlineVars->nCT;
      nGT               = OnlineVars->nGT;
      tracksdca         = OnlineVars->tracksdca;
      curvemin          = OnlineVars->curvemin;
      curvemean         = OnlineVars->curvemean;
      lambdamin         = OnlineVars->lambdamin;
      lambdamean        = OnlineVars->lambdamean;
      curvesign         = OnlineVars->curvesign;
      phi               = OnlineVars->phi;
      fX                = OnlineVars->X;
      fY                = OnlineVars->Y;
      fZ                = OnlineVars->Z;
      vStatus           = OnlineVars->vStatus;

      double rfout = reader->EvaluateMVA( fMethodName );

      //did you use a cut in training? Apply it now...
      if(nCT<=0 && nGT<=0)
      {
         //FAILED CUT
         //std::cout << "failed cut, what happens if we mva it? " << rfout << "\n";
         dumper_flow->rfout=-99;
         dumper_flow->pass_online_mva=(rfout>grfcut);
         return flow;
      }
      dumper_flow->rfout=rfout;
      dumper_flow->pass_online_mva=(rfout>grfcut);

      return flow; 
  }

};

class OfflineMVAFactory: public TAFactory
{
public:
   OfflineA2MVAFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("OfflineMVAFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
         if (args[i] == "--usemva")
         {
            fFlags.fUseMVA = true;
            if(i == args.size()-1)
            {
               std::cout << "Using default MVA model, if you want to select a model do --usemva myModel\n";
               std::cout << fFlags.fXMLModel << "\n";
               break;
            }
            else if(args[i+1].substr(args[i+1].size() - 4,4) != ".xml")
            {
               std::cerr << "Non xml model given, using default.\n";
               std::cout << fFlags.fXMLModel << "\n";
            }
            else
            {
               fFlags.fXMLModel = args[++i];
            }
         }
         else
        {
            fFlags.fUseMVA = true;
        }
      }
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("OfflineMVAFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("OfflineMVAFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new OfflineA2MVA(runinfo, &fFlags);
   }
};

static TARegister tar(new OfflineMVAFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */


