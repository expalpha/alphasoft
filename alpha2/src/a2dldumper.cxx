//
// A2 deep learning dumper module for taking events and saving to super trees. This also saved the SVD.
//
// L GOLINO
//

#include <stdio.h>

#include "manalyzer.h"
#include "midasio.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include "A2Flow.h"
#include "TTree.h"
#include "EventTracker.h"


#include "TA2MVADumper.h"

class TA2DumperFlags
{
    public:
        std::string fFileName;
        std::string fTreeName;
};

class TA2Dumper: public TARunObject
{
public:
    //List of event IDs we want to save.
    std::vector<std::pair<int,int>> fEventIDs; 
    //The location of saving said events.
    TTree *fDataTree;
    TFile *fRootFile;
    EventTracker* fA2DumperEventTracker = NULL;


    //The data we want to save is all in this object.
    std::vector<TA2MVADumper*> fMVADumpers;

    //Flags & counters.
    TA2DumperFlags* fFlags;
    int fCurrentEventNumber;
    size_t fCurrentEventIndex = 0;
    bool fEventsLoaded = false;
    bool fEventsSorted = false;

    TA2Dumper(TARunInfo* runInfo, TA2DumperFlags* flags) : TARunObject(runInfo)
    {
        //printf("ctor, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
        fFlags = flags;
        fModuleName="a2dumper";
        
        //Setup the filename for the root file.
        std::string fileName = "dumperoutput";
        fileName += fFlags->fTreeName;
        fileName += std::to_string(runInfo->fRunNo);
        fileName += ".root";
        fRootFile = TFile::Open(fileName.c_str(),"RECREATE");
        
        //Set up the new tree using name as input.
        if(fFlags->fTreeName == "mixing" || fFlags->fTreeName == "cosmic")
            fDataTree = new TTree(fFlags->fTreeName.c_str(),"data from siliconEvent");
        else
        {
            std::cout << "WARNING: No --datalabel given (cosmic or mixing). I could dump to a generic root tree but I want to save you headaches later. Please rerun with a label.\n";
            exit(-1);
        }
        fMVADumpers.push_back( new TA2MVAEventIDDumper(fDataTree) );
        fMVADumpers.push_back( new TA2MVAClassicDumper(fDataTree) );
        fMVADumpers.push_back( new TA2MVAXYZ(fDataTree) );
        fMVADumpers.push_back( new TA2MVAStatusDumper(fDataTree) );
        fMVADumpers.push_back( new TA2MVASiliconHitDumper(fDataTree) );
        fMVADumpers.push_back( new TA2MVASiliconRMSDumper(fDataTree) );
    }

    ~TA2Dumper()
    {
        //printf("dtor!\n");
    }
    
    void BeginRun(TARunInfo* runInfo)
    {
        fCurrentEventIndex = 0;
        fCurrentEventNumber = -1;
        printf("BeginRun, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
        fA2DumperEventTracker = new EventTracker(fFlags->fFileName, runInfo->fRunNo);
    }

    void EndRun(TARunInfo* runInfo)
    {
        printf("EndRun, run %d\n", runInfo->fRunNo);

        //Write the tree and file, then close file.
        fDataTree->Write();
        fRootFile->Write();
        fRootFile->Close();
    }

    //Presently this module doesn't need to touch TMEvent data
    //TAFlowEvent* Analyze(TARunInfo* runInfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow)
    //{
    //   START_TIMER;
    //    flow = new UserProfilerFlow(flow,"custom profiling",timer_start);
    //    return flow;
    //}

    TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /* runInfo */, TAFlags* flags, TAFlowEvent* flow)
    {
        SilEventFlow* fe=flow->Find<SilEventFlow>();
        if (!fe)
        {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
        }
        TAlphaEvent* alphaEvent=fe->alphaevent;
        TSiliconEvent* siliconEvent=fe->silevent;


        if( !fA2DumperEventTracker->IsEventInRange(siliconEvent->GetVF48NEvent(), siliconEvent->GetVF48Timestamp()) )
        {
            return flow;
        }

        //std::cout << "Dumping event " << siliconEvent->GetVF48NEvent() << std::endl;

        //Update variables of the MVA class. This will check whether the eventnumbers match.
        //If they match this function will return true, if not: false. This allows us to know
        //whether to fill the tree and increment the EventIndex and EventNumber.
        size_t good_flow = 0;
        for ( TA2MVADumper* d: fMVADumpers)
        {
            if(d->UpdateVariables(siliconEvent, alphaEvent, siliconEvent->GetVF48NEvent()))
            {
                good_flow++; 
            }
        }
        if (good_flow)
        {
            assert (good_flow == (int)fMVADumpers.size());
            //Update current event number to be checked against (remember everything here is in order).   
            std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
            fDataTree->Fill();
        }
        return flow;
    }

    void AnalyzeSpecialEvent(TARunInfo* runInfo, TMEvent* event)
    {
        printf("a2dump::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", runInfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
    }
};

class TA2DumperFactory: public TAFactory
{
public:
    TA2DumperFlags fFlags;
   void Usage()
   {
      printf("TA2DumperFactory::Help!\n");
      printf("\t--eventlist filename\tSave events from TA2Plot.WriteEventList() function\n");
      printf("\t--datalabel label\tValid data labels currently: mixing and cosmic\n");
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("Init!\n");
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--eventlist")
               fFlags.fFileName = args[i+1];
            if( args[i] == "--datalabel")
               fFlags.fTreeName = args[i+1];
         }
   }
   
   void Finish()
   {
      printf("Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runInfo)
   {
      printf("NewRunObject, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
      return new TA2Dumper(runInfo, &fFlags);
   }
};

static TARegister tar(new TA2DumperFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
