//
// SVD Efficiency per Strip
// using cosmic rays and calculating the fraction
// of hits on a given hybrid to the number of
// expected hits resulting from the intersection 
// of a 5-hits tracks that are expected to intersect
//
// A. Capra
// March 2024 (Based on 2014 work)
//

#include <stdio.h>
#include <stdint.h>
#include <map>


#include "manalyzer.h"
#include "midasio.h"

#include "A2Flow.h"
#include "TAlphaEvent.h"
#include "TSiliconEvent.h"

#include "TH1D.h"

#include "TSISChannels.h"
#include "TVF48SiMap.h"

#include "SiMod.h"

class HitEfficiencyFlags
{
public:
   bool fEnable = false;
};

class HitEfficiencyModule: public TARunObject
{
private:
   TH1D* hphits;
   TH1D* hpexpected;
   TH1D* hpeff;
   TH1D* hnhits;
   TH1D* hnexpected;
   TH1D* hneff;

public:
   HitEfficiencyFlags* fFlags;
   bool fTrace;

   HitEfficiencyModule(TARunInfo* runinfo, HitEfficiencyFlags* flags) : 
                        TARunObject(runinfo), fFlags(flags),fTrace(false)
   {    
      fModuleName = "HitEfficiency";
   }

   ~HitEfficiencyModule()
   {
      if (fTrace)
         printf("HitEfficiencyModule::dtor!\n");
   }
   

   void BeginRun(TARunInfo* runInfo)
   {
      printf("HitEfficiencyModule::BeginRun %d!",runInfo->fRunNo);
      if( !fFlags->fEnable )
      {
         printf("DISABLED\n");
         return;
      }
      else printf("\n");

      runInfo->fRoot->fOutputFile->cd();
      gDirectory->mkdir("HitEfficiency")->cd();

      hphits = new TH1D("hphitsocc","P-Strip Occupancy per Hybrid;Silicon Number",72,-0.5,71.5);
      hpexpected = new TH1D("hpexpected","Expected P-Strip Occupancy per Hybrid;Silicon Number",72,-0.5,71.5);
      hnhits = new TH1D("hnhitsocc","N-Strip Occupancy per Hybrid;Silicon Number",72,-0.5,71.5);
      hnexpected = new TH1D("hnexpected","Expected N-Strip Occupancy per Hybrid;Silicon Number",72,-0.5,71.5);
      
      runInfo->fRoot->fOutputFile->cd();
   }
  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      if( !fFlags->fEnable ) return flow;
      if (fTrace)
         printf("HitEfficiencyModule::AnalyzeFlowEvent, run %d\n", runinfo->fRunNo);

      SilEventFlow* fe = flow->Find<SilEventFlow>();
      if (!fe)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
   
      TAlphaEvent* event = fe->alphaevent;
      event->SetCosmic(true);
      event->CosmicHitEfficiency(hphits, hpexpected, hnhits, hnexpected);

      return flow;
   }

   void EndRun(TARunInfo* runInfo)
   {
      if( !fFlags->fEnable ) return;
      runInfo->fRoot->fOutputFile->cd("HitEfficiency");

      std::cout << "SVD Hit Efficiency Monitor end run" << runInfo->fRunNo << std::endl;

      hpeff = (TH1D*) hphits->Clone("hpeff");
      hpeff->SetTitle("P-Strip Efficiency per Hybrid;Silicon Number");
      hpeff->Divide(hpexpected);

      hneff = (TH1D*) hnhits->Clone("hneff");
      hneff->SetTitle("N-Strip Efficiency per Hybrid;Silicon Number");
      hneff->Divide(hnexpected);

      runInfo->fRoot->fOutputFile->cd();
   }
};

class HitEfficiencyFactory: public TAFactory
{
public:
   HitEfficiencyFlags fFlags;

public:
   void Help()
   {
      printf("HitEfficiencyFactory::Help!\n");
      printf("\t--hiteff\tEnable 'hit efficiency' calculaation for cosmic runs\n");
   }
   void Usage()
   {
     Help();
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("HitEfficiencyFactory::Init!\n");
      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--hiteff")
            fFlags.fEnable = true;
      }
   }

   void Finish()
   {
      if (fFlags.fEnable)
         printf("HitEfficiencyFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("HitEfficiencyFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new HitEfficiencyModule(runinfo, &fFlags);
   }
};

static TARegister tar(new HitEfficiencyFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
