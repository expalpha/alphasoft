//
// Handle special dumps and add to flow (RW, ELENA, laser shutter etc)
//
// JTK McKENNA
//

#include <stdio.h>

#include "manalyzer.h"
#if HAVE_MIDAS
#include "midas.h"
#endif
#include "midasio.h"
#include "TSISEvent.h"
#include "TSpill.h"
#include "AnalysisFlow.h"
#include "A2Flow.h"
#include "TSISChannels.h"
#include "TDumpList.h"
#include "GEM_BANK_flow.h"
#include <iostream>
#include <iomanip> //set precision
#include <sstream>

class SpecialDumpMakerModuleFlags
{
public:
   bool fPrint = false;
};


class SpecialDumpMakerModule: public TARunObject
{
private:
   //double LastSISTS=0;
   static const int MAXDET=10;
public:
   SpecialDumpMakerModuleFlags* fFlags;
   bool fTrace = false;

   
   TSISChannel fADChannel;
   TSISChannel fPreTriggerChannel;
   TSISChannel fCATSeqStart;
   TSISChannel fRCTSeqStart;
   TSISChannel fATMSeqStart;
   TSISChannel fLaserShutterOpen;
   TSISChannel fLaserShutterClose;

   int fADCounter;
   double fADLastTime;
   int fPreTriggerCounter;
   double fPreTriggerLastTime;
   int fLaserShutterOpenCounter;
   int fLaserShutterCloseCounter;
   
   int detectorCh[MAXDET];
   TString detectorName[MAXDET];
   
   std::map<std::string,int> fRWCounter ={};
   
   bool have_svd_events = false;
   double fLastWarningTime = 0.;
   
   double fLastDumpTime;
   std::mutex SequencerLock[USED_SEQ];
   
   SpecialDumpMakerModule(TARunInfo* runinfo, SpecialDumpMakerModuleFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Handle Dumps";
#endif
      if (fTrace)
         printf("SpecialDumpMakerModule::ctor!\n");
   }

   ~SpecialDumpMakerModule()
   {
      if (fTrace)
         printf("SpecialDumpMakerModule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("SpecialDumpMakerModule::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      TSISChannels* SISChannels=new TSISChannels( runinfo->fRunNo );
      fADChannel = SISChannels->GetChannel("SIS_AD", runinfo->fRunNo);
      fPreTriggerChannel = SISChannels->GetChannel("SIS_DIX_ALPHA", runinfo->fRunNo);
      fCATSeqStart = SISChannels->GetChannel("SIS_PBAR_SEQ_START", runinfo->fRunNo);
      fRCTSeqStart = SISChannels->GetChannel("SIS_RECATCH_SEQ_START", runinfo->fRunNo);
      fATMSeqStart = SISChannels->GetChannel("SIS_ATOM_SEQ_START", runinfo->fRunNo);
      fLaserShutterOpen = SISChannels->GetChannel("LASER_SHUTTER_OPEN", runinfo->fRunNo);
      fLaserShutterClose = SISChannels->GetChannel("LASER_SHUTTER_CLOSE", runinfo->fRunNo);
      delete SISChannels;

      fADCounter = 0;
      fADLastTime = -1.;

      fPreTriggerCounter = 0;
      fPreTriggerLastTime = -1.;

      fLaserShutterOpenCounter = 0;
      fLaserShutterCloseCounter = 0;

      fLastDumpTime = 0.;
   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("SpecialDumpMakerModule::EndRun, run %d\n", runinfo->fRunNo);
   }
   
   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("SpecialDumpMakerModule::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }

   //Catch sequencer flow in the main thread, so that we have expected dumps ASAP
   TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent*, TAFlags*, TAFlowEvent* flow)
   {
      if (fTrace)
         printf("SpecialDumpMakerModule::Analyze, run %d\n", runinfo->fRunNo);

      //dumplist[iSeq].Print();
      return flow;
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      SISEventFlow* SISFlow = flow->Find<SISEventFlow>();
      GEMBANK_Flow* GEMFlow = flow->Find<GEMBANK_Flow>();
      GEMBANKARRAY_Flow* GEMArrayFlow = flow->Find<GEMBANKARRAY_Flow>();
      felabviewFlowEvent* LabVIEWFlow = flow->Find<felabviewFlowEvent>();

      if (SISFlow || GEMFlow || GEMArrayFlow || LabVIEWFlow)
      {
        //We have some work to do on the flow
      }
      else
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         //Nothing to do here
         return flow;   
      }
      A2SpillFlow* f = flow->Find<A2SpillFlow>();
      if(!f)
         f=new A2SpillFlow(flow);

      
      if (SISFlow)
      {
         //Add timestamps to dumps
         for (int j=0; j<NUM_SIS_MODULES; j++)
         {
            const std::vector<std::shared_ptr<TSISEvent>> ce = SISFlow->sis_events[j];
            for (size_t i = 0; i < ce.size(); i++)
            {
               const std::shared_ptr<TSISEvent> e = ce.at(i);
               if (e->GetCountsInChannel(fADChannel))
               {
                 TA2Spill* beam = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs ] Beam %d ------------------------------------------------------->	",e->GetRunTime(),fADCounter++);
                 f->spill_events.push_back(beam);
                 fADLastTime = e->GetRunTime();
#if HAVE_MIDAS
                 if ( fPreTriggerLastTime > 0 )
                    if ( fADLastTime - fPreTriggerLastTime > 100 )
                       cm_msg(MTALK, "alpha2online","Warning: Pre Trigger more than an AD cycle old");
                 if (fLastDumpTime > 0 )
                    if ( fADLastTime - fLastDumpTime > 300 )
                    {
                        if(e->GetRunTime()-fLastWarningTime > 900)
                        {
                           std::string warning = 
                              std::string("Warning: Sequencer has been idle for ")\
                              + std::to_string( int( fADLastTime - fLastDumpTime ) )\
                              + std::string(" seconds");
                           cm_msg(MTALK, "alpha2online","%s",warning.c_str());
                           fLastWarningTime = e->GetRunTime();
                        }
                    }

#endif
               }
               if (e->GetCountsInChannel(fPreTriggerChannel))
               {
                 TA2Spill* pretrigger = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs ]----- Pre Trigger %d------->",e->GetRunTime(), fPreTriggerCounter++);
                 f->spill_events.push_back(pretrigger);
                 fPreTriggerLastTime = e->GetRunTime();
               }
               if (e->GetCountsInChannel(fCATSeqStart))
               {
                 TA2Spill* catseqstart = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs ]----- CAT Seq Start ------->",e->GetRunTime());
                 f->spill_events.push_back(catseqstart);
               }
               if (e->GetCountsInChannel(fRCTSeqStart))
               {
                 TA2Spill* rctseqstart = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs ]----- RCT Seq Start ------->",e->GetRunTime());
                 f->spill_events.push_back(rctseqstart);
               }
               if (e->GetCountsInChannel(fATMSeqStart))
               {
                 TA2Spill* atmseqstart = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs ]----- ATM Seq Start ------->",e->GetRunTime());
                 f->spill_events.push_back(atmseqstart);
               }
               if (e->GetCountsInChannel(fLaserShutterOpen))
               {
                 TA2Spill* shutter_open = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs ]\t\t----- Laser shutter opened %d-------",e->GetRunTime(), fLaserShutterOpenCounter++);
                 f->spill_events.push_back(shutter_open);
               }
               if (e->GetCountsInChannel(fLaserShutterClose))
               {
                 TA2Spill* shutter_closed = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs ]\t\t----- Laser shutter closed %d-------",e->GetRunTime(), fLaserShutterCloseCounter++);
                 f->spill_events.push_back(shutter_closed);
               }
            }

         }
      }






      double LNE0 = -1.;
      //double LNE5 = -1.;
      uint32_t unixtime = -1;
      if (GEMFlow)
      {
         // On 3rd July 2023 this changed from a float to a double in the ODB
         GEMBANK<double> *bank = (GEMBANK<double>*) GEMFlow->data;
         unixtime = GEMFlow->MIDAS_TIME;
         if (bank->GetCategoryName() == "ADandELENA" && bank->GetSizeOfDataArray())
         {
            //LNE0
            if (bank->GetVariableName() == "LNEAPULB0030")
               LNE0 = *(bank->GetFirstDataEntry()->DATA(0));
            //LNE5
            //if (bank->GetVariableName() == "LNEAPULB5030")
               //LNE5 = *(bank->GetFirstDataEntry()->DATA(0));
#if HAVE_MIDAS
            //repition time
            if(bank->GetVariableName() == "RepititionTime")
            {
               double repitionTime = *(bank->GetFirstDataEntry()->DATA(0));
               if(repitionTime>130)
               {
                  cm_msg(MTALK, "alpha2online","Warning: Beam repitition greater than 130 seconds, call CCC");
               }
            }
#endif
         }
      }

      if (GEMArrayFlow)
      {
         GEMBANKARRAY *array = GEMArrayFlow->data;
         unixtime = GEMArrayFlow->MIDAS_TIME;
         int bank_no = 0;
          // On 3rd July 2023 this changed from a float to a double in the ODB
         GEMBANK<double>* bank = (GEMBANK<double>*) array->GetGEMBANK(bank_no);
         while(bank)
         {

            if (bank->GetCategoryName() == "ADandELENA")// && bank->GetSizeOfDataArray())
            {
               //LNE0
               if (bank->GetVariableName() == "LNEAPULB0030")
               {
                  LNE0 = *(bank->GetFirstDataEntry()->DATA(0));
               }
               //LNE5
               //if (bank->GetVariableName() == "LNEAPULB5030")
               //{
               //   LNE5 = *(bank->GetFirstDataEntry()->DATA(0));
               //}
            }
            bank = (GEMBANK<double>*) array->GetGEMBANK(++bank_no);
         }
      }
      if (LNE0 > 0 )//|| LNE5 > 0)
      {
         

         TA2Spill* elena = NULL;
#if 1
         elena = new TA2Spill(runinfo->fRunNo,unixtime,"---------------------> LNE0: %.2E ",LNE0 * 1E6);
#else
         std::string ELENA_STRING = std::string("LNE0: ") + std::to_string(LNE0) + std::string("\tLNE5: ") + std::to_string(LNE5);
         if (LNE0 > 0 && LNE5 > 0)
            elena = new TA2Spill(runinfo->fRunNo,unixtime,"---------------------> LNE0: %.2E \t LNE5: %.2E	",LNE0 * 1E6,LNE5 * 1E6);
         else if (LNE0 > 0)
            elena = new TA2Spill(runinfo->fRunNo,unixtime,"---------------------> LNE0: %.2E ",LNE0 * 1E6);
         else if (LNE5 > 0)
            elena = new TA2Spill(runinfo->fRunNo,unixtime,"---------------------> \t\t\tLNE5: %.2E ",LNE5 * 1E6);
#endif
         if (elena)
            f->spill_events.push_back(elena);
         //std::cout << "DATA" << LNE0 << "\t" << LNE5 << std::endl;
      }
  
  
      if (LabVIEWFlow)
      {
         if (LabVIEWFlow->GetBankName() == "PADT" )
         {
            std::ostringstream CsI;
            CsI << std::setprecision(2) << "\t" << LabVIEWFlow->GetBankName() << ": ";
            std::vector<std::string> names = { "CsI1*", "CsI1", "CsI2", "CsIA", "CsIB", "CsIC", "CsID", "CsE", "CsIF" };
            for (size_t i = 1; i < LabVIEWFlow->GetData()->size(); i++)
            {
               if ( i - 1 < names.size() )
                  CsI << names.at(i-1) << std::string(": ");
               CsI << LabVIEWFlow->GetData()->at(i) << "\t";
            }
            //TInfoSpill = new TInfoSpill(padt, 0 ,unixtime, padt.c_str());
            TA2Spill* lv = new TA2Spill(runinfo->fRunNo,unixtime,CsI.str().c_str());
            f->spill_events.push_back(lv);
         }
         else if( LabVIEWFlow->GetBankName() == "PADG")
         {
            std::ostringstream CsI;
            CsI << std::setprecision(2) << "\t" << LabVIEWFlow->GetBankName() << ": ";
            std::vector<std::string> names = { "PScreen", "AG1", "AG2", "BL A", "BL B", "BL C", "BL D", "BL E", "BL F" };
            for (size_t i = 1; i < LabVIEWFlow->GetData()->size(); i++)
            {
               if ( i - 1 < names.size() )
                  CsI  << names.at(i-1)  << std::string(": ");

               CsI << LabVIEWFlow->GetData()->at(i) << "\t";
            }
            //TInfoSpill = new TInfoSpill(padt, 0 ,unixtime, padt.c_str());
            TA2Spill* lv = new TA2Spill(runinfo->fRunNo,unixtime,CsI.str().c_str());
            f->spill_events.push_back(lv);
         }
         // Check pattern XRWX
         else if ( strncmp(LabVIEWFlow->GetBankName().c_str() + 1, "RW", 2) == 0)
         {
            //Catching trap
            if (LabVIEWFlow->GetBankName().at(0) == 'C' || LabVIEWFlow->GetBankName().at(0) == 'A')
            {
               std::ostringstream RWText;
               
               std::string sweep_name = LabVIEWFlow->GetBankName() +
                  std::string("S") + std::to_string(int(LabVIEWFlow->GetData()->at(1)));
               RWText << "\t" << sweep_name << "[";
               RWText << fRWCounter[sweep_name]++;
               RWText << "]: ";
               // This is a clunky handling of the frequency units
               double StartSweep = LabVIEWFlow->GetData()->at(2);
               double StopSweep = LabVIEWFlow->GetData()->at(3);
               //If units should be kHz
               if ( StartSweep > 1000 && StopSweep > 1000)
               {
                   StartSweep /= 1000;
                   StopSweep /= 1000;
                   //If units should be MHz
                   if ( StartSweep > 1000 && StopSweep > 1000)
                   {
                      StartSweep /= 1000;
                      StopSweep /= 1000;
                      RWText << StartSweep << "-" << StopSweep <<"MHz\t";
                   }
                   else // Am kHz
                   {
                      RWText << StartSweep << "-" << StopSweep <<"kHz\t";
                   }
               }
               else // Am Hz
               {
                  RWText << StartSweep << "-" << StopSweep << "Hz\t";
               }
               RWText << "Total "<< LabVIEWFlow->GetData()->at(4) <<"s (" << LabVIEWFlow->GetData()->at(5) <<" fade ms)\t";
               RWText << LabVIEWFlow->GetData()->at(6) <<"-"<< LabVIEWFlow->GetData()->at(7) << "V";
               TA2Spill* lv = new TA2Spill(runinfo->fRunNo,unixtime,RWText.str().c_str());
               f->spill_events.push_back(lv);
            }

         }
      }
      flow=f;
      return flow; 
   }

};

class SpecialDumpMakerModuleFactory: public TAFactory
{
public:
   SpecialDumpMakerModuleFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("SpecialDumpMakerModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
      }
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("SpecialDumpMakerModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("SpecialDumpMakerModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new SpecialDumpMakerModule(runinfo, &fFlags);
   }
};

static TARegister tar(new SpecialDumpMakerModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
