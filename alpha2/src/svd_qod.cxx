//
// SVD QOD, dumps occupancy data to histogram. Currently only ASICs but feel free to add. 
//
// L GOLINO
// A. Capra 
// March 2024
//

#include <stdio.h>
#include <stdint.h>
#include <map>

#include "manalyzer.h"
#include "midasio.h"

#include "A2Flow.h"
#include "TAlphaEvent.h"
#include "TSiliconEvent.h"
#include "TAlphaEventVertex.h" // new imported .h file
#include "TAlphaEventMap.h"

#include "TH1D.h"
#include "TH1I.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TPaveText.h"
#include "TCanvas.h"
#include "TPaveStats.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TLegend.h"
#include <THStack.h>
#include "TFrame.h"
#include "TPad.h"

#include "TSISChannels.h"
#include "TVF48SiMap.h"

#include "SiMod.h"

class SVD_QODFlags
{
public:
   bool fEnable = false;
   double colorPalette = 55;
};

class SVD_QOD: public TARunObject
{
private:
   TH1D hist[nSil*4];
   TH1D hist_si_occ;
   TH1D hist_si_occ_nside;
   TH1D hist_si_occ_pside;
   TH2D strip_adc[nSil*4];
   TH1D adc_hist[nSil*4];
   TH1D hist_inner;         // inner layer hits
   TH1D hist_middle;        // middle layer hits
   TH1D hist_outer;         // outer layer hits
   TH1D hist_totHits;       // total hits per event
   TH1D hist_timeVF48;      // VF48 time between events
   TH1D hist_timeRN;        // Run Time between events
   TH1D hist_timeExp;       // Exp Time between events
   //TH1D hist_tracks;      // histogram number of track per event
   TH2D Rphi_zpos;          // Polar coordinate occupancy histo
   TH2D Rphi_zneg;          // Polar coordinate occupancy histo
   TH2D histXY_zpos;        // XY occupancy histo
   TH2D histXY_zneg;        // XY occupancy histo
   TH2D histAsicOccupancyPside;   // Silicon Module Occupancy
   TH2D histAsicOccupancyNside;   // Silicon Module Occupancy
   TH1D histHitsZ;			// Histogram of Z-variable
   TH1D histCosmic;         // Histogram with cosmic events candidates
   
   // variables for the qod

   // //
   // variables to store the time of the last event
   Double_t foldTimeVF48   = 0;
   Double_t foldTimeRN     = 0;
   Double_t foldTimeExp    = 0;
   Double_t foldTimeCosmic = 0;
   Double_t maxInnerHits  = 0;
   Double_t maxMiddleHits = 0;
   Double_t maxOuterHits  = 0;
   // //

   // variable for computing asymmetry in Z hits
   Double_t Zmin = 0;
   Double_t Zmax = 0;
   
public:
   SVD_QODFlags* fFlags;
   bool fTrace;

   SVD_QOD(TARunInfo* runinfo, SVD_QODFlags* flags) : 
   TARunObject(runinfo), fFlags(flags),fTrace(false)
   {    
      fModuleName = "SVDQOD";
   }

   ~SVD_QOD()
   {
      if (fTrace)
         printf("SVD_QOD::dtor!\n");
   }

   void BeginRun(TARunInfo* runInfo)
   {
      printf("SVD_QOD::BeginRun %d!",runInfo->fRunNo);
      if( !fFlags->fEnable )
      {
         printf("DISABLED\n");
         return;
      }
      else printf("\n");

      runInfo->fRoot->fOutputFile->cd();
      gDirectory->mkdir("svd_qod")->cd();
      hist_si_occ = TH1D( "Si_Occs", "Silicon Module Occupancies",nSil,-0.5,(nSil - 0.5));
	  hist_si_occ_nside = TH1D( "Si_Occs_nside", "Silicon Module Occupancies",nSil,-0.5,(nSil - 0.5));
	  hist_si_occ_pside = TH1D( "Si_Occs_pside", "Silicon Module Occupancies",nSil,-0.5,(nSil - 0.5));
	  

      gDirectory->mkdir("Occupancy")->cd();
      char name[128];
      char title[256];
      TVF48SiMap  VF48SiMap;
      int m, c, ttcchannel;
      for (int isil=0; isil<nSil; isil++)
      {    
         for( int iASIC = 1; iASIC <= 4; iASIC++ ) 
         {
            VF48SiMap.GetVF48( isil, iASIC, m, c, ttcchannel );
            snprintf( name, 128, "VF48%d_Channel%03d_Si%02d_ASIC%d_OCC", m, c, isil, iASIC );
            snprintf( title, 256, "VF48# %d Channel# %3d Si# %02d ASIC# %d : OCC", m, c, isil, iASIC );
            hist[isil*4 + (iASIC-1)] = TH1D( name, title, 128, 0, 128 );
            
            snprintf( name, 128, "VF48%d_Channel%03d_Si%02d_ASIC%d_ADC", m, c, isil, iASIC );
            snprintf( title, 256, "VF48# %d Channel# %3d Si# %02d ASIC# %d : ADC ", m, c, isil, iASIC );     
            strip_adc[isil*4 + (iASIC-1)] = TH2D( name, title, 128,0.,128.,200,-1000.,1000. );

            snprintf( name, 128, "VF48%d_Channel%03d_Si%02d_ASIC%d_pedsubADC", m, c, isil, iASIC );
            snprintf( title, 256, "VF48# %d Channel# %3d Si# %02d ASIC# %d : pedestal-subtracted ADC", m, c, isil, iASIC );
            adc_hist[isil*4 + (iASIC-1)] = TH1D( name, title,200,-1000.,1000. );
         }
      }     
      runInfo->fRoot->fOutputFile->cd();
	  hist_inner   = TH1D("InLay",  "Inner Layer Hits",    351, -0.5,  350.5); 
	  hist_middle  = TH1D("MidLay", "Middle Layer Hits",   351, -0.5,  350.5);
	  hist_outer   = TH1D("OutLay", "Outer Layer Hits",    351, -0.5,  350.5);
	  hist_totHits = TH1D("TotLay", "Hits per Event",      351, -0.5,  350.5);
	  hist_timeVF48= TH1D("dTvf48", "VF48 Time between Events", 100,0., 0.);
	  hist_timeRN  = TH1D("dTrun",  "Run Time between Events",  100,0., 0.);
	  hist_timeExp = TH1D("dTExp",  "Exp Time between Events",  100,0., 0.);
	  //hist_tracks  = TH1D("tracks", "Number of Tracks", 101, -0.05, 10.05);
	  histHitsZ    = TH1D("Z-Hits", "Z",  29,    -14.5,    14.5);
	  histCosmic   = TH1D("CosmicCandidates", "Cosmic Events", 100, 0., 0.);
	  Rphi_zpos    = TH2D("Rphi_zpos", "Radius vs phi (Z>0)", 36, -180, +180, 20, 0.0, 20 );
	  Rphi_zneg    = TH2D("Rphi_zneg", "Radius vs phi (Z<0)", 36, -180, +180, 20, 0.0, 20 );
	  histXY_zpos  = TH2D("XYmodZpos","Modules Occupancy (Z>0)", 31, -15.5, 15.5, 41, -15.5, 15.5);
	  histXY_zneg  = TH2D("XYmodZneg","Modules Occupancy (Z<0)", 31, -15.5, 15.5, 41, -15.5, 15.5);
	  histAsicOccupancyPside = TH2D("ModOccP", "Asic Module Occupancy P-side", 56, 0, 28,  12 ,0., 6. );
	  histAsicOccupancyNside = TH2D("ModOccN", "Asic Module Occupancy N-side", 56, 0, 28,  12 ,0., 6. );

	  // Plots Layout
	  hist_si_occ.SetLineColor(1);
	  hist_si_occ_nside.SetLineColor(2);
	  hist_si_occ_pside.SetLineColor(8);
	  
	  hist_inner.SetLineColor(4);
	  hist_inner.SetLineWidth(2);
	  hist_inner.GetXaxis()->SetTitle("Hits per Event");
	  
	  hist_middle.SetLineColor(2);
	  hist_middle.SetLineWidth(3);
	  hist_middle.GetXaxis()->SetTitle("Hits per Event");
	  
	  hist_outer.SetLineColor(6);
	  hist_outer.SetLineWidth(3);
	  hist_outer.GetXaxis()->SetTitle("Hits per Event");
	  
	  hist_totHits.SetLineColor(1);
	  hist_totHits.SetLineWidth(2);
	  hist_totHits.GetXaxis()->SetTitle("Total Hits per Event");
	  
	  hist_timeVF48.SetLineColor(4);
	  hist_timeVF48.SetLineWidth(2);
	  hist_timeVF48.GetXaxis()->SetTitle("Time VF48 btw events [second]");
	  
	  hist_timeRN.SetLineColor(4);
	  hist_timeRN.SetLineWidth(2);
	  hist_timeRN.GetXaxis()->SetTitle("Run Time btw events [second]");
	  
	  hist_timeExp.SetLineColor(4);
	  hist_timeExp.SetLineWidth(2);
	  hist_timeExp.GetXaxis()->SetTitle("Exp Time btw events [second]"); 
	  
	  //hist_tracks.SetLineColor(3);
	  //hist_tracks.SetLineWidth(2);
	  //hist_tracks.GetXaxis()->SetTitle("Number of Tracks");
	  
	  histHitsZ.SetLineColor(1);
	  histHitsZ.SetLineWidth(2);
	  histHitsZ.GetXaxis()->SetTitle("Z position of modules");
	  
	  histCosmic.SetLineColor(1);
	  histCosmic.SetLineWidth(2);
	  histCosmic.GetXaxis()->SetTitle("Time between events [second]");

	  gStyle->SetPalette(fFlags->colorPalette);
	  Rphi_zpos.SetOption("LEGO2Z POL");
	  //Rphi_zpos.SetOption("COLZPOL");
	  Rphi_zpos.GetXaxis()->SetTitle("phi");
	  Rphi_zpos.GetYaxis()->SetTitle("radius");
	  
	  Rphi_zneg.SetOption("LEGO2Z POL");
	  //Rphi_zneg.SetOption("COLZPOL");
	  Rphi_zneg.GetXaxis()->SetTitle("phi");
	  Rphi_zneg.GetYaxis()->SetTitle("radius");
	  
	  histXY_zpos.GetXaxis()->SetTitle("X module position [cm]");
	  histXY_zpos.GetYaxis()->SetTitle("Y module position [cm]");
	  histXY_zpos.SetOption("COLZ");
	  
	  histXY_zneg.GetXaxis()->SetTitle("X module position [cm]");
	  histXY_zneg.GetYaxis()->SetTitle("Y module position [cm]");
	  histXY_zneg.SetOption("COLZ");
	  
	  histAsicOccupancyPside.GetXaxis()->SetTitle("Asic Number #");
	  histAsicOccupancyPside.GetYaxis()->SetTitle("Slice Number #");
	  histAsicOccupancyPside.GetXaxis()->SetNdivisions(7.);
	  histAsicOccupancyPside.SetOption("COLZ");
	  
	  histAsicOccupancyNside.GetXaxis()->SetTitle("Asic Number #");
	  histAsicOccupancyNside.GetYaxis()->SetTitle("Slice Number #");
	  histAsicOccupancyNside.GetXaxis()->SetNdivisions(7.);
	  histAsicOccupancyNside.SetOption("COLZ");
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      if( !fFlags->fEnable ) return flow;
      if (fTrace)
         printf("SVD_QOD::AnalyzeFlowEvent, run %d\n", runinfo->fRunNo);

      SilEventFlow* silFlow = flow->Find<SilEventFlow>();

      if(!silFlow)
      {
         return flow;
      }

      TSiliconEvent* silEvent = silFlow->silevent;
	  if(!silEvent){
		  return flow;
	  }
	  // get the Alpha Event from the class SilEventFlow
	  TAlphaEvent* alphaEvent = silFlow->alphaevent;
	  
	  // Get time of the event
	  Double_t TimeVF48      = 0;
	  Double_t TimeRN        = 0;
	  Double_t TimeExp       = 0;
	  TimeVF48 = silEvent->GetVF48Timestamp();
	  TimeRN   = silEvent->GetTSRunTime();
	  TimeExp  = silEvent->GetExptTime();

	  int hitsInner  = 0;  // inner layer number of hits
	  int hitsMiddle = 0;  // middle layer number of hits
	  int hitsOuter  = 0;  // outer layer number of hits
	  
	  //Update the histogram
      for (int i = 0; i < nSil; i++)
      {
         TSiliconModule* module = silEvent->GetSiliconModule(i);
         if(!module)
            continue;
         int iSil = module->GetModuleNumber();
         std::vector<TSiliconVA*> asics = module->GetASICs();
		 int LayerNum = alphaEvent->GetLayerNum(iSil); // Layer number of the iSil module
		 for(const TSiliconVA* silva: asics)
         {
            if(!silva) continue;

            int nASIC = silva->GetASICNumber() - 1;
            for(uint j=0; j<nSTRIPs; ++j)
            {
               if( silva->Hit[j] )
               {
                  double pedsubadc = silva->PedSubADC[j];
                  hist[iSil*4 +nASIC].Fill( (double) j );
                  hist_si_occ.Fill(iSil);
                  strip_adc[iSil*4 +nASIC].Fill( j, pedsubadc ); // Not Written in the file ROOT file
                  adc_hist[iSil*4 +nASIC].Fill( pedsubadc );
				  
				  if(silva->IsAPSide()){ hist_si_occ_pside.Fill(iSil);}
				  else{ hist_si_occ_nside.Fill(iSil);}
				  
				  // Asics Occupancy
				  double asicNumber = -1; double layerAsic = -1;
				  int offsetAsicNum = (silva->IsAPSide()) ? 3 : 1;
				  if( iSil < 72 && iSil >= 58){
					  asicNumber = (iSil - 58)*2 + (silva->GetASICNumber() - offsetAsicNum)*0.5; layerAsic = 5;
				  }else if( iSil < 58 && iSil >= 46){
					  asicNumber = (iSil - 46)*2 + (silva->GetASICNumber() - offsetAsicNum)*0.5; layerAsic = 4;
				  }else if( iSil < 46 && iSil >= 36){
					  asicNumber = (iSil - 36)*2 + (silva->GetASICNumber() - offsetAsicNum)*0.5; layerAsic = 3;
				  }else if( iSil < 36 && iSil >= 22){
					  asicNumber =  (iSil - 22)*2 + (silva->GetASICNumber() - offsetAsicNum)*0.5; layerAsic = 0;
				  }else if( iSil < 22 && iSil >= 10){
					  asicNumber =  (iSil - 10)*2 + (silva->GetASICNumber() - offsetAsicNum)*0.5; layerAsic = 1;
				  }else{
					  asicNumber =  (iSil - 0)*2 + (silva->GetASICNumber() - offsetAsicNum)*0.5; layerAsic = 2;
				  }
				  
				  if(silva->IsAPSide()){ histAsicOccupancyPside.Fill(asicNumber, layerAsic);}
				  else{ histAsicOccupancyNside.Fill(asicNumber, layerAsic);}

				  if(LayerNum == 0){
					  hitsInner += 1;
				  }else if(LayerNum == 1){
					  hitsMiddle += 1;
				  }else if(LayerNum == 2){
					  hitsOuter += 1;
				  }
				  double xModPosition = alphaEvent->GetModuleX(iSil); // get x position of the module
				  double yModPosition = alphaEvent->GetModuleY(iSil); // get y position of the module
				  double zModPosition = alphaEvent->GetModuleZ(iSil); // get z position of the module
				  if(zModPosition > 0){ Zmax += 1;}
				  else{Zmin += 1;}
				  
				  double RadiusMod = sqrt(TMath::Power(xModPosition,2) + TMath::Power(yModPosition,2)); 
				  // if(zModPosition > 0){Rphi_zpos.Fill(alphaEvent->GetModuleAngle(iSil)*180/TMath::Pi(), RadiusMod);}
				  // if(zModPosition < 0){Rphi_zneg.Fill(alphaEvent->GetModuleAngle(iSil)*180/TMath::Pi(), RadiusMod);}
				  if(zModPosition > 0){ histXY_zpos.Fill(xModPosition, yModPosition);}
				  if(zModPosition < 0){ histXY_zneg.Fill(xModPosition, yModPosition);}
				  histHitsZ.Fill(zModPosition);
				  //std::cout << "angle: " << alphaEvent->GetModuleAngle(iSil)*180/TMath::Pi() 
				  //<< " X position "  << alphaEvent->GetModuleX(iSil) 
				  //<< " Y position: " << alphaEvent->GetModuleY(iSil) 
				  //<< " Radius: "     << RadiusMod
				  //<< std::endl;
			   }
            }
         } 
      }
	  // Fill Layer histograms
	  if(hitsInner  != 0){hist_inner.Fill(hitsInner);}
	  if(hitsMiddle != 0){hist_middle.Fill(hitsMiddle);}
	  if(hitsOuter  != 0){hist_outer.Fill(hitsOuter);}
	  maxInnerHits  = (maxInnerHits > hitsInner)    ?  maxInnerHits : hitsInner;
	  maxMiddleHits = (maxMiddleHits > hitsMiddle)  ?  maxMiddleHits : hitsMiddle;
	  maxOuterHits  = (maxOuterHits > hitsOuter)    ?  maxOuterHits : hitsOuter;

	  if((hitsInner + hitsMiddle + hitsOuter) != 0){hist_totHits.Fill((hitsInner + hitsMiddle + hitsOuter));}
	  // Fill histogram interval of time between two next to next events
	  if((hitsInner + hitsMiddle + hitsOuter) != 0){
		  hist_timeVF48 .Fill((TimeVF48 - foldTimeVF48));
		  // hist_timeRN   .Fill((TimeRN - foldTimeRN));
		  // hist_timeExp  .Fill((TimeExp - foldTimeExp));
		  foldTimeVF48 = TimeVF48; foldTimeRN = TimeRN; foldTimeExp = TimeExp;   // set the time of the last events
		  //hist_tracks.Fill(Ntracks);                                        // Fill number of Tracks histogram
	  }
	  
	  // HISTOGRAM WITH COSMIC EVENTS CANDIDATES
	  // ONLY EVENTS WITH 6 HITS ARE CONSIDERED
	  Double_t TimeCosmic = 0;
	  if(alphaEvent->GetNHits() == 6){
		  TimeCosmic = silEvent->GetVF48Timestamp(); // Get the VF48 time
		  histCosmic.Fill((TimeCosmic - foldTimeCosmic));
		  foldTimeCosmic = TimeCosmic;
	  }
      return flow;
   }

   void EndRun(TARunInfo* runInfo)
   {
      if( !fFlags->fEnable ) return;
      runInfo->fRoot->fOutputFile->cd("svd_qod");
	  
	  // Set range for Hits plots
	  hist_inner.SetAxisRange (-0.5,  maxInnerHits,"X");
	  hist_middle.SetAxisRange(-0.5,  maxMiddleHits,"X");
	  hist_outer.SetAxisRange (-0.5,  maxOuterHits,"X");
      std::cout << "SVD Monitor end run\n";
	  
	  // fit the time VF48
	  TFitResultPtr resultPtr;
	  if(hist_timeVF48.GetMean() <= 1){ // fit range for "mixing" runs
	      resultPtr = hist_timeVF48.Fit("expo", "L", "Q", 0.005, 3);
	  }else{                            // fit range for "cosmic" runs
		  resultPtr = hist_timeVF48.Fit("expo", "L", "Q", 0.005, 12.);
	  }
	  TPaveText* fitResults = new TPaveText(0.5, 0.5, 0.9, 0.9, "NDC");
	  fitResults->SetFillColor(0);
	  fitResults->SetTextSize(0.03);
	  // Print fit results in TPaveText
	  char fitResult[100];
	  snprintf(fitResult, sizeof(fitResult), "Fit Parameters:");
	  fitResults->AddText(fitResult);
	  TF1* fitFunc = hist_timeVF48.GetFunction("expo");
      snprintf(fitResult,sizeof(fitResult), "%s = %.3f +/- %.3f", fitFunc->GetParName(0), fitFunc->GetParameter(0), fitFunc->GetParError(0));
	  fitResults->AddText(fitResult);
	  snprintf(fitResult, sizeof(fitResult), "rate = %.3f +/- %.3f Hz", -fitFunc->GetParameter(1), fitFunc->GetParError(1));
      fitResults->AddText(fitResult);
      // Save the fit results onto the plot
      hist_timeVF48.GetListOfFunctions()->Add(fitResults);
	  
	  // Fit Cosmic Candidates
	  TFitResultPtr resultPtr2;
	  resultPtr2 = histCosmic.Fit("expo", "L", "Q", 0, 5);
	  TPaveText* CosmicFitResults = new TPaveText(0.5, 0.5, 0.9, 0.9, "NDC");
	  CosmicFitResults->SetFillColor(0);
	  CosmicFitResults->SetTextSize(0.03);
	  // Print fit results in TPaveText
	  char CosmicFitResult[100];
	  sprintf(CosmicFitResult, "Fit Parameters:");
	  CosmicFitResults->AddText(CosmicFitResult);
	  TF1* fitFunc2 = histCosmic.GetFunction("expo");
      sprintf(CosmicFitResult, "%s = %.3f +/- %.3f", fitFunc2->GetParName(0), fitFunc2->GetParameter(0), fitFunc2->GetParError(0));
	  CosmicFitResults->AddText(CosmicFitResult);
	  sprintf(CosmicFitResult, "rate = %.3f +/- %.3f Hz", -fitFunc2->GetParameter(1), fitFunc2->GetParError(1));
      CosmicFitResults->AddText(CosmicFitResult);
      // Save the fit results onto the plot
      histCosmic.GetListOfFunctions()->Add(CosmicFitResults);

      runInfo->fRoot->fOutputFile->cd("svd_qod");
      hist_si_occ.Write();
	  hist_si_occ_nside.Write();
	  hist_si_occ_pside.Write();
	  //hist_inner.Write();
	  //hist_middle.Write();
	  //hist_outer.Write();
	  //hist_totHits.Write();
	  //hist_timeVF48.Write();
	  //hist_timeRN.Write();
	  //hist_timeExp.Write();
	  //hist_tracks.Write();
	  //Rphi_zpos.Write();
	  //Rphi_zneg.Write();
	  //histHitsZ.Write();
	  //histXY_zpos.Write();
	  //histXY_zneg.Write();
	  histCosmic.Write();
	  histAsicOccupancyPside.SetStats(0);
	  histAsicOccupancyPside.Write();
	  histAsicOccupancyNside.SetStats(0);
	  histAsicOccupancyNside.Write();
	  
	  // save summary canvas with some of the plots
	  auto summaryCanvas = new TCanvas("summary", std::to_string(runInfo->fRunNo).c_str(), 1700, 1000);
	  summaryCanvas->Divide(4,2,0.005,0.005);

	  summaryCanvas->cd(1);   // Occupancy modules with Z>0
	  histXY_zpos.SetStats(0);
	  histXY_zpos.Draw();

	  summaryCanvas->cd(2);   // Occupancy modules with Z<0
	  histXY_zneg.SetStats(0);
	  histXY_zneg.Draw();
	  TPaveText* asymmetry = new TPaveText(0.2478, 0.4557, 0.7484, 0.5458, "NDC");
	  TString newtext = TString::Format("(Nz+ - Nz-) / (Nz+ + Nz-) = %.3f", (Zmax - Zmin) / (Zmax + Zmin));
	  asymmetry->SetFillColor(0);
	  asymmetry->SetTextColor(2);
	  asymmetry->SetTextSize(0.035);
	  asymmetry->AddText(newtext);
	  asymmetry->SetBorderSize(0);
	  asymmetry->SetFillStyle(0);
	 
	  asymmetry->Draw("SAME");
	  gPad->Update();
	  
	  summaryCanvas->cd(8);   // Legend
	  histCosmic.Draw(); // Draw Cosmic Candidates histogram
	  gPad->Update();
	  TPaveStats *stc = (TPaveStats*)histCosmic.FindObject("stats");
	  stc->SetOptStat(1000000210);
	  stc->SetOptFit(1100);
	  stc->SetTextSize(0.03);
	  // Adjust position of stat box
	  stc->SetX1NDC(0.509);
	  stc->SetX2NDC(0.909);
	  stc->SetY1NDC(0.278);
	  stc->SetY2NDC(0.480);
	  
	  // LEGEND FOR MODULE NUMBER/LAYER 
	  TPaveText* LayerModMap = new TPaveText(0.2, 0.5, 0.7, 0.9, "NDC");
	  LayerModMap->SetFillColor(0);
	  LayerModMap->SetTextColor(1);
	  LayerModMap->SetTextSize(0.03);
	  LayerModMap->AddText("slice 0: Modules 22-35");
	  LayerModMap->AddText("slice 1: Modules 10-21");
	  LayerModMap->AddText("slice 2: Modules 0-9");
	  LayerModMap->AddText("slice 3: Modules 36-45");
	  LayerModMap->AddText("slice 4: Modules 46-57");
	  LayerModMap->AddText("slice 5: Modules 58-71");
	  LayerModMap->SetBorderSize(1);
	  //LayerModMap->Draw();
	  
	  //histHitsZ.Draw("same");
	  //TPaveStats *stz = (TPaveStats*)histHitsZ.FindObject("stats");
	  //stz->SetOptStat(1000000211);
	  // Adjust position of stat box
	  //stz->SetX1NDC(0.398);
	  //stz->SetX2NDC(0.598);
	  //stz->SetY1NDC(0.7139);
	  //stz->SetY2NDC(0.875);
	  
	  summaryCanvas->cd(4);  // ASICs Occupancies
	  histAsicOccupancyNside.GetYaxis()->SetTickLength(0);
	  histAsicOccupancyNside.Draw();
	  
	  int nBinsX = histAsicOccupancyNside.GetNbinsX();
      int nBinsY = histAsicOccupancyNside.GetNbinsY();
	  for (int j = 1; j <= nBinsY; j = j + 2) {
		int counter = 0; int modnum = 0; int nmod = 0;
		if(j == 1){ modnum = 22; nmod = 13;}
		if(j == 3){ modnum = 10; nmod = 11;}
		if(j == 5){ modnum = 0;  nmod = 9;}
		if(j == 7){ modnum = 36; nmod = 9;}
		if(j == 9){ modnum = 46; nmod = 11;}
		if(j == 11){ modnum = 58;nmod = 13;}
		for (int i = 1; i <= nBinsX; i = i + 4){
		  double x = histAsicOccupancyNside.GetXaxis()->GetBinCenter(i) + 0.04;
		  double y = histAsicOccupancyNside.GetYaxis()->GetBinCenter(j) + 0.35;
		  TText *text = new TText(x, y, Form("%d", modnum + counter));
		  text->SetTextAlign(22);  // center the text
		  text->SetTextSize(0.034); // set text dimension
		  if(counter <= nmod){text->Draw("same");}
		  counter += 1; // update module number
        }
      }
	    
	  summaryCanvas->cd(5);  // VF48 time of hits with hits
	  hist_timeVF48.Draw();
	  gPad->Update();
	  TPaveStats *st = (TPaveStats*)hist_timeVF48.FindObject("stats");
	  st->SetOptStat(1000000210);
	  st->SetOptFit(1100);
	  st->SetTextSize(0.03);
	  // Adjust position of stat box
	  st->SetX1NDC(0.509);
	  st->SetX2NDC(0.909);
	  st->SetY1NDC(0.278);
	  st->SetY2NDC(0.480);
	  
	  summaryCanvas->cd(6);  // Silicon Modules Occupancies
	  THStack* stack =  new THStack("stack", "Silicon Modules Occupancies; Module Number ; Counts #");
	  stack->Add(&hist_si_occ);
	  stack->Add(&hist_si_occ_nside);
	  stack->Add(&hist_si_occ_pside);
	  stack->Draw("nostack");
	  auto legend1 = new TLegend(0.425,0.12,0.89,0.26);
	  TString entriesText = TString::Format("entries = %d", (int)hist_si_occ.GetEntries());
	  TString psideText = TString::Format("p-side avg count %.d", (int)hist_si_occ_pside.GetEntries()/hist_si_occ_pside.GetNbinsX());
	  TString nsideText = TString::Format("n-side avg count %.d", (int)hist_si_occ_nside.GetEntries()/hist_si_occ_nside.GetNbinsX());
	  TString totalText = TString::Format("total avg count  %.d", (int)hist_si_occ.GetEntries()/hist_si_occ.GetNbinsX());
	  legend1->AddEntry("Si_Occs", totalText ,"l");
	  legend1->AddEntry("Si_Occs_nside", nsideText, "l");
	  legend1->AddEntry("Si_Occs_pside", psideText,"l");
	  legend1->AddEntry("Si_Occs", entriesText ,"l");
	  legend1->SetTextSize(0.035);
	  legend1->SetFillStyle(0); // make legend transparent
	  legend1->Draw();
	  
	  
	  summaryCanvas->cd(7);  // Hits per event and per layer
	  TPad *pad7 = (TPad*)gPad;
	  pad7->SetLeftMargin(0.15);  // increase left margin
	  pad7->SetRightMargin(0.05); // decrease right margin
	  THStack* stack0 = new THStack("stack0", "Hits per Layer per Events; Hits per Event ; Counts #");
	  stack0->Add(&hist_inner);
	  stack0->Add(&hist_middle);
	  stack0->Add(&hist_outer);
	  stack0->Add(&hist_totHits);
	  stack0->Draw("nostack");
	  if(hist_totHits.GetMaximumBin() < 20){  // decrease histogram range for cosmic run, with "low" hits
		  stack0->GetXaxis()->SetLimits(-0.5, 30.5);
	  }
	  gPad->Update(); // update the pad
	  auto legend = new TLegend(0.62,0.699,0.95,0.90);
	  legend->AddEntry("InLay", " inner  layer hits" ,"l");
	  legend->AddEntry("MidLay"," middle layer hits", "l");
	  legend->AddEntry("OutLay"," outer  layer hits" ,"l");
	  legend->AddEntry("TotLay"," total hits",  "l");
	  legend->SetFillStyle(0); // make legend transparent
	  legend->SetTextSize(0.035);
	  legend->Draw();
	  summaryCanvas->Modified();
	  summaryCanvas->Update();
	  
	  
	  summaryCanvas->cd(3); // Asic Occupancies
	  histAsicOccupancyPside.GetYaxis()->SetTickLength(0);
	  histAsicOccupancyPside.Draw();
	  nBinsX = histAsicOccupancyPside.GetNbinsX();
      nBinsY = histAsicOccupancyPside.GetNbinsY();
	  for (int j = 1; j <= nBinsY; j = j + 2) {
		int counter = 0; int modnum = 0; int nmod = 0;
		if(j == 1){ modnum = 22; nmod = 13;}
		if(j == 3){ modnum = 10; nmod = 11;}
		if(j == 5){ modnum = 0;  nmod = 9;}
		if(j == 7){ modnum = 36; nmod = 9;}
		if(j == 9){ modnum = 46; nmod = 11;}
		if(j == 11){ modnum = 58;nmod = 13;}
		for (int i = 1; i <= nBinsX; i = i + 4){
		  double x = histAsicOccupancyPside.GetXaxis()->GetBinCenter(i) + 0.04;
		  double y = histAsicOccupancyPside.GetYaxis()->GetBinCenter(j) + 0.35;
		  TText *text = new TText(x, y, Form("%d", modnum + counter));
		  text->SetTextAlign(22);  // center the text
		  text->SetTextSize(0.033); // set text dimension
		  if(counter <= nmod){text->Draw("same");}
		  counter += 1; // update module number
        }
      }

      // Create a ttext object and save the run file name
	  summaryCanvas->cd();
	  // Create a new Pad
	  TPad *overlayPad = new TPad("overlayPad", "", 0.4, 0.4, 0.6, 0.6);
	  overlayPad->SetFillStyle(0); // make it transparent
	  overlayPad->SetFrameFillStyle(0); // make it transparent
	  overlayPad->Draw();
	  overlayPad->cd();
	  
	  TPaveText *infoText = new TPaveText(0.2, 0.45, 0.8, 0.55, "NDC");
      infoText->AddText(runInfo->fFileName.c_str());
      infoText->SetFillColor(0);
      infoText->SetFillStyle(0);
      infoText->SetBorderSize(0);
	  infoText->SetTextSize(0.087);
      infoText->Draw();
	  
	  summaryCanvas->Update();
      summaryCanvas->Modified();

	  summaryCanvas->Write();
      runInfo->fRoot->fOutputFile->cd("svd_qod/Occupancy");
      for( uint i=0; i<nSil*4; ++i)
      {
         hist[i].Write(); // occupancy
         std::string pname(strip_adc[i].GetName());
         pname += "mean";
         // Unused, is this a bug?
		 // TProfile* hprof = strip_adc[i].ProfileX(pname.c_str()); // mean ADC
         adc_hist[i].Write(); // pedestal-subtracted ADC
      }
      runInfo->fRoot->fOutputFile->cd();
   }
};

//static TARegister tar1(new TAFactoryTemplate<SVD_QOD>);
class SVD_QODFactory: public TAFactory
{
public:
   SVD_QODFlags fFlags;

public:
   void Help()
   {
      printf("SVD_QODFactory::Help!\n");
      printf("\t--hiteff\tEnable 'hit efficiency' calculation for cosmic runs\n");
   }
   void Usage()
   {
     Help();
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("SVD_QODFactory::Init!\n");
      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--qod")
            fFlags.fEnable = true;
		    if(args[i].find("-col") != std::string::npos){
				std::string prefix = "-col";
				std::string numPart = args[i].substr(args[i].find(prefix) + prefix.length());
				//std::cout << "Color Palette: " << numPart << std::endl;
				try {
					fFlags.colorPalette = std::stod(numPart);
				} catch (const std::invalid_argument& e) {
					std::cerr << "Conversion error: invalid argument." << std::endl;
					std::cout << "Using standard color palette 55" << std::endl;
				} catch (const std::out_of_range& e) {
					std::cerr << "Conversion error: out of range." << std::endl;
					std::cout << "Using standard color palette 55" << std::endl;
				}
				
				if(std::stod(numPart) < 51 || std::stod(numPart) > 113){
					std::cout << "color Palette out of range (51 - 113)" << std::endl;
					std::cout << "Using standard color palette 55" << std::endl;
					fFlags.colorPalette = 55;
					}
			}
		}
	}

   void Finish()
   {
      if (fFlags.fEnable)
         printf("SVD_QODFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("SVD_QODFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new SVD_QOD(runinfo, &fFlags);
   }
};

static TARegister tar(new SVD_QODFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
