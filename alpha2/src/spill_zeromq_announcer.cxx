//
// ALPHA 2 (SVD AND SIS) spill_log_module
//
// JTK McKenna
//


#include <list>
#include <stdio.h>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include "manalyzer.h"
#include "midasio.h"
#include "TSystem.h"
#include <TEnv.h>

#include "AnalysisFlow.h"
#include "A2Flow.h"

#include "time.h"
#include <vector>

#include <sys/stat.h> // struct stat_buffer;

#include "TSISChannels.h"

#if HAVE_ZERO_MQ
// From https://raw.githubusercontent.com/zeromq/cppzmq/master/zmq.hpp
#include "zmq.hpp"
#endif


class SpillLogFlags
{
public:
   bool fPrint = false;
   bool fSendZMQ = false;
};

std::thread fSpillLogMessageSenderThread;

class SpillLogMessageSender
{
   public:
#if HAVE_ZERO_MQ
   zmq::context_t context;
   zmq::socket_t publisher;
#endif
   std::mutex fSpillMessageQueueMutex;
   std::deque<std::string> fSpillMessageQueue;


   bool fRunning = false;
   bool fShutdown = false;


   SpillLogMessageSender()
#if HAVE_ZERO_MQ
      : context(1), publisher(context, ZMQ_PUB)
   {
      std::cout << __FILE__ << " " << __LINE__ << std::endl;
      publisher.bind("tcp://*:5556");
      std::cout << __FILE__ << " " << __LINE__ << std::endl;
#else
   {
#endif
   }

   void Start()
   {
      fRunning = true;
      fSpillLogMessageSenderThread = std::thread(&SpillLogMessageSender::Run, this);
   }
   void Send(__attribute__((unused)) const std::string& message)
   {
#if HAVE_ZERO_MQ
      zmq::message_t msg(message.size());
      memcpy(msg.data(), message.data(), message.size());
      publisher.send(msg, zmq::send_flags::dontwait);
#endif
   }

   void Queue(__attribute__((unused)) const std::string& message)
   {
      if (!fRunning)
      {
         std::cout << "SpillLogMessageSender not running! Message not sent!" << std::endl;
         return;
      }
#if HAVE_ZERO_MQ
      std::lock_guard<std::mutex> lock(fSpillMessageQueueMutex);
      fSpillMessageQueue.push_back(message);
#endif
   }

   void SendMessage()
   {
      std::string message;
      // Lock scope
      {
         std::lock_guard<std::mutex> lock(fSpillMessageQueueMutex);
         if (fSpillMessageQueue.empty())
            return;
         message = std::move(fSpillMessageQueue.front());
         fSpillMessageQueue.pop_front();
      }
      // Unlock then send
      Send(message);
   }

   void Shutdown()
   {
      fShutdown = true;
      if (fSpillLogMessageSenderThread.joinable()) fSpillLogMessageSenderThread.join();
   }

   ~SpillLogMessageSender()
   {
#if HAVE_ZERO_MQ
      publisher.close();
#endif
   }

   void Run()
   {
      while (!fShutdown)
      {
#if HAVE_ZERO_MQ
         SendMessage();
         usleep(100);
#else
         std::cout << "ZeroMQ not available! Please turn off ZMQ transmission" << std::endl;
         usleep(1000000);
#endif
      }
   }
};



class SpillLogZeroMQAnnouncer: public TARunObject
{
public: 
   SpillLogFlags* fFlags;
   SpillLogMessageSender fSpillLogMessageSender;

   bool fTrace = false;
   

public:

   SpillLogZeroMQAnnouncer(TARunInfo* runinfo, SpillLogFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
      fModuleName = "ZeroMQAnnouncer";
      if (fTrace)
         printf("SpillLog::ctor!\n");
      if (fFlags->fSendZMQ)
         fSpillLogMessageSender.Start();
   }

   ~SpillLogZeroMQAnnouncer()
   {
      if (fTrace)
         printf("SpillLog::dtor!\n");
      fSpillLogMessageSender.Shutdown();
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("SpillLog::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());

      std::string run_number_info = "{ \"Runinfo/Run number\" : " + std::to_string(runinfo->fRunNo) + " }";
      fSpillLogMessageSender.Queue(run_number_info);

      std::string start_time;
      runinfo->fOdb->RS("Runinfo/Start time", &start_time);
      std::string start_time_info = "{ \"Runinfo/Start time\" : \"" + start_time + "\" }";
      fSpillLogMessageSender.Queue(start_time_info);

      TSISChannels* sisch = new TSISChannels(runinfo->fRunNo);

      std::string channel_names = "{ \"SIS_CHANNELS\": [";
      for (int i = 0; i < NUM_SIS_CHANNELS * NUM_SIS_MODULES; i++)
      {
         channel_names += "\"";
         channel_names += sisch->GetDescription(i, runinfo->fRunNo);
         channel_names += "\"";
         if (i != - NUM_SIS_CHANNELS * NUM_SIS_MODULES - 1)
            channel_names += ",";
      }
      delete sisch;
      channel_names += "]}";

      fSpillLogMessageSender.Queue(channel_names);

      std::string seq_names = "{ \"SEQUENCERS\": [";
      for (int i = 0; i < NUMSEQ; i++)
      {
         seq_names += "\"";
         seq_names += SEQ_NAMES_SHORT[i];
         seq_names += "\"";
         if (i != NUMSEQ - 1)
            seq_names += ",";
      }
      seq_names += "]}";

      fSpillLogMessageSender.Queue(seq_names);
   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("SpillLog::EndRun, run %d\n", runinfo->fRunNo);
      std::string stop_time;
      runinfo->fOdb->RS("Runinfo/Stop time", &stop_time);
      std::string stop_time_info = "{ \"Runinfo/Stop time\" : \"" + stop_time + "\" }";
      fSpillLogMessageSender.Queue(stop_time_info);

   }

   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("SpillLog::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }

   TAFlowEvent* AnalyzeFlowEvent(__attribute__((unused)) TARunInfo* runinfo, __attribute__((unused)) TAFlags* flags, TAFlowEvent* flow)
   {
      const TInfoSpillFlow* TInfoFlow= flow->Find<TInfoSpillFlow>();
      if (TInfoFlow)
      {
         for (const TInfoSpill* s: TInfoFlow->spill_events)
         {
            fSpillLogMessageSender.Queue(s->toJson());
         }
      }
      const A2SpillFlow* SpillFlow= flow->Find<A2SpillFlow>();
      if (SpillFlow)
      {
         for (const TA2Spill* s: SpillFlow->spill_events)
         {
            fSpillLogMessageSender.Queue(s->toJson());
         }
      }
      return flow;
   }

   void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event)
   {
      if (fTrace)
         printf("SpillLog::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", 
                runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
   }
};

class SpillLogZMQAnnoucerFactory: public TAFactory
{
public:
   SpillLogZMQAnnoucerFactory(): TAFactory()
   {
   }
   SpillLogFlags fFlags;

public:
   void Usage()
   {
      std::cout<<"SpillLogZMQAnnoucerFactory::Help!"<<std::endl;
      std::cout<<"\t--zmq\t\tAnnounce ZeroMQ messages"<<std::endl;
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("SpillLogZMQAnnoucerFactory::Init!\n");
      

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
         if (args[i] == "--zmq")
            fFlags.fSendZMQ = true;
      }
 
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("SpillLogZMQAnnoucerFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("SpillLogZMQAnnoucerFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new SpillLogZeroMQAnnouncer(runinfo, &fFlags);
   }
};

static TARegister tar(new SpillLogZMQAnnoucerFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
