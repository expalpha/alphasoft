//
// SVD data montior, uses a circular buffer to histogram 
//
// JTK McKENNA
//

#include <stdio.h>
#include <stdint.h>
#include <map>

#include "manalyzer.h"
#include "midasio.h"

#include "A2Flow.h"
#include "TAlphaEvent.h"
#include "TSiliconEvent.h"

#include "TStyle.h"
#include "TColor.h"
#include "TF2.h"
#include "TExec.h"
#include "TCanvas.h"
#include "TPaveText.h"


#include "TCanvas.h"

#include "TSISChannels.h"

#define USE_RCT 0

enum LivePlotType {
   kCounts,
   kRate,
   kBackgroundSubtracted,
   kTotalTime
};
enum DetectorType {
   kTrigger,
   kReadout,
   kPassCut,
   kMVA
};

class SpillLogMonitor: public TARunObject
{
private:
#if USE_RCT
   TCanvas fRCTCountCanvas;
#endif
   TCanvas fATMCountCanvas;
   int fIO32_NOBUSY;
   int fIO32;
   class table_entry{
      const double fTriggerRate = 9.8;
      const double fPassCutRate = 45E-3;
      const double fMVARate = 7E-3;
      int fVF48Trig = 0;
      int fVF48Read = 0;
      int fPassCut = 0;
      int fMVA = 0;
      double fTotalTime = 0;
      int fCount = 1;
      public:
      table_entry()
      {
         fVF48Trig = 0;
         fVF48Read = 0;
         fPassCut = 0;
         fMVA = 0;
         fTotalTime = 0;
         fCount = 1;
      }
      table_entry(const TA2Spill* spill,const int trig_chan, const int read_chan): table_entry()
      {
         if (!spill->fScalerData)
            return;
         fVF48Trig  = spill->fScalerData->fDetectorCounts[trig_chan];
         fVF48Read  = spill->fScalerData->fDetectorCounts[read_chan];
         fPassCut   = spill->fScalerData->fPassCuts;
         fMVA       = spill->fScalerData->fPassMVA;
         fTotalTime = spill->GetStopTime() - spill->GetStartTime();
      }
      int operator+=(const table_entry& e)
      {
         fVF48Trig  += e.fVF48Trig;
         fVF48Read  += e.fVF48Read;
         fPassCut   += e.fPassCut;
         fMVA       += e.fMVA;
         fTotalTime += e.fTotalTime;
         fCount     += e.fCount;
         return 0;
      }

      double GetTypeDetector(const LivePlotType plot_type, const DetectorType detector) const
      {
         switch (plot_type)
         {
         case kCounts:
            return this->Counts(detector);
            break;
         case kRate:
            return this->Rate(detector);
         case kBackgroundSubtracted:
            return this->BackgroundSubtracted(detector);
         case kTotalTime:
            return fTotalTime;
         default:
            break;
         }
         return 0;
      }
      double Counts(const DetectorType detector) const {
         switch (detector)
         {
            case kTrigger:
               return fVF48Trig;
            case kReadout:
               return fVF48Read;
            case kPassCut:
               return fPassCut;
            case kMVA:
               return fMVA;
         }
         return 0;
      }
      double Rate(const DetectorType detector) const {
         switch (detector)
         {
            case kTrigger:
               return fVF48Trig / fTotalTime;
            case kReadout:
               return fVF48Read / fTotalTime;
            case kPassCut:
               return fPassCut / fTotalTime;
            case kMVA:
               return fMVA / fTotalTime;
         }
         return 0;
      }
      double BackgroundSubtracted(const DetectorType detector) const {
         switch (detector)
         {
            case kTrigger:
               return fVF48Trig - fTotalTime * fTriggerRate;
            case kReadout:
               return fVF48Read - fTotalTime * fTriggerRate;
            case kPassCut:
               return fPassCut - fTotalTime * fPassCutRate;
            case kMVA:
               return fMVA - fTotalTime * fMVARate;
         }
         return 0;
      }
      std::string title_line() const
      {
         return "Time\tCounts\tTrigs\tReads\tPassCut\tMVA\t";
      }
      std::string count_line() const
      {
         std::string line;
         line += std::to_string(fTotalTime) + "\t";
         line += std::to_string(fCount) + "\t";
         line += std::to_string(fVF48Trig) + "\t";
         line += std::to_string(fVF48Read) + "\t";
         line += std::to_string(fPassCut) + "\t";
         line += std::to_string(fMVA) + "\t";
         return line;
      }
      std::string rate_line() const
      {
         std::string line;
         line += std::to_string(fTotalTime) + "\t";
         line += std::to_string(fCount) + "\t";
         line += std::to_string(fVF48Trig/fTotalTime) + "\t";
         line += std::to_string(fVF48Read/fTotalTime) + "\t";
         line += std::to_string(fPassCut/fTotalTime) + "\t";
         line += std::to_string(fMVA/fTotalTime) + "\t";
         return line;
      }
      friend std::ostream& operator<< (std::ostream& stream, const table_entry& t)
      {
         stream << t.fCount << "\t" << t.fTotalTime << "\t" << t.fVF48Trig << "\t" << t.fVF48Read << "\t" << t.fPassCut << "\t" << t.fMVA;
         return stream;
      }
   };
#if USE_RCT
   std::map<std::string,table_entry> fRCTTotalTable;
#endif
   std::map<std::string,table_entry> fATMTotalTable;

#if USE_RCT   
   std::vector<std::vector<TH1D>> fRCTHisto;
#endif
   std::vector<std::vector<TH1D>> fATMHisto;

public:
   bool fTrace = false;
   SpillLogMonitor(TARunInfo* runinfo)
      : TARunObject(runinfo),
#if USE_RCT
         fRCTCountCanvas("RCTSpillTable","RCTSpillTable"),
#endif
         fATMCountCanvas("ATMSpillTable","ATMSpillTable")

   {
      fModuleName = "SpillLogMonitor";
#if USE_RCT
      fRCTHisto.resize(sizeof(LivePlotType));
#endif
      fATMHisto.resize(sizeof(LivePlotType));
      for (size_t i = 0; i < sizeof(LivePlotType); i++)
      {
#if USE_RCT
         fRCTHisto.at(i).resize(sizeof(DetectorType));
#endif
         fATMHisto.at(i).resize(sizeof(DetectorType));
      }
   }
   ~SpillLogMonitor()
   {
      if (fTrace)
         printf("SpillLogMonitor::dtor!\n");
   }
   

   void BeginRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("SpillLogMonitor::BeginRun %d!\n",runinfo->fRunNo);
      TSISChannels* sisch = new TSISChannels(runinfo->fRunNo);
#if LIVE_DUMP_MARKERS
      for (int j=0; j<USED_SEQ; j++) 
      {
         DumpStartChannels[j] =SISChannels->GetChannel(StartNames[j],runinfo->fRunNo);
         DumpStopChannels[j]  =SISChannels->GetChannel(StopNames[j], runinfo->fRunNo);
      }
#endif
      fIO32_NOBUSY = sisch->GetChannel("IO32_TRIG_NOBUSY").toInt();
      fIO32 = sisch->GetChannel("IO32_TRIG").toInt();
      delete sisch;
#if USE_RCT
      fRCTTotalTable.clear();
#endif
      fATMTotalTable.clear();

      for (size_t plot_type = 0; plot_type < sizeof(LivePlotType); plot_type++)
      {
         for (size_t detector = 0; detector < sizeof(DetectorType); detector++)
         {
            std::string histo_name = "";
            std::string histo_title = "";
            switch (detector)
            {
            case kTrigger:
               histo_name += "IO32_NOBUSY";
               histo_title += "SVD Triggers ";
               break;
            case kReadout:
               histo_name += "IO32";
               histo_title += "SVD Reads ";
               break;
            case kPassCut:
               histo_name += "PassCut";
               histo_title += "Passed Cuts ";
               break;
            case kMVA:
               histo_name += "PassMVA";
               histo_title += "Passed MVA ";
               break;
            default:
               std::cerr << "Unknown detector in DetectorType enum" << std::endl;
               break;
            }
            switch (plot_type)
            {
            case kCounts:
               histo_name += "Counts";
               histo_title += "Counts; Dump; Counts";
               break;
            case kRate:
               histo_name += "Rate";
               histo_title += "Rate; Dump; Counts/s";
               break;
            case kBackgroundSubtracted:
               histo_name += "Background Subtracted Counts";
               histo_title += "Background Subtracted Counts; Dump; Counts";
               break;
            case kTotalTime:
               histo_name += "Total Time";
               histo_title += "Total Time; Dump; s";
               break;
            default:
               std::cerr << "Unknown plot type in LivePlotType enum" << std::endl;
               break;
            }
#if USE_RCT
            std::string rct_name = std::string("RCT ") + histo_name;
            std::string rct_title = std::string("RCT ") + histo_title;
            fRCTHisto.at(plot_type).at(detector) = TH1D(rct_name.c_str(),rct_title.c_str(),0,0,0);
#endif
            std::string atm_name = std::string("ATM ") + histo_name;
            std::string atm_title = std::string("ATM ") + histo_title;
            fATMHisto.at(plot_type).at(detector) = TH1D(atm_name.c_str(),atm_title.c_str(),0,0,0);
         }
      }
      //fATMCountCanvas.Divide(1,2);
      gDirectory->cd();
   }
  
   TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow)
   {
      if (fTrace)
         printf("SpillLogMonitor::Analyze, run %d, event serno %d, id 0x%04x, data size %d\n", runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
      return flow;
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      if (fTrace)
         printf("SpillLogMonitor::AnalyzeFlowEvent, run %d\n", runinfo->fRunNo);

      const A2SpillFlow* SpillFlow= flow->Find<A2SpillFlow>();
      if (!SpillFlow)
      {
         *flags|=TAFlag_SKIP_PROFILE;
         return flow;
      }
      for (size_t i=0; i<SpillFlow->spill_events.size(); i++)
      {
         TA2Spill* s=SpillFlow->spill_events.at(i);
         //if (fTotalTable.count(s->fName))
         if (s->fName.size() && s->fIsDumpType)
         {
#if USE_RCT
            if (
               // Is RCT
               s->fSeqData->fSequenceNum == 1 )
               fRCTTotalTable[s->fName] += table_entry(s,fIO32_NOBUSY,fIO32);
#endif
            if (
               // Is atom trap
               s->fSeqData->fSequenceNum == 2)
               fATMTotalTable[s->fName]+= table_entry(s,fIO32_NOBUSY,fIO32);
         }
      }
      if ( SpillFlow->spill_events.empty() )
         return flow;

      // Clear existing histograms
      for (size_t plot_type = 0; plot_type < sizeof(LivePlotType); plot_type++)
      {
         for (size_t detector = 0; detector < sizeof(DetectorType); detector++)
         {
#if USE_RCT
            TH1D* rct = &fRCTHisto.at(plot_type).at(detector);
            rct->Reset();
            rct->SetCanExtend(TH1::kAllAxes);
            rct->SetStats(0);
#endif
            TH1D* atm = &fATMHisto.at(plot_type).at(detector);
            atm->Reset();
            atm->SetCanExtend(TH1::kAllAxes);
            atm->SetStats(0);
         }
      }
      // Fill existing histograms
#if USE_RCT
      for (const auto& p: fRCTTotalTable)
      {
         for (size_t plot_type = 0; plot_type < sizeof(LivePlotType); plot_type++)
         {
            for (size_t detector = 0; detector < sizeof(DetectorType); detector++)
            {
               fRCTHisto[plot_type][detector].Fill(p.first.c_str(), p.second.GetTypeDetector((LivePlotType)plot_type,(DetectorType)detector));
            }
         }
      }
#endif
      for (const auto& p: fATMTotalTable)
      {
         for (size_t plot_type = 0; plot_type < sizeof(LivePlotType); plot_type++)
         {
            for (size_t detector = 0; detector < sizeof(DetectorType); detector++)
            {
               fATMHisto[plot_type][detector].Fill(p.first.c_str(), p.second.GetTypeDetector((LivePlotType)plot_type,(DetectorType)detector));
            }
         }
      }

      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#if USE_RCT
      fRCTCountCanvas.cd(1);
      fRCTHisto[kRate][DetectorType::kTrigger].Draw("hist");
      fRCTCountCanvas.cd(2);
      fRCTHisto[kRate][DetectorType::kPassCut].Draw("hist");
      fRCTCountCanvas.cd(3);
      fRCTHisto[kRate][DetectorType::kMVA].Draw("hist");
      fRCTCountCanvas.cd(4);
      fRCTHisto[kBackgroundSubtracted][DetectorType::kPassCut].Draw("hist");
      fRCTCountCanvas.cd(5);
      fRCTHisto[kBackgroundSubtracted][DetectorType::kMVA].Draw("hist");
      fRCTCountCanvas.cd(6);
      fRCTHisto[kTotalTime][DetectorType::kTrigger].Draw("hist");
      fRCTCountCanvas.Update();
      fRCTCountCanvas.Draw();
#endif


      //fATMCountCanvas.cd(1);
      //fATMHisto[kCounts][DetectorType::kTrigger].Draw("hist");
      //fATMCountCanvas.cd(2);
      //fATMHisto[kCounts][DetectorType::kPassCut].Draw("hist");
      //fATMCountCanvas.cd(3);
      //fATMHisto[kCounts][DetectorType::kMVA].Draw("hist");
      //fATMCountCanvas.cd(4);
      fATMCountCanvas.cd();
      fATMHisto[kBackgroundSubtracted][DetectorType::kPassCut].Draw("hist");
      //fATMCountCanvas.cd(5);
      //fATMHisto[kBackgroundSubtracted][DetectorType::kMVA].Draw("hist");
      //fATMCountCanvas.cd(6);
      //fATMHisto[kTotalTime][DetectorType::kTrigger].Draw("hist");
      fATMCountCanvas.Draw();



      //fLiveCanvas.Draw();
      return flow;
   }

   void EndRun(TARunInfo* /*runinfo*/)
   {
      std::cout << "SVD Monitor end run\n";
#if USE_RCT
      std::cout << "RCT:" << std::endl;
      for (const auto& p: fRCTTotalTable)
      {
         std::cout << p.first << ":\t" << p.second << std::endl;
      }
#endif
      std::cout << "ATM:" << std::endl;
      for (const auto& p: fATMTotalTable)
      {
         std::cout << p.first << ":\t" << p.second << std::endl;
      }


   }
};



static TARegister tar1(new TAFactoryTemplate<SpillLogMonitor>);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
