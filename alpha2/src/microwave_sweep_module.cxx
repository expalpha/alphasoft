//
// ALPHA 2 (SVD AND SIS) microwave_sweep_module
//
// JTK McKenna
//


#include <list>
#include <stdio.h>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include "manalyzer.h"
#include "midasio.h"
#include "TSystem.h"
#include <TEnv.h>

#include "AnalysisFlow.h"
#include "A2Flow.h"
#include "TTree.h"

#include "GEM_BANK.h"
#include "GEM_BANK_flow.h"
#include "time.h"
#include <vector>

#ifdef HAVE_MIDAS
#include "midas.h"
#include "msystem.h"
#include "mrpc.h"
#endif


#include <deque>


#ifdef HAVE_XMLSERVER
#include "xmlServer.h"
#include "TROOT.h"
#endif

#ifdef XHAVE_LIBNETDIRECTORY
#include "netDirectoryServer.h"
#endif

#include "TSISChannels.h"

class MicrowaveDumpLogFlags
{
public:
   bool fPrint = false;
   bool fWriteElog = false;
   bool fWriteSpillDB = false;
   bool fWriteSpillTxt = false;
   bool fNoSpillSummary = false;
   bool fOnlineMicrowaveDumpLog = false;
};

class MicrowaveDataTable
{
   class WindowCounts
   {
      double fStartTime = -1.;
      double fStopTime  = -1.;
      bool fIsSweep; // False is between sweeps
      int fVF48Triggers = 0;
      int fVF48Reads    = 0;
      int fPassCut      = 0;
      int fPassMVA      = 0;
      public:
      WindowCounts(const bool is_sweep, const double start_time):
         fStartTime(start_time),
         fIsSweep(is_sweep)
      {
      }
      bool GetIsSweep() const { return fIsSweep; }
      void SetStopTime(const double stop_time)
      {
         fStopTime = stop_time;
      }
      void AddSISCounts(const int triggers, const int reads)
      {
         fVF48Triggers += triggers;
         fVF48Reads    += reads;
      }
      void AddVertCounts(const int passcuts, const int passmva)
      {
         fPassCut += passcuts;
         fPassMVA += passmva;
      }
      double GetStartTime() const { return fStartTime; }
      double GetStopTime() const { return fStopTime; }
      bool TimeInRange(const double t) const {
         if (t >= fStartTime && t < fStopTime )
            return true;
         if (t >= fStartTime && fStopTime < 0.)
            return true;
         return false;
      }
      const std::string CountLine() const
      {
         return
            std::to_string(fVF48Triggers) + std::string("\t") +
            std::to_string(fVF48Reads) + std::string("\t") +
            std::to_string(fPassCut) + std::string("\t") +
            std::to_string(fPassMVA) + std::string("\t");
      }
      void Print() const
      {
         std::cout << fStartTime << "\t" << fStopTime << "\t" << CountLine() << std::endl;
      }
   };
   std::deque<WindowCounts> fWindows;
   std::vector<double> fLaserFreq;
   std::vector<double> fLaserPow;

   public:
      void SetFrequencies( const std::vector<double>& data)
      {
         fLaserFreq = data;
      }
      void SetPower(const std::vector<double>& data)
      {
         fLaserPow = data;
      }

   void AddStartMicrowave(const double start_time)
   {
      fWindows.emplace_back(false, start_time);
   }
   void AddStopMicrowave(const double stop_time)
   {
      if (fWindows.size())
         fWindows.back().SetStopTime(stop_time);
      else
         fWindows.emplace_back(true, stop_time);
   }
   void AddSISCounts(const double runtime, const int triggers, const int reads)
   {
      for (auto& w: fWindows)
      {
         if (w.TimeInRange(runtime))
            return w.AddSISCounts(triggers,reads);
      }
   }
   void AddVertCounts(const double runtime, const int passcuts, const int passmva)
   {
      for (auto& w: fWindows)
      {
         if (w.TimeInRange(runtime))
            return w.AddVertCounts(passcuts, passmva);
      }
   }
   void Clear()
   {
      fWindows.clear();
      fLaserFreq.clear();
      fLaserPow.clear();
   }
   void AddBank(const GEMBANK<double>* bank)
   {
      for (uint32_t i=0; i<bank->NumberOfEntries; i++)
      {
         const GEMDATA<double>* gemdata = bank->GetDataEntry(i);
         if (bank->GetCategoryName() == "MicrowaveSynth" && bank->GetSizeOfDataArray())
         {
            if (bank->GetVariableName() == "Frequencies")
            {
               int entries = gemdata->GetEntries(bank->BlockSize);
               std::vector<double> data;
               data.reserve(entries);
               for (int i = 0; i < entries; i++) {
                  data.push_back(*gemdata->DATA(i));
               }
               this->SetFrequencies(data);
            }
            if (bank->GetVariableName() == "Power")
            {
               int entries = gemdata->GetEntries(bank->BlockSize);
               std::vector<double> data;
               data.reserve(entries);
               for (int i = 0; i < entries; i++) {
                  data.push_back(*gemdata->DATA(i));
               }
               this->SetPower(data);
            }
         }
      }
   }
   std::string Title()
   {
      return std::string("\t\t\tFreqIndex\tFreq\tPow\tVF48Trig\tVF48Read\tPassCut\tMVA");
   }
   std::vector<std::string> BuildCountTable(const double start, const double stop)
   {
      if(fLaserPow.size()==0)
      {
         return std::vector<std::string>();
      }
      std::vector<std::string> table;
      int freq_index = 0;
      for (const auto& w: fWindows)
      {
         if (w.GetStopTime() < start)
            continue;
         if (w.GetStartTime() > stop)
            break;
         //if (w.GetIsSweep())
         table.emplace_back(
            std::string("[") + std::to_string(w.GetStartTime()) + "-" + std::to_string(w.GetStopTime()) + "]\t" +
            std::to_string(freq_index - 1) + "\t" + 
            std::to_string(fLaserFreq.at(freq_index)) + "\t" +
            std::to_string(fLaserPow.at(freq_index)) + "\t" + 
            w.CountLine()
            );
         freq_index++;
      }
      return table;
   }
   void DrawCountTable(const double start, const double stop)
   {
      auto table = BuildCountTable(start,stop);
      for (const auto& t: table)
         std::cout << t << std::endl;
   }


};

class MicrowaveDumpLog: public TARunObject
{
public: 
   MicrowaveDumpLogFlags* fFlags;
   bool fTrace = false;


   TSISChannel MicSynthStart;
   TSISChannel MicSynthStop;
   TSISChannel VF48Trigs;
   TSISChannel VF48Reads;

   MicrowaveDataTable fMicrowaveTable;

   std::vector<std::string> fInMemorySpillTable;

   MicrowaveDumpLog(TARunInfo* runinfo, MicrowaveDumpLogFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="MicrowaveDumpLog";
#endif
      if (fTrace)
         printf("MicrowaveDumpLog::ctor!\n");
   }

   ~MicrowaveDumpLog()
   {
      if (fTrace)
         printf("MicrowaveDumpLog::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      fMicrowaveTable.Clear();
      TSISChannels SISChannels( runinfo->fRunNo );
      MicSynthStart = SISChannels.GetChannel("MIC_SYNTH_STEP_START",runinfo->fRunNo);
      MicSynthStop  = SISChannels.GetChannel("MIC_SYNTH_STEP_STOP", runinfo->fRunNo);
      VF48Trigs     = SISChannels.GetChannel("IO32_TRIG_NOBUSY", runinfo->fRunNo);
      VF48Reads     = SISChannels.GetChannel("IO32_TRIG", runinfo->fRunNo);
   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("MicrowaveDumpLog::EndRun, run %d\n", runinfo->fRunNo);
      fMicrowaveTable.Clear();
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("MicrowaveDumpLog::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {

      const SISEventFlow* SISFlow           = flow->Find<SISEventFlow>();
      const SVDQODFlow* SVDFlow             = flow->Find<SVDQODFlow>();
      const GEMBANK_Flow* GEMFlow           = flow->Find<GEMBANK_Flow>();
      const GEMBANKARRAY_Flow* GEMArrayFlow = flow->Find<GEMBANKARRAY_Flow>();
      const A2SpillFlow* SpillFlow          = flow->Find<A2SpillFlow>();
      if ( !SISFlow && !SVDFlow && !GEMFlow &&  !GEMArrayFlow && !SpillFlow)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         //Nothing to do here
         return flow;   
      }

      if (SISFlow)
      {

         for (const std::vector<std::shared_ptr<TSISEvent>>& ce : SISFlow->sis_events)
         for (const std::shared_ptr<TSISEvent>& e: ce)
         {
            const int starts = e->GetCountsInChannel(MicSynthStart);
            const int stops =  e->GetCountsInChannel(MicSynthStop);
            const int trigs =  e->GetCountsInChannel(VF48Trigs);
            const int reads =  e->GetCountsInChannel(VF48Reads);

            for (int j = 0; j < starts; j++)
               fMicrowaveTable.AddStartMicrowave(e->GetRunTime());
            for (int j = 0; j < stops; j++)
               fMicrowaveTable.AddStopMicrowave(e->GetRunTime());
            if (trigs || reads)
               fMicrowaveTable.AddSISCounts(e->GetRunTime(),trigs,reads);
         }
      }

      if (SVDFlow)
      {
         for (const auto& e: SVDFlow->SVDQODEvents)
         {
            fMicrowaveTable.AddVertCounts(
               e->GetRunTime(),
               (int)e->GetOnlinePassCuts(1),
               (int)e->GetOnlinePassCuts(2)
            );
         }
      }

      if (GEMFlow)
      {
         // On 3rd July 2023 this changed from a float to a double in the ODB
         const GEMBANK<double> *bank = (const GEMBANK<double>*) GEMFlow->data;
         fMicrowaveTable.AddBank(bank);
      }

      if (GEMArrayFlow)
      {
         GEMBANKARRAY *array = GEMArrayFlow->data;
         int bank_no = 0;
          // On 3rd July 2023 this changed from a float to a double in the ODB
         GEMBANK<double>* bank = (GEMBANK<double>*) array->GetGEMBANK(bank_no);
         while(bank)
         {
            fMicrowaveTable.AddBank(bank);
            bank = (GEMBANK<double>*) array->GetGEMBANK(++bank_no);
         }
      }


      if (SpillFlow)
      {
         for (size_t i=0; i<SpillFlow->spill_events.size(); i++)
         {
            const TA2Spill* s = SpillFlow->spill_events.at(i);
            if (s->IsMatchForDumpName("Microwave Dump"))
            {
               TInfoSpillFlow* infoflow = flow->Find<TInfoSpillFlow>();
               if (!infoflow)
                  infoflow = new TInfoSpillFlow(flow);
               flow = infoflow;
               std::vector<std::string> table = fMicrowaveTable.BuildCountTable(s->GetStartTime(), s->GetStopTime());
               if (table.size())
                  infoflow->spill_events.push_back(new TInfoSpill(runinfo->fRunNo,0.,0.,"\t\t\t\t%s",fMicrowaveTable.Title().c_str()));
               for (const auto& t: table)
               {
                  infoflow->spill_events.push_back(new TInfoSpill(runinfo->fRunNo,0.,0.,"\t\t\t\t%s",t.c_str()));
               }
               fMicrowaveTable.Clear();
               break;
            }
         }
      }

      return flow;
   }

   void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event)
   {
      if (fTrace)
         printf("MicrowaveDumpLog::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", 
                runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
   }
};

class MicrowaveDumpLogFactory: public TAFactory
{
public:
   MicrowaveDumpLogFactory(): TAFactory()
   {
      gEnv->SetValue("Gui.DefaultFont","-*-courier-medium-r-*-*-12-*-*-*-*-*-iso8859-1");  
   }
   MicrowaveDumpLogFlags fFlags;

public:
   void Usage()
   {
      std::cout<<"MicrowaveDumpLogFactory::Help!"<<std::endl;
      std::cout<<"\t--elog\t\tWrite elog (not implemented)"<<std::endl;
      std::cout<<"\t--spilldb\t\tSwrite to Spill log sqlite database (local)"<<std::endl;
      std::cout<<"\t--spilltxt\t\tWrite Spill log to MicrowaveDumpLog/reload.txt"<<std::endl;
      std::cout<<"\t--nospillsummary\t\tTurn off spill log table printed at end of run"<<std::endl;
      std::cout<<"\t--onlinespills\t\tWrite spills live to MicrowaveDumpLog in midas"<<std::endl;
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("MicrowaveDumpLogFactory::Init!\n");
      

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
         if (args[i] == "--elog")
            fFlags.fWriteElog = true;
         if (args[i] == "--spilldb")
            fFlags.fWriteSpillDB = true;
         if (args[i] == "--spilltxt")
            fFlags.fWriteSpillTxt = true;
         if (args[i] == "--nospillsummary")
            fFlags.fNoSpillSummary = true;
         if (args[i] == "--onlinespills")
            fFlags.fOnlineMicrowaveDumpLog = true;
      }
 
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("MicrowaveDumpLogFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("MicrowaveDumpLogFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new MicrowaveDumpLog(runinfo, &fFlags);
   }
};

static TARegister tar(new MicrowaveDumpLogFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
