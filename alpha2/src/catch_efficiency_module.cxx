//
// Slow down flow to real time (testing module)
//
// JTK McKENNA
//

#include <stdio.h>

#include "manalyzer.h"
#include "midasio.h"
#include "A2Flow.h"

#include "TSISChannels.h"

#ifdef HAVE_MIDAS
#include "midas.h"
#include "msystem.h"
#include "mrpc.h"
#endif

#include <iostream>
class CatchEfficiencyModuleFlags
{
public:
   bool fPrint = false;

};
class CatchEfficiencyModule: public TARunObject
{
private:
   clock_t start_time;
   TA2Spill* HotDump=NULL;
   TA2Spill* ColdDump=NULL;
   int fCTDetector = -1;
   int fIO32_NOBUSY = -1;

public:
   CatchEfficiencyModuleFlags* fFlags;
   bool fTrace = false;
   
   
   CatchEfficiencyModule(TARunInfo* runinfo, CatchEfficiencyModuleFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Catch Efficiency";
#endif
      if (fTrace)
         printf("CatchEfficiencyModule::ctor!\n");
   }

   ~CatchEfficiencyModule()
   {
      if (fTrace)
         printf("CatchEfficiencyModule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("CatchEfficiencyModule::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      start_time=clock();
      
      TSISChannels* sisch = new TSISChannels(runinfo->fRunNo);
      fCTDetector = sisch->GetChannel("CT_SiPM_OR").toInt();
      fIO32_NOBUSY = sisch->GetChannel("IO32_TRIG_NOBUSY").toInt();
      delete sisch;
    }

   void EndRun(TARunInfo* runinfo)
   {
      if (HotDump)
         delete HotDump;
      if (ColdDump)
         delete ColdDump;
      if (fTrace)
         printf("CatchEfficiencyModule::EndRun, run %d\n", runinfo->fRunNo);
   }
   
   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("CatchEfficiencyModule::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
  {
      A2SpillFlow* SpillFlow= flow->Find<A2SpillFlow>();
      if (SpillFlow)
      {
         if (fTrace)
            printf("CatchEfficiencyModule::AnalyzeFlowEvent, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      
         for (size_t i=0; i<SpillFlow->spill_events.size(); i++)
         {
            TA2Spill* s=SpillFlow->spill_events.at(i);
            //s->Print();
            if (!s->fSeqData) continue;
            int thisSeq=s->fSeqData->fSequenceNum;
#if HAVE_MIDAS
            if (strcmp(s->fName.c_str(),"\"FastRampDown\"")==0)
            {
               if (s->fScalerData)
               {
                  if (s->fScalerData->fPassCuts>0 && s->fScalerData->fPassCuts<100)
                  {
                     std::cout << "SUCCESS " <<s->fScalerData->fPassCuts << std::endl;
                     cm_msg(MTALK, "alpha2online","Not bad! %d passed cuts",s->fScalerData->fPassCuts);
                  }
                  if (s->fScalerData->fPassCuts>=100 && s->fScalerData->fPassCuts<150)
                  {
                     std::cout << "SUCCESS " <<s->fScalerData->fPassCuts << std::endl;
                     cm_msg(MTALK, "alpha2online","Success! %d passed cuts",s->fScalerData->fPassCuts);
                  }
                  if (s->fScalerData->fPassCuts>=150 && s->fScalerData->fPassCuts<200)
                  {
                     std::cout << "SUCCESS " <<s->fScalerData->fPassCuts << std::endl;
                     cm_msg(MTALK, "alpha2online","Wow! %d passed cuts",s->fScalerData->fPassCuts);
                  }
                  if (s->fScalerData->fPassCuts>=200)
                  {
                     std::cout << "SUCCESS " <<s->fScalerData->fPassCuts << std::endl;
                     cm_msg(MTALK, "alpha2online","Fast Ramp Down! %d passed cuts",s->fScalerData->fPassCuts);
                  }
                  else
                  {
                     cm_msg(MTALK, "alpha2online","Sorry Neils, no passed cuts");
                  }
               }
            }
            if (strcmp(s->fName.c_str(),"\"Mixing\"")==0)
            {
               if (s->fScalerData)
                  if (fIO32_NOBUSY > 0)
                     if (s->fScalerData->fDetectorCounts.at(fIO32_NOBUSY))
                     {
                        int counts = s->fScalerData->fDetectorCounts.at(fIO32_NOBUSY);
                        double factor = 0;
                        // This rounding method breaks down when we have around than 10M triggers...
                        if (counts < 100000)
                           factor = pow(10.0, 2 - ceil(log10(fabs(counts))));
                        else
                           factor = pow(10.0, 3 - ceil(log10(fabs(counts))));
                        counts = round(counts * factor) / factor;   
                        cm_msg(MTALK, "alpha2online","%d mixing triggers",counts);
                      }
            }
#endif
            if (thisSeq==0) //Catching trap
            //if (strcmp(s->SeqName.c_str(),"cat")==0)
            if (strcmp(s->fName.c_str(),"\"Hot Dump\"")==0)
            {
               if (HotDump)
                  delete HotDump;
               //Hot dump happens before cold dump... so if something exists in memory... its out of date
               if (ColdDump)
               {
                  delete ColdDump;
                  ColdDump = NULL;
               }
               HotDump = new TA2Spill(*s);
#if HAVE_MIDAS
               // To do: Replace '54' with a look up to channel number
               if (HotDump->fScalerData)
                  if (fCTDetector > 0)
                     if (HotDump->fScalerData->fDetectorCounts.at(fCTDetector) < 5000)
                         cm_msg(MTALK, "alpha2online","Warning: Hot Dump is Low");
#endif
            }
            if (strcmp(s->fName.c_str(),"\"Cold Dump\"")==0)
            {
               if (ColdDump)
                  delete ColdDump;
               ColdDump = new TA2Spill(*s);
               if (HotDump)
               {
                  std::cout<<"catch_efficiency_module::CalculateCatchEfficiency"<<std::endl;
                  TA2Spill* eff = *ColdDump/(*ColdDump+HotDump);
                  //eff->Print();
                  SpillFlow->spill_events.push_back(eff);
               }
            }
            
         }
      }
      else
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
      }
      return flow; 
  }


   void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event)
   {
      if (fTrace)
         printf("CatchEfficiencyModule::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", 
                runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
   }
};

class CatchEfficiencyModuleFactory: public TAFactory
{
public:
   CatchEfficiencyModuleFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("CatchEfficiencyModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
      }
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("CatchEfficiencyModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("CatchEfficiencyModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new CatchEfficiencyModule(runinfo, &fFlags);
   }
};

static TARegister tar(new CatchEfficiencyModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
