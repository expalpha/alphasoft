//
// Create the TA2Spills and push into flow, (new - simple version)
//
// L GOLINO
//

#include <stdio.h>
#include <time.h>

#include "manalyzer.h"
#if HAVE_MIDAS
#include "midas.h"
#endif
#include "midasio.h"
#include "TSISEvent.h"
#include "TSpill.h"
#include "AnalysisFlow.h"
#include "A2Flow.h"
#include "TSISChannels.h"
#include "TDumpList.h"
#include "GEM_BANK_flow.h"
#include <iostream>
#include <iomanip> //set precision
#include <sstream>

class DumpMakerModuleFlags
{
public:
   bool fPrint = false;
   bool fTrace = false;
};

class DumpMakerModule: public TARunObject
{
private:

public:
   DumpMakerModuleFlags* fFlags;
   bool fTrace = false;

   TString fStartChannelNames[NUMSEQ]={"SIS_PBAR_DUMP_START","SIS_RECATCH_DUMP_START","SIS_ATOM_DUMP_START","SIS_POS_DUMP_START","NA","NA","NA","NA","NA"};
   TString fStopChannelNames[NUMSEQ] ={"SIS_PBAR_DUMP_STOP", "SIS_RECATCH_DUMP_STOP", "SIS_ATOM_DUMP_STOP", "SIS_POS_DUMP_STOP","NA","NA","NA","NA","NA"};
   TString fStartSeqChannelNames[NUMSEQ]={"SIS_PBAR_SEQ_START","SIS_RECATCH_SEQ_START","SIS_ATOM_SEQ_START","NA","NA","NA","NA","NA","NA"};
   TString fSeqNames[NUMSEQ]={"cat","rct","atm","pos","NA","NA","NA","NA","NA"};

   std::array<TSISChannel,USED_SEQ> fSISStartChannels;
   std::array<TSISChannel,USED_SEQ> fSISStopChannels;
   std::array<TSISChannel,USED_SEQ> fSISSeqChannels;

   TSISChannel fMixingChannel;

   std::deque<std::shared_ptr<TSVD_QOD>> fSVDEventBuffer;
   std::deque<std::shared_ptr<TSISEvent>> fSISEventBuffer;

   //Variables needed to build a spill, each of these members is an array of the number of sequencers, and has 1 element per dump. Ie: each spill needs 1 start and stop dump marker, 1 start/stop SIS trg, and a vector of seq states (fSeqStates)
   //Records the SIS triggers as they come in
   std::array<std::deque< double >, USED_SEQ> fStartDumpSISTrgs;
   std::array<std::deque< double >, USED_SEQ> fStopDumpSISTrgs;
   //Saves the dump markers as they come in
   std::array<std::deque< TDumpMarker >, USED_SEQ> fStartDumpMarkers;
   std::array<std::deque< TDumpMarker >, USED_SEQ> fStopDumpMarkers;
   //Records whether we've thrown away a start or stop, initially zero it becomes 1 if we throw away a start...
   std::array<std::deque< int >, USED_SEQ> fFuzzyStart;
   std::array<std::deque< int >, USED_SEQ> fFuzzyStop;
   //Vector to store the states, this is the same as above (one element per dump) but that element is now a vector because each dump has multiple states.
   std::array<std::deque< std::vector<TSequencerState> >, USED_SEQ> fSeqStates;
   //Deque for mixing flag (this comes from outside and offers a good way to sync things)
   std::deque<Double_t> fMixingSISTrgs;

   //This tracks the most recent seq we are on.
   int fCurrentSeq[NUMSEQ] = {0};


   int fMarkerCounts[USED_SEQ][2] = {{0}};


   double fLastDumpTime[USED_SEQ] = {0};

   std::mutex fSequencerLock[USED_SEQ];
   std::mutex moduleLock;

   
   DumpMakerModule(TARunInfo* runinfo, DumpMakerModuleFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Handle Dumps";
#endif
      if (fFlags->fTrace)
         printf("DumpMakerModule::ctor!\n");
   }

   ~DumpMakerModule()
   {
      if (fFlags->fTrace)
         printf("DumpMakerModule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if (fFlags->fTrace)
         printf("DumpMakerModule::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());

      TSISChannels* SISChannels=new TSISChannels( runinfo->fRunNo );
      for (int j=0; j<USED_SEQ; j++) 
      {
         fSISStartChannels[j]    = SISChannels->GetChannel(fStartChannelNames[j],runinfo->fRunNo);
         fSISStopChannels[j]     = SISChannels->GetChannel(fStopChannelNames[j], runinfo->fRunNo);
         fSISSeqChannels[j]     = SISChannels->GetChannel(fStartSeqChannelNames[j], runinfo->fRunNo);
      }
            
      fMixingChannel = SISChannels->GetChannel("MIXING_FLAG", runinfo->fRunNo);

      delete SISChannels;

      for (size_t i = 0; i < USED_SEQ; i++)
      {
         fLastDumpTime[i]=std::numeric_limits<double>::max();
      }
      
   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fFlags->fTrace)
         printf("DumpMakerModule::EndRun, run %d\n", runinfo->fRunNo);

      for (int j=0; j<USED_SEQ; j++) 
      {
         if(fStartDumpSISTrgs.at(j).size())
         {
            if (fFlags->fTrace) std::cout << "SPILL WARNING: End run but still start dumps SIS in chamber. Seq: "<< GetSequencerName(j) << " (" << fStartDumpSISTrgs.at(j).size() << ") dumps remaining.\n"; 
         }
         if(fStopDumpSISTrgs.at(j).size())
         {
            if (fFlags->fTrace) std::cout << "SPILL WARNING: End run but still stop dumps SIS in chamber. Seq: "<< GetSequencerName(j) << " (" << fStopDumpSISTrgs.at(j).size() << ") dumps remaining.\n"; 
         }
         if(fStartDumpMarkers.at(j).size())
         {
            if (fFlags->fTrace) std::cout << "SPILL WARNING: End run but still start dumps markers in chamber. Seq: "<< GetSequencerName(j) << " (" << fStartDumpMarkers.at(j).size() << ") dumps remaining.\n"; 
         }
         if(fStopDumpMarkers.at(j).size())
         {
            if (fFlags->fTrace) std::cout << "SPILL WARNING: End run but still stop dumps markers in chamber. Seq: "<< GetSequencerName(j) << " (" << fStopDumpMarkers.at(j).size() << ") dumps remaining.\n"; 
         }
      }

      if (fFlags->fTrace) std::cout << "SPILL INFO: Spill markers:\n";
      for(int i = 0; i<USED_SEQ; i++)
      {
         for(int j = 0; j<2; j++)
         {
            if (fFlags->fTrace) std::cout << fMarkerCounts[i][j] << ",";
         }
         if (fFlags->fTrace) std::cout << "\n";
      }
      if (fFlags->fTrace) std::cout << "SPILL INFO: Do they match??\n";
   }
   
   void PauseRun(TARunInfo* runinfo)
   {
      if (fFlags->fTrace)
         printf("DumpMakerModule::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fFlags->fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }

   //Catch sequencer flow in the main thread, so that we have expected dumps ASAP
   TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* me, TAFlags* flags, TAFlowEvent* flow)
   {
      if( me->event_id != 8 ) // sequencer event id
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }

      DumpFlow* DumpsFlow=flow->Find<DumpFlow>();
      if (!DumpsFlow)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      const uint ndumps=DumpsFlow->DumpMarkers.size();
      if (!ndumps)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      if (fFlags->fTrace)
         printf("DumpMakerModule::Analyze, run %d\n", runinfo->fRunNo);

      //For now only unpack flow if seq == 2 (atom)
      int seqNum=DumpsFlow->SequencerNum;
      fCurrentSeq[seqNum] = DumpsFlow->DumpMarkers.at(0).fSequenceCount+1;
      A2SpillFlow* f=new A2SpillFlow(flow);
      time_t mts = me->time_stamp;
      char buf [20];
      std::strftime(buf, 20, "%Y-%m-%d %H:%M:%S", localtime(&mts));
      TA2Spill* seqData = new TA2Spill(runinfo->fRunNo,me->time_stamp,"[ %s ] ----- %s Seq Data Received ------->", buf, GetSequencerName(seqNum).c_str());
      f->spill_events.push_back(seqData);
      if (fFlags->fTrace) std::cout << "\n\n Seq num = " << fCurrentSeq[seqNum] << "\n";
      {
         //Lock scope
         std::lock_guard<std::mutex> locallock(moduleLock);
         int numExistingDumps = fStartDumpMarkers.at(seqNum).size();
         std::lock_guard<std::mutex> lock(fSequencerLock[seqNum]);

         //Loop through the sequencer xml data (unpacked elsewhere) and push back dump markers.
         for(TDumpMarker dump: DumpsFlow->DumpMarkers)
         {
            if(dump.fDumpType == TDumpMarker::kDumpTypes::Start )
            {
               if (fFlags->fTrace) std::cout << "SPILL INFO: Adding dump " << dump.fDescription << " to fDumpNames(" << seqNum <<") \n";
               fStartDumpMarkers.at(seqNum).push_back(dump);
               fFuzzyStart.at(seqNum).push_back(0);
            }
            if(dump.fDumpType == TDumpMarker::kDumpTypes::Stop )
            {
               fStopDumpMarkers.at(seqNum).push_back(dump);
               fFuzzyStop.at(seqNum).push_back(0);
            }
         }
         if(fStartDumpMarkers.at(seqNum).size() != fStopDumpMarkers.at(seqNum).size())
         {
            if (fFlags->fTrace) std::cout << "SPILL WARNING: Non equal number of starts and stops encountered. I will continue but things might be bad...\n";
         }

         //Local lock this scope... The moment we grab numExistingDumps we do not want the FlushEvents() to pop_front of the fStartDumpMarkers vector (which it will do when a new dump is created - unless we lock)

         for(size_t i=numExistingDumps; i<fStartDumpMarkers.at(seqNum).size(); i++)
         {
            if (fFlags->fTrace) std::cout << "SPILL INFO: For spill " << i << " we want to add states: " << fStartDumpMarkers.at(seqNum).at(i).fonState << " - " << fStopDumpMarkers.at(seqNum).at(i).fonState << ".\n";
            
            fSeqStates.at(seqNum).push_back( std::vector<TSequencerState>{} );
            for(int j=fStartDumpMarkers.at(seqNum).at(i).fonState; j<fStopDumpMarkers.at(seqNum).at(i).fonState+1; j++)
            {
               if (fFlags->fTrace) std::cout << "SPILL INFO: For spill " << i << " we are adding state " << j << ".\n";
               fSeqStates.at(seqNum).at(i).push_back( DumpsFlow->fStates.at(j) );
               fStartDumpMarkers.at(seqNum).at(i).fStatesFull = true;
            }
         } 
      }

      return f;
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      SISEventFlow* SISFlow = flow->Find<SISEventFlow>();
      SVDQODFlow* SVDFlow = flow->Find<SVDQODFlow>();

      if (SISFlow || SVDFlow )
      {
        //We have some work to do on the flow
      }
      else
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         //Nothing to do here
         return flow;   
      }

      //Add every SIS event to the buffer.
      if (SISFlow)
      {
         for (int j=0; j<NUM_SIS_MODULES; j++)
         {
            const std::vector<std::shared_ptr<TSISEvent>> ce = SISFlow->sis_events[j];
            for(std::shared_ptr<TSISEvent> event: ce)
            {
               fSISEventBuffer.push_back(event);
            }
         }
         if (fFlags->fTrace) std::cout << "DumpMakerModule::AnalyzeFlowEvent SIS Event Buffer size: " << fSISEventBuffer.size() << std::endl;
      }
      //else  std::cout << "DumpMakerModule::AnalyzeFlowEvent NO SIS flow event" << std::endl;

      //Add every SVDEvent to the buffer
      if (SVDFlow)
      {
         for(std::shared_ptr<TSVD_QOD> event: SVDFlow->SVDQODEvents)
         {
            fSVDEventBuffer.push_back(event);
         }
         if (fFlags->fTrace) std::cout << "DumpMakerModule::AnalyzeFlowEvent SVD Event Buffer size: " << fSVDEventBuffer.size() << std::endl;
      }
      //else std::cout << "DumpMakerModule::AnalyzeFlowEvent NO SVD flow event" << std::endl;


      A2SpillFlow* f=new A2SpillFlow(flow);
      if (SISFlow)
      {
         for (int j=0; j<NUM_SIS_MODULES; j++)
         {
            const std::vector<std::shared_ptr<TSISEvent>> ce = SISFlow->sis_events[j];
            for (size_t i = 0; i < ce.size(); i++)
            {
               const std::shared_ptr<TSISEvent> e = ce.at(i);
               for (int a = 0; a < USED_SEQ; a++)
               {
                  //Lock scope
                  std::lock_guard<std::mutex> lock(fSequencerLock[a]);
                  
                  if (fSISSeqChannels.at(a).IsValid())
                  {
                     const int counts = e->GetCountsInChannel(fSISSeqChannels.at(a));
                     //If we have counts>0 here we have a new sequencer trigger, this function will check whether all lists are in order when this happens.
                     if(counts>0)
                     {
                        //This checks if we have a new seq start SIS trigger, but still have SIS triggers around. 
                        //If this happens we've received too many SIS triggers to spills, need to warn the user.
                        switch(NewSequencerTriggerCheck(a, e->GetRunTime()))
                        {
                           TA2Spill* badseq;
                           case 0: //0 = no problem, everything is safe as houses, just continue
                              break;

                           case -1: //-1 = little problem but I think it was handled ok...
                              badseq = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs, %s ] SPILL WARNING: New sequencer trigger with unmatched dumps, recovered successfully.",e->GetRunTime(), GetSequencerName(a).c_str());
                              f->spill_events.push_back(badseq);
                              break;

                           case -2: //-2 is BIG problem, something is very bad...
                              badseq = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs, %s ] SPILL WARNING: New sequencer trigger with unmatched dumps, recovered unsuccessfully (dump markers may be out of sync).",e->GetRunTime(), GetSequencerName(a).c_str());
                              f->spill_events.push_back(badseq);
                              break;
                        }
                     }
                  }

                  //Check for SIS triggers.
                  if (fSISStartChannels.at(a).IsValid())
                  {
                     const int counts = e->GetCountsInChannel(fSISStartChannels.at(a));
                     //if counts>0 we have a start dump.

                     //Check we have start dump markers, if not (size == 0) warn the user.
                     if(counts>0 && fStartDumpMarkers.at(a).size()==0)
                     {
                        TA2Spill* badstart = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs, %s ] SPILL WARNING: Start dump SIS trigger but no sequencer data. Did run start late?",e->GetRunTime(), GetSequencerName(a).c_str());
                        f->spill_events.push_back(badstart);
                        fMarkerCounts[a][0]++;
                        goto stopDumpCheck;
                     }

                     //Loop over these counts (start dumps, usually only 1)
                     for (int nstarts = 0; nstarts < counts; nstarts++)
                     {
                        if (fFlags->fTrace) std::cout << "SPILL INFO: Start dump spotted: midastime: " << e->GetMidasUnixTime() << ", runtime: " << e->GetRunTime() << ", seq: " << GetSequencerName(a) << "\n";
                        fMarkerCounts[a][0]++;
                        //We've just seen a start dump, that means that there should currently be an equal number of stops and starts right now:
                        if(fStartDumpSISTrgs.at(a).size()>fStopDumpSISTrgs.at(a).size())
                        {
                           TA2Spill* fuzzystart= new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs, %s ] SPILL WARNING: Multiple start dump SIS triggers in a row detected - throwing one away (this dump will have fuzzy start)",e->GetRunTime(), GetSequencerName(a).c_str());
                           f->spill_events.push_back(fuzzystart);
                           if (fFlags->fTrace) std::cout << "SPILL WARNING: Multiple starts in a row detected, I have to throw one away but I don't know which is real- seq:" << GetSequencerName(a) << "\n";
                           if (fFlags->fTrace) std::cout << "SPILL WARNING: Because I threw away a start, the start time of this dump will be fuzzy:" << GetSequencerName(a) << "\n";
                           //If we've detected another start dump we have to just throw one away... We will never know which is the correct start time, this dump will have a fuzzy start time:
                           if(fFuzzyStart.at(a).size())
                              fFuzzyStart.at(a).back()=1;
                           else
                              fFuzzyStart.at(a).push_back(1);
                        }
                        else
                        {
                           //If everything looks good, lets add to the list of triggers
                           fStartDumpSISTrgs.at(a).push_back(e->GetRunTime());
                           fLastDumpTime[a] = e->GetRunTime();
                        }
                     }
                  }

                  stopDumpCheck:
                  if (fSISStopChannels.at(a).IsValid())
                  {
                     const int counts = e->GetCountsInChannel(fSISStopChannels.at(a));
                     //if counts>0 we have a stop dump.

                     //Check we have start dump markers, if not (size == 0) warn the user.
                     if(counts>0 && fStopDumpMarkers.at(a).size()==0)
                     {
                        TA2Spill* badstop = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs, %s ] SPILL WARNING: Stop dump SIS trigger but no sequencer data. Did run start late?",e->GetRunTime(), GetSequencerName(a).c_str());
                        f->spill_events.push_back(badstop);
                        fMarkerCounts[a][1]++;
                        continue;
                     }

                     for (int nstops = 0; nstops < counts; nstops++)
                     {
                        if (fFlags->fTrace) std::cout << "SPILL INFO: Stop dump spotted: midastime: " << e->GetMidasUnixTime() << ", runtime: " << e->GetRunTime() << ", seq: " << GetSequencerName(a) << "\n";
                        fMarkerCounts[a][1]++;
                        //We've just seen a stop dump, that means that there should currently be more starts than stops:
                        if(fStopDumpSISTrgs.at(a).size()>=fStartDumpSISTrgs.at(a).size())
                        {
                           TA2Spill* fuzzystop = new TA2Spill(runinfo->fRunNo,e->GetMidasUnixTime(),"[ %fs, %s ] SPILL WARNING: Multiple stop dump SIS triggers in a row detected - throwing one away (this dump will have fuzzy stop).",e->GetRunTime(), GetSequencerName(a).c_str());
                           f->spill_events.push_back(fuzzystop);
                           if (fFlags->fTrace) std::cout << "SPILL WARNING: Multiple stops in a row detected, I have to throw one away but I don't know which is real- seq:" << GetSequencerName(a) << "\n";
                           if (fFlags->fTrace) std::cout << "SPILL WARNING: Because I threw away a stop, the stop time of this dump will be fuzzy:" << GetSequencerName(a) << "\n";
                           //If we've detected another stop dump we have to just throw one away... We will never know which is the correct stop time, this dump will have a fuzzy stop time:
                           if(fFuzzyStop.at(a).size())
                              fFuzzyStop.at(a).back()=1;
                           else
                              fFuzzyStop.at(a).push_back(1);
                        }
                        else
                        {
                           //If everything looks good, lets add to the list of triggers
                           fStopDumpSISTrgs.at(a).push_back(e->GetRunTime());
                           fLastDumpTime[a] = e->GetRunTime();
                        }
                     }
                  }
               }
            
            
               if (fMixingChannel.IsValid())
               {
                  const int counts = e->GetCountsInChannel(fMixingChannel);
                  if(counts>0)
                  {
                     if (fFlags->fTrace) std::cout << "pushing back " << e->GetRunTime() << "\n"; 
                     fMixingSISTrgs.push_back(e->GetRunTime());
                     if (fFlags->fTrace) std::cout << "pushed back " << fMixingSISTrgs.at(0) << "\n"; 

                  }
               }
            }
         }
      }


      //Check we are ready to flush and if so, flush (this creates a new spill with the first element of each of our vectors defined in this class)
      for (int a = 0; a < USED_SEQ; a++)
      {
         bool ready = ReadyToFlush(a);//Are we???
         if(ready)
            FlushEvents(runinfo->fRunNo, flow, f, a);
      }

      //Clear the buffers
      ClearBuffers();

      return f; 
   }

   bool ReadyToFlush(int seqNum)
   {
      // bool ready = false; -- unused AC 10/1/2024
      //This checks we have start and stop markers, and start/stop SIS trgs, as well as seq states.
      if(fStartDumpMarkers.at(seqNum).size()>0 && fStopDumpMarkers.at(seqNum).size()>0 
         && fStartDumpSISTrgs.at(seqNum).size()>0 && fStopDumpSISTrgs.at(seqNum).size()>0 
         && fSeqStates.at(seqNum).size()>0 )
      {
         //Lets do checks... First: 
         if(fStartDumpSISTrgs.at(seqNum).at(0) > fStopDumpSISTrgs.at(seqNum).at(0))
         {
            //Dump has negative length? We are checking this earlier so in theory shouldn't need this?
            //ONE OF THESE IS BAD, BUT IS IT THE START OR THE STOP?
            if(fStartDumpSISTrgs.at(seqNum).size()>fStopDumpSISTrgs.at(seqNum).size()) //if they're equal we want to remove the stop, this '>' is intended.
            {
               //We have more starts than stops....
               //Throwing away a start....
               if (fFlags->fTrace) std::cout << "SPILL WARNING: Dump with negative length detected, throwing away the start marker (I think it's fake).\n";
               fStartDumpSISTrgs.at(seqNum).pop_front();
               fFuzzyStart.at(seqNum).at(0) = 1; //Since we've thrown away a start this front is fuzzy.
            }
            else
            {
               if (fFlags->fTrace) std::cout << "SPILL WARNING: Dump with negative length detected, throwing away the stop marker (I think it's fake).\n";
               fStopDumpSISTrgs.at(seqNum).pop_front();
               fFuzzyStop.at(seqNum).at(0) = 1; //Since we've thrown away a stop this front is fuzzy.
            }
            //We'll try again later.
            return false;
         }
         if( fSVDEventBuffer.size() > 0 )
         {
             if(fSVDEventBuffer.back()->GetRunTime() < fStopDumpSISTrgs.at(seqNum).at(0))
             {
                  //EventBuffer hasnt caught up yet, try again later...
                  //if (fFlags->fTrace) std::cout << "SPILL INFO: SVD Event buffer has not caught up yet: seq: " << seqNum << ": " << fSVDEventBuffer.back()->GetRunTime() << " < " << fStopDumpSISTrgs.at(seqNum).at(0) << "\n";
                  return false;
             }
         }
         if(fSISEventBuffer.back()->GetRunTime() < fStopDumpSISTrgs.at(seqNum).at(0))
         {
            //EventBuffer hasnt caught up yet, try again later...
            //if (fFlags->fTrace) std::cout << "SPILL INFO: SIS Event buffer has not caught up yet: seq: " << seqNum << ": " << fSISEventBuffer.back()->GetRunTime() << " < " << fStopDumpSISTrgs.at(seqNum).at(0) << "\n";
            return false;
         }

         //This checks we dont have a mixing desync...
         if(fStartDumpMarkers.at(seqNum).at(0).fDescription == "\"Mixing\"")
         {
            //We are not ready for this yet, I have found a couple runs where this breaks things. We need to return to it later, and I should look into those runs and see why they break
            //69345, 69374, 69350 
            /*if(fMixingSISTrgs.size() > 0 && abs(fStartDumpSISTrgs.at(seqNum).at(0) - fMixingSISTrgs.at(0)) > 0.001) //1ms? Arbitrary choice...
            {
               if (fFlags->fTrace) std::cout << "SPILL INFO: Mixing spill desync: " << fStartDumpSISTrgs.at(seqNum).at(0) << " vs " << fMixingSISTrgs.at(0) << "\n";

               //We probably need 2 seperate ways to handle this depending on whether we missed it or already built it...
               if(fStartDumpSISTrgs.at(seqNum).at(0) < fMixingSISTrgs.at(0))
               {
                  //Since the current SIS trigger is before of the sequencer spill, we remove the sis triggers so they can match again.
                  //fStartDumpSISTrgs.at(seqNum).pop_front();
                  //fStopDumpSISTrgs.at(seqNum).pop_front();
                  return false;
                  
               }
               if(fStartDumpSISTrgs.at(seqNum).at(0) > fMixingSISTrgs.at(0))
               {
                  //Since the current SIS trigger is after of the sequencer spill, we remove the sequence stuff so they can match again.
                  //fStartDumpMarkers.at(seqNum).pop_front();
                  //fStopDumpMarkers.at(seqNum).pop_front();
                  //fSeqStates.at(seqNum).pop_front();
                  return false;
               }
            }
            else if(fMixingSISTrgs.size() == 0)
            {
               if (fFlags->fTrace) std::cout << "SPILL WARNING: Trying to build mixing dump but no mixing flag yet? Something has gone bad...\n";
               return false;
            }
            else
            {
               fMixingSISTrgs.pop_front();
            }*/
         }
         return true;
      }
      else
      {
         //if (fFlags->fTrace) std::cout << "SPILL WARNING: Waiting for queues to fill seq: " << seqNum << "\n";
         return false;
      }

   }

   TAFlowEvent* FlushEvents(int runNumber, TAFlowEvent* /*flow*/, A2SpillFlow* f, int numSeq)
   {
      //We know everything is ready to dump if we're here so lets build some spills...

      //global lock - this is popping the front of the queues while other stuff it reading them. Do not pop the queues while something else is reading.

      //Add the correct time to our dump marker...
      TDumpMarker* startDump = new TDumpMarker(fStartDumpMarkers.at(numSeq).at(0));
      startDump->fRunTime = fStartDumpSISTrgs.at(numSeq).at(0);

      //Add the correct time to our dump marker....
      TDumpMarker* stopDump = new TDumpMarker(fStopDumpMarkers.at(numSeq).at(0));
      stopDump->fRunTime = fStopDumpSISTrgs.at(numSeq).at(0);

      //startDump->Print();
      //stopDump->Print();

      //Create a new pair.
      TDumpMarkerPair<TSVD_QOD,TSISEvent,NUM_SIS_MODULES>* markerPair = new TDumpMarkerPair<TSVD_QOD,TSISEvent,NUM_SIS_MODULES>();
      markerPair->AddStartDump(*startDump);
      markerPair->AddStopDump(*stopDump);

      //Add the seq states we pushed back in the Analyze function.
      for(TSequencerState state: fSeqStates.at(numSeq).at(0))
      {
         markerPair->AddState(state);
      }

      //Add SVD events to the spill.
      for(std::shared_ptr<TSVD_QOD> event: fSVDEventBuffer)
      {
         if((fStartDumpSISTrgs.at(numSeq).at(0) < event->GetRunTime()) && (event->GetRunTime() < fStopDumpSISTrgs.at(numSeq).at(0)))
         {
            markerPair->AddSVDEvent(*event);
         }
      }
      //Add SIS events to the spill.
      for(std::shared_ptr<TSISEvent> sisevent: fSISEventBuffer)
      {
         if((fStartDumpSISTrgs.at(numSeq).at(0) < sisevent->GetRunTime()) && (sisevent->GetRunTime() < fStopDumpSISTrgs.at(numSeq).at(0)))
         {
            markerPair->AddScalerEvent(*sisevent);
         }
      }

      //Build the spill object
      TA2Spill* newSpill = new TA2Spill(runNumber, markerPair);
      f->spill_events.push_back(newSpill);

      //Check whether it has a fuzzy start/stop.
      if(fFuzzyStart.at(numSeq).at(0))
      {
         if (fFlags->fTrace) std::cout << "SPILL WARNING: Spill " << newSpill->fName << "[" << newSpill->fRepetition << "] has fuzzy start time. Do not trust me. You have been warned. 😈\n";
         newSpill->fFuzzyStart = true;
      }
      if(fFuzzyStop.at(numSeq).at(0))
      {
         if (fFlags->fTrace) std::cout << "SPILL WARNING: Spill " << newSpill->fName << "[" << newSpill->fRepetition << "] has fuzzy stop time. Do not trust me. You have been warned. 😈\n";
         newSpill->fFuzzyStop = true;
      }

      //Spill has 0 states? This probably shouldn't happen (maybe the sequencer genuinely does nothing during this spill?)
      if(markerPair->fStates.size() == 0)
      {
         if (fFlags->fTrace) std::cout << "SPILL WARNING: fStates.size() = " << markerPair->fStates.size() << ". Are you sure?\n";
      }

      {
         //Analyze function is looping over these sometimes, do not pop_front when it is doing that.
         std::lock_guard<std::mutex> locallock(moduleLock);
         fStartDumpMarkers.at(numSeq).pop_front();
         fStopDumpMarkers.at(numSeq).pop_front();
         fStartDumpSISTrgs.at(numSeq).pop_front();
         fStopDumpSISTrgs.at(numSeq).pop_front();
         fFuzzyStart.at(numSeq).pop_front();
         fFuzzyStop.at(numSeq).pop_front();
         fSeqStates.at(numSeq).pop_front();
      }
      delete markerPair;

      return f;
   }

   void ClearBuffers()
   {
      // Initialize the minimum value to a very large number
      double minDump = std::numeric_limits<double>::max();
      
      // Iterate over fStartDumpSISTrgs
      for (const auto& deque : fStartDumpSISTrgs) 
      {
         for (const auto& val : deque) 
         {
            if (val < minDump) 
            {
               minDump = val;
            }
         }
      }
      
      // Iterate over fStopDumpSISTrgs
      for (const auto& deque : fStopDumpSISTrgs) 
      {
         for (const auto& val : deque) 
         {
            if (val < minDump) 
            {
               minDump = val;
            }
         }
      }

      if(minDump == std::numeric_limits<double>::max())
      {
         if (fFlags->fTrace) std::cout << "SPILL INFO: No stop value found, clearing whole queue.\n";
      }
      
      //Done with everything, lets release.
      //if (fFlags->fTrace) std::cout << "Cleaning events up to t = " << lastStartDumpTime << "\n"; 
      while(fSVDEventBuffer.size())
      {
         if(fSVDEventBuffer.at(0)->GetRunTime() < minDump)
            fSVDEventBuffer.pop_front();
         else
            break;
      }
      while(fSISEventBuffer.size())
      {
         if(fSISEventBuffer.at(0)->GetRunTime() < minDump)
            fSISEventBuffer.pop_front();
         else
            break;
      }
   }

   int NewSequencerTriggerCheck(int a, double seqstarttime)
   {
      //If we're in here theres been a new sequencer trigger, lets count hot many old dumps and markers are still around.
      size_t numOldStartTrigsLeft = 0;
      size_t numOldStopTrigsLeft = 0;
      size_t numOldStartDumpMarksLeft = 0;
      size_t numOldStopDumpMarksLeft = 0;

      //Count how may SIS triggers are less than the Seq start
      for(size_t k=0; k<fStartDumpSISTrgs.at(a).size(); k++)
      {
         if(fStartDumpSISTrgs.at(a).at(k) < seqstarttime)
            numOldStartTrigsLeft++;
         else
            break;
      }
      if (fFlags->fTrace) std::cout << "SPILL INFO: NUM START TRIGS LEFT = " << numOldStartTrigsLeft << "\n";

      //Count how may SIS triggers are less than the Seq start
      for(size_t k=0; k<fStopDumpSISTrgs.at(a).size(); k++)
      {
         if(fStopDumpSISTrgs.at(a).at(k) < seqstarttime)
            numOldStopTrigsLeft++;
         else
            break;                                 
      }
      if (fFlags->fTrace) std::cout << "SPILL INFO: NUM STOP TRIGS LEFT = " << numOldStopTrigsLeft << "\n";
      
      //Count how may SIS triggers are less than the Seq start
      for(size_t k=0; k<fStartDumpMarkers.at(a).size(); k++)
      {
         if( fStartDumpMarkers.at(a).at(k).fSequenceCount < fCurrentSeq[a])
            numOldStartDumpMarksLeft++;
         else
            break;                                 
      }
      if (fFlags->fTrace) std::cout << "SPILL INFO: NUM START MARKS LEFT = " << numOldStartDumpMarksLeft << "\n";
   
      //Count how may SIS triggers are less than the Seq Stop
      for(size_t k=0; k<fStopDumpMarkers.at(a).size(); k++)
      {
         if( fStopDumpMarkers.at(a).at(k).fSequenceCount < fCurrentSeq[a])
            numOldStopDumpMarksLeft++;
         else
            break;                                 
      }
      if (fFlags->fTrace) std::cout << "SPILL INFO: NUM Stop MARKS LEFT = " << numOldStopDumpMarksLeft << "\n";
      
      //Check all 4 are identical....
      if( numOldStartTrigsLeft == numOldStopTrigsLeft && numOldStopTrigsLeft == numOldStartDumpMarksLeft && numOldStartDumpMarksLeft == numOldStopDumpMarksLeft)
      {
         //THis is great, we have no problem...
         if (fFlags->fTrace) std::cout << "SPILL INFO: New seq but everything looks good: " << numOldStartTrigsLeft << " \n";
         return 0;
      }
      else if(numOldStartTrigsLeft == numOldStopTrigsLeft && numOldStartDumpMarksLeft == numOldStopDumpMarksLeft)
      {
         if (fFlags->fTrace) std::cout << "SPILL WARNING: New seq but one is bigger, we need to handle this... " << numOldStartTrigsLeft << numOldStopTrigsLeft << numOldStartDumpMarksLeft << numOldStopDumpMarksLeft << "\n";
         //So here we have both start and stop SIS triggers, and dump markers, but they dont match. We flush all missing ones.
         int diff = numOldStopTrigsLeft - numOldStartDumpMarksLeft;
         for(int k=0; k<diff; k++)
         {
            if(diff>0) //if diff > 0 there are more SIS triggers than dump markers, so lets flush the SIS trigs until they match.
            {
               //fStartDumpSISTrgs.at(a).pop_front();
               //fStopDumpSISTrgs.at(a).pop_front();
            }
            else if(diff<0) ////if diff < 0 there are more dump markers than SIS triggers, so lets flush the dump marks until they match.
            {
               //fStartDumpSISTrgs.at(a).pop_front();
               //fStopDumpSISTrgs.at(a).pop_front();
            }

         }
         //This is a pretty bad situation to be in, we need to warn the user...
         return -1;
      }
      else
      {
         if (fFlags->fTrace) std::cout << "SPILL WARNING: This really should not happen, we have a disaster... " << numOldStartTrigsLeft << numOldStopTrigsLeft << numOldStartDumpMarksLeft << numOldStopDumpMarksLeft << "\n";
         return -2;
      }
   }


};

class DumpMakerModuleFactory: public TAFactory
{
public:
   DumpMakerModuleFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("DumpMakerModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
         if (args[i] == "--spilltrace")
            fFlags.fTrace = true;
      }
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("DumpMakerModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("DumpMakerModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new DumpMakerModule(runinfo, &fFlags);
   }
};

static TARegister tar(new DumpMakerModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
