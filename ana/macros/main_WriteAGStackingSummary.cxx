#include <iostream>
#include <iomanip>

#include "TAGPlot.h"
#include "TAGPlot_Filler.h"

void WriteStackingSummary(const int runNumber)
{
	std::ostringstream oss;
	oss << "R" << runNumber << "_StackingSummary.txt";
	std::string filename = oss.str();
	std::ofstream output(filename);
	
	std::vector<TAGSpill> ct_holds = Get_AG_Spills(runNumber, {"Hold"}, {-1});
	std::vector<TAGSpill> ct_hots = Get_AG_Spills(runNumber, {"Hot Dump"}, {-1});
	std::vector<TAGSpill> ct_tenths = Get_AG_Spills(runNumber, {"Tenth Dump"}, {-1});
	std::vector<TAGSpill> rct_holds = Get_AG_Spills(runNumber, {"Hold rctb"}, {-1});
	std::vector<TAGSpill> rct_hots = Get_AG_Spills(runNumber, {"Hot Dump RCTB"}, {-1});
	std::vector<TAGSpill> mixings = Get_AG_Spills(runNumber, {"Mixing"}, {-1});
	std::vector<TAGSpill> rights = Get_AG_Spills(runNumber, {"Right dump top"}, {-1});
	std::vector<TAGSpill> lefts = Get_AG_Spills(runNumber, {"left dump bot"}, {-1});

	TChronoChannel CTChannel = Get_Chrono_Channel(runNumber, "SIPM_CT_OR");
	TChronoChannel AtmBotGChannel = Get_Chrono_Channel(runNumber, "SIPM_BOTG_TOP_OR");
	TChronoChannel UDSChannel = Get_Chrono_Channel(runNumber, "SIPM_UDS");
	TChronoChannel LDSChannel = Get_Chrono_Channel(runNumber, "SIPM_LDS");

	// Stacking summary table
	int w = 11;
	std::vector<std::vector<TAGSpill>> cols{ ct_holds, ct_hots, ct_tenths, rct_holds, rct_hots, mixings, rights, lefts };
	std::vector<TChronoChannel> col_chans{ CTChannel, CTChannel, CTChannel, AtmBotGChannel, AtmBotGChannel, AtmBotGChannel, UDSChannel, LDSChannel };
	std::vector<int> sum(cols.size(), 0);

	if(mixings.size() > 0) {
		output << "Stacking summary:\n";
		output << std::setw(3) << "#" << std::setw(w) << "CT_Hold"
			<< std::setw(w) << "CT_Hot" << std::setw(w) << "CT_Tenth"
		   	<< std::setw(w) << "RCT_Hold" << std::setw(w) << "RCT_Hot"
		   	<< std::setw(w) << "Mixing" << std::setw(w) << "Top"
			<< std::setw(w) << "Bot" << std::endl;
	}

	for(size_t i = 0; i < mixings.size(); ++i) {
		output << std::setw(3) << i;
		for(size_t j = 0; j < cols.size(); ++j) {
			if(i < cols.at(j).size() && col_chans.at(j).IsValidChannel()) {
				int counts = cols.at(j).at(i).fScalerData->fDetectorCounts.at(col_chans.at(j).GetIndex());
				output << std::setw(w) << counts;

				sum.at(j) += counts;
			} else {
				output << std::setw(w) << "N/A";
			}
		}
		output << std::endl;
	}
	output << std::string(w * cols.size() + 3, '-') << std::endl;

	if(mixings.size() > 0) {
		output << std::setw(3) << "avg";
		for(size_t i = 0; i < sum.size(); ++i) {
			output << std::setw(w) << sum.at(i) / mixings.size();
		}
	}
	output << std::endl;
   
   //~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//
   // Lifetime analysis, for handling lifetimes in multiple traps at once
   //~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//~//

   std::vector<TAGSpill> colds = Get_AG_Spills(runNumber, {"Cold Dump"}, {-1});
   int coldcats=0; int coldags=0;
   for (const TAGSpill& cold: colds) {
      if (cold.GetSequenceName()=="cat") coldcats++;
      if (cold.GetSequenceName()=="atm_botg") coldags++;
      if (cold.GetSequenceName()=="atm_topg") coldags++;
      if (cold.GetSequenceName()=="rct_botg") coldags++;
      if (cold.GetSequenceName()=="rct_topg") coldags++;
   }
   // For look up of TChronoChannel
   std::vector<std::string> ColdCatChannels = { "SIPM_CT_OR" };
   std::vector<std::string> ColdAgChannels = { "SIPM_UDS" };
   std::vector<std::string> FifthOrTenthChannels = { "SIPM_CT_OR" };
   // Actual channel addresses
   std::vector<TChronoChannel> ColdCatTChronoChannels;
   std::vector<TChronoChannel> ColdAgTChronoChannels;
   std::vector<TChronoChannel> FifthOrTenthTChronoChannels;
   // For human readable display
   std::vector<std::string> ColdCatChannelNames = { "SIPM_CT_OR" };
   std::vector<std::string> ColdAgChannelNames = { "SIPM_UDS" };
   for (const std::string& c: FifthOrTenthChannels)
      FifthOrTenthTChronoChannels.emplace_back(Get_Chrono_Channel(runNumber,c.c_str()));
   for (const std::string& c: ColdCatChannels)
      ColdCatTChronoChannels.emplace_back(Get_Chrono_Channel(runNumber,c.c_str()));
   for (const std::string& c: ColdAgChannels)
      ColdAgTChronoChannels.emplace_back(Get_Chrono_Channel(runNumber,c.c_str()));

   std::vector<TAGSpill> lifetimeSpills = Get_AG_Spills(runNumber, {"Lifetime", "Pbar lifetime rctb"}, {-1,-1});
   double lifetimeSpillCat_start=-1; double lifetimeSpillCat_stop=-1; bool isLifetimeCat = false;
   int coldCat1=-1; int coldCat2=-1; int tenthCat1=-1; int tenthCat2=-1; double coldCat1time=-1;
   double lifetimeSpillAtmBotg_start=-1; double lifetimeSpillAtmBotg_stop=-1; bool isLifetimeAtmBotg = false;
   int coldAtmBotg1=-1; int coldAtmBotg2=-1; int tenthAtmBotg1=-1; int tenthAtmBotg2=-1; double coldAtmBotg1time=-1;
   double lifetimeSpillAtmTopg_start=-1; double lifetimeSpillAtmTopg_stop=-1; bool isLifetimeAtmTopg = false;
   int coldAtmTopg1=-1; int coldAtmTopg2=-1; int tenthAtmTopg1=-1; int tenthAtmTopg2=-1; double coldAtmTopg1time=-1;
   double lifetimeSpillRctBotg_start=-1; double lifetimeSpillRctBotg_stop=-1; bool isLifetimeRctBotg = false;
   int coldRctBotg1=-1; int coldRctBotg2=-1; int tenthRctBotg1=-1; int tenthRctBotg2=-1; double coldRctBotg1time=-1;
   double lifetimeSpillRctTopg_start=-1; double lifetimeSpillRctTopg_stop=-1; bool isLifetimeRctTopg = false;
   int coldRctTopg1=-1; int coldRctTopg2=-1; int tenthRctTopg1=-1; int tenthRctTopg2=-1; double coldRctTopg1time=-1;
   for (const TAGSpill& s: lifetimeSpills) {
      if (s.GetSequenceName()=="cat") { lifetimeSpillCat_start = s.GetStartTime(); lifetimeSpillCat_stop = s.GetStopTime(); isLifetimeCat = true;}
      if (s.GetSequenceName()=="atm_botg") { lifetimeSpillAtmBotg_start = s.GetStartTime(); lifetimeSpillAtmBotg_stop = s.GetStopTime(); isLifetimeAtmBotg = true;}
      if (s.GetSequenceName()=="atm_topg") { lifetimeSpillAtmTopg_start = s.GetStartTime(); lifetimeSpillAtmTopg_stop = s.GetStopTime(); isLifetimeAtmTopg = true;}
      if (s.GetSequenceName()=="rct_botg") { lifetimeSpillRctBotg_start = s.GetStartTime(); lifetimeSpillRctBotg_stop = s.GetStopTime(); isLifetimeRctBotg = true;}
      if (s.GetSequenceName()=="rct_topg") { lifetimeSpillRctTopg_start = s.GetStartTime(); lifetimeSpillRctTopg_stop = s.GetStopTime(); isLifetimeRctTopg = true;}
   }
   
   if (isLifetimeCat) {
      for (const TAGSpill& cold: colds) {
         if (cold.GetSequenceName()=="cat" and cold.GetStartTime()<lifetimeSpillCat_start) {
            for (const TChronoChannel& c: ColdCatTChronoChannels) {
               coldCat1 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel()); // Last cold dump before lifetime
               coldCat1time = cold.GetStartTime();
            }
         }
         if (cold.GetSequenceName()=="cat" and cold.GetStartTime()>lifetimeSpillCat_stop) { 
            for (const TChronoChannel& c: ColdCatTChronoChannels) {
               coldCat2 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
            break;
         } // First cold dump after lifetime
      }
      for (const TAGSpill& tenth: ct_tenths) {
         if (tenth.GetStartTime()<coldCat1time) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthCat1 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
         } // Last tenth dump before lifetime
         if (tenth.GetStartTime()>coldCat1time and tenth.GetStartTime()<lifetimeSpillCat_stop) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthCat2 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());;
            }
            break;
         } // First tenth dump after lifetime
      }
   }
   if (isLifetimeAtmBotg) {
      for (const TAGSpill& cold: colds) {
         if (cold.GetSequenceName()=="atm_botg" and cold.GetStartTime()<lifetimeSpillAtmBotg_start) {
            for (const TChronoChannel& c: ColdAgTChronoChannels) {
               coldAtmBotg1 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel()); // Last cold dump before lifetime
               coldAtmBotg1time = cold.GetStartTime();
            }
         }
         if (cold.GetSequenceName()=="atm_botg" and cold.GetStartTime()>lifetimeSpillAtmBotg_stop) { 
            for (const TChronoChannel& c: ColdAgTChronoChannels) {
               coldAtmBotg2 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
            break;
         } // First cold dump after lifetime
      }
      for (const TAGSpill& tenth: ct_tenths) {
         if (tenth.GetStartTime()<coldAtmBotg1time) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthAtmBotg1 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
         } // Last tenth dump before lifetime
         if (tenth.GetStartTime()>coldAtmBotg1time and tenth.GetStartTime()<lifetimeSpillAtmBotg_stop) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthAtmBotg2 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());;
            }
            break;
         } // First tenth dump after lifetime
      }
   }
   if (isLifetimeAtmTopg) {
      for (const TAGSpill& cold: colds) {
         if (cold.GetSequenceName()=="atm_topg" and cold.GetStartTime()<lifetimeSpillAtmTopg_start) {
            for (const TChronoChannel& c: ColdAgTChronoChannels) {
               coldAtmTopg1 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel()); // Last cold dump before lifetime
               coldAtmTopg1time = cold.GetStartTime();
            }
         }
         if (cold.GetSequenceName()=="atm_topg" and cold.GetStartTime()>lifetimeSpillAtmTopg_stop) { 
            for (const TChronoChannel& c: ColdAgTChronoChannels) {
               coldAtmTopg2 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
            break;
         } // First cold dump after lifetime
      }
      for (const TAGSpill& tenth: ct_tenths) {
         if (tenth.GetStartTime()<coldAtmTopg1time) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthAtmTopg1 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
         } // Last tenth dump before lifetime
         if (tenth.GetStartTime()>coldAtmTopg1time and tenth.GetStartTime()<lifetimeSpillAtmTopg_stop) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthAtmTopg2 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());;
            }
            break;
         } // First tenth dump after lifetime
      }
   }
   if (isLifetimeRctBotg) {
      for (const TAGSpill& cold: colds) {
         if (cold.GetSequenceName()=="rct_botg" and cold.GetStartTime()<lifetimeSpillRctBotg_start) {
            for (const TChronoChannel& c: ColdAgTChronoChannels) {
               coldRctBotg1 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel()); // Last cold dump before lifetime
               coldRctBotg1time = cold.GetStartTime();
            }
         }
         if (cold.GetSequenceName()=="rct_botg" and cold.GetStartTime()>lifetimeSpillRctBotg_stop) { 
            for (const TChronoChannel& c: ColdAgTChronoChannels) {
               coldRctBotg2 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
            break;
         } // First cold dump after lifetime
      }
      for (const TAGSpill& tenth: ct_tenths) {
         if (tenth.GetStartTime()<coldRctBotg1time) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthRctBotg1 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
         } // Last tenth dump before lifetime
         if (tenth.GetStartTime()>coldRctBotg1time and tenth.GetStartTime()<lifetimeSpillRctBotg_stop) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthRctBotg2 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());;
            }
            break;
         } // First tenth dump after lifetime
      }
   }
   if (isLifetimeRctTopg) {
      for (const TAGSpill& cold: colds) {
         if (cold.GetSequenceName()=="rct_topg" and cold.GetStartTime()<lifetimeSpillRctTopg_start) {
            for (const TChronoChannel& c: ColdAgTChronoChannels) {
               coldRctTopg1 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel()); // Last cold dump before lifetime
               coldRctTopg1time = cold.GetStartTime();
            }
         }
         if (cold.GetSequenceName()=="rct_topg" and cold.GetStartTime()>lifetimeSpillRctTopg_stop) { 
            for (const TChronoChannel& c: ColdAgTChronoChannels) {
               coldRctTopg2 = cold.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
            break;
         } // First cold dump after lifetime
      }
      for (const TAGSpill& tenth: ct_tenths) {
         if (tenth.GetStartTime()<coldRctTopg1time) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthRctTopg1 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
            }
         } // Last tenth dump before lifetime
         if (tenth.GetStartTime()>coldRctTopg1time and tenth.GetStartTime()<lifetimeSpillRctTopg_stop) {
            for (const TChronoChannel& c: FifthOrTenthTChronoChannels) {
               tenthRctTopg2 = tenth.fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());;
            }
            break;
         } // First tenth dump after lifetime
      }
   }

   TAGAnalysisReport report = Get_AGAnalysis_Report(runNumber);

   if(isLifetimeCat)
   {
      double lifetime_start = report.GetRunStartTime() + lifetimeSpillCat_start;

      output << "\n\nLifetime calculation for CAT:\n";
      output << "Using panel: " << ColdCatChannelNames.at(0).c_str() << " for the following calculations... \n\n";

      double lifetimeDumpLength = lifetimeSpillCat_stop - lifetimeSpillCat_start;

      double coldDump0 = 1.0*coldCat1;
      double coldDump1 = 1.0*coldCat2;
      double FifthDump0 = 1.0*tenthCat1;
      double FifthDump1 = 1.0*tenthCat2;

      output << "Lifetime = time / ln[(coldDump0/tenthDump0) / (coldDump1/tenthDump1)] \n";
      output << "Lifetime = " << lifetimeDumpLength << " / ln[(" << coldDump0 << "/" << FifthDump0 << ") / (" << coldDump1 << "/" << FifthDump1 << ")] \n";
      output << "Lifetime = " << lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) )) << "s";
      double lifetime_s = lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) ));
      output << " = " << lifetime_s/60 << "m";
      output << " = " << lifetime_s/(60*60) << "h\n\n";

      output << "delta = (lifetime**2/time)*sqrt[1/coldDump0 + 1/tenthDump0 + 1/coldDump1+1/tenthDump1] \n";
      output << "delta = (" << lifetime_s << "**2/" << lifetimeDumpLength << ")*sqrt[1/" << coldDump0 << " + 1/" << FifthDump0 << " + 1/" << coldDump1 << "+1/" << FifthDump1 << "] \n";
      output << "delta = " << ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1)) << "s";
      double delta_s = ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1));
      output << " = " << delta_s/60 << "m";
      output << " = " << delta_s/(60*60) << "h\n";

      TDatime datetimelifetime(lifetime_start);
      std::string datetimeSQL(datetimelifetime.AsSQLString());

      output << "\nCat lifetime @ " << datetimeSQL << " = " << lifetime_s/60 << "m +/- " << delta_s/60 << "m \n";

   }

   if(isLifetimeRctBotg)
   {
      double lifetime_start = report.GetRunStartTime() + lifetimeSpillRctBotg_start;


      output << "\n\nLifetime calculation for Rct Botg:\n";
      output << "Using panel: " << ColdAgChannelNames.at(0).c_str() << " for the following calculations... \n\n";

      double lifetimeDumpLength = lifetimeSpillRctBotg_stop - lifetimeSpillRctBotg_start;

      double coldDump0 = 1.0*coldRctBotg1;
      double coldDump1 = 1.0*coldRctBotg2;
      double FifthDump0 = 1.0*tenthRctBotg1;
      double FifthDump1 = 1.0*tenthRctBotg2;

      output << "Lifetime = time / ln[(coldDump0/tenthDump0) / (coldDump1/tenthDump1)] \n";
      output << "Lifetime = " << lifetimeDumpLength << " / ln[(" << coldDump0 << "/" << FifthDump0 << ") / (" << coldDump1 << "/" << FifthDump1 << ")] \n";
      output << "Lifetime = " << lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) )) << "s";
      double lifetime_s = lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) ));
      output << " = " << lifetime_s/60 << "m";
      output << " = " << lifetime_s/(60*60) << "h\n\n";

      output << "delta = (lifetime**2/time)*sqrt[1/coldDump0 + 1/tenthDump0 + 1/coldDump1+1/tenthDump1] \n";
      output << "delta = (" << lifetime_s << "**2/" << lifetimeDumpLength << ")*sqrt[1/" << coldDump0 << " + 1/" << FifthDump0 << " + 1/" << coldDump1 << "+1/" << FifthDump1 << "] \n";
      output << "delta = " << ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1)) << "s";
      double delta_s = ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1));
      output << " = " << delta_s/60 << "m";
      output << " = " << delta_s/(60*60) << "h\n";

      TDatime datetimelifetime(lifetime_start);
      std::string datetimeSQL(datetimelifetime.AsSQLString());

      output << "\nRct Botg lifetime @ " << datetimeSQL << " = " << lifetime_s/60 << "m +/- " << delta_s/60 << "m \n";

   }

   if(isLifetimeRctTopg)
   {
      double lifetime_start = report.GetRunStartTime() + lifetimeSpillRctTopg_start;

      output << "\n\nLifetime calculation for Rct Botg:\n";
      output << "Using panel: " << ColdAgChannelNames.at(0).c_str() << " for the following calculations... \n\n";

      double lifetimeDumpLength = lifetimeSpillRctTopg_stop - lifetimeSpillRctTopg_start;

      double coldDump0 = 1.0*coldRctTopg1;
      double coldDump1 = 1.0*coldRctTopg2;
      double FifthDump0 = 1.0*tenthRctTopg1;
      double FifthDump1 = 1.0*tenthRctTopg2;

      output << "Lifetime = time / ln[(coldDump0/tenthDump0) / (coldDump1/tenthDump1)] \n";
      output << "Lifetime = " << lifetimeDumpLength << " / ln[(" << coldDump0 << "/" << FifthDump0 << ") / (" << coldDump1 << "/" << FifthDump1 << ")] \n";
      output << "Lifetime = " << lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) )) << "s";
      double lifetime_s = lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) ));
      output << " = " << lifetime_s/60 << "m";
      output << " = " << lifetime_s/(60*60) << "h\n\n";

      output << "delta = (lifetime**2/time)*sqrt[1/coldDump0 + 1/tenthDump0 + 1/coldDump1+1/tenthDump1] \n";
      output << "delta = (" << lifetime_s << "**2/" << lifetimeDumpLength << ")*sqrt[1/" << coldDump0 << " + 1/" << FifthDump0 << " + 1/" << coldDump1 << "+1/" << FifthDump1 << "] \n";
      output << "delta = " << ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1)) << "s";
      double delta_s = ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1));
      output << " = " << delta_s/60 << "m";
      output << " = " << delta_s/(60*60) << "h\n";

      TDatime datetimelifetime(lifetime_start);
      std::string datetimeSQL(datetimelifetime.AsSQLString());

      output << "\nRct Topg lifetime @ " << datetimeSQL << " = " << lifetime_s/60 << "m +/- " << delta_s/60 << "m \n";

   }
   if(isLifetimeAtmBotg)
   {
      double lifetime_start = report.GetRunStartTime() + lifetimeSpillAtmBotg_start;


      output << "\n\nLifetime calculation for Atm Botg:\n";
      output << "Using panel: " << ColdAgChannelNames.at(0).c_str() << " for the following calculations... \n\n";

      double lifetimeDumpLength = lifetimeSpillAtmBotg_stop - lifetimeSpillAtmBotg_start;

      double coldDump0 = 1.0*coldAtmBotg1;
      double coldDump1 = 1.0*coldAtmBotg2;
      double FifthDump0 = 1.0*tenthAtmBotg1;
      double FifthDump1 = 1.0*tenthAtmBotg2;

      output << "Lifetime = time / ln[(coldDump0/tenthDump0) / (coldDump1/tenthDump1)] \n";
      output << "Lifetime = " << lifetimeDumpLength << " / ln[(" << coldDump0 << "/" << FifthDump0 << ") / (" << coldDump1 << "/" << FifthDump1 << ")] \n";
      output << "Lifetime = " << lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) )) << "s";
      double lifetime_s = lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) ));
      output << " = " << lifetime_s/60 << "m";
      output << " = " << lifetime_s/(60*60) << "h\n\n";

      output << "delta = (lifetime**2/time)*sqrt[1/coldDump0 + 1/tenthDump0 + 1/coldDump1+1/tenthDump1] \n";
      output << "delta = (" << lifetime_s << "**2/" << lifetimeDumpLength << ")*sqrt[1/" << coldDump0 << " + 1/" << FifthDump0 << " + 1/" << coldDump1 << "+1/" << FifthDump1 << "] \n";
      output << "delta = " << ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1)) << "s";
      double delta_s = ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1));
      output << " = " << delta_s/60 << "m";
      output << " = " << delta_s/(60*60) << "h\n";

      TDatime datetimelifetime(lifetime_start);
      std::string datetimeSQL(datetimelifetime.AsSQLString());

      output << "\nAtm Botg lifetime @ " << datetimeSQL << " = " << lifetime_s/60 << "m +/- " << delta_s/60 << "m \n";

   }

   if(isLifetimeAtmTopg)
   {
      double lifetime_start = report.GetRunStartTime() + lifetimeSpillAtmTopg_start;

      output << "\n\nLifetime calculation for Atm Botg:\n";
      output << "Using panel: " << ColdAgChannelNames.at(0).c_str() << " for the following calculations... \n\n";

      double lifetimeDumpLength = lifetimeSpillAtmTopg_stop - lifetimeSpillAtmTopg_start;

      double coldDump0 = 1.0*coldAtmTopg1;
      double coldDump1 = 1.0*coldAtmTopg2;
      double FifthDump0 = 1.0*tenthAtmTopg1;
      double FifthDump1 = 1.0*tenthAtmTopg2;

      output << "Lifetime = time / ln[(coldDump0/tenthDump0) / (coldDump1/tenthDump1)] \n";
      output << "Lifetime = " << lifetimeDumpLength << " / ln[(" << coldDump0 << "/" << FifthDump0 << ") / (" << coldDump1 << "/" << FifthDump1 << ")] \n";
      output << "Lifetime = " << lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) )) << "s";
      double lifetime_s = lifetimeDumpLength / (TMath::Log( (coldDump0/FifthDump0) / (coldDump1/FifthDump1) ));
      output << " = " << lifetime_s/60 << "m";
      output << " = " << lifetime_s/(60*60) << "h\n\n";

      output << "delta = (lifetime**2/time)*sqrt[1/coldDump0 + 1/tenthDump0 + 1/coldDump1+1/tenthDump1] \n";
      output << "delta = (" << lifetime_s << "**2/" << lifetimeDumpLength << ")*sqrt[1/" << coldDump0 << " + 1/" << FifthDump0 << " + 1/" << coldDump1 << "+1/" << FifthDump1 << "] \n";
      output << "delta = " << ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1)) << "s";
      double delta_s = ((lifetime_s*lifetime_s)/(lifetimeDumpLength))*sqrt((1/coldDump0)+(1/FifthDump0)+(1/coldDump1)+(1/FifthDump1));
      output << " = " << delta_s/60 << "m";
      output << " = " << delta_s/(60*60) << "h\n";

      TDatime datetimelifetime(lifetime_start);
      std::string datetimeSQL(datetimelifetime.AsSQLString());

      output << "\nAtm Topg lifetime @ " << datetimeSQL << " = " << lifetime_s/60 << "m +/- " << delta_s/60 << "m \n";

   }
   // End of lifetime analysis


}

int main(int argc, char* argv[])
{
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 5000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 40000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }
   WriteStackingSummary(runNumber);
   return 0;
}


