#include "TAGPlot.h"


void SaveFRD(const int runNumber)
{
   auto FRDs = Get_AG_Spills(runNumber,{"*RampDown"},{-1});
   if (FRDs.empty()) {
      std::cout << "No FRD in this run" <<std::endl;
      return;
   }
   TAGDetectorEvent e;
   std::map<std::string,int> DumpNameCounter;
   for (const auto& spill: FRDs)
   {
      std::cout << " --------------------" << spill.fName << " --------------------\n";
      TAGPlot a;
      a.AddTimeGate(runNumber, spill.GetStartTime(), spill.GetStopTime());
      a.LoadData();

      std::string dumpname = spill.GetSanitisedName();
      if (DumpNameCounter.count(dumpname) == 0)
         DumpNameCounter[dumpname] = 0;
      
      for (size_t i = 0; i < e.CutNames().size(); i++)
      {
         std::string name = "R";
         name+= std::to_string(runNumber);
         name += "-";
         name += dumpname;
         name += std::to_string(DumpNameCounter[dumpname]);
         //name += "_CutType_";
         //name += std::to_string(i);
         name += "_";
         name += e.CutNames().at(i);
         name += ".png";
         a.DrawVertexCanvas(name.c_str(), std::to_string(i))->SaveAs(name.c_str());
      }
      std::string name = std::string("R") + std::to_string(runNumber) + std::string("-") + dumpname + std::to_string(DumpNameCounter[dumpname]);
      a.ExportCSV(name.c_str());
      DumpNameCounter[dumpname]++;
   }
}

int main(int argc, char* argv[])
{
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 5000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 40000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }
   SaveFRD(runNumber);
   return 0;
}
