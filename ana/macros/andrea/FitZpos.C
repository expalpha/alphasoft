#include <vector>
#include <iostream>
#include <string>
#include <cstdlib>

#include "BuildConfig.h"
#include "RootUtilGlobals.h"
#include "TAGPlot.h"

#include "TCanvas.h"
#include "TH1D.h"
#include "TF1.h"
#include "TString.h"
#include "TStyle.h"

void FitZpos(std::vector<std::vector<int>> runNumber,
	     std::vector<std::string> description,
	     std::vector<int> repetition, std::string CutIndex)
{
  gStyle->SetOptStat(111);
  gStyle->SetOptFit(11);
  
  double Zmin = -1100.;
  double Zmax = 0.;
  std::vector<TH1D*> hZs;
  //   std::vector<TH2D*> hZTs;
  rootUtils::SetDefaultBinNumber(1000);
  int i=0;
  for (size_t jj=0; jj<runNumber.size(); jj++) {
    printf("Loading runs list %zu\n",jj);
    std::vector<int> runs = runNumber.at(jj);
    TAGPlot a;
    std::vector<std::string> desc;
    if (jj<description.size()) desc.push_back(description.at(jj));
    else desc.push_back(description[0]);
    printf("%s\n",desc[0].c_str());
    for (int run: runs) {
      printf("Adding run %d\n",run);
      a.AddDumpGates(run, desc, repetition);
    }
    a.LoadData();
    a.FillHisto(CutIndex);
    printf("Filling histos %d\n",i);
    TH1D* hZ = a.GetTH1D("zvtx");
    //TH2D* hZT = a.GetTH2D("ztvtx");
    hZs.push_back((TH1D*)hZ->Clone(Form("zvtx_%d",i)));
    //hZTs.push_back((TH2D*)hZT->Clone(Form("ztvtx_%d",i)));
    printf("...\n");
    hZs[i]->GetXaxis()->SetRangeUser(Zmin,Zmax);
    //hZTs[i]->GetXaxis()->SetRangeUser(Zmin,Zmax);
    printf("Histo %s has counts %.0f\n",desc[0].c_str(),hZs[i]->GetEntries());
    printf("...\n");
    i++;
  }
  printf("Number of Histos %zu\n",hZs.size());
  printf("Creating canvas\n");
  // TString cname = "c_ZT";
  // gStyle->SetOptStat(0);
  // TCanvas* c = new TCanvas(cname.Data(),cname.Data(), 1500, 1000);
  int col = 0;
  // //TLegend* legend = new TLegend(0.1,0.7,0.3,0.9);
  // for (TH2D* hZi: hZTs) {
  //    col++;
  //    hZi->SetMarkerColor(col);
  //    hZi->SetMarkerSize(2);
  //    hZi->SetMarkerStyle(21+col);
  //    hZi->Draw("same");
  //    //legend->AddEntry(hZi,description[col-1].c_str(),"p");
  // }
  // //legend->Draw();
  // c->Update();
   
  TString c2name = "c_Z";
  TCanvas* c2 = new TCanvas(c2name.Data(),c2name.Data(), 1500, 1000);
  col = 2;
  double max_Y = 0;
  for (TH1D* hZi: hZs) {
    double max = hZi->GetMaximum();
    if (max>max_Y) max_Y = max;
  }
  //TLegend* legend2 = new TLegend(0.1,0.7,0.3,0.9);

  int nz=0;
  std::vector<TF1*> ffit;
  for (TH1D* hZi: hZs)
    {
      col++;
      hZi->SetMaximum(1.1*max_Y);
      hZi->SetLineColor(col);
      hZi->SetFillColor(col);
      hZi->SetFillStyle(3001+col);
      hZi->Draw("same");
      //legend2->AddEntry(hZi,description[col-1].c_str(),"f");
      double meanZ = hZi->GetMean();
      double rms = hZi->GetStdDev();
      double isigma = 40.;
      double cst = hZi->GetBinContent(2);
      cout<<"Initial Values: A = "<<max_Y
	  <<"; mean = "<<meanZ
	  <<"; sigma = "<<isigma
	  <<"; const = "<<cst<<endl;
      TString funcname = TString::Format("fitZ_%d",nz);
      ffit.push_back( new TF1(funcname,"gaus(0)+pol0(3)",meanZ-3*rms,meanZ+3*rms) );
      ffit.back()->SetParName(0,"A");
      ffit.back()->SetParameter(0,max_Y);
      ffit.back()->SetParName(1,"mean");
      ffit.back()->SetParameter(1,meanZ);
      ffit.back()->SetParName(2,"sigma");
      ffit.back()->SetParameter(2,isigma);
      ffit.back()->SetParName(3,"const");
      ffit.back()->SetParameter(3,cst);
      
      hZi->Fit(ffit.back(),"R");

      // ffit.back()->FixParameter(1,ffit.back()->GetParameter(1));
      // ffit.back()->FixParameter(3,ffit.back()->GetParameter(3));

      // hZi->Fit(ffit.back(),"B","",
      // 	       ffit.back()->GetParameter(1)-2*ffit.back()->GetParameter(2),
      // 	       ffit.back()->GetParameter(1)+2*ffit.back()->GetParameter(2));
	
      ++nz;
    }
  //legend2->Draw();
  c2->Update();

  cout<<"Number of fits: "<<nz<<" ("<<ffit.size()<<")"<<endl;

}
