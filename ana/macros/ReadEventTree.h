#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"

#include "TLegend.h"
#include "TPaveText.h"
#include "TStyle.h"
#include "TROOT.h"

#include "TObjArray.h"
#include "TClonesArray.h"
#include "TString.h"
#include "TObjString.h"
#include "TVector3.h"

#include "TF1.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "TStoreEvent.hh"
#include "TSpacePoint.hh"

#include "IntGetters.h"
#include "TStringGetters.h"
