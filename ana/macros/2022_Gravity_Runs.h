#ifndef _2022_GRAVITY_RUNS_
#define _2022_GRAVITY_RUNS_

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <random>


int myrandom (int i) { return std::rand()%i;}
std::map<double, std::vector<std::vector<int>>> ShuffleMap (std::map<double, std::vector<std::vector<int>>> mymap)
{
    // Set fixed seed for randomness
    srand(42);
    //map<int,string> m;
    std::vector<double> v;

    for(auto i: mymap)
    {
        //std::cout << i.first << ":" << i.second.at(0) << std::endl;
        v.push_back(i.first);
    }
    std::random_shuffle(v.begin(), v.end(), myrandom);
    std::vector<double>::iterator it=v.begin();
    std::cout << std::endl;
    for(auto& i:mymap)
    {
        std::vector<std::vector<int>> ts=i.second;
        i.second=mymap[*it];
        mymap[*it]=ts;
        it++;
    }
    for(auto i: mymap)
    {
        //std::cout << i.first << ":" << i.second.at(0) << std::endl;
    }
    return mymap;
}

std::map<double, std::vector<std::vector<int>>> GetMap(bool shuffle = false)
{

  std::map<double, std::vector<std::vector<int>>> mymap = {
                                                            {{1},{
                                                               {9464},
                                                               {9471},
                                                               {9503},
                                                               {9513},
                                                               {9558},
                                                               {9569},
                                                               {9664}
                                                            }},
                                                            {{-1},{
                                                               {9463},
                                                               {9494},
                                                               {9504},
                                                               {9514},
                                                               {9560},
                                                               {9570},
                                                               {9661,9662}
                                                            }},
                                                            {{0},{
                                                               {9462},
                                                               {9486},
                                                               {9495},
                                                               {9505},
                                                               {9556},
                                                               {9565},
                                                               {9573}
                                                            }},
                                                            {{2},{
                                                               {9461},
                                                               {9502},
                                                               {9561},
                                                               {9566},
                                                               {9571},
                                                               {9645,9646,9647},
                                                               {9668}
                                                            }},
                                                            {{-2},{
                                                               {9460},
                                                               {9496},
                                                               {9512},
                                                               {9564},
                                                               {9568},
                                                               {9572},
                                                               {9659}
                                                            }},
                                                            {{-10},{
                                                               {9388},
                                                               {9395},
                                                               {9406},
                                                               {9421},
                                                               {9436}
                                                            }},
                                                            {{10},{
                                                               {9382},
                                                               {9384},
                                                               {9392},
                                                               {9408},
                                                               {9412},
                                                               {9422}
                                                            }},
                                                            {{-10.1},{
                                                               {9654,9655,9656},
                                                               {9660},
                                                               {9665},
                                                               {9673,9674},
                                                               {9678,9679,9680,9684},
                                                               {9694,9695}
                                                            }},
                                                            {{10.1},{
                                                               {9658},
                                                               {9663},
                                                               {9669,9670,9671},
                                                               {9675,9676,9677},
                                                               {9689,9690,9691},
                                                               {9696}
                                                            }},
                                                            {{3},{
                                                               {9586},
                                                               {9595},
                                                               {9613,9614},
                                                               {9618},
                                                               {9622},
                                                               {9632,9633,9634},
                                                               {9672}
                                                            }},
                                                            {{-3},{
                                                               {9585},
                                                               {9589},
                                                               {9612},
                                                               {9617},
                                                               {9621},
                                                               {9631},
                                                               {9657}
                                                            }},
                                                            {{0.5},{
                                                               {9588},
                                                               {9611},
                                                               {9616},
                                                               {9620},
                                                               {9629,9630},
                                                               {9639,9644},
                                                               {9685,9687}
                                                            }},
                                                            {{-0.5},{
                                                               {9587},
                                                               {9596},
                                                               {9615},
                                                               {9619},
                                                               {9624,9625,9626},//9624 no spillog
                                                               {9636,9637,9638},
                                                               {9648,9649,9650}
                                                            }},
                                                            {{1.5},{
                                                               {9701,9702},
                                                               {9712,9713},
                                                               {9716,9717,9718},
                                                               {9723,9724},
                                                               {9727,9728},
                                                               {9732}
                                                            }},
                                                            {{-1.5},{
                                                               {9704,9705},
                                                               {9714,9715},
                                                               {9719,9720,9721},
                                                               {9725,9726},
                                                               {9729},
                                                               {9733,9734,9735}
                                                            }}
                                                         };

    if(shuffle)
    {
        mymap = ShuffleMap(mymap);
    }
    return mymap;
}


std::map<double, std::vector<std::vector<int>>> GetSlowMap(bool shuffle = false)
{

  
  std::map<double, std::vector<std::vector<int>>> mymap = {
                                                            {{0},{
                                                               {9743},
                                                               {9747},
                                                               {9752},
                                                               {9759},
                                                               {9772},
                                                               {9777}
                                                            }},
                                                            {{-1},{
                                                               {9750},
                                                               {9751},
                                                               {9757,9758},
                                                               {9763,9765}, //9764 has empty spill log
                                                               {9776},
                                                               {9779}
                                                            }},
                                                            {{-2},{
                                                               {9745,9746},
                                                               {9748,9749},
                                                               {9756},
                                                               {9761},
                                                               {9773,9774},
                                                               {9778}
                                                            }},
                                                            // // 10g runs are fast ramps used for calibration
                                                            // {{-10},{
                                                            //    {9388},
                                                            //    {9395},
                                                            //    {9406},
                                                            //    {9421},
                                                            //    {9436}
                                                            // }},
                                                            // {{10},{
                                                            //    {9382},
                                                            //    {9384},
                                                            //    {9392},
                                                            //    {9408},
                                                            //    {9412},
                                                            //    {9422}
                                                            // }}
                                                         };
    if(shuffle)
    {
        mymap = ShuffleMap(mymap);
        mymap = ShuffleMap(mymap);
        mymap = ShuffleMap(mymap);
    }
    return mymap;
}

std::map<double, std::string> GetLabels(bool shuffle = false)
{

  std::map<double, std::string> mymap = {
                                                            {    {0},{"zerog"}},
                                                            {  {0.5},{"plus0p5g"}},
                                                            { {-0.5},{"minus0p5g"}},
                                                            {    {1},{"plus1g"}},
                                                            {   {-1},{"minus1g"}},
                                                            {  {1.5},{"plus1p5g"}},
                                                            { {-1.5},{"minus1p5g"}},
                                                            {    {2},{"plus2g"}},
                                                            {   {-2},{"minus2g"}},
                                                            {    {3},{"plus3g"}},
                                                            {   {-3},{"minus3g"}},
                                                            {   {10},{"plus10g"}},
                                                            {  {-10},{"minus10g"}},
                                                            { {10.1},{"plus10p1g"}},
                                                            {{-10.1},{"minus10p1g"}},
                                                         };
   return mymap;
}

std::map<double, std::string> GetSlowLabels(bool shuffle = false)
{

  std::map<double, std::string> mymap = {
                                                            {    {0},{"slowzerog"}},
                                                            {   {-1},{"slowminus1g"}},
                                                            {   {-2},{"slowminus2g"}},
                                                            {   {10},{"plus10g"}},
                                                            {  {-10},{"minus10g"}},
                                                         };
   return mymap;
}

std::map<int,std::vector<int>> GetZElectrodeRuns() {
   std::map<int,std::vector<int>> zmap = {
      {  2, { 9022 } },
      {  3, { 9025 } },
      {  6, { 9026 } },
      {  9, { 9027 } },
      { 15, { 9018 } },
      { 18, { 9015 } },
      { 20, { 9019 } }, 
      { 23, { 9017 } },
      { 26, { 9020 } },
      { 29, { 9016 } },
      { 32, { 9021 } },
      { 35, { 9013, 9014 } }
  };
  return zmap;
}

std::map<int,std::vector<int>> GetZLifetimeRuns() {
{
   std::map<int,std::vector<int>> zmap = {
     // At time of writing I dont know which electrode these are in
     { 0, {
            // These runs are from long before the experiment and probably
            // not going to be used for the paper
            //            
            // https://alphacpc05.cern.ch/elog/DataLog/86433
            // 7968,
            // https://alphacpc05.cern.ch/elog/DataLog/86459
            // 7984,
            // https://alphacpc05.cern.ch/elog/DataLog/86554
            // 8054,
            // https://alphacpc05.cern.ch/elog/DataLog/86597
            // 8086,
            // https://alphacpc05.cern.ch/elog/DataLog/86655
            // 8113,
            // https://alphacpc05.cern.ch/elog/DataLog/86763
            // 8191,
            // https://alphacpc05.cern.ch/elog/DataLog/85249
            // 7045,
            // https://alphacpc05.cern.ch/elog/DataLog/85738
            // 7325,
            // https://alphacpc05.cern.ch/elog/DataLog/85736
            // 7318,
            // https://alphacpc05.cern.ch/elog/DataLog/88887
            // 8877,
            // https://alphacpc05.cern.ch/elog/DataLog/86772
            // 8198,
            // https://alphacpc05.cern.ch/elog/DataLog/86847
            // 8263,
            
            // This is from the time of the 2022 experiment! Useful!
            // https://alphacpc05.cern.ch/elog/DataLog/91768
            9567,
            // 4 locaitons
            // https://alphacpc05.cern.ch/elog/DataLog/92027
            9731,
            // 2 locations
            // https://alphacpc05.cern.ch/elog/DataLog/92031
            9738 
          }
       }
    };
    return zmap;
}
}

std::map<std::string,std::vector<int>> GetSingleMirrorReleaseRuns() {
   // old method, keeping it here so other scripts are not broken
   std::map<std::string,std::vector<int>> map = {
     {"plus", {8618,8627,8633,8673,8703,8709,8711}},
     {"minus",{8619,8628,8634,8674,8708,8710,8744}}
   };
   return map;
}

std::map<std::string, std::vector<std::vector<int>>> GetSingleMirrorReleaseRunsVector() {
   // used in Official2022GravityRampWindows.C
   std::map<std::string, std::vector<std::vector<int>>> map = {
     {"plusMANYg", {{8618},{8627},{8633},{8673},{8703},{8709},{8711}}},
     {"minusMANYg",{{8619},{8628},{8634},{8674},{8708},{8710},{8744}}}
   };
   return map;
}

std::map<std::string,std::vector<std::vector<int>>> GetOctFRDRuns() {
   std::map<std::string,std::vector<std::vector<int>>> map = {
     {"OctFRD", {{9782},{9785}}}
   };
   return map;
}

std::vector<int> GetGravityRunList() 
{
   std::vector<int> runList;
   std::map<double,std::vector<std::vector<int>>> map = GetMap();
   for (const std::pair<double,std::vector<std::vector<int>>>& series : map)
   {
      for (const auto& runs: series.second)
      {
         for (const auto& run: runs)
         {
            runList.emplace_back(run);
         }
      }
   }
   map = GetSlowMap();
   for (const std::pair<double,std::vector<std::vector<int>>>& series : map)
   {
      for (const auto& runs: series.second)
      {
         for (const auto& run: runs)
         {
            runList.emplace_back(run);
         }
      }
   }
   return runList;
}

std::vector<int> GetSortedGravityRunList()
{
   std::vector<int> runList = GetGravityRunList();
   std::sort(runList.begin(),runList.end());
   return runList;
}

// Function to get the run numbers of all runs relivant to the 2022 Gravity meansurement
// Lifetime
// Z position
// Cosmic runs
// Actual measurement
std::vector<int> GetSortedAllRuns()
{
   std::vector<int> runList = GetGravityRunList();
   for (const auto& pair: GetZElectrodeRuns())
      for (const int& run: pair.second)
         runList.emplace_back(run);
   for (const auto& pair: GetZLifetimeRuns())
      for (const int& run: pair.second)
         runList.emplace_back(run);
   for (const auto& pair: GetSingleMirrorReleaseRuns())
      for (const int& run: pair.second)
         runList.emplace_back(run);
   std::sort(runList.begin(),runList.end());
   return runList;
}


void PrintAllSortedRuns()
{
  std::vector<int> runList = GetSortedAllRuns();
   for (const int& run: runList)
      std::cout << run <<"\n";
   std::cout << std::endl;
   return;
}  

void PrintSortedGravityRuns()
{
   std::vector<int> runList = GetSortedGravityRunList();
   for (const int& run: runList)
      std::cout << run <<"\n";
   std::cout << std::endl;
   return;
   
}


struct time_window {
  double plot_start; /// First time (seconds from start of run) of a time window to save to CSV
  double plot_stop; /// Last time (seconds from start of run) of a time window to save to CSV
  //double plot_zero; /// Time (seconds from start of run) defined as 'zero' ie the start of a ramp
};


// Declare all time windows... these time windows are adjusted per Chris'
// elog : elog:ALPHA/30666
time_window GetBackgroundTimes(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023
time_window GetSolBTimes(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023
time_window GetLOcTimes(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023
time_window GetECR0Times(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023 (its just the time between ramps)
time_window GetMAGB0Times(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023
time_window GetECR1Times(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023
time_window GetSOcTimes(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023
time_window GetMAGB1Times(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023
time_window GetECR2Times(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023
std::string GetLOcDumpName(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023
std::string GetMAGBName(int runNumber); // Reviewed by JTKM, IC, JS - 11th Jan 2023

// We will see 102ms wait time often below
// The magnet controller needs 100ms to set the Magnet PID
// elog:AGMagnetEvents/1374
// TODO Does the 2ms come from the sequencer?

time_window GetBackgroundTimes(int runNumber)
{
   time_window thewindow;
   thewindow.plot_start = 0.;
   thewindow.plot_stop = 0.;
   // List of runs that didn't start from zero pbars
   std::vector<int> end_runs = {
     
   };
   // Return runs that have no background window
   for (const int& run: end_runs)
      if (run == runNumber)
         return thewindow;
   
   std::vector<TAGSpill> PbarTransfer = Get_AG_Spills(runNumber,{"Pbar Transfer FSTLNE"},{0});
   if (PbarTransfer.empty())
      PbarTransfer = Get_AG_Spills(runNumber,{"Ekick3"},{0});
   if (PbarTransfer.empty())
   {
      std::cout <<"Run " << runNumber << " has no background" <<std::endl; 
      return thewindow;
   }
   thewindow.plot_start = 10.;
   thewindow.plot_stop = PbarTransfer.front().GetStartTime();
   return thewindow;
}

time_window GetSolBTimes(int runNumber)
{
   // SolB Starts 60 seconds before the start dump marker of LOct
   // We include the 1.102 seconds during the LOct ramp (since the ramp hasn't started yet)
   // Total time = 60 + 1.102

   // Stop time = Start of long octopole
   // Start time = Stop time - 60
   // Time zero = Start time
   time_window thewindow;
   
   if (runNumber > 8744)
   {
      // Get start of LOct
      std::string LOctName = GetLOcDumpName( runNumber );
      std::vector<TAGSpill> LOct = Get_AG_Spills(runNumber,{LOctName.c_str()},{0});
      double start_time_of_LOct_dump = LOct.front().GetStartTime();
      
      // Compute time window
      thewindow.plot_start = start_time_of_LOct_dump - 60;
      thewindow.plot_stop = start_time_of_LOct_dump + 1.102;
   } 
   
   // single mirror release runs had SolB, LOct and MirA/G ramps, 10s wait and All Magnets rampdown
   else if (runNumber >= 8618 && runNumber <= 8744)
    {
      std::string SolBName = "SolBRampDown";
      std::vector<TAGSpill> SolB = Get_AG_Spills(runNumber,{SolBName.c_str()},{0});
      thewindow.plot_start = SolB.front().GetStartTime();
      thewindow.plot_stop =SolB.front().GetStopTime();
   }
   return thewindow;
}

time_window GetLOcTimes(int runNumber)
{
   // LOct starts 1.102 seconds after the dump marker
   // The ramp lasts 1 second... but quasi trapped stuff comes out late
   // here we add 21 seconds (per Chuckman's elog:/ALPHA/30518) and 
   // ignore the stop time of the dump marker
   
   // Get start of LOct
   std::string LOctName = GetLOcDumpName( runNumber );
   std::vector<TAGSpill> LOct = Get_AG_Spills(runNumber,{LOctName.c_str()},{0});
   double start_time_of_LOct_dump = LOct.front().GetStartTime();
   double stop_time_of_LOct_dump = LOct.front().GetStopTime();
   
   // Compute time window
   time_window thewindow;
   thewindow.plot_start = start_time_of_LOct_dump + 1.102;
   thewindow.plot_stop = thewindow.plot_start + 21.;
   
   // single mirror release runs had SolB, LOct and MirA/G ramps, 10s wait and All Magnets rampdown
   if (runNumber >= 8618 && runNumber <= 8744)
   {
      thewindow.plot_start = start_time_of_LOct_dump;
      thewindow.plot_stop = stop_time_of_LOct_dump;      
   }

   return thewindow;
}

time_window GetECR0Times(int runNumber)
{
   // single mirror release runs had SolB, LOct and MirA/G ramps, 10s wait and All Magnets rampdown
   if (runNumber >= 8618 && runNumber <= 8744)
   {
      time_window thewindow;
      thewindow.plot_start = 0;
      thewindow.plot_stop = 0;
      return thewindow;      
   }
   
   // Get the time after the LOct, until the start of MAGB ramp0
   double end_time_of_LOct = GetLOcTimes(runNumber).plot_stop;
   
   double start_time_of_MAGB0 = GetMAGB0Times(runNumber).plot_start;
   time_window thewindow;
   thewindow.plot_start = end_time_of_LOct;
   thewindow.plot_stop = start_time_of_MAGB0;
   
   return thewindow;
}

time_window GetMAGB0Times(int runNumber)
{
   // MAGB ramp starts 0.102 after the start dump marker
   // MAGB lasts 20 seconds for fast ramp, 130s for slow ramp
   // PLUS The magnet controller waits an 1 second after the end of the ramp...
   // AND 0.1 second before being ready to ramp the porch to zero
   /* 
    * <Segment time="1" type="Wait"/>
    * <Segment time=".1" type="Wait" waitOnTrigger="1"/>
    */
   // We include this 1.1 seconds to see quasitrapped atoms by using the
   // end dump marker

   // Get start of MAGB
   std::string MAGBName = GetMAGBName( runNumber );
   std::vector<TAGSpill> MAGB = Get_AG_Spills(runNumber,{MAGBName.c_str()},{0});
   double start_time_of_MAGB_dump = MAGB.front().GetStartTime();
   double stop_time_of_MAGB_dump = MAGB.front().GetStopTime();
   time_window thewindow;
   thewindow.plot_start = start_time_of_MAGB_dump + 0.102;
   thewindow.plot_stop = stop_time_of_MAGB_dump;
   return thewindow;
}

time_window GetECR1Times(int runNumber)
{
   // Get the end of the MAGB ramp until the start of the Short Oct ramp
   time_window thewindow;
   thewindow.plot_start = GetMAGB0Times(runNumber).plot_stop;
   thewindow.plot_stop = GetSOcTimes(runNumber).plot_start;

   // for runs without SOc ramp get MAGB1 start time, if no MAGB1 get end of run
   if (thewindow.plot_stop == 0)
   {
      std::cerr << "GetECR1Times(" << runNumber << ") stop time is 0, no SOc? Will try MAGB1" <<std::endl;
      thewindow.plot_stop = GetMAGB1Times(runNumber).plot_start;
      if  (thewindow.plot_stop == 0)
            std::cerr << "GetECR1Times(" << runNumber << ") stop time is 0, no MAGB1? Will get end of run time" <<std::endl;
            thewindow.plot_stop = GetAGTotalRunTime(runNumber) - 10.;
   }
   return thewindow;
}

time_window GetSOcTimes(int runNumber)
{
   // Short oct ramp starts 1.002 second after the start of the dump marker
   // The ramp lasts for 1 second
   // However, there is 12s or 16s before MAGB1, so we could include that time in this window until MAGB1 ramp start
   // If there is no MAGB2, then this is the last dump in the sequence
   // ref elog Subject: Re: Magnet ramp timing for current gravity sequences
   //          elog:ALPHA/30666

   time_window thewindow;
   thewindow.plot_start = 0;
   thewindow.plot_stop = 0;
 
   std::string OcName = "Oc RampDown";
   std::string MAGBName = GetMAGBName( runNumber );
   std::vector<TAGSpill> Oc = Get_AG_Spills(runNumber,{OcName.c_str()},{0});
   std::vector<TAGSpill> MAGB2 = Get_AG_Spills(runNumber,{MAGBName.c_str()},{1});
   if (Oc.size())
   {
      thewindow.plot_start = Oc.front().GetStartTime() + 1.002;
      if (MAGB2.empty())
         // If there is no MAGB2 (ie no ramp from porch to zero, there is nothing that happens after this dump... so 16.2 seconds is the same as infinite time)
         thewindow.plot_stop = Oc.front().GetStartTime() + 1.002 + 16.2;
      else
         thewindow.plot_stop = GetMAGB1Times(runNumber).plot_start;
   }
   
   // single mirror release runs had SolB, LOct and MirA/G ramps, 10s wait and All Magnets rampdown
   if (runNumber >= 8618 && runNumber <= 8744)
   {
      std::string OcName = "All Magnets RampDown";
      std::vector<TAGSpill> Oc = Get_AG_Spills(runNumber,{OcName.c_str()},{0});
      thewindow.plot_start = Oc.front().GetStartTime();
      thewindow.plot_stop = Oc.front().GetStopTime();
   }
   return thewindow;
}

time_window GetMAGB1Times(int runNumber)
{
   // MAGB1 ramp starts 0.102 after the start dump marker
   // From the magnet controller:
   /*
    * 100ms DCCT wait (its already waited for the trigger after the SOc ramp down)
    */
   // MAGB1 lasts 5 seconds
   // We include this 1 second to see quasitrapped atoms
   // ref elog:ALPHA/30666

   time_window thewindow;
   thewindow.plot_start = 0.;
   thewindow.plot_stop = 0.;

   // Get start of MAGB
   std::string MAGBName = GetMAGBName( runNumber );
   std::vector<TAGSpill> MAGB2 = Get_AG_Spills(runNumber,{MAGBName.c_str()},{1});
   if (MAGB2.empty())
      return thewindow;
   
   thewindow.plot_start = MAGB2.front().GetStartTime() + 0.102;
   thewindow.plot_stop  = thewindow.plot_start + 5. + 1.;
   return thewindow;
}

time_window GetECR2Times(int runNumber)
{
   // This function gets the time from the last dump until the end of run
   time_window thewindow;
   
   std::string MAGBName = GetMAGBName( runNumber );
   std::vector<TAGSpill> MAGB2 = Get_AG_Spills(runNumber,{MAGBName.c_str()},{1});
   if (MAGB2.empty())
      // In the case there is no ramp from porch to zero... get last time from SOc
      thewindow.plot_start = GetSOcTimes(runNumber).plot_stop;
   else
      thewindow.plot_start = GetMAGB1Times(runNumber).plot_stop;
   
   thewindow.plot_stop = GetAGTotalRunTime(runNumber) - 10.;
   
   if (thewindow.plot_start == 0.)
   {
      std::cerr << "GetECR3Times(" << runNumber << ") failed somehow! It couldnt determine the start time" <<std::endl;
      thewindow.plot_stop = 0.;
   }
   
   return thewindow;
}

std::string GetLOcDumpName(int runNumber)
{
   // The "correct" dump name is "LOc RampDown"
   std::string LOcName = "LOc RampDown";
   // Before run 9256, there was no space
   if (runNumber < 9256)
      LOcName = "LOctRampDown";
   // These are two special cases with no camel case
   if (runNumber == 9382 || runNumber == 9384)
      LOcName = "LOc ramp down";
   return LOcName;
}

std::string GetMAGBName(int runNumber)
{
   // The "correct" dump name is "MAGB Tickle RampDown" (from run 9388 to 9786)
   std::string MAGBName = "MAGB Tickle RampDown";

   // The first runs had the wrong dump name (run 8619 to 9121)
   if(runNumber < 9122)
   {
      if (Get_AG_Spills(runNumber,{"MirGRampDown"},{0}).size())
         MAGBName = "MirGRampDown";
      else if (Get_AG_Spills(runNumber,{"MirARampDown"},{0}).size())
         MAGBName = "MirARampDown";
   }

   // The first two runs were special and used snake camel case (sort of)
   if (runNumber == 9382 || runNumber == 9384)
      MAGBName = "MAGB_Tickle_Ramp down";
   return MAGBName;
}

void PrintRunDates()
{
   // Warm up files (and get the open file warnings out the way before writing table
   for (const auto& run: GetSortedGravityRunList())
   {
      Get_File(run);
   }

   std::cout << "g,firstrun,lastrun,StartTime,StopTime,StartTimeString,StopTimeString\n";
   for (auto gvalue: GetMap())
   {
      for (const auto& runs: gvalue.second)
      {
         int firstrun = runs.front();
         int lastrun = runs.back(); 
         TAGAnalysisReport firstr=Get_AGAnalysis_Report(firstrun);
         TAGAnalysisReport lastr=Get_AGAnalysis_Report(lastrun);
         std::cout<<gvalue.first << ","<< firstrun << ","<< lastrun << "," << firstr.GetRunStartTime() << "," << lastr.GetRunStopTime() << "," << firstr.GetRunStartTimeString() << "," << lastr.GetRunStopTimeString() << "\n";
      }
   }
}

double GetBiasFromRun(int runNumber)
{
   // for 20s ramps
   for (auto gvalue: GetMap())
   {
      for (const auto& runs: gvalue.second)
      {
         for (const auto& run: runs)
         {
            if(runNumber == run)
               return gvalue.first;
         }
      }
   }
   // 130s ramps
   for (auto gvalue: GetSlowMap())
   {
      for (const auto& runs: gvalue.second)
      {
         for (const auto& run: runs)
         {
            if(runNumber == run)
               return gvalue.first;
         }
      }
   }
   return -99;
}





#endif
