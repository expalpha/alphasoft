TH1D* GetHistoFromCanvas(std::string&);
TCanvas* zHistoFit(TH1D*);
Double_t background    (Double_t *, Double_t *);
Double_t lorentzianPeak(Double_t *, Double_t *);
Double_t gaussianPeak  (Double_t *,Double_t *);
Double_t fitFunction   (Double_t *, Double_t *);
Double_t zmin;
Double_t zmax;
Double_t bin_width;
Int_t l_width = 4;

void zfit(std::string sf="") {
    if(sf.size()<=0) {
        std::cout << "Please choose/input a file to analyze" << std::endl;
        gROOT->ProcessLine(".q");
	return;
    }
    TH1D* h = GetHistoFromCanvas(sf); ///< Get the histo from the CANVAS ROOT FILE
    TCanvas* c = zHistoFit(h); ///< Get the Fit
    std::string filename = sf + ".png";
    c->SaveAs(filename.c_str());
}


TCanvas* zHistoFit(TH1D *hz = nullptr) {
    zmin = hz->GetXaxis()->GetXmin();
    zmax = hz->GetXaxis()->GetXmax();
    bin_width = (zmax-zmin)/hz->GetXaxis()->GetNbins();
    std::cout << "Bin width " << bin_width << std::endl;
    ///< Plotting the histogram
    TCanvas *c = new TCanvas("c","c",900,1200);
    c->Draw();
    hz->Draw("E0");
    ///< Performing the fitting 
   TF1 *fitFcn = new TF1("fitFcn",fitFunction,zmin,zmax,9);
   fitFcn->SetNpx(500);
   fitFcn->SetLineColor(kBlue-7); fitFcn->SetLineWidth(l_width); 

   Double_t zmean = hz->GetMean();
   fitFcn->SetParameters(hz->GetMaximum()*0.3,(zmax-zmin)/2.,150.,hz->GetMaximum()*0.3,(zmax-zmin)/2.,50.,hz->GetMaximum()*0.3,(zmax-zmin)/2.,10.);
   fitFcn->SetParameter(0,hz->GetMaximum()*0.6); fitFcn->SetParameter(1,zmean); fitFcn->SetParameter(2,10.);
   fitFcn->SetParameter(3,hz->GetMaximum()*0.3); fitFcn->SetParameter(4,zmean); fitFcn->SetParameter(5,30.);
   fitFcn->SetParameter(6,hz->GetMaximum()*0.1); fitFcn->SetParameter(7,zmean); fitFcn->SetParameter(8,100.);
   hz->Fit("fitFcn","V+","ep");
   std::cout << "Chi2/NDF " << fitFcn->GetChisquare()/fitFcn->GetNDF() << std::endl;

   ///< Drawing the results
   TF1 *gaus0 = new TF1("gaus0",gaussianPeak,zmin,zmax,3); gaus0->SetNpx(500);
   TF1 *gaus1 = new TF1("gaus1",gaussianPeak,zmin,zmax,3); gaus1->SetNpx(500);
   TF1 *gaus2 = new TF1("gaus2",gaussianPeak,zmin,zmax,3); gaus2->SetNpx(500);

   gaus0->SetLineColor(kRed-7); gaus0->SetLineWidth(l_width); gaus0->SetLineStyle(9); 
   gaus1->SetLineColor(kMagenta-9); gaus1->SetLineWidth(l_width); gaus1->SetLineStyle(9); 
   gaus2->SetLineColor(kGray+2);  gaus2->SetLineWidth(l_width); gaus2->SetLineStyle(9); 
   // writes the fit results into the par array
   double par[9];
   fitFcn->GetParameters(par);
 
   gaus0->SetParameters(par);
   gaus0->Draw("same");
   std::cout << "Back integral " << gaus0->Integral(zmin, zmax)/bin_width << std::endl;

   gaus1->SetParameters(&par[3]);
   gaus1->Draw("same");
   std::cout << "gaus1 integral " << gaus1->Integral(zmin, zmax)/bin_width << std::endl;

   gaus2->SetParameters(&par[6]);
   gaus2->Draw("same");
   std::cout << "SignalFcn2 integral " << gaus2->Integral(zmin, zmax)/bin_width << std::endl;

   // draw the legend
   std::ostringstream os;
   TLegend *legend1=new TLegend(0.15,0.55,0.42,0.7);
   legend1->SetTextSize(0.03);
   legend1->AddEntry(hz,"Data","lpe");
   legend1->AddEntry(gaus0,"Gaussian 0","l");
   legend1->AddEntry(gaus1,"Gaussian 1","l");
   legend1->AddEntry(gaus2,"Gaussian 2","l");
   os.clear(); os.str(""); os << "Global (#chi^{2} " << std::setprecision(3) << fitFcn->GetChisquare()/fitFcn->GetNDF() << ")";
   legend1->AddEntry(fitFcn,os.str().c_str(),"l");
   legend1->Draw();

   ///< Showing integrals
   Double_t I0=0., I1=0., I2=0., IT=0.;
   Double_t I0p=0., I1p=0., I2p=0.;
   I0 = gaus0->Integral(zmin, zmax)/bin_width;
   I1 = gaus1->Integral(zmin, zmax)/bin_width;
   I2 = gaus2->Integral(zmin, zmax)/bin_width;
   IT = I0 + I1 + I2;
   I0p = (I0/IT)*100.; ///< In percentage
   I1p = (I1/IT)*100.; ///< In percentage
   I2p = (I2/IT)*100.; ///< In percentage
   TLegend *legend2=new TLegend(0.55,0.55,0.87,0.7);
   legend2->SetTextSize(0.03);
   legend2->AddEntry(hz,"Events","");
   os.clear(); os.str(""); os << std::fixed << std::setprecision(0) << I0 << " (" << std::setprecision(1) << I0p << "\%)";
   legend2->AddEntry(gaus0,os.str().c_str(),"l");
   os.clear(); os.str(""); os << std::fixed << std::setprecision(0) << I1 << " (" << std::setprecision(1) << I1p << "\%)";
   legend2->AddEntry(gaus1,os.str().c_str(),"l");
   os.clear(); os.str(""); os << std::fixed << std::setprecision(0) << I2 << " (" << std::setprecision(1) << I2p << "\%)";
   legend2->AddEntry(gaus2,os.str().c_str(),"l");
   os.clear(); os.str(""); os << "Sum " << std::fixed << std::setprecision(0) << IT;
   legend2->AddEntry(fitFcn,os.str().c_str(),"l");
   legend2->Draw();

   ///< Showing Sigmas
   Double_t s0=0., s1=0., s2=0., s0e1=0.;
   s0 = gaus0->GetParameter(2);
   s1 = gaus1->GetParameter(2);
   s2 = gaus2->GetParameter(2);
   s0e1 = (s0+s1)/2.;
   TLegend *legend3=new TLegend(0.55,0.30,0.87,0.45);
   legend3->SetTextSize(0.03);
   legend3->AddEntry(hz,"Sigmas","");
   os.clear(); os.str(""); os << std::fixed << std::setprecision(1) << "#sigma_{gaus0} " << s0 << " cm";
   legend3->AddEntry(gaus0,os.str().c_str(),"l");
   os.clear(); os.str(""); os << std::fixed << std::setprecision(1) << "#sigma_{gaus1} " << s1 << " cm";
   legend3->AddEntry(gaus1,os.str().c_str(),"l");
   os.clear(); os.str(""); os << std::fixed << std::setprecision(1) << "#sigma_{gaus2} " << s2 << " cm";
   legend3->AddEntry(gaus2,os.str().c_str(),"l");
   os.clear(); os.str(""); os << "#bar{#sigma}_{gaus0+gaus1} " << std::fixed << std::setprecision(1) << s0e1 << " cm";
   legend3->AddEntry(fitFcn,os.str().c_str(),"");
   legend3->Draw();

   c->Modified(); c->Update();
   return c;
}

TH1D* GetHistoFromCanvas(std::string& s) {
    TFile *f = new TFile(s.c_str()); 
    TCanvas *abc = (TCanvas*)f->Get("abc");
    ///< Accessing the primitives of the abc canvas
    TList *abc_list = (TList*)abc->GetListOfPrimitives();
//    abc_list->ls();
    TIter abcI(abc_list);
    TPad *p = (TPad*)abcI(); ///< This is the pad containing the 
    ///< Accessing the primitives of the TPad
    TList *p_list = (TList*)p->GetListOfPrimitives();
//    p_list->ls();
    TIter pI(p_list);
    TH1D *h = (TH1D*)pI();
    h->SetName("");
    h->SetStats("rme");
    h->GetYaxis()->SetTitleOffset(1.6);
    h->SetMarkerStyle(20);
    h->SetMarkerSize(0.8);
    return h;
}



// Quadratic background function
Double_t background(Double_t *x, Double_t *par) {
   return par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
}
 
// Lorenzian Peak function
Double_t lorentzianPeak(Double_t *x, Double_t *par) {
  return (0.5*par[0]*par[1]/TMath::Pi()) / TMath::Max( 1.e-10,(x[0]-par[2])*(x[0]-par[2]) + .25*par[1]*par[1]);
}

// Gaussian Peak function 
Double_t gaussianPeak(Double_t *x,Double_t *par) {
      Double_t arg = 0;
      if (par[2]!=0) arg = (x[0] - par[1])/par[2];
      return par[0]*TMath::Exp(-0.5*arg*arg);;
} 

// Sum of background and peak function
Double_t fitFunction(Double_t *x, Double_t *par) {
//  return background(x,par) + lorentzianPeak(x,&par[3]);
//   return background(x,par) + gaussianPeak(x,&par[3]);
//   return background(x,par) + gaussianPeak(x,&par[3]) + gaussianPeak(x,&par[6]);
  return gaussianPeak(x,par) + gaussianPeak(x,&par[3]) + gaussianPeak(x,&par[6]);
}

