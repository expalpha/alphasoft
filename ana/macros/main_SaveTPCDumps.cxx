#include "PlotGetters.h"

#include "TChronoChannelName.h"
#include "TChronoChannelGetters.h"
#include "TAGPlot_Filler.h"

void SaveAllDumpsTPC(int runNumber, bool SaveEmpty = false)
{
   std::vector<TAGSpill> all = Get_AG_Spills(runNumber,{"*"},{-1});
   
   std::vector<TAGPlot*> plots;
   std::vector<std::string> plot_names;
   
   
   std::map<std::string,int> repetition_counter;
   if (all.empty())
   {
      double svd = GetTotalRunTimeFromTPC(runNumber);
      double sis = GetTotalRunTimeFromChrono(runNumber);
      double tmax;
      if ( sis > svd)
         tmax = sis;
      else
         tmax = svd;
      TAGPlot* a = new TAGPlot();
      a->AddTimeGate(runNumber, 0, tmax);
      plots.push_back(a);
      a->LoadData();
   }
   else
   {
      TAGPlot_Filler DataLoader;

      for (size_t j = 0; j < all.size(); j++)
      {
         TAGSpill s = all.at(j);

         if (s.GetSequenceName() != "atm_botg" && s.GetSequenceName() != "rct_botg")
            continue;

         if (s.GetStartTime() == s.GetStopTime())
         {
            std::cout<<s.fName << " has no time duration (its a pulse...) skipping plot" <<std::endl;
            continue;
         }
         if (!s.fScalerData)
         {
            std::cout<<s.fName << " has no scaler data (its an information dump...) skipping plot" <<std::endl;
            continue;
         }

         std::string dump_name = s.GetSequenceName() + "_" + s.fName + "_" + std::to_string(repetition_counter[s.GetSequenceName() + "_" + s.fName]++);
         // Remove quote marks... they upset uploading to elog
         dump_name.erase(std::remove(dump_name.begin(), dump_name.end(), '"'), dump_name.end());
         std::cout << "dump_name:"<< dump_name <<std::endl;

         TAGPlot* a = new TAGPlot();
         a->AddDumpGates(runNumber, { s });
         DataLoader.BookPlot(a);

         plots.push_back(a);
         plot_names.push_back(dump_name);
      }
      DataLoader.LoadData();
   }

   
   std::string folder = "AutoChronoPlots/";
   gSystem->mkdir(folder.c_str());
   folder += std::to_string(runNumber) + "/";
   gSystem->mkdir(folder.c_str());
   folder += "TPC/";
   gSystem->mkdir(folder.c_str());
   {
      TAGDetectorEvent e;
      const int ncuts = e.CutNames().size();
      for (int cut = 0; cut < ncuts; cut++ )
      {
         std::string subfolder = folder + e.GetOnlinePassCutsName(cut) + "/";
         gSystem->mkdir(subfolder.c_str());
      }
   }  
   if (all.empty())
   {
      std::cout <<"No dumps found... Save as:\t";
      TAGDetectorEvent e;
      const int ncuts = e.CutNames().size();
      for (int cut = 0; cut < ncuts; cut++ )
      {
         std::string filename = folder + e.GetOnlinePassCutsName(cut) + "/R" + std::to_string(runNumber) + std::string("_EntireRun.png");
         std::cout  << filename<<std::endl;
         plots.front()->DrawVertexCanvas("entire_run", std::to_string(cut))->SaveAs(filename.c_str());
      }
      std::cout<<"Done"<<std::endl;
   }
   else
   {
      for (size_t j = 0; j < plots.size(); j++)
      {   
         TAGDetectorEvent e;
         const int ncuts = e.CutNames().size();
         for (int cut = 0; cut < ncuts; cut++ )
         {
            std::cout <<"cut: " << cut <<std::endl;
            if (plots.at(j)->GetVertexEvents().CountPassedCuts(cut) || SaveEmpty)
            {
               std::string filename = folder + e.GetOnlinePassCutsName( cut) + "/R" + std::to_string(runNumber) + std::string("_") + plot_names.at(j) + ".png";
               std::cout <<"\t" << filename<<std::endl;
               plots.at(j)->DrawVertexCanvas(plot_names.at(j).c_str(),std::to_string(cut))->SaveAs(filename.c_str());
            }
         }
      }
   }
}
   
int main(int argc, char* argv[])
{
   gStyle->SetPalette(kCool);
   int runNumber;
   std::vector<std::string> args;

   for (int i=0; i<argc; i++) 
   {
      args.push_back(argv[i]);
   }
   if (args.size() != 2)
   {
      std::cout << __FILE__ << " requires exactly 1 argument: The run number\n";
      return 1;
   }
    
   runNumber = stoi(args.at(1));
   if (runNumber < 5000)
   {
      std::cout <<"run number too small (" << runNumber << ")\n";
      return 1;
   }
   if (runNumber > 40000)
   {
      std::cout <<"run number too large (" << runNumber << ")\n";
      return 1;
   }
   SaveAllDumpsTPC(runNumber);
   return 0;
}
