//Std Lib
#include <vector>
//Root stuff
#include "TChain.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TBranch.h"
#include "TFile.h"
#include "TSystem.h"
//Our stuff
#include "TA2Plot.h"
#include "TA2Plot_Filler.h"
//#include "../../agana/EventTracker.h"
#include "bsc/CalibrateBarrel.C"
#include "EventTracker.h"
#include <sstream>

int main(int argc, char* argv[])
{
    std::string inputfile;
    std::vector<std::string> args;
    for (int i=0; i<argc; i++) 
    {
        args.push_back(argv[i]);
    }
    for (unsigned int i=1; i<args.size(); i++) // loop over the commandline options
    { 
        if(args[i] == "--input")
            inputfile = args[i+1];
    }

    //Grab all input runs
    //Loop through them getting the runs we need to callibrate them
    //Check unique runs
    //print to new file
    //put file in correct location


    // Vector to store the integers
    std::vector<int> runNumbers;

    // Open the input file
    std::ifstream inputFile(inputfile);
    if (!inputFile) {
        std::cerr << "Unable to open file: " << inputfile << std::endl;
        return 1;
    }

    // Read the file line by line
    std::string line;
    while (std::getline(inputFile, line)) {
        // Create a stringstream from the line
        std::stringstream ss(line);

        // Read the integer part (before the comma)
        int number;
        if (ss >> number) {
            runNumbers.push_back(number);
        }

        // Ignore the rest of the line (after the comma)
    }

    // Close the input file
    inputFile.close();

    // Print the contents of the vector
    std::cout << "runNumbers read from the file:" << std::endl;
    for (int num : runNumbers) {
        std::cout << num << " ";
    }
    std::cout << std::endl;


    std::vector<int> allRunsForAgana;

    for(int run: runNumbers)
    {
        std::vector<int> currentRunsForAgana = GetRunNumsForCalibration(run);
        for(int currentrun: currentRunsForAgana)
        {
            allRunsForAgana.push_back(currentrun);
        }
    }

    std::sort( allRunsForAgana.begin(), allRunsForAgana.end() );
    allRunsForAgana.erase( std::unique( allRunsForAgana.begin(), allRunsForAgana.end() ), allRunsForAgana.end() );



    // Define the output file name
    std::string outputFileName = "values_for_agana.txt";

    // Open the output file
    std::ofstream outputFile(outputFileName);
    if (!outputFile) {
        std::cerr << "Unable to open file: " << outputFileName << std::endl;
        return 1;
    }

    // Write the filtered vector to the file
    for (int num : allRunsForAgana) {
        outputFile << num << std::endl;
    }

    // Close the output file
    outputFile.close();

    std::cout << "Unique values written to " << outputFileName << std::endl;


    return EXIT_SUCCESS;
}