 // By Pooja Woosaree
// Work in progress

#include <ROOT/RDF/Utils.hxx>
#include <ROOT/TSeq.hxx>
#include <ROOT/RCsvDS.hxx>
#include <ROOT/RRawFile.hxx>
#include <TError.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
 
#include <algorithm>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>


//export AGOUTPUT=/eos/experiment/ALPHAg/woollysocks

Double_t eightgaus(Double_t *x, Double_t *par){
//TF1 *total = new TF1("mstotal","pol0(0)+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)+gaus(16)+gaus(19)+gaus(22)",mean - 2*stddev,mean+2*stddev);
  return par[0]+par[1]*TMath::Exp(-0.5*((x[0]-par[2])/par[3])*((x[0]-par[2])/par[3]))+par[4]*TMath::Exp(-0.5*((x[0]-par[2])/par[5])*((x[0]-par[2])/par[5]))+par[6]*TMath::Exp(-0.5*((x[0]-par[7])/par[8])*((x[0]-par[7])/par[8]))+par[9]*TMath::Exp(-0.5*((x[0]-par[7])/par[10])*((x[0]-par[7])/par[10]))+par[11]*TMath::Exp(-0.5*((x[0]-par[12])/par[13])*((x[0]-par[12])/par[13]))+par[14]*TMath::Exp(-0.5*((x[0]-par[12])/par[15])*((x[0]-par[12])/par[15]))+par[16]*TMath::Exp(-0.5*((x[0]-par[17])/par[18])*((x[0]-par[17])/par[18]))+par[19]*TMath::Exp(-0.5*((x[0]-par[17])/par[20])*((x[0]-par[17])/par[20]));
}

Double_t fourgaus(Double_t *x, Double_t *par){
  return par[0]+par[1]*TMath::Exp(-0.5*((x[0]-par[2])/par[3])*((x[0]-par[2])/par[3]))+par[4]*TMath::Exp(-0.5*((x[0]-par[2])/par[5])*((x[0]-par[2])/par[5]))+par[6]*TMath::Exp(-0.5*((x[0]-par[7])/par[8])*((x[0]-par[7])/par[8]))+par[9]*TMath::Exp(-0.5*((x[0]-par[7])/par[10])*((x[0]-par[7])/par[10]));
}

Double_t twogaus(Double_t *x, Double_t *par){
  return par[0]+par[1]*TMath::Exp(-0.5*((x[0]-par[2])/par[3])*((x[0]-par[2])/par[3]))+par[4]*TMath::Exp(-0.5*((x[0]-par[2])/par[5])*((x[0]-par[2])/par[5]));
}

void zlifetime(std::string cut = "6") {

  TAGPlot a(-1200,0);
  //pbar lifetime
  std::vector<int> runNumberE28 = {9567}; //E27 + E28
  std::vector<int> runNumberElife1 = {9731}; //E11,23,29,35
  std::vector<int> runNumberElife2 = {9738}; //E23,35
  std::vector<int> runNumberLifes = {9731, 9738};
      rootUtils::SetDefaultBinNumber(800);
  ofstream zfit;
  TString Szfit = "zfit_lifetimes_800bins_glassshoe_cut";
  Szfit += cut;
  Szfit += ".csv";
  zfit.open(Szfit);
  zfit << "electrode, fit mean (mm), fit error (mm), sigma z (mm), sigma error (mm)"<<std::endl;

  for (int runNumberE: runNumberElife2){
    std::vector<TAGSpill> pbars = Get_AG_Spills(runNumberE, {"Lifetime"},{0});
    if (pbars.empty()){
      std::cout<<runNumberE<<" has no intentional pbar losses\n";
      continue;
    }
    TAGPlot b(-1200,0);
    for (const TAGSpill& loss: pbars) {
      b.AddDumpGates({pbars});
      b.LoadData();
      b.FillHisto("6");
      a += b;
    }
  }
      std::string cs = "zElectrode_lifetimeR9738_4gauss_800bins_glassshoe_cut";
      cs += cut;
      std::string cspng(cs);
      cs += ".root";
      cspng += ".png";

      TCanvas* c2 = new TCanvas("cs","cs",1000,900);
      a.FillHisto(cut);
      TH1D* hZ = a.GetTH1D("zvtx");
      hZ->Draw();
      int mean  = hZ->GetMean();
      int stddev = hZ->GetStdDev();
      
      
      //TF1 *total = new TF1("mstotal",eightgaus,mean - 2*stddev, mean + 2*stddev+200,21);
      TF1 *total = new TF1("mstotal",fourgaus,mean - 2*stddev, mean + 2*stddev+200,11);
      //TF1 *total = new TF1("mstotal",twogaus, mean-2*stddev,mean+2*stddev+200,6);

      ////////////////////////////////////////////////////////////////////////////////////
      //1 peak, 400 bins
      // 2 gaussian parameters
      // total->SetParLimits(0,0.,40.);
      // total->SetParLimits(1,130.,300.);
      // total->SetParLimits(2,-645.,-500.);
      // total->SetParLimits(3,15.,25.);
      // total->SetParLimits(4,0.,60.);
      // total->SetParLimits(5,35.,100.);

      /////////////////////////////////////////////////////////////////////////////////////
      //2 peaks, 400 bins
      // // 4 gaussian parameters
      // total->SetParLimits(0,10.,40.);
      // total->SetParLimits(1,1000.,1100.);
      // total->SetParLimits(2,-750.,-600.);
      // total->SetParLimits(3,15.,25.);
      // total->SetParLimits(4,150.,400.);
      // total->SetParLimits(5,35.,100.);
      // total->SetParLimits(6,300.,700.);
      // total->SetParLimits(7,-550.,-400.);
      // total->SetParLimits(8,15.,25.);
      // total->SetParLimits(9,50.,200.);
      // total->SetParLimits(10,30.,100.);

      /////////////////////////////////////////////////////////////////////////////////////
      //2 peaks, 800 bins
      // // 4 gaussian parameters
      total->SetParLimits(0,10.,40.);
      total->SetParLimits(1,500.,550.);
      total->SetParLimits(2,-750.,-600.);
      total->SetParLimits(3,15.,25.);
      total->SetParLimits(4,150.,400.);
      total->SetParLimits(5,40.,100.);
      total->SetParLimits(6,250.,300.);
      total->SetParLimits(7,-550.,-400.);
      total->SetParLimits(8,15.,25.);
      total->SetParLimits(9,50.,200.);
      total->SetParLimits(10,30.,100.);
      
      /////////////////////////////////////////////////////////////////////////////////////
      //4 peaks, 400 bins
      // 8 gaussian parameters
      // total->SetParLimits(0,10.,20.);
      // total->SetParLimits(1,900.,1000.);
      // total->SetParLimits(2,-1000.,-800.);
      // total->SetParLimits(3,15.,25.);
      // total->SetParLimits(4,200.,400.);
      // //total->SetParLimits(5,-1000.,-800.);
      // total->SetParLimits(5,25.,100.);
      // total->SetParLimits(6,400.,450.);
      // total->SetParLimits(7,-750.,-600.);
      // total->SetParLimits(8,15.,25.);
      // total->SetParLimits(9,150.,250.);
      // //total->SetParLimits(11,-750.,-600.);
      // total->SetParLimits(10,35.,100.);
      // total->SetParLimits(11,300.,330.);
      // total->SetParLimits(12,-600.,-550.);
      // total->SetParLimits(13,15.,25.);
      // total->SetParLimits(14,100.,200.);
      // //total->SetParLimits(17,-600.,-550.);
      // total->SetParLimits(15,25.,100.);
      // total->SetParLimits(16,250.,300.);
      // total->SetParLimits(17,-550.,-400.);
      // total->SetParLimits(18,15.,25.);
      // total->SetParLimits(19,50.,75.);
      // //total->SetParLimits(23,-550.,-400.);
      // total->SetParLimits(20,30.,100.);
      // total->SetParNames ("E11 H",
      // 		"E11 Z",
      // 		"E11 d", 
      // 		"E11 H2", 
      // 		"E11 Z2", 
      // 		"E11 d2",
      // 		"E23 H", 
      // 		"E23 Z", 
      // 		"E23 d", 
      // 		"E23 H2", 
      // 		"E23 Z2",
      // 		"E23 d2",
      // 		"E29 H",
      // 		"E29 Z",
      // 		"E29 d",
      // 		"E29 H2",
      // 		"E29 Z2",
      // 		"E29 d2",
      // 		"E35 H",
      // 		"E35 Z",
      // 		"E35 d",
      // 		"E35 H2",
      // 		"E35 Z2",
      // 		"E35 d2");
      ////////////////////////////////////////////////////////////////////////////////////////

      /////////////////////////////////////////////////////////////////////////////////////
      //4 peaks, 400 bins
      // 8 gaussian parameters combined plots
      // total->SetParLimits(0,10.,40.);
      // total->SetParLimits(1,900.,1100.);
      // total->SetParLimits(2,-1000.,-800.);
      // total->SetParLimits(3,15.,25.);
      // total->SetParLimits(4,200.,400.);
      // //total->SetParLimits(5,-1000.,-800.);
      // total->SetParLimits(5,25.,100.);
      // total->SetParLimits(6,1400.,1600.);
      // total->SetParLimits(7,-750.,-600.);
      // total->SetParLimits(8,15.,25.);
      // total->SetParLimits(9,200.,600.);
      // //total->SetParLimits(11,-750.,-600.);
      // total->SetParLimits(10,35.,100.);
      // total->SetParLimits(11,300.,330.);
      // total->SetParLimits(12,-600.,-550.);
      // total->SetParLimits(13,15.,25.);
      // total->SetParLimits(14,100.,200.);
      // //total->SetParLimits(17,-600.,-550.);
      // total->SetParLimits(15,25.,100.);
      // total->SetParLimits(16,700.,800.);
      // total->SetParLimits(17,-550.,-400.);
      // total->SetParLimits(18,15.,25.);
      // total->SetParLimits(19,50.,200.);
      // //total->SetParLimits(23,-550.,-400.);
      // total->SetParLimits(20,30.,100.);

      ///////////////////////////////////////////////////////////////////////////////////////
      //single peak, 400 bins
      // total->SetParLimits(0,1.,2.); //1.433
      // total->SetParLimits(1,140.,160.); //140
      // total->SetParLimits(2,-580.,-582.); //-581.375
      // total->SetParLimits(3,25.,40.);//25
      // total->SetParLimits(4,10.,15.);//10
      // total->SetParLimits(5,-580.,-582.);//-581.375
      // total->SetParLimits(6,25.,100.);//81
      ///////////////////////////////////////////////////////////////////////////////////////


      hZ->Fit(total,"R");


      // //total->SetParLimits(0,2000.,3500.);
      // //total->SetParLimits(0,200.,400.);
      // total->SetParLimits(1,-900.,-770.);
      // total->SetParLimits(2,25.,40.);
      // //total->SetParLimits(3,1000.,2000.);
      // //total->SetParLimits(3,100.,200.);
      // total->SetParLimits(4,-770.,-611.);
      // total->SetParLimits(5,25.,40.);
      // //total->SetParLimits(6,800.,1000.);
      // //total->SetParLimits(6,0.,200.);
      // total->SetParLimits(7,-611.,-480.);
      // total->SetParLimits(8,25.,40.);
      // //total->SetParLimits(9,550.,700.);
      // //total->SetParLimits(9,0.,200.);
      // total->SetParLimits(10,-480.,-300.);
      // total->SetParLimits(11,25.,40.);
      // total->SetParLimits(12,0.,300.);

      //4 gaussian
       // total->SetParLimits(1,-770.,-611.);
       // total->SetParLimits(2,25.,40.);
       // total->SetParLimits(4,-480.,-300.);
       // total->SetParLimits(5,25.,40.);
       // total->SetParLimits(6,0.,300.);

      // 8 gaussian fit
      // TF1 *total = new TF1("mstotal","gaus(0)+gaus(3)+gaus(6)+gaus(9)+gaus(12)+gaus(15)+gaus(18)+gaus(21)",mean - 2*stddev, mean + 2*stddev);
      // 8 gaussian parameters
      // total->SetParLimits(0,2000.,3500.);
      // total->SetParLimits(1,-1000.,-770.);
      // total->SetParLimits(2,25.,40.);
      // total->SetParLimits(3,600.,700.);
      // total->SetParLimits(4,-1000.,-770.);
      // total->SetParLimits(5,25.,40.);
      // total->SetParLimits(6,800.,900.);
      // total->SetParLimits(7,-770.,-611.);
      // total->SetParLimits(8,25.,40.);
      // total->SetParLimits(9,550.,650.);
      // total->SetParLimits(10,-770.,-611.);
      // total->SetParLimits(11,25.,40.);
      // total->SetParLimits(12,450.,550.);
      // total->SetParLimits(13,-611.,-480.);
      // total->SetParLimits(14,25.,40.);
      // total->SetParLimits(15,450.,550.);
      // total->SetParLimits(16,-611.,-480.);
      // total->SetParLimits(17,25.,40.);
      // total->SetParLimits(18,300.,400.);
      // total->SetParLimits(19,-480.,-332.);
      // total->SetParLimits(20,25.,40.);
      // total->SetParLimits(21,300.,400.);
      // total->SetParLimits(22,-480.,-332.);
      // total->SetParLimits(23,25.,40.);


      //zfit << "E11" <<","<< total->GetParameter(1)<<","<< total->GetParError(1) << "," << total->GetParameter(2) << "," << total->GetParError(2) << "\n"<<"E23"<<","<<total->GetParameter(4)<<","<< total->GetParError(4) << "," << total->GetParameter(5) << "," << total->GetParError(5) << "\n"<<"E29"<<","<<total->GetParameter(7)<<","<< total->GetParError(7) << "," << total->GetParameter(8) << "," << total->GetParError(8) << "\n"<<"E35"<<","<<total->GetParameter(10)<<","<< total->GetParError(10) << "," << total->GetParameter(11) << "," << total->GetParError(11) << "\n";
      // zfit << "E23"<<","<<total->GetParameter(0)<<","<< total->GetParError(0) << "," << total->GetParameter(1) << "," << total->GetParError(1) << "\n"<<"E35"<<","<<total->GetParameter(3)<<","<< total->GetParError(3) << "," << total->GetParameter(4) << "," << total->GetParError(4) << "\n";
      hZ->Draw("same");
      //gStyle->SetOptFit(1000);
      c2->Update();
      c2->SaveAs(cs.c_str());
      c2->SaveAs(cspng.c_str());
 
    
  
  zfit.close();
}

