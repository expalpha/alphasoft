//Std Lib
#include <vector>
//Root stuff
#include "TChain.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TBranch.h"
#include "TFile.h"
#include "TSystem.h"
//Our stuff
#include "TA2Plot.h"
#include "TA2Plot_Filler.h"
//#include "../../agana/EventTracker.h"
#include "EventTracker.h"

int main(int argc, char* argv[])
{
    std::vector<int> runNumbers;
    std::string eventlist;
    std::string runlist;
    std::string location = ".";
    std::vector<std::string> args;
    enum ERunType
    {
        kMixing,
        kCosmic,
        kInvalidOption
    };
    ERunType runType = kInvalidOption;

    for (int i=0; i<argc; i++) 
    {
        args.push_back(argv[i]);
    }
    for (unsigned int i=1; i<args.size(); i++) // loop over the commandline options
    { 
        if(args[i] == "--mixing")
            runType = kMixing;
        if(args[i] == "--cosmic")
            runType = kCosmic;
        if(args[i] == "--eventlist")
            eventlist = args[i+1];
        if(args[i] == "--runlist")
            runlist = args[i+1];
        if(args[i] == "--location")
            location = args[i+1]; 
    }

    std::ifstream runFile(runlist);
    std::string runline;
    if (runFile.is_open())
    {
        while ( std::getline(runFile, runline) )
        {
            int runNumber;
            std::cout << runline << std::endl;
            std::sscanf(runline.c_str(), "%d", &runNumber);
            runNumbers.push_back(runNumber);
        }
        runFile.close();
    }

    ///====================================
    /// Populate a vector of event trackers
    ///====================================
    std::vector<EventTracker*> eventTrackers;
    /*std::ifstream eventFile(eventlist);
    std::string eventline;
    if(eventFile.fail()){
        std::cout << "WARNING: Eventlist failed to open. Likely doesn't exist but could be a permission error.\n";
        exit(-1);
    }
    if (eventFile.is_open())
    {
        while ( std::getline(eventFile, eventline) )
        {
            //std::cout << eventline << std::endl;
            int runNumber, firstEvent, lastEvent;
            //The following is apparantly best method: https://quick-bench.com/q/CWCbHcvWTZBXydPA_mju2r75LX0
            if (3 == std::sscanf(eventline.c_str(), "%d:%d-%d", &runNumber, &firstEvent, &lastEvent))
            {
                bool addTracker = true;
                for(EventTracker tracker: eventTrackers)
                {
                    if(runNumber == tracker.GetRunNumber())
                    {
                        addTracker = false;
                    }
                }
                if(addTracker)
                {
                    std::cout << "Adding tracker " << runNumber << " to list.\n";
                    EventTracker tracker = EventTracker(eventlist, runNumber);
                    eventTrackers.push_back(tracker);
                }
            }
        }
        eventFile.close();
    }*/
    /// This stuff above was all pretty dumb...
    for(int run: runNumbers)
    {
        //std::cout << "Adding run " << run << " to the eventtracker vector.\n";
        eventTrackers.push_back(new EventTracker(eventlist, run));
    }
    ///====================================
    /// Populate a vector of event trackers
    ///====================================





    std::cout << "Location chosen is " << location << "\n";


    //Create files and trees here
    TChain* chain = nullptr;
    switch(runType)
    {
        case kMixing:
            chain = new TChain("mixing");
            break;
        case kCosmic:
            chain = new TChain("cosmic");
            break;
        case kInvalidOption:
            std::cout << "Invalid option chosen" << std::endl;
            exit(1);
            break;
    }

    for(size_t i = 0; i<runNumbers.size(); i++)
    {
        std::string filename = location;
        filename += "/dumperoutput";
        filename += std::to_string(runNumbers[i]);
        filename += ".root";
        std::cout << filename << "\n";
        if(gSystem->AccessPathName(filename.c_str()))
        {
            std::cout << "Dumper file: " << filename << " does not exist.\n";
        }
        else
        {
            chain->Add(filename.c_str());
        }
    }

    const int nEntries = chain->GetEntries();

    std::cout << "Num ENTRIES = " << nEntries << "\n";

    TFile *outFile = nullptr;
    TTree *trainTree, *validTree, *testTree;
    trainTree = validTree = testTree = nullptr;
    switch(runType)
    {
        case(kMixing):
            outFile = new TFile("mvaSignal.root", "RECREATE");
            trainTree = chain->CloneTree(0);
            trainTree->SetName("trainSignal");
            validTree = chain->CloneTree(0);
            validTree->SetName("validationSignal");
            testTree = chain->CloneTree(0);
            testTree->SetName("testSignal");
            break;
        case(kCosmic):
            outFile = new TFile("mvaBackground.root", "RECREATE");
            trainTree = chain->CloneTree(0);
            trainTree->SetName("trainBackground");
            validTree = chain->CloneTree(0);
            validTree->SetName("validationBackground");
            testTree = chain->CloneTree(0);
            testTree->SetName("testBackground");
            break;
        case(kInvalidOption):
            outFile = nullptr;
            trainTree = validTree = testTree = nullptr;
            break;
    }
    //std::cout << "DO WE GET HERE?????" << std::endl;

    if( !trainTree || !validTree || !testTree || !outFile )
    {
        std::cerr << "Filter: Null pointers to TTrees or to TFile because of invalid option" << std::endl;
        return EXIT_FAILURE;
    }

    TLeaf* runNum;
    TLeaf* IDNum; 
    int k=0;
    for (int i = 1; i <= nEntries; i++) 
    {
        //std::cout << "i = " << i << ".\n";
        chain->GetEntry(i);
        runNum = chain->GetLeaf("RunNumber", "RunNumber");
        IDNum  = chain->GetLeaf("EventID", "RunNumber");
        //std::cout << "DO WE GET HERE?????" << std::endl;
        bool dump = false;
        for(EventTracker* tracker: eventTrackers)
        {
            //std::cout << "Does" << int(runNum->GetValue()) << " = " << tracker->GetRunNumber() <<"?\n";
            //std::cout << "EventID = " << IDNum->GetValue() << "\n";
            if(int(runNum->GetValue()) == tracker->GetRunNumber())
            {
                //std::cout <<"Yes. Now we check range.\n";
                int idnum = int(IDNum->GetValue()) ;
                dump = tracker->IsEventInRange(idnum, 0.);
                //std::cout <<"Is it in range? Computer says: " << dump << "\n";
                break;
            }
        }
        if(dump)
        {
            if (k%3 == 0)
            {
                //std::cout << "k = " << k << ". Filling train tree.\n";
                trainTree->Fill();
            }
            if (k%3 == 1)
            {
                //std::cout << "k = " << k << ". Filling valid tree.\n";
                validTree->Fill();
            }
            if (k%3 == 2)
            {
                //std::cout << "k = " << k << ". Filling test tree.\n";
                testTree->Fill();
            }
            k++;
        }
    }

    std::cout << "Final number of events dumper = " << k << " filtered down from " << nEntries << " events \n";

    outFile->cd();
    outFile->Write();
    outFile->Close();

    return EXIT_SUCCESS;
}