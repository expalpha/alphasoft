#ifndef _TBARMVADUMPER_
#define _TBARMVADUMPER_

#include "TMVADumper.h"
#include "TStoreEvent.hh"
#include "TBarEvent.hh"
#include "TBarMVAVars.h"
#include "TMVA/Reader.h"
#include <numeric>
#include <cmath>
#include "CosmicFinder.hh"
#include "TPCconstants.hh"

#include "generalizedspher.h"

/// @brief Base class for TAGMVADumper
class TAGMVADumper: public TMVADumper
{
   public:
   /// @brief Default constructor. Creates branches for each variable names in the dumper class.
   /// @param tree TTree to dump the data to.
   TAGMVADumper(TTree* tree): TMVADumper(tree)
   {
       
   }
   
   /// @brief Update the variables before dumping to root file.
   /// @param storeEvent TStoreEvent for obtaining the event information.
   /// @param currentEventNumber EventNumber to know whether to dump or not.
   /// @return Success or not.
   virtual bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber) = 0;

   /// @brief Print the MVA event before dumping.
   void PrintEvent()
   {
      std::cout << "Print not overwritten for this event type. \n";
   }

};


/// @brief Basic dumper that writes out the Event ID number and Run Number 
class TAGMVAEventIDDumper: public TAGMVADumper
{
   public:
      int fRunNumber; ///> EventRunNumber
      int fEventID; ///>EventID

   /// <inheritdoc />
   TAGMVAEventIDDumper(TTree* tree): TAGMVADumper(tree)
   {
      fTree->Branch("RunNumber", &fRunNumber, "RunNumber/I");
      fTree->Branch("EventID", &fEventID, "EventID/I");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("RunNumber", &fRunNumber);      
      reader->AddVariable("EventID", &fEventID);
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      // Safely handle case where we have the wrong Event ID
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }
      // Update class members
      fEventID = storeEvent->GetEventNumber();
      fRunNumber = storeEvent->GetRunNumber();
      // Report success!
      return true;
   }
};


/// @brief Copy this as a template for your own implementation
class TAGMVATestDumper: public TAGMVADumper
{
   public:   
      float X;

   /// <inheritdoc />
   TAGMVATestDumper(TTree* tree): TAGMVADumper(tree)
   {
      fTree->Branch("X", &X, "X/F");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("X", &X);      
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;

      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         //std::cout<<currentEventNumber <<" != "<< siliconEvent->GetVF48NEvent() <<std::endl;
         return false;
      }
      //std::cout<<"\t"<<currentEventNumber <<" == "<< siliconEvent->GetVF48NEvent()<< "  :)" <<std::endl;
      this->X=storeEvent->GetVertexX();
      return true;
   }

   /// <inheritdoc />
   void PrintEvent()
   {
      std::cout << "==============================================================" << std::endl;
      std::cout << "=============== Printing an MVA event ================" << std::endl;
      std::cout << "=============== Not Implemented ================" << std::endl;
      std::cout << "================================================================" << std::endl;
   }
};


/// @brief Classic dumper, takes basic event information.
class TAGMVAClassicDumper: public TAGMVADumper
{
   public:   
      float X, Y, Z;
      float vStatus;
      float barMultiplicity;
      float pointsPerTrack;
      float meanRSigma;
      float meanZSigma;
      float numTracks;
      float numPoints;

   /// <inheritdoc />
   TAGMVAClassicDumper(TTree* tree): TAGMVADumper(tree)
   {
      fTree->Branch("X", &X, "X/F");
      fTree->Branch("Y", &Y, "Y/F");
      fTree->Branch("Z", &Z, "Z/F");
      fTree->Branch("vertexStatus", &vStatus, "vStatus/F");
      fTree->Branch("barMultiplicity", &barMultiplicity, "barMultiplicity/F");
      fTree->Branch("pointsPerTrack", &pointsPerTrack, "pointsPerTrack/F");
      fTree->Branch("meanRSigma", &meanRSigma, "meanRSigma/F");
      fTree->Branch("meanZSigma", &meanZSigma, "meanZSigma/F");
      fTree->Branch("numTracks", &numTracks, "numTracks/F");
      fTree->Branch("numPoints", &numPoints, "numPoints/F");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("X", &X); 
      reader->AddVariable("Y", &Y);      
      reader->AddVariable("Z", &Z);      
      reader->AddVariable("vertexStatus", &vStatus);      
      reader->AddVariable("barMultiplicity", &barMultiplicity);      
      reader->AddVariable("pointsPerTrack", &pointsPerTrack);      
      reader->AddVariable("meanRSigma", &meanRSigma);      
      reader->AddVariable("meanZSigma", &meanZSigma);      
      reader->AddVariable("numTracks", &numTracks);      
      reader->AddVariable("numPoints", &numPoints);      
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }
      this->X=storeEvent->GetVertexX();
      this->Y=storeEvent->GetVertexY();
      this->Z=storeEvent->GetVertexZ();
      this->vStatus=storeEvent->GetVertexStatus();
      this->barMultiplicity=storeEvent->GetBarMultiplicity();
      this->pointsPerTrack=storeEvent->GetNumberOfPointsPerTrack();
      this->meanRSigma=storeEvent->GetMeanRSigma();
      this->meanZSigma=storeEvent->GetMeanZSigma();
      this->numTracks=storeEvent->GetNumberOfTracks();
      this->numPoints=storeEvent->GetNumberOfPoints();
      return true;
   }
};


/// @brief Classic BV dumper, dumps basic BV information available in the TBarEvent.
class TAGMVAClassicBVDumper: public TAGMVADumper
{
   public:   
      float fNumBars, fNumEnds, fNumTDC;
      float fNumADC, fNumBarsComp, fNumBarsHComp, fNumMBars, fNumMBarsComp, fNumMBarsHComp;
      float fNumClusters;
      float fNumMatchedCompleteBarClusters, fNumHalfMatchedCompleteBarClusters;

   /// <inheritdoc />
   TAGMVAClassicBVDumper(TTree* tree): TAGMVADumper(tree)
   {
      fTree->Branch("numBars", &fNumBars, "numBars/F");
      fTree->Branch("numEnds", &fNumEnds, "numEnds/F");
      fTree->Branch("numTDC", &fNumTDC, "numTDC/F");
      fTree->Branch("fNumADC", &fNumADC, "fNumADC/F");

      fTree->Branch("fNumBarsComp", &fNumBarsComp, "fNumBarsComp/F");
      fTree->Branch("fNumBarsHComp", &fNumBarsHComp, "fNumBarsHComp/F");
      fTree->Branch("fNumMBars", &fNumMBars, "fNumMBars/F");
      fTree->Branch("fNumMBarsComp", &fNumMBarsComp, "fNumMBarsComp/F");
      fTree->Branch("fNumMBarsHComp", &fNumMBarsHComp, "fNumMBarsHComp/F");

      fTree->Branch("fNumClusters", &fNumClusters, "fNumClusters/F");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("numBars", &fNumBars); 
      reader->AddVariable("numEnds", &fNumEnds); 
      reader->AddVariable("numTDC", &fNumTDC);
      reader->AddVariable("fNumADC", &fNumADC);

      reader->AddVariable("fNumBarsComp", &fNumBarsComp);
      reader->AddVariable("fNumBarsHComp", &fNumBarsHComp);
      reader->AddVariable("fNumMBars", &fNumMBars);
      reader->AddVariable("fNumMBarsComp", &fNumMBarsComp);
      reader->AddVariable("fNumMBarsHComp", &fNumMBarsHComp);

      reader->AddVariable("fNumClusters", &fNumClusters);
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }
      const TBarEvent* barEvent = storeEvent->GetBarEvent();

      this->fNumBars = barEvent->GetNumBars();
      this->fNumEnds = barEvent->GetNumEnds();
      this->fNumADC = barEvent->GetNumADC();
      this->fNumTDC = barEvent->GetNumTDC();

      this->fNumBarsComp = barEvent->GetNumBarsComplete();
      this->fNumBarsHComp = barEvent->GetNumBarsHalfComplete();
      this->fNumMBars= barEvent->GetNumMatchedBars();
      this->fNumMBarsComp = barEvent->GetNumMatchedBarsComplete();
      this->fNumMBarsHComp = barEvent->GetNumMatchedBarsHalfComplete();

      this->fNumClusters = barEvent->GetNumClusters();

      
      //PrintEvent();

      return true;
   }
   
   /// <inheritdoc />
   void PrintEvent()
   {
      std::cout << "==============================================================" << std::endl;
      std::cout << "===============Printing a BV numhits MVA event================" << std::endl;
      std::cout << "numBars = "                                      << fNumBars << "\n"; 
      std::cout << "numEnds = "                                      << fNumEnds << "\n"; 
      std::cout << "numTDC = "                                       << fNumTDC << "\n";
      std::cout << "fNumADC = "                                      << fNumADC << "\n";
      std::cout << "fNumBarsComp = "                                 << fNumBarsComp << "\n";
      std::cout << "fNumBarsHComp = "                                << fNumBarsHComp << "\n";
      std::cout << "fNumMBars = "                                    << fNumMBars << "\n";
      std::cout << "fNumMBarsComp = "                                << fNumMBarsComp << "\n";
      std::cout << "fNumMBarsHComp = "                               << fNumMBarsHComp << "\n";
      std::cout << "fNumClusters = "                                 << fNumClusters << "\n";
      std::cout << "================================================================" << std::endl;
   }
};


/// @brief A2-style High-level dumper, calculated many of the high level vars used in A2 and dumps them.
class TAGMVAHLParameterDumper: public TAGMVADumper
{
   public:   
      float nhits,totalresidual,r;
      float S0rawPerp,S0axisrawZ,phi_S0axisraw,nCT,nGT,tracksdca;
      float curvemin,curvemean,lambdamin,lambdamean,curvesign,phi;
      float meantotalresidual, nsphits, nwirehits, npadhits; //Lukas additions.
      float usedresidual, meanusedresidual;
   
   /// <inheritdoc />
   TAGMVAHLParameterDumper(TTree* tree): TAGMVADumper(tree)
   {
      fTree->Branch("nhits", &nhits, "nhits/F");                              //	1. Total num channels registering hits
      fTree->Branch("totalresidual", &totalresidual, "totalresidual/F");      //done //	2. Sum of squared residual distances (to respective track/helix)
      fTree->Branch("r", &r, "r/F");                                             //done //	3. radial r position of vertex
      fTree->Branch("S0rawPerp", &S0rawPerp, "S0rawPerp/F");                  //	4. Sphericity variable (how evenly spaced are the tracks)
      fTree->Branch("S0axisrawZ", &S0axisrawZ, "S0axisrawZ/F");               //	5. Cosine of the angle between event axis and detector axis
      fTree->Branch("phi_S0axisraw", &phi_S0axisraw, "phi_S0axisraw/F");      //	6. Angle between event axis and vertical direction in x-y
      fTree->Branch("nCT", &nCT, "nCT/F");                                    //	7. Number of recon tracks
      fTree->Branch("nGT", &nGT, "nGT/F");                                    //	8. Number of 3 hit combos used as track candidates
      fTree->Branch("tracksdca", &tracksdca, "tracksdca/F");                  //	9. The distance of closest approach of tracks
      fTree->Branch("curvemin", &curvemin, "curvemin/F");                     //	10. Min of track radius
      fTree->Branch("curvemean", &curvemean, "curvemean/F");                  //	11. Mean of track radius
      fTree->Branch("lambdamin", &lambdamin, "lambdamin/F");                  //	12. Min pitch
      fTree->Branch("lambdamean", &lambdamean, "lambdamean/F");               //	13. Mean pitch
      fTree->Branch("curvesign", &curvesign, "curvesign/F");                  // 14. Integer sum of sense of curvature
      fTree->Branch("phi", &phi, "phi/F");                                       //done // 15. radial phi position of vertex
      fTree->Branch("meantotalresidual", &meantotalresidual, "meantotalresidual/F");                                       //done // 15. radial phi position of vertex
      fTree->Branch("usedresidual", &usedresidual, "usedresidual/F");                                       //done // 15. radial phi position of vertex
      fTree->Branch("meanusedresidual", &meanusedresidual, "meanusedresidual/F");                                       //done // 15. radial phi position of vertex
      fTree->Branch("nsphits", &nsphits, "nsphits/F");                                       //done // 15. radial phi position of vertex
      fTree->Branch("nwirehits", &nwirehits, "nwirehits/F");                                       //done // 15. radial phi position of vertex
      fTree->Branch("npadhits", &npadhits, "npadhits/F");                                       //done // 15. radial phi position of vertex
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("nhits", &nhits);      
      reader->AddVariable("totalresidual", &totalresidual);
      reader->AddVariable("r", &r);
      reader->AddVariable("S0rawPerp", &S0rawPerp);
      reader->AddVariable("S0axisrawZ", &S0axisrawZ);
      reader->AddVariable("phi_S0axisraw", &phi_S0axisraw);
      reader->AddVariable("nCT", &nCT);
      reader->AddVariable("nGT", &nGT);
      reader->AddVariable("tracksdca", &tracksdca);
      reader->AddVariable("curvemin", &curvemin);
      reader->AddVariable("curvemean", &curvemean);
      reader->AddVariable("lambdamin", &lambdamin);
      reader->AddVariable("lambdamean", &lambdamean);
      reader->AddVariable("curvesign", &curvesign);
      reader->AddVariable("phi", &phi);    
      reader->AddVariable("meantotalresidual", &meantotalresidual);    
      reader->AddVariable("usedresidual", &usedresidual);    
      reader->AddVariable("meanusedresidual", &meanusedresidual);    
      reader->AddVariable("nsphits", &nsphits);    
      reader->AddVariable("nwirehits", &nwirehits);    
      reader->AddVariable("npadhits", &npadhits);    
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }


      //Either save all three or pick one?
      this->nsphits=storeEvent->GetNumSPHits();   
      this->npadhits=storeEvent->GetNumPADHits(); 
      this->nwirehits=storeEvent->GetNumAWHits(); 
      this->nhits=storeEvent->GetNumberOfPoints();



      const TVector3 vtx = storeEvent->GetVertex();
      //std::cout << "Vertex status = " << storeEvent->GetVertexStatus() << "\n";
      //std::cout << "Vertex [x,y,z] = [" << vtx.X() << "," << vtx.Y() << "," << vtx.Z() << "] \n";
      if(storeEvent->GetVertexStatus()>0)
      {
         this->r = vtx.Perp();
         this->phi = vtx.Phi();
      }
      else
      {
         this->r = ALPHAg::kLargeNegativeUnknown;
         this->phi = ALPHAg::kLargeNegativeUnknown;

      }




      ////Do I want GetUsedHelices() or fStoreHelixArray?? i  assume GetUsedHelices() for now
      //Perhaps we only want to use helices with a certain status? For now I will all helices here.
      Int_t nAT =  storeEvent->GetHelixArray()->GetEntries(); // all tracks
      this->nCT = 0;
      Int_t nraw = 0;  

      std::vector<double> velxraw;
      std::vector<double> velyraw;
      std::vector<double> velzraw;  
      for (int i = 0; i< nAT ; ++i)
      {
         TStoreHelix* helix = (TStoreHelix*)storeEvent->GetHelixArray()->At(i);
         if (!helix) continue;
         //Double_t fc = helix->GetC();
         Double_t fphi0 = helix->GetPhi0();
         Double_t fLambda = helix->GetLambda();
         //Double_t s=0.; // calculates velx,y,z at POCA
         // special case for s = 0
         // Int_t HelixHits=helix->GetNumberOfPoints(); //Unused


         // This seems to be true all the time... please check - Joe... Has this been checked for A2? Maybe I should check for AG too.
         //if ( helix->GetStatus()==1 ) - Not sure if we want this or the line below??
         //std::cout << "Helix status in loop for RAW values = " << helix->GetStatus() << std::endl;
         //if ( helix->GetStatus()==1 || helix->GetStatus()==2 || helix->GetStatus()==3 )
         if ( helix->GetStatus()>=0 )
         {
            ++nraw; // == ntracks
            velxraw.push_back( - TMath::Sin(fphi0));
            velyraw.push_back( TMath::Cos(fphi0)) ;
            velzraw.push_back( fLambda );
            this->nCT++;
         }
      }


      //std::cout << "Trivial helix has " << storeEvent->GetTrivialHelix().GetNumberOfPoints() << " points. The TStoreEvent has " << storeEvent->GetNumberOfPoints() << ". \n";
      //Only use the trivial line if there were more than 2 space points (and therefore ideally a reasonable line)
      if(storeEvent->GetNumberOfPoints() >= 2)
         this->totalresidual = storeEvent->GetTrivialHelix().CalculateResiduals();
      else //Otherwise just use default value. 
         this->totalresidual = ALPHAg::kLargeNegativeUnknown;

      this->meantotalresidual = 0;
      size_t numResiduals = storeEvent->GetTrivialHelix().GetResidualsVector().size();
      for (size_t i = 0; i<  numResiduals; ++i)
      {
         //Also add to a mean residual member
         this->meantotalresidual+=storeEvent->GetTrivialHelix().GetResidualsVector().at(i);
      }
      this->meantotalresidual = numResiduals>0?this->meantotalresidual/numResiduals:ALPHAg::kLargeNegativeUnknown;
      
      //std::cout << "Used trivial helix has " << storeEvent->GetUsedTrivialHelix().GetNumberOfPoints() << " points. The TStoreEvent has " << storeEvent->GetNumberOfPoints() << ". \n";
      //Only use the trivial line if there were more than 2 space points (and therefore ideally a reasonable line)
      if(storeEvent->GetNumberOfPoints() >= 2)
         this->usedresidual = storeEvent->GetUsedTrivialHelix().CalculateResiduals();
      else //Otherwise just use default value. 
         this->usedresidual = ALPHAg::kLargeNegativeUnknown;

      this->meanusedresidual = 0;
      numResiduals = storeEvent->GetUsedTrivialHelix().GetResidualsVector().size();
      for (size_t i = 0; i<  numResiduals; ++i)
      {
         //Also add to a mean residual member
         this->meanusedresidual+=storeEvent->GetUsedTrivialHelix().GetResidualsVector().at(i);
      }
      this->meanusedresidual = numResiduals>0?this->meanusedresidual/numResiduals:ALPHAg::kLargeNegativeUnknown;


      //std::cout << "\n\n Our residual = " << this->residual << " \n";
      //std::cout << "Redidual for all other tracks = [";
      /*for (int i = 0; i< nAT ; ++i)
      {
         TStoreHelix* helix = (TStoreHelix*)storeEvent->GetHelixArray()->At(i);
         if (!helix) continue;
         std::cout << helix->GetResidualsSquared() << ",";
      }
      std::cout << "] \n"; */


      //Set tracks DCA (distance of closest approach)
      this->tracksdca = storeEvent->GetDCA();


      std::vector<double> velx;
      std::vector<double> vely;
      std::vector<double> velz;
      // alpha event part

      ////Do I want GetUsedHelices() or fStoreHelixArray?? i  assume GetUsedHelices() for now
      //Perhaps we only want to use helices with a certain status? For now I will all helices here.
      Int_t nGTL =  storeEvent->GetUsedHelices()->GetEntries(); // all (used) tracks

      this->nGT = 0;
      //If there are no tracks, these numbers will not get updated. As such we want to set them as impossible value so the 
      //MVA can differentiate between these and real events.
      if(nGTL<1)
      {
         this->curvemin=ALPHAg::kLargeNegativeUnknown;
         this->curvemean=ALPHAg::kLargeNegativeUnknown;
         this->lambdamin=ALPHAg::kLargeNegativeUnknown;
         this->lambdamean=ALPHAg::kLargeNegativeUnknown;
         this->curvesign=ALPHAg::kLargeNegativeUnknown;
      }
      else //Otherwise set them as the default to use for the calculation below. Maybe we can make these not be separate with a change to the logic below but for now this is fine. 
      {
         this->curvemin=ALPHAg::kLargePositiveUnknown;
         this->curvemean=0.;
         this->lambdamin=ALPHAg::kLargePositiveUnknown;
         this->lambdamean=0;
         this->curvesign=0;
      }
      for (int i = 0; i< nGTL ; ++i)
      {
         TStoreHelix* helix = (TStoreHelix*)storeEvent->GetUsedHelices()->At(i);
         //if(aehlx->GetHelixStatus()<0) continue;
         Double_t fc = helix->GetC();
         Double_t fphi0 = helix->GetPhi0();
         Double_t fLambda = helix->GetLambda();

         //Unused
         //Double_t s=0.; // calculates velx,y,z at POCA
         // special case for s = 0
         velx.push_back( - TMath::Sin(fphi0) );
         vely.push_back( TMath::Cos(fphi0) ) ;
         velz.push_back( fLambda );

         //std::cout << "Helix status in loop for CORRECTED values = " << helix->GetStatus() << std::endl;
         // select good helices, after removal of duplicates
         //if (helix->GetStatus()==1) this or line below? 
         if (helix->GetStatus()==2 || helix->GetStatus()==3) 
         //Here we are doing some checking of the helix?? Perhaps this should be done above also.
         {
            this->nGT++;
            this->curvemin= fabs(fc)>this->curvemin? this->curvemin:fabs(fc);
            this->lambdamin= fabs(fLambda)>this->lambdamin? this->lambdamin:fabs(fLambda);
            this->curvemean+=fabs(fc);
            this->lambdamean+=fabs(fLambda);
            this->curvesign+=(fc>0)?1:-1;
         }
      }
      if(this->nGT>0){
         this->lambdamean/=this->nGT;
         this->curvemean/=this->nGT;
      }



      Double_t S0axisrawX = ALPHAg::kLargeNegativeUnknown;
      //Unused in online_mva
      Double_t S0axisrawY = ALPHAg::kLargeNegativeUnknown;
      this->S0axisrawZ = ALPHAg::kLargeNegativeUnknown;

      if(nraw>0) //If we have any raw tracks at all
      {
         TVector3* S0axisraw;
         TVector3* S0valuesraw;

         /* std::cout << "Sphericity for the HL dumper: x = [";
         for(auto entry: velxraw)
         {
            std::cout << entry << "," ;
         } 
         std::cout << "]"; 
         std::cout << ", y = ["; 
         for(auto entry: velyraw)
         {
            std::cout << entry << "," ;
         } 
         std::cout << "]"; 
         std::cout << ", z = ["; 
         for(auto entry: velzraw)
         {
            std::cout << entry << "," ;
         } 
         std::cout << "]" << std::endl;  */
         
         
         sphericity(velxraw, velyraw, velzraw, 0, &S0axisraw, &S0valuesraw); // generalizedspher.h
         this->S0rawPerp = S0valuesraw->Perp(); //R value of perp vector

         //Unused in online_mva
         S0axisrawX = S0axisraw->X();
         //Unused
         S0axisrawY = S0axisraw->Y();
         this->S0axisrawZ = S0axisraw->Z();
         this->phi_S0axisraw = TMath::ACos(S0axisrawY/TMath::Sqrt(S0axisrawX*S0axisrawX+S0axisrawY*S0axisrawY));
         delete S0axisraw;
         delete S0valuesraw;
      }
      else //If no raw tracks we need to set them to large unknown negative.
      {
         this->S0rawPerp = ALPHAg::kLargeNegativeUnknown;
         this->phi_S0axisraw = ALPHAg::kLargeNegativeUnknown;

      }
    
      //printf("\n\n nAT = %i \n nCT = %f \n nGTL = %i \n nGT = %f \n allHels = %i \n usedHels = %i \n\n", nAT, this->nCT, nGTL, this->nGT, storeEvent->GetHelixArray()->GetEntries(), storeEvent->GetUsedHelices()->GetEntries());
      
      //if(fTrace)
      //PrintEvent();
      
      return true;
   }

   /// <inheritdoc />
   void PrintEvent()
   {
      std::cout << "=============================================================" << std::endl;
      std::cout << "===============Printing a storeEvent MVA event===============" << std::endl;
      std::cout << "nhits = " << nhits << std::endl;      
      std::cout << "totalresidual = " << totalresidual << std::endl;
      std::cout << "meantotalresidual = " << meantotalresidual << std::endl;
      std::cout << "r = " << r << std::endl;
      std::cout << "phi = " << phi << std::endl;   
      std::cout << "S0rawPerp = " << S0rawPerp << std::endl;
      std::cout << "S0axisrawZ = " << S0axisrawZ << std::endl;
      std::cout << "phi_S0axisraw = " << phi_S0axisraw << std::endl;
      std::cout << "nCT = " << nCT << std::endl;
      std::cout << "nGT = " << nGT << std::endl;
      std::cout << "tracksdca = " << tracksdca << std::endl;
      std::cout << "curvemin = " << curvemin << std::endl;
      std::cout << "curvemean = " << curvemean << std::endl;
      std::cout << "lambdamin = " << lambdamin << std::endl;
      std::cout << "lambdamean = " << lambdamean << std::endl;
      std::cout << "nsphits = " << nsphits << std::endl;
      std::cout << "nwirehits = " << nwirehits << std::endl;
      std::cout << "npadhits = " << npadhits << std::endl;
      std::cout << "==============================================================" << std::endl;
   }
};


/// @brief Dumper solely for residual. Initiates a CosmicFinder() to calculate residual.
class TAGMVAResidualDumper: public TAGMVADumper
{
   public:   
      float residual;
      double fMagneticField=1;

   /// <inheritdoc />
   TAGMVAResidualDumper(TTree* tree, double field): TAGMVADumper(tree)
   {
      fTree->Branch("residual", &residual, "residual/F");
      fMagneticField = field;
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("residual", &residual);      
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }
      this->residual=storeEvent->GetVertexX();

      //Now lets actually calculate the residual using a CosmicFinder:
      CosmicFinder finder(fMagneticField, &TAMultithreadHelper::gfLock);

      // Setup Cosmic Analysis 
      std::vector<TCosmic> cosmics;         

      //cosfind.Create(&helices,cosmics);
      finder.Create(storeEvent,cosmics);

      // Perform Cosmic Analysis
      /*int cf_status = */finder.Process(cosmics); //Don't use the cf_status currently but it will give you error status if needed.

      //Print results 
      //std::cout<<"CosmicFinder Status: "<<cf_status<<std::endl;
      finder.Status();
      //std::cout << "Residual found for event = " << finder.GetResidual() << "\n";

      //Set residual
      if(finder.GetStatus()==0)
         this->residual=finder.GetResidual();
      else
         this->residual=ALPHAg::kLargeNegativeUnknown;

      
      //Reset finder (gets deleted anyway)
      finder.Reset();




      /*printf("triv line: %f, triv helix: %f, used line: %f,  used helix: %f \n",
                        storeEvent->GetTrivialLine().GetResidualsSquared(),
                        storeEvent->GetTrivialHelix().GetResidualsSquared(),
                        storeEvent->GetUsedTrivialLine().GetResidualsSquared(),
                        storeEvent->GetUsedTrivialHelix().GetResidualsSquared()
                         );*/






      return true;
   }
};


/// @brief Dumper for all BV variables for Gareth's 2024 BV analysis
class TAGMVAGarethsTOFDumper: public TAGMVADumper
{
   public:
   std::array<float,NUM_BV_VARS> vars;
   const std::vector<std::string> var_names = TBarMVAVars::GetVarNames();
   
   /// <inheritdoc />
   TAGMVAGarethsTOFDumper(TTree* tree): TAGMVADumper(tree)
   {
      for (int i=0; i<NUM_BV_VARS; i++) {
         if (!TBarMVAVars::IsVarUsed(var_names[i])) continue;
         fTree->Branch(var_names[i].c_str(), &vars[i], Form("%s/F",var_names[i].c_str()));
      }
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      for (int i=0; i<NUM_BV_VARS; i++) {
         if (!TBarMVAVars::IsVarUsed(var_names[i])) continue;
         reader->AddVariable(var_names[i].c_str(),&vars[i]);
      }
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      // Safely handle case where we have the wrong Event ID
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }
      TBarEvent* myBarEvent = new TBarEvent(*storeEvent->GetBarEvent());
      if(!myBarEvent)
      {
         return false;
      }

      TBarMVAVars mva_vars(*myBarEvent);
      mva_vars.CalculateVars();
      mva_vars.EnforceValueRange(20);

      for (int i=0; i<NUM_BV_VARS; i++) {
         vars[i] = (float)mva_vars.GetVar(i);
      }

      // Report success!
      return true;
   }
};


/// @brief AnodeWire hits dumper
class TAGMVAAnodeWireHitsDumper: public TAGMVADumper
{
   public:   
      std::vector<int>* fAnodeWireNHits;
      const int AW_COLUMNS = int(ALPHAg::_anodescol);
      const int AW_ROWS = int(ALPHAg::_anodesrow);

   /// <inheritdoc />
   TAGMVAAnodeWireHitsDumper(TTree* tree): TAGMVADumper(tree)
   {
      fAnodeWireNHits = new std::vector<int>(AW_ROWS*AW_COLUMNS, 0.);
      fTree->Branch("fAnodeWireNHits", "std::vector<int>", &fAnodeWireNHits);
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      char typefloat = 'I';
      reader->DataInfo().AddVariablesArray("fAnodeWireNHits", AW_ROWS*AW_COLUMNS, "fAnodeWireNHits", "nHits", -999., 999., typefloat, false, &fAnodeWireNHits);
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      AgRawSignalsFlow* agrsf=flow->Find<AgRawSignalsFlow>();
      if (!agfe || !agrsf)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if (!storeEvent)
      {
         return false;
      }
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         std::cout<<currentEventNumber <<" != "<< storeEvent->GetEventNumber() <<std::endl;
         return false;
      }

      for(int i=0; i<AW_COLUMNS; i++)
      {
         for(int j=0; j<AW_ROWS; j++)
         {
            fAnodeWireNHits->at(i*AW_ROWS+j) = agrsf->GetAnodeWireNHits(i,j);
         }
      }
      return true;
   }
};


/// @brief AnodeWire signal dumper.
class TAGMVAAnodeWireHeightDumper: public TAGMVADumper
{
   public:   
      std::vector<float>* fAnodeWireWHeight;
      const int AW_COLUMNS = int(ALPHAg::_anodescol);
      const int AW_ROWS = int(ALPHAg::_anodesrow);

   /// <inheritdoc />
   TAGMVAAnodeWireHeightDumper(TTree* tree): TAGMVADumper(tree)
   {
      fAnodeWireWHeight = new std::vector<float>(AW_ROWS*AW_COLUMNS, 0.);
      fTree->Branch("fAnodeWireWHeight", "std::vector<float>", &fAnodeWireWHeight);
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      char typefloat = 'F';
      reader->DataInfo().AddVariablesArray("fAnodeWireWHeight", AW_ROWS*AW_COLUMNS, "fAnodeWireWHeight", "nHits", -999., 999., typefloat, false, &fAnodeWireWHeight);
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      AgRawSignalsFlow* agrsf=flow->Find<AgRawSignalsFlow>();
      if (!agfe || !agrsf)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if (!storeEvent)
      {
         return false;
      }
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         std::cout<<currentEventNumber <<" != "<< storeEvent->GetEventNumber() <<std::endl;
         return false;
      }

      for(int i=0; i<AW_COLUMNS; i++)
      {
         for(int j=0; j<AW_ROWS; j++)
         {
            fAnodeWireWHeight->at(i*AW_ROWS+j) = agrsf->GetAnodeWireWHeight(i,j);
         }
      }
      return true;
   }
};

/// @brief Pad signal dumper.
class TAGMVAPadHitsDumper: public TAGMVADumper
{
   public:   
      std::vector<int>* fPadNHits;
      const int PAD_ROWS = int(ALPHAg::_padrow);
      const int PAD_COLUMNS = int(ALPHAg::_padcol);

   /// <inheritdoc />
   TAGMVAPadHitsDumper(TTree* tree): TAGMVADumper(tree)
   {
      fPadNHits = new std::vector<int>(PAD_ROWS*PAD_COLUMNS, 0.);
      fTree->Branch("fPadNHits", "std::vector<int>", &fPadNHits);
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      char typefloat = 'I';
      reader->DataInfo().AddVariablesArray("fPadNHits", PAD_ROWS*PAD_COLUMNS, "fPadNHits", "nHits", -999., 999., typefloat, false, &fPadNHits);
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      AgRawSignalsFlow* agrsf=flow->Find<AgRawSignalsFlow>();
      if (!agfe || !agrsf)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if (!storeEvent)
      {
         return false;
      }
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         std::cout<<currentEventNumber <<" != "<< storeEvent->GetEventNumber() <<std::endl;
         return false;
      }

      for(int i=0; i<PAD_COLUMNS; i++)
      {
         for(int j=0; j<PAD_ROWS; j++)
         {
            fPadNHits->at(i*PAD_ROWS+j) = agrsf->GetPadNHits(i,j);
         }
      }
      return true;
   }
};


/// @brief Pad signal dumper.
class TAGMVAPadHeightDumper: public TAGMVADumper
{
   public:   
      std::vector<float>* fPadWHeight;
      const int PAD_ROWS = int(ALPHAg::_padrow);
      const int PAD_COLUMNS = int(ALPHAg::_padcol);

   /// <inheritdoc />
   TAGMVAPadHeightDumper(TTree* tree): TAGMVADumper(tree)
   {
      fPadWHeight = new std::vector<float>(PAD_ROWS*PAD_COLUMNS, 0.);
      fTree->Branch("fPadWHeight", "std::vector<float>", &fPadWHeight);
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      char typefloat = 'F';
      reader->DataInfo().AddVariablesArray("fPadWHeight", PAD_ROWS*PAD_COLUMNS, "fPadWHeight", "nHits", -999., 999., typefloat, false, &fPadWHeight);
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      AgRawSignalsFlow* agrsf=flow->Find<AgRawSignalsFlow>();
      if (!agfe || !agrsf)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if (!storeEvent)
      {
         return false;
      }
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         std::cout<<currentEventNumber <<" != "<< storeEvent->GetEventNumber() <<std::endl;
         return false;
      }

      for(int i=0; i<PAD_COLUMNS; i++)
      {
         for(int j=0; j<PAD_ROWS; j++)
         {
            fPadWHeight->at(i*PAD_ROWS+j) = agrsf->GetPadWHeight(i,j);
         }
      }
      return true;
   }
};



/// @brief With new tracking changes and BV callibration this add some new variables that might be fun
class TAGMVA2024Dumper: public TAGMVADumper
{
   public:   
      float fNMatchedTracks;
      float fNUnmatchedTracks;
      float fNPileUpTracks;

      float S0BVPerp;
      float S0BVX;
      float S0BVY;
      float S0BVZ;
      float S0BVPhi;
      float SBV;
      float S0BV;

      float S0BVTPCPerp;
      float S0BVTPCX;
      float S0BVTPCY;
      float S0BVTPCZ;
      float S0BVTPCPhi;
      float SBVTPC;
      float S0BVTPC;

      float fIsPileup;


   /// <inheritdoc />
   TAGMVA2024Dumper(TTree* tree): TAGMVADumper(tree)
   {
      fTree->Branch("fNMatchedTracks", &fNMatchedTracks, "fNMatchedTracks/F");
      fTree->Branch("fNUnmatchedTracks", &fNUnmatchedTracks, "fNUnmatchedTracks/F");
      fTree->Branch("fNPileUpTracks", &fNPileUpTracks, "fNPileUpTracks/F");

      fTree->Branch("S0BVPerp", &S0BVPerp, "S0BVPerp/F");
      fTree->Branch("S0BVX", &S0BVX, "S0BVX/F");
      fTree->Branch("S0BVY", &S0BVY, "S0BVY/F");
      fTree->Branch("S0BVZ", &S0BVZ, "S0BVZ/F");
      fTree->Branch("S0BVPhi", &S0BVPhi, "S0BVPhi/F");
      fTree->Branch("SBV", &SBV, "SBV/F");
      fTree->Branch("S0BV", &S0BV, "S0BV/F");

      fTree->Branch("S0BVTPCPerp", &S0BVTPCPerp, "S0BVTPCPerp/F");
      fTree->Branch("S0BVTPCX", &S0BVTPCX, "S0BVTPCX/F");
      fTree->Branch("S0BVTPCY", &S0BVTPCY, "S0BVTPCY/F");
      fTree->Branch("S0BVTPCZ", &S0BVTPCZ, "S0BVTPCZ/F");
      fTree->Branch("S0BVTPCPhi", &S0BVTPCPhi, "S0BVTPCPhi/F");
      fTree->Branch("SBVTPC", &SBVTPC, "SBVTPC/F");
      fTree->Branch("S0BVTPC", &S0BVTPC, "S0BVTPC/F");

      fTree->Branch("fIsPileup", &fIsPileup, "fIsPileup/F");
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      reader->AddVariable("fNMatchedTracks", &fNMatchedTracks);      
      reader->AddVariable("fNUnmatchedTracks", &fNUnmatchedTracks);      
      reader->AddVariable("fNPileUpTracks", &fNPileUpTracks);  

      reader->AddVariable("S0BVPerp", &S0BVPerp);  
      reader->AddVariable("S0BVX", &S0BVX);  
      reader->AddVariable("S0BVY", &S0BVY);  
      reader->AddVariable("S0BVZ", &S0BVZ);  
      reader->AddVariable("S0BVPhi", &S0BVPhi);  
      reader->AddVariable("SBV", &SBV);  
      reader->AddVariable("S0BV", &S0BV);  

      reader->AddVariable("S0BVTPCPerp", &S0BVTPCPerp);  
      reader->AddVariable("S0BVTPCX", &S0BVTPCX);  
      reader->AddVariable("S0BVTPCY", &S0BVTPCY);  
      reader->AddVariable("S0BVTPCZ", &S0BVTPCZ);  
      reader->AddVariable("S0BVTPCPhi", &S0BVTPCPhi);  
      reader->AddVariable("SBVTPC", &SBVTPC);  
      reader->AddVariable("S0BVTPC", &S0BVTPC); 

      reader->AddVariable("fIsPileup", &fIsPileup);  
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }
      TBarEvent* barEvent = new TBarEvent(*storeEvent->GetBarEvent());
      if(!barEvent)
      {
         return false;
      }

      this->fNMatchedTracks = barEvent->GetNumberOfMatchedTracks();
      this->fNUnmatchedTracks = barEvent->GetNumberOfUnmatchedTracks();
      this->fNPileUpTracks = barEvent->GetNumberOfPileUpTracks();
      this->fIsPileup = barEvent->IsPileUp();

      //If not enough bar hits to make sphericity measurement return default value
      if(barEvent->GetNumBars()<2)
      {
         this->S0BVPerp = ALPHAg::kLargeNegativeUnknown;
         this->S0BVX = ALPHAg::kLargeNegativeUnknown;
         this->S0BVY = ALPHAg::kLargeNegativeUnknown;
         this->S0BVZ = ALPHAg::kLargeNegativeUnknown;
         this->S0BVPhi = ALPHAg::kLargeNegativeUnknown;
         this->SBV = ALPHAg::kLargeNegativeUnknown;
         this->S0BV = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPCPerp = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPCX = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPCY = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPCZ = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPCPhi = ALPHAg::kLargeNegativeUnknown;
         this->SBVTPC = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPC = ALPHAg::kLargeNegativeUnknown;

         return true;
      }


      //Sphericity of event via the BV from origin (denoted BV) and between vertex (denoted BVTPC)
      std::vector<double> xBV;
      std::vector<double> yBV;
      std::vector<double> zBV;
      std::vector<double> xBVTPC;
      std::vector<double> yBVTPC;
      std::vector<double> zBVTPC;
      for(TBarHit hit: barEvent->GetBarHits())
      {
         if(!hit.IsComplete()) continue;
         if(!hit.HasZed()) continue;
         if(std::isnan(hit.GetZed())) continue;
         TVector3 bvhitVertex = hit.Get3Vector();
         xBV.push_back(bvhitVertex.X());
         yBV.push_back(bvhitVertex.Y());
         zBV.push_back(bvhitVertex.Z());

         if(storeEvent->GetVertexStatus()<1) continue;
         TVector3 bvtpchitVertex = (hit.Get3Vector()*1000) - storeEvent->GetVertex();
         //std::cout << "Vertex = " << storeEvent->GetVertexX() << "," << storeEvent->GetVertexY() << "," << storeEvent->GetVertexZ() << "\n";
         //std::cout << "BV hit = " << hit.Get3Vector()[0] << "," << hit.Get3Vector()[1] << "," << hit.Get3Vector()[2] << "\n";
         xBVTPC.push_back(bvtpchitVertex.X());
         yBVTPC.push_back(bvtpchitVertex.Y());
         zBVTPC.push_back(bvtpchitVertex.Z());
      }





/* std::cout << "Sphericity for the BV dumper: x = [";
         for(auto entry: xBV)
         {
            std::cout << entry << "," ;
         } 
         std::cout << "]"; 
         std::cout << ", y = ["; 
         for(auto entry: yBV)
         {
            std::cout << entry << "," ;
         } 
         std::cout << "]"; 
         std::cout << ", z = ["; 
         for(auto entry: zBV)
         {
            std::cout << entry << "," ;
         } 
         std::cout << "]" << std::endl;  */


      TVector3* S0axisrawBV;
      TVector3* S0valuesrawBV;
      //Here i ask directly if there are enough elements to check the sphericity in case there are issues else where with GetNCompleteTracks
      if(xBV.size()>0 && yBV.size()>0 && zBV.size()>0)
      {
         sphericity(xBV, yBV, zBV, 0, &S0axisrawBV, &S0valuesrawBV);
         double S0rawPerp = S0valuesrawBV->Perp(); //R value of perp vector
         double S0axisrawX = S0axisrawBV->X();
         double S0axisrawY = S0axisrawBV->Y();
         double S0axisrawZ = S0axisrawBV->Z();
         double phi_S0axisraw = TMath::ACos(S0axisrawY/TMath::Sqrt(S0axisrawX*S0axisrawX+S0axisrawY*S0axisrawY));
         double SBV = 1.5*(S0valuesrawBV->Y() + S0valuesrawBV->Z());
         double sphericityBV = sqrt(S0valuesrawBV->X() + S0valuesrawBV->Y());

         this->S0BVPerp = S0rawPerp;
         this->S0BVX = S0axisrawX;
         this->S0BVY = S0axisrawY;
         this->S0BVZ = S0axisrawZ;
         this->S0BVPhi = phi_S0axisraw;
         this->SBV = SBV;
         this->S0BV = sphericityBV;
         delete S0axisrawBV;
         delete S0valuesrawBV;
      }
      else
      {
         this->S0BVPerp = ALPHAg::kLargeNegativeUnknown;
         this->S0BVX = ALPHAg::kLargeNegativeUnknown;
         this->S0BVY = ALPHAg::kLargeNegativeUnknown;
         this->S0BVZ = ALPHAg::kLargeNegativeUnknown;
         this->S0BVPhi = ALPHAg::kLargeNegativeUnknown;
         this->SBV = ALPHAg::kLargeNegativeUnknown;
         this->S0BV = ALPHAg::kLargeNegativeUnknown;
      }




/* 

       std::cout << "Sphericity for the BVTPC dumper: x = [";
         for(auto entry: xBVTPC)
         {
            std::cout << entry << "," ;
         } 
         std::cout << "]"; 
         std::cout << ", y = ["; 
         for(auto entry: yBVTPC)
         {
            std::cout << entry << "," ;
         } 
         std::cout << "]"; 
         std::cout << ", z = ["; 
         for(auto entry: zBVTPC)
         {
            std::cout << entry << "," ;
         } 
         std::cout << "]" << std::endl;  */
 

      TVector3* S0axisrawBVTPC;
      TVector3* S0valuesrawBVTPC;
      if(xBVTPC.size()>0 && yBVTPC.size()>0 && zBVTPC.size()>0)
      {
         sphericity(xBVTPC, yBVTPC, zBVTPC, 0, &S0axisrawBVTPC, &S0valuesrawBVTPC);
         double S0rawPerpBVTPC = S0valuesrawBVTPC->Perp(); //R value of perp vector
         double S0axisrawXBVTPC = S0axisrawBVTPC->X();
         double S0axisrawYBVTPC = S0axisrawBVTPC->Y();
         double S0axisrawZBVTPC = S0axisrawBVTPC->Z();
         double phi_S0axisrawBVTPC = TMath::ACos(S0axisrawYBVTPC/TMath::Sqrt(S0axisrawXBVTPC*S0axisrawXBVTPC+S0axisrawYBVTPC*S0axisrawYBVTPC));
         double SBVTPC = 1.5*(S0valuesrawBVTPC->Y() + S0valuesrawBVTPC->Z());
         double sphericityBVTPC = sqrt(S0valuesrawBVTPC->X() + S0valuesrawBVTPC->Y());

         this->S0BVTPCPerp = S0rawPerpBVTPC;
         this->S0BVTPCX = S0axisrawXBVTPC;
         this->S0BVTPCY = S0axisrawYBVTPC;
         this->S0BVTPCZ = S0axisrawZBVTPC;
         this->S0BVTPCPhi = phi_S0axisrawBVTPC;
         this->SBVTPC = SBVTPC;
         this->S0BVTPC = sphericityBVTPC;

         delete S0axisrawBVTPC;
         delete S0valuesrawBVTPC;

      }
      else
      {
         this->S0BVTPCPerp = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPCX = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPCY = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPCZ = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPCPhi = ALPHAg::kLargeNegativeUnknown;
         this->SBVTPC = ALPHAg::kLargeNegativeUnknown;
         this->S0BVTPC = ALPHAg::kLargeNegativeUnknown;
      }



      //std::cout << "=============== Event Number ================" << std::endl;
      //std::cout << storeEvent->GetEventNumber() << std::endl;
      //PrintEvent();

      return true;
   }


   /// <inheritdoc />
   void PrintEvent()
   {
      std::cout << "==============================================================" << std::endl;
      std::cout << "=============== Printing an MVA event ================" << std::endl;
      std::cout << "fNMatchedTracks = " << this->fNMatchedTracks << std::endl;
      std::cout << "fNUnmatchedTracks = " << this->fNUnmatchedTracks << std::endl;
      std::cout << "fNPileUpTracks = " << this->fNPileUpTracks << std::endl;
      std::cout << "S0BVPerp = " << this->S0BVPerp << std::endl;
      std::cout << "S0BVX = " << this->S0BVX << std::endl;
      std::cout << "S0BVY = " << this->S0BVY << std::endl;
      std::cout << "S0BVZ = " << this->S0BVZ << std::endl;
      std::cout << "S0BVPhi = " << this->S0BVPhi << std::endl;
      std::cout << "SBV = " << this->SBV << std::endl;
      std::cout << "S0BV = " << this->S0BV << std::endl;
      std::cout << "S0BVTPCPerp = " << this->S0BVTPCPerp << std::endl;
      std::cout << "S0BVTPCX = " << this->S0BVTPCX << std::endl;
      std::cout << "S0BVTPCY = " << this->S0BVTPCY << std::endl;
      std::cout << "S0BVTPCZ = " << this->S0BVTPCZ << std::endl;
      std::cout << "S0BVTPCPhi = " << this->S0BVTPCPhi << std::endl;
      std::cout << "SBVTPC = " << this->SBVTPC << std::endl;
      std::cout << "S0BVTPC = " << this->S0BVTPC << std::endl;
      std::cout << "================================================================" << std::endl;
   }
};


/// @brief All TOFs dumped as vector
class TAGMVAAllTOFsDumper: public TAGMVADumper
{
   public:   
      std::vector<float>* fAllTOFs;
      const int numTOFsToSave = 200;

   /// <inheritdoc />
   TAGMVAAllTOFsDumper(TTree* tree): TAGMVADumper(tree)
   {
      fAllTOFs = new std::vector<float>(numTOFsToSave, 0.);
      fTree->Branch("fAllTOFs", "std::vector<float>", &fAllTOFs);
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      char typefloat = 'F';
      reader->DataInfo().AddVariablesArray("fAllTOFs", numTOFsToSave, "fAllTOFs", "fAllTOFs", -999., 999., typefloat, false, &fAllTOFs);
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if (!storeEvent)
      {
         return false;
      }
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }
      TBarEvent* barEvent = new TBarEvent(*storeEvent->GetBarEvent());
      if(!barEvent)
      {
         return false;
      }

      for(int i=0; i<barEvent->GetNumTOF(); i++)
      {
         fAllTOFs->at(i) = barEvent->GetTOFs().at(i).GetTOF();
      }
      return true;
   }
};

/// @brief All distances between TOF pairs dumped as vector
class TAGMVAAllTOFDistsDumper: public TAGMVADumper
{
   public:   
      std::vector<float>* fAllTOFDists;
      const int numTOFsToSave = 200;

   /// <inheritdoc />
   TAGMVAAllTOFDistsDumper(TTree* tree): TAGMVADumper(tree)
   {
      fAllTOFDists = new std::vector<float>(numTOFsToSave, 0.);
      fTree->Branch("fAllTODists", "std::vector<float>", &fAllTOFDists);
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      char typefloat = 'F';
      reader->DataInfo().AddVariablesArray("fAllTOFDists", numTOFsToSave, "fAllTOFDists", "fAllTOFDists", -999., 999., typefloat, false, &fAllTOFDists);
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if (!storeEvent)
      {
         return false;
      }
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }
      TBarEvent* barEvent = new TBarEvent(*storeEvent->GetBarEvent());
      if(!barEvent)
      {
         return false;
      }

      for(int i=0; i<barEvent->GetNumTOF(); i++)
      {
         fAllTOFDists->at(i) = barEvent->GetTOFs().at(i).GetDistanceOverC();
      }
      return true;
   }
};

/// @brief All TDC bar hit times dumped as vector
class TAGMVAAllTDCsDumper: public TAGMVADumper
{
   public:   
      std::vector<float>* fAllTDCs;
      const int numTDCsToSave = 20;

   /// <inheritdoc />
   TAGMVAAllTDCsDumper(TTree* tree): TAGMVADumper(tree)
   {
      fAllTDCs = new std::vector<float>(numTDCsToSave, 0.);
      fTree->Branch("fAllTDCs", "std::vector<float>", &fAllTDCs);
   }

   /// <inheritdoc />
   void LoadVariablesToReader(TMVA::Reader* reader)
   {
      char typefloat = 'F';
      reader->DataInfo().AddVariablesArray("fAllTDCs", numTDCsToSave, "fAllTDCs", "fAllTDCs", -999., 999., typefloat, false, &fAllTDCs);
   }

   /// <inheritdoc />
   bool UpdateVariables(TAFlowEvent* flow, int currentEventNumber)
   {
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
         return false;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if (!storeEvent)
      {
         return false;
      }
      if ( currentEventNumber != storeEvent->GetEventNumber() )
      {
         return false;
      }
      TBarEvent* barEvent = new TBarEvent(*storeEvent->GetBarEvent());
      if(!barEvent)
      {
         return false;
      }

      double event_tdc_time = barEvent->GetEventTDCTime()*1e9;
      double avg_tdc_time = 0;
      std::vector<double> all_tdc_times;
      for (const TBarHit& h: barEvent->GetBarHits())
      {
         // Pileup events should be excluded already
         double tdc_time = h.GetTime()*1e9 - event_tdc_time;
         all_tdc_times.push_back(tdc_time);
         avg_tdc_time += tdc_time;
      }
      avg_tdc_time /= barEvent->GetNumBars();
      std::sort(all_tdc_times.begin(),all_tdc_times.end());

      for(int i=0; i<all_tdc_times.size(); i++)
      {
         fAllTDCs->at(i) = all_tdc_times.at(i) - avg_tdc_time;
      }
      return true;
   }
};





#endif