//
// RecoFlow.h
//
// manalyzer flow objects for ALPHA-g events
// 
// NM, LM, AC, JTKM, LMG
//

#ifndef _RECOFLOW_
#define _RECOFLOW_ 1

#ifdef BUILD_AG

#include "TBarEvent.hh"

class AgBarEventFlow: public TAFlowEvent
{
public:
   TBarEvent* BarEvent;
public:
   AgBarEventFlow(TAFlowEvent* flow, TBarEvent* b) //ctor
      : TAFlowEvent(flow)
   {
      BarEvent=b;
   }
   ~AgBarEventFlow() //dtor
   {
      if (BarEvent)
         {
            //   BarEvent->Reset();
            delete BarEvent;
         }
   }
};


#endif

#include "Sequencer_Channels.h"
// To avoid parsing the sequencer XML in the main thread, we copy the 
// text into the flow to be processed in its own thread
class SEQTextFlow: public TAFlowEvent
{
public:
   char* data;
   int size=0;
   void AddData(char* _data, int _size)
   {
      size=_size;
      data=(char*) malloc(size);
      memcpy(data, _data, size);
      return;
   }
   void Clear()
   {
      if (size)
         free( data );
      size=0;
   }
   SEQTextFlow(TAFlowEvent* flow): TAFlowEvent(flow)
   {
   }
   ~SEQTextFlow()
   {
      Clear();
   }
};
#ifdef BUILD_AG
#include "TAGSpill.h"
#include "TPCconstants.hh"


class AGSpillFlow: public TAFlowEvent
{
public:
   std::vector<TAGSpill*> spill_events;

   AGSpillFlow(TAFlowEvent* flow): TAFlowEvent(flow)
   {
   }
   ~AGSpillFlow()
   {
      for (size_t i=0; i<spill_events.size(); i++)
         delete spill_events[i];
      spill_events.clear();
   }

};

#include "TStoreEvent.hh"
class AgAnalysisFlow: public TAFlowEvent
{
public:
   TStoreEvent *fEvent = NULL;

public:
   AgAnalysisFlow(TAFlowEvent* flow, TStoreEvent* e) // ctor
      : TAFlowEvent(flow)
   { 
      fEvent=e;
   }
   ~AgAnalysisFlow()
   {
      if (fEvent) delete fEvent;
      fEvent=NULL;
   }

};

#include "SignalsType.hh"
#include "TracksFinder.hh"
#include "TFitVertex.hh"
#include "TFitHelix.hh"

class AgSignalsFlow: public TAFlowEvent
{
public:
   std::vector<ALPHAg::TWireSignal> awSig;
   std::vector<ALPHAg::TPadSignal> pdSig;
   int fNpadSigs = 0;

   std::vector<ALPHAg::wfholder> PadWaves; //Intermediary within deconv_pad_module
   std::vector<ALPHAg::electrode> PadIndex; //Intermediary within deconv_pad_module

   std::vector< std::vector<ALPHAg::TPadSignal> > comb; //Intermediary within match_modules
   std::vector<ALPHAg::TPadSignal> combinedPads; //Intermediary within match_modules

   std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> > matchSig;

   bool bad = false;
   
   // Reco associated containers
   bool fSkipReco = false;
   std::vector< TSpacePoint> fSpacePoints;
   std::vector< track_t> fTrackVector;
   std::vector<TTrack> fTracksArray;
   std::vector<TFitLine> fLinesArray;
   std::vector<TFitHelix> fHelixArray;
   // Used in helix joiner, left empty
   std::vector<HelixAndIDs> fCombinedHelixArray;

  //  // Reco associated containers - the trivial track for MVA
  // std::vector<track_t> fTrivialTrackVector;
  // std::vector<TTrack> fTrivialTrackArray;
  // std::vector<TFitLine> fTrivialLineArray;
  // std::vector<TFitHelix> fTrivialHelixArray;
   
  //  // Reco associated containers - the USED trivial track for MVA
  // std::vector<track_t> fUsedTrivialTrackVector;
  // std::vector<TTrack> fUsedTrivialTrackArray;
  // std::vector<TFitLine> fUsedTrivialLineArray;
  // std::vector<TFitHelix> fUsedTrivialHelixArray;

  // for display module
   std::vector<ALPHAg::wf_ref> AWwf;
   std::vector<ALPHAg::wf_ref> PADwf;

  // for diagnostics
   std::vector<ALPHAg::TWireSignal> adc32max;
  std::vector<ALPHAg::TWireSignal> adc32ped;
   //  std::vector<signal> adc32range;
   std::vector<ALPHAg::TPadSignal> pwbMax;
   //  std::vector<ALPHAg::signal> pwbRange;
  std::vector<ALPHAg::TPadSignal> pwbPed;

  // vertex info
   vertexstatus_t fitStatus;
   TFitVertex fitVertex;
   
public:
   AgSignalsFlow(TAFlowEvent* flow):
      TAFlowEvent(flow)
   {
      fSkipReco = false;
      bad = false;
      fitStatus = kUninitialized;
   }
  
   AgSignalsFlow(TAFlowEvent* flow,
                 std::vector<ALPHAg::TWireSignal> s):
      TAFlowEvent(flow)
   {
      fSkipReco = false;
      bad = false;
      awSig = std::move(s);
      fitStatus = kUninitialized;
   }

   AgSignalsFlow(TAFlowEvent* flow,
                 std::vector<ALPHAg::TWireSignal>& s,
                 std::vector<ALPHAg::TPadSignal>& p):
      TAFlowEvent(flow)
   {
      fSkipReco = false;
      bad = false;
      awSig = std::move(s);
      pdSig = std::move(p);
      fNpadSigs = pdSig.size();
      fitStatus = kUninitialized;
   }

   AgSignalsFlow(TAFlowEvent* flow,
                 std::vector<ALPHAg::TWireSignal>& s,std::vector<ALPHAg::TPadSignal>& p,
                 std::vector<ALPHAg::wf_ref>& awf, std::vector<ALPHAg::wf_ref>& pwf):
      TAFlowEvent(flow)
   {
      fSkipReco = false;
      bad = false;
      AWwf = std::move(awf);
      PADwf = std::move(pwf);
      awSig = std::move(s);
      pdSig = std::move(p);
      fNpadSigs = pdSig.size();
      fitStatus = kUninitialized;
   }

   ~AgSignalsFlow()
   {
      awSig.clear();
      pdSig.clear();
      matchSig.clear();
      AWwf.clear();
      PADwf.clear();
      adc32max.clear();
     adc32ped.clear();
      pwbMax.clear();
      fHelixArray.clear();
      fLinesArray.clear();
   }

   void DeletePadSignals()
   {
      pdSig.clear();
   }

   void AddPadSignals( std::vector<ALPHAg::TPadSignal> s )
   {
      pdSig = std::move(s);
   }

   void AddMatchSignals( std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal>> ss )
   {
      //matchSig=ss;
      matchSig = std::move(ss);
   }

   void AddAWWaveforms(std::vector<ALPHAg::wf_ref> af)
   {
      AWwf = std::move(af);
   }

   void AddPADWaveforms(std::vector<ALPHAg::wf_ref> pf)
   {
      PADwf = std::move(pf);
   }

   void AddWaveforms(std::vector<ALPHAg::wf_ref> af, std::vector<ALPHAg::wf_ref> pf)
   {
      AWwf = std::move(af);
      PADwf = std::move(pf);
   }

   void AddAdcPeaks(std::vector<ALPHAg::TWireSignal> s)
   {
      adc32max = std::move(s);
   }

   void AddAdcPeds(std::vector<ALPHAg::TWireSignal> s)
   {
      adc32ped = std::move(s);
   }

   void AddPwbPeaks(std::vector<ALPHAg::TPadSignal> s)
   {
      pwbMax = std::move(s);
   }
};


class AgRawSignalsFlow: public TAFlowEvent
{
public:
   static const int AW_COLUMNS = 1; //int(ALPHAg::_anodescol);
   static const int AW_ROWS = 256; //int(ALPHAg::_anodesrow);
   static const int PAD_COLUMNS = 32; //int(ALPHAg::_padcol);
   static const int PAD_ROWS = 576; //int(ALPHAg::_padrow);

   std::array<int, AW_COLUMNS*AW_ROWS> fAWSigNHits;
   std::array<double, AW_COLUMNS*AW_ROWS> fAWSigTotalWHeight;
   std::array<int, PAD_COLUMNS*PAD_ROWS> fPADSigNHits;
   std::array<double, PAD_COLUMNS*PAD_ROWS> fPADSigTotalWHeight;
   
public:
   AgRawSignalsFlow(TAFlowEvent* flow):
      TAFlowEvent(flow)
   {
      // Allocate a single large block of memory for all the arrays
      //fAWSigNHits           = new int[AW_COLUMNS*AW_ROWS];
      //fAWSigTotalWHeight    = new double[AW_COLUMNS*AW_ROWS];
      //fPADSigNHits          = new int[PAD_COLUMNS*PAD_ROWS];
      //fPADSigTotalWHeight   = new double[PAD_COLUMNS*PAD_ROWS];

            fAWSigNHits = {0};
      fAWSigTotalWHeight = {0};
      fPADSigNHits = {0};
      fPADSigTotalWHeight = {0};

      // Set all the elements of the array to zeros using memset
      //memset(fAWSigNHits,           0, (AW_COLUMNS*AW_ROWS) * sizeof(int));
      //memset(fAWSigTotalWHeight,    0, (AW_COLUMNS*AW_ROWS) * sizeof(double));
      //memset(fPADSigNHits,          0, (PAD_COLUMNS*PAD_ROWS) * sizeof(int));
      //memset(fPADSigTotalWHeight,   0, (PAD_COLUMNS*PAD_ROWS) * sizeof(double));
   }

   AgRawSignalsFlow(TAFlowEvent* flow,
                 std::vector<ALPHAg::TWireSignal>& awsigs,
                 std::vector<ALPHAg::TPadSignal>& padsigs):
      TAFlowEvent(flow)
   {
      // Allocate a single large block of memory for all the arrays
      fAWSigNHits = {0};
      fAWSigTotalWHeight = {0};
      fPADSigNHits = {0};
      fPADSigTotalWHeight = {0};

      // Set all the elements of the array to zeros using memset
      //memset(fAWSigNHits,           0, (AW_COLUMNS*AW_ROWS) * sizeof(int));
      //memset(fAWSigTotalWHeight,    0, (AW_COLUMNS*AW_ROWS) * sizeof(double));
      //memset(fPADSigNHits,          0, (PAD_COLUMNS*PAD_ROWS) * sizeof(int));
      //memset(fPADSigTotalWHeight,   0, (PAD_COLUMNS*PAD_ROWS) * sizeof(double));

      AddRawWireSignals(awsigs);
      AddRawPadSignals(padsigs);
   }

   void AddRawWireSignals(std::vector<ALPHAg::TWireSignal>& awsigs)
   {
      for(size_t i=0; i<awsigs.size(); i++)
      {
         AddRawWireSignal(awsigs.at(i));
      }
   }

   void AddRawWireSignal(ALPHAg::TWireSignal& awsig)
   {
      if (awsig.sec>0 || awsig.idx>256)
      {
         std::cout << "\n\n\n\n\n FINALLY \n\n\n\n\n ITS HERE \n\n\n\n\n Adding: [" << awsig.sec << "][" << awsig.idx << "] raw signal.\n\n\n\n";
      }
      
      fAWSigNHits[awsig.sec*AW_ROWS + awsig.idx]++;
      fAWSigTotalWHeight[awsig.sec*AW_ROWS + awsig.idx]+=awsig.height; //Need a check height is assigned first.

   }

   void AddRawPadSignals(std::vector<ALPHAg::TPadSignal>& padsigs)
   {
      for(size_t i=0; i<padsigs.size(); i++)
      {
         AddRawPadSignal(padsigs.at(i));
      }
   }

   void AddRawPadSignal(ALPHAg::TPadSignal& padsigs)
   {
      fPADSigNHits[padsigs.sec*PAD_ROWS + padsigs.idx]++;
      fPADSigTotalWHeight[padsigs.sec*PAD_ROWS + padsigs.idx]+=padsigs.height; //Need a check height is assigned first.
   }

   ~AgRawSignalsFlow()
   {
      //Apparantly no need to destroy anything here.
      //https://stackoverflow.com/questions/48414225/destroying-a-vector-of-vectors
   }

   int GetAnodeWireNHits(int i, int j)
   {
      return fAWSigNHits[i*AW_ROWS + j];
   }
   double GetAnodeWireWHeight(int i, int j)
   {
      return fAWSigTotalWHeight[i*AW_ROWS + j];
   }
   int GetPadNHits(int i, int j)
   {
      return fPADSigNHits[i*PAD_ROWS + j];
   }
   double GetPadWHeight(int i, int j)
   {
      return fPADSigTotalWHeight[i*PAD_ROWS + j];
   }

};

#include "TKDTree.h"
#include <string>
#include "TKDTreeMatch.hh"


class AgKDTreeMatchFlow: public TAFlowEvent
{
public:
   std::list<KDTreeIDContainer2D*> f2DTrees;
   AgKDTreeMatchFlow(TAFlowEvent* flow):
      TAFlowEvent(flow)
   {

   }
   ~AgKDTreeMatchFlow()
   {
      for (KDTreeIDContainer2D* t: f2DTrees)
         delete t;
      f2DTrees.clear();
   }
   KDTreeIDContainer2D* AddKDTree(int entries, const char* name)
   {
      f2DTrees.emplace_back(new KDTreeIDContainer2D(entries,name));
      return f2DTrees.back();
   }
   KDTreeIDContainer2D* GetTree(const std::string name)
   {
      for (KDTreeIDContainer2D* t: f2DTrees)
         {
            if (t->IsMatch(name))
               return t;
         }
      return nullptr;
   }
   std::vector<double>* GetXArray(const std::string name)
   { 
      KDTreeIDContainer2D* t = GetTree(name);
      if (t)
         return &t->X();
      else
         return nullptr;
   }
   std::vector<double>* GetYArray(std::string name)
   { 
      KDTreeIDContainer2D* t = GetTree(name);
      if (t)
         return &t->Y();
      else
         return nullptr;
   }
};

#endif
#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
