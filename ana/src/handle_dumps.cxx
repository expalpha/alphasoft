//
// Slow down flow to real time (testing module)
//
// JTK McKENNA
//

#include <stdio.h>

#include "manalyzer.h"
#include "midasio.h"
#include "TSpill.h"
#include "AgFlow.h"
#include "AnalysisFlow.h"
#include "RecoFlow.h"

#include "cb_flow.h"
#include "store_cb.h"

#include "TAGDetectorEvent.hh"
#include "TChronoChannel.h"
#include "TChronoChannelName.h"
#include "TChronoBoardCounter.h"
#include "TDumpList.h"
#include <iostream>

#include <array>


class DumpMakerModuleFlags
{
public:
   bool fPrint = false;
};

class DumpMakerModule: public TARunObject
{
public:
   DumpMakerModuleFlags* fFlags;
   bool fTrace = false;

   TString fSeqNames[NUMSEQ]={"cat","rct","atm","pos","rct_botg","atm_botg","atm_topg","rct_topg","bml"};
   std::string fCBNames[CHRONO_N_BOARDS]={"cbtrg","cb01","cb02","cb03","cb04"};

   std::array<TChronoChannel,USED_SEQ> fChronoStartChannels;
   std::array<TChronoChannel,USED_SEQ> fChronoStopChannels;
   std::array<TChronoChannel,USED_SEQ> fChronoSeqChannels;

   TChronoChannel fMixingChannel;

   std::deque<std::shared_ptr<TAGDetectorEvent>> fTPCEventBuffer;
   std::deque<std::shared_ptr<TCbFIFOEvent>> fChronoEventBuffer;

   //Variables needed to build a spill, each of these members is an array of the number of sequencers, and has 1 element per dump. Ie: each spill needs 1 start and stop dump marker, 1 start/stop Chrono trg, and a vector of seq states (fSeqStates)
   //Records the Chrono triggers as they come in
   std::array<std::deque< double >, USED_SEQ> fStartDumpChronoTrgs;
   std::array<std::deque< double >, USED_SEQ> fStopDumpChronoTrgs;
   //Saves the dump markers as they come in
   std::array<std::deque< TDumpMarker >, USED_SEQ> fStartDumpMarkers;
   std::array<std::deque< TDumpMarker >, USED_SEQ> fStopDumpMarkers;
   //Records whether we've thrown away a start or stop, initially zero it becomes 1 if we throw away a start...
   std::array<std::deque< int >, USED_SEQ> fFuzzyStart;
   std::array<std::deque< int >, USED_SEQ> fFuzzyStop;
   //Vector to store the states, this is the same as above (one element per dump) but that element is now a vector because each dump has multiple states.
   std::array<std::deque< std::vector<TSequencerState> >, USED_SEQ> fSeqStates;
   //Deque for mixing flag (this comes from outside and offers a good way to sync things)
   std::deque<Double_t> fMixingChronoTrgs;

   //This tracks the most recent seq we are on.
   int fCurrentSeq[NUMSEQ] = {0};

   // Flips to false when we get the first TPC data AgAnalysisFlow. While false, don't require TPC data to flush buffers.
   bool fNoTPCAnalysis = true;


   int fMarkerCounts[USED_SEQ][2] = {{0}};


   std::mutex fSequencerLock[USED_SEQ];
   std::mutex moduleLock;

   //The packet buffer... Packets can sometimes be more than 10s off despite having the correct timestamp within the packet...
   //We want this as low as possible without breaking things. This will essentially cause the spill log to lag behind at a minimum of this amount of seconds.
   double fPacketBuffer = 2;

   //Wait this long before clearing the buffers.
   double fBufferClearWait = 5; 
   
   DumpMakerModule(TARunInfo* runinfo, DumpMakerModuleFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Handle Dumps";
#endif
      if (fTrace)
         printf("DumpMakerModule::ctor!\n");
   }

   ~DumpMakerModule()
   {
      if (fTrace)
         printf("DumpMakerModule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("DumpMakerModule::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());

      //Set all channels as default initially.
      for (int i=0; i<USED_SEQ; i++)
      {
         int iSeq=USED_SEQ_NUM[i];
         
         std::cout<<i<<" is " << iSeq <<std::endl;
         fChronoStartChannels.at(iSeq).SetChannel(-1);
         fChronoStartChannels.at(iSeq).SetBoard("");
         fChronoStopChannels.at(iSeq).SetChannel(-1);
         fChronoStopChannels.at(iSeq).SetBoard("");
         //StartSeqChannel[iSeq].Channel=-1;
         //StartSeqChannel[iSeq].Board=-1;
      }

      //Now try set the channels correctly
      for (const std::pair<const std::string,int>& board: TChronoChannel::CBMAP)
      {
         TChronoChannelName name(runinfo->fOdb,board.first);
         int channel = -1;
         for (int i = 0; i < USED_SEQ; i++)
         {
            int iSeq=USED_SEQ_NUM.at(i);
            channel = name.GetChannel(StartDumpName[i]);
            if (channel>0)
            {
               std::cout<<"Sequencer["<<iSeq<<"]:"<<StartDumpName[iSeq]<<" on channel:"<<channel<< " board:"<<board.first << "("<<board.second<<")"<<std::endl;
               fChronoStartChannels.at(iSeq).SetChannel(channel);
               fChronoStartChannels.at(iSeq).SetBoard(board.first);
               std::cout<<"Start Channel:"<<channel<<std::endl;
            }
            channel = name.GetChannel(StopDumpName[i]);
            if (channel>0)
            {
               std::cout<<"Sequencer["<<iSeq<<"]:"<<StopDumpName[iSeq]<<" on channel:"<<channel<< " board:"<<board.first << "("<<board.second<<")"<<std::endl;
               fChronoStopChannels.at(iSeq).SetChannel(channel);
               fChronoStopChannels.at(iSeq).SetBoard(board.first);
               std::cout<<"Stop Channel:"<<channel<<std::endl;
            }
         }
      }

   }

   void EndRun(__attribute__((unused)) TARunInfo* runinfo)
   {
      for (int j=0; j<USED_SEQ; j++) 
      {
         if(fStartDumpChronoTrgs.at(j).size())
         {
            std::cout << "AG SPILL WARNING: End run but still start dumps Chrono in chamber. Seq: "<< GetSequencerName(j) << " (" << fStartDumpChronoTrgs.at(j).size() << ") dumps remaining.\n"; 
         }
         if(fStopDumpChronoTrgs.at(j).size())
         {
            std::cout << "AG SPILL WARNING: End run but still stop dumps Chrono in chamber. Seq: "<< GetSequencerName(j) << " (" << fStopDumpChronoTrgs.at(j).size() << ") dumps remaining.\n"; 
         }
         if(fStartDumpMarkers.at(j).size())
         {
            std::cout << "AG SPILL WARNING: End run but still start dumps markers in chamber. Seq: "<< GetSequencerName(j) << " (" << fStartDumpMarkers.at(j).size() << ") dumps remaining.\n"; 
         }
         if(fStopDumpMarkers.at(j).size())
         {
            std::cout << "AG SPILL WARNING: End run but still stop dumps markers in chamber. Seq: "<< GetSequencerName(j) << " (" << fStopDumpMarkers.at(j).size() << ") dumps remaining.\n"; 
         }
      }

      /*std::cout << "AG SPILL INFO: Spill markers:\n";
      for(int i = 0; i<USED_SEQ; i++)
      {
         for(int j = 0; j<2; j++)
         {
            std::cout << fMarkerCounts[i][j] << ",";
         }
         std::cout << "\n";
      }
      std::cout << "AG SPILL INFO: Do they match??\n";*/

   }
   
   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("DumpMakerModule::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }

   //Catch sequencer flow in the main thread, so that we have expected dumps ASAP
   TAFlowEvent* Analyze(  __attribute__((unused)) TARunInfo* runinfo, TMEvent* me, TAFlags* flags, TAFlowEvent* flow)
   {
      if( me->event_id != 8 ) // sequencer event id
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      DumpFlow* DumpsFlow=flow->Find<DumpFlow>();
      if (!DumpsFlow)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      const uint ndumps=DumpsFlow->DumpMarkers.size();
      if (!ndumps)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }


      int seqNum=DumpsFlow->SequencerNum;
      fCurrentSeq[seqNum] = DumpsFlow->DumpMarkers.at(0).fSequenceCount+1;
      //std::cout << "\n\n Seq num = " << fCurrentSeq[seqNum] << "\n";
      {
         //Lock scope
         std::lock_guard<std::mutex> locallock(moduleLock);
         int numExistingDumps = fStartDumpMarkers.at(seqNum).size();
         std::lock_guard<std::mutex> lock(fSequencerLock[seqNum]);

         //Loop through the sequencer xml data (unpacked elsewhere) and push back dump markers.
         for(const TDumpMarker& dump: DumpsFlow->DumpMarkers)
         {
            //std::cout << "Got dump\n";
            if(dump.fDumpType == TDumpMarker::kDumpTypes::Start )
            {
               //std::cout << "SPILL INFO: Adding dump " << dump.fDescription << " to fDumpNames(" << seqNum <<") \n";
               fStartDumpMarkers.at(seqNum).push_back(dump);
               fFuzzyStart.at(seqNum).push_back(0);
            }
            if(dump.fDumpType == TDumpMarker::kDumpTypes::Stop )
            {
               fStopDumpMarkers.at(seqNum).push_back(dump);
               fFuzzyStop.at(seqNum).push_back(0);
            }
         }

         if(fStartDumpMarkers.at(seqNum).size() != fStopDumpMarkers.at(seqNum).size())
         {
            std::cout << "SPILL WARNING: Non equal number of starts and stops encountered. I will continue but things might be bad...\n";
         }

         //Local lock this scope... The moment we grab numExistingDumps we do not want the FlushEvents() to pop_front of the fStartDumpMarkers vector (which it will do when a new dump is created - unless we lock)

         for(size_t i=numExistingDumps; i<fStartDumpMarkers.at(seqNum).size(); i++)
         {
            //std::cout << "SPILL INFO: For spill " << i << " we want to add states: " << fStartDumpMarkers.at(seqNum).at(i).fonState << " - " << fStopDumpMarkers.at(seqNum).at(i).fonState << ".\n";
            
            fSeqStates.at(seqNum).push_back( std::vector<TSequencerState>{} );
            for(int j=fStartDumpMarkers.at(seqNum).at(i).fonState; j<fStopDumpMarkers.at(seqNum).at(i).fonState+1; j++)
            {
               //std::cout << "SPILL INFO: For spill " << i << " we are adding state " << j << ".\n";
               fSeqStates.at(seqNum).at(i).push_back( DumpsFlow->fStates.at(j) );
               fStartDumpMarkers.at(seqNum).at(i).fStatesFull = true;
            }
         }

      }
      return flow;
   }

   TAFlowEvent* AnalyzeFlowEvent( __attribute__((unused)) TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      TCbFlow* FIFOFlow = flow->Find<TCbFlow>();

      TCbFIFOEventFlow* ChronoFlow = flow->Find<TCbFIFOEventFlow>();
      if (!FIFOFlow && !ChronoFlow)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      AGSpillFlow* f = new AGSpillFlow(flow);
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();

      //Add every Chrono event to the buffer.
      //std::map<std::string,std::vector<std::shared_ptr<TCbFIFOEvent>>> fCbHits;
      double currentRunTime = -1;
      if (ChronoFlow)
      {
         for (int j=0; j<CHRONO_N_BOARDS; j++)
         {
            const std::vector<std::shared_ptr<TCbFIFOEvent>> ce = ChronoFlow->fCbHits[fCBNames[j]];
            for(std::shared_ptr<TCbFIFOEvent> event: ce)
            {
               fChronoEventBuffer.push_back(event);
               if(currentRunTime < 0)
               {
                  currentRunTime = event->GetRunTime();
               }
            }
         }
      }

      //Add every TPCEvent to the buffer
      if (agfe)
      {
         if (fNoTPCAnalysis) fNoTPCAnalysis = false;
         //std::cout << "agfe event found??? \n";
         const TStoreEvent* storeEvent = agfe->fEvent;
         const TBarEvent* barEvent = agfe->fEvent->GetBarEvent();
         std::shared_ptr<TAGDetectorEvent> sharedEvent = std::make_shared<TAGDetectorEvent>(storeEvent, barEvent);
         fTPCEventBuffer.push_back( sharedEvent );
      }



      if (ChronoFlow)
      {
         for (const std::pair<const std::string,std::vector<std::shared_ptr<TCbFIFOEvent>>>& hits: ChronoFlow->fCbHits)
         {
            for (int a=0; a<USED_SEQ; a++)
            {
               std::lock_guard<std::mutex> lock(fSequencerLock[a]);
               if (fChronoStartChannels[a].GetBoard() == hits.first)
               {
                  for (const std::shared_ptr<TCbFIFOEvent>& hit: hits.second)
                  {
                     if (fChronoStartChannels[a].GetChannel() == int( hit->GetChannel() ))
                     {
                        if (hit->IsLeadingEdge())
                        {
                           //Check we have start dump markers, if not (size == 0) warn the user.
                           if(fStartDumpMarkers.at(a).size()==0)
                           {
                              TAGSpill* badstart = new TAGSpill(runinfo->fRunNo,hit->GetMidasTime(),"[ %fs, %s ] SPILL WARNING: Start dump Chrono trigger but no sequencer data. Did run start late?",hit->GetRunTime(), GetSequencerName(a).c_str());
                              f->spill_events.push_back(badstart);
                              fMarkerCounts[a][0]++;
                              goto stopDumpCheck;
                           }
                           std::cout << std::fixed << std::setprecision(15); // Set the desired precision (e.g., 15 digits)
                           //std::cout << "SPILL INFO: Start dump spotted: midastime: " << hit->GetMidasTime() << ", runtime: " << hit->GetRunTime() << ", seq: " << GetSequencerName(a) << "\n";
                           fMarkerCounts[a][0]++;
                           //We've just seen a start dump, that means that there should currently be an equal number of stops and starts right now:
                           /*if(fStartDumpChronoTrgs.at(a).size()>fStopDumpChronoTrgs.at(a).size()
                           && (currentRunTime - fStartDumpChronoTrgs.at(a).back() > fPacketBuffer
                              || currentRunTime - fStopDumpChronoTrgs.at(a).back() > fPacketBuffer))
                           {
                              //std::cout << "Buffer is catching up. Wait\n";
                              TAGSpill* fuzzystart= new TAGSpill(runinfo->fRunNo,hit->GetRunTime(),"[ %fs, %s ] SPILL WARNING: Multiple start dump Chrono triggers in a row detected - throwing one away (this dump will have fuzzy start)",hit->GetRunTime(), GetSequencerName(a).c_str());
                              f->spill_events.push_back(fuzzystart);
                              std::cout << "SPILL WARNING: Multiple starts in a row detected, I have to throw one away but I don't know which is real- seq:" << GetSequencerName(a) << "\n";
                              std::cout << "SPILL WARNING: Because I threw away a start, the start time of this dump will be fuzzy:" << GetSequencerName(a) << "\n";
                              //If we've detected another start dump we have to just throw one away... We will never know which is the correct start time, this dump will have a fuzzy start time:
                              if(fFuzzyStart.at(a).size())
                                 fFuzzyStart.at(a).back()=1;
                              else
                                 fFuzzyStart.at(a).push_back(1);
                           }*/
                          //So the above checks for multiple start dumps in a row... The issue is they always come like this...
                           if(false) {}
                           else
                           {
                              //If everything looks good, lets add to the list of triggers
                              fStartDumpChronoTrgs.at(a).push_back(hit->GetRunTime());
                           }
                        }
                     }
                  }
               }
               stopDumpCheck:
               if (fChronoStopChannels[a].GetBoard() == hits.first)
               {
                  for (const std::shared_ptr<TCbFIFOEvent> &hit: hits.second)
                  {
                     if (fChronoStopChannels[a].GetChannel() == int( hit->GetChannel() ))
                     {
                        if (hit->IsLeadingEdge())
                        {
                           //Check we have start dump markers, if not (size == 0) warn the user.
                           if(fStopDumpMarkers.at(a).size()==0)
                           {
                              TAGSpill* badstop = new TAGSpill(runinfo->fRunNo,hit->GetMidasTime(),"[ %fs, %s ] SPILL WARNING: Stop dump Chrono trigger but no sequencer data. Did run start late?",hit->GetRunTime(), GetSequencerName(a).c_str());
                              f->spill_events.push_back(badstop);
                              fMarkerCounts[a][1]++;
                              continue;
                           }
                           std::cout << std::fixed << std::setprecision(15); // Set the desired precision (e.g., 15 digits)
                           //std::cout << "SPILL INFO: Stop dump spotted: midastime: " << hit->GetMidasTime() << ", runtime: " << hit->GetRunTime() << ", seq: " << GetSequencerName(a) << "\n";
                           fMarkerCounts[a][1]++;
                           //We've just seen a stop dump, that means that there should currently be more starts than stops:
                           /*if(fStopDumpChronoTrgs.at(a).size()>=fStartDumpChronoTrgs.at(a).size()
                           && (currentRunTime - fStartDumpChronoTrgs.at(a).back() > fPacketBuffer
                              || currentRunTime - fStopDumpChronoTrgs.at(a).back() > fPacketBuffer))
                           {
                              TAGSpill* fuzzystop = new TAGSpill(runinfo->fRunNo,hit->GetMidasTime(),"[ %fs, %s ] SPILL WARNING: Multiple stop dump Chrono triggers in a row detected - throwing one away (this dump will have fuzzy stop).",hit->GetRunTime(), GetSequencerName(a).c_str());
                              f->spill_events.push_back(fuzzystop);
                              std::cout << "SPILL WARNING: Multiple stops in a row detected, I have to throw one away but I don't know which is real- seq:" << GetSequencerName(a) << "\n";
                              std::cout << "SPILL WARNING: Because I threw away a stop, the stop time of this dump will be fuzzy:" << GetSequencerName(a) << "\n";
                              //If we've detected another stop dump we have to just throw one away... We will never know which is the correct stop time, this dump will have a fuzzy stop time:
                              if(fFuzzyStop.at(a).size())
                                 fFuzzyStop.at(a).back()=1;
                              else
                                 fFuzzyStop.at(a).push_back(1);
                           }*/
                           //So the above checks for multiple start dumps in a row... The issue is they always come like this...
                          if(false) {}
                           else
                           {
                              //If everything looks good, lets add to the list of triggers
                              fStopDumpChronoTrgs.at(a).push_back(hit->GetRunTime());
                           }
                           
                        }
                     }
                  }
               }
            }            

            for (int a=0; a<USED_SEQ; a++)
            {
               std::lock_guard<std::mutex> lock(fSequencerLock[a]);
               //if (SISFlow->sis_events[j].size()
               //JOE! This copy constructor is pretty ugly!
               int board_no = TChronoChannel::CBMAP.at(hits.first);
               std::vector<TChronoBoardCounter> counters;
               //hits.reserve(hits.second.size());
               for (const std::shared_ptr<TCbFIFOEvent>& e: hits.second)
                  counters.emplace_back(e,board_no);
               //dumplist[a].AddScalerEvents(counters);
               //dumplist[a].Print();
            }
         
         
            //Check if we are ready to flush but only with a chrono event, that has a time.
            //This is so we can wait for a couple seconds before we flush.
            //Check we are ready to flush and if so, flush (this creates a new spill with the first element of each of our vectors defined in this class)
            for (int a = 0; a < USED_SEQ; a++)
            {
               bool ready = ReadyToFlush(a, currentRunTime);//Are we???
               if(ready)
               {
                  FlushEvents(runinfo->fRunNo, flow, f, a);
               }
            }
         
         
         
         
         
         
         }






      }


      //Clear the buffers
      ClearBuffers();

      return f; 
   }
   

   bool ReadyToFlush(int seqNum, double runTime)
   {
      // bool ready = false; -- unused AC 10/1/2024
      //This checks we have start and stop markers, and start/stop Chrono trgs, as well as seq states.
      //if(fStartDumpChronoTrgs.at(seqNum).size()>0)
      //std::cout << "runTime = " << runTime << ", lastStart = " << fStartDumpChronoTrgs.at(seqNum).back() << "\n";
      //if(fStopDumpChronoTrgs.at(seqNum).size()>0)
      //std::cout << "runTime = " << runTime << ", lastStart = " << fStopDumpChronoTrgs.at(seqNum).back() << "\n";
      if(fStartDumpMarkers.at(seqNum).size()>0 && fStopDumpMarkers.at(seqNum).size()>0 
         && fStartDumpChronoTrgs.at(seqNum).size()>0 && fStopDumpChronoTrgs.at(seqNum).size()>0 
         && fSeqStates.at(seqNum).size()>0 && (fTPCEventBuffer.size()>0 or fNoTPCAnalysis) && fChronoEventBuffer.size()>0)
      {
         //This check waits 2 seconds after the last dump marker to flush, just to make sure there is nothing in the next packet...
         if(runTime - fStartDumpChronoTrgs.at(seqNum).at(fStartDumpChronoTrgs.at(seqNum).size()-1) < fPacketBuffer)
         {
            //std::cout << "runTime = " << runTime << ", lastStart = " << fStartDumpChronoTrgs.at(seqNum).at(fStartDumpChronoTrgs.at(seqNum).size()-1) << "\n";
            return false;
         }
         //This check waits 2 seconds after the last dump marker to flush, just to make sure there is nothing in the next packet...
         if(runTime - fStopDumpChronoTrgs.at(seqNum).at(fStopDumpChronoTrgs.at(seqNum).size()-1) < fPacketBuffer)
         {
            //std::cout << "runTime = " << runTime << ", lastStart = " << fStopDumpChronoTrgs.at(seqNum).at(fStopDumpChronoTrgs.at(seqNum).size()-1) << "\n";
            return false;
         }
         //Lets do checks... First: 
         if(fStartDumpChronoTrgs.at(seqNum).at(0) > fStopDumpChronoTrgs.at(seqNum).at(0))
         {
            //Dump has negative length? We are checking this earlier so in theory shouldn't need this?
            //ONE OF THESE IS BAD, BUT IS IT THE START OR THE STOP?
            if(fStartDumpChronoTrgs.at(seqNum).size()>fStopDumpChronoTrgs.at(seqNum).size()) //if they're equal we want to remove the stop, this '>' is intended.
            {
               //We have more starts than stops....
               //Throwing away a start....
               std::cout << "SPILL WARNING: Dump with negative length detected, throwing away the start marker (I think it's fake).\n";
               fStartDumpChronoTrgs.at(seqNum).pop_front();
               fFuzzyStart.at(seqNum).at(0) = 1; //Since we've thrown away a start this front is fuzzy.
            }
            else
            {
               std::cout << "SPILL WARNING: Dump with negative length detected, throwing away the stop marker (I think it's fake).\n";
               fStopDumpChronoTrgs.at(seqNum).pop_front();
               fFuzzyStop.at(seqNum).at(0) = 1; //Since we've thrown away a stop this front is fuzzy.
            }
            //We'll try again later.
            std::cout << "We'll try again later \n";
            return false;
         }
         //std::cout << "size tpc = " << fTPCEventBuffer.size() << ", ";
         //std::cout << "runtime = " << fTPCEventBuffer.back()->GetRunTime() << "\n";
         //std::cout << "size chrono = " << fStopDumpChronoTrgs.at(seqNum).size() << ", ";
         //std::cout << "runtime = " << fStopDumpChronoTrgs.at(seqNum).at(0) << "\n";
         if(!fNoTPCAnalysis and fTPCEventBuffer.back()->GetRunTime() < fStopDumpChronoTrgs.at(seqNum).at(0))
         {
            //EventBuffer hasnt caught up yet, try again later...
            //std::cout << "SPILL INFO: SVD Event buffer has not caught up yet: seq: " << seqNum << ": " << fTPCEventBuffer.back()->GetRunTime() << " < " << fStopDumpChronoTrgs.at(seqNum).at(0) << "\n";
            return false;
         }
         if(fChronoEventBuffer.back()->GetRunTime() < fStopDumpChronoTrgs.at(seqNum).at(0))
         {
            //EventBuffer hasnt caught up yet, try again later...
            //std::cout << "SPILL INFO: Chrono Event buffer has not caught up yet: seq: " << seqNum << ": " << fChronoEventBuffer.back()->GetRunTime() << " < " << fStopDumpChronoTrgs.at(seqNum).at(0) << "\n";
            return false;
         }

         //This checks we dont have a mixing desync...
         if(fStartDumpMarkers.at(seqNum).at(0).fDescription == "\"Mixing\"")
         {
            if(fMixingChronoTrgs.size() > 0 && abs(fStartDumpChronoTrgs.at(seqNum).at(0) - fMixingChronoTrgs.at(0)) > 0.001) //1ms? Arbitrary choice...
            {
               //std::cout << "SPILL INFO: Mixing spill desync: " << fStartDumpChronoTrgs.at(seqNum).at(0) << " vs " << fMixingChronoTrgs.at(0) << "\n";

               //We probably need 2 seperate ways to handle this depending on whether we missed it or already built it...
               if(fStartDumpChronoTrgs.at(seqNum).at(0) < fMixingChronoTrgs.at(0))
               {
                  //Since the current Chrono trigger is before of the sequencer spill, we remove the Chrono triggers so they can match again.
                  //fStartDumpChronoTrgs.at(seqNum).pop_front();
                  //fStopDumpChronoTrgs.at(seqNum).pop_front();
                  return false;
                  
               }
               if(fStartDumpChronoTrgs.at(seqNum).at(0) > fMixingChronoTrgs.at(0))
               {
                  //Since the current Chrono trigger is after of the sequencer spill, we remove the sequence stuff so they can match again.
                  //fStartDumpMarkers.at(seqNum).pop_front();
                  //fStopDumpMarkers.at(seqNum).pop_front();
                  //fSeqStates.at(seqNum).pop_front();
                  return false;
               }
            }
            else if(fMixingChronoTrgs.size() == 0)
            {
               std::cout << "SPILL WARNING: Trying to build mixing dump but no mixing flag yet? Something has gone bad...\n";
               //return false;
            }
            else
            {
               //fMixingChronoTrgs.pop_front();
            }
         }
         //std::cout << "WE ARE FLUSHING!!!!!\n";
         return true;
      }
      else
      {
          //std::cout << "fStartDumpMarkers size = " << fStartDumpMarkers.at(seqNum).size() << "\n"; 
          //std::cout << "fStopDumpMarkers size = " << fStopDumpMarkers.at(seqNum).size() << "\n"; 
          //std::cout << "fStartDumpChronoTrgs size = " << fStartDumpChronoTrgs.at(seqNum).size() << "\n"; 
          //std::cout << "fStopDumpChronoTrgs size = " << fStopDumpChronoTrgs.at(seqNum).size() << "\n"; 
          //std::cout << "fSeqStates size = " << fSeqStates.at(seqNum).size() << "\n"; 
          //std::cout << "fTPCEventBuffer size = " << fTPCEventBuffer.size() << "\n"; 
          //std::cout << "fChronoEventBuffer size = " << fChronoEventBuffer.size() << "\n";


         //std::cout << "SPILL WARNING: Waiting for queues to fill seq: " << seqNum << "\n";
         return false;
      }

   }

   TAFlowEvent* FlushEvents(int runNumber, TAFlowEvent* /*flow*/, AGSpillFlow* f, int numSeq)
   {
      //We know everything is ready to dump if we're here so lets build some spills...

      //global lock - this is popping the front of the queues while other stuff it reading them. Do not pop the queues while something else is reading.

      //Add the correct time to our dump marker...
      TDumpMarker* startDump = new TDumpMarker(fStartDumpMarkers.at(numSeq).at(0));
      startDump->fRunTime = fStartDumpChronoTrgs.at(numSeq).at(0);

      //Add the correct time to our dump marker....
      TDumpMarker* stopDump = new TDumpMarker(fStopDumpMarkers.at(numSeq).at(0));
      stopDump->fRunTime = fStopDumpChronoTrgs.at(numSeq).at(0);

      //printf("Flushing with start: %.4f, stop: %.4f\n",startDump->fRunTime,stopDump->fRunTime);

      //startDump->Print();
      //stopDump->Print();

      //Create a new pair.
      TDumpMarkerPair<TAGDetectorEvent,TChronoBoardCounter,CHRONO_N_BOARDS>* markerPair = new TDumpMarkerPair<TAGDetectorEvent,TChronoBoardCounter,CHRONO_N_BOARDS>();
      markerPair->AddStartDump(*startDump);
      markerPair->AddStopDump(*stopDump);

      //Add the seq states we pushed back in the Analyze function.
      for(TSequencerState state: fSeqStates.at(numSeq).at(0))
      {
         markerPair->AddState(state);
      }

      //Add SVD events to the spill.
      for(std::shared_ptr<TAGDetectorEvent> event: fTPCEventBuffer)
      {
         if((fStartDumpChronoTrgs.at(numSeq).at(0) < event->GetRunTime()) && (event->GetRunTime() < fStopDumpChronoTrgs.at(numSeq).at(0)))
         {
            markerPair->AddTPCEvent(*event);
         }
      }

      //Add Chrono events to the spill.
      for(std::shared_ptr<TCbFIFOEvent> c: fChronoEventBuffer)
      {
         if((fStartDumpChronoTrgs.at(numSeq).at(0) < c->GetRunTime()) && (c->GetRunTime() < fStopDumpChronoTrgs.at(numSeq).at(0)))
         {
            int board_no = TChronoChannel::CBMAP.at(c->fBoard);
            TChronoBoardCounter counter(c,board_no);
            markerPair->AddScalerEvent(counter);
         }
      }

      //Build the spill object
      TAGSpill* newSpill = new TAGSpill(runNumber, markerPair);
      f->spill_events.push_back(newSpill);

      //Check whether it has a fuzzy start/stop.
      if(fFuzzyStart.at(numSeq).at(0))
      {
         std::cout << "SPILL WARNING: Spill " << newSpill->fName << "[" << newSpill->fRepetition << "] has fuzzy start time. Do not trust me. You have been warned. 😈\n";
         newSpill->fFuzzyStart = true;
      }
      if(fFuzzyStop.at(numSeq).at(0))
      {
         std::cout << "SPILL WARNING: Spill " << newSpill->fName << "[" << newSpill->fRepetition << "] has fuzzy stop time. Do not trust me. You have been warned. 😈\n";
         newSpill->fFuzzyStop = true;
      }

      //Spill has 0 states? This probably shouldn't happen (maybe the sequencer genuinely does nothing during this spill?)
      if(markerPair->fStates.size() == 0)
      {
         std::cout << "SPILL WARNING: fStates.size() = " << markerPair->fStates.size() << ". Are you sure?\n";
      }

      {
         //Analyze function is looping over these sometimes, do not pop_front when it is doing that.
         std::lock_guard<std::mutex> locallock(moduleLock);
         fStartDumpMarkers.at(numSeq).pop_front();
         fStopDumpMarkers.at(numSeq).pop_front();
         fStartDumpChronoTrgs.at(numSeq).pop_front();
         fStopDumpChronoTrgs.at(numSeq).pop_front();
         fFuzzyStart.at(numSeq).pop_front();
         fFuzzyStop.at(numSeq).pop_front();
         fSeqStates.at(numSeq).pop_front();
      }

      return f;
   }

   void ClearBuffers()
   {
      int tpc_cleared = 0;
      int cb_cleared = 0;
      double minStartDump = std::numeric_limits<double>::max();
      for (auto starts: fStartDumpChronoTrgs) {
         for (double t: starts) {
            if (t<minStartDump) minStartDump = t;
         }
     }
      //if(minStartDump == std::numeric_limits<double>::max())
      //{
      //   std::cout << "SPILL INFO: No stop value found, clearing whole queue.\n";
      //}
      double cleartothistime = minStartDump;
      if (minStartDump==std::numeric_limits<double>::max()) {
         // If there's no start dumps queued up, use the time of the latest chrono/TPC event instead
         if (fTPCEventBuffer.size()>0) {
            cleartothistime = fTPCEventBuffer.back()->GetRunTime();
         }
         if (fChronoEventBuffer.size()>0) {
            if (fChronoEventBuffer.back()->GetRunTime()<cleartothistime)
            cleartothistime = fChronoEventBuffer.back()->GetRunTime();
         }
      }
      cleartothistime -= fBufferClearWait; // Keep an extra fBufferClearWait seconds in case things come out of order
      
      //Done with everything, lets release.
      //std::cout << "Cleaning events up to t = " << lastStartDumpTime << "\n"; 
      while(fTPCEventBuffer.size())
      {
         //std::cout << "Have event = " << fTPCEventBuffer.at(0)->GetRunTime() << std::endl;
         if(fTPCEventBuffer.at(0)->GetRunTime() < cleartothistime) {
            fTPCEventBuffer.pop_front();
            tpc_cleared++;
         }
         else
            break;
      }
      while(fChronoEventBuffer.size())
      {
         if(fChronoEventBuffer.at(0)->GetRunTime() < cleartothistime) {
            fChronoEventBuffer.pop_front();
            cb_cleared++;
         }
         else
            break;
      }
      //if (tpc_cleared+cb_cleared>0) {
         //printf("ClearBuffers cleared %d tpc events and %d chrono events which were before %.4f\n",tpc_cleared,cb_cleared,lastStartDumpTime);
      //}

   }

   int NewSequencerTriggerCheck(int a, double seqstarttime)
   {
      //If we're in here theres been a new sequencer trigger, lets count hot many old dumps and markers are still around.
      size_t numOldStartTrigsLeft = 0;
      size_t numOldStopTrigsLeft = 0;
      size_t numOldStartDumpMarksLeft = 0;
      size_t numOldStopDumpMarksLeft = 0;

      //Count how may Chrono triggers are less than the Seq start
      for(size_t k=0; k<fStartDumpChronoTrgs.at(a).size(); k++)
      {
         if(fStartDumpChronoTrgs.at(a).at(k) < seqstarttime)
            numOldStartTrigsLeft++;
         else
            break;
      }
      //std::cout << "SPILL INFO: NUM START TRIGS LEFT = " << numOldStartTrigsLeft << "\n";

      //Count how may Chrono triggers are less than the Seq start
      for(size_t k=0; k<fStopDumpChronoTrgs.at(a).size(); k++)
      {
         if(fStopDumpChronoTrgs.at(a).at(k) < seqstarttime)
            numOldStopTrigsLeft++;
         else
            break;                                 
      }
      //std::cout << "SPILL INFO: NUM STOP TRIGS LEFT = " << numOldStopTrigsLeft << "\n";
      
      //Count how may Chrono triggers are less than the Seq start
      for(size_t k=0; k<fStartDumpMarkers.at(a).size(); k++)
      {
         if( fStartDumpMarkers.at(a).at(k).fSequenceCount < fCurrentSeq[a])
            numOldStartDumpMarksLeft++;
         else
            break;                                 
      }
      //std::cout << "SPILL INFO: NUM START MARKS LEFT = " << numOldStartDumpMarksLeft << "\n";
   
      //Count how may Chrono triggers are less than the Seq Stop
      for(size_t k=0; k<fStopDumpMarkers.at(a).size(); k++)
      {
         if( fStopDumpMarkers.at(a).at(k).fSequenceCount < fCurrentSeq[a])
            numOldStopDumpMarksLeft++;
         else
            break;                                 
      }
      //std::cout << "SPILL INFO: NUM Stop MARKS LEFT = " << numOldStopDumpMarksLeft << "\n";
      
      //Check all 4 are identical....
      if( numOldStartTrigsLeft == numOldStopTrigsLeft && numOldStopTrigsLeft == numOldStartDumpMarksLeft && numOldStartDumpMarksLeft == numOldStopDumpMarksLeft)
      {
         //THis is great, we have no problem...
         //std::cout << "SPILL INFO: New seq but everything looks good: " << numOldStartTrigsLeft << " \n";
         return 0;
      }
      else if(numOldStartTrigsLeft == numOldStopTrigsLeft && numOldStartDumpMarksLeft == numOldStopDumpMarksLeft)
      {
         std::cout << "SPILL WARNING: New seq but one is bigger, we need to handle this... " << numOldStartTrigsLeft << numOldStopTrigsLeft << numOldStartDumpMarksLeft << numOldStopDumpMarksLeft << "\n";
         //So here we have both start and stop Chrono triggers, and dump markers, but they dont match. We flush all missing ones.
         int diff = numOldStopTrigsLeft - numOldStartDumpMarksLeft;
         for(int k=0; k<diff; k++)
         {
            if(diff>0) //if diff > 0 there are more Chrono triggers than dump markers, so lets flush the Chrono trigs until they match.
            {
               //fStartDumpChronoTrgs.at(a).pop_front();
               //fStopDumpChronoTrgs.at(a).pop_front();
            }
            else if(diff<0) ////if diff < 0 there are more dump markers than Chrono triggers, so lets flush the dump marks until they match.
            {
               //fStartDumpChronoTrgs.at(a).pop_front();
               //fStopDumpChronoTrgs.at(a).pop_front();
            }

         }
         //This is a pretty bad situation to be in, we need to warn the user...
         return -1;
      }
      else
      {
         std::cout << "SPILL WARNING: This really should not happen, we have a disaster... " << numOldStartTrigsLeft << numOldStopTrigsLeft << numOldStartDumpMarksLeft << numOldStopDumpMarksLeft << "\n";
         return -2;
      }
   }

};

class DumpMakerModuleFactory: public TAFactory
{
public:
   DumpMakerModuleFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("DumpMakerModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
      }
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("DumpMakerModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("DumpMakerModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new DumpMakerModule(runinfo, &fFlags);
   }
};

static TARegister tar(new DumpMakerModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
