//
// AG module for adding online MVA object to the flow.
//
// L GOLINO
//

#include <stdio.h>

#include "manalyzer.h"
#include "midasio.h"

#include "AnalysisFlow.h"
#include "RecoFlow.h"


#include "TApplication.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"

#include "generalizedspher.h"
class DumperFlags
{
public:
   bool fPrint = false;
   double fMagneticField = 1.;

};

class Dumper: public TARunObject
{
private:
   OnlineAGMVAStruct* onlineVars;
   
  //TString gVarList="nhits,residual,r,S0rawPerp,S0axisrawZ,phi_S0axisraw,nCT,nGT,tracksdca,curvemin,curvemean,lambdamin,lambdamean,curvesign,";
  
public:
   DumperFlags* fFlags;
   bool fTrace = false;
   
   
   Dumper(TARunInfo* runinfo, DumperFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Dumper Module";
#endif
      if (fTrace)
         printf("Dumper::ctor!\n");
   }

   ~Dumper()
   {
      if (fTrace)
         printf("Dumper::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("Dumper::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("Dumper::EndRun, run %d\n", runinfo->fRunNo);
   }
   
   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("Dumper::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }
  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* /*flags*/, TAFlowEvent* flow)
   {
    AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
    if (!agfe)
    {
        return flow;
    }
    TStoreEvent* storeEvent=agfe->fEvent;
    if (!storeEvent)
    {
        return flow;
    }
      
    if (fTrace)
        printf("Dumper::AnalyzeFlowEvent, run %d\n", runinfo->fRunNo);

    //Here create online MVA Struct and return to flow. Catch this later in offline_ag_mva_module,cxx
    onlineVars=new OnlineAGMVAStruct();

    //Here populate onlineVars

    //Either save all three or pick one?
    onlineVars->nhits=storeEvent->GetNumSPHits();                 // - Not implemented. Need a way to bring number of wire hits and stuff forward from the AgFlowEvent.
    onlineVars->nhits=storeEvent->GetNumPADHits();                // - Not implemented. Need a way to bring number of wire hits and stuff forward from the AgFlowEvent.
    onlineVars->nhits=storeEvent->GetNumAWHits();                 // - Not implemented. Need a way to bring number of wire hits and stuff forward from the AgFlowEvent.

    const TVector3 vtx = storeEvent->GetVertex();
    onlineVars->r = vtx.Perp();
    onlineVars->phi = vtx.Phi();

    ////Do I want GetUsedHelices() or fStoreHelixArray?? i  assume GetUsedHelices() for now
    //Perhaps we only want to use helices with a certain status? For now I will all helices here.
    Int_t nAT =  storeEvent->GetHelixArray()->GetEntries(); // all tracks
    onlineVars->nCT = 0;
    Int_t nraw = 0;  

    std::vector<double> velxraw;
    std::vector<double> velyraw;
    std::vector<double> velzraw;  
    for (int i = 0; i< nAT ; ++i)
    {
        TStoreHelix* helix = (TStoreHelix*)storeEvent->GetHelixArray()->At(i);
        if (!helix) continue;
        //Double_t fc = helix->GetC();
        Double_t fphi0 = helix->GetPhi0();
        Double_t fLambda = helix->GetLambda();
        //Double_t s=0.; // calculates velx,y,z at POCA
        // special case for s = 0
        // Int_t HelixHits=helix->GetNumberOfPoints(); //Unused


        // onlineVars seems to be true all the time... please check - Joe... Has onlineVars been checked for A2? Maybe I should check for AG too.
        //if ( helix->GetStatus()==1 ) - Not sure if we want onlineVars or the line below??
        //std::cout << "Helix status in loop for RAW values = " << helix->GetStatus() << std::endl;
        //if ( helix->GetStatus()==1 || helix->GetStatus()==2 || helix->GetStatus()==3 )
        if ( helix->GetStatus()>=0 )
        {
        ++nraw; // == ntracks
        velxraw.push_back( - TMath::Sin(fphi0));
        velyraw.push_back( TMath::Cos(fphi0)) ;
        velzraw.push_back( fLambda );
        onlineVars->nCT++;
        }
    }

      //Only use the trivial line if there were more than 2 space points (and therefore ideally a reasonable line)
      if(storeEvent->GetNumberOfPoints() >= 2)
         onlineVars->totalresidual = storeEvent->GetTrivialHelix().CalculateResiduals();
      else //Otherwise just use default value. 
         onlineVars->totalresidual = ALPHAg::kLargeNegativeUnknown;

      onlineVars->meantotalresidual = 0;
      size_t numResiduals = storeEvent->GetTrivialHelix().GetResidualsVector().size();
      for (size_t i = 0; i<  numResiduals; ++i)
      {
         //Also add to a mean residual member
         onlineVars->meantotalresidual+=storeEvent->GetTrivialHelix().GetResidualsVector().at(i);
      }
      onlineVars->meantotalresidual = numResiduals>0?onlineVars->meantotalresidual/numResiduals:ALPHAg::kLargeNegativeUnknown;
      
      //Only use the trivial line if there were more than 2 space points (and therefore ideally a reasonable line)
      if(storeEvent->GetNumberOfPoints() >= 2)
         onlineVars->usedresidual = storeEvent->GetUsedTrivialHelix().CalculateResiduals();
      else //Otherwise just use default value. 
         onlineVars->usedresidual = ALPHAg::kLargeNegativeUnknown;

      onlineVars->meanusedresidual = 0;
      numResiduals = storeEvent->GetUsedTrivialHelix().GetResidualsVector().size();
      for (size_t i = 0; i<  numResiduals; ++i)
      {
         //Also add to a mean residual member
         onlineVars->meanusedresidual+=storeEvent->GetUsedTrivialHelix().GetResidualsVector().at(i);
      }
      onlineVars->meanusedresidual = numResiduals>0?onlineVars->meanusedresidual/numResiduals:ALPHAg::kLargeNegativeUnknown;


    //Set tracks DCA (distance of closest approach)
    onlineVars->tracksdca = storeEvent->GetDCA();


    std::vector<double> velx;
    std::vector<double> vely;
    std::vector<double> velz;
    // alpha event part

    ////Do I want GetUsedHelices() or fStoreHelixArray?? i  assume GetUsedHelices() for now
    //Perhaps we only want to use helices with a certain status? For now I will all helices here.
    Int_t nGTL =  storeEvent->GetUsedHelices()->GetEntries(); // all (used) tracks

    onlineVars->nGT = 0;
    onlineVars->curvemin=9999.;
    onlineVars->curvemean=0.;
    onlineVars->lambdamin=9999.;
    onlineVars->lambdamean=0;
    onlineVars->curvesign=0;
    for (int i = 0; i< nGTL ; ++i)
    {
        TStoreHelix* helix = (TStoreHelix*)storeEvent->GetUsedHelices()->At(i);
        //if(aehlx->GetHelixStatus()<0) continue;
        Double_t fc = helix->GetC();
        Double_t fphi0 = helix->GetPhi0();
        Double_t fLambda = helix->GetLambda();

        //Unused
        //Double_t s=0.; // calculates velx,y,z at POCA
        // special case for s = 0
        velx.push_back( - TMath::Sin(fphi0) );
        vely.push_back( TMath::Cos(fphi0) ) ;
        velz.push_back( fLambda );

        //std::cout << "Helix status in loop for CORRECTED values = " << helix->GetStatus() << std::endl;
        // select good helices, after removal of duplicates
        //if (helix->GetStatus()==1) onlineVars or line below? 
        if (helix->GetStatus()==2 || helix->GetStatus()==3) 
        //Here we are doing some checking of the helix?? Perhaps onlineVars should be done above also.
        {
        onlineVars->nGT++;
        onlineVars->curvemin= fabs(fc)>onlineVars->curvemin? onlineVars->curvemin:fabs(fc);
        onlineVars->lambdamin= fabs(fLambda)>onlineVars->lambdamin? onlineVars->lambdamin:fabs(fLambda);
        onlineVars->curvemean+=fabs(fc);
        onlineVars->lambdamean+=fabs(fLambda);
        onlineVars->curvesign+=(fc>0)?1:-1;
        }
    }
    if(onlineVars->nGT>0){
        onlineVars->lambdamean/=onlineVars->nGT;
        onlineVars->curvemean/=onlineVars->nGT;
    }
    Double_t S0axisrawX = -99.;
    //Unused in online_mva
    Double_t S0axisrawY = -99.;
    onlineVars->S0axisrawZ = -99.;

    if(nraw>0)
    {
        TVector3* S0axisraw;
        TVector3* S0valuesraw;
        sphericity(velxraw, velyraw, velzraw, 0, &S0axisraw, &S0valuesraw); // generalizedspher.h
        onlineVars->S0rawPerp = S0valuesraw->Perp();

        //Unused in online_mva
        S0axisrawX = S0axisraw->X();
        //Unused
        S0axisrawY = S0axisraw->Y();
        onlineVars->S0axisrawZ = S0axisraw->Z();
        onlineVars->phi_S0axisraw = TMath::ACos(S0axisrawY/TMath::Sqrt(S0axisrawX*S0axisrawX+S0axisrawY*S0axisrawY));
        delete S0axisraw;
        delete S0valuesraw;
    }

      flow=new AGOnlineMVAFlow(flow,onlineVars);
      return flow; 
  }
};

class DumperFactory: public TAFactory
{
public:
   DumperFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("DumperFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
         if( args[i] == "--Bfield" )
         {
            fFlags.fMagneticField = atof(args[++i].c_str());
         }
      }
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("DumperFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("DumperFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new Dumper(runinfo, &fFlags);
   }
};

static TARegister tar(new DumperFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
