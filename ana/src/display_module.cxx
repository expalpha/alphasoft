//
// display_module.cxx
//
// display of TPC data
//

#include <stdio.h>
#include <iostream>

#include "manalyzer.h"
#include "midasio.h"

#include "AgFlow.h"
#include "RecoFlow.h"

#include "TStoreEvent.hh"
#include "TBarMVAVars.h"
#include "Aged.h"
#include "X11/Intrinsic.h"

#define DELETE(x) if (x) { delete (x); (x) = NULL; }

#define MEMZERO(p) memset((p), 0, sizeof(p))

using std::cout;
using std::cerr;
using std::endl;

class DisplayFlags
{
public:
   bool fBatch;
   bool fForce;
   int fShowBarTypes;

   enum operator_type {lessthan, lessthanequal, equal, greaterthanequal, greaterthan, notequal};
   std::map<std::string,operator_type> operator_map = {{"lt",lessthan},{"lte",lessthanequal},
         {"e",equal},{"=",equal},{"gte",greaterthanequal},{"gt",greaterthan},{"ne",notequal},
         {"<",lessthan},{"<=",lessthanequal},{"==",equal},{">",greaterthan},{">=",greaterthanequal},
         {"!=",notequal}};

   std::vector<std::string> skip_var;
   std::vector<operator_type> skip_operator;
   std::vector<double> skip_val;

   DisplayFlags():fBatch(true),fForce(false),fShowBarTypes(1)
   {}
   ~DisplayFlags() {}
};

class DisplayRun: public TARunObject
{
private:
   Aged *aged;
   DisplayFlags* fFlags;

   enum skip_var_type {kSkipVertexStatus, kSkipEventNumber, kSkipNPoints, kSkipNTracks, kSkipNTracksUsed, kSkipNTracksUnused,
                        kSkipNPads, kSkipNWires, kSkipNPointsPerTrack, kSkipVertexR, kSkipVertexX, kSkipVertexY, kSkipVertexZ, kSkipVertexCut, kSkipVertexChi2,
                        kSkipHelixMaxChi2R, kSkipHelixMaxChi2Z, kSkipHelixMaxC, kSkipHelixMinC, kSkipHelixMaxD, kSkipHelixMinD,
                        kSkipHelixMaxZ0, kSkipHelixMinZ0,
                        kSkipNBarsComplete, kSkipNBarsHalfComplete, kSkipNBars, kSkipNBarClusters,
                        kSkipNBarsCompleteMatched, kSkipNBarsMatched,
                        kSkipNBarsCompleteUnmatched, kSkipNBarsUnmatched,
                        kSkipNNegative, kSkipNFlatTop, kSkipNFlatBottom, kSkipBVOnFire, kSkipBVHitType,
                        kSkipADCCh, kSkipTDCCh, kSkipEndCh, kSkipNDoubleADC, kSkipNDoubleTDC, kSkipNDoubleHits, kSkipNDoubleAny,
                        kSkipCombinedADC, kSkipBVMinZ, kSkipBVMaxZ, kSkipBVMVA, kSkipPileUp, kBadADCFit};

   std::vector<skip_var_type> fSkipVarType;
   std::map<int, int> fSkipVarVal;
   std::map<int, std::string> fSkipVarString;
   std::vector<DisplayFlags::operator_type> fSkipOperatorType;
   std::vector<double> fSkipCutVal;

public:

   DisplayRun(TARunInfo* runinfo, DisplayFlags* f)
      : TARunObject(runinfo), aged(0), fFlags(f)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Display Module";
#endif
      printf("DisplayRun::ctor!\n");
   }

   ~DisplayRun()
   {
      DELETE(aged);
      printf("DisplayRun::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      printf("DisplayRun::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      
      time_t run_start_time = 0;
      #ifdef INCLUDE_VirtualOdb_H
      run_start_time = runinfo->fOdb->odbReadUint32("/Runinfo/Start time binary", 0, 0);
      #endif
      #ifdef INCLUDE_MVODB_H
      runinfo->fOdb->RU32("Runinfo/Start time binary",(uint32_t*) &run_start_time);
      #endif
      printf("ODB Run start time: %d: %s", (int)run_start_time, ctime(&run_start_time));
      
      // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      // ********* CREATE your display here
      // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      if( !fFlags->fBatch && !aged ) 
         {
            printf("New Aged ");
            aged = new Aged();
            aged->SetShowBarTypes(fFlags->fShowBarTypes);
         }
      printf("Parsing aged --skipto flags...\n");
      SetUpSkipConditions();
   }

   void EndRun(TARunInfo* runinfo)
   {
      printf("DisplayRun::EndRun, run %d\n", runinfo->fRunNo);
      time_t run_stop_time = 0;
      #ifdef INCLUDE_VirtualOdb_H
      run_stop_time = runinfo->fOdb->odbReadUint32("/Runinfo/Stop time binary", 0, 0);
      #endif
      #ifdef INCLUDE_MVODB_H
      runinfo->fOdb->RU32("Runinfo/Stop time binary", (uint32_t*) &run_stop_time);
      #endif
      printf("ODB Run stop time: %d: %s", (int)run_stop_time, ctime(&run_stop_time));

      // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      // ********* DESTROY your display here
      // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      DELETE(aged);
   }

   void PauseRun(TARunInfo* runinfo)
   {
      printf("DisplayRun::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      printf("DisplayRun::ResumeRun, run %d\n", runinfo->fRunNo);
   }

   //   TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow)
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      if( fFlags->fBatch )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      
      //printf("DisplayModule::Analyze, run %d\n",runinfo->fRunNo);

      AgEventFlow *ef = flow->Find<AgEventFlow>();

      if (!ef || !ef->fEvent)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      
      AgEvent* age = ef->fEvent;
      
      if( !age->a16 )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }

/*      if( !age->feam && !fFlags->fForce )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
*/      
      // DISPLAY low-level stuff here
      AgSignalsFlow* SigFlow = flow->Find<AgSignalsFlow>();
      if( !SigFlow )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      
      // DISPLAY high-level stuff here
      AgAnalysisFlow* analysis_flow = flow->Find<AgAnalysisFlow>();
      if( !analysis_flow || !analysis_flow->fEvent )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      
      // DISPLAY BSC waveform stuff here
      AgEventFlow* evtFlow = flow->Find<AgEventFlow>();
      if( !evtFlow || !evtFlow->fEvent )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      
      // DISPLAY BSC stuff here
      AgBarEventFlow* bar_flow = flow->Find<AgBarEventFlow>();
      if( !bar_flow || !bar_flow->BarEvent)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }

      //printf("DisplayRun::Analyze event no %d, FlowEvent no %d, BarEvent no %d\n", age->counter,analysis_flow->fEvent->GetEventNumber(),bar_flow->BarEvent-> GetID());


      //analysis_flow->fEvent->Print();

      if (aged) {
         if (!ShouldSkipEvent(analysis_flow->fEvent,bar_flow->BarEvent)) {
            // flags=aged->ShowEvent(age,analysis_flow,SigFlow,bar_flow,flags,runinfo);
            std::map<int,std::vector<double>*> BVwf = GetBVWaveforms(evtFlow->fEvent);
            flags=aged->ShowEvent(*analysis_flow->fEvent, *bar_flow->BarEvent, SigFlow->AWwf, SigFlow->PADwf, BVwf, runinfo->fRunNo, flags);
            //printf("Aged::ShowEvent is %d\n",*flags);
         }
      }
      return flow;
   }

   void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event)
   {
      printf("DisplayRun::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
   }

   std::map<int,std::vector<double>*> GetBVWaveforms(AgEvent* evt) {
      std::map<int,std::vector<double>*> waveforms;
      if (!evt) return waveforms;
      const Alpha16Event* data = evt->a16;
      if (!data) return waveforms;
      std::vector<Alpha16Channel*> channels = data->hits;
      for (auto c: channels) {
         std::vector<double>* v = new std::vector<double>; //FIXME memory leak but idk how to iterate over a map to delete these
         for (int s: c->adc_samples) v->push_back(double(s));
         waveforms[c->bsc_bar]=v;
      }
      return waveforms;
   }

   // Do all the string comparisons to parse the --skipto flags at the start of the run.
   void SetUpSkipConditions() {

      // Comparison operator
      for (unsigned int i=0; i<fFlags->skip_operator.size(); i++) {
         fSkipOperatorType.push_back(fFlags->skip_operator[i]);
      }

      // Value to cut on
      for (unsigned int i=0; i<fFlags->skip_val.size(); i++) {
         fSkipCutVal.push_back(fFlags->skip_val[i]);
      }

      // Variable to cut on
      for (unsigned int i=0; i<fFlags->skip_var.size(); i++) {
         // BV bar event variables
         if (fFlags->skip_var[i]=="n_bars_complete") fSkipVarType.push_back(kSkipNBarsComplete);
         if (fFlags->skip_var[i]=="n_bars_half_complete") fSkipVarType.push_back(kSkipNBarsHalfComplete);
         if (fFlags->skip_var[i]=="n_bars") fSkipVarType.push_back(kSkipNBars);
         if (fFlags->skip_var[i]=="n_bar_clusters") fSkipVarType.push_back(kSkipNBarClusters);
         if (fFlags->skip_var[i]=="n_bars_matched") fSkipVarType.push_back(kSkipNBarsMatched);
         if (fFlags->skip_var[i]=="n_bars_complete_matched") fSkipVarType.push_back(kSkipNBarsCompleteMatched);
         if (fFlags->skip_var[i]=="n_bars_unmatched") fSkipVarType.push_back(kSkipNBarsUnmatched);
         if (fFlags->skip_var[i]=="n_bars_complete_unmatched") fSkipVarType.push_back(kSkipNBarsCompleteUnmatched);
         if (fFlags->skip_var[i]=="bv_on_fire") fSkipVarType.push_back(kSkipBVOnFire);
         if (fFlags->skip_var[i]=="n_negative") fSkipVarType.push_back(kSkipNNegative);
         if (fFlags->skip_var[i]=="n_flat_top") fSkipVarType.push_back(kSkipNFlatTop);
         if (fFlags->skip_var[i]=="n_flat_bottom") fSkipVarType.push_back(kSkipNFlatBottom);
         if (fFlags->skip_var[i]=="n_double_any") fSkipVarType.push_back(kSkipNDoubleAny);
         if (fFlags->skip_var[i]=="n_double_adc") fSkipVarType.push_back(kSkipNDoubleADC);
         if (fFlags->skip_var[i]=="n_double_tdc") fSkipVarType.push_back(kSkipNDoubleTDC);
         if (fFlags->skip_var[i]=="n_double_hits") fSkipVarType.push_back(kSkipNDoubleHits);
         if (fFlags->skip_var[i]=="combined_amp") fSkipVarType.push_back(kSkipCombinedADC);
         if (fFlags->skip_var[i]=="bv_min_z") fSkipVarType.push_back(kSkipBVMinZ);
         if (fFlags->skip_var[i]=="bv_max_z") fSkipVarType.push_back(kSkipBVMaxZ);
         if (fFlags->skip_var[i]=="pileup") fSkipVarType.push_back(kSkipPileUp);
         if (fFlags->skip_var[i]=="bad_adc_fit") fSkipVarType.push_back(kBadADCFit);
         if (fFlags->skip_var[i].substr(0,7)=="bv_mva_") {
            fSkipVarString[int(fSkipVarType.size())] = fFlags->skip_var[i].substr(7);
            fSkipVarType.push_back(kSkipBVMVA);
         }
         if (fFlags->skip_var[i].substr(0,7)=="tdc_ch_") {
            fSkipVarVal[int(fSkipVarType.size())] = std::stoi(fFlags->skip_var[i].substr(7));
            fSkipVarType.push_back(kSkipTDCCh);
         }
         if (fFlags->skip_var[i].substr(0,7)=="adc_ch_") {
            fSkipVarVal[int(fSkipVarType.size())] = std::stoi(fFlags->skip_var[i].substr(7));
            fSkipVarType.push_back(kSkipADCCh);
         }
         if (fFlags->skip_var[i].substr(0,7)=="end_ch_") {
            fSkipVarVal[int(fSkipVarType.size())] = std::stoi(fFlags->skip_var[i].substr(7));
            fSkipVarType.push_back(kSkipEndCh);
         }
         if (fFlags->skip_var[i].substr(0,12)=="bv_hit_type_") {
            fSkipVarVal[int(fSkipVarType.size())] = std::stoi(fFlags->skip_var[i].substr(12));
            fSkipVarType.push_back(kSkipBVHitType);
         }

         // TPC event/vertex variables
         if (fFlags->skip_var[i]=="vertex_status") fSkipVarType.push_back(kSkipVertexStatus);
         if (fFlags->skip_var[i]=="event_number") fSkipVarType.push_back(kSkipEventNumber);
         if (fFlags->skip_var[i]=="n_points") fSkipVarType.push_back(kSkipNPoints);
         if (fFlags->skip_var[i]=="n_tracks") fSkipVarType.push_back(kSkipNTracks);
         if (fFlags->skip_var[i]=="n_tracks_used") fSkipVarType.push_back(kSkipNTracksUsed);
         if (fFlags->skip_var[i]=="n_tracks_unused") fSkipVarType.push_back(kSkipNTracksUnused);
         if (fFlags->skip_var[i]=="n_pads") fSkipVarType.push_back(kSkipNPads);
         if (fFlags->skip_var[i]=="n_wires") fSkipVarType.push_back(kSkipNWires);
         if (fFlags->skip_var[i]=="n_points_per_track") fSkipVarType.push_back(kSkipNPointsPerTrack);
         if (fFlags->skip_var[i]=="vertex_x") fSkipVarType.push_back(kSkipVertexX);
         if (fFlags->skip_var[i]=="vertex_y") fSkipVarType.push_back(kSkipVertexY);
         if (fFlags->skip_var[i]=="vertex_z") fSkipVarType.push_back(kSkipVertexZ);
         if (fFlags->skip_var[i]=="vertex_r") fSkipVarType.push_back(kSkipVertexR);
         if (fFlags->skip_var[i]=="helix_max_chi2_r") fSkipVarType.push_back(kSkipHelixMaxChi2R);
         if (fFlags->skip_var[i]=="helix_max_chi2_z") fSkipVarType.push_back(kSkipHelixMaxChi2Z);
         if (fFlags->skip_var[i]=="helix_max_c") fSkipVarType.push_back(kSkipHelixMaxC);
         if (fFlags->skip_var[i]=="helix_min_c") fSkipVarType.push_back(kSkipHelixMinC);
         if (fFlags->skip_var[i]=="helix_max_d") fSkipVarType.push_back(kSkipHelixMaxD);
         if (fFlags->skip_var[i]=="helix_min_d") fSkipVarType.push_back(kSkipHelixMinD);
         if (fFlags->skip_var[i]=="helix_max_z0") fSkipVarType.push_back(kSkipHelixMaxZ0);
         if (fFlags->skip_var[i]=="helix_min_z0") fSkipVarType.push_back(kSkipHelixMinZ0);

         // Vertex cuts
         if (fFlags->skip_var[i].substr(0,3)=="cut") {
            fSkipVarVal[int(fSkipVarType.size())] = std::stoi(fFlags->skip_var[i].substr(3));
            fSkipVarType.push_back(kSkipVertexCut);
         }
         // End of variable declaration
      }
   }

   bool ShouldSkipEvent(TStoreEvent* storeEvt, TBarEvent* barEvt) {
      for (unsigned int i=0; i<fSkipVarType.size(); i++) {
         double val = -999;

         // Event skipping by G.S. - used to run aged and skip to "interesting" events.
         // Add your definition of variables to skip to here.

         if (fSkipVarType[i]==kSkipBVMVA) {
            std::vector<std::string> varNames = TBarMVAVars::GetVarNames();
            auto it = std::find(varNames.begin(),varNames.end(),fSkipVarString[i]);
            if (it==varNames.end()) {
               printf("Warning! Variable %s is not in the list of MVA variables!\n",fSkipVarString[i].data());
            }
            else {
               TBarMVAVars mva(*barEvt);
               mva.CalculateVars();
               val = mva.GetVar(fSkipVarString[i]);
            }
         }

         switch (fSkipVarType[i])
         {
            // BV bar event variables
            case kSkipBVMVA:
               break;
            case kSkipNBarsComplete:
               val = barEvt->GetNumBarsComplete();
               break;
            case kSkipNBarsHalfComplete:
               val = barEvt->GetNumBarsHalfComplete() - barEvt->GetNumBarsComplete();
               break;
            case kSkipNBars:
               val = barEvt->GetNumBarsHalfComplete();
               break;
            case kSkipNBarClusters:
               val = barEvt->GetNumClusters();
               break;
            case kSkipNBarsMatched:
               val = barEvt->GetNumMatchedBars();
               break;
            case kSkipNBarsCompleteMatched:
               val = barEvt->GetNumMatchedBarsComplete();
               break;
            case kSkipNBarsUnmatched:
               val = barEvt->GetNumBars() - barEvt->GetNumMatchedBars();
               break;
            case kSkipNBarsCompleteUnmatched:
               val = barEvt->GetNumBarsComplete() - barEvt->GetNumMatchedBarsComplete();
               break;
            case kSkipBVOnFire:
               val = barEvt->IsInjection();
               break;
            case kSkipPileUp:
               val = barEvt->IsPileUp();
               break;
            case kBadADCFit:
               val = 0;
               for (TBarADCHit& h: barEvt->GetADCHits()) {
                  if (h.GetAmpRaw()>30000 and h.GetAmpFit()<25000) {
                     printf("Bad ADC fit on ch %d..\n",h.GetEndID());
                     val++;
                  }
               }
               break;
            case kSkipNNegative:
               val = 0;
               for (TBarADCHit& h: barEvt->GetADCHits()) {
                  if (h.GetStartsNegative()) {
                     printf("Hit on ch %d starts negative..\n",h.GetEndID());
                     val++;
                  }
               }
               break;
            case kSkipNFlatTop:
               val = 0;
               for (TBarADCHit& h: barEvt->GetADCHits()) {
                  if (h.IsFlatTop()) {
                     printf("Hit on ch %d has flat top..\n",h.GetEndID());
                     val++;
                  }
               }
               break;
            case kSkipNFlatBottom:
               val = 0;
               for (TBarADCHit& h: barEvt->GetADCHits()) {
                  if (h.IsFlatBottom()) {
                     printf("Hit on ch %d has flat bottom..\n",h.GetEndID());
                     val++;
                  }
               }
               break;
            case kSkipBVHitType:
               val = 0;
               for (TBarHit &h: barEvt->GetBarHits()) { if (h.GetHitType()==fSkipVarVal[i]) val++; }
               break;
               
            case kSkipADCCh:
               val = 0;
               for (TBarADCHit &h: barEvt->GetADCHits()) { if (h.GetEndID()==fSkipVarVal[i]) val++; }
               break;
            case kSkipTDCCh:
               val = 0;
               for (TBarTDCHit &h: barEvt->GetTDCHits()) { if (h.GetEndID()==fSkipVarVal[i]) val++; }
               break;
            case kSkipEndCh:
               val = 0;
               for (TBarEndHit &h: barEvt->GetEndHits()) { if (h.GetEndID()==fSkipVarVal[i]) val++; }
               break;
            case kSkipNDoubleHits:
               {
                  std::vector<bool> hits(64,false);
                  int n_repeat = 0;
                  for (TBarHit &h: barEvt->GetBarHits()) {
                     if (!h.IsHalfComplete()) continue;
                     int barID = h.GetBarID();
                     if (barID>=0 and barID<64) {
                        if (hits[barID]) {
                           printf("Repeat hit on barID %d\n",barID);
                           n_repeat++;
                        }
                        hits[barID] = true;
                     }
                  }
                  val = n_repeat;
                  if (n_repeat>0) printf("%d repeats\n",n_repeat);
               }
               break;
            case kSkipNDoubleADC:
               {
                  std::vector<bool> hits(128,false);
                  int n_repeat = 0;
                  for (TBarADCHit &h: barEvt->GetADCHits()) {
                     if (!h.IsGood()) continue;
                     int endID = h.GetEndID();
                     if (endID>=0 and endID<128) {
                        if (hits[endID]) {
                           printf("Repeat hit on endID %d\n",endID);
                           n_repeat++;
                        }
                        hits[endID] = true;
                     }
                  }
                  val = n_repeat;
                  printf("%d repeats\n",n_repeat);
               }
               break;
            case kSkipNDoubleTDC:
               {
                  std::vector<bool> hits(128,false);
                  int n_repeat = 0;
                  for (TBarTDCHit &h: barEvt->GetTDCHits()) {
                     if (h.Is165Reflection()) continue;
                     int endID = h.GetEndID();
                     if (endID>=0 and endID<128) {
                        if (hits[endID]) {
                           printf("Repeat hit on endID %d\n",endID);
                           n_repeat++;
                        }
                        hits[endID] = true;
                     }
                  }
                  val = n_repeat;
               }
               break;
            case kSkipNDoubleAny:
               {
                  std::vector<bool> hits(64,false);
                  int n_repeat = 0;
                  for (TBarHit &h: barEvt->GetBarHits()) {
                     int barID = h.GetBarID();
                     if (barID>=0 and barID<64) {
                        if (hits[barID]) {
                           printf("Repeat hit on barID %d\n",barID);
                           n_repeat++;
                        }
                        hits[barID] = true;
                     }
                  }
                  val = n_repeat;
                  printf("%d repeats\n",n_repeat);
               }
               break;
            case kSkipCombinedADC:
               val = 0;
               for (TBarHit &h: barEvt->GetBarHits()) {
                  if (!h.IsComplete()) continue;
                  double comb = h.GetCombinedAmplitude();
                  if (comb>val) val=comb;
               }     
               break;
            case kSkipBVMinZ:
               val = 100000;
               for (TBarHit &h: barEvt->GetBarHits()) {
                  if (!h.IsComplete()) continue;
                  double z = h.GetZed();
                  if (z<val) val=z;
               }     
               break;
            case kSkipBVMaxZ:
               val = -100000;
               for (TBarHit &h: barEvt->GetBarHits()) {
                  if (!h.IsComplete()) continue;
                  double z = h.GetZed();
                  if (z>val) val=z;
               }     
               break;

            // TPC event/vertex variables
            case kSkipVertexStatus:
               val = storeEvt->GetVertexStatus();
               break;
            case kSkipEventNumber:
               val = storeEvt->GetEventNumber();
               break;
            case kSkipNPoints:
               val = storeEvt->GetNumberOfPoints();
               break;
            case kSkipNTracks:
               val = storeEvt->GetNumberOfTracks();
               break;
            case kSkipNTracksUsed:
               val = storeEvt->GetUsedHelices()->GetEntries();
               break;
            case kSkipNTracksUnused:
               val = storeEvt->GetNumberOfTracks() - storeEvt->GetUsedHelices()->GetEntries();
               break;
            case kSkipNPads:
               val = storeEvt->GetNumberOfPadSigs();
               break;
            case kSkipNWires:
               val = storeEvt->GetNumberOfAwSigs();
               break;
            case kSkipNPointsPerTrack:
               val = storeEvt->GetNumberOfPointsPerTrack();
               break;
            case kSkipVertexX:
               val = storeEvt->GetVertexX();
               break;
            case kSkipVertexY:
               val = storeEvt->GetVertexY();
               break;
            case kSkipVertexZ:
               val = storeEvt->GetVertexZ();
               break;
            case kSkipVertexR:
               val = TMath::Sqrt(storeEvt->GetVertexX()*storeEvt->GetVertexX()+storeEvt->GetVertexY()*storeEvt->GetVertexY());
               break;
            case kSkipVertexChi2:
               val = storeEvt->GetVertexChi2();
               break;

            // TPC track variables
            case kSkipHelixMaxChi2R:
               {
                  double max_chi2_r = -1;
                  for (const auto* helix: *(storeEvt->GetHelixArray())) {
                     const TStoreHelix* h = (const TStoreHelix*)helix;
                     if (h->GetRchi2() > max_chi2_r) max_chi2_r = h->GetRchi2();
                  }
                  val = max_chi2_r;
               }
               break;
            case kSkipHelixMaxChi2Z:
               {
                  double max_chi2_z = -1;
                  for (const auto* helix: *(storeEvt->GetHelixArray())) {
                     const TStoreHelix* h = (const TStoreHelix*)helix;
                     if (h->GetRchi2() > max_chi2_z) max_chi2_z = h->GetZchi2();
                  }
                  val = max_chi2_z;
               }
               break;
            case kSkipHelixMaxC:
               {
                  double max_c = -1;
                  for (const auto* helix: *(storeEvt->GetHelixArray())) {
                     const TStoreHelix* h = (const TStoreHelix*)helix;
                     if (h->GetC() > max_c) max_c = h->GetC();
                  }
                  val = max_c;
               }
               break;
            case kSkipHelixMinC:
               {
                  double min_c = 1e15;
                  for (const auto* helix: *(storeEvt->GetHelixArray())) {
                     const TStoreHelix* h = (const TStoreHelix*)helix;
                     if (h->GetC() < min_c) min_c = h->GetC();
                  }
                  val = min_c;
               }
               break;
            case kSkipHelixMaxD:
               {
                  double max_d = -1;
                  for (const auto* helix: *(storeEvt->GetHelixArray())) {
                     const TStoreHelix* h = (const TStoreHelix*)helix;
                     if (TMath::Abs(h->GetD()) > max_d) max_d = TMath::Abs(h->GetD());
                  }
                  val = max_d;
               }
               break;
            case kSkipHelixMinD:
               {
                  double min_d = 1e15;
                  for (const auto* helix: *(storeEvt->GetHelixArray())) {
                     const TStoreHelix* h = (const TStoreHelix*)helix;
                     if (TMath::Abs(h->GetD()) < min_d) min_d = TMath::Abs(h->GetD());
                  }
                  val = min_d;
               }
               break;
            case kSkipHelixMaxZ0:
               {
                  double max_z0 = -1e15;
                  for (const auto* helix: *(storeEvt->GetHelixArray())) {
                     const TStoreHelix* h = (const TStoreHelix*)helix;
                     if (h->GetZ0() > max_z0) max_z0 = h->GetZ0();
                  }
                  val = max_z0;
               }
               break;
            case kSkipHelixMinZ0:
               {
                  double min_z0 = 1e15;
                  for (const auto* helix: *(storeEvt->GetHelixArray())) {
                     const TStoreHelix* h = (const TStoreHelix*)helix;
                     if (h->GetZ0() < min_z0) min_z0 = h->GetZ0();
                  }
                  val = min_z0;
               }
               break;

            // Cuts variables
            case kSkipVertexCut:
               TAGDetectorEvent* detEvt = new TAGDetectorEvent(storeEvt, barEvt);
               if (fSkipVarVal[i]>=int(detEvt->CutNames().size())) continue;
               val = detEvt->GetOnlinePassCuts(fSkipVarVal[i]);
               break;
         }

         // End of variable definition
         if (val==-999) continue;

         // Compare variable to flagged value
         switch (fSkipOperatorType[i]) {
            case DisplayFlags::lessthan:
               if (val>=fSkipCutVal[i]) {
                  //printf("Skipping event with %s = %.1e (requires < %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
                  return true;
               }
               //printf("  Event with %s = %.1e (requires < %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
               break;
            case DisplayFlags::lessthanequal:
               if (val>fSkipCutVal[i]) {
                  //printf("Skipping event with %s = %.1e (requires <= %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
                  return true;
               }
               //printf("  Event with %s = %.1e (requires <= %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
               break;
            case DisplayFlags::equal:
               if (val!=fSkipCutVal[i]) {
                  //printf("Skipping event with %s = %.1e (requires == %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
                  return true;
               }
               //printf("  Event with %s = %.1e (requires == %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
               break;
            case DisplayFlags::notequal:
               if (val==fSkipCutVal[i]) {
                  //printf("Skipping event with %s = %.1e (requires != %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
                  return true;
               }
               //printf("  Event with %s = %.1e (requires != %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
               break;
            case DisplayFlags::greaterthan:
               if (val<=fSkipCutVal[i]) {
                  //printf("Skipping event with %s = %.1e (requires > %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
                  return true;
               }
               //printf("  Event with %s = %.1e (requires > %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
               break;
            case DisplayFlags::greaterthanequal:
               if (val<fSkipCutVal[i]) {
                  //printf("Skipping event with %s = %.1e (requires >= %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
                  return true;
               }
               //printf("  Event with %s = %.1e (requires >= %.1e)\n",fFlags->skip_var[i].c_str(),val,fFlags->skip_val[i]);
               break;
         }         
      }
      return false;
   }
};

class DisplayModuleFactory: public TAFactory
{
public:
   DisplayFlags fFlags;

public:
   void Usage()
   {
      printf("DisplayModuleFactory::Help!\n");
      printf("\t--aged      Turn AG event display on\n");
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("DisplayModuleFactory::Init!\n");
      // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      // READ cmd line parameters to pass to this module here
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--aged" )
               fFlags.fBatch = false;
            if( args[i] == "--forcereco" )
               fFlags.fForce=true;
            if( args[i] == "--showbartypes" )
               fFlags.fShowBarTypes = atoi(args[++i].c_str());
            if( args[i] == "--skipto" ) {
               if (i+3<args.size() and fFlags.operator_map.count(args[i+2])) {
                  fFlags.skip_var.push_back(args[i+1]);
                  fFlags.skip_operator.push_back(fFlags.operator_map[args[i+2]]);
                  fFlags.skip_val.push_back(atof(args[i+3].c_str()));
               }
            }
         }
      // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   }
   void Finish()
   {
      printf("DisplayModuleFactory::Finish!\n");
   }
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      if( fFlags.fBatch )
         printf("DisplayModuleFactory::NewRunObject, run %d, file %s -- BATCH MODE\n",
                runinfo->fRunNo, runinfo->fFileName.c_str());
      else
         printf("DisplayModuleFactory::NewRunObject, run %d, file %s\n", 
                runinfo->fRunNo, runinfo->fFileName.c_str());

      return new DisplayRun(runinfo,&fFlags);
   }
};


static TARegister tar(new DisplayModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
