#include "manalyzer.h"
#include "midasio.h"
#include "AgFlow.h"
#include "RecoFlow.h"
#include "AnaSettings.hh"
#include "json.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#include "TMath.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"
#include "TBarEvent.hh"
#include "TBarCaliFile.h"
#include "TVector3.h"

class MatchingModuleFlags
{
public:
   bool fPrint = false;
   bool fDiag = false;
   std::string fCaliFile = "";
   bool fNoCali = false;
   AnaSettings* ana_settings=0;

   MatchingModuleFlags() // ctor
   { }
   ~MatchingModuleFlags() // dtor
   { }
};

class BscMatchingModule: public TARunObject
{
public:
   MatchingModuleFlags* fFlags;

private:

   TBarCaliFile* Calibration;

   // BV geometry
   const double inner_diameter = 446.0; // mm
   const double outer_diameter = 486.0; // mm
   const double radius = (inner_diameter+outer_diameter)/4.; // Use centre of BV
   //const double length = 2604.; // mm
   const double veff = 1.8427e8;

   // Matching parameters
   double max_dphi; // rad
   const double max_top_bot_diff_t; // s, maximum allowed time between top time and matched bot time

public:

   BscMatchingModule(TARunInfo* runinfo, MatchingModuleFlags* f): TARunObject(runinfo), fFlags(f),
                 max_dphi(f->ana_settings->GetDouble("BscModule","max_dphi")),
                 max_top_bot_diff_t(f->ana_settings->GetDouble("BscModule","max_top_bot_diff"))
                 
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="BV Matching Module";
#endif
      printf("BscMatchingModule::ctor!\n");
   }

   ~BscMatchingModule()
   {
      printf("BscMatchingModule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      if (fFlags->fPrint) { printf("BscMatchingModule::begin!\n"); };

      Calibration = new TBarCaliFile();
      if (!fFlags->fNoCali) {
         if (fFlags->fCaliFile=="") Calibration->LoadCali(runinfo->fRunNo,true);
         else Calibration->LoadCaliFile(fFlags->fCaliFile,true);
      }

      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
   }


   void EndRun(TARunInfo* runinfo)
   {

      if (fFlags->fPrint) {printf("EndRun, run %d\n", runinfo->fRunNo);}
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if (fFlags->fPrint) {printf("PauseRun, run %d\n", runinfo->fRunNo);}
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fFlags->fPrint) {printf("ResumeRun, run %d\n", runinfo->fRunNo);}
   }

   // Main function
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
   
      AgBarEventFlow *bf = flow->Find<AgBarEventFlow>();
      if(!bf)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      TBarEvent *barEvt=bf->BarEvent;
      if (!barEvt)
      {
         if (fFlags->fPrint) {printf("BscMatchingModule: TBarEvent not found!\n");}
         return flow;
      }
      
      if( fFlags->fPrint ) printf("BscMatchingModule::AnalyzeFlowEvent(...) Start\n");

      std::vector<TBarADCHit>& adchits = barEvt->GetADCHits();
      std::vector<bool> adcmatch(adchits.size(),false);
      std::vector<TBarTDCHit>& tdchits = barEvt->GetTDCHits();
      std::vector<bool> tdcmatch(tdchits.size(),false);
      std::vector<TBarEndHit>& endhits = barEvt->GetEndHits();
      std::vector<bool> endmatch(endhits.size(),false);
      double adc_tdc_offset = barEvt->GetEventTDCTime();

      std::vector<TBarHit> barhits;
      barhits = MatchEndEnd(barhits,endhits,endmatch);         // Hit type 1
      barhits = MatchEndTDC(barhits,endhits,tdchits,endmatch,tdcmatch); // Hit type 2
      barhits = MatchEndADC(barhits,endhits,adchits,endmatch,adcmatch,adc_tdc_offset); // Hit type 3
      barhits = BarHitsFromEndHits(barhits,endhits, endmatch);  // Hit type 4
      barhits = MatchTDCTDC(barhits,tdchits,tdcmatch);         // Hit type 5
      barhits = MatchTDCADC(barhits,tdchits,adchits,tdcmatch,adcmatch,adc_tdc_offset); // Hit type 6
      barhits = MatchADCADC(barhits,adchits,adcmatch);         // Hit type 7
      barhits = BarHitsFromTDCHits(barhits,tdchits, tdcmatch);  // Hit type 8
      barhits = BarHitsFromADCHits(barhits,adchits, adcmatch);  // Hit type 9
      
      AgSignalsFlow* SigFlow = flow->Find<AgSignalsFlow>();
      if( !SigFlow ) 
      {
         if (fFlags->fPrint) {printf("BscMatchingModule: AgSignalsFlow not found!\n");}
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }

      for (const TBarHit& barhit: barhits) {
         barEvt->AddBarHit(barhit);
      }
      barEvt->FindMainGroup();

      if (!SigFlow->fSkipReco)
      {
         std::vector<TFitHelix>& helixArray = SigFlow->fHelixArray;
         barhits = MatchWithTPC(barEvt->GetBarHits(),helixArray, barEvt);
      }

      if( fFlags->fPrint ) printf("BscMatchingModule::AnalyzeFlowEvent(...) End\n");
      
      //AgBarEventFlow 
      //e->SetBarEvent(barEvt);
      return flow;
   }

   //________________________________
   // MAIN FUNCTIONS

   std::vector<TBarHit> MatchEndEnd(std::vector<TBarHit> &barhits, const std::vector<TBarEndHit> &endhits,
                                    std::vector<bool> &endmatch) const
   {
      std::vector<double> diffs;
      std::vector<int> indixes_i;
      std::vector<int> indixes_j;
      // Make a list of all valid index pairs and their time difference
      for (size_t i=0; i<endhits.size(); i++) {
         const TBarEndHit& bothit = endhits[i];
         if (!(bothit.IsBottom())) continue; // Avoid double counting
         const double time1 = bothit.GetTDCTimeRaw();
         for (size_t j=0; j<endhits.size(); j++) {
            const TBarEndHit& tophit = endhits[j];
            if (bothit.GetBarID()!=tophit.GetBarID()) continue;
            if (!(tophit.IsTop())) continue; // Avoid double counting
            const double time2 = tophit.GetTDCTimeRaw();
            if (TMath::Abs(time1 - time2) > max_top_bot_diff_t) continue;
            diffs.push_back(TMath::Abs(time1 - time2));
            indixes_i.push_back(i);
            indixes_j.push_back(j);
         }
      }
      while (diffs.size() > 0) { // While there is still pairs in the list...
         // Look at the pair with smallest dt
         const int min_diff_index = std::min_element(diffs.begin(),diffs.end()) - diffs.begin();
         const int i = indixes_i.at(min_diff_index);
         const int j = indixes_j.at(min_diff_index);
         const TBarEndHit& bothit = endhits.at(i);
         const TBarEndHit& tophit = endhits.at(j);
         if (!(endmatch.at(i)) and !(endmatch.at(j))) { // See if both hits are unmatched
            if (fFlags->fPrint) printf("Hit type 1 on bar %d\n",tophit.GetBarID());            
            TBarHit h(tophit,bothit); // Make a BarHit
            const double time_top = tophit.GetTime();
            const double time_bottom = bothit.GetTime();
            const int barID = tophit.GetBarID();
            h.SetZed(GetZedFromTwoTimes(time_top,time_bottom,barID));
            const double amp_top = tophit.GetADCHit().GetAmpFit();
            const double amp_bot = bothit.GetADCHit().GetAmpFit();
            const double zed_adc = GetZedFromTwoAmplitudes(amp_top,amp_bot,barID);
            h.SetADCZed(zed_adc);
            h.SetTime(GetTimeFromTwoEnds(time_top, time_bottom,barID));
            if (fFlags->fPrint) printf("Hit zed = %.6f, time = %.3f\n",h.GetZed(),1e9*h.GetTime());
            h.SetIsGoodForTOF(tophit.GetTDCHit().IsGoodForTOF() and bothit.GetTDCHit().IsGoodForTOF());
            barhits.emplace_back(h);
            endmatch.at(i) = true;
            endmatch.at(j) = true;
         }
         // Erase pair from list
         diffs.erase(diffs.begin()+min_diff_index);
         indixes_i.erase(indixes_i.begin()+min_diff_index);
         indixes_j.erase(indixes_j.begin()+min_diff_index);
      }

      return barhits;
   }
   std::vector<TBarHit> MatchEndTDC(std::vector<TBarHit> &barhits, const std::vector<TBarEndHit> &endhits,
                                    const std::vector<TBarTDCHit> &tdchits, std::vector<bool> &endmatch,
                                    std::vector<bool> &tdcmatch) const
   {
      std::vector<double> diffs;
      std::vector<int> indixes_i;
      std::vector<int> indixes_j;
      // Make a list of all valid index pairs and their time difference
      for (size_t i=0; i<endhits.size(); i++) {
         const TBarEndHit& endhit = endhits[i];
         const double time1 = endhit.GetTDCTimeRaw();
         for (size_t j=0; j<tdchits.size(); j++) {
            const TBarTDCHit& tdchit = tdchits[j];
            if (endhit.GetBarID()!=tdchit.GetBarID()) continue; // Different bar
            if (tdchit.IsMatchedToADC()) continue; // Avoid double counting
            if (tdchit.IsTop()==endhit.IsTop()) continue; // Same end
            if (!tdchit.IsGood()) continue; // 165 ns TDC calbe reflection
            const double time2 = tdchit.GetTime();
            if (TMath::Abs(time1 - time2) > max_top_bot_diff_t) continue;
            diffs.push_back(TMath::Abs(time1 - time2));
            indixes_i.push_back(i);
            indixes_j.push_back(j);
         }
      }
      while (diffs.size() > 0) { // While there is still pairs in the list...
         // Look at the pair with smallest dt
         const int min_diff_index = std::min_element(diffs.begin(),diffs.end()) - diffs.begin();
         const int i = indixes_i.at(min_diff_index);
         const int j = indixes_j.at(min_diff_index);
         const TBarEndHit& endhit = endhits.at(i);
         const TBarTDCHit& tdchit = tdchits.at(j);
         if (!(endmatch.at(i)) and !(tdcmatch.at(j))) { // See if both hits are unmatched
            if (fFlags->fPrint) printf("Hit type 2 on bar %d\n",endhit.GetBarID());            
            TBarHit h(endhit,tdchit); // Make a BarHit
            double time_top = -1;
            if (endhit.IsTop()) time_top = endhit.GetTime();
            if (tdchit.IsTop()) time_top = tdchit.GetTimeCali();
            double time_bottom = -1;
            if (endhit.IsBottom()) time_bottom = endhit.GetTime();
            if (tdchit.IsBottom()) time_bottom = tdchit.GetTimeCali();
            int barID = endhit.GetBarID();
            h.SetZed(GetZedFromTwoTimes(time_top,time_bottom,barID));
            h.SetTime(GetTimeFromTwoEnds(time_top, time_bottom,barID));
            if (fFlags->fPrint) printf("Hit zed = %.6f, time = %.3f\n",h.GetZed(),1e9*h.GetTime());
            barhits.push_back(h);
            endmatch.at(i) = true;
            tdcmatch.at(j) = true;
         }
         // Erase pair from list
         diffs.erase(diffs.begin()+min_diff_index);
         indixes_i.erase(indixes_i.begin()+min_diff_index);
         indixes_j.erase(indixes_j.begin()+min_diff_index);
      }
      return barhits;
   }
   std::vector<TBarHit> MatchEndADC(std::vector<TBarHit> &barhits, const std::vector<TBarEndHit> &endhits,
                                    const std::vector<TBarADCHit> &adchits, std::vector<bool> &endmatch,
                                    std::vector<bool> &adcmatch, const double adc_tdc_offset) const
   {
      std::vector<double> diffs;
      std::vector<int> indixes_i;
      std::vector<int> indixes_j;
      // Make a list of all valid index pairs and their time difference
      for (size_t i=0; i<endhits.size(); i++) {
         const TBarEndHit& endhit = endhits[i];
         const double time1 = endhit.GetTDCTimeRaw();
         for (size_t j=0; j<adchits.size(); j++) {
            const TBarADCHit& adchit = adchits[j];
            if (!adchit.IsGood()) continue;
            if (endhit.GetBarID()!=adchit.GetBarID()) continue; // Different bar
            if (adchit.IsMatchedToTDC()) continue; // Avoid double counting
            if (adchit.IsTop()==endhit.IsTop()) continue; // Same end
            const double time2 = adchit.GetStartTime()*1e-9 + adc_tdc_offset;
            if (TMath::Abs(time1 - time2) > max_top_bot_diff_t) continue;
            diffs.push_back(TMath::Abs(time1 - time2));
            indixes_i.push_back(i);
            indixes_j.push_back(j);
         }
      }
      while (diffs.size() > 0) { // While there is still pairs in the list...
         // Look at the pair with smallest dt
         const int min_diff_index = std::min_element(diffs.begin(),diffs.end()) - diffs.begin();
         const int i = indixes_i.at(min_diff_index);
         const int j = indixes_j.at(min_diff_index);
         const TBarEndHit& endhit = endhits.at(i);
         const TBarADCHit& adchit = adchits.at(j);
         if (!(endmatch.at(i)) and !(adcmatch.at(j))) { // See if both hits are unmatched
            if (fFlags->fPrint) printf("Hit type 3 on bar %d\n",endhit.GetBarID());            
            TBarHit h(endhit,adchit); // Make a BarHit
            barhits.push_back(h);
            endmatch.at(i) = true;
            adcmatch.at(j) = true;
         }
         // Erase pair from list
         diffs.erase(diffs.begin()+min_diff_index);
         indixes_i.erase(indixes_i.begin()+min_diff_index);
         indixes_j.erase(indixes_j.begin()+min_diff_index);
      }
      return barhits;
   }
   std::vector<TBarHit> MatchTDCTDC(std::vector<TBarHit> &barhits, const std::vector<TBarTDCHit> &tdchits,
                                    std::vector<bool> &tdcmatch) const
   {
      std::vector<double> diffs;
      std::vector<int> indixes_i;
      std::vector<int> indixes_j;
      // Make a list of all valid index pairs and their time difference
      for (size_t i=0; i<tdchits.size(); i++) {
         const TBarTDCHit& bothit = tdchits[i];
         if (!(bothit.IsBottom())) continue; // Avoid double counting
         if (bothit.IsMatchedToADC()) continue; // Avoid double counting
         if (!bothit.IsGood()) continue; // 165 ns TDC calbe reflection
         double time1 = bothit.GetTime();
         for (size_t j=0; j<tdchits.size(); j++) {
            const TBarTDCHit& tophit = tdchits[j];
            if (bothit.GetBarID()!=tophit.GetBarID()) continue;
            if (!(tophit.IsTop())) continue; // Avoid double counting
            if (tophit.IsMatchedToADC()) continue; // Avoid double counting
            if (!tophit.IsGood()) continue; // 165 ns TDC calbe reflection
            double time2 = tophit.GetTime();
            if (TMath::Abs(time1 - time2) > max_top_bot_diff_t) continue;
            diffs.push_back(TMath::Abs(time1 - time2));
            indixes_i.push_back(i);
            indixes_j.push_back(j);
         }
      }
      while (diffs.size() > 0) { // While there is still pairs in the list...
         // Look at the pair with smallest dt
         const int min_diff_index = std::min_element(diffs.begin(),diffs.end()) - diffs.begin();
         const int i = indixes_i.at(min_diff_index);
         const int j = indixes_j.at(min_diff_index);
         const TBarTDCHit& bothit = tdchits.at(i);
         const TBarTDCHit& tophit = tdchits.at(j);
         if (!(tdcmatch.at(i)) and !(tdcmatch.at(j))) { // See if both hits are unmatched
            if (fFlags->fPrint) printf("Hit type 5 on bar %d\n",tophit.GetBarID());            
            TBarHit h(tophit,bothit); // Make a BarHit
            const double time_top = tophit.GetTimeCali();
            const double time_bottom = bothit.GetTimeCali();
            int barID = tophit.GetBarID();
            h.SetZed(GetZedFromTwoTimes(time_top,time_bottom,barID));
            h.SetTime(GetTimeFromTwoEnds(time_top, time_bottom,barID));
            if (fFlags->fPrint) printf("Hit zed = %.6f, time = %.3f\n",h.GetZed(),1e9*h.GetTime());
            barhits.emplace_back(h);
            tdcmatch.at(i) = true;
            tdcmatch.at(j) = true;
         }
         // Erase pair from list
         diffs.erase(diffs.begin()+min_diff_index);
         indixes_i.erase(indixes_i.begin()+min_diff_index);
         indixes_j.erase(indixes_j.begin()+min_diff_index);
      }

      return barhits;
   }
   std::vector<TBarHit> MatchTDCADC(std::vector<TBarHit> &barhits, const std::vector<TBarTDCHit> &tdchits,
                                    const std::vector<TBarADCHit> &adchits, std::vector<bool> &tdcmatch,
                                    std::vector<bool> &adcmatch, double adc_tdc_offset) const
   {
      std::vector<double> diffs;
      std::vector<int> indixes_i;
      std::vector<int> indixes_j;
      // Make a list of all valid index pairs and their time difference
      for (size_t i=0; i<tdchits.size(); i++) {
         const TBarTDCHit& tdchit = tdchits[i];
         if (tdchit.IsMatchedToADC()) continue; // Avoid double counting
         if (!tdchit.IsGood()) continue; // 165 ns TDC calbe reflection
         const double time1 = tdchit.GetTime();
         for (size_t j=0; j<adchits.size(); j++) {
            const TBarADCHit& adchit = adchits[j];
            if (tdchit.GetBarID()!=adchit.GetBarID()) continue; // Different bar
            if (adchit.IsMatchedToTDC()) continue; // Avoid double counting
            if (adchit.IsTop()==tdchit.IsTop()) continue; // Same end
            if (!adchit.IsGood()) continue;
            const double time2 = adchit.GetStartTime()*1e-9 + adc_tdc_offset;
            if (TMath::Abs(time1 - time2) > max_top_bot_diff_t) continue;
            diffs.push_back(TMath::Abs(time1 - time2));
            indixes_i.push_back(i);
            indixes_j.push_back(j);
         }
      }
      while (diffs.size() > 0) { // While there is still pairs in the list...
         // Look at the pair with smallest dt
         const int min_diff_index = std::min_element(diffs.begin(),diffs.end()) - diffs.begin();
         const int i = indixes_i.at(min_diff_index);
         const int j = indixes_j.at(min_diff_index);
         const TBarTDCHit& tdchit = tdchits.at(i);
         const TBarADCHit& adchit = adchits.at(j);
         if (!(tdcmatch.at(i)) and !(adcmatch.at(j))) { // See if both hits are unmatched
            if (fFlags->fPrint) printf("Hit type 6 on bar %d\n",tdchit.GetBarID());            
            // Make a BarHit at back of barhits
            barhits.emplace_back(tdchit,adchit);
            tdcmatch.at(i) = true;
            adcmatch.at(j) = true;
         }
         // Erase pair from list
         diffs.erase(diffs.begin()+min_diff_index);
         indixes_i.erase(indixes_i.begin()+min_diff_index);
         indixes_j.erase(indixes_j.begin()+min_diff_index);
      }
      return barhits;
   }
   std::vector<TBarHit> MatchADCADC(std::vector<TBarHit> &barhits, const std::vector<TBarADCHit> &adchits,
                                    std::vector<bool> &adcmatch) const
   {
      std::vector<double> diffs;
      std::vector<int> indixes_i;
      std::vector<int> indixes_j;
      // Make a list of all valid index pairs and their time difference
      for (size_t i=0; i<adchits.size(); i++) {
         const TBarADCHit& bothit = adchits[i];
         if (!bothit.IsGood()) continue;
         if (!(bothit.IsBottom())) continue; // Avoid double counting
         if (bothit.IsMatchedToTDC()) continue; // Avoid double counting
         const double time1 = bothit.GetStartTime();
         for (size_t j=0; j<adchits.size(); j++) {
            const TBarADCHit& tophit = adchits[j];
            if (!tophit.IsGood()) continue;
            if (bothit.GetBarID()!=tophit.GetBarID()) continue;
            if (!(tophit.IsTop())) continue; // Avoid double counting
            if (tophit.IsMatchedToTDC()) continue; // Avoid double counting
            const double time2 = tophit.GetStartTime();
            if (TMath::Abs(time1 - time2) > max_top_bot_diff_t) continue;
            diffs.push_back(TMath::Abs(time1 - time2));
            indixes_i.push_back(i);
            indixes_j.push_back(j);
         }
      }
      while (diffs.size() > 0) { // While there is still pairs in the list...
         // Look at the pair with smallest dt
         const int min_diff_index = std::min_element(diffs.begin(),diffs.end()) - diffs.begin();
         const int i = indixes_i.at(min_diff_index);
         const int j = indixes_j.at(min_diff_index);
         const TBarADCHit& bothit = adchits.at(i);
         const TBarADCHit& tophit = adchits.at(j);
         if (!(adcmatch.at(i)) and !(adcmatch.at(j))) { // See if both hits are unmatched
            if (fFlags->fPrint) printf("Hit type 7 on bar %d\n",tophit.GetBarID());            
            // Make a BarHit at the back of barhits
            barhits.emplace_back(tophit,bothit);
            adcmatch.at(i) = true;
            adcmatch.at(j) = true;
         }
         // Erase pair from list
         diffs.erase(diffs.begin()+min_diff_index);
         indixes_i.erase(indixes_i.begin()+min_diff_index);
         indixes_j.erase(indixes_j.begin()+min_diff_index);
      }

      return barhits;
   }
   std::vector<TBarHit> BarHitsFromEndHits(std::vector<TBarHit>& barhits, std::vector<TBarEndHit>& endhits, std::vector<bool>& endmatch) const {
      for (int i=0; i<int(endhits.size()); i++) {
         if (endmatch.at(i)) continue;
         const TBarEndHit& endhit = endhits.at(i);
         if (fFlags->fPrint) printf("Hit type 4 on bar %d\n",endhit.GetBarID());
         // Copy into barhits
         barhits.emplace_back(endhit);
      }
      return barhits;
   }
   std::vector<TBarHit> BarHitsFromTDCHits(std::vector<TBarHit>& barhits, std::vector<TBarTDCHit>& tdchits, std::vector<bool>& tdcmatch) const {
      for (int i=0; i<int(tdchits.size()); i++) {
         if (tdcmatch.at(i)) continue;
         const TBarTDCHit& tdchit = tdchits.at(i);
         if (tdchit.IsMatchedToADC()) continue;
         if (tdchit.Is165Reflection()) continue; // 165 ns TDC calbe reflection
         if (fFlags->fPrint) printf("Hit type 8 on bar %d\n",tdchit.GetBarID());
         // Copy into barhits
         barhits.emplace_back(tdchit);
      }
      return barhits;
   }
   std::vector<TBarHit> BarHitsFromADCHits(std::vector<TBarHit>& barhits, std::vector<TBarADCHit>& adchits, std::vector<bool>& adcmatch) const {
      for (int i=0; i<int(adchits.size()); i++) {
         if (adcmatch.at(i)) continue;
         const TBarADCHit& adchit = adchits.at(i);
         if (!adchit.IsGood()) continue;
         if (adchit.IsMatchedToTDC()) continue;
         if (fFlags->fPrint) printf("Hit type 9 on bar %d\n",adchit.GetBarID());
         // Copy into barhits
         barhits.emplace_back(adchit);
      }
      return barhits;
   }

   double GetZedFromTwoTimes(double time_top, double time_bottom, int barID) const {
      if (Calibration->IsLoaded() and Calibration->GetVEff(barID)>0)
         return 0.5 * (time_bottom - time_top) * Calibration->GetVEff(barID);
      else
         return 0.5*(time_bottom - time_top)*veff;
   }
   double GetZedFromTwoAmplitudes(double amp_top, double amp_bot, int barID) const {
      if (Calibration->IsLoaded() and Calibration->GetVEff(barID)>0)
         return 0.5 * TMath::Log(amp_top/amp_bot) * Calibration->GetADCLambda(barID) + Calibration->GetADCZedOffset(barID);
      else
         return 0.5 * TMath::Log(amp_top/amp_bot) * 0.9;
   }
   double GetTimeFromTwoEnds(double time_top, double time_bottom, int barID) const {
      if (Calibration->IsLoaded() and Calibration->GetVEff(barID)>0)
         //return (time_top+time_bottom)/2. - (0.001*length)/(2*Calibration->GetVEff(barID));
         return (time_top+time_bottom)/2.;
      else
         //return (time_top+time_bottom)/2. - (0.001*length)/(2*veff);
         return (time_top+time_bottom)/2.;
   }

   std::vector<TBarHit> MatchWithTPC(std::vector<TBarHit>& barhits, std::vector<TFitHelix>& helixArray, TBarEvent* barEvt) const {
      std::vector<bool> ismatched(helixArray.size(),false);
      // First, loop over each bar hit. We will try to match each bar hit to a track.
      for (unsigned int ibar=0; ibar<barhits.size(); ibar++) {
         const TBarHit& barhit = barhits[ibar];
         if (!barhit.IsComplete()) continue; // Match only complete bar hits. Can revisit this later. !!!
         const TVector3& bar = barhit.Get3Vector();
         double best_dist = 1000;
         int best_hit_index = -1;
         TVector3 best_closest;
         double best_dist_other = 1000;
         int best_hit_index_other = -1;
         TVector3 best_closest_other;
         // Now loop over each TPC helix. Multiple bar hits can be matched to the same helix.
         // Each helix keeps a list of each bar hit that is matched to it, on each branch.
         for (unsigned int i_tpc=0; i_tpc<helixArray.size(); i_tpc++) {
            const TFitHelix& hel = helixArray.at(i_tpc);
            if (TMath::Abs(hel.GetD() + 2*hel.GetRc())<radius) continue; // Skip this track, it doesn't reach the BV
            const TVector3 closest_scaled = hel.GetClosestToPoint(bar.X()*1e3,bar.Y()*1e3,bar.Z()*1e3);
            const TVector3 closest(closest_scaled.X()*1e-3,closest_scaled.Y()*1e-3,closest_scaled.Z()*1e-3);
            const double d_phi = TMath::Abs(closest.DeltaPhi(bar));
            const double dist_XY = (closest-bar).Perp();
            if (d_phi < max_dphi and dist_XY < best_dist and !hel.IsDoubleBranched()) {
               // Best match so far
                  best_hit_index = i_tpc;
                  best_dist = dist_XY;
                  best_closest = closest;
            }
            if (d_phi < max_dphi and dist_XY < best_dist and hel.IsDoubleBranched() and hel.GetDirection(closest_scaled.X(),closest_scaled.Y()) < 0) {
               // Best match so far (one side of double sided track)
               best_hit_index = i_tpc;
               best_dist = dist_XY;
               best_closest = closest;
            }
            if (d_phi < max_dphi and dist_XY < best_dist_other and hel.IsDoubleBranched() and hel.GetDirection(closest_scaled.X(),closest_scaled.Y()) > 0) {
               // Best match so far (other side of double sided track)
               best_hit_index_other = i_tpc;
               best_dist_other = dist_XY;
               best_closest_other = closest;
            }

         }
         bool hit_found = (best_hit_index!=-1), hit_found_other = (best_hit_index_other!=-1);
         if (hit_found or hit_found_other) { // Hit found
            unsigned int index; double dist; TVector3 closest; int whichbranch;
            if (hit_found and (!hit_found_other or best_dist_other>best_dist)) {
               index = best_hit_index; dist = best_dist; closest = best_closest; whichbranch = 1;
            }
            else {
               index = best_hit_index_other; dist = best_dist_other; closest = best_closest_other; whichbranch = 2;
            }

            if (fFlags->fPrint) printf("TPC match found on bar %d hit type %d\n",barhit.GetBarID(),barhit.GetHitType());
            if (fFlags->fPrint) printf("Bar hit point (%.3f, %.3f) - Closest TPC point (%.3f, %.3f) - XY distance %.3f\n\n",barhit.Get3Vector().X(),barhit.Get3Vector().Y(),closest.X(),closest.Y(),dist);
            barhits[ibar].SetTPCHit(closest);
            TFitHelix& hel = helixArray.at(index);
            barhits[ibar].SetTPCTrackNum(hel.GetTrackNum());
            ismatched[index] = true;
            if (whichbranch==1) hel.AddBarHitFirstBranch(ibar);
            else hel.AddBarHitSecondBranch(ibar);
            if (!hel.IsPileup() and !barhits[ibar].IsInMainGroup()) {
               hel.SetIsPileup(true);
               barEvt->IncrementNumberOfPileUpTracks();
            }
         }
      }
      for (bool m: ismatched) {
         if (m) barEvt->IncrementNumberOfMatchedTracks();
         else barEvt->IncrementNumberOfUnmatchedTracks();
      }

      if (fFlags->fPrint) {
         printf("BscMatchingModule::Total BarHits %ld, total TPC points %d\n",barhits.size(),helixArray.size());
         for (const TBarHit& barhit: barhits) {
            if (barhit.HasTPCHit()) printf("BscMatchingModule::BarHit type %d on bar %d matched to TPC\n", barhit.GetHitType(), barhit.GetBarID());
         }
      }


      return barhits;
   }

};

class matchingModuleFactory: public TAFactory
{
public:
   MatchingModuleFlags fFlags;
public:
   void Help()
   { 
      printf("MatchingModuleFactory::Help\n");
      printf("\t--anasettings /path/to/settings.json\t\t load the specified analysis settings\n");
      printf("\t--bscprint\t\t verbose printing\n");
      printf("\t--bscdiag\t\t diagnostic histograms\n");
      printf("\t--bsccalifile /path/to/BarrelCalibration.root\t\tloads barrel calibration directly from specified file\n");
      printf("\t--nobsccali\t\tskips loading barrel calibration file\n");
   }
   void Usage()
   {
      Help();
   }
   void Init(const std::vector<std::string> &args)
   {
      TString json="default";
      printf("matchingModuleFactory::Init!\n");
      for (unsigned i=0; i<args.size(); i++)
         {
         if( args[i]=="-h" || args[i]=="--help" )
            Help();
         if( args[i] == "--bscprint")
            fFlags.fPrint = true;
         if( args[i] == "--bscdiag")
            fFlags.fDiag = true;
         if( args[i] == "--bscnocali")
            fFlags.fNoCali = true;
         if( args[i] == "--bsccalifile") {
            i++;
            fFlags.fCaliFile = args[i];
         }
         if( args[i] == "--anasettings" ) 
            json=args[++i];
         }
      fFlags.ana_settings = new AnaSettings(json.Data(),args);
   }

   void Finish()
   {
      printf("matchingModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("matchingModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new BscMatchingModule(runinfo,&fFlags);
   }
};

static TARegister tar(new matchingModuleFactory);


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
