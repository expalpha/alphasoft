//
// TPC data montior, uses a circular buffer to histogram 
//
// JTK McKENNA
//

#include <stdio.h>

#include "manalyzer.h"
#include "midasio.h"

#include "RecoFlow.h"
#include "TStoreEvent.hh"

#include "TStyle.h"
#include "TColor.h"
#include "TH1.h"
#include "TH2.h"
#include "TF2.h"
#include "TExec.h"
#include "TCanvas.h"

#define SECONDS_TO_BUFFER 60


//Time to group SVD Events (seconds)
#define INTEGRATION_TIME 0.5

//#define BUFFER_DEPTH 2000
#define BUFFER_DEPTH SECONDS_TO_BUFFER / INTEGRATION_TIME

#define BINS_PER_SECOND ?

#define XMAX 100
#define YMAX 100
#define ZMAX 1300


#include "TCanvas.h"

#include "TAGDetectorEvent.hh"


const TAGDetectorEvent EventClassifier;

class TTPC
{
   public:

   int fBin;
   
   double fRunTime;
   
   std::vector<bool> fHasVertex;
   std::vector<std::vector<bool>> fPassCut;
   std::vector<double> fX;
   std::vector<double> fY;
   std::vector<double> fZ;
   // Occupancy... think about barrel?
   //std::vector<uint32_t> fCounts;
   
   TTPC(int _bin)//: fCounts(nSil * 4,0)
   {
      fBin = _bin;
      fRunTime = fBin * INTEGRATION_TIME;
   }
   ~TTPC()
   {

   }
   void operator +=(const TStoreEvent& data)
   {
      fHasVertex.push_back(data.GetVertexStatus() > 0);
      fPassCut.emplace_back( EventClassifier.ApplyCuts(&data, data.GetBarEvent()));//data.GetPassedCuts());
      //fRunTime = data.GetVF48Timestamp();
      fX.push_back(data.GetVertexX());
      fY.push_back(data.GetVertexY());
      fZ.push_back(data.GetVertexZ());
      //std::cout<<fX << "," << fY << "," << fZ <<std::endl;
            
      // CALCULATE OCCUPANCY HERE
            
   }
   double GetRunTime() const
   {
      return fRunTime;
   }
};

class TTPCMonitor: public TARunObject
{
private:
   // Ring buffer would probably be quicker... but lets just get this working
   std::deque<TTPC> fFIFO;

   TCanvas fLiveVertex;
   // Show only the selected 'passed cut'
   TCanvas fLivePassed;
   //TCanvas fLiveOccupancy;
   std::vector<TH2I> fXYvert;

   //TH2D fZY;
   std::vector<TH2I> fZTvert;

   int NCutsTypes;
   //TH1I fR;
   //TH2I fOccupancyT[4];
   TStyle* fSVDStyle;
   

   std::chrono::time_point<std::chrono::high_resolution_clock> fLastHistoUpdate;

public:
   bool fTrace = false;

   TTPCMonitor(TARunInfo* runinfo)
      : TARunObject(runinfo),
         fFIFO(
               std::deque<TTPC>()
            ),
         fLiveVertex("LiveVertex","LiveVertex"),
         fLivePassed("LivePassedCuts","LivePassedCuts")
         //fLiveOccupancy("LiveOccupancy","LiveOccupancy")
   {
      TAGDetectorEvent e;
      NCutsTypes = e.CutNames().size();

      fLastHistoUpdate = std::chrono::high_resolution_clock::now(); //measure time starting here
      fModuleName = "TPC Monitor";
      for (int i = 0; i < BUFFER_DEPTH; i++)
      {
         fFIFO.emplace_back(
            TTPC(
                  i - BUFFER_DEPTH 
               )
            );
      }
      fSVDStyle = new TStyle("SVDStyle","SVDStyle");
      fSVDStyle->SetPalette(kCool);
   }
   ~TTPCMonitor()
   {
      printf("TTPCMonitor::dtor!\n");
   }
   

   void BeginRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("TTPCMonitor::BeginRun %d\n", runinfo->fRunNo);
      fSVDStyle->SetPalette(kCool);
      TAGDetectorEvent e;
      for (int i = 0; i < NCutsTypes; i++ )
      {
         std::string name = "XY Vertex CutType:" + std::to_string(i) + " [" + e.GetOnlinePassCutsName(i) + "]";
         std::string title(name);
         name += "; X(cm); Y(cm)";
         fXYvert.emplace_back(
               name.c_str(),
               title.c_str(),
               100,-XMAX,XMAX,
               100,-YMAX,YMAX
            );
         name = "ZT Vertex CutType:" + std::to_string(i) + " [" + e.GetOnlinePassCutsName(i) + "]";
         title = name;
         title += "; Z(cm); Run Time(s);";
         fZTvert.emplace_back(
               name.c_str(),
               title.c_str(),
               100,-ZMAX,ZMAX,
               BUFFER_DEPTH, fFIFO.front().GetRunTime(), fFIFO.back().GetRunTime()
            );
      }
      /*
      fOccupancyT[0]= TH2I(
               "Hit Occupancy",
               "Hit Occupancy; Run Time(s); Silicon Module No;",
               BUFFER_DEPTH, fFIFO.front().GetRunTime(), fFIFO.back().GetRunTime(),
               nSil,0,nSil
            );
      fOccupancyT[1]= TH2I(
               "Hit Occupancy with Tracks",
               "Hit Occupancy with Tracks; Run Time(s); Silicon Module No;",
               BUFFER_DEPTH, fFIFO.front().GetRunTime(), fFIFO.back().GetRunTime(),
               nSil,0,nSil
            );

      fOccupancyT[2]= TH2I(
               "Hit Occupancy with Vertex",
               "Hit Occupancy with Vertex; Run Time(s); Silicon Module No;",
               BUFFER_DEPTH, fFIFO.front().GetRunTime(), fFIFO.back().GetRunTime(),
               nSil,0,nSil
            );
            
      fOccupancyT[3]= TH2I(
               "Hit Occupancy with Pass Cut",
               "Hit Occupancy with Pass Cut; Run Time(s); Silicon Module No;",
               BUFFER_DEPTH, fFIFO.front().GetRunTime(), fFIFO.back().GetRunTime(),
               nSil,0,nSil
            );
      */
      fLiveVertex.Divide(NCutsTypes,2);
      //fLiveOccupancy.Divide(1,4);
      gDirectory->cd();
   }
  
   TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow)
   {
      if (fTrace)
         printf("TTPCMonitor::Analyze, run %d, event serno %d, id 0x%04x, data size %d\n", runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
      return flow;
   }
   
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      AgAnalysisFlow *af = flow->Find<AgAnalysisFlow>();
      if (!af )
      {
         *flags|=TAFlag_SKIP_PROFILE;
         return flow;
      }
      if (fTrace)
         printf("TTPCMonitor::AnalyzeFlowEvent %d\n",runinfo->fRunNo);
      // prepare event to store in TTree
      TStoreEvent* analyzed_event = af->fEvent;

      int i = fFIFO.back().fBin;
      while (fFIFO.back().GetRunTime() < analyzed_event->GetTimeOfEvent())
      {
         fFIFO.emplace_back(TTPC(++i));
         fFIFO.pop_front();
      }

      //Find bin of the first event
      int bin = 0;
      while ( analyzed_event->GetTimeOfEvent() > fFIFO.at(bin).GetRunTime())
         bin++;
      fFIFO.at(bin) += *analyzed_event;

      auto time_now = std::chrono::high_resolution_clock::now(); //measure time starting here
      auto dt = std::chrono::duration_cast<std::chrono::milliseconds>( time_now - fLastHistoUpdate);
      if ( dt.count() < 500) // up to ~10fps
         return flow;
      fLastHistoUpdate = time_now;

      //Resise histograms
      /*for (int j=0; j < 4; j++)
         fOccupancyT[j].GetXaxis()->Set(BUFFER_DEPTH, fFIFO.front().GetRunTime(), fFIFO.back().GetRunTime());
      */
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);

      for (auto& h: fZTvert)
         h.GetYaxis()->Set(BUFFER_DEPTH, fFIFO.front().GetRunTime(), fFIFO.back().GetRunTime());
      /*
      for (int j=0; j < 4; j++)
         fOccupancyT[j].Reset();
      */
      for (auto& h: fXYvert)
         h.Reset();
      for (auto& h: fZTvert)
         h.Reset();
      fSVDStyle->SetPalette(kCool);

      //Update the histograms
      for (TTPC& s: fFIFO)
      {
         const size_t nVerts = s.fHasVertex.size();
         for (size_t i = 0; i < nVerts; i++)
         {
            if (s.fHasVertex[i] <= 0)
              continue;
            for (size_t j = 0; j < s.fPassCut[i].size(); j++)
            {
               if (!s.fPassCut[i].at(j))
                  continue;
               fXYvert[j].Fill(s.fX[i], s.fY[i]);
               fZTvert[j].Fill(s.fZ[i], s.fRunTime);
            }
         }
         // Fill occupancy here!
         /*
         for (int j = 0; j < 4; j++)
         for (int i = 0; i < nSil; i++)
         {
            if (s.fCounts[i + nSil*j])
            {
               fOccupancyT[j].Fill(s.fRunTime, i, s.fCounts[i + nSil*j]);
               //std::cout<<s.fRunTime << "\t" << s.fCounts[i] << std::endl;
            }
         }*/
      }
      int ii = 1; int jj = 0;
      while ( jj < NCutsTypes)
      {
         fLiveVertex.cd(ii);
         fXYvert[jj].Draw("colz");
         ii++;
         jj++;
      }
      jj = 0;
      while ( jj < NCutsTypes)  
      {
         fLiveVertex.cd(ii);
         fZTvert[jj].Draw("colz");
         ii++;
         jj++;
      }
      fLiveVertex.Draw();
      
      //Maybe think about making a new histogram that has more bins?
      fLivePassed.cd();
      if ( 6  < fZTvert.size() )
      {
         fZTvert[6].GetZaxis()->SetRangeUser(0, 15);
         fZTvert[6].Draw("colz");
      }
      fLivePassed.Draw();
      //for (int i = 0; i < 4; i++)
      //{
         //fLiveOccupancy.cd(i + 1);
         //fOccupancyT[i].Draw("colz");
      //}
      //fLiveCanvas.Draw();
      return flow;
   }
};


static TARegister tar1(new TAFactoryTemplate<TTPCMonitor>);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
