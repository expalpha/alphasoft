//
// AG offline MVA application (jagana)
//
// L GOLINO
//


//Turning this whole mother off
#include <stdio.h>
#include <numeric>
#include <cmath>

#include "manalyzer.h"
#include "midasio.h"

#include "RecoFlow.h"
#include "AnalysisFlow.h"

#include "TApplication.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"

#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/RReader.hxx"

#include "TBarMVAVars.h"

//#include "../OnlineMVA/weights/alphaClassification_BDTF.class.C"
//#include "../OnlineMVA/weights/alphaClassification_BDTF.weights.xml"

class OfflineAGMVAFlags
{
public:
   bool fPrint = false;
   bool fUseMVA = false;
   std::string fXMLModel = "./MVA/AGOnlineMVA/dataset/weights/AGTMVAClassification_BDT.weights.xml";
   bool fPrintMVAValue = false;

};


class OfflineAGMVA: public TARunObject
{
private:
   //ReadBDTF* r;
   TString gmethodName;
   TString gdir;
   double grfcut;

   TMVA::Reader *reader;

      float X, Y, Z;
      float vStatus;
      float barMultiplicity;
      float pointsPerTrack;
      float meanRSigma;
      float meanZSigma;
      float numTracks;
      float numPoints;

      float fNumBars, fNumEnds, fNumTDC;
      float fNumADC, fNumBarsComp, fNumBarsHComp, fNumMBars, fNumMBarsComp, fNumMBarsHComp;
      float fNumClusters;

      float nhits,r;
      float S0rawPerp,S0axisrawZ,phi_S0axisraw,nCT,nGT,tracksdca;
      float curvemin,curvemean,lambdamin,lambdamean,curvesign,phi;
      float nsphits, nwirehits, npadhits; //Lukas additions.
      float totalresidual,meantotalresidual,usedresidual,meanusedresidual;
      //Unused
      //float meanresidual;
      std::array<float,NUM_BV_VARS> bv_var;
  
public:
   OfflineAGMVAFlags* fFlags;
   bool fTrace = false;
   
   
   OfflineAGMVA(TARunInfo* runinfo, OfflineAGMVAFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef MANALYZER_PROFILER
      fModuleName="Offline AG MVA Module";
#endif
      if (fTrace)
         printf("OfflineAGMVA::ctor!\n");
      
      //~4mHz Background (42% efficiency)
      grfcut=0;
   }

   ~OfflineAGMVA()
   {
      if (fTrace)
         printf("OfflineAGMVA::dtor!\n");
      if (reader)
         delete reader;
   }
   
   
   void BeginRun(TARunInfo* runinfo)
   {
      reader = new TMVA::Reader();
      if (fTrace)
         printf("OfflineAGMVA::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
     if(!fFlags->fUseMVA)
        return;

      //Joe, is this not needed?
      //runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory 

      std::string foldername = fFlags->fXMLModel;



      TMVA::Experimental::RReader* rreader = new TMVA::Experimental::RReader(foldername);
      for(std::string variable: rreader->GetVariableNames())
      {
         std::cout << "Adding variable " << variable << " to the TMVA::Reader\n";
         //A2 style hyper parameters
         if(variable == "vStatus") reader->AddVariable( "vStatus",&vStatus);
         if(variable == "barMultiplicity") reader->AddVariable( "barMultiplicity",&barMultiplicity);
         if(variable == "pointsPerTrack") reader->AddVariable( "pointsPerTrack",&pointsPerTrack);
         if(variable == "meanRSigma") reader->AddVariable( "meanRSigma",&meanRSigma);
         if(variable == "numTracks") reader->AddVariable( "numTracks",&numTracks);
         if(variable == "numPoints") reader->AddVariable( "numPoints",&numPoints);
         if(variable == "meanZSigma") reader->AddVariable( "meanZSigma",&meanZSigma);
         
         //A2 style hyper parameters
         if(variable == "nhits") reader->AddVariable("nhits",&nhits);      
         if(variable == "r") reader->AddVariable("r",&r);
         if(variable == "S0rawPerp") reader->AddVariable("S0rawPerp",&S0rawPerp);
         if(variable == "S0axisrawZ") reader->AddVariable("S0axisrawZ",&S0axisrawZ);
         if(variable == "phi_S0axisraw") reader->AddVariable("phi_S0axisraw",&phi_S0axisraw);
         if(variable == "nCT") reader->AddVariable("nCT",&nCT);
         if(variable == "nGT") reader->AddVariable("nGT",&nGT);
         if(variable == "tracksdca") reader->AddVariable("tracksdca",&tracksdca);
         if(variable == "curvemin") reader->AddVariable("curvemin",&curvemin);
         if(variable == "curvemean") reader->AddVariable("curvemean",&curvemean);
         if(variable == "lambdamin") reader->AddVariable("lambdamin",&lambdamin);
         if(variable == "lambdamean") reader->AddVariable("lambdamean",&lambdamean);
         if(variable == "curvesign") reader->AddVariable("curvesign",&curvesign);
         if(variable == "phi") reader->AddVariable("phi",&phi);    
         if(variable == "nsphits") reader->AddVariable("nsphits",&nsphits);    
         if(variable == "npadhits") reader->AddVariable("npadhits",&npadhits);    
         if(variable == "nwirehits") reader->AddVariable("nwirehits",&nwirehits); 

         //Cosmic dumper
         if(variable == "usedresidual") reader->AddVariable("usedresidual",&usedresidual);
         if(variable == "meanusedresidual") reader->AddVariable("meanusedresidual",&meanusedresidual);
         if(variable == "totalresidual") reader->AddVariable("totalresidual",&totalresidual);
         if(variable == "meantotalresidual") reader->AddVariable("meantotalresidual",&meantotalresidual);

         //BV NumVariables
         if(variable == "numBars") reader->AddVariable("numBars",&fNumBars); 
         if(variable == "numEnds") reader->AddVariable("numEnds",&fNumEnds); 
         if(variable == "fNumADC") reader->AddVariable("fNumADC",&fNumADC);
         if(variable == "fNumClusters") reader->AddVariable("fNumClusters",&fNumClusters);
         if(variable == "fNumTDC") reader->AddVariable("fNumTDC",&fNumTDC);
         if(variable == "fNumBarsComp") reader->AddVariable("fNumBarsComp",&fNumBarsComp);
         if(variable == "fNumBarsHComp") reader->AddVariable("fNumBarsHComp",&fNumBarsHComp);
         if(variable == "fNumMBars") reader->AddVariable("fNumMBars",&fNumMBars);
         if(variable == "fNumMBarsComp") reader->AddVariable("fNumMBarsComp",&fNumMBarsComp);
         if(variable == "fNumMBarsHComp") reader->AddVariable("fNumMBarsHComp",&fNumMBarsHComp);

         // Backwards compatibility
         if(variable == "fNumHalfBarClusters") reader->AddVariable("fNumHalfBarClusters",&fNumClusters);

         // BV TOF MVA variables
         for (int ivar=0; ivar<NUM_BV_VARS; ivar++) {
            if (!TBarMVAVars::IsVarUsed(TBarMVAVars::GetVarName(ivar))) continue;
            if (variable == TBarMVAVars::GetVarName(ivar)) reader->AddVariable(TBarMVAVars::GetVarName(ivar), &bv_var[ivar]);
         }

      }
  


      std::string methodname = foldername.substr( foldername.find_last_of("_") + 1, 10);
      methodname = methodname.substr( 0, methodname.find_first_of("."));

      reader->BookMVA( methodname.c_str(),  foldername.c_str() );
   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("OfflineAGMVA::EndRun, run %d\n", runinfo->fRunNo);
   }
   
   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("OfflineAGMVA::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }
  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      if(!fFlags->fUseMVA)
        return flow;

      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!agfe)
      {
#ifdef MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      TStoreEvent* storeEvent=agfe->fEvent;
      if( (storeEvent->GetEventNumber()%1000)==0 )
      {
         fFlags->fPrintMVAValue = true;
      }
      //THIS IS ONLY REQUIRED IF YOU TRAINED WITH A CUT
      if(storeEvent->GetVertexStatus() <= 0)
      {
         agfe->fEvent->SetMVAValue(-99);
         return flow;
      }
         
      const TBarEvent* barEvent = storeEvent->GetBarEvent();      

      X                = storeEvent->GetVertexX();
      Y                = storeEvent->GetVertexY();
      Z                = storeEvent->GetVertexZ();
      vStatus          = storeEvent->GetVertexStatus();
      barMultiplicity  = storeEvent->GetBarMultiplicity();
      pointsPerTrack   = storeEvent->GetNumberOfPointsPerTrack();
      meanRSigma       = storeEvent->GetMeanRSigma();
      meanZSigma       = storeEvent->GetMeanZSigma();
      numTracks        = storeEvent->GetNumberOfTracks();
      numPoints        = storeEvent->GetNumberOfPoints();
      
      AGOnlineMVAFlow* agmva=flow->Find<AGOnlineMVAFlow>();

      if(!agmva)
         return flow;
         
      OnlineAGMVAStruct* onlineVars = agmva->dumper_event;
      nhits                      = onlineVars->nhits;      
      r                          = onlineVars->r;
      S0rawPerp                  = onlineVars->S0rawPerp;
      S0axisrawZ                 = onlineVars->S0axisrawZ;
      phi_S0axisraw              = onlineVars->phi_S0axisraw;
      nCT                        = onlineVars->nCT;
      nGT                        = onlineVars->nGT;
      tracksdca                  = onlineVars->tracksdca;
      curvemin                   = onlineVars->curvemin;
      curvemean                  = onlineVars->curvemean;
      lambdamin                  = onlineVars->lambdamin;
      lambdamean                 = onlineVars->lambdamean;
      curvesign                  = onlineVars->curvesign;
      phi                        = onlineVars->phi; 

      totalresidual              = onlineVars->totalresidual;
      meantotalresidual          = onlineVars->meantotalresidual;
      usedresidual               = onlineVars->usedresidual;
      meanusedresidual           = onlineVars->meanusedresidual;

      fNumBars = barEvent->GetNumBars();
      fNumEnds = barEvent->GetNumEnds();
      fNumADC = barEvent->GetNumADC();
      fNumTDC = barEvent->GetNumTDC();

      fNumBarsComp = barEvent->GetNumBarsComplete();
      fNumBarsHComp = barEvent->GetNumBarsHalfComplete();
      fNumMBars= barEvent->GetNumMatchedBars();
      fNumMBarsComp = barEvent->GetNumMatchedBarsComplete();
      fNumMBarsHComp = barEvent->GetNumMatchedBarsHalfComplete();

      fNumClusters = barEvent->GetNumClusters();

      TBarMVAVars myMVAvars(*barEvent);
      myMVAvars.CalculateVars();
      myMVAvars.EnforceValueRange(20);
      for (int ivar=0; ivar<NUM_BV_VARS; ivar++) {
         bv_var[ivar] = myMVAvars.GetVar(ivar);
      }

      double rfout = reader->EvaluateMVA( "BDT" );
      agfe->fEvent->SetMVAValue(rfout);

      if(fFlags->fPrintMVAValue)
      {
         std::cout << "Book event " << storeEvent->GetEventNumber() << ": rfout value = " << agfe->fEvent->GetMVAValue() << ".\n";
         fFlags->fPrintMVAValue = false;
      }
      //std::cout << "\nWE HAVE SUCCESSFULLY BOOKED AN EVENT. IT GAVE CUT VALUE OF: " << agfe->fEvent->GetMVAValue() << "\n";

      return flow; 
  }

};

class OfflineAGMVAFactory: public TAFactory
{
public:
   OfflineAGMVAFlags fFlags;

public:
   void Help()
   {
      std::cout << "OfflineAGMVAFactory::Help\n";
      std::cout << "\t--usemva [myModel.xml]\t\tEnable event classification using "
         << "the trained XML model file. If you do not set a model, the default "
         << "will be used (" << fFlags.fXMLModel << ")\n";
   }
   void Usage()
   {
      Help();
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("OfflineAGMVAFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
        if (args[i] == "--usemva")
         {
            fFlags.fUseMVA = true;
            if(i == args.size()-1)
            {
               std::cout << "Using default MVA model, if you want to select a model do --usemva myModel\n";
               std::cout << fFlags.fXMLModel << "\n";
            }
            else if(args[i+1].substr(fFlags.fXMLModel.size() - 4,4) != ".xml")
            {
               std::cerr << "Non xml model given, using default.\n";
               std::cout << fFlags.fXMLModel << "\n";
            }
            else
            {
               fFlags.fXMLModel = args[++i];
            }
         }
        else
        {
            fFlags.fUseMVA = true;
        }
      }
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("OfflineAGMVAFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("OfflineAGMVAFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new OfflineAGMVA(runinfo, &fFlags);
   }
};

static TARegister tar(new OfflineAGMVAFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */


