#include "AgFlow.h"
#include "RecoFlow.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TBarEvent.hh"
#include "TBarEndHit.h"
#include "TBarADCHit.h"
#include "TBarTDCHit.h"
#include "TBarHit.h"


#include "SignalsType.hh"
#include <set>
#include <iostream>

#include "AnaSettings.hh"
class QoDBVFlags
{
public:
   bool fDiag=false;
   AnaSettings* ana_settings=NULL;

public:
   QoDBVFlags() // ctor
   { }

   ~QoDBVFlags() // dtor
   { }
};

class QoDBVModule: public TARunObject
{
public:
   QoDBVFlags* fFlags = NULL;
   int fCounter;
   bool fTrace = false;
  
private:

   // Occupancy
   TLegend* leg1 = NULL;
   TH1D* hAdcNoise = NULL;
   TH1D* hAdcOcc = NULL;
   TH1D* hMatchOcc = NULL;
   TH1D* hTdcOcc = NULL;
   TLegend* leg3 = NULL;
   TH1D* hBarOccHalf = NULL;
   TH1D* hBarOccComplete = NULL;
   TH1D* hBVTPCOcc = NULL;

   // Number of hits
   TLegend* leg4 = NULL;
   TH1D* hNClusters = NULL;
   TH1D* hNBars = NULL;
   TLegend* leg5 = NULL;
   TH1D* hNAdc = NULL;
   TH1D* hNAdcGood = NULL;
   TH1D* hNTdc = NULL;
   TH1D* hNMatch = NULL;

   // ADC quantities
   TH2D* hAdcAmp = NULL;
   TLegend* leg2 = NULL;
   TH1D* hAdcFit = NULL;
   TH1D* hAdcRaw = NULL;

   // TDC quantities
   TH2D* hTdcMinusAdcTime = NULL;
   TH2D* hFineTime = NULL;

   // Calculated quantites
   TH2D* hTOFmDist = NULL;
   TH2D* hTOFvsDist = NULL;
   TH2D* hBarHitZed = NULL;
   TH2D* hBVTPCDeltaZed = NULL;



   TCanvas fQODBVCanvas;

public:
   QoDBVModule(TARunInfo* runinfo, QoDBVFlags* f):
      TARunObject(runinfo),
      fFlags(f),
      fCounter(0),
      fQODBVCanvas("LiveQODBV","LiveQODBV",3600,2400)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="QualityOfDataBarrelVeto";
#endif
   }

   ~QoDBVModule()
   {
      if (hAdcNoise)
         delete hAdcNoise;
      if (hAdcOcc)
         delete hAdcOcc;
      if (hTdcOcc)
         delete hTdcOcc;
      if (hMatchOcc)
         delete hMatchOcc;
      if (hBarOccHalf)
         delete hBarOccHalf;
      if (hBarOccComplete)
         delete hBarOccComplete;
      if (hBarHitZed)
         delete hBarHitZed;
      if (hNAdc)
         delete hNAdc;
      if (hNAdcGood)
         delete hNAdcGood;
      if (hNTdc)
         delete hNTdc;
      if (hNMatch)
         delete hNMatch;
      if (hTdcMinusAdcTime)
         delete hTdcMinusAdcTime;
      if (hBVTPCDeltaZed)
         delete hBVTPCDeltaZed;
      if (hBVTPCOcc)
         delete hBVTPCOcc;
      if (leg1)
         delete leg1;
      if (leg3)
         delete leg3;
      if (leg4)
         delete leg4;
      if (leg5)
         delete leg5;
      if (hNClusters)
         delete hNClusters;
      if (hNBars)
         delete hNBars;
      if (hAdcAmp)
         delete hAdcAmp;
      if (hAdcFit)
         delete hAdcFit;
      if (hAdcRaw)
         delete hAdcRaw;
      if (hFineTime)
         delete hFineTime;
      if (hTOFmDist)
         delete hTOFmDist;
      if (hTOFvsDist)
         delete hTOFvsDist;
    }

   void BeginRun(TARunInfo* runinfo)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      if(!fFlags->fDiag) return;
      printf("QoDBVModule::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      fCounter = 0;

      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      gDirectory->mkdir("QODBV")->cd();      

      // ADC occupancy
      hAdcNoise = new TH1D("hAdcNoise","Occupancy per ADC channel;ADC channel number",128,-0.5,127.5);
      hAdcNoise->SetMinimum(0.);
      hAdcNoise->SetLineColor(kGray+2); hAdcNoise->SetFillColor(kGray+2); hAdcNoise->SetFillStyle(3003);
      hAdcNoise->GetXaxis()->SetNdivisions(8,16);
      hAdcOcc = new TH1D("hAdcOcc","Occupancy per ADC channel;ADC channel number",128,-0.5,127.5);
      hAdcOcc->SetMinimum(0.);
      hAdcOcc->SetLineColor(kBlue); hAdcOcc->SetFillColor(kBlue); hAdcOcc->SetFillStyle(3003);
      hAdcOcc->GetXaxis()->SetNdivisions(8,16);
      hMatchOcc = new TH1D("hMatchOcc","ADC+TDC matches per channel;Channel number",128,-0.5,127.5);
      hMatchOcc->SetMinimum(0.);
      hMatchOcc->SetLineColor(kGreen); hMatchOcc->SetFillColor(kGreen); hMatchOcc->SetFillStyle(3003);
      hMatchOcc->GetXaxis()->SetNdivisions(8,16);

      // TDC occupancy
      hTdcOcc = new TH1D("hTdcOcc","Occupancy per TDC channel;TDC channel number",128,-0.5,127.5);
      hTdcOcc->SetLineColor(kRed); hTdcOcc->SetFillColor(kRed); hTdcOcc->SetFillStyle(3003);
      hTdcOcc->SetMinimum(0.);
      hTdcOcc->GetXaxis()->SetNdivisions(8,16);

      // Bar occupancy
      hBarOccHalf = new TH1D("hBarOccHalf","Occupancy per bar;Bar number",64,-0.5,63.5);
      hBarOccHalf->SetLineColor(kBlue); hBarOccHalf->SetFillColor(kBlue); hBarOccHalf->SetFillStyle(3003);
      hBarOccHalf->SetMinimum(0.);
      hBarOccHalf->GetXaxis()->SetNdivisions(8,16);
      hBarOccComplete = new TH1D("hBarOccComplete","Occupancy per bar (complete);Bar number",64,-0.5,63.5);
      hBarOccComplete->SetLineColor(kGreen); hBarOccComplete->SetFillColor(kGreen); hBarOccComplete->SetFillStyle(3003);
      hBarOccComplete->SetMinimum(0.);
      hBarOccComplete->GetXaxis()->SetNdivisions(8,16);
      hBVTPCOcc = new TH1D("hBVTPCOcc","BV hits matched to TPC;Bar number",64,-0.5,63.5);
      hBVTPCOcc->SetLineColor(kRed); hBVTPCOcc->SetFillColor(kRed); hBVTPCOcc->SetFillStyle(3003);
      hBVTPCOcc->SetMinimum(0.);
      hBVTPCOcc->GetXaxis()->SetNdivisions(8,16);

      // Number of hits
      hNClusters = new TH1D("hNClusters","Number of clusters per event;Number of hits",19,-0.5,18.5);
      hNClusters->SetLineColor(kBlack); hNClusters->SetFillColor(kBlack); hNClusters->SetFillStyle(3003);
      hNClusters->SetMinimum(0.); hNClusters->SetStats(0);
      hNClusters->GetXaxis()->SetNdivisions(8,16);
      hNBars = new TH1D("hNBars","Number of bar hits per event;Number of hits",19,-0.5,18.5);
      hNBars->SetLineColor(kMagenta); hNBars->SetFillColor(kMagenta); hNBars->SetFillStyle(3003);
      hNBars->SetMinimum(0.); hNBars->SetStats(0);
      hNBars->GetXaxis()->SetNdivisions(8,16);
      hNAdc = new TH1D("hNAdc","Number of ADC hits per event;Number of ADC hits",19,-0.5,18.5);
      hNAdc->SetLineColor(kGray+2); hNAdc->SetFillColor(kGray+2); hNAdc->SetFillStyle(3003);
      hNAdc->SetMinimum(0.); hNAdc->SetStats(0);
      hNAdc->GetXaxis()->SetNdivisions(8,16);
      hNAdcGood = new TH1D("hNAdcGood","Number of good ADC hits per event;Number of ADC hits",19,-0.5,18.5);
      hNAdcGood->SetLineColor(kBlue); hNAdcGood->SetFillColor(kBlue); hNAdcGood->SetFillStyle(3003);
      hNAdcGood->SetMinimum(0.); hNAdcGood->SetStats(0);
      hNAdcGood->GetXaxis()->SetNdivisions(8,16);
      hNTdc = new TH1D("hNTdc","Number of TDC hits per event;Number of TDC hits",19,-0.5,18.5);
      hNTdc->SetLineColor(kRed); hNTdc->SetFillColor(kRed); hNTdc->SetFillStyle(3003);
      hNTdc->SetMinimum(0.); hNTdc->SetStats(0);
      hNTdc->GetXaxis()->SetNdivisions(8,16);
      hNMatch = new TH1D("hNMatch","Number of ADC+TDC matches per event;Number of ADC+TDC matches",19,-0.5,18.5);
      hNMatch->SetLineColor(kGreen); hNMatch->SetFillColor(kGreen); hNMatch->SetFillStyle(3003);
      hNMatch->SetMinimum(0.); hNMatch->SetStats(0);
      hNMatch->GetXaxis()->SetNdivisions(8,16);

      hAdcAmp = new TH2D("hAdcAmp","ADC amplitude per channel;Bar End number;ADC Amplitude",128,-0.5,127.5,100,0,30000.);
      hAdcAmp->GetXaxis()->SetNdivisions(8,16); hAdcAmp->SetStats(0);
      hAdcFit = new TH1D("hAdcFit","ADC amplitude with fitting;Fit ADC Amplitude",200,0,50000.);
      hAdcFit->SetLineColor(kRed); hAdcFit->SetFillColor(kRed); hAdcFit->SetFillStyle(3003);
      hAdcFit->GetXaxis()->SetNdivisions(8,16); hAdcFit->SetStats(0);
      hAdcRaw = new TH1D("hAdcRaw","ADC amplitude without fitting;Raw ADC Amplitude",200,0,50000.);
      hAdcRaw->SetLineColor(kBlue); hAdcRaw->SetFillColor(kBlue); hAdcRaw->SetFillStyle(3003);
      hAdcRaw->GetXaxis()->SetNdivisions(8,16); hAdcRaw->SetStats(0);
      hTdcMinusAdcTime = new TH2D("hTdcMinusAdcTime","TDC - ADC time difference;Bar End number;TDC time - ADC time [ns]",128,-0.5,127.5,200,-20,20);
      hTdcMinusAdcTime->GetXaxis()->SetNdivisions(8,16); hTdcMinusAdcTime->SetStats(0);
      hTOFmDist = new TH2D("hTOFmDist","Time of flight minus distance;Bar number;TOF - distance/c [ns]",64,-0.5,63.5,100,-5,5);
      hTOFmDist->GetXaxis()->SetNdivisions(8,16); hTOFmDist->SetStats(0);
      hTOFvsDist = new TH2D("hTOFvsDist","Time of flight vs distance;Distance/c [ns];TOF [ns]",100,0,5,100,0,5);
      hTOFvsDist->GetXaxis()->SetNdivisions(8,16); hTOFvsDist->SetStats(0);

      hBarHitZed = new TH2D("hBarHitZed","Zed position;Bar number;Zed position [m]",64,-0.5,63.5,100,-2,2);
      hBarHitZed->GetXaxis()->SetNdivisions(8,16); hBarHitZed->SetStats(0);
      hBVTPCDeltaZed = new TH2D("hBVTPCDeltaZed","BV-TPC zed difference;Bar number;Zed BV - zed TPC [m]",64,-0.5,63.5,100,-0.5,0.5);
      hBVTPCDeltaZed->GetXaxis()->SetNdivisions(8,16); hBVTPCDeltaZed->SetStats(0);

      hFineTime = new TH2D("hFineTime","Fine time;Bar End number;Fine time [ns]",128,-0.5,127.5,200,-1,6);
      hFineTime->GetXaxis()->SetNdivisions(8,16); hFineTime->SetStats(0);

      leg1 = new TLegend(0.55,0.75,0.75,0.9);
      leg1->AddEntry(hAdcNoise,"ADC all hits","f");
      leg1->AddEntry(hAdcOcc,"ADC good hits","f");
      leg1->AddEntry(hTdcOcc,"TDC hits","f");
      leg1->AddEntry(hMatchOcc,"ADC+TDC matches","f");

      leg2 = new TLegend(0.65,0.75,0.95,0.9);
      leg2->AddEntry(hAdcRaw,"Raw ADC amplitude","f");
      leg2->AddEntry(hAdcFit,"Fit ADC amplitude","f");

      leg3 = new TLegend(0.1,0.65,0.4,0.95);
      leg3->AddEntry(hBarOccHalf,"Bar Hits","f");
      leg3->AddEntry(hBarOccComplete,"Complete Bar Hits","f");
      leg3->AddEntry(hBVTPCOcc,"Bar Hits (+TPC)","f");

      leg4 = new TLegend(0.65,0.65,0.95,0.9);
      leg4->AddEntry(hNAdc,"ADC all hits","f");
      leg4->AddEntry(hNAdcGood,"ADC good hits","f");
      leg4->AddEntry(hNTdc,"TDC hits","f");
      leg4->AddEntry(hNMatch,"ADC+TDC matches","f");
      leg5 = new TLegend(0.65,0.65,0.95,0.9);
      leg5->AddEntry(hNBars,"Bar hits","f");
      leg5->AddEntry(hNClusters,"Bar clusters","f");

      fQODBVCanvas.Divide(2,3);
      TVirtualPad* p1 = fQODBVCanvas.cd(1);
      p1->Divide(2,1);
      TVirtualPad* p2 = fQODBVCanvas.cd(2);
      p2->Divide(2,1);
      TVirtualPad* p3 = fQODBVCanvas.cd(3);
      p3->Divide(2,1);
      TVirtualPad* p4 = fQODBVCanvas.cd(4);
      p4->Divide(2,1);
      TVirtualPad* p5 = fQODBVCanvas.cd(5);
      p5->Divide(2,1);
      TVirtualPad* p6 = fQODBVCanvas.cd(6);
      p6->Divide(2,1);

      //Reset root directory
      runinfo->fRoot->fOutputFile->cd();
   }

   void EndRun(TARunInfo* runinfo)
   {
      printf("QoDBVModule::EndRun, run %d    Total Counter %d\n", runinfo->fRunNo, fCounter);
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      fQODBVCanvas.Write();

   }

   TAFlowEvent* AnalyzeFlowEvent( __attribute__((unused)) TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {      
      if(!fFlags->fDiag)
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      const AgBarEventFlow* barFlow = flow->Find<AgBarEventFlow>();
      if( !barFlow )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      const TBarEvent* barEvt = barFlow->BarEvent;
      FillDiagHistos(barEvt);
      
      // Update Live histograms every 500 events (this doesn't need to update very often)
      if (fCounter % 500 == 0)
      {
         std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
         TVirtualPad* p1 = fQODBVCanvas.cd(1);
         p1->cd(1);
         hAdcNoise->SetMaximum(hAdcNoise->GetEntries()*1.5/128.);
         hAdcNoise->SetTitle("Bar End Occupancy;End number");
         hAdcNoise->Draw();
         hAdcOcc->Draw("same");
         hTdcOcc->Draw("same");
         hMatchOcc->Draw("same");
         leg1->Draw();
         gPad->SetGrid();

         p1->cd(2);
         hBarOccHalf->SetTitle("Bar Occuapancy;Bar Number");
         hBarOccHalf->Draw();
         hBarOccComplete->Draw("same");
         hBVTPCOcc->Draw("same");
         leg3->Draw();
         gPad->SetGrid();

         TVirtualPad* p2 = fQODBVCanvas.cd(2);
         p2->cd(2);
         hNClusters->SetTitle("Bar Hits Per Event;Number of hits");
         hNClusters->Draw();
         hNBars->Draw("same");
         leg5->Draw();
         gPad->SetGrid();
         
         p2->cd(1);
         hNMatch->SetTitle("End Hits Per Event;Number of hits");
         hNMatch->Draw();
         hNTdc->Draw("same");
         hNAdcGood->Draw("same");
         hNAdc->Draw("same");
         leg4->Draw();
         gPad->SetGrid();

         TVirtualPad* p3 = fQODBVCanvas.cd(3);
         TVirtualPad* p31 = p3->cd(1);
         p31->SetLogy();
         hAdcRaw->SetTitle("ADC amplitude;ADC Amplitude");
         hAdcRaw->Draw();
         hAdcFit->Draw("same");
         leg2->Draw();
         p31->SetLogy();
         gPad->SetGrid();
         gPad->Update();

         TVirtualPad* p32 = p3->cd(2);
         p32->SetLogz();
         p32->SetLeftMargin(0.15);
         hAdcAmp->Draw("colz");
         gPad->SetGrid();
         p32->SetLogz();
         gPad->Update();

         TVirtualPad* p4 = fQODBVCanvas.cd(4);
         p4->cd(1);
         hFineTime->Draw("colz");
         gPad->SetGrid();

         p4->cd(2);
         hTdcMinusAdcTime->Draw("colz");
         gPad->SetGrid();

         TVirtualPad* p5 = fQODBVCanvas.cd(5);
         p5->cd(1);
         hBarHitZed->Draw("colz");
         gPad->SetGrid();

         p5->cd(2);
         hBVTPCDeltaZed->Draw("colz");
         gPad->SetGrid();

         TVirtualPad* p6 = fQODBVCanvas.cd(6);
         p6->cd(1);
         hTOFvsDist->Draw("colz");
         gPad->SetGrid();

         p6->cd(2);
         hTOFmDist->Draw("colz");
         gPad->SetGrid();

         fQODBVCanvas.Draw();
      }

      ++fCounter;
      return flow;
   }

   void FillDiagHistos(const TBarEvent* barEvt) {

      // Event histograms
      const int NAdc = barEvt->GetNumADC();
      const int NMatch = barEvt->GetNumEnds();
      const int NBarsHalf = barEvt->GetNumBarsHalfComplete();
      const int NBarClusters = barEvt->GetNumClusters();
      hNAdc->Fill(NAdc);
      hNMatch->Fill(NMatch);
      hNBars->Fill(NBarsHalf);
      hNClusters->Fill(NBarClusters);
      
      // ADC histograms
      for (const TBarADCHit &h: barEvt->GetADCHits()) {
         const int endID = h.GetEndID();
         hAdcNoise->Fill(endID);
         if (!h.IsGood()) continue;
         hAdcOcc->Fill(endID);
      }

      // TDC histograms
      int n_tdc = 0;
      for (const TBarTDCHit &h: barEvt->GetTDCHits()) {
         if (!h.IsGood()) continue;
         const int endID = h.GetEndID();
         const double finetime = h.GetFineTime()*1e9;
         hFineTime->Fill(endID,finetime);
         hTdcOcc->Fill(endID);
         n_tdc++;
      }
      hNTdc->Fill(n_tdc);

      // EndHit histograms
      for (const TBarEndHit &h: barEvt->GetEndHits()) {
         const int endID = h.GetEndID();
         const double ampl_fit = h.GetAmpFit();
         const double ampl_raw = h.GetAmpRaw();
         const double timediff = (h.GetTime()*1e9 - barEvt->GetEventTDCTime()*1e9) - h.GetADCTime();
         hMatchOcc->Fill(endID);
         hAdcAmp->Fill(endID,ampl_fit);
         hAdcFit->Fill(ampl_fit);
         hAdcRaw->Fill(ampl_raw);
         if (timediff!=0) hTdcMinusAdcTime->Fill(endID,timediff);
      }

      // BarHit histograms
      for (const TBarHit &h: barEvt->GetBarHits()) {
         if (!h.IsHalfComplete()) continue;
         const int barID = h.GetBarID();
         hBarOccHalf->Fill(barID);
      }
      for (const TBarHit &h: barEvt->GetBarHits()) {
         if (!h.IsComplete()) continue;
         const int barID = h.GetBarID();
         const double z = h.GetZed();
         hBarOccComplete->Fill(barID);
         hBarHitZed->Fill(barID,z);
      }
      for (const TBarHit &h: barEvt->GetBarHits()) {
         if (!h.IsComplete()) continue;
         if (!h.HasTPCHit()) continue;
         const int barID = h.GetBarID();
         const double delta_z = h.GetZed() - h.GetZedTPC();
         //const double delta_phi = (h.Get3Vector()).DeltaPhi(h.GetTPCHit())*(32/TMath::Pi());
         hBVTPCDeltaZed->Fill(barID,delta_z);
      }
      for (const TBarHit &h: barEvt->GetBarHits()) {
         if (!h.IsHalfComplete()) continue;
         if (!h.HasTPCHit()) continue;
         const int barID = h.GetBarID();
         hBVTPCOcc->Fill(barID);
      }
      for (const TBarTOF &tof: barEvt->GetTOFs()) {
         if (!tof.IsComplete()) continue;
         double TOFmDist = tof.GetTOFmDist()*1e9;
         double TOF = tof.GetTOF()*1e9;
         double distoverc = tof.GetDistanceOverC()*1e9;
         double barID = tof.GetFirstHit().GetBarID();
         hTOFmDist->Fill(barID,TOFmDist);
         hTOFvsDist->Fill(distoverc,TOF);
      }

   }

};


class QoDBVModuleFactory: public TAFactory
{
public:
   QoDBVFlags fFlags;
   
public:
   void Usage()
   {
      printf("QODModule::Help\n");
      printf("\t--qod Enable this module\n ");
   }
   void Init(const std::vector<std::string> &args)
   {
      TString json = "default";
      printf("QoDBVModuleFactory::Init!\n");

      for (size_t i=0; i<args.size(); i++) {
         if( args[i] == "--qod" || args[i] == "--QOD")
         {
            fFlags.fDiag = true;
         }
         else if( args[i] == "--anasettings" )
            json=args[++i];
      }
      fFlags.ana_settings=new AnaSettings(json.Data(),args);
   }

   void Finish()
   {
      printf("BVModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("QoDBVModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new QoDBVModule(runinfo, &fFlags);
   }
};

static TARegister tar(new QoDBVModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
