//
// ALPHAg (Chronobox) spill_log_module
//
// JTK McKenna
//


#include <list>
#include <stdio.h>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include "manalyzer.h"
#include "midasio.h"
#include "TSystem.h"
#include <TEnv.h>

#include "AgFlow.h"
#include "AnalysisFlow.h"
#include "RecoFlow.h"

#include "store_cb.h"

#include "TChronoChannel.h"
#include "TChronoChannelName.h"
#include "TROOT.h"
#include "TTree.h"

#include <vector>
//MAX DET defined here:
#include "TSpill.h"

#ifdef HAVE_MIDAS
#include "midas.h"
#include "msystem.h"
#include "mrpc.h"
#endif

#define DELETE(x) if (x) { delete (x); (x) = NULL; }

#define MEMZERO(p) memset((p), 0, sizeof(p))

#define HOT_DUMP_LOW_THR 500

time_t LastUpdate;
//struct tm LastUpdate = {0};


#ifdef HAVE_XMLSERVER
#include "xmlServer.h"
#include "TROOT.h"
#endif

#ifdef XHAVE_LIBNETDIRECTORY
#include "netDirectoryServer.h"
#endif


class SpillLogFlags
{
public:
   bool fPrint = false;
   bool fWriteElog = false;
   bool fWriteSpillDB = false;
   bool fWriteSpillTxt = false;
   bool fNoSpillSummary = false;
   bool fOnlineSpillLog = false;
   bool fVertsInSpillLog = false;
   bool fMVAInSpillLog = false;
};

#if HAVE_MIDAS

class SpillLogPrinter
{
   private:
      std::array<std::string,2> fSpillLogTitle;
      int fSpillLogLineNumber;
      const int fPrintInterval;
   public:
   SpillLogPrinter(): fPrintInterval(25)
   {
     fSpillLogLineNumber = 0;
   }
   ~SpillLogPrinter()
   {
     // The new manalzyer calls the dtor between runs... 
     //cm_msg1(MINFO, "SpillLog", "CHRONO","Exiting...");
   }
   void Reset()
   {
     fSpillLogLineNumber = 0;
   }
   void BeginRun(const std::array<std::string,2>& SpillLogTitle, int RunNo)
   {
      Reset();
      if (!RunNo)
         return;
      fSpillLogTitle = SpillLogTitle;
      size_t width = 0;
      for (const std::string& s: fSpillLogTitle)
         if (s.size() > width)
            width = s.size();
      
      int indent = width / 2 - 52/2;
      std::string logo_indent(indent,' ');
      //For readability make a large break from the last run

      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                             ############                          ####           ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                         #####################                   #########        ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                      ###########       ##########             ############       ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                    ##########             #########          ##############      ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                  ###########                #########       #######  ######      ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                 ##########                   #########     #####        ####     ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                ###########                    #########   #####          ###     ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                               ###########                      #########  ###            ###     ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                              ############                       #############             ##     ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                              ###########                         ###########     @@@@@           ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                             ############                          ##########   @@@@  @@@@        ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                             ############                          ##########           @@@       ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                             #   #######  @@@     @@@ @@@       @@@ #########     @@@@@@@@@       ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "     @@@     @@@   @@@@@@@   #   #######  @@@     @@@  @@@      @@   #########   @@@    @@@       ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "      @@@@@@@@@    @@        #   #######  @@@     @@@   @@@@@@@@@    ##########  @@@     @@@      ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "       @@  @@@     @@        #         #  @@@@@@@@@@@    @@@  @@@    ##########   @@@    @@@      ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "        @@ @@      @@        #   ####     @@@     @@@     @@@@@@     ###########   @@@@@@@@@@     ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "         @@@       @@        #         #  @@@     @@@      @@@@     #############    @@@@ @@@     ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                             ############                           #############                 ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                             ############                           ##############                ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                              ###########                          ##### ##########               ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                              ###########                          ####   #########               ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                               ###########                        #####   ##########              ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                ##########                       #####     ##########             ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                 ##########                     ######      #########             ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                  ##########                   ######       ##########            ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                   ##########                 ######         ##########           ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                     ##########            #######            #########           ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                       ###########     #########              ##########          ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                          ####################                 ##########         ");
      cm_msg1(MINFO, "SpillLog", "SIS", "%s%s", logo_indent.c_str(), "                                              ############                                        ");


      std::string line(width,'-');
      std::string first_line(line);
      std::string title = std::string("# Begin run ") + std::to_string(RunNo) + std::string(" #");
      if (title.size() < first_line.size() + 10)
      {
         size_t title_pos = 0;
         int line_pos = line.size() / 2 - title.size() / 2; 
         while (title_pos < title.size())
         {
            first_line.at(line_pos++ ) = title.at(title_pos++);
         }
      }
      else
      {
         first_line = title;
      }
      
      //cm_msg_log((INT)MINFO, (const char*) "SpillLog", (const char*) first_line.c_str() );
      cm_msg1(MINFO, "SpillLog", "CHRONO", "%s", first_line.c_str());
      //cm_msg1(MINFO, "SpillLog", "CHRONO", "%s", line.c_str());
      for (const std::string& s: fSpillLogTitle)
         cm_msg1(MINFO, "SpillLog", "CHRONO", "%s", s.c_str());
      cm_msg1(MINFO, "SpillLog", "CHRONO", "%s", line.c_str());
      fSpillLogLineNumber++;
   }
   void EndRun(int RunNo)
   {
      size_t width = 0;
      for (const std::string& s: fSpillLogTitle)
         if (s.size() > width)
            width = s.size();
      std::string line(width,'=');
      cm_msg1(MINFO, "SpillLog", "CHRONO","%s", line.c_str());
      cm_msg1(MINFO, "SpillLog", "CHRONO","End run %d",RunNo);
   }
   
   void PrintLine(const char *string)
   {
      fSpillLogLineNumber++;
      if ( (fSpillLogLineNumber % fPrintInterval ) == 0 )
      {
         for (const std::string& s: fSpillLogTitle)
            cm_msg1(MINFO, "SpillLog", "CHRONO", "%s", s.data());
      }
      cm_msg1(MINFO, "SpillLog", "CHRONO", "%s", string);
   }
   
};
#endif

class SpillLog: public TARunObject
{
public: 
   SpillLogFlags* fFlags;
   bool fTrace = false;
   Int_t RunState =-1;
   Int_t gRunNumber =0;
   time_t run_start_time=0;
   time_t run_stop_time=0;
#ifdef HAVE_MIDAS
   SpillLogPrinter fSpillLogPrinter;
   MVOdb* alphagonline_variables = nullptr;
#endif
   std::vector<std::string> fHistoryDumps;

   std::vector<std::string> InMemorySpillTable;
   TTree* SpillTree = NULL;

   //Live spill log body:
   std::ofstream LiveSpillLog;
   //Column headers
   std::ofstream SpillLogHeader;
   //List of active dumps
   std::ofstream LiveSequenceLog[NUMSEQ];

   std::vector<TChronoChannel> chrono_channels;
   int n_chrono_channels;


   //Dump Markers to give timestamps (From DumpFlow)
   //std::vector<TString> Description[4];
   //std::vector<Int_t> DumpType[4]; //1=Start, 2=Stop
   //std::vector<Int_t> fonCount[4];
   // Int_t SequencerNum[4];
   Int_t gADSpillNumber;
   TSpill* gADSpill;
   TChronoChannel gADSpillChannel={"",-1};

   Int_t gPOSSpillNumber;
   TChronoChannel gPOSSpillChannel={"",-1};
   // Dont hard code this! Look up during Begin Run!
   TChronoChannel fA_or_D={"cb02",22};
private:
   sqlite3 *ppDb; //SpillLogDatabase handle
   sqlite3_stmt * stmt;
   std::array<std::string,2> fSpillLogTitle;
   uint32_t fDumpNameWidth = 35;
   uint32_t fSeqIndentation = 2;
   uint32_t fDetectorColumnWidth = 8;
public:

   SpillLog(TARunInfo* runinfo, SpillLogFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="AGSpillLog";
#endif
      if (fTrace)
         printf("SpillLog::ctor!\n");
   }

   ~SpillLog()
   {
      if (fTrace)
         printf("SpillLog::dtor!\n");
   }
   void SaveToTree(TARunInfo* runinfo,TAGSpill* s)
   {
         if (!s) return;
         std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
         runinfo->fRoot->fOutputFile->cd();
         if (!SpillTree)
            SpillTree = new TTree("AGSpillTree","AGSpillTree");
         TBranch* b_variable = SpillTree->GetBranch("TAGSpill");
         if (!b_variable)
            SpillTree->Branch("TAGSpill","TAGSpill",&s,16000,1);
         else
            SpillTree->SetBranchAddress("TAGSpill",&s);
         SpillTree->Fill();
   }


   void BeginRun(TARunInfo* runinfo)
   {
      TSpill::ClearDumpCounter();
      if (fTrace)
         printf("SpillLog::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());

      std::vector<std::string> channels =
      {
         //channel name, descriptive name
         "PMT_CATCH_OR",
         "SiPM_B",
         "SiPM_E",
         "TPC_TRIG",
         "SiPM_A_AND_D",
         "SiPM_C_AND_F",
         "SiPM A_OR_C-AND-D_OR_F",
         "SiPM_C",
         "SiPM_D",
         "SiPM_F"
      };

      std::vector<std::string> channel_names =
      {
         //channel name, descriptive name
         "CATCH OR",
         "TOP PM",
         "BOT PM",
         "TPC",
         "SiPM_A_AND_D",
         "SiPM_C_AND_F",
         "SiPM A_OR_C-AND-D_OR_F",
         "SiPM_C",
         "SiPM_D",
         "SiPM_F"
      };

      MVOdb* channel_settings = runinfo->fOdb->Chdir("Equipment/alphagonline/Settings", true);
      channel_settings->RSA("ChannelIDName",&channels, true, 60, 250);
      channel_settings->RSA("ChannelDisplayName",&channel_names, true, 60, 250);

      // Dumps to log as history items
      fHistoryDumps = 
      {
         "Mixing",
         "Hot Dump",
         "Tenth Dump",
         "Cold Dump"
      };
      // Dumps to log as history items
      channel_settings->RSA("HistoryDumps",&fHistoryDumps, true, 20, 80);

      channel_settings->RU32("DumpLabelWidth",&fDumpNameWidth,true);
      channel_settings->RU32("SequencerIdentationSize",&fSeqIndentation,true);
      channel_settings->RU32("DetectorColumnWidth",&fDetectorColumnWidth,true);
#ifdef HAVE_MIDAS
      alphagonline_variables = runinfo->fOdb->Chdir("Equipment/alphagonline/Variables", true);
#endif
      delete channel_settings;
      //Remove empty channels
      std::vector<std::string> tmpchan(channels);
      std::vector<std::string> tmpnames(channel_names);
      channels.clear();
      channel_names.clear();
      for (size_t i = 0; i < tmpchan.size(); i++)
      {
         if (tmpchan.at(i).size() || tmpnames.at(i).size())
         {
            channels.emplace_back(tmpchan.at(i));
            channel_names.emplace_back(tmpnames.at(i));
         }
      }
      //Print channel list into spill log
      std::string channel_summary = "Channel List: ";
      for (size_t i = 0; i < channels.size(); i++)
      {
         if (channels.at(i).empty()) continue;
         channel_summary += channels.at(i);
         channel_summary += " = ";
         channel_summary += channel_names.at(i);
         if (i != channels.size() - 1)
         channel_summary +=", ";
      }
#if HAVE_MIDAS
      cm_msg1(MINFO, "SpillLog", "alphagonline", "%s", channel_summary.c_str());
      //Check the number of Channels and Channel names match
      int n_chans = 0;
      int n_names = 0;
      for (const std::string& s: channels)
      {
         if (s.size())
            n_chans++;
      }
      for (const std::string& s: channel_names)
      {
         if (s.size())
            n_names++;
      }
      if (n_chans != n_names)
         cm_msg1(MERROR, "SpillLog", "CHRONO", "ChannelIDName entires (%d) does not match ChannelDisplayName entires (%d)",n_chans, n_names);
#endif


      for (size_t i=0; i<channels.size(); i++)
      {
         bool found = false;
         for (const std::pair<const std::string, int>& board: TChronoChannel::CBMAP)
         {
            TChronoChannelName name(runinfo->fOdb,board.first);
            int channel = name.GetChannel(channels.at(i));

            std::cout<<"CHANNEL"<<channel<<std::endl;
            if (channel >= 0)
            {
               found = true;
               chrono_channels.emplace_back(TChronoChannel(board.first,channel));
               break;
            }
            
            
            
         }
         if (!found)
            chrono_channels.emplace_back(TChronoChannel("",-1));
      }

      n_chrono_channels=chrono_channels.size();

      for (auto c: chrono_channels)
         std::cout<<"\t"<<c<<std::endl;
      fSpillLogTitle.at(0) = "            Dump Time            | ";
      fSpillLogTitle.at(1) = "                                 | ";

      std::array<std::string,2> dump_names;
      for (int i = 0; i < NUMSEQ; i++)
      {
         // Alternate rows for labels
         bool isEven = i % 2;
         // One row gets the label

         std::string seq_name = SEQ_NAMES_SHORT[i];
         // Pad until filled to right width
         const int spare_space_in_double_column = fSeqIndentation * 2 - seq_name.size();
         if ( spare_space_in_double_column > 0)
             seq_name.append(spare_space_in_double_column,' ' );
         dump_names.at( isEven)   += seq_name;
         // Other row gets ' ' the size of label
         if (i == 0)
            dump_names.at( !isEven ) += std::string(fSeqIndentation,' ');
      }
      //Pad to proper width
      for (size_t i = 0; i < dump_names.size(); i++)
      {
         dump_names.at(i).insert(
            dump_names.at(i).size(),
            fDumpNameWidth - dump_names.at(i).size() - 1,
            ' '
         );
         fSpillLogTitle.at(i) += dump_names.at(i) + std::string("|");
      }

      bool IsEven = false;
      int extra_chans = 0;
      std::vector<std::string> extra_chan_names;
      if (fFlags->fVertsInSpillLog) {
         extra_chans++;
         extra_chan_names.push_back("Vertices");
      }
      extra_chans++;
      extra_chan_names.push_back("Pass Cuts");
      if (fFlags->fMVAInSpillLog) {
         extra_chans++;
         extra_chan_names.push_back("MVA");
      }
      for (size_t i=0; i<channel_names.size() + extra_chans; i++)
      {
         std::string channel_name;
         if ( i < channel_names.size()) {
            if (channels.at(i).empty()) continue;
            channel_name = channel_names.at(i);
         } else {
            if (i < channel_names.size() + extra_chans) channel_name = extra_chan_names.at(i-channel_names.size());
         }
         std::string clipped_channel_name = channel_name.substr(0,fDetectorColumnWidth * 2);
         // Attempt to right justify the column (if there is too much text it wont work)
         const int spare_space =  fDetectorColumnWidth - clipped_channel_name.size();
         if (spare_space > 0)
            clipped_channel_name.insert(0, spare_space + 1,' ' );
         // Pad until filled to right width
         const int spare_space_in_double_column =  fDetectorColumnWidth * 2 + 2 - clipped_channel_name.size();
         if ( spare_space_in_double_column > 0)
            clipped_channel_name.append(spare_space_in_double_column,' ' );
         
         fSpillLogTitle.at(IsEven) += clipped_channel_name;
         if (i == 0)
            fSpillLogTitle.at(!IsEven) += std::string(fDetectorColumnWidth,' ');
         // Flip the IsEven (can't use i because of the continue statement)
         IsEven = !IsEven;
      }

#ifdef HAVE_MIDAS
      if (fFlags->fOnlineSpillLog)
      if (runinfo->fRunNo)
      {
         fSpillLogPrinter.BeginRun(fSpillLogTitle, runinfo->fRunNo);
      }
#endif
      if (fFlags->fWriteSpillDB)
      {
         if (sqlite3_open("SpillLog/AGSpillLog.db",&ppDb) == SQLITE_OK)
         {
            std::cout<<"Database opened ok"<<std::endl;
         }
         else
         {
            exit(555);
         }
      }
            if (!fFlags->fNoSpillSummary)
      {
         InMemorySpillTable.push_back("Begin run "+std::to_string(runinfo->fRunNo) );
         InMemorySpillTable.push_back(fSpillLogTitle.at(0));
         InMemorySpillTable.push_back(fSpillLogTitle.at(1));
         InMemorySpillTable.push_back(std::string(fSpillLogTitle.at(0).size(),'-'));
      }
      if (fFlags->fWriteSpillTxt)
      {
         //Live spill log body:
         LiveSpillLog.open("SpillLog/reload.txt");
         LiveSpillLog<<"Begin run "<<runinfo->fRunNo<<"\t\t"<<std::endl;

         //Column headers
         SpillLogHeader.open("SpillLog/title.txt");
         SpillLogHeader << fSpillLogTitle.at(0);
         SpillLogHeader << fSpillLogTitle.at(1);

         SpillLogHeader<<std::string(fSpillLogTitle.at(0).size(),'-')<<std::endl;
         SpillLogHeader.close();
         //List of active dumps

         for (int i=0; i<NUMSEQ; i++)
         {
            std::string name;

            name += "SpillLog/Sequencers/Seq";
            name += std::to_string(i);
            name += ".txt";
            std::cout<<name.c_str()<<std::endl;
            LiveSequenceLog[i].open(name.c_str());
         }
      }
      gRunNumber=runinfo->fRunNo;

#ifdef INCLUDE_VirtualOdb_H
      RunState=runinfo->fOdb->odbReadInt("/runinfo/State"); //The odb isn't in its 'final' state before run, so this isFile Edit Options Buffers Tools C++ Help   
      run_start_time = runinfo->fOdb->odbReadUint32("/Runinfo/Start time binary", 0, 0);
      run_stop_time = runinfo->fOdb->odbReadUint32("/Runinfo/Stop time binary", 0, 0);
#endif
#ifdef INCLUDE_MVODB_H
      runinfo->fOdb->RU32("Runinfo/Start time binary",(uint32_t*) &run_start_time);
      runinfo->fOdb->RU32("Runinfo/Stop time binary",(uint32_t*) &run_stop_time);
      runinfo->fOdb->RI("runinfo/State",&RunState);
#endif

      std::cout<<"START:"<< run_start_time<<std::endl;
      std::cout<<"STOP: "<< run_stop_time<<std::endl;

      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("SpillLog::EndRun, run %d\n", runinfo->fRunNo);
      //runinfo->State
      if (fFlags->fOnlineSpillLog)
      if (runinfo->fRunNo)
      {
#ifdef HAVE_MIDAS
         fSpillLogPrinter.EndRun(runinfo->fRunNo);
#else
         std::cout<<"WARNING: fOnlineSpillLog set but software not build with MIDAS"<<std::endl;
#endif

      }

      InMemorySpillTable.push_back("End run");
      InMemorySpillTable.push_back(std::string(fSpillLogTitle.at(0).size(),'-'));
      const size_t lines = InMemorySpillTable.size();
      unsigned long byte_size=0;
      for (size_t i=0; i<lines; i++)
         byte_size+=InMemorySpillTable[i].size()*sizeof(char);
      std::string unit;
      double factor=1;
      if (byte_size>(unsigned long)factor*1024)
      {
         unit="kb";
         factor*=1024.;
      }
      if (byte_size>(unsigned long)factor*1024)
      {
         unit="mb";
         factor*=1024.;
      }
      if (byte_size>(unsigned long)factor*1024)
      {
         unit="gb";
         factor*=1024.;
      }
      std::cout<<"Spill log in memory size: "<<(double)byte_size/factor<<unit.c_str()<<std::endl;
      if (!fFlags->fNoSpillSummary)
         for (size_t i=0; i<lines; i++)
            std::cout<<InMemorySpillTable[i].c_str()<<std::endl;

      if (fFlags->fWriteSpillDB)
         sqlite3_close(ppDb);

      if (fFlags->fWriteSpillTxt)
      {
         //Live spill log body:
         LiveSpillLog<<"End run " <<gRunNumber<<std::endl;
         LiveSpillLog.close();

         for (int i=0; i<NUMSEQ; i++)
         {
            LiveSequenceLog[i].close();
         }
      }
      
      //if (fileCache)
      if (runinfo->fRunNo)
      {
         //TString DataLoaderPath=outfileName(0,outfileName.Length()-5);
         TString spillLogName="spilllogs/R";
         spillLogName+=gRunNumber;
         spillLogName+="spilllog.log";
         std::cout <<"Log file: "<<spillLogName<<std::endl;
         std::ofstream spillLog (spillLogName);
         if (lines <= 700)
            spillLog<<"[code]";
         for (size_t i=0; i<lines; i++)
            spillLog<<InMemorySpillTable[i].c_str()<<std::endl;
         if (lines <= 700)
            spillLog<<"[/code]"<<std::endl;
         spillLog.close();
#ifdef HAVE_MIDAS
         char cmd[200]={0};
         // If spill log too large for message
         if (lines > 700)
            sprintf(cmd,"echo 'elog:/1' | ssh -x alpha@alphadaq /home/alpha/packages/elog/elog -h localhost -p 8083 -l SpillLog -f ${AGRELEASE}/%s -a Run=%d -a Author=alphagonline &",spillLogName.Data(),gRunNumber);
         else
            sprintf(cmd,"cat %s | ssh -x alpha@alphadaq /home/alpha/packages/elog/elog -h localhost -p 8083 -l SpillLog -a Run=%d -a Author=alphagonline &",spillLogName.Data(),gRunNumber);
         printf("--- Command: \n%s\n", cmd);
         if ( fFlags->fWriteElog )
         {
            std::cout <<"Writing elog"<<std::endl;
            int status = system(cmd);
            if (status)
            {
               cm_msg1(MERROR, "SpillLog", "CHRONO", "command [%s] returned error code %d",cmd, status);
               std::cout <<"Some problem with elog post?" << std::endl;
            }
         }
#endif
      }
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("SpillLog::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeModule, run %d\n", runinfo->fRunNo);
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      const TInfoSpillFlow* TInfoFlow= flow->Find<TInfoSpillFlow>();
      if (TInfoFlow)
      {
         for (TInfoSpill* s: TInfoFlow->spill_events)
         {
            InMemorySpillTable.push_back(s->fName.c_str());
#ifdef HAVE_MIDAS
            if (fFlags->fOnlineSpillLog)
                fSpillLogPrinter.PrintLine(s->fName.c_str());
#endif
         }
      }
      const AGSpillFlow* SpillFlow= flow->Find<AGSpillFlow>();
      if (SpillFlow)
      {
         for (size_t i=0; i<SpillFlow->spill_events.size(); i++)
         {
            TAGSpill* s = SpillFlow->spill_events.at(i);
#if HAVE_MIDAS
            if (strcmp(s->fName.c_str(),"\"Mixing\"")==0)
            {
               if (s->fScalerData)
                  if (s->fScalerData->fDetectorCounts.at(fA_or_D.GetBoardNumber() * CHRONO_N_CHANNELS + fA_or_D.GetChannel()))
                  {
                     int counts = s->fScalerData->fDetectorCounts.at(fA_or_D.GetBoardNumber() * CHRONO_N_CHANNELS + fA_or_D.GetChannel());
                     double factor = pow(10.0, 2 - ceil(log10(fabs(counts))));
                     counts = round(counts * factor) / factor;   
                     cm_msg(MTALK, "alphagonline","%d mixing triggers",counts);
                  }
            }
#endif
            //Add spills that just have text data
            if (!s->fIsDumpType && !s->IsInfoType)
            {
               InMemorySpillTable.push_back(s->fName.c_str());
#ifdef HAVE_MIDAS
               if (fFlags->fOnlineSpillLog)
                  fSpillLogPrinter.PrintLine(s->fName.c_str());
#endif
                //continue;
            }
            //Add spills that have analysis data in (eg Catching efficiency: Cold Dump / Hot Dump)
            if (s->IsInfoType)
            {
                //s->Print();
                InMemorySpillTable.push_back(s->Content(chrono_channels,fSeqIndentation, fDumpNameWidth,fDetectorColumnWidth,fFlags->fVertsInSpillLog,true,fFlags->fMVAInSpillLog).Data());
#ifdef HAVE_MIDAS
                if (fFlags->fOnlineSpillLog)
                   fSpillLogPrinter.PrintLine(s->Content(chrono_channels,fSeqIndentation, fDumpNameWidth,fDetectorColumnWidth,fFlags->fVertsInSpillLog,true,fFlags->fMVAInSpillLog).Data());
#endif
                continue;
            }
            if (!s->fSeqData) continue;

            if (fFlags->fWriteSpillTxt)
               LiveSpillLog << s->Content(chrono_channels,fSeqIndentation,fDumpNameWidth,fDetectorColumnWidth,fFlags->fVertsInSpillLog,true,fFlags->fMVAInSpillLog);
            if (fFlags->fWriteSpillDB)
               s->AddToDatabase(ppDb,stmt);
            if (!fFlags->fNoSpillSummary)
               InMemorySpillTable.push_back(s->Content(chrono_channels,fSeqIndentation,fDumpNameWidth,fDetectorColumnWidth,fFlags->fVertsInSpillLog,true,fFlags->fMVAInSpillLog).Data());
#ifdef HAVE_MIDAS
            if (fFlags->fOnlineSpillLog)
               fSpillLogPrinter.PrintLine(s->Content(chrono_channels,fSeqIndentation,fDumpNameWidth,fDetectorColumnWidth,fFlags->fVertsInSpillLog,true,fFlags->fMVAInSpillLog).Data());
            if ( alphagonline_variables )
            {
               bool write_to_odb = false;
               for ( const std::string& dumpname: fHistoryDumps)
               {
                  if (fHistoryDumps.empty())
                     continue;
                  if (s->IsMatchForDumpName(dumpname))
                  {
                     write_to_odb = true;
                     break;
                  }
               }
               if (write_to_odb)
               {
                  std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
                  std::string variable_name = s->GetSequenceName() + "_" + s->GetSanitisedName();
                  alphagonline_variables->WIA(
                     variable_name.c_str(),
                     s->fScalerData->GetScalerCounts()
                  );
               }
            }
#endif
            SaveToTree(runinfo,s);
         }
      }
      else
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
      }
      return flow;
   }

   void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event)
   {
      if (fTrace)
         printf("SpillLog::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", 
                runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
   }
};

class SpillLogFactory: public TAFactory
{
public:
   SpillLogFactory(): TAFactory()
   {
      gEnv->SetValue("Gui.DefaultFont","-*-courier-medium-r-*-*-12-*-*-*-*-*-iso8859-1");  
   }
   SpillLogFlags fFlags;

public:
   void Usage()
   {
      std::cout<<"SpillLogFactor::Help!"<<std::endl;
      std::cout<<"\t--elog\t\tWrite elog (not implemented)"<<std::endl;
      std::cout<<"\t--spilldb\t\tSwrite to Spill log sqlite database (local)"<<std::endl;
      std::cout<<"\t--spilltxt\t\tWrite Spill log to SpillLog/reload.txt"<<std::endl;
      std::cout<<"\t--nospillsummary\t\tTurn off spill log table printed at end of run"<<std::endl;
      std::cout<<"\t--onlinespills\t\tWrite spills live to SpillLog in midas"<<std::endl;
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("SpillLogFactory::Init!\n");
      

      for (unsigned i=0; i<args.size(); i++) {
         if (args[i] == "--print")
            fFlags.fPrint = true;
         if (args[i] == "--elog")
            fFlags.fWriteElog = true;
         if (args[i] == "--spilldb")
            fFlags.fWriteSpillDB = true;
         if (args[i] == "--spilltxt")
            fFlags.fWriteSpillTxt = true;
         if (args[i] == "--nospillsummary")
            fFlags.fNoSpillSummary = true;
         if (args[i] == "--onlinespills")
            fFlags.fOnlineSpillLog = true;
         if (args[i] == "--slverts")
            fFlags.fVertsInSpillLog = true;
         if (args[i] == "--slmva")
            fFlags.fMVAInSpillLog = true;
      }
 
   }

   void Finish()
   {
      if (fFlags.fPrint)
         printf("SpillLogFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("SpillLogFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new SpillLog(runinfo, &fFlags);
   }
};

static TARegister tar(new SpillLogFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
