#include "AgFlow.h"
#include "RecoFlow.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TGraph.h"

#include "cb_flow.h"

#include "TBarEvent.hh"
#include <set>
#include <iostream>
#include "TVector3.h"

class BscHistoFlags
{
public:
   bool fPrint=false;
   bool fPulser=false;
   bool fProtoTOF = false;
   bool fBscDiag = false;
   bool fWriteOffsetFile = true;
   bool fRecOff = false;
   double fRefrac = -1;

public:
   BscHistoFlags() // ctor
   { }

   ~BscHistoFlags() // dtor
   { }
};

class BscHistoModule: public TARunObject
{
public:
   BscHistoFlags* fFlags = NULL;
   int fCounter;

private:

   bool diagnostics;
   int pulser_reference_chan = 40;

   double last_event_time = 0;
   int n_ends = 128;

   const char *hit_types[10] = {"0:No hit","1:End+End","2:End+TDC","3:End+ADC",
      "4:End+None","5:TDC+TDC","6:TDC+ADC","7:ADC+ADC","8:TDC+None","9:ADC+None"};

   // Event
   TH1D* hEventTimeSince;

   // ADC
   TH1D* hADCOccupancy;
   TH2D* hADCCorrelation;
   TH1D* hADCMultiplicity;
   TH2D* hEventTimeVsADCEnd;
   TH2D* hPeakTime;
   TH2D* hADCAmpVsEnd;
   TH2D* hADCAmpFitVsEnd;
   TH2D* hADCTOT;
   TH2D* hADCAmpVsTOT;
   TH2D* hADCAmpFitVsTOT;
   TH2D* hADCAmpVsAmpFit;
   TH2D* hADCNegativePulseCorrelation;

   // TDC
   TH1D* hTdcOccupancy;
   TH1D* hTdcCoincidence;
   TH2D* hTdcCorrelation;
   TH1D* hTdcMultiplicity;
   TH2D* hTdcSingleChannelMultiplicity;
   TH2D* hTdcSingleChannelHitTime;
   TH2D* hTdcTimeVsCh0;
   std::vector<TH2D*> hTdcTimeVsCh;
   TH2D* hTdcTimeRawVsCh0;
   std::vector<TH2D*> hTdcTimeRawVsCh;
   TH2D* hFineTimeCounter;
   TH2D* hFineTime;
   TH2D* hTdcTOT;
   TH2D* hTdcTime;

   // ADC TDC Matching
   TH2D* hAdcTdcTime;
   TH1D* hEndOccupancy;
   TH2D* hEndTime;
   TH2D* hADCAmpFitVsTDCTOT;
   TH2D* hADCAmpFitVsTDCTOTch5;
   TH2D* hADCAmpFitVsTDCTOTch20;
   TH2D* hTdcTOTMatched;
   TH2D* hTW;

   // Bar Matching
   TH1D* hHitType;
   TH1D* hHitTypeTPCMatched;
   TH1D* hBarOccupancy;
   TH2D* hBarOccupancyHitType;
   TH1D* hBarOccupancyTPCMatched;
   TH2D* hBarOccupancyHitTypeTPCMatched;
   TH2D* hBarZed;
   TH2D* hBarZedMatched;
   TH2D* hBarZedTPC;
   TH2D* hBarZedTPCHitType;
   TH2D* hBarDeltaZedToTPC;
   TH2D* hBarDeltaPhiToTPC;
   TH2D* hBarZedVsTPC;
   TH2D* hBarPhiVsTPC;
   TH2D* hBarZedHitType;
   TH2D* hBarZedMatchedHitType;
   TH2D* hBarDeltaZedToTPCHitType;
   TH2D* hBarDeltaPhiToTPCHitType;

public:
   BscHistoModule(TARunInfo* runinfo, BscHistoFlags* f)
      :  TARunObject(runinfo), fFlags(f), fCounter(0)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Bsc Histo Module";
#endif
      diagnostics=f->fBscDiag;
   }

   ~BscHistoModule(){}

   void BeginRun(TARunInfo* runinfo)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      if(!diagnostics) return;
      printf("BscHistoModule::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      fCounter = 0;

      last_event_time = 0;

      n_ends = 128;
      if (fFlags->fProtoTOF) n_ends = 16;

      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      if( !gDirectory->cd("bsc_histo_module") )
         gDirectory->mkdir("bsc_histo_module")->cd();

      // Event
      gDirectory->mkdir("event_histos")->cd();
      hEventTimeSince = new TH1D("hEventTimeSince","Event time since last event;Time [s]",1000,0,0.1);
      gDirectory->cd("..");

      // ADC
      gDirectory->mkdir("ADC_histos")->cd();
      hADCOccupancy = new TH1D("hADCOccupancy","ADC channel occupancy;Channel number;Counts",n_ends,-0.5,n_ends-0.5);
      hADCCorrelation = new TH2D("hADCCorrelation","ADC channel correlation;Channel number;Channel number",n_ends,-0.5,n_ends-0.5,n_ends,-0.5,n_ends-0.5);
      hADCMultiplicity = new TH1D("hADCMultiplicity","ADC hit multiplicity;Number of ADC channels hit",n_ends,-0.5,n_ends-0.5);
      hEventTimeVsADCEnd = new TH2D("hEventTimeVsADCEnd","Event time per ADC channel;Event time [s];ADC channel",10000,0,1000,n_ends,-0.5,n_ends-0.5);
      hPeakTime = new TH2D("hPeakTime","Peak time per ADC channel;ADC channel;ADC peak time [ns]",n_ends,-0.5,n_ends-0.5,700,0,7000);
      hADCAmpVsEnd = new TH2D("hADCAmpVsEnd","ADC pulse amplitude;ADC channel;Amplitude",n_ends,-0.5,n_ends-0.5,200,0.,50000.);
      hADCAmpFitVsEnd = new TH2D("hADCAmpFitVsEnd","ADC fit pulse amplitude;ADC channel;Amplitude",n_ends,-0.5,n_ends-0.5,200,0.,50000.);
      hADCTOT = new TH2D("hADCTOT","ADC time over threshold;ADC channel;ADC TOT (ns)",n_ends,-0.5,n_ends-0.5,100,0,1000);
      hADCAmpVsTOT = new TH2D("hADCAmpVsTOT","Pulse amplitude vs ADC time over threshold;Amplitude;Time over threshold (ns)",200,0.,50000.,100,0,1000);
      hADCAmpFitVsTOT = new TH2D("hADCAmpFitVsTOT","Fit pulse amplitude vs ADC time over threshold;Fit amplitude;Time over threshold (ns)",200,0.,50000.,100,0,1000);
      hADCAmpVsAmpFit = new TH2D("hADCAmpVsAmpFit","Pulse amplitude vs fit pulse amplitude;Amplitude;Fit amplitude",200,0,50000.,200,0,50000.);
      hADCNegativePulseCorrelation = new TH2D("hADCNegativePulseCorrelation","ADC channel correlation with negative pulses;Channel number of negative pulse;Channel number of positive pulse",n_ends,-0.5,n_ends-0.5,n_ends,-0.5,n_ends-0.5);
      gDirectory->cd("..");

      // TDC
      gDirectory->mkdir("TDC_histos")->cd();
      hTdcOccupancy = new TH1D("hTdcOccupancy","TDC channel occupancy;Channel number",128,-0.5,127.5);
      hTdcCoincidence = new TH1D("hTdcCoincidence","TDC hits with corresponing hit on other end;Channel number",128,-0.5,127.5);
      hTdcCorrelation = new TH2D("hTdcCorrelation","TDC channel correlation;Channel number;Channel number",128,-0.5,127.5,128,-0.5,127.5);
      hTdcMultiplicity = new TH1D("hTdcMultiplicity","TDC channel multiplicity;Number of TDC channels hit",17,-0.5,16.5);
      hFineTime = new TH2D("hFineTime","Fine time;TDC channel number;Fine time (s)",128,-0.5,127.5,200,-1e-9,6e-9);
      hFineTimeCounter = new TH2D("hFineTimeCounter","Fine time counter;TDC channel number;Fine time counter",128,-0.5,127.5,1024,0,1024);
      hTdcSingleChannelMultiplicity = new TH2D("hTdcSingleChannelMultiplicity","Number of TDC hits on one bar end;Channel number;Number of TDC hits",128,-0.5,127.5,10,-0.5,9.5);
      hTdcSingleChannelHitTime = new TH2D("hTdcSingleChannelHitTime","Time of subsequent hits on same channel;Channel number;Time of subsequent hits after first hit (ns)",128,-0.5,127.5,1000,0,400);
      hTdcTimeVsCh0 = new TH2D("hTdcTimeVsCh0","Corrected TDC time with reference to channel forty;Channel number;TDC time minus ch40 time (ns)",128,-0.5,127.5,2000,-35,35);
      hTdcTimeRawVsCh0 = new TH2D("hTdcTimeRawVsCh0","Raw TDC time with reference to channel forty;Channel number;TDC time minus ch40 time (ns)",128,-0.5,127.5,2000,-35,35);
      hTdcTOT = new TH2D("hTdcTOT","TDC time over threshold;TDC channel;TDC TOT (ns)",n_ends,-0.5,n_ends-0.5,1000,0,100);
      hTdcTime = new TH2D("hTdcTime","TDC time;TDC channel;TDC time (ns)",n_ends,-0.5,n_ends-0.5,4000,-2000,2000);
      gDirectory->mkdir("TimeVsCh")->cd();
      for (int i=0; i<128; i++) {
         hTdcTimeVsCh.push_back(new TH2D(Form("hTimeVsCh%d",i),Form("Corrected TDC time minus ch %d time;Channel number;TDC time",i),128,-0.5,127.5,2000,-35,35));
         hTdcTimeRawVsCh.push_back(new TH2D(Form("hTimeRawVsCh%d",i),Form("Raw TDC time minus ch %d time;Channel number;TDC time",i),128,-0.5,127.5,2000,-35,35));
      }
      gDirectory->cd("..");
      gDirectory->cd("..");

      // ADC TDC Matching
      gDirectory->mkdir("ADC_TDC_matching_histos")->cd();
      hAdcTdcTime = new TH2D("hAdcTdcTime","TDC vs ADC time;ADC time [ns];TDC time minus event time [ns]",700,0,7000,8000,-4000,4000);
      hEndOccupancy = new TH1D("hEndOccupancy","EndHit (ADC+TDC) occupancy;Channel number",n_ends,-0.5,n_ends-0.5);
      hEndTime = new TH2D("hEndTime","TDC vs ADC time after matching;ADC time [ns];TDC time minus event time [ns]",700,0,7000,8000,-4000,4000);
      hADCAmpFitVsTDCTOT = new TH2D("hADCAmpFitVsTDCTOT","Fit pulse amplitude vs TDC time over threshold;Fit amplitude;Time over threshold (ns)",200,0.,50000.,100,0,100);
      hADCAmpFitVsTDCTOTch5 = new TH2D("hADCAmpFitVsTDCTOTch5","Fit pulse amplitude vs TDC time over threshold (channel 5);Fit amplitude;Time over threshold (ns)",200,0.,50000.,100,0,100);
      hADCAmpFitVsTDCTOTch20 = new TH2D("hADCAmpFitVsTDCTOTch20","Fit pulse amplitude vs TDC time over threshold (channel 20);Fit amplitude;Time over threshold (ns)",200,0.,50000.,100,0,100);
      hTdcTOTMatched = new TH2D("hTdcTOTMatched","TDC time over threshold for hits matching ADC;TDC channel;TDC TOT (ns)",n_ends,-0.5,n_ends-0.5,1000,0,100);
      hTW = new TH2D("hTW","Time walk correction;Bar end;Applied time walk correcction from ADC (ns)",n_ends,-0.5,n_ends-0.5,200,0.1,10);
      gDirectory->cd("..");

      // Bar Matching
      gDirectory->mkdir("Full_bar_histos")->cd();
      hHitType = new TH1D("hHitType","BarHit hit type;Hit type",10,-0.5,9.5);
      hHitTypeTPCMatched = new TH1D("hHitTypeTPCMatched","BarHit hit type (TPC matched);Hit type",10,-0.5,9.5);
      hBarOccupancy = new TH1D("hBarOccupancy","BarHit occupancy;Bar number",n_ends/2,-0.5,n_ends/2-0.5);
      hBarOccupancyHitType = new TH2D("hBarOccupancyHitType","BarHit occupancy;Bar number;Hit Type",n_ends/2,-0.5,n_ends/2-0.5,10,-0.5,9.5);
      hBarOccupancyTPCMatched = new TH1D("hBarOccupancyTPCMatched","BarHit occupancy (TPC matched);Bar number",n_ends/2,-0.5,n_ends/2-0.5);
      hBarOccupancyHitTypeTPCMatched = new TH2D("hBarOccupancyHitTypeTPCMatched","BarHit occupancy (TPC matched);Bar number;Hit Type",n_ends/2,-0.5,n_ends/2-0.5,10,-0.5,9.5);
      hBarZed = new TH2D("hBarZed","BarHit zed position;Bar number;Zed [m]",n_ends/2,-0.5,n_ends/2-0.5,3000,-3,3);
      hBarZedMatched = new TH2D("hBarZedMatched","BarHit zed position;Bar number;Zed [m]",n_ends/2,-0.5,n_ends/2-0.5,3000,-3,3);
      hBarZedTPC = new TH2D("hBarZedTPC","BarHit zed position from TPC;Bar number;Zed [m]",n_ends/2,-0.5,n_ends/2-0.5,3000,-3,3);
      hBarZedTPCHitType = new TH2D("hBarZedTPCHitType","BarHit zed position from TPC;Hit type;Zed [m]",10,-0.5,9.5,3000,-3,3);
      hBarZedHitType = new TH2D("hBarZedHitType","BarHit zed position;Hit type;Zed [m]",10,-0.5,9.5,3000,-3,3);
      hBarZedMatchedHitType = new TH2D("hBarZedMatchedHitType","BarHit zed position;Hit type;Zed [m]",10,-0.5,9.5,3000,-3,3);
      gDirectory->cd("..");

      // TPC Matching
      gDirectory->mkdir("TPC_matching_histos")->cd();
      hBarDeltaZedToTPC = new TH2D("hBarDeltaZedToTPC","BarHit Z_BV minus Z_TPC;Bar number;Delta zed [m]",n_ends/2,-0.5,n_ends/2-0.5,1000,-3,3);
      hBarZedVsTPC = new TH2D("hBarZedVsTPC","BarHit Z_BV vs Z_TPC;Z_TPC [m];Z_BV [m]",1000,-3,3,1000,-3,3);
      hBarDeltaZedToTPCHitType = new TH2D("hBarDeltaZedToTPCHitType","BarHit Z_BV minus Z_TPC;Hit type;Delta zed [m]",10,-0.5,9.5,1000,-3,3);
      hBarDeltaPhiToTPC = new TH2D("hBarDeltaPhiToTPC","BarHit phi_BV minus phi_TPC;Bar number;Delta phi [bars]",n_ends/2,-0.5,n_ends/2-0.5,1280,-64,64);
      hBarPhiVsTPC = new TH2D("hBarPhiVsTPC","BarHit phi_BV vs phi_TPC;phi_BV [bars];phi_TPC [bars]",128,-64,64,1280,-64,64);
      hBarDeltaPhiToTPCHitType = new TH2D("hBarDeltaPhiToTPCHitType","BarHit phi_BV minus phi_TPC;Hit type;Phi [bars]",10,-0.5,9.5,1280,-64,64);
      gDirectory->cd("..");

   }

   void EndRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("BscHistoModule::EndRun, run %d    Total Counter %d\n", runinfo->fRunNo, fCounter);
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      if (diagnostics) {
         for (int itype=0;itype<10;itype++) {
            hHitType->GetXaxis()->SetBinLabel(itype+1,hit_types[itype]);
            hHitTypeTPCMatched->GetXaxis()->SetBinLabel(itype+1,hit_types[itype]);
            hBarOccupancyHitType->GetYaxis()->SetBinLabel(itype+1,hit_types[itype]);
            hBarZedHitType->GetXaxis()->SetBinLabel(itype+1,hit_types[itype]);
            hBarOccupancyHitTypeTPCMatched->GetYaxis()->SetBinLabel(itype+1,hit_types[itype]);
            hBarZedTPCHitType->GetXaxis()->SetBinLabel(itype+1,hit_types[itype]);
            hBarZedMatchedHitType->GetXaxis()->SetBinLabel(itype+1,hit_types[itype]);
            hBarDeltaZedToTPCHitType->GetXaxis()->SetBinLabel(itype+1,hit_types[itype]);
            hBarDeltaPhiToTPCHitType->GetXaxis()->SetBinLabel(itype+1,hit_types[itype]);
         }
      }
      runinfo->fRoot->fOutputFile->Write();

   }

   void PauseRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("ResumeRun, run %d\n", runinfo->fRunNo);
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {      
      if( (!diagnostics) and (!(fFlags->fWriteOffsetFile and fFlags->fPulser)) )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      
      const AgEventFlow* ef = flow->Find<AgEventFlow>();
     
      if (!ef || !ef->fEvent)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
           
      AgBarEventFlow *bef = flow->Find<AgBarEventFlow>();
      if( !bef || !bef->BarEvent)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }

      TBarEvent* barEvt = bef->BarEvent;
      //AgEvent* agEvt = ef->fEvent;

      if( fFlags->fPrint ) printf("BscHistoModule::AnalyzeFlowEvent start\n");

      double event_time = barEvt->GetRunTime();

      //if( fFlags->fPrint ) printf("Filling event histos\n");
      hEventTimeSince->Fill(event_time-last_event_time);


      //if( fFlags->fPrint ) printf("Filling ADC histos\n");
      ADCHistos(barEvt);
      //if( fFlags->fPrint ) printf("Filling TDC histos\n");
      TDCHistos(barEvt);
      //if ( fFlags->fPrint ) printf("Filling ADC TDC matching histos\n");
      ADCTDCHistos(barEvt);
      if( fFlags->fPrint ) printf("Filling Bar histos\n");
      BarHistos(barEvt);
      //if( fFlags->fPrint ) printf("Filling TOF histos\n");

      last_event_time = event_time;
      ++fCounter;
      if( fFlags->fPrint ) printf("BscHistoModule::AnalyzeFlowEvent complete\n");
      return flow;
   }

   void ADCHistos(TBarEvent* barEvt)
   {
      const std::vector<TBarADCHit>& ADCHits = barEvt->GetADCHits();
      const double event_time = barEvt->GetRunTime();
      hADCMultiplicity->Fill(ADCHits.size());
      for (const TBarADCHit& h: ADCHits) {
         const int endID = h.GetEndID();
         hADCOccupancy->Fill(endID);
         for (const TBarADCHit& h2: ADCHits) {
            const int endID2 = h2.GetEndID();
            if (endID!=endID2) hADCCorrelation->Fill(endID,endID2);
         }
         if (h.GetStartsNegative()) {
            for (const TBarADCHit& h2: ADCHits) {
               const int endID2 = h2.GetEndID();
               if (!(h2.GetStartsNegative())) hADCNegativePulseCorrelation->Fill(endID,endID2);
            }
         }
         if  (h.GetStartsNegative()) continue;
         hEventTimeVsADCEnd->Fill(event_time,endID);
         hPeakTime->Fill(endID,h.GetPeakTime());
         hADCAmpVsEnd->Fill(endID,h.GetAmpRaw());
         hADCAmpFitVsEnd->Fill(endID,h.GetAmpFit());
         const double TOT = h.GetEndTime()-h.GetStartTime();
         hADCTOT->Fill(endID,TOT);
         hADCAmpVsTOT->Fill(h.GetAmpRaw(),TOT);
         hADCAmpFitVsTOT->Fill(h.GetAmpFit(),TOT);
         hADCAmpVsAmpFit->Fill(h.GetAmpRaw(),h.GetAmpFit());
      }
   }

   void TDCHistos(TBarEvent* barEvt)
   {
      int max_chan=128;
      if (fFlags->fProtoTOF) max_chan=16;
      std::vector<int> counts(max_chan,0);
      std::vector<double> t0(max_chan,0);
      for (const TBarTDCHit& tdchit: barEvt->GetTDCHits()) {
         const int bar = tdchit.GetEndID();
         const double time = tdchit.GetTimeCali();
         const int fine_count = tdchit.GetFineCount();
         const double fine_time = tdchit.GetFineTime();
         const double TOT = tdchit.GetTimeOverThr();
         hTdcOccupancy->Fill(bar);
         hTdcTime->Fill(bar,time*1e9);
         hFineTime->Fill(bar,fine_time);
         hFineTimeCounter->Fill(bar,fine_count);
         hTdcTOT->Fill(bar,TOT*1e9);
         counts[bar]++;
         if (t0[bar]!=0) {
            hTdcSingleChannelHitTime->Fill(bar,1e9*(time-t0[bar]));
         }
         if (t0[bar]==0) t0[bar] = time;
         if (!(fFlags->fProtoTOF)) {
            bool matched = false;
            for (const TBarTDCHit& tdchit2: barEvt->GetTDCHits()) {
               if (tdchit2.GetEndID()!=(bar+64) and tdchit2.GetEndID()!=(bar-64)) continue;
               const double time2 = tdchit2.GetTimeCali();
               if (TMath::Abs(time-time2)>50*1e-9) continue;
               matched = true;
            }
            if (matched) hTdcCoincidence->Fill(bar);
         }
      }
      int bars=0;
      for (int bar=0;bar<max_chan;bar++) {
         if (counts[bar]>0) {
            bars++;
            hTdcSingleChannelMultiplicity->Fill(bar,counts[bar]);
         }
         for (int bar2=0;bar2<max_chan;bar2++) {
            if (bar==bar2) continue;
            hTdcCorrelation->Fill(bar,bar2,counts[bar]*counts[bar2]);
         }
      }
      hTdcMultiplicity->Fill(bars);
      double ch0 = 0;
      double ch0_raw = 0;
      std::vector<double> ch_first(128,0.);
      std::vector<double> ch_first_raw(128,0.);
      for (const TBarTDCHit& tdchit: barEvt->GetTDCHits()) {
         int bar = tdchit.GetEndID();
         if (bar==pulser_reference_chan and ch0==0) {
            ch0 = tdchit.GetTimeCali();
            ch0_raw = tdchit.GetTime();
         }
         if (bar>=0 and bar<128 and ch_first.at(bar)==0) {
            ch_first.at(bar) = tdchit.GetTimeCali();
            ch_first_raw.at(bar) = tdchit.GetTime();
         }
      }
      if (ch0!=0) {
         for (const TBarTDCHit& tdchit: barEvt->GetTDCHits()) {
            const int bar = tdchit.GetEndID();
            const double time = tdchit.GetTimeCali();
            const double time_raw = tdchit.GetTime();
            if (bar==pulser_reference_chan) continue;
            hTdcTimeVsCh0->Fill(bar,1e9*(time-ch0));
            hTdcTimeRawVsCh0->Fill(bar,1e9*(time_raw-ch0_raw));
         }
      }
      for (int xz=0; xz<128; xz++) {
         if (ch_first.at(xz)==0) continue;
         for (const TBarTDCHit& tdchit: barEvt->GetTDCHits()) {
            const int bar = tdchit.GetEndID();
            if (bar==xz) continue;
            const double time = tdchit.GetTimeCali();
            const double time_raw = tdchit.GetTime();
            hTdcTimeVsCh.at(xz)->Fill(bar,1e9*(time-ch_first.at(xz)));
            hTdcTimeRawVsCh.at(xz)->Fill(bar,1e9*(time_raw-ch_first_raw.at(xz)));
         }
      }
   }

   void ADCTDCHistos(TBarEvent* barEvt)
   {
      const std::vector<TBarADCHit>& ADCHits = barEvt->GetADCHits();
      const std::vector<TBarTDCHit>& TDCHits = barEvt->GetTDCHits();
      // Unused:
      const double zero_tdc_time = barEvt->GetEventTDCTime();
      for (const TBarADCHit& adc: ADCHits) {
         for (const TBarTDCHit& tdc: TDCHits) {
            if (adc.GetEndID()!=tdc.GetEndID()) continue;
            hAdcTdcTime->Fill(adc.GetStartTime(),1e9*(tdc.GetTimeCali()));
         }
      }
      const std::vector<TBarEndHit>& EndHits = barEvt->GetEndHits();
      for (const TBarEndHit& endhit: EndHits) {
         const int endID = endhit.GetEndID();
         hEndOccupancy->Fill(endID);
         const double adc_time = endhit.GetADCTime();
         const double tdc_time = 1e9*endhit.GetTDCTimeCali();
         const double tdc_time_zeroed = tdc_time - zero_tdc_time;
         hEndTime->Fill(adc_time,tdc_time_zeroed);
         hADCAmpFitVsTDCTOT->Fill(endhit.GetAmpFit(),1e9*endhit.GetTDCHit().GetTimeOverThr());
         if (endID==5) hADCAmpFitVsTDCTOTch5->Fill(endhit.GetAmpFit(),1e9*endhit.GetTDCHit().GetTimeOverThr());
         if (endID==20) hADCAmpFitVsTDCTOTch20->Fill(endhit.GetAmpFit(),1e9*endhit.GetTDCHit().GetTimeOverThr());
         hTdcTOTMatched->Fill(endID,1e9*endhit.GetTDCHit().GetTimeOverThr());
         hTW->Fill(endID,1e9*endhit.GetTimeWalkFromADC());
      }
   }

   void BarHistos(TBarEvent* barEvt) {
      const std::vector<TBarHit>& BarHits = barEvt->GetBarHits();
      for (const TBarHit& h: BarHits) {
         const int barID = h.GetBarID();
         const int type = h.GetHitType();
         // All BarHits
         hHitType->Fill(type);
         hBarOccupancy->Fill(barID);
         hBarOccupancyHitType->Fill(barID,type);

         // All BarHits with a calculable hit time
         if (h.HasTime()) {
         }
         // All BarHits with a calculable zed position
         if (h.HasZed()) {
            const double z = h.GetZed();
            hBarZed->Fill(barID,z);
            hBarZedHitType->Fill(type,z);
         }
         // All BarHits with a TPC hit
         if (h.HasTPCHit()) {
            hHitTypeTPCMatched->Fill(type);
            hBarOccupancyTPCMatched->Fill(barID);
            hBarOccupancyHitTypeTPCMatched->Fill(barID,type);
            const double ztpc = h.GetTPCHit().z();
            hBarZedTPC->Fill(barID,ztpc);
            hBarZedTPCHitType->Fill(type,ztpc);
         }
         // All BarHits with a TPC hit and a zed position
         if (h.HasTPCHit() and h.HasZed()) {
            const double z = h.GetZed();
            const double ztpc = h.GetZedTPC();
            const double phi = h.Get3Vector().Phi()*(32/TMath::Pi());
            const double phi_tpc = (h.GetTPCHit().Phi())*(32/TMath::Pi());
            const double delta_phi = (h.Get3Vector()).DeltaPhi(h.GetTPCHit())*(32/TMath::Pi());
            hBarZedMatched->Fill(barID,z);
            hBarZedMatchedHitType->Fill(type,z);
            hBarDeltaZedToTPC->Fill(barID,z-ztpc);
            hBarZedVsTPC->Fill(ztpc,z);
            hBarDeltaZedToTPCHitType->Fill(type,z-ztpc);
            hBarDeltaPhiToTPC->Fill(barID,delta_phi);
            hBarPhiVsTPC->Fill(phi,phi_tpc);
            hBarDeltaPhiToTPCHitType->Fill(type,delta_phi);
         }

      }
   }

};


class BscHistoModuleFactory: public TAFactory
{
public:
   BscHistoFlags fFlags;
   
public:
   void Help()
   {  
      printf("BscHistoModuleFactory::Help\n");
      printf("\t--bscdiag\t\t\tenables analysis histograms\n");
      printf("\t--bscpulser\t\t\tanalyze run with calibration pulser data instead of cosmics/hbar data\n");
      printf("\t--bscProtoTOF\t\t\tanalyze run with with TRIUMF prototype instead of full BV\n");
      printf("\t--bscprint\t\t\tverbose mode\n");
      printf("\t--bscDontWriteOffsetFile\t\t\tdoes not save a calibration file to correct the tdc channel-by-channel time offsets\n");
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("BscHistoModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if( args[i] == "--bscdiag" )
            fFlags.fBscDiag = true;
         if (args[i] == "--bscprint")
            fFlags.fPrint = true;
         if( args[i] == "--bscpulser" )
            fFlags.fPulser = true;
         if( args[i] == "--bscProtoTOF" )
            fFlags.fProtoTOF = true;
         if( args[i] == "--bscDontWriteOffsetFile" )
            fFlags.fWriteOffsetFile = false;
         if( args[i] == "--recoff" )
            fFlags.fRecOff = true;

      }
   }

   void Finish()
   {
      printf("BscHistoModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("BscHistoModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new BscHistoModule(runinfo, &fFlags);
   }
};

static TARegister tar(new BscHistoModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
