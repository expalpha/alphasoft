#include "AgFlow.h"
#include "RecoFlow.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TMath.h"
#include "TSpectrum.h"
#include "TFitResult.h"
#include "Math/MinimizerOptions.h"

#include "SignalsType.hh"
#include <set>
#include <iostream>

#include "AnaSettings.hh"
#include "PadMerge.hh"

class PadMergeFlags
{
public:
   bool fRecOff = false; //Turn reconstruction off
   bool fTimeCut = false;
   bool fUseSpec = false;
   double start_time = -1.;
   double stop_time = -1.;
   bool fEventRangeCut = false;
   int start_event = -1;
   int stop_event = -1;
   AnaSettings* ana_settings=NULL;
   bool fDiag = false;
   bool fTrace = false;
   bool fForceReco = false;

   int ThreadID=-1;
   int TotalThreads=0;
   PadMergeFlags() // ctor
   { }

   ~PadMergeFlags() // dtor
   { }
};

class PadMergeModule: public TARunObject
{
public:
   PadMergeFlags* fFlags = NULL;
   bool fTrace = false;
   int fCounter = 0;
   bool diagnostic = false;
   

private:
   PadMerge* padm;

public:

   PadMergeModule(TARunInfo* runinfo, PadMergeFlags* f)
      : TARunObject(runinfo)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="PadMerge Module";
#endif
      if (fTrace)
         printf("PadMergeModule::ctor!\n");

      fFlags = f;

      //First thread
#ifdef HAVE_MANALYZER_PROFILER
      if (fFlags->ThreadID < 0)
         fModuleName="PadMerge Module (CombPads)";
      //Multithreaded fitting
      else if ( fFlags->ThreadID < fFlags->TotalThreads ) 
         fModuleName="PadMerge Module (Combine " +
            std::to_string(fFlags->ThreadID + 1) + 
            "/" +
            std::to_string(fFlags->TotalThreads) +
            ")";
      else if (fFlags->TotalThreads==0 && fFlags->ThreadID==1)
         fModuleName="PadMerge Module (spacepoints)";
#endif
      diagnostic=fFlags->fDiag; // dis/en-able histogramming
      fTrace=fFlags->fTrace; // enable verbosity
   }

   ~PadMergeModule()
   {
      if(fTrace)
         printf("PadMergeModule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(fTrace)
         printf("PadMergeModule::BeginRun, run %d, file %s\n", 
                runinfo->fRunNo, runinfo->fFileName.c_str());
      fCounter = 0;
      bool MTing = runinfo->fMtInfo;
      if(fTrace)
         printf("PadMergeModule::BeginRun Are we MTing? %d\n",MTing);
                
      padm=new PadMerge(fFlags->ana_settings,fTrace);
      //Global lock from manalzer (needed if your using roots basic fitting methods)
      padm->SetGlobalLockVariable(&TAMultithreadHelper::gfLock);
      padm->SetDiagnostic(diagnostic);
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      padm->Setup(runinfo->fRoot->fOutputFile);
      padm->Init();
   }

   void EndRun(TARunInfo* runinfo)
   {
      if(fTrace)
         printf("PadMergeModule::EndRun, run %d    Total Counter %d\n", runinfo->fRunNo, fCounter);
      delete padm;
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeRun, run %d\n", runinfo->fRunNo);
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      if(fTrace)
         printf("PadMergeModule::Analyze, run %d, counter %d\n",
                runinfo->fRunNo, fCounter++);

      // turn off recostruction
      if (fFlags->fRecOff)
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      

      const AgEventFlow* ef = flow->Find<AgEventFlow>();

      if (!ef || !ef->fEvent)
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      AgSignalsFlow* SigFlow = flow->Find<AgSignalsFlow>();
      if( !SigFlow ) 
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( SigFlow->bad || SigFlow->awSig.empty() )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      if( fTrace )
         {
            printf("PadMergeModule::Analyze, PAD # signals %d\n", int(SigFlow->pdSig.size()));
         }  
     
      if( SigFlow->pdSig.size() )
         {
            // -----------------
            //I am the first thread... 
            // -----------------
            if (fFlags->ThreadID < 0)
               {
                  SigFlow->comb = padm->CombPads( SigFlow->pdSig );
                  return flow;
               }
            // -----------------
            //else process comb in multiple threads, the number of these is defined by MAX_THREADS below
            // -----------------
            else if ( fFlags->ThreadID < fFlags->TotalThreads )  
               {
                  size_t start=floor(SigFlow->comb.size() * fFlags->ThreadID/fFlags->TotalThreads);
                  size_t stop=floor( SigFlow->comb.size() * (fFlags->ThreadID + 1) / fFlags->TotalThreads);

                  //std::cout<<"SIZE:"<<SigFlow->comb.size()<<"\t"<<start<<" - "<<stop<<std::endl;
                  for (size_t i=start; i<stop; i++)
                     SigFlow->combinedPads = padm->CombineAPad( SigFlow->comb,SigFlow->combinedPads,i );
                  return flow;
               }
         }
      // -----------------
      // Ending thread
      // -----------------

      return flow;
   }
};


class PadMergeModuleFactory: public TAFactory
{
public:
   PadMergeFlags fFlags;

public:
   void Help()
   {
      printf("PadMergeModuleFactory::Help\n");
      printf("\t--forcereco\t\tEnable reconstruction when no pads are associated with the event by setting z=0\n");
   }
   void Usage()
   {
      Help();
   }

   void Init(const std::vector<std::string> &args)
   {
      TString json="default";
      // if(fFlags.ThreadID < 1)
      printf("PadMergeModuleFactory::Init %d -> %d args!\n",fFlags.ThreadID, int(args.size()) );

      for(unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--usetimerange" )
               {
                  fFlags.fTimeCut=true;
                  i++;
                  fFlags.start_time=atof(args[i].c_str());
                  i++;
                  fFlags.stop_time=atof(args[i].c_str());
                  printf("Using time range for reconstruction: ");
                  printf("%f - %fs\n",fFlags.start_time,fFlags.stop_time);
               }
            if( args[i] == "--useeventrange" )
               {
                  fFlags.fEventRangeCut=true;
                  i++;
                  fFlags.start_event=atoi(args[i].c_str());
                  i++;
                  fFlags.stop_event=atoi(args[i].c_str());
                  printf("Using event range for reconstruction: ");
                  printf("Analyse from (and including) %d to %d\n",fFlags.start_event,fFlags.stop_event);
               }
            if (args[i] == "--recoff")
               fFlags.fRecOff = true;
            if( args[i] == "--diag" )
               fFlags.fDiag = true;
            if( args[i] == "--trace" )
               fFlags.fTrace = true;
            if(args[i] == "--forcereco")
               fFlags.fForceReco=true;
            if (args[i] == "--anasettings")
               {
                  json=args[++i];
               }
         }
            fFlags.ana_settings = new AnaSettings(json, args);
            if(fFlags.fTrace) fFlags.ana_settings->Print();
   }
   
   PadMergeModuleFactory(int ThreadIndex = -1, int NThreads = 0)
   {
       fFlags.ThreadID=ThreadIndex;
       fFlags.TotalThreads=NThreads;
   }

   void Finish()
   {
      if(fFlags.fTrace == true)
         printf("PadMergeModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      if(fFlags.fTrace == true)
         printf("PadMergeModuleFactory::NewRunObject, run %d, file %s\n", 
                runinfo->fRunNo, runinfo->fFileName.c_str());
      return new PadMergeModule(runinfo, &fFlags);
   }
};

static TARegister tar(new PadMergeModuleFactory);
#if N_PAD_MERGE_THREADS==1
static TARegister tar1(new PadMergeModuleFactory(0,1));
#elif N_PAD_MERGE_THREADS==2
static TARegister tar1(new PadMergeModuleFactory(0,2));
static TARegister tar2(new PadMergeModuleFactory(1,2));
#elif N_PAD_MERGE_THREADS==4
static TARegister tar1(new PadMergeModuleFactory(0,4));
static TARegister tar2(new PadMergeModuleFactory(1,4));
static TARegister tar3(new PadMergeModuleFactory(2,4));
static TARegister tar4(new PadMergeModuleFactory(3,4));
#elif N_PAD_MERGE_THREADS==8
static TARegister tar1(new PadMergeModuleFactory(0,8));
static TARegister tar2(new PadMergeModuleFactory(1,8));
static TARegister tar3(new PadMergeModuleFactory(2,8));
static TARegister tar4(new PadMergeModuleFactory(3,8));
static TARegister tar5(new PadMergeModuleFactory(4,8));
static TARegister tar6(new PadMergeModuleFactory(5,8));
static TARegister tar7(new PadMergeModuleFactory(6,8));
static TARegister tar8(new PadMergeModuleFactory(7,8));
#elif N_PAD_MERGE_THREADS==16
static TARegister tar1(new PadMergeModuleFactory(0,16));
static TARegister tar2(new PadMergeModuleFactory(1,16));
static TARegister tar3(new PadMergeModuleFactory(2,16));
static TARegister tar4(new PadMergeModuleFactory(3,16));
static TARegister tar5(new PadMergeModuleFactory(4,16));
static TARegister tar6(new PadMergeModuleFactory(5,16));
static TARegister tar7(new PadMergeModuleFactory(6,16));
static TARegister tar8(new PadMergeModuleFactory(7,16));
static TARegister tar9(new PadMergeModuleFactory(8,16));
static TARegister tar10(new PadMergeModuleFactory(9,16));
static TARegister tar11(new PadMergeModuleFactory(10,16));
static TARegister tar12(new PadMergeModuleFactory(11,16));
static TARegister tar13(new PadMergeModuleFactory(12,16));
static TARegister tar14(new PadMergeModuleFactory(13,16));
static TARegister tar15(new PadMergeModuleFactory(14,16));
static TARegister tar16(new PadMergeModuleFactory(15,16));
#else
#error N_PAD_MERGE_THREADS does not have a valid setting
#endif
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
