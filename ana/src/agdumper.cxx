//
// AG Dumper Module for taking events and saving to super trees.
//
// L GOLINO
//

#include <stdio.h>

#include "manalyzer.h"
#include "midasio.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include "RecoFlow.h"
#include "TTree.h"
#include "EventTracker.h"
#include "TBarCaliFile.h"

#include "TAGMVADumper.h"

class TAGDumperFlags
{
    public:
        std::string fFileName;
        std::string fTreeName;
        bool fTrace = false;
        std::string fCaliFile = "";
        bool fNoCali = false;
        bool fRequireCali = false;
        float fMagneticField;
};

class TAGDumper: public TARunObject
{
public:
    //List of event IDs we want to save.
    std::vector<std::pair<int,int>> fEventIDs; 
    //The location of saving said events.
    TTree *fTree;
    TFile *fRootFile;
    EventTracker* fAGDumperEventTracker = NULL;

    //The data we want to save is all in this object.
    std::vector<TAGMVADumper*> fMVADumpers;

    TBarCaliFile* Calibration;
    bool cali_loaded = false;

    //Flags & counters.
    TAGDumperFlags* fFlags;

    TAGDumper(TARunInfo* runInfo, TAGDumperFlags* flags) : TARunObject(runInfo)
    {
        //printf("ctor, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
        fFlags = flags;
        fModuleName="agdumper";
        
        //Setup the filename for the root file.
        std::string fileName = "dumperoutput";
        fileName += fFlags->fTreeName;
        fileName += std::to_string(runInfo->fRunNo);
        fileName += ".root";
        fRootFile = TFile::Open(fileName.c_str(),"RECREATE");
        
        //Set up the new tree using name as input.
        if(fFlags->fTreeName == "mixing" || fFlags->fTreeName == "cosmic")
            fTree = new TTree(fFlags->fTreeName.c_str(),"data from storeEvent");
        else
        {
            std::cout << "WARNING: No --datalabel given (cosmic or mixing). I could dump to a generic root tree but I want to save you headaches later. Please rerun with a label.\n";
            exit(-1);
        }

        fMVADumpers.push_back( new TAGMVAEventIDDumper(fTree) );
        fMVADumpers.push_back( new TAGMVAClassicDumper(fTree) );
        fMVADumpers.push_back( new TAGMVAClassicBVDumper(fTree) );
        fMVADumpers.push_back( new TAGMVAHLParameterDumper(fTree) );
        fMVADumpers.push_back( new TAGMVAGarethsTOFDumper(fTree) );
        fMVADumpers.push_back( new TAGMVA2024Dumper(fTree) );
        fMVADumpers.push_back( new TAGMVAAllTOFsDumper(fTree) );
        fMVADumpers.push_back( new TAGMVAAllTOFDistsDumper(fTree) );
        fMVADumpers.push_back( new TAGMVAAllTDCsDumper(fTree) );



        //fMVADumpers.push_back( new TA2MVAXYZ(fTree) );
    }

    ~TAGDumper()
    {
        //printf("dtor!\n");
    }
    
    void BeginRun(TARunInfo* runInfo)
    {
        printf("BeginRun, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
        fAGDumperEventTracker = new EventTracker(fFlags->fFileName, runInfo->fRunNo);
        std::cout << "GetEventCut = " << fAGDumperEventTracker->GetEventCut() << "\n";
        std::cout << "GetTimeCut = " << fAGDumperEventTracker->GetTimeCut() << "\n";
        std::cout << "GetRunNumber = " << fAGDumperEventTracker->GetRunNumber() << "\n";

        // Load bv calibration from calibration file
        Calibration = new TBarCaliFile();
        if (!fFlags->fNoCali) {
            if (fFlags->fCaliFile=="") Calibration->LoadCali(runInfo->fRunNo,true);
            else Calibration->LoadCaliFile(fFlags->fCaliFile,true);
        }
        if (Calibration->IsLoaded()) cali_loaded = true;

        /*
        // Commented out by Gareth. fEventIDs is private so this does not work
        if(fAGDumperEventTracker->fEventIDs.size()>0)
        {
            std::cout << "first = " << fAGDumperEventTracker->fEventIDs.at(0).first << "\n";
            std::cout << "second = " << fAGDumperEventTracker->fEventIDs.at(0).second << "\n";
        }
        else
        {
            std::cout << "first = gone \n";
            std::cout << "second = also gone\n";

        }
        */
    }

    void EndRun(TARunInfo* runInfo)
    {
        printf("EndRun, run %d\n", runInfo->fRunNo);

        //Write the tree and file, then close file.
        fTree->Write();
        fRootFile->Write();
        fRootFile->Close();
    }

    //Presently this module doesn't need to touch TMEvent data
    //TAFlowEvent* Analyze(TARunInfo* runInfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow)
    //{
    //   START_TIMER;
    //    flow = new UserProfilerFlow(flow,"custom profiling",timer_start);
    //    return flow;
    //}

    TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runInfo, TAFlags* /*flags*/, TAFlowEvent* flow)
    {
        if (fFlags->fRequireCali and !cali_loaded)
        {
            return flow;
        }

        if (fFlags->fTrace)
            printf("AGDumper::AnalyzeFlowEvent, run %d\n", runInfo->fRunNo);
        AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
        if (!agfe)
        {
            return flow;
        }
        TStoreEvent* storeEvent=agfe->fEvent;


        if( !fAGDumperEventTracker->IsEventInRange(storeEvent->GetEventNumber(), storeEvent->GetTimeOfEvent()) )
        {
            return flow;
        }

        //Update variables of the MVA class. This will check whether the eventnumbers match.
        //If they match this function will return true, if not: false. This allows us to know
        //whether to fill the tree and increment the EventIndex and EventNumber.
        size_t good_flow = 0;
        for ( TAGMVADumper* d: fMVADumpers)
        {
            if(d->UpdateVariables(flow, storeEvent->GetEventNumber()))
            {
                good_flow++;
            }
        }
        if (good_flow)
        {
           assert (good_flow == fMVADumpers.size());
           std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
           fTree->Fill();
        }
        return flow;
    }

    void AnalyzeSpecialEvent(TARunInfo* runInfo, TMEvent* event)
    {
        if(fFlags->fTrace)
            printf("agdump::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", runInfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
    }
};

class TAGDumperFactory: public TAFactory
{
public:
    TAGDumperFlags fFlags;
   void Usage()
   {
      printf("TAGDumperFactory::Help!\n");
      printf("\t--eventlist filename\tSave events from TA2Plot.WriteEventList() function\n");
      printf("\t--datalabel label\tValid data labels currently: mixing and cosmic\n");
      printf("\t--bsccalifile /path/to/BarrelCalibration.root\t\tloads barrel calibration directly from specified file\n");
      printf("\t--nobsccali\t\tskips loading barrel calibration file\n");
      printf("\t--dumperreqcali\t\tagdumper does not dump without bv calibration\n");
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("Init!\n");
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--eventlist")
               fFlags.fFileName = args[i+1];
            if( args[i] == "--datalabel")
               fFlags.fTreeName = args[i+1];
            if( args[i] == "--verbose")
               fFlags.fTrace = true;
            if( args[i] == "--Bfield" )
               fFlags.fMagneticField = atof(args[++i].c_str());
            if( args[i] == "--dumperreqcali")
                fFlags.fRequireCali = true;
            if( args[i] == "--bscnocali")
                fFlags.fNoCali = true;
            if( args[i] == "--bsccalifile") {
                i++;
                fFlags.fCaliFile = args[i];
            }
         }
   }
   
   void Finish()
   {
      printf("Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runInfo)
   {
      printf("NewRunObject, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
      return new TAGDumper(runInfo, &fFlags);
   }
};

static TARegister tar(new TAGDumperFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
