#include "manalyzer.h"
#include "midasio.h"
#include "AgFlow.h"
#include "RecoFlow.h"
#include "AnaSettings.hh"
#include "json.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TSystem.h"
#include "TMath.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TBarEvent.hh"
#include "TBarCaliFile.h"

class AdcTdcMatchingFlags
{
public:
   bool fPrint = false;
   bool fTimeCut = false;
   double start_time = -1.;
   double stop_time = -1.;
   std::string fCaliFile = "";
   bool fNoCali = false;
   AnaSettings* ana_settings=0;
};


class adctdcmatchingmodule: public TARunObject
{
public:
   AdcTdcMatchingFlags* fFlags;

private:

   TBarCaliFile* Calibration;

   // Constant value declaration
   const double max_adc_tdc_diff_t; // s, maximum allowed time between ADC time and matched TDC time
   const double twA; // Default time walk correction, dt = A/sqrt(amp)

   // Counter initialization
   int c_adc = 0;
   int c_tdc = 0;
   int c_adctdc = 0;

public:

   adctdcmatchingmodule(TARunInfo* runinfo, AdcTdcMatchingFlags* flags): 
      TARunObject(runinfo), fFlags(flags),
      max_adc_tdc_diff_t(flags->ana_settings->GetDouble("BscModule","max_adc_tdc_diff")),
      twA(flags->ana_settings->GetDouble("BscModule","TWfromAmp"))
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="bsc adc tdc matching module";
#endif
      printf("adctdcmatchingmodule::ctor!\n");
   }

   ~adctdcmatchingmodule()
   {
      printf("adctdcmatchingmodule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      runinfo->fRoot->fOutputFile->cd();

      Calibration = new TBarCaliFile();
      if (!fFlags->fNoCali) {
         if (fFlags->fCaliFile=="") Calibration->LoadCali(runinfo->fRunNo,true);
         else Calibration->LoadCaliFile(fFlags->fCaliFile,true);
      }

   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fFlags->fPrint ) printf("EndRun, run %d\n", runinfo->fRunNo);
      printf("adc tdc matching module stats:\n");
      printf("Total number of adc hits = %d\n",c_adc);
      printf("Total number of tdc hits = %d\n",c_tdc);
      printf("Total number of adc+tdc combined hits = %d\n",c_adctdc);
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("ResumeRun, run %d\n", runinfo->fRunNo);
   }

   // Main function
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      // Unpack Event flow
      const AgEventFlow* ef = flow->Find<AgEventFlow>();
      if (!ef || !ef->fEvent)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
           
      AgBarEventFlow *bef = flow->Find<AgBarEventFlow>();
      if( !bef || !bef->BarEvent)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }

      AgEvent* age = ef->fEvent;
      TBarEvent* barEvt = bef->BarEvent;
      if(!age)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      if(!barEvt)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      if (fFlags->fTimeCut) {
         if (age->time<fFlags->start_time) {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
         if (age->time>fFlags->stop_time) {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      }

      if( fFlags->fPrint ) printf("adctdcmatchingmodule::AnalyzeFlowEvent start\n");
      std::vector<TBarADCHit>& ADCHits = barEvt->GetADCHits();
      if (ADCHits.size()==0) {
         if( fFlags->fPrint ) printf("adctdcmatchingmodule::No ADC hits\n");
         return flow;
      }
      std::vector<TBarTDCHit>& TDCHits = barEvt->GetTDCHits();
      if (TDCHits.size()==0) {
         if( fFlags->fPrint ) printf("adctdcmatchingmodule::No TDC hits\n");
         return flow;
      }

      std::vector<TBarEndHit> end_hits = MatchAdcTdc(ADCHits,TDCHits,barEvt);
      ApplyTimeWalk(end_hits);
      for (const TBarEndHit& h: end_hits) barEvt->AddEndHit(h);

      c_adc+=ADCHits.size();
      c_tdc+=TDCHits.size();
      c_adctdc+=end_hits.size();
      
      if( fFlags->fPrint ) printf("adctdcmatchingmodule::End\n");
      
      return flow;
   }

   //________________________________
   // MAIN FUNCTIONS

   std::vector<TBarEndHit> MatchAdcTdc(std::vector<TBarADCHit>& ADCHits,std::vector<TBarTDCHit>& TDCHits, TBarEvent* barEvt) {
      std::vector<TBarEndHit> end_hits;
      if (ADCHits.size()==0 or TDCHits.size()==0) return end_hits;

      // Sorts ADC hit by amplitude
      std::sort(ADCHits.begin(), ADCHits.end(), CompareADCAmplitude);
      double max_adc_amp = ADCHits.at(0).GetAmpFit();

      // Tries to find the best TDC-ADC time offset.
      // Chooses one of the ADC-TDC time differences from among bars with both ADC and TDC hits.
      // Does this by minimizing the sum of ((TDC-ADC-offset)*(ADC amplitude))^2 over all bars.
      double best_residuals = 0;
      double best_time_offset = 0;
      for (const TBarADCHit& adc: ADCHits) {
         if (!adc.IsGood()) continue;
         int adc_ch = adc.GetEndID();
         if (adc_ch<0 or adc_ch>127) continue;
         if (adc.GetAmpFit() < 0.5*max_adc_amp) continue; // To save run time on very busy events, lets not concern ourselves with small fry
         double adc_time = adc.GetStartTime()*1e-9;
         for (const TBarTDCHit& tdc: TDCHits) {
            if (!tdc.IsGood()) continue;
            int tdc_ch = tdc.GetEndID();
            if (tdc_ch!=adc_ch) continue; // Not a match
            double tdc_time = tdc.GetTimeCali();

            // Try using the offset between this ADC and TDC hit as the global ADC-TDC offset for this event
            double possible_time_offset = tdc_time - adc_time;

            // Find the residuals summed over all ADC hits
            // i.e. the (TDC-ADC-offset) time difference to the closest TDC hit
            // weighted by ADC amplitude 
            double residuals = 0;
            for (const TBarADCHit& adc1: ADCHits) {
               if (!adc1.IsGood()) continue;
               double adc_time1 = adc1.GetStartTime()*1e-9;
               int adc_ch1 = adc1.GetEndID();
               if (adc_ch1<0 or adc_ch1>127) continue;

               // Find a residual to each TDC hit on the same bar, and choose the smallest one.
               double this_hit_best_residual = 0;
               for (unsigned int itdc=0; itdc<TDCHits.size(); itdc++) {
                  const TBarTDCHit& tdc1 = TDCHits.at(itdc);
                  if (!tdc1.IsGood()) continue;
                  int tdc_ch1 = tdc1.GetEndID();
                  if (tdc_ch1!=adc_ch1) continue;  // Not a match
                  double tdc_time1 = tdc1.GetTimeCali();
                  double this_hit_residual = (tdc_time1-adc_time1-possible_time_offset)*(tdc_time1-adc_time1-possible_time_offset)*adc1.GetAmpFit()*adc1.GetAmpFit();
                  if (this_hit_residual<this_hit_best_residual or this_hit_best_residual==0) this_hit_best_residual = this_hit_residual;
               }
               residuals += this_hit_best_residual;
            }
            // Best choice yet!
            if (residuals < best_residuals or best_residuals==0) {
               best_residuals = residuals;
               best_time_offset = possible_time_offset;
               break;
            }
         }
      }

      // Matches a TDC hit to each ADC hit
      for (TBarADCHit& adc: ADCHits) {
         if (!adc.IsGood()) continue;
         double adc_time = adc.GetStartTime()*1e-9;
         std::vector<bool> tdc_used(TDCHits.size(),false);
         std::vector<double> diffs;
         std::vector<int> indices;
         for (size_t i=0; i<TDCHits.size(); i++) {
            TBarTDCHit& tdc = TDCHits[i];
            if (!tdc.IsGood()) continue;
            if (adc.GetEndID()!=tdc.GetEndID()) continue;
            if (tdc_used[i]) continue;
            const double tdc_time = tdc.GetTimeCali();
            const double tdc_time_zeroed = tdc_time - best_time_offset;
            diffs.push_back(TMath::Abs( adc_time - tdc_time_zeroed));
            indices.push_back(i);
         }
         if (diffs.size()==0) {
            //if (fFlags->fPrint) printf("Ch %d ADC hit not matched to TDC! Amplitude %.6f TOT %d\n",adc->GetEndID(),adc->GetAmpFitCalibrated(),adc->GetTimeOverThreshold());
            continue;
         }
         const int min_index = std::min_element(diffs.begin(),diffs.end()) - diffs.begin();
         const double min_diff = *std::min_element(diffs.begin(), diffs.end());
         if (min_diff > max_adc_tdc_diff_t) {
            //if (fFlags->fPrint) printf("Ch %d ADC hit not matched to TDC! Amplitude %.6f TOT %d\n",adc->GetEndID(),adc->GetAmpFitCalibrated(),adc->GetTimeOverThreshold());
            continue;
         }
         TBarTDCHit& tdc = TDCHits[indices[min_index]];
         tdc_used[indices[min_index]] = true;
         adc.SetMatchedToTDC(true);

         TBarEndHit h(adc,tdc);
         end_hits.push_back(h);
         TDCHits[indices[min_index]].SetMatchedToADC(true);
      }

      barEvt->SetEventTDCTime(best_time_offset);
      return end_hits;
   }

   void ApplyTimeWalk(std::vector<TBarEndHit>& endhits) {
      for (TBarEndHit& endhit: endhits) {
         double sqrtamp = TMath::Sqrt(endhit.GetAmpFit());
         if (std::isnan(sqrtamp)) {
            printf("NaN sqrtamp on bar %d... fit amp %.3f, raw amp %.3f\n",endhit.GetBarID(),endhit.GetAmpFit(),endhit.GetAmpRaw());
            printf("Please fix the BV ADC fitting code... <3\n");
            sqrtamp = 180.;
         }

         const int barID = endhit.GetBarID();
         if (Calibration->IsLoaded()) {
            const double tw = (endhit.IsTop()) ? Calibration->GetTWTop(barID) : Calibration->GetTWBot(barID);
            endhit.SetTimeWalkFromADC(tw/sqrtamp);
         }
         else {
            endhit.SetTimeWalkFromADC(twA*1e-7/sqrtamp);
         }
      }
   }

   static bool CompareADCAmplitude(TBarADCHit h1, TBarADCHit h2) {
      return h1.GetAmpFit() > h2.GetAmpFit();
   }

};


class AdcTdcMatchingModuleFactory: public TAFactory
{
public:
   AdcTdcMatchingFlags fFlags;
public:
   void Help()
   {  
      printf("AdcTdcMatchingModuleFactory::Help\n");
      printf("\t--anasettings /path/to/settings.json\t\t load the specified analysis settings\n");
      printf("\t--bscprint\t\t\tverbose mode\n");
      printf("\t--bsccalifile /path/to/BarrelCalibration.root\t\tloads barrel calibration directly from specified file\n");
      printf("\t--nobsccali\t\tskips loading barrel calibration file\n");
      printf("\t--usetimerange 123.4 567.8\t\tLimit analysis to a time range\n");
   }
   void Usage()
   {
      Help();
   }
   void Init(const std::vector<std::string> &args)
   {
      TString json="default";
      printf("AdcTdcMatchingModuleFactory::Init!\n");
      for (unsigned i=0; i<args.size(); i++) { 
         if( args[i]=="-h" || args[i]=="--help" )
            Help();
         if (args[i] == "--bscprint")
            fFlags.fPrint = true; 
         if( args[i] == "--anasettings" ) 
            json=args[++i];
         if( args[i] == "--bscnocali")
            fFlags.fNoCali = true;
         if( args[i] == "--bsccalifile") {
            i++;
            fFlags.fCaliFile = args[i];
         }
         if( args[i] == "--usetimerange" )
            {
               fFlags.fTimeCut=true;
               i++;
               fFlags.start_time=atof(args[i].c_str());
               i++;
               fFlags.stop_time=atof(args[i].c_str());
               printf("Using time range for reconstruction: ");
               printf("%f - %fs\n",fFlags.start_time,fFlags.stop_time);
            }
      }
      fFlags.ana_settings = new AnaSettings(json.Data(),args);
   }

   void Finish()
   {
      printf("AdcTdcMatchingModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("AdcTdcMatchingModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new adctdcmatchingmodule(runinfo,&fFlags);
   }
};

static TARegister tar(new AdcTdcMatchingModuleFactory);


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
