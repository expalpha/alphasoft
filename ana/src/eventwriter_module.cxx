//
// reco_spacepoint_builder.cxx
//
// reconstruction of TPC data
//

#include <stdio.h>
#include <iostream>

#include "manalyzer.h"
#include "midasio.h"

#include "AgFlow.h"
#include "RecoFlow.h"

#include <TTree.h>

#include "TStoreEvent.hh"

#include "AnaSettings.hh"
#include "json.hpp"

class TEventWriterFlags {
public:
   bool lite_mode;

public:
   TEventWriterFlags() // ctor
   {
      lite_mode = false;
   }

   ~TEventWriterFlags() // dtor
   {
   }
};

class TEventWriter : public TARunObject {
public:
   bool       do_plot = false;
   const bool fTrace  = false;
   // bool fTrace = true;
   bool fVerb = false;

   TEventWriterFlags *fFlags;

private:
public:
   TStoreEvent *analyzed_event;
   TBarEvent   *bar_event;
   TTree       *StoreEventTree, *BarEventTree;

   TEventWriter(TARunInfo *runinfo, TEventWriterFlags *f) : TARunObject(runinfo), fFlags(f)

   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName = "TStoreEvent_Writer";
#endif
   }

   ~TEventWriter()
   {
      printf("TEventWriter::dtor!\n");
   }

   void BeginRun(TARunInfo *runinfo)
   {
      printf("TEventWriter::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory

      analyzed_event = new TStoreEvent;
      StoreEventTree = new TTree("StoreEventTree", "StoreEventTree");
      StoreEventTree->Branch("StoredEvent", &analyzed_event, 32000, 0);
      delete analyzed_event;
      analyzed_event = NULL;
      bar_event      = new TBarEvent();
      BarEventTree   = new TTree("TBarEventTree", "TBarEventTree");
      BarEventTree->Branch("TBarEvent", &bar_event, 32000, 0);
      delete bar_event;
      bar_event = NULL;
   }

   void EndRun(TARunInfo *runinfo)
   {
      printf("TEventWriter::EndRun, run %d\n", runinfo->fRunNo);
   }

   void PauseRun(TARunInfo *runinfo)
   {
      printf("TEventWriter::PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo *runinfo)
   {
      printf("TEventWriter::ResumeRun, run %d\n", runinfo->fRunNo);
   }

   TAFlowEvent *AnalyzeFlowEvent(TARunInfo *runinfo, TAFlags *flags, TAFlowEvent *flow)
   {
      if (fTrace) printf("TEventWriter::AnalyzeFlowEvent, run %d\n", runinfo->fRunNo);

      // save a copy of the barell scrintillator hits
      AgBarEventFlow *bef = flow->Find<AgBarEventFlow>();

      // Finds AgAnalysisFlow after AgBarEventFlow
      AgAnalysisFlow *af = flow->Find<AgAnalysisFlow>();
      if (!af) {
         *flags |= TAFlag_SKIP_PROFILE;
         return flow;
      }
      // prepare event to store in TTree
      analyzed_event = af->fEvent;
      if (bef) bar_event = bef->BarEvent;

      if (!analyzed_event) return flow;

      if (!bar_event) {
         if (fTrace) std::cerr << "TEventWriter::AnalyzeFlowEvent no BSC event found" << std::endl;
      } else {
         if (bar_event->GetNumBars() <= 0)
            if (fTrace) std::cout << "TEventWriter::AnalyzeFlowEvent no BSC hits found" << std::endl;
         // analyzed_event->AddBarrelHits(bar_event);
         analyzed_event->AddBarEvent(bar_event);
      }

      if (bar_event || analyzed_event) {
         std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
         if (bar_event) BarEventTree->Fill();
         if (analyzed_event && !fFlags->lite_mode) StoreEventTree->Fill();
      }

      return flow;
   }

   void AnalyzeSpecialEvent(TARunInfo *runinfo, TMEvent *event)
   {
      printf("TEventWriter::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", runinfo->fRunNo,
             event->serial_number, (int)event->event_id, event->data_size);
   }
};

class TEventWriterFactory : public TAFactory {
public:
   TEventWriterFlags fFlags;

public:
   void Help() { printf("TEventWriterFactory::Help\n"); }
   void Usage() { Help(); }
   void Init(const std::vector<std::string> &args)
   {
      TString json = "default";
      printf("TEventWriterFactory::Init!\n");
      for (unsigned i = 0; i < args.size(); i++) {
         if (args[i] == "-h" || args[i] == "--help")
            Help();
         else if (args[i] == "--lite")
            fFlags.lite_mode = true;
      }
   }

   void         Finish() { printf("TEventWriterFactory::Finish!\n"); }
   TARunObject *NewRunObject(TARunInfo *runinfo)
   {
      printf("TEventWriterFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new TEventWriter(runinfo, &fFlags);
   }
};

static TARegister tar(new TEventWriterFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
