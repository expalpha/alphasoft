#include "AgFlow.h"
#include "RecoFlow.h"

#include "TH2D.h"
#include "SignalsType.hh"

#include "TStoreLine.hh"
#include "TStoreHelix.hh"
#include "TSpacePoint.hh"

#include "AnaSettings.hh"

#include <cctype>

class PHspectrumFlags
{
public:  
   bool fEnabled;
   int fNtracks;
   double fMagneticField;
   AnaSettings* ana_settings;
  
   PHspectrumFlags():fEnabled(false),// ctor
                     fNtracks(1),fMagneticField(1.0),
                     ana_settings(0)
   { }
  
   ~PHspectrumFlags() // dtor
   { }
};

class PHspectrum: public TARunObject
{
public:
   PHspectrumFlags* fFlags;
   bool fTrace = false;
private:
   TH2D* hawphspect;
   TH2D* hpadphspect;
   TH2D* hpwbphspect;
   TH2D* hadcphspect;

   TH2D* hawphtime;
   TH2D* hadcphtime;

   TH2D* hawphspect_pc;
   TH2D* hpadphspect_pc;
   TH2D* hpwbphspect_pc;
   TH2D* hadcphspect_pc;
   
   ALPHAg::padmap* pmap;

   int fNtracks;
   double fCoincTime; // ns
   double fpc_timecut; //ns

public:
   PHspectrum(TARunInfo* runinfo, PHspectrumFlags* f):TARunObject(runinfo),
                                                      fFlags(f),pmap(0),
                                                      fNtracks(1),fCoincTime(20.),
                                                      fpc_timecut(120.)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="PHspectrum Module";
#endif
   }
   ~PHspectrum() {}

   void BeginRun(TARunInfo* runinfo)
   {
      if(!fFlags->fEnabled) return;
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      gDirectory->mkdir("phspectrum")->cd();

      hawphspect = new TH2D("hawphspect","Avalanche Size Anodes for Tracks;AW;Ne",256,0.,256.,200,0.,2000.);
      hpadphspect = new TH2D("hpadphspect","Avalanche Size Pads for Tracks;pad;Ne",32*576,0.,ALPHAg::_padcol*ALPHAg::_padrow,
                             200,0.,10000.);
      hpwbphspect = new TH2D("hpwbphspect","Max PWB P.H.;pad;PH",32*576,0.,ALPHAg::_padcol*ALPHAg::_padrow,1000,0.,4200.);
      hadcphspect = new TH2D("hadcphspect","Max ADC P.H.;AW;PH",256,0.,256.,1000,0.,17000.);

      hawphtime = new TH2D("hawphtime","Avalanche Size Anodes vs Time for Tracks;Time [ns];Ne",310,-320.,5280.,200,0.,2000.);
      hadcphtime = new TH2D("hadcphtime","Max ADC P.H. vs Time;Time [ns];PH",310,-320.,5280.,1000,0.,17000.);

      hawphspect_pc = new TH2D("hawphspect_pc","Avalanche Size Anodes for Tracks in PC;AW;Ne",256,0.,256.,200,0.,2000.);
      hpadphspect_pc = new TH2D("hpadphspect_pc","Avalanche Size Pads for Tracks in PC;pad;Ne",32*576,0.,ALPHAg::_padcol*ALPHAg::_padrow,
                             200,0.,10000.);
      hpwbphspect_pc = new TH2D("hpwbphspect_pc","Max PWB P.H. in PC;pad;PH",32*576,0.,ALPHAg::_padcol*ALPHAg::_padrow,1000,0.,4200.);
      hadcphspect_pc = new TH2D("hadcphspect_pc","Max ADC P.H. in PC;AW;PH",256,0.,256.,1000,0.,17000.);

      pmap = new ALPHAg::padmap;

      fNtracks = fFlags->fNtracks;
      if (fFlags->ana_settings)
         fCoincTime = fFlags->ana_settings->GetDouble("MatchModule","coincTime");

      printf("PHspectrum::BeginRun() Run %d, Events with >= %d tracks\n", runinfo->fRunNo,fNtracks);
   }

   void EndRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("phspectrum::EndRun %d\n",runinfo->fRunNo);
      delete pmap; 
   }
   
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      if(!fFlags->fEnabled)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      
      AgAnalysisFlow* AnaFlow = flow->Find<AgAnalysisFlow>();
      if( !AnaFlow )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      TStoreEvent* e = AnaFlow->fEvent;
      if( !e )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      
      AgSignalsFlow* SigFlow = flow->Find<AgSignalsFlow>();
      if( !SigFlow )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      if (fTrace)
         printf("phspectrum::AnalyzeFlowEvent %d\n",runinfo->fRunNo);
      
      const std::vector<ALPHAg::TWireSignal>& adc32 = SigFlow->adc32max;
      const std::vector<ALPHAg::TPadSignal>& pwb = SigFlow->pwbMax;
      
      const std::vector<ALPHAg::TWireSignal>& aws = SigFlow->awSig;
      const std::vector<ALPHAg::TPadSignal>& pads = SigFlow->pdSig;

      bool ret1 = false, ret2 = false;
      if( fFlags->fMagneticField > 0. )
         ret1 = HelPHspect(e,adc32,pwb,aws,pads);
      else
         ret2 = LinePHspect(e,adc32,pwb,aws,pads);

      bool there_are_tracks = ret1 || ret2;
      if( there_are_tracks )
         FillSignalHisto(adc32,pwb,aws,pads);
      
      return flow;
   }

   void FillHistos(const TObjArray* points,
                   const std::vector<ALPHAg::TWireSignal> &adc32, const std::vector<ALPHAg::TPadSignal> &pwb,
                   const std::vector<ALPHAg::TWireSignal> &aws, const std::vector<ALPHAg::TPadSignal> &pads)
   {
      int nPoints = points->GetEntriesFast();
      if (fTrace)
         std::cout<<"PHspectrum::FillHistos() # of points: "<<nPoints<<std::endl;
      for(int j=0; j<nPoints; ++j )
         {
            TSpacePoint* sp = (TSpacePoint*) points->At(j);
            double time = sp->GetTime();
            int wire = sp->GetWire();
            for(auto& s: aws)
               {
                  if( s.idx == wire && fabs(s.t - time) < 1.e-3 )
                     {
                        hawphspect->Fill(double(wire),s.height);
                        if( time < fpc_timecut )
                           hawphspect_pc->Fill(double(wire),s.height);
                        //    hawphtime->Fill(time,s.height);
                     }
               }
            for(auto& s: adc32)
               {
                  if( s.idx == wire )
                     {
                        hadcphspect->Fill(double(wire),s.height);
                        if( time < fpc_timecut )
                           hadcphspect_pc->Fill(double(wire),s.height);
                        //    hadcphtime->Fill(time,s.height);
                     }
               }
            
            int sec,row,pad=sp->GetPad();
            pmap->get(pad,sec,row);
            for(auto& s: pads)
               {
                  if( s.idx == row && s.sec == sec && fabs(s.t - time) < fCoincTime )
                     {
                        hpadphspect->Fill(double(pad),s.height);
                        if( time < fpc_timecut )
                           hpadphspect_pc->Fill(double(pad),s.height);
                     }
               }
            for(auto& s: pwb)
               {
                  if( s.idx == row && s.sec == sec )
                     {
                        hpwbphspect->Fill(double(pad),s.height);
                        if( time < fpc_timecut )
                           hpwbphspect_pc->Fill(double(pad),s.height);
                     }
               }
         }// points loop 
   }


   void FillSignalHisto( const std::vector<ALPHAg::TWireSignal> &adc32, const std::vector<ALPHAg::TPadSignal> &pwb,
                         const std::vector<ALPHAg::TWireSignal> &aws, const std::vector<ALPHAg::TPadSignal> &pads)
   {
      if (fTrace)
         std::cout<<"PHspectrum::FillSignalHisto()"<<std::endl;
      for(auto& s: aws)
         {
            hawphspect->Fill(double(s.idx),s.height);
            if( s.t < fpc_timecut )
               hawphspect_pc->Fill(double(s.idx),s.height);
            hawphtime->Fill(s.t,s.height);
         }
      for(auto& s: adc32)
         {
            hadcphspect->Fill(double(s.idx),s.height);
            if( s.t < fpc_timecut )
               hadcphspect_pc->Fill(double(s.idx),s.height);
            hadcphtime->Fill(s.t,s.height);
         }
      
      for(auto& s: pads)
         {
            int pad = pmap->index(s.sec, s.idx);
            
            hpadphspect->Fill(double(pad),s.height);
            if( s.t < fpc_timecut )
               hpadphspect_pc->Fill(double(pad),s.height);
              
         }
      for(auto& s: pwb)
         {
            int pad = pmap->index(s.sec, s.idx);
            hpwbphspect->Fill(double(pad),s.height);
            if( s.t < fpc_timecut )
               hpwbphspect_pc->Fill(double(pad),s.height);
         }
   }

   bool HelPHspect(TStoreEvent* anEvent,
                   const std::vector<ALPHAg::TWireSignal> &adc32, const std::vector<ALPHAg::TPadSignal> &pwb,
                   const std::vector<ALPHAg::TWireSignal> &aws, const std::vector<ALPHAg::TPadSignal> &pads)
   {
      const TObjArray* helices = anEvent->GetHelixArray();
      int nTracks = helices->GetEntriesFast();
      if (fTrace)
         std::cout<<"PHspectrum::HelPHspect event # "<<anEvent->GetEventNumber()<<" @ "<<anEvent->GetTimeOfEvent()<<"s found: "<<nTracks<<" tracks"<<std::endl;
      if( nTracks < fNtracks ) return false;
      else return true;
      for( int i=0; i<nTracks; ++i )
         {
            TStoreHelix* h = (TStoreHelix*) helices->At(i);
            const TObjArray* points = h->GetSpacePoints();
            FillHistos( points, adc32, pwb, aws, pads );
         }// tracks loop
      return true;
   }// function: HelPHspect

   bool LinePHspect(TStoreEvent* anEvent,
                    const std::vector<ALPHAg::TWireSignal> &adc32, const std::vector<ALPHAg::TPadSignal> &pwb,
                    const std::vector<ALPHAg::TWireSignal> &aws, const std::vector<ALPHAg::TPadSignal> &pads)
   {
      const TObjArray* lines = anEvent->GetLineArray();
      int nTracks = lines->GetEntriesFast();
      //if (fTrace)
         std::cout<<"PHspectrum::LinePHspect event # "<<anEvent->GetEventNumber()<<" @ "<<anEvent->GetTimeOfEvent()<<"s found: "<<nTracks<<" tracks"<<std::endl;
      if( nTracks < fNtracks ) return false;
      else return true;
      for( int i=0; i<nTracks; ++i )
         {
            TStoreLine* l = (TStoreLine*) lines->At(i);
            const TObjArray* points = l->GetSpacePoints();
            FillHistos( points, adc32, pwb, aws, pads );
         }// tracks loop
         return true;
   }// function: LinePHspect
};


class PHspectrumFactory: public TAFactory
{
public:
   PHspectrumFlags fFlags;

   void Help()
   {
      printf("PHspectrumFactory::Help\n");
      printf("\t--phspect [Ntracks]\tEnable extractction of Pulse Height Spectra for tracks, default number of tracks is Ntracks = %d\n",fFlags.fNtracks);
      printf("\t--Bfield 0.1234\t\t set magnetic field value in Tesla, default B = %.4f T\n",fFlags.fMagneticField);
   }
   void Usage()
   {
      Help();
   }
   void Init(const std::vector<std::string> &args)
   {     
      TString json="default";
      printf("PHspectrumFactory::Init!\n");
      for(unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--phspect" ) 
               {
                  fFlags.fEnabled = true;
                  if( std::isdigit(args[++i][0]) )
                     fFlags.fNtracks = std::stoi(args[i]);
               }
            if( args[i] == "--Bfield" )
            {
               fFlags.fMagneticField = atof(args[++i].c_str());
            }
            if( args[i] == "--anasettings" ) json=args[++i];
         }
      fFlags.ana_settings = new AnaSettings(json.Data(), args);
   }

   void Finish()
   {
      printf("PHspectrumFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("PHspectrumFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new PHspectrum(runinfo, &fFlags);
   }
};

static TARegister tar(new PHspectrumFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
