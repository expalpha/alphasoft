#include "manalyzer.h"
#include "midasio.h"
#include "AgFlow.h"
#include "RecoFlow.h"
#include "AnaSettings.hh"
#include "json.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TSystem.h"
#include "TMath.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TBarEvent.hh"
#include "TBarCaliFile.h"

class TdcFlags
{
public:
   bool fPrint = false;
   bool fProtoTOF = false;
   bool fLinearTDC = false;
   bool fTimeCut = false;
   std::string fCaliFile = "";
   bool fNoCali = false;
   double start_time = -1.;
   double stop_time = -1.;
   AnaSettings* ana_settings=0;
};


class tdcmodule: public TARunObject
{
public:
   TdcFlags* fFlags;

private:

   TBarCaliFile* Calibration;
   const double TDCwindow;
   std::vector<int> TDCmaskTOF;

   // Constant value declaration
      // https://daq.triumf.ca/elog-alphag/alphag/1961
   const double epoch_freq = 97656.25; // 200MHz/(2<<11); KO+Thomas approved right frequency
   const double coarse_freq = 200.0e6; // 200MHz
      // linear calibration:
      // $ROOTANASYS/libAnalyzer/TRB3Decoder.hxx
   const double trb3LinearLowEnd = 17.0;
   const double trb3LinearHighEnd = 450.0;

   // Counters
   int hit_counter = 0;

   // Trigger channel
   double trigger_channel_time; // Only use trigger channel on fpga 0. 
                                // Each fpga has its own trigger channel,
                                // but we want consistent reference time within event.
   
   // Epoch time
   int epoch_third = -1;
   int epoch_overflow = 0;

   // Container declaration
   std::vector<double> tdc_offsets;
   int BVTdcMap[128][5];
   double fine_counter_lookup_table[256][1024];
   
public:

   tdcmodule(TARunInfo* runinfo, TdcFlags* flags): 
      TARunObject(runinfo), fFlags(flags),
      TDCwindow(flags->ana_settings->GetDouble("BscModule","TDCwindow")),
      TDCmaskTOF(fFlags->ana_settings->GetIntVector("BscModule","TDCmaskTOF"))
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="bsc tdc module";
#endif
      printf("tdcmodule::ctor!\n");
   }

   ~tdcmodule()
   {
      printf("tdcmodule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);

      // Loads BV map of maps
      TString BVmapmapfile=getenv("AGRELEASE");
      BVmapmapfile+="/ana/bscint/TDC_which_map_to_use.map";
      TString BVmapfile="";
      std::ifstream fMapOfMaps(BVmapmapfile.Data());
      std::vector<std::string> a_filename;
      int i_file = 0;
      if (fMapOfMaps) {
         int thisrun = runinfo->fRunNo;
         std::vector<int> run_no_start;
         std::vector<int> run_no_stop;
         std::string line; getline(fMapOfMaps, line);
         while (fMapOfMaps) {
            run_no_start.push_back(44);
            run_no_stop.push_back(44);
            a_filename.push_back("somethingiswrongifyoureseeingthis");
            fMapOfMaps >> run_no_start[i_file] >> run_no_stop[i_file] >> a_filename[i_file];
            if (a_filename[i_file]=="" or a_filename[i_file]==" ") break;
            if (run_no_start[i_file]<=thisrun and run_no_stop[i_file]>thisrun) {
               BVmapfile+=getenv("AGRELEASE");
               BVmapfile+="/ana/bscint/TDC_";
               BVmapfile+=a_filename[i_file];
               BVmapfile+=".map";
               break;
            }
            i_file++;
         }
         // Loads BV map
         if (BVmapfile!="") {
            std::ifstream fBVMap(BVmapfile.Data());
            if(fBVMap) {
               printf("bsc_tdc_module.cxx: Loading BV TDC map from %s\n",BVmapfile.Data());
               std::string comment; getline(fBVMap, comment);
               for(int i=0; i<128; i++) {
                  fBVMap >> BVTdcMap[i][0] >> BVTdcMap[i][1] >> BVTdcMap[i][2] >> BVTdcMap[i][3] >> BVTdcMap[i][4];
               }
               fBVMap.close();
            }
            else printf("~~~~~~~~~~~ WARNING!\nbsc_tdc_module.cxx: Cannot load map file for run %d from file %s\n~~~~~~~~~~~ WARNING!\n",thisrun,BVmapfile.Data());
         }
         else printf("~~~~~~~~~~~ WARNING!\nbsc_tdc_module.cxx: Cannot load map file for run %d from file %s\n~~~~~~~~~~~ WARNING!\n",thisrun,BVmapfile.Data());

         // Load fine counter look up table from json file
         TString filename=TString::Format("%s/ana/bscint/fine_counters_%s.json",getenv("AGRELEASE"),a_filename[i_file].c_str());
         std::cout<<"Loading fine counter lookup table from "<<filename.Data()<<std::endl;
         std::ifstream json_file(filename.Data());
         std::stringstream strStream;
         strStream << json_file.rdbuf(); //read the file
         std::string str = strStream.str(); //str holds the content of the file
         nlohmann::json fine_counter_lookup_json = nlohmann::json::parse(str);

         for (int i=0;i<256;i++) {
            auto tablei = fine_counter_lookup_json[i];
            for (int j=0;j<1024;j++) {
               fine_counter_lookup_table[i][j] = tablei[j];
            }
         }
         std::cout<<"Fine counter look up table json parsing success!"<<std::endl;
         std::cout<<"Fine counter look up table loaded from "<<filename.Data()<<std::endl;
         json_file.close();

      }

      // Loads calibration file
      Calibration = new TBarCaliFile();
      if (!fFlags->fNoCali) {
         if (fFlags->fCaliFile=="") Calibration->LoadCali(runinfo->fRunNo,true);
         else Calibration->LoadCaliFile(fFlags->fCaliFile,true);
      }

      printf("Masking TDC channels from TOF: ");
      for (int i: TDCmaskTOF) printf("%d, ",i);
      printf("\n");

   }
   void EndRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("EndRun, run %d\n", runinfo->fRunNo);
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("ResumeRun, run %d\n", runinfo->fRunNo);
   }

   // Main function
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      // Unpack Event flow
      AgEventFlow *ef = flow->Find<AgEventFlow>();

      if (!ef || !ef->fEvent)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }

      AgEvent* age = ef->fEvent;
      if(!age)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      if (fFlags->fTimeCut) {
         if (age->time<fFlags->start_time) {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
         if (age->time>fFlags->stop_time) {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      }
      
      // Unpack tdc data from event
      TdcEvent* tdc = age->tdc;

      if( tdc )
         {
            if( tdc->complete )
               {
                  AgBarEventFlow *bef = flow->Find<AgBarEventFlow>();
                  if (!bef) return flow;
                  TBarEvent *barEvt = bef->BarEvent;
                  if (!barEvt) return flow;
                  if( fFlags->fPrint ) printf("tdcmodule::AnalyzeFlowEvent analysing event\n");

                  std::vector<TBarTDCHit> tdc_hits = GetTDCHits(tdc,barEvt->GetRunTime());
                  std::vector<TBarTDCHit> tdc_hits_clean = Remove165Noise(tdc_hits);
                  for (const TBarTDCHit& t:tdc_hits_clean) barEvt->AddTDCHit(t);

                  if( fFlags->fPrint ) printf("tdcmodule::AnalyzeFlowEvent comlpete\n");
               }
            else
               if( fFlags->fPrint )
                  std::cout<<"tdcmodule::AnalyzeFlowEvent  TDC event incomplete\n"<<std::endl;
         }
      else {
         if( fFlags->fPrint )         
            std::cout<<"tdcmodule::AnalyzeFlowEvent  No TDC event\n"<<std::endl;
      }

      return flow;
   }

   //________________________________
   // MAIN FUNCTIONS

   // Reads the tdc data and creates TBarTDCHits
   std::vector<TBarTDCHit> GetTDCHits(TdcEvent* tdc, double /*event_time*/) {
      trigger_channel_time = -1;
      std::vector<TBarTDCHit> tdc_hits;
      std::vector<TdcHit*> tdc_data = tdc->hits;
      for (TdcHit* t: tdc_data) {
         if (t->rising_edge==1) { // Rising edge
            const int chan = t->chan;
            // Skips bad channels or fine_time=1023 bad events
            if ( chan < 0) continue;
            if (int(t->fine_time)==1023) continue; 
            if (chan ==0) { // Trigger TDC channel
               if (t->fpga!=0) continue; // Only uses fpga 0 trigger channel. fpga 1,2,3 have their own trigger but lets be consistent here.
               int fixed_epoch_counter = FixEpoch(int(t->epoch));
               double tdc_time = GetFinalTimeLinear(fixed_epoch_counter,t->coarse_time,t->fine_time);
               trigger_channel_time = tdc_time;
               continue;
            }
            int endID = -1;
            endID = BVFindBarID(int(t->fpga),int(t->chan));
            if (endID<0 or endID>=128) continue;
            // Corrects for epoch counter overflow
            int fixed_epoch_counter = FixEpoch(int(t->epoch));
            // Gets TDC time
            double tdc_time;
            double fine_time;
            if (fFlags->fLinearTDC or fFlags->fProtoTOF) {
               tdc_time = GetFinalTimeLinear(fixed_epoch_counter,t->coarse_time,t->fine_time); 
               fine_time = GetFineTimeLinear(t->fine_time);
            } 
            else {
               tdc_time = GetFinalTime(fixed_epoch_counter,t->coarse_time,t->fine_time,endID);
               fine_time = GetFineTime(t->fine_time,endID);
            }
            // skips first hit (first hit is "stuck in TDC" from previously)
            hit_counter+=1;
            if (hit_counter==1) continue;
            // Sets epoch third
            if (hit_counter==2) {
               epoch_third = 1+int(TMath::Floor(3* int(t->epoch) / 268435456. ));
               if (fFlags->fPrint) printf("BscTdcModule::Initially setting epoch third to %d\n",epoch_third);
            }
            // Subtracts trigger time
            if (fFlags->fPrint and trigger_channel_time==-1) printf("Data hit before trigger hit -- should not happen!\nData time %.12f trigger time %.12f\n",tdc_time,trigger_channel_time);
            tdc_time = tdc_time - trigger_channel_time;
            // Skips tdc hits outside "TDC windowing window"
            if (-1*TDCwindow > tdc_time or tdc_time > TDCwindow) continue;
            // Calculates the TDC channel offset from the calibration file
            double chan_offset = 0;
            if (Calibration->IsLoaded()) chan_offset = Calibration->GetEndOffset(endID);

            // Checks if channel is masked from TOF
            bool is_good_for_tof = true;
            for (int itt: TDCmaskTOF) {
               if (endID==itt) is_good_for_tof = false;
            }

            // Creates a TBarTDCHit
            TBarTDCHit h;
            h.SetEndID(endID);
            h.SetTime(tdc_time);
            h.SetFineCount(int(t->fine_time));
            h.SetFineTime(fine_time);
            h.SetChannelOffset(chan_offset);
            h.SetIsGoodForTOF(is_good_for_tof);
            tdc_hits.push_back(h);
         }
         if (t->rising_edge==0) { // Falling edge
            // Skips bad channels or fine_time=1023 bad events
            if (int(t->chan)<=0) continue;
            if (int(t->fine_time)==1023) continue; 
            int endID = -1;
            endID = BVFindBarID(int(t->fpga),int(t->chan));
            if (endID<0) continue;
            // Corrects for epoch counter overflow
            int fixed_epoch_counter = FixEpoch(int(t->epoch));
            // Gets TDC time
            double tdc_time;
            if (fFlags->fLinearTDC or fFlags->fProtoTOF) {
               tdc_time = GetFinalTimeLinear(fixed_epoch_counter,t->coarse_time,t->fine_time); 
            } 
            else {
               tdc_time = GetFinalTime(fixed_epoch_counter,t->coarse_time,t->fine_time,endID);
            }
            // Subtracts zero time
            if (fFlags->fPrint and trigger_channel_time==-1) printf("Data hit before trigger hit -- should not happen!\nData time %.12f trigger time %.12f\n",tdc_time,trigger_channel_time);
            tdc_time = tdc_time - trigger_channel_time;
            // Skips tdc hits outside "TDC windowing window"
            if (-1*TDCwindow > tdc_time or tdc_time > TDCwindow) continue;
            // Finds hit without falling edge
            for (TBarTDCHit& hit: tdc_hits) {
               if (endID!=hit.GetEndID()) continue; // Wrong channel
               if (hit.GetTimeOverThr()!=-1) continue; // Hit already has rising edge
               if (tdc_time < hit.GetTime()) continue; // Falling edge before rising edge
               // Writes time over threshold
               double TOT = tdc_time - hit.GetTime();
               hit.SetTimeOverThr(TOT);
               break;
            }
         }
      }
      return tdc_hits;
   }

   std::vector<TBarTDCHit> Remove165Noise(std::vector<TBarTDCHit> TDCHits) {
      for (TBarTDCHit& h1: TDCHits) {
         double time = h1.GetTime();
         double ch = h1.GetEndID();
         for (TBarTDCHit& h: TDCHits) {
            if (ch==h.GetEndID() and TMath::Abs(time-h.GetTime()-165e-9)<5e-9) h1.SetIs165Reflection(true);
         }
      }
      return TDCHits;
   }

   //________________________________
   // HELPER FUNCTIONS

   int FixEpoch( int epoch_counter )
   {
      int fixed_epoch = 0;
      if (epoch_third==1 and epoch_counter > 89478485 and epoch_counter < 178956970) {
         epoch_third = 2;
      }
      else if (epoch_third==2 and epoch_counter > 178956970) {
         epoch_third = 3;
      }
      else if (epoch_third==2 and epoch_counter < 89478485) {
         epoch_third = 1;
      }
      else if (epoch_third==3 and epoch_counter < 89478485) {
         epoch_third = 1;
         epoch_overflow += 1;
      }
      else if (epoch_third==3 and epoch_counter > 89478485 and epoch_counter < 178956970) {
         epoch_third = 2;
      }
      else if (epoch_third==1 and epoch_counter > 178956970) {
         epoch_third = 3;
         epoch_overflow -= 1;
      }
      fixed_epoch = epoch_counter + epoch_overflow*268435456;
      return fixed_epoch;
   }

   /*double GetTimeWalk(int endID, double TOT) {
      // Not implemented, should get time walk from TDC time over threshold
      return 0;
   }*/

   double GetFinalTimeLinear( uint32_t epoch, uint16_t coarse, uint16_t fine ) // Calculates time from tdc data (in seconds) using the linear approximation
   {
      double B = double(fine) - trb3LinearLowEnd;
      double C = trb3LinearHighEnd - trb3LinearLowEnd;
      return double(epoch)/epoch_freq +  double(coarse)/coarse_freq - (B/C)/coarse_freq;
   }
   double GetCoarseTime( uint32_t epoch, uint16_t coarse) // Calculates coarse time only
   {
      return double(epoch)/epoch_freq +  double(coarse)/coarse_freq;
   }
   double GetFinalTime( uint32_t epoch, uint16_t coarse, uint16_t fine, int bar ) // Calculates time from tdc data (in seconds) using a fine time counter lookup table
   {
      int TDC_chan = BVFindTDCChan(bar);
      auto lookup = fine_counter_lookup_table[TDC_chan];
      double fine_time = lookup[fine];
      return double(epoch)/epoch_freq +  double(coarse)/coarse_freq - fine_time;
   }
   double GetFineTimeLinear( uint16_t fine ) // Calculates fine time correction from tdc data (in seconds) using the linear approximation
   {
      double B = double(fine) - trb3LinearLowEnd;
      double C = trb3LinearHighEnd - trb3LinearLowEnd;
      return (B/C)/coarse_freq;
   }
   double GetFineTime( uint16_t fine, int bar ) // Calculates fine time correction from tdc data (in seconds) using a fine time counter lookup table
   {
      int TDC_chan = BVFindTDCChan(bar);
      auto lookup = fine_counter_lookup_table[TDC_chan];
      double fine_time = lookup[fine];
      return fine_time;
   }

   int BVFindBarID(int fpga, int chan)
   {
      if (chan==0) return -1;
      for (int end=0; end<128; end++) {
         if (fpga+1==BVTdcMap[end][1] and chan==BVTdcMap[end][4]) return end;
      }
      if (fFlags->fPrint) printf("bsc_tdc_module failed to get bar number for fpga %d chan %d \n",fpga+1,chan);
      return -1;
   }
   int BVFindTDCChan(int end) //TDC channel from 0 to 256
   {
      int fpga = -1; // 2, 3, or 4
      int chan = -1; // 1-64
      if (end<0 or end>=128) return -1;
      if (end<128) {
         fpga = BVTdcMap[end][1];
         chan = BVTdcMap[end][4];
      }
      return 64*(fpga-1) + chan -1;
   }

};


class tdcModuleFactory: public TAFactory
{
public:
   TdcFlags fFlags;
public:
   void Help()
   {  
      printf("TdcModuleFactory::Help\n");
      printf("\t--anasettings /path/to/settings.json\t\t load the specified analysis settings\n");
      printf("\t--bscprint\t\t\tverbose mode\n");
      printf("\t--usetimerange 123.4 567.8\t\tLimit analysis to a time range\n");
      printf("\t--linearTDC\t\t\tuses a linear approximation to find the TDC fine time instead of a lookup table\n");
      printf("\t--bscProtoTOF\t\t\tanalyze run with with TRIUMF prototype instead of full BV\n");
      printf("\t--bsccalifile /path/to/BarrelCalibration.root\t\tloads barrel calibration directly from specified file\n");
      printf("\t--nobsccali\t\tskips loading barrel calibration file\n");
   }
   void Usage()
   {
      Help();
   }
   void Init(const std::vector<std::string> &args)
   {
      TString json="default";
      printf("tdcModuleFactory::Init!\n");
      for (unsigned i=0; i<args.size(); i++) { 
         if( args[i]=="-h" || args[i]=="--help" )
            Help();
         if (args[i] == "--bscprint")
            fFlags.fPrint = true; 
         if( args[i] == "--bscProtoTOF" )
            fFlags.fProtoTOF = true;
         if( args[i] == "--linearTDC" )
            fFlags.fLinearTDC = true;
         if( args[i] == "--anasettings" ) 
            json=args[++i];
         if( args[i] == "--bscnocali")
            fFlags.fNoCali = true;
         if( args[i] == "--bsccalifile") {
            i++;
            fFlags.fCaliFile = args[i];
         }
         if( args[i] == "--usetimerange" )
            {
               fFlags.fTimeCut=true;
               i++;
               fFlags.start_time=atof(args[i].c_str());
               i++;
               fFlags.stop_time=atof(args[i].c_str());
               printf("Using time range for reconstruction: ");
               printf("%f - %fs\n",fFlags.start_time,fFlags.stop_time);
            }
      }
      fFlags.ana_settings = new AnaSettings(json.Data(),args);
   }

   void Finish()
   {
      printf("tdcModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("tdcModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new tdcmodule(runinfo,&fFlags);
   }
};

static TARegister tar(new tdcModuleFactory);


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
