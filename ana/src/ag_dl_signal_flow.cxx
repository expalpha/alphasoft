//
// AG module for adding AgRawSignalsFlow to the event flow
//
// L GOLINO
//

#include <stdio.h>
#include <iostream>
#include "EventTracker.h"
#include "manalyzer.h"
#include "AgFlow.h"
#include "RecoFlow.h"
#include "midasio.h"


/*


#include <TTree.h>

#include "TStoreEvent.hh"

#include "AnaSettings.hh"
#include "json.hpp"

#include "TRecoRawSignalFlow.hh"
#include "TTrackBuilder.hh"*/

class RawSignalFlowFlags
{
public:
   std::string fFileName;
   bool fTrace;

public:
   RawSignalFlowFlags()
   { }

   ~RawSignalFlowFlags() // dtor
   { }
};

class RawSignalFlow: public TARunObject
{
public:
   const bool fTrace = false;
   RawSignalFlowFlags* fFlags;
   EventTracker* fAGDumperEventTracker = NULL;

private:


public:
   RawSignalFlow(TARunInfo* runInfo, RawSignalFlowFlags* f): TARunObject(runInfo),
                                                 fFlags(f)
   {
   }

   ~RawSignalFlow()
   {
      printf("RawSignalFlow::dtor!\n");
   }

   void BeginRun(TARunInfo* runInfo)
   {
      if (fTrace)
         printf("RawSignalFlow::BeginRun, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
      fAGDumperEventTracker = new EventTracker(fFlags->fFileName, runInfo->fRunNo);
   }

   void EndRun(TARunInfo* runInfo)
   {
      if (fTrace)
         printf("RawSignalFlow::EndRun, run %d\n", runInfo->fRunNo);
   }

   void PauseRun(TARunInfo* runInfo)
   {
      if (fTrace)
         printf("RawSignalFlow::PauseRun, run %d\n", runInfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runInfo)
   {
      if (fTrace)
         printf("RawSignalFlow::ResumeRun, run %d\n", runInfo->fRunNo);
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runInfo, TAFlags* /* flags */, TAFlowEvent* flow)
   {
      if( fTrace )
         printf("RawSignalFlow::AnalyzeFlowEvent, run %d\n", runInfo->fRunNo);

      AgEventFlow* ef = flow->Find<AgEventFlow>();
      AgSignalsFlow* sf = flow->Find<AgSignalsFlow>();
      AgAnalysisFlow* agfe=flow->Find<AgAnalysisFlow>();
      if (!ef || !agfe || !sf)
      {
         return flow;
      }
      TStoreEvent* storeEvent=agfe->fEvent;

      if( !fAGDumperEventTracker->IsEventInRange(storeEvent->GetEventNumber(), storeEvent->GetTimeOfEvent()) )
      {
         return flow;
      }


      if(sf)
      {
      //std::cout << "DO WE HET HERE 1???????\n\n\n";
         AgRawSignalsFlow* raw_sig_flow = new AgRawSignalsFlow(flow, sf->awSig, sf->pdSig);

         return raw_sig_flow;
         
         /*printf("Event No: %i has %li AW sigs:\n", age->counter, sf->awSig.size());
         for(size_t i=0; i<sf->awSig.size(); i++)
         {
            printf("Signal No: %li - phi: %f - errphi: %f - t: %f - height: %f - errh: %f - sec: %d - idx: %d - gain: %f\n",
                     i, sf->awSig.at(i).phi, sf->awSig.at(i).errphi, sf->awSig.at(i).t,
                     sf->awSig.at(i).height, sf->awSig.at(i).errh, sf->awSig.at(i).sec, sf->awSig.at(i).idx, sf->awSig.at(i).gain);
         }

         printf("Event No: %i has %li PAD sigs:\n", age->counter, sf->pdSig.size());
         for(size_t i=0; i<sf->pdSig.size(); i++)
         {
            printf("Signal No: %li - z: %f - errz: %f - phi: %f - errphi: %f - t: %f - height: %f - errh: %f - sec: %d - idx: %d - gain: %f\n",
                     i, sf->pdSig.at(i).z, sf->pdSig.at(i).errz,
                     sf->pdSig.at(i).phi, sf->pdSig.at(i).errphi, sf->pdSig.at(i).t,
                     sf->pdSig.at(i).height, sf->pdSig.at(i).errh, sf->pdSig.at(i).sec, sf->pdSig.at(i).idx, sf->pdSig.at(i).gain);
         }*/
      }
      return flow;
   }

   void AnalyzeSpecialEvent(TARunInfo* runInfo, TMEvent* event)
   {
      if (fTrace)
         printf("RawSignalFlow::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", runInfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
   }
};

class RawSignalFlowFactory: public TAFactory
{
public:
   RawSignalFlowFlags fFlags;
public:
   void Help()
   {
      printf("TAGDumperFactory::Help!\n");
      printf("\t--eventlist filename\tSave events from TA2Plot.WriteEventList() function\n");
      printf("\t--datalabel label\tValid data labels currently: mixing and cosmic\n");
   }
   void Usage()
   {
      Help();
   }

   RawSignalFlowFactory()
   {

   }

   void Init(const std::vector<std::string> &args)
   {
      printf("Init!\n");
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--eventlist")
               fFlags.fFileName = args[i+1];
            if( args[i] == "--verbose")
               fFlags.fTrace = true;
         }
   }

   void Finish()
   {
      printf("RawSignalFlowFactory::Finish!\n");
   }
   TARunObject* NewRunObject(TARunInfo* runInfo)
   {
      printf("RawSignalFlowFactory::NewRunObject, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
      return new RawSignalFlow(runInfo,&fFlags);
   }
};

static TARegister tar(new RawSignalFlowFactory);




/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
