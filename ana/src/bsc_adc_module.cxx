#include "manalyzer.h"
#include "midasio.h"
#include "AgFlow.h"
#include "RecoFlow.h"
#include "AnaSettings.hh"
#include "json.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#include <numeric>
#include "TMath.h"
#include "TH1D.h"
#include "TF1.h"
#include "TFile.h"
#include "TTree.h"
#include "TSystem.h"
#include "TBarEvent.hh"
#include "TBarCaliFile.h"
#include"fitBV.hh"
#include"Minuit2/FunctionMinimum.h"
#include"Minuit2/VariableMetricMinimizer.h"


class BscFlags
{
public:
   bool fPrint = false;
   bool fProtoTOF = false; // TRIUMF prototype
   bool fFitAll = false; // If false, fits only saturated waveforms
   bool fFixedThr = false; // If true, use fixed adc threshold. If false, use threshold calibrated by channel gain
   bool fTimeCut = false;
   double start_time = -1.;
   double stop_time = -1.;
   std::string fCaliFile = "";
   bool fNoCali = false;
   AnaSettings* ana_settings=0;
};

class BscAdcModule: public TARunObject
{
private:

   // Set parameters
   const int pedestal_length = 100;
   double amplitude_cut; // Used in place of calibrated noise level if no calibration is found
   //double adc_dynamic_range = 2.0; // ADC dynamic range of 2 volts
   double n_ends = 128;

   TBarCaliFile* Calibration;

public:
   BscFlags* fFlags;

   BscAdcModule(TARunInfo* runinfo, BscFlags* flags)
      : TARunObject(runinfo), 
        amplitude_cut(flags->ana_settings->GetDouble("BscModule","ADCamplitude")),
        fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="bsc adc module";
#endif
      
   }

   ~BscAdcModule()
   {   }


   void BeginRun(TARunInfo* runinfo)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory

      if (fFlags->fProtoTOF) n_ends = 16;

      // Load amplitude calibration from calibration file
      Calibration = new TBarCaliFile();
      if (!fFlags->fNoCali) {
         if (fFlags->fCaliFile=="") Calibration->LoadCali(runinfo->fRunNo,true);
         else Calibration->LoadCaliFile(fFlags->fCaliFile,true);
      }

   }

   void EndRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("EndRun, run %d\n", runinfo->fRunNo);
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if( fFlags->fPrint ) printf("ResumeRun, run %d\n", runinfo->fRunNo);
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      const AgEventFlow *ef = flow->Find<AgEventFlow>();

      if (!ef || !ef->fEvent)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      const AgEvent* e = ef->fEvent;
      const Alpha16Event* data = e->a16;
      if( !data )
         {
            std::cout<<"BscAdcModule::AnalyzeFlowEvent(...) No Alpha16Event in AgEvent # "
                     <<e->counter<<std::endl;
            return flow;
         }
      if (fFlags->fTimeCut) {
         if (e->time<fFlags->start_time) {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
         if (e->time>fFlags->stop_time) {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      }

      if( fFlags->fPrint )
         printf("BscAdcModule::AnalyzeFlowEvent(...) Event number is : %d \n", data->counter);

      std::vector<TBarADCHit> adc_hits = AnalyzeBars(data, runinfo);
      TBarEvent* BarEvent = new TBarEvent();
      BarEvent->SetID(e->counter);
      BarEvent->SetRunTime(e->time);
      for (const TBarADCHit& h: adc_hits) BarEvent->AddADCHit(h);

      if( fFlags->fPrint )
         printf("BscAdcModule::AnalyzeFlowEvent(...) has %d hits\n",BarEvent->GetNumEnds());

      flow = new AgBarEventFlow(flow, BarEvent);
      return flow;
   }

// -------------- Main function, called for each event ----------

   std::vector<TBarADCHit> AnalyzeBars(const Alpha16Event* data, TARunInfo* /*runinfo*/)
   {
      std::vector<Alpha16Channel*> channels = data->hits;
      std::vector<TBarADCHit> adc_hits;
      for(unsigned int i = 0; i < channels.size(); ++i)
         {
            auto& ch = channels.at(i);   // Alpha16Channel*
            int chan = ch->adc_chan;
            int bar = ch->bsc_bar;
            if (fFlags->fProtoTOF) bar=chan;

            // Cuts out AW channels
            if( chan >= 16 ) continue; // it's AW
            if( bar < 0 ) continue;

            // Calculates baseline
            double sum = 0;
            double sum_sq = 0;
            for(int b = 0; b < pedestal_length; b++) {
               sum += ch->adc_samples.at(b);
               sum_sq += ch->adc_samples.at(b)*ch->adc_samples.at(b);
            }
            double baseline = sum/pedestal_length;
            double baseline_sigma = TMath::Sqrt(sum_sq/pedestal_length - baseline*baseline);
            double threshold = 2*baseline_sigma;

            std::vector<int> start_times;
            std::vector<int> end_times;
            std::vector<int> max_values;
            int starts_negative = -1;
            std::vector<int> hit_starts_negative;

            // Finds start, end, and amplitudes of all peaks over calibrated threshold in waveform
            int sample_length = int(ch->adc_samples.size());
            bool over_thr = false;
            int max_value = 0;
            bool flat_bottom = false;
            for (int ii=0;ii<sample_length;ii++) {
               if (ch->adc_samples.at(ii) < -32750) flat_bottom = true;
               if (over_thr) {
                  if (ch->adc_samples.at(ii) - baseline < threshold) {
                     if (ii<699 and (ch->adc_samples.at(ii+1) - baseline > threshold)) continue; // Checks next three bins to see
                     if (ii<698 and (ch->adc_samples.at(ii+2) - baseline > threshold)) continue; // if pulse really ended
                     if (ii<697 and (ch->adc_samples.at(ii+3) - baseline > threshold)) continue; // or just noise around threshold
                     over_thr=false; // End of pulse
                     max_values.push_back(max_value);
                     end_times.push_back(ii);
                     hit_starts_negative.push_back(starts_negative);
                     max_value = 0;
                     starts_negative = -1;
                  }
                  if (ch->adc_samples.at(ii) - baseline > max_value) {
                     max_value = ch->adc_samples.at(ii) - baseline;
                  }
               }
               else {
                  if (ch->adc_samples.at(ii) - baseline < -10*threshold && starts_negative==-1) { // If pulse starts by going negative
                     starts_negative=1;
                  }
                  if (ch->adc_samples.at(ii) - baseline > threshold) {
                     over_thr=true; // Start of pulse
                     start_times.push_back(ii);
                     if (starts_negative==-1) starts_negative=0;
                  }
               }
            }
            // Final pulse never ended...
            if (end_times.size()==start_times.size()-1) {
               end_times.push_back(700);
               max_values.push_back(max_value);
               hit_starts_negative.push_back(starts_negative);
            }
            if (end_times.size()!=start_times.size() or start_times.size()!=max_values.size()) {
               printf("bsc_adc_module::Unequal number of start and end times! This should never happen\n");
            }
            
            // Add each hit seperately...
            for (size_t hit_i = 0; hit_i < start_times.size(); hit_i++) {
               double amp = max_values[hit_i];

               // Sets weights of saturated bins to zero
               std::vector<double> weights(ch->adc_samples.size(),0.);
               bool saturated = false;
               int lower_limit = start_times[hit_i];
               int upper_limit = end_times[hit_i];
               if (lower_limit > 20) lower_limit-=20;
               if (upper_limit < 695) upper_limit+=4;
               for (int ii=lower_limit;ii<upper_limit;ii++) {
                  if (ch->adc_samples.at(ii) > 32750) {
                     weights[ii] = 0.;
                     saturated = true;
                  }
                  else weights[ii] = 1.;
               }

               double fit_amp=amp;
               std::vector<double> fit_params(5,-1);

               // Fits pulses with minuit2
               if (fFlags->fFitAll or saturated) {

                  // Creates fitting FCN
                  SkewGaussFcn theFCN(ch->adc_samples, weights);

                  // Initial fitting parameters
                  std::vector<double> init_params = {amp*2, ((lower_limit+20)+(upper_limit-4))/2., ((upper_limit-4)-(lower_limit+20))/4., 0.2, baseline};
                  std::vector<double> init_errors = {amp, ((lower_limit+20)+(upper_limit-4))/20., ((upper_limit-4)-(lower_limit+20))/80., 0.02, 300};

                  // Creates minimizer
                  ROOT::Minuit2::VariableMetricMinimizer theMinimizer; 
                  theMinimizer.Builder().SetPrintLevel(0);
                  int error_level_save = gErrorIgnoreLevel;
                  gErrorIgnoreLevel = kFatal;

                  // Minimize to fit waveform
                  ROOT::Minuit2::FunctionMinimum min = theMinimizer.Minimize(theFCN, init_params, init_errors);
                  gErrorIgnoreLevel = error_level_save;

                  // Gets minimized parameters
                  ROOT::Minuit2::MnUserParameterState the_state = min.UserState();
                  fit_amp = the_state.Value(0) - baseline;
                  for (int fpi=0; fpi<5; fpi++) {
                     fit_params[fpi] = the_state.Value(fpi);
                  }

                  // Fallback in case fitting fails
                  if (saturated and fit_amp<amp) fit_amp = amp;

               }

               // Writes TBarADCHit
               TBarADCHit h;
               h.SetEndID(bar);
               h.SetStartTime(start_times[hit_i]*10);
               h.SetEndTime(end_times[hit_i]*10);
               h.SetFlatTop(saturated);
               h.SetFlatBottom(flat_bottom);
               h.SetAmp(amp);
               h.SetChannelNoise((fFlags->fFixedThr or !Calibration->IsLoaded()) ? amplitude_cut : Calibration->GetADCNoiseLevel(bar));
               h.SetAmpFit(fit_amp);
               h.SetStartsNegative((hit_starts_negative.empty()) ? -1 : hit_starts_negative[hit_i]);
               h.SetBaseline(baseline);
               h.SetBaselineSigma(baseline_sigma);
               for (int fpi=0; fpi<5; fpi++) {
                  h.SetFitParam(fpi, fit_params[fpi]);
               }
               adc_hits.push_back(h);

            }
         }
      return adc_hits;
   }

};

class BscAdcModuleFactory: public TAFactory
{
public:
   BscFlags fFlags;
public:
   void Help()
   {   
      printf("BscAdcModuleFactory::Help\n");
      printf("\t--anasettings /path/to/settings.json\t\t load the specified analysis settings\n");
      printf("\t--bscProtoTOF\t\t\tanalyze run with with TRIUMF prototype instead of full BV\n");
      printf("\t--bscprint\t\t\tverbose mode\n");
      printf("\t--bscfitall\t\t\tfits all bsc adc waveforms instead of only saturated waveforms\n");
      printf("\t--bscfixedthr\t\t\tfix adc threshold, instead of using calibrated noise level\n");
      printf("\t--usetimerange 123.4 567.8\t\tLimit analysis to a time range\n");
      printf("\t--bsccalifile /path/to/BarrelCalibration.root\t\tloads barrel calibration directly from specified file\n");
      printf("\t--nobsccali\t\tskips loading barrel calibration file\n");
   }
   void Usage()
   {
      Help();
   }
   void Init(const std::vector<std::string> &args)
   {
      TString json="default";
      printf("BscAdcModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++) {
         if( args[i]=="-h" || args[i]=="--help" )
            Help();
         if (args[i] == "--bscprint")
            fFlags.fPrint = true;
         if (args[i] == "--bscProtoTOF")
            fFlags.fProtoTOF = true;
         if( args[i] == "--anasettings" ) 
            json=args[++i];
         if( args[i] == "--bscfitall")
            fFlags.fFitAll = true;
         if( args[i] == "--bscfixedthr")
            fFlags.fFixedThr = true;
         if( args[i] == "--bscnocali")
            fFlags.fNoCali = true;
         if( args[i] == "--bsccalifile") {
            i++;
            fFlags.fCaliFile = args[i];
         }
         if( args[i] == "--usetimerange" )
            {
               fFlags.fTimeCut=true;
               i++;
               fFlags.start_time=atof(args[i].c_str());
               i++;
               fFlags.stop_time=atof(args[i].c_str());
               printf("Using time range for reconstruction: ");
               printf("%f - %fs\n",fFlags.start_time,fFlags.stop_time);
            }
      }
      fFlags.ana_settings = new AnaSettings(json,args);
   }

   void Finish()
   {
      printf("BscAdcModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("BscAdcModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new BscAdcModule(runinfo, &fFlags);
   }
};

static TARegister tar(new BscAdcModuleFactory);


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
