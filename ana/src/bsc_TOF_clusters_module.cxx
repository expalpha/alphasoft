#include "manalyzer.h"
#include "midasio.h"
#include "AgFlow.h"
#include "RecoFlow.h"
#include "AnaSettings.hh"
#include "json.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#include "TMath.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"
#include "TBarEvent.hh"
#include "TBarCaliFile.h"
#include "TBarCluster.h"
#include "TBarTOF.h"
#include "TVector3.h"

class TOFModuleFlags
{
public:
   bool fPrint = false;
   std::string fCaliFile = "";
   bool fNoCali = false;
   AnaSettings* ana_settings=0;

   TOFModuleFlags() // ctor
   { }
   ~TOFModuleFlags() // dtor
   { }
};

class BscTOFModule: public TARunObject
{
public:
   TOFModuleFlags* fFlags;

private:

   TBarCaliFile* Calibration;

public:

   BscTOFModule(TARunInfo* runinfo, TOFModuleFlags* f): TARunObject(runinfo), fFlags(f)
                 
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="BV TOF Module";
#endif
      printf("BscTOFModule::ctor!\n");
   }

   ~BscTOFModule()
   {
      printf("BscTOFModule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      if (fFlags->fPrint) { printf("BscTOFModule::begin!\n"); };

      Calibration = new TBarCaliFile();
      if (!fFlags->fNoCali) {
         if (fFlags->fCaliFile=="") Calibration->LoadCali(runinfo->fRunNo,true);
         else Calibration->LoadCaliFile(fFlags->fCaliFile,true);
      }
   }


   void EndRun(TARunInfo* runinfo)
   {

      if (fFlags->fPrint) {printf("EndRun, run %d\n", runinfo->fRunNo);}
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if (fFlags->fPrint) {printf("PauseRun, run %d\n", runinfo->fRunNo);}
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fFlags->fPrint) {printf("ResumeRun, run %d\n", runinfo->fRunNo);}
   }

   // Main function
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
   
      AgBarEventFlow *bf = flow->Find<AgBarEventFlow>();
      if(!bf)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      TBarEvent *barEvt=bf->BarEvent;
      if (!barEvt)
      {
         if (fFlags->fPrint) {printf("BscTOFModule: TBarEvent not found!\n");}
         return flow;
      }
         
      if( fFlags->fPrint ) printf("BscTOFModule::AnalyzeFlowEvent(...) Start\n");
      
      if (barEvt->GetNumBarsHalfComplete()>30) {
         if( fFlags->fPrint ) printf("BscTOFModule::AnalyzeFlowEvent(...) Skipping event - More than 30 half complete bar hits\n");
         return flow;
      }
      //Clusterize(barEvt);
      GenerateClusters(barEvt);
      FindTOFs(barEvt);
      
      if( fFlags->fPrint ) printf("BscTOFModule::AnalyzeFlowEvent(...) End\n");

      //AgBarEventFlow 
      //e->SetBarEvent(barEvt);
      return flow;
   }

   //________________________________
   // MAIN FUNCTIONS

   void Clusterize(TBarEvent* barEvt) {
      std::vector<TBarCluster> clusters;
      std::vector<bool> valid;
      for (TBarHit &h: barEvt->GetBarHits()) {
         if (!h.IsHalfComplete()) continue;
         int barID = h.GetBarID();
         bool match = false;
         for (unsigned int i=0; i<clusters.size(); i++) {
            TBarCluster &c = clusters.at(i);
            if (barID==c.GetBarIDBefore() or barID==c.GetBarIDAfter()) { // could add some timing/Z requirement here
               if (match==true) { // second match, we have to merge clusters
                  int old_cluster_num = h.GetClusterNum();
                  valid.at(old_cluster_num) = false;
                  TBarCluster &c1 = clusters.at(old_cluster_num);
                  for (TBarHit &h2: c1.GetBarHits()) {
                     c.AddBarHit(h2);
                     h2.SetClusterNum(i);
                  }
               }
               match = true;
               c.AddBarHit(h);
               h.SetClusterNum(i);
            }
         }
         if (match==false) { // no match, make a new cluster
            TBarCluster cl;
            cl.AddBarHit(h);
            h.SetClusterNum(clusters.size());
            clusters.push_back(cl);
            valid.push_back(true);
         }
      }
      for (unsigned int i=0; i<clusters.size(); i++) {
         if (!valid[i]) continue;
         barEvt->AddCluster(clusters[i]);
      }
   }

   void GenerateClusters(TBarEvent* barEvt) {
      std::vector<TBarCluster> clusters;
      std::vector<TBarHit>& barhits = barEvt->GetBarHits();

      // Sorts bar hits by bar ID. Should avoid having to merge clusters in case of e.g. 1->2->4->3
      std::sort(barhits.begin(),barhits.end(),CompareBarHits);

      int current_barID = -999;
      int last_barID = -999;
      std::vector<int> last_cluster_ids; // too dumb to figure out pass by reference... this is shady but should work

      for (unsigned int i_hit=0; i_hit<barhits.size(); i_hit++) {
         TBarHit &h = barhits.at(i_hit);
         if (!h.IsHalfComplete()) continue;
         current_barID = h.GetBarID();
         if (current_barID==last_barID+1 or current_barID==last_barID) { // Add to previous cluster
            int cluster_num = clusters.size()-1;
            last_cluster_ids.push_back(i_hit);
            clusters[cluster_num].AddBarHit(h);
            h.SetClusterNum(cluster_num);
         }
         else { // Create new cluster
            TBarCluster cl;
            cl.AddBarHit(h);
            h.SetClusterNum(clusters.size());
            clusters.push_back(cl);
            last_cluster_ids = {(int)i_hit};
         }
         last_barID = current_barID;
      }
      if (clusters.size()>=2) { 
         TBarCluster& first = clusters[0];
         TBarCluster& last = clusters[clusters.size()-1];
         if (first.GetStartBarID()==0 and last.GetEndBarID()==63) { // wraparound, merge clusters
            for (int i: last_cluster_ids) {
               first.AddBarHit(barhits.at(i));
               TBarHit& h = barhits.at(i);
               h.SetClusterNum(0);
            }
            clusters.pop_back();
         }
      }

      for (unsigned int i=0; i<clusters.size(); i++) {
         barEvt->AddCluster(clusters[i]);
      }

   }

   void FindTOFs(TBarEvent* barEvt) {
      const std::vector<TBarHit>& barHits = barEvt->GetBarHits();
      for (unsigned int i=0; i<barHits.size(); i++) {
         const TBarHit &hi = barHits.at(i);
         if (!hi.IsComplete()) continue;
         if (!hi.HasTime()) continue;
         if (!hi.IsGoodForTOF()) continue;
         for (unsigned int j=i+1; j<barHits.size(); j++) {
            const TBarHit &hj = barHits.at(j);
            if (!hj.IsComplete()) continue;
            if (!hj.HasTime()) continue;
            if (!hj.IsGoodForTOF()) continue;
            TBarTOF tof(hi,hj);
            int bar_1 = tof.GetFirstHit().GetBarID();
            int bar_2 = tof.GetSecondHit().GetBarID();
            if (Calibration->IsLoaded()) {
               tof.SetFirstGlobalOffset(Calibration->GetGlobalOffset(bar_1));
               tof.SetSecondGlobalOffset(Calibration->GetGlobalOffset(bar_2));
               tof.SetBarToBarOffset(Calibration->GetBarToBarOffset(bar_1,bar_2));
               tof.SetBarToBarSigma(Calibration->GetBarToBarSigma(bar_1,bar_2));
               tof.SetOffsetsLoaded(true);
            }
            barEvt->AddTOF(tof);
         }
      }
   }

   static bool CompareBarHits(TBarHit h1, TBarHit h2) {
      return h1.GetBarID() < h2.GetBarID();
   }

};

class TOFModuleFactory: public TAFactory
{
public:
   TOFModuleFlags fFlags;
public:
   void Help()
   { 
      printf("TOFModuleFactory::Help\n");
      printf("\t--anasettings /path/to/settings.json\t\t load the specified analysis settings\n");
      printf("\t--bscprint\t\t verbose printing\n");
      printf("\t--bscdiag\t\t diagnostic histograms\n");
      printf("\t--bsccalifile /path/to/BarrelCalibration.root\t\tloads barrel calibration directly from specified file\n");
      printf("\t--nobsccali\t\tskips loading barrel calibration file\n");
   }
   void Usage()
   {
      Help();
   }
   void Init(const std::vector<std::string> &args)
   {
      TString json="default";
      printf("TOFModuleFactory::Init!\n");
      for (unsigned i=0; i<args.size(); i++)
         {
         if( args[i]=="-h" || args[i]=="--help" )
            Help();
         if( args[i] == "--bscprint")
            fFlags.fPrint = true;
         if( args[i] == "--bscnocali")
            fFlags.fNoCali = true;
         if( args[i] == "--bsccalifile") {
            i++;
            fFlags.fCaliFile = args[i];
         }
         if( args[i] == "--anasettings" ) 
            json=args[++i];
         }
      fFlags.ana_settings = new AnaSettings(json.Data(),args);
   }

   void Finish()
   {
      printf("TOFModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("TOFModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new BscTOFModule(runinfo,&fFlags);
   }
};

static TARegister tar(new TOFModuleFactory);


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
