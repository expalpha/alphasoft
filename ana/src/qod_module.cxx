#include "AgFlow.h"
#include "RecoFlow.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TCanvas.h"

#include "SignalsType.hh"
#include <set>
#include <iostream>

#include "AnaSettings.hh"
class QoDFlags
{
public:
   bool fDiag=false;
   int fHistogramScaleDownFactor = 1;
   AnaSettings* ana_settings=NULL;

public:
   QoDFlags() // ctor
   { }

   ~QoDFlags() // dtor
   { }
};

class QoDModule: public TARunObject
{
public:
   QoDFlags* fFlags = NULL;
   int fCounter;
   bool fTrace = false;
  
private:
   bool diagnostics;

   // Overwritten by  "QualityOfData" : "NpadsCut" in json file
   int fNPadCut = 9999;

   // AW diagonostrics (electronics)
   TProfile* hAdcAmp_prox = NULL;
   TProfile* hADCped_prox = NULL;
   TProfile* hADCrms_prox = NULL;

   // AW diagonostrics (drift)
   TH1D* hOccAw = NULL;
   TH2D* hTimeAmpAw = NULL;

   // pad diagnostics (drift)
   TH2D* hOccPad = NULL;
   TH2D* hTimeAmpPad = NULL;
   TH2D* hTimePadRow = NULL;

   // matching diagnostics
   TH2D* hawcol_sector_time = NULL;

   TCanvas fQODCanvas;

public:
   QoDModule(TARunInfo* runinfo, QoDFlags* f):
      TARunObject(runinfo),
      fFlags(f),
      fCounter(0),
      fQODCanvas("LiveQOD","LiveQOD")
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="QualityOfData";
#endif
      if (f->ana_settings)
         fNPadCut = fFlags->ana_settings->GetInt("PadMergeModule","NpadsCut");
      diagnostics=f->fDiag;
   }

   ~QoDModule()
   {
      if (hAdcAmp_prox)
         delete hAdcAmp_prox;
      if (hADCped_prox)
         delete hADCped_prox;
      if (hADCrms_prox)
         delete hADCrms_prox;

      // AW diagonostrics (drift)
      if (hOccAw)
         delete hOccAw;
      if (hTimeAmpAw)
         delete hTimeAmpAw;

      // pad diagnostics (drift)
      if (hOccPad)
         delete hOccPad;
      if (hTimeAmpPad)
         delete hTimeAmpPad;
      if (hTimePadRow)
         delete hTimePadRow;

      // matching diagnostics
      if (hawcol_sector_time)
         delete hawcol_sector_time; 
    }

   void BeginRun(TARunInfo* runinfo)
   {
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      if(!diagnostics) return;
      printf("QoDModule::BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      fCounter = 0;

      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      gDirectory->mkdir("QOD")->cd();
      hAdcAmp_prox = new TProfile("hAdcAmp_prox","Average Maximum WF Amplitude Vs Channel;AW;ADC",
                                  256,0.,256.,0.,17000.);
      hAdcAmp_prox->SetMinimum(0.);

      hADCped_prox = new TProfile("hADCped_prox","Average ADC pedestal per AW;AW;ADC",
                                  256,0.,256.,-16384.,16384.);
      hADCped_prox->SetMinimum(0.);
      hADCrms_prox = new TProfile("hADCrms_prox","Average ADC RMS per AW;AW;ADC",
                                  256,0.,256.,0.,16384.);
      hADCrms_prox->SetMinimum(0.);
      

      hOccAw = new TH1D("hOccAw","Occupancy per AW (post-deconv);AW number",256,0.,256.);
      hOccAw->SetMinimum(0.);

      hTimeAmpAw = new TH2D("hTimeAmpAw","AW Reconstructed Avalanche Time Vs Size;Drift Time [ns];Aval. size [a.u.]",
                            560,-320.,5280.,
                            200,0.,2000.);

      hOccPad = new TH2D("hOccPad","Occupancy per Pad (post-deconv);row;sec;N",576,0.,576.,32,0.,32.);

      hTimeAmpPad = new TH2D("hTimeAmpPad","Pad Reconstructed Avalanche Time Vs Size",
                             560,-320.,5280.,250,0.,5000.);

      hTimePadRow = new TH2D("hTimePadRow","Reconstructed Avalanche Time Vs Pad Rows;row;time [ns]",576,0.,576,560,-320.,5280.);

      hawcol_sector_time = new TH2D("hawcol_sector_time","AW vs PAD Time with Matching Sector;AW [ns];PAD [ns]",
                                    700,-320.,5280,700,-320.,5280);

      fQODCanvas.Divide(2,2);
      TVirtualPad* p1 = fQODCanvas.cd(1);
      p1->Divide(2,1);
      TVirtualPad* p2 = fQODCanvas.cd(2);
      p2->Divide(2,1);
      TVirtualPad* p3 = fQODCanvas.cd(3);
      p3->Divide(2,1);
      //Reset root directory
      runinfo->fRoot->fOutputFile->cd();
   }

   void EndRun(TARunInfo* runinfo)
   {
      printf("QoDModule::EndRun, run %d    Total Counter %d\n", runinfo->fRunNo, fCounter);
   }

   TAFlowEvent* AnalyzeFlowEvent( __attribute__((unused)) TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {      
      if(!diagnostics)
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      const AgSignalsFlow* SigFlow = flow->Find<AgSignalsFlow>();
      if( !SigFlow )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( fTrace )
         {
            if( SigFlow->adc32max.size() )
               printf("QoDModule::Analyze, ADC # signals %d\n", 
                      int(SigFlow->adc32max.size()));
            else
               printf("QoDModule::Analyze, NO ADC signals\n");
            if( SigFlow->adc32ped.size() )
               printf("QoDModule::Analyze, ADC # pedestals %d\n", 
                      int(SigFlow->adc32ped.size()));
            else
               printf("QoDModule::Analyze, NO ADC pedestals\n");
            if( SigFlow->awSig.size() )
               printf("QoDModule::Analyze, AW # signals %d\n", 
                      int(SigFlow->awSig.size()));
            else
               printf("QoDModule::Analyze, NO AW signals\n");

            if( SigFlow->pdSig.size() )
               printf("QoDModule::Analyze, PAD # signals %d\n",
                      int(SigFlow->pdSig.size()));
            else
               printf("QoDModule::Analyze, NO PAD signals\n");
            
            if( SigFlow->matchSig.size() )
               printf("QoDModule::Analyze, SP # %d\n",
                      int(SigFlow->matchSig.size()));
            else
               printf("QoDModule::Analyze, NO SP matches\n");
         }
      
      // Filling every N'th event... this scale down help online (live) analysis
      if (fCounter % fFlags->fHistogramScaleDownFactor ==0 )
      {
         if( SigFlow->adc32max.size() )
            ADCdiagnostic(SigFlow->adc32max, SigFlow->adc32ped);

         if( SigFlow->awSig.size() )
            AWdiagnostic(SigFlow->awSig);

         if( SigFlow->pdSig.size() && (int) SigFlow->pdSig.size() < fNPadCut )
            PADdiagnostic(SigFlow->pdSig);

         if( SigFlow->pdSig.size() && (int) SigFlow->pdSig.size() < fNPadCut && SigFlow->awSig.size() )
            MatchDiagnostic(SigFlow->awSig,SigFlow->pdSig);
      }

      // Update Live histograms every 500 events (this doesn't need to update very often)
      if (fCounter % 500 == 0)
      {
         std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
         TVirtualPad* p1 = fQODCanvas.cd(1);
         p1->cd(1);
         hTimeAmpAw->Draw();
         gPad->SetGrid();

         p1->cd(2);
         hADCrms_prox->Draw("HIST");
         gPad->SetGrid();

         TVirtualPad* p2 = fQODCanvas.cd(2);
         p2->cd(1);
         hAdcAmp_prox->Draw("HIST");
         gPad->SetGrid();

         p2->cd(2);
         hOccAw->Draw("HIST");
         gPad->SetGrid();

         TVirtualPad* p3 = fQODCanvas.cd(3);
         p3->Divide(2,1);
         p3->cd(1);
         hTimeAmpPad->Draw();
         gPad->SetGrid();

         p3->cd(2);
         hawcol_sector_time->Draw("col");

         fQODCanvas.cd(4);
         hOccPad->Draw();

         fQODCanvas.Draw();
      }

      ++fCounter;
      return flow;
   }

   void AWdiagnostic(const std::vector<ALPHAg::TWireSignal>& sanode)
   {
      int ntop = 0;
      for (const ALPHAg::TWireSignal& sig:sanode)
      { 
         if( sig.sec ) continue;
         hOccAw->Fill(sig.idx);
         hTimeAmpAw->Fill(sig.t,sig.height);
         ++ntop;
      }
      if( fTrace )
         std::cout<<"QoDModule::AWdiagnostic # hits: "<<ntop<<std::endl;
   }

   void ADCdiagnostic(const std::vector<ALPHAg::TWireSignal>& wfamp, const std::vector<ALPHAg::TWireSignal>& wfped)
   {
      if( wfamp.size() > 0 )
      {
         for (const ALPHAg::TWireSignal& sig: wfamp )
         {
            hAdcAmp_prox->Fill(sig.idx,sig.height);
         }
      }

      if( wfped.size() > 0 )
      {
         for (const ALPHAg::TWireSignal& sig: wfped )
         {
            hADCped_prox->Fill(sig.idx,sig.height);
            hADCrms_prox->Fill(sig.idx,sig.errh);
         }
      }
   }

   void PADdiagnostic(const std::vector<ALPHAg::TPadSignal>& spad)
   {
      int nhit=0;
      //std::cout<<"QoDModule::PADdiagnostic()"<<std::endl;
      for (const ALPHAg::TPadSignal& sig: spad)
      { 
         hOccPad->Fill(sig.idx,sig.sec);
         hTimeAmpPad->Fill(sig.t,sig.height);
         hTimePadRow->Fill(sig.idx,sig.t);
         ++nhit;
         // std::cout<<"\t"<<nhit<<" "<<iSig->sec<<" "<<iSig->i<<" "<<iSig->height<<" "<<iSig->t<<std::endl;
      }
      if( fTrace )
         std::cout<<"QoDModule::PADdiagnostic # hit: "<<nhit<<std::endl;
   }

   void MatchDiagnostic(const std::vector<ALPHAg::TWireSignal>& awsignals,
                        const std::vector<ALPHAg::TPadSignal>& padsignals)
   {

      // This is a faster implmentation than below (more cache friendly I think)
      // remember, always test of code and prove an implmentation is faster than other :)
      // I am sort of suprised this is faster as I am allocate memory for the vectors (which is slow)

      // Cache aw times and sectors
      const size_t nwires = awsignals.size();
      std::vector<double> awt;
      std::vector<int> awsec;
      awt.reserve(nwires);
      awsec.reserve(nwires);
      for( const ALPHAg::TWireSignal& aw : awsignals) // Should be fast because there are no 'if' statements
      {
         awt.emplace_back( aw.t );
         awsec.emplace_back(aw.idx/8);
      }

      // Cache pad times and sectors
      const size_t npads = padsignals.size();
      std::vector<double> pdt;
      std::vector<int> pdsec;
      pdt.reserve(npads);
      pdsec.reserve(npads);
      for( const ALPHAg::TPadSignal& pd : padsignals ) // Should be fast because there are no 'if' statements
      {
         pdt.emplace_back( pd.t);
         pdsec.emplace_back( pd.sec );
      }

      // Now the data is compact in memory, now do the 'branchy' code
      int Nmatch = 0;
      for (size_t i = 0; i < nwires; i++)
      {
         const double wiretime = awt[i];
         if ( wiretime <= 0)
            continue;
         const int sector = awsec[i];
         for (size_t j = 0; j < npads; j++)
         {
            if (sector != pdsec[j])
               continue;
            const double padtime = pdt[j];
            if (padtime <=0 )
               continue;
            hawcol_sector_time->Fill( wiretime , padtime );
            ++Nmatch;
         } 
      }
      // The above is a faster implmentation of this:
      // for( const ALPHAg::TWireSignal& aw : awsignals)
      // {
      //    const double awt = aw.t;
      //    if( awt <= 0. )
      //       continue;
      //    const int sector = aw.idx/8;
      //    for( const ALPHAg::TPadSignal& pd : padsignals )
      //    {
      //       if( sector != pd.sec )
      //          continue;
      //       const double pdt = pd.t;
      //       if ( pdt <= 0. )
      //          continue;
      //       hawcol_sector_time->Fill( awt , pdt );
      //       ++Nmatch;
      //    }
      // }
      if( fTrace )
         std::cout<<"QoDModule::Match Number of Matches: "<<Nmatch<<std::endl;
   }

};


class QoDModuleFactory: public TAFactory
{
public:
   QoDFlags fFlags;
   
public:
   void Usage()
   {
      printf("QODModule::Help\n");
      printf("\t--qod [sample event N event]    Enable this module, then sample every N event (N = 1 or higher)\n ");
   }
   void Init(const std::vector<std::string> &args)
   {
      TString json = "default";
      printf("QoDModuleFactory::Init!\n");

      for (size_t i=0; i<args.size(); i++) {
         if( args[i] == "--qod" || args[i] == "--QOD")
         {
            fFlags.fDiag = true;
            fFlags.fHistogramScaleDownFactor = 1;
            // If the next arguement starts with "--", then there isn't a scale down factor
            if (args.size() > i + 1 ) // protect against this being the last argument 
            {
               if (strncmp (args.at(i + 1).c_str(), "--", 2) == 0)
               {
                  fFlags.fHistogramScaleDownFactor = 1;
               }
               else
               {
                  fFlags.fHistogramScaleDownFactor = atoi(args[++i].c_str());
               }
            }
            if ( fFlags.fHistogramScaleDownFactor <= 0)
            {
               std::cout <<"--qod requries a scale down factor:\n";
               Usage();
               exit(1);
            }
            if ( fFlags.fHistogramScaleDownFactor > 100)
            {
               std::cout <<"--qod requries a scale down factor:\n";
               Usage();
               exit(1);
            }
            std::cout << "Enabling QOD plots for every " << fFlags.fHistogramScaleDownFactor << " events" << std::endl;
         }
         else if( args[i] == "--anasettings" )
            json=args[++i];
      }
      fFlags.ana_settings = new AnaSettings(json.Data(), args);
   }

   void Finish()
   {
      printf("QoDModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("QoDModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new QoDModule(runinfo, &fFlags);
   }
};

static TARegister tar(new QoDModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
