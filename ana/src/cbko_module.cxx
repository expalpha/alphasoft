//
// MIDAS analyzer for chronobox data
//
// K.Olchanski
//

#include <stdio.h>
#include <vector>
#include <iostream>
#include <string>

#include "manalyzer.h"
#include "midasio.h"

#include "unpack_cb.h"

#include "cb_flow.h"

class CbkoModule: public TARunObject
{
public:
   bool fTrace = false;

   const double kTsFreq = 10.0e6; // 10 MHz

   uint32_t fT0 = 0;
   uint32_t fTE = 0;
   int fCountWC = 0;
   int fCountMissingWC = 0;

   std::vector<CbUnpack*> fCbUnpack;
   std::vector<std::string> fCbBanks;
   std::vector<std::string> fCbNames;
   std::vector<std::vector<std::vector<CbHit>>> fCbHits;

   // Cross calibration of chronoboxes to sequencer MIDAS time (feXsequencer
   // runs on the DAQ, so is like a good best guess at the actual time)
   std::vector<uint32_t> fCbFirstMidasTime;
   std::vector<bool> fSyncToSequencer;
   std::vector<int> fSyncDiffToSequencer;
   
   int fRunEventCounter = 0;

   bool fCheckHitsOk = true;

   bool fPrint = true;

   CbkoModule(TARunInfo* runinfo) : TARunObject(runinfo)
   {

      fModuleName="cbko_module";
      fRunEventCounter = 0;

      fCbBanks.push_back("CBFT");
      fCbBanks.push_back("CBF1");
      fCbBanks.push_back("CBF2");
      fCbBanks.push_back("CBF3");
      fCbBanks.push_back("CBF4");

      fCbNames.push_back("cbtrg");
      fCbNames.push_back("cb01");
      fCbNames.push_back("cb02");
      fCbNames.push_back("cb03");
      fCbNames.push_back("cb04");

      fCbUnpack.push_back(new CbUnpack(23));
      fCbUnpack.push_back(new CbUnpack(59));
      fCbUnpack.push_back(new CbUnpack(59));
      fCbUnpack.push_back(new CbUnpack(59));
      fCbUnpack.push_back(new CbUnpack(59));

      for (size_t i  = 0; i < fCbUnpack.size(); i++)
      {
         fCbFirstMidasTime.push_back(0);
         fSyncToSequencer.push_back(0);
         fSyncDiffToSequencer.push_back(0);
      }

      fCbUnpack[0]->fKludge = 1;

      fCbHits.resize(fCbBanks.size());
      for (size_t i=0; i<fCbBanks.size(); i++) {
         fCbHits[i].resize(fCbUnpack[i]->fNumInputs);
      }
   }

   ~CbkoModule()
   {
      fCbBanks.clear();
      fCbNames.clear();

      for (CbUnpack* c: fCbUnpack)
         delete c;
      fCbUnpack.clear();

      fCbFirstMidasTime.clear();
      fSyncToSequencer.clear();
      fSyncDiffToSequencer.clear();

      fCbHits.clear();
      // empty
   }
  
   void BeginRun(TARunInfo* runinfo)
   {
      printf("BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      fRunEventCounter = 0;

      for (uint32_t& t: fCbFirstMidasTime)
         t = 0;
   }

   bool KillDupes(size_t ibank, size_t ichan)
   {
      bool all_ok = true;
      size_t nhits = fCbHits[ibank][ichan].size();
      if (nhits < 1)
         return all_ok;
      //printf("%s: chan %zu: %zu hits\n", fCbBanks[ibank].c_str(), ichan, fCbHits[ibank][ichan].size());
      double time_prev = 0;
      double min_dt = 99999;
      size_t count = 0;
      for (size_t ihit=0; ihit<nhits-1; ihit++) {
         if (fCbHits[ibank][ichan][ihit].flags&CB_HIT_FLAG_TE)
            continue;
         double time = fCbHits[ibank][ichan][ihit].time;
         double dt = time-time_prev;
         //if (ibank==0 && ichan==0)
            //   printf("dt %f\n", dt);
         time_prev = time;
         if (dt < 0) {
            printf("time goes backwards!\n");
            all_ok = false;
            break;
         }
         
         if (dt < 0.000001) {
            //printf("%s: chan %zu: hit %zu: duplicate time %.6f\n", fCbBanks[ibank].c_str(), ichan, ihit, time);
            fCbHits[ibank][ichan][ihit].flags = -1;
            count++;
            continue;
         }
         
         if (dt < min_dt)
            min_dt = dt;
      }
      printf("%s: chan %zu: %zu hits, min_dt %.6f, %zu duplicates\n", fCbBanks[ibank].c_str(), ichan, fCbHits[ibank][ichan].size(), min_dt, count);
      return all_ok;
   }

   bool KillDupes()
   {
      bool all_ok = true;
      printf("Check duplicates:\n");
      for (size_t ibank=0; ibank<fCbBanks.size(); ibank++) {
         size_t nchan = fCbHits[ibank].size();
         for (size_t ichan=0; ichan<nchan; ichan++) {
            all_ok |= KillDupes(ibank, ichan);
         }
      }
      printf("Check duplicates: ok %d\n", all_ok);
      return all_ok;
   }

   bool Check(size_t itrgchan, size_t ichan)
   {
      bool all_ok = true;
      size_t all_count = 0;
      printf("Check cbtrg chan %zu and chronobox channel %zu:\n", itrgchan, ichan);
      assert(fCbBanks.size() == 5);
      std::vector<size_t> ihit;
      ihit.resize(fCbBanks.size());
      size_t itrgbank = 0;
      printf("%s: chan %zu: %zu hits\n", fCbBanks[itrgbank].c_str(), itrgchan, fCbHits[itrgbank][itrgchan].size());
      for (size_t ibank=1; ibank<=4; ibank++) {
         printf("%s: chan %zu: %zu hits\n", fCbBanks[ibank].c_str(), ichan, fCbHits[ibank][ichan].size());
      }
      while (1) {
         std::vector<double> tv;
         if (1) {
            if (ihit[itrgbank] >= fCbHits[itrgbank][itrgchan].size())
               break;
            while (fCbHits[itrgbank][itrgchan][ihit[itrgbank]].flags&CB_HIT_FLAG_TE) {
               if (ihit[itrgbank] >= fCbHits[itrgbank][itrgchan].size())
                  break;
               ihit[itrgbank]++;
            }
            if (ihit[itrgbank] >= fCbHits[itrgbank][itrgchan].size())
               break;
            tv.push_back(fCbHits[itrgbank][itrgchan][ihit[itrgbank]].time);
            ihit[itrgbank]++;
         }
         for (size_t ibank=1; ibank<=4; ibank++) {
            if (ihit[ibank] >= fCbHits[ibank][ichan].size())
               break;
            while (fCbHits[ibank][ichan][ihit[ibank]].flags&CB_HIT_FLAG_TE) {
               if (ihit[ibank] >= fCbHits[ibank][ichan].size())
                  break;
               ihit[ibank]++;
            }
            if (ihit[ibank] >= fCbHits[ibank][ichan].size())
               break;
            tv.push_back(fCbHits[ibank][ichan][ihit[ibank]].time);
            ihit[ibank]++;
         }
         if (tv.size() != 5)
            break;

         bool ok = true;
         for (size_t i=0; i<5; i++) {
            if (fabs(tv[i]-tv[0]) > 0.000009)
               ok = false;
         }
         all_ok &= ok;
         all_count++;

         if (!all_ok) {
            for (size_t i=0; i<5; i++) {
               printf(" %.6f", tv[i]);
            }
            printf(" ok %d\n", ok);
         }
      }

      printf("Check cbtrg chan %zu and chronobox channel %zu, %zu hits, ok %d\n", itrgchan, ichan, all_count, all_ok);
      return all_ok;
   }

   bool Check4(size_t ichan)
   {
      bool all_ok = true;
      size_t all_count = 0;
      printf("Check chronobox channel %zu:\n", ichan);
      assert(fCbBanks.size() == 5);
      std::vector<size_t> ihit;
      ihit.resize(fCbBanks.size());
      for (size_t ibank=1; ibank<=4; ibank++) {
         printf("%s: chan %zu: %zu hits\n", fCbBanks[ibank].c_str(), ichan, fCbHits[ibank][ichan].size());
      }
      while (1) {
         std::vector<double> tv;
         for (size_t ibank=1; ibank<=4; ibank++) {
            if (ihit[ibank] >= fCbHits[ibank][ichan].size())
               break;
            while (fCbHits[ibank][ichan][ihit[ibank]].flags&CB_HIT_FLAG_TE) {
               if (ihit[ibank] >= fCbHits[ibank][ichan].size())
                  break;
               ihit[ibank]++;
            }
            if (ihit[ibank] >= fCbHits[ibank][ichan].size())
               break;
            tv.push_back(fCbHits[ibank][ichan][ihit[ibank]].time);
            ihit[ibank]++;
         }
         if (tv.size() != 4)
            break;

         bool ok = true;
         for (size_t i=0; i<4; i++) {
            if (fabs(tv[i]-tv[0]) > 0.000001)
               ok = false;
         }
         all_ok &= ok;
         all_count++;

         if (!all_ok) {
            for (size_t i=0; i<4; i++) {
               printf(" %.6f", tv[i]);
            }
            printf(" ok %d\n", ok);
         }
      }

      printf("Check chronobox channel %zu, %zu hits, ok %d\n", ichan, all_count, all_ok);
      return all_ok;
   }

   bool Check2(size_t ibank1, size_t ichan1, size_t ibank2, size_t ichan2)
   {
      //bool print = true;
      bool print = false;
      //if (ibank1 == 0) print = true;
      bool all_ok = true;
      size_t all_count = 0;
      if (print) {
         printf("Check bank %zu channel %zu against bank %zu channel %zu:\n", ibank1, ichan1, ibank2, ichan2);
         printf("%s: chan %zu: %zu hits, first time %.6f\n", fCbBanks[ibank1].c_str(), ichan1, fCbHits[ibank1][ichan1].size(), fCbHits[ibank1][ichan1][0].time);
         printf("%s: chan %zu: %zu hits, first time %.6f\n", fCbBanks[ibank2].c_str(), ichan2, fCbHits[ibank2][ichan2].size(), fCbHits[ibank2][ichan2][0].time);
      }
      size_t n1 = fCbHits[ibank1][ichan1].size();
      size_t n2 = fCbHits[ibank2][ichan2].size();

      size_t i1 = 0;
      size_t i2 = 0;

      double drift = 0;

      size_t missing1 = 0;
      size_t missing2 = 0;

      size_t dupe1 = 0;

      double t1prev = -1;
      //double t2prev = -1;
      
      while (1) {
         if (i1 >= n1)
            break;
         if (i2 >= n2)
            break;

         while (fCbHits[ibank1][ichan1][i1].flags&CB_HIT_FLAG_TE) {
            i1++;
            if (i1 >= n1)
               break;
         }

         while (fCbHits[ibank2][ichan2][i2].flags&CB_HIT_FLAG_TE) {
            i2++;
            if (i2 >= n2)
               break;
         }

         if (i1 >= n1)
            break;
         if (i2 >= n2)
            break;

         double t1 = fCbHits[ibank1][ichan1][i1].time;
         double t2 = fCbHits[ibank2][ichan2][i2].time;

         if (fabs(t1 - t1prev) < 0.000001) {
            if (print)
               printf("hit %zu %zu time %.6f dupe\n", i1, i2, t1);
            dupe1++;
            all_ok = false;
            i1++;
            t1prev = t1;
            continue;
         }

         if (fabs(t1-t2-drift) < 0.000002) {
            if (print)
               printf("hit %zu %zu time %.6f %.6f match (diff %.6f, drift %.6f)\n", i1, i2, t1, t2, t1-t2, drift);
            drift = t1-t2;
            all_count++;
            i1++;
            i2++;
            t1prev = t1;
            //t2prev = t2;
         } else if (t1 > t2) {
            if (print)
               printf("hit %zu %zu time %.6f %.6f mismatch\n", i1, i2, t1, t2);
            // bank1 ahead of bank2, missing a hit? check next hits in bank2
            missing1++;
            all_ok = false;
            i2++;
            all_count++;
         } else if (t1 < t2) {
            if (print)
               printf("hit %zu %zu time %.6f %.6f mismatch\n", i1, i2, t1, t2);
            // bank2 ahead of bank1, missing a hit? check next hits in bank1
            missing2++;
            all_ok = false;
            i1++;
            all_count++;
         } else {
            printf("hit %zu %zu time %.6f %.6f mismatch, stopping\n", i1, i2, t1, t2);
            all_ok = false;
            break;
         }
      }

      if (fabs(drift) > 0.000001)
         all_ok = false;

      printf("Check bank %s channel %2zu against bank %s channel %2zu: matching %zu, bank %s: missing %zu, dupe %zu, missing in bank %s: %zu, drift %.6f. ok %d\n", fCbBanks[ibank1].c_str(), ichan1, fCbBanks[ibank2].c_str(), ichan2, all_count, fCbBanks[ibank1].c_str(), missing1, dupe1, fCbBanks[ibank2].c_str(), missing2, drift, all_ok);

      return all_ok;
   }

   void EndRun(TARunInfo* runinfo)
   {
      printf("EndRun, run %d\n", runinfo->fRunNo);
      printf("Counted %d events in run %d\n", fRunEventCounter, runinfo->fRunNo);
      double elapsed = fTE - fT0;
      double minutes = elapsed/60.0;
      double hours = minutes/60.0;
      printf("Elapsed time %d -> %d is %.0f sec or %f minutes or %f hours\n", fT0, fTE, elapsed, minutes, hours);
      if (fCountMissingWC) {
         printf("Wraparounds: %d, missing %d, cannot compute TS frequency\n", fCountWC, fCountMissingWC);
      } else {
         double tsbits = (1<<24);
         printf("Wraparounds: %d, approx rate %f Hz, ts freq %.1f Hz\n", fCountWC, fCountWC/elapsed, 0.5*fCountWC/elapsed*tsbits);
      }

      for (size_t ibank=0; ibank<fCbBanks.size(); ibank++) {
         for (size_t ichan=0; ichan<fCbHits[ibank].size(); ichan++) {
            if (fCbHits[ibank][ichan].size() > 0) {
               printf("%s: chan %zu: %zu hits\n", fCbBanks[ibank].c_str(), ichan, fCbHits[ibank][ichan].size());
            }
         }
      }

      if (0) {
      for (size_t ibank=0; ibank<fCbBanks.size(); ibank++) {
         for (size_t ichan=0; ichan<fCbHits[ibank].size(); ichan++) {
            if (ibank==0 && ichan!=0 && ichan!=3 && ichan!=4)
               continue;
            if (ibank>0 && ichan != 33)
               continue;
            printf("%s: chan %zu: %zu hits\n", fCbBanks[ibank].c_str(), ichan, fCbHits[ibank][ichan].size());
            //if (ibank==0)
            //   continue;
            for (size_t ihit=0; ihit<fCbHits[ibank][ichan].size(); ihit++) {
               const CbHit* phit = &fCbHits[ibank][ichan][ihit];
               if (phit->flags & CB_HIT_FLAG_TE)
                  continue;
               printf("%s: hit %zu, time %.6f sec, %d+%d, channel %2d (%d)\n", fCbBanks[ibank].c_str(), ihit, phit->time, phit->timestamp, phit->epoch, phit->channel, (phit->flags&CB_HIT_FLAG_TE));
            }
         }
      }
      }

      bool ok = true;

      if (!fCheckHitsOk) {
         ok = false;
         printf("CBFx: Chronobox CheckHits has FAILED, see above messages!\n");
      } else {
         printf("CBFx: Chronobox CheckHits ok!\n");
      }

      //KillDupes();

      //KillDupes(0, 0);
      //KillDupes(0, 1);
      //KillDupes(0, 2);
      //KillDupes(0, 3);

      ok &= Check4(33);
      ok &= Check4(36);

      //Check(0, 33);
      //Check(1, 33);
      //Check(2, 33);
      //Check(3, 33);
      //Check(3, 36);

      ok &= Check2(1, 33, 1, 36);
      ok &= Check2(1, 33, 2, 33);
      ok &= Check2(0, 0, 1, 33);
      ok &= Check2(0, 1, 1, 33);
      ok &= Check2(0, 2, 1, 33);
      ok &= Check2(0, 3, 1, 33);

      if (ok)
         printf("CBFx: Chronobox checks ok!\n");
      else
         printf("CBFx: Chronobox checks FAILED!\n");
   }

   bool CheckHitsRange(int ibank, const CbHits& hits, size_t first, size_t last)
   {
      bool ok = true;

      double min_time = hits[first].time;
      double max_time = hits[first].time;
      int min_epoch = hits[first].epoch;
      int max_epoch = hits[first].epoch;
      
      for (size_t i=first; i<last; i++) {
         double time = hits[i].time;
         int epoch = hits[i].epoch;
         if (time < min_time) min_time = time;
         if (time > max_time) max_time = time;
         if (epoch < min_epoch) min_epoch = epoch;
         if (epoch > max_epoch) max_epoch = epoch;
      }
      
      if (max_time - min_time > 1.01) {
         ok = false;
      }

      if (!ok) {
         //printf("range %zu..%zu: ", first, last);
         printf("%s, hits: %6zu, time: %.6f..%.6f (diff %.6f) sec, epoch: %d..%d (diff %d)\n", fCbBanks[ibank].c_str(), last-first, min_time, max_time, max_time-min_time, min_epoch, max_epoch, max_epoch-min_epoch);
      }
      
      if (!ok) {
         for (size_t i=first; i<last; i++) {
            //int channel = hits[i].channel;
            //double time = hits[i].time;
            //int epoch = hits[i].epoch;
            
            //printf("%s: hit %zu, time %.6f sec, %d+%d, channel %2d (%d)\n", fCbBanks[ibank].c_str(), i, hits[i].time, hits[i].timestamp, hits[i].epoch, hits[i].channel, (hits[i].flags&CB_HIT_FLAG_TE));
         }
      }

      return ok;
   }

   bool CheckHits(int ibank, const CbHits& hits)
   {
      if (hits.empty())
         return true;

      bool ok = true;

      if (hits.size() < 10000) {
         ok &= CheckHitsRange(ibank, hits, 0, hits.size());
      } else {
         size_t first = 0;
         while (1) {
            bool done = false;
            size_t last = first + 10000;
            if (last > hits.size()) {
               last = hits.size();
               done = true;
            }
            ok &= CheckHitsRange(ibank, hits, first, last);
            if (done)
               break;
            first = last;
         }
      }

      return ok;
   }
   std::vector<std::shared_ptr<TCbFIFOEvent>> BuildFIFOFlow(const std::string name, const CbHits& hits, int runNumber)
   {
      std::vector<std::shared_ptr<TCbFIFOEvent>> FIFOEvents;
      FIFOEvents.reserve(hits.size());
      for (const CbHit& hit: hits)
      {
         FIFOEvents.emplace_back( std::make_shared<TCbFIFOEvent>(hit, name,runNumber) );
      }
      return FIFOEvents;
   }
   void AnalyzeCbFifo(int ibank, CbUnpack* cb, TMEvent* event, TMBank* cbbank)
   {
      if (!fCbFirstMidasTime[ibank])
      {
         fCbFirstMidasTime[ibank] = event->time_stamp;
         std::cout << "Syncronising chronobox id " << ibank << " first time stamp: ";
         std::cout <<  event->time_stamp << "\n";
      }

      if (fSyncToSequencer.at(ibank))
      {
         std::cout << "Syncronising chronobox id " << ibank << ":\t";
         std::cout << fSyncDiffToSequencer.at(ibank) << " - " << event->time_stamp << " = dt: ";
         std::cout << fSyncDiffToSequencer.at(ibank) - (int)event->time_stamp << "s\n";
         fSyncDiffToSequencer.at(ibank) -= (int)event->time_stamp;
         fSyncToSequencer.at(ibank) = false;
      }
      int nwords = cbbank->data_size/4; // byte to uint32_t words
      uint32_t* cbdata = (uint32_t*)event->GetBankData(cbbank);

      CbHits hits;
      CbScalers scalers;

      //if (ibank==0)
      //   cb->fVerbose = true;
      //else
      //   cb->fVerbose = false;
      //
      //printf("%s: %d words\n", fCbBanks[ibank].c_str(), nwords);

      cb->Unpack(cbdata, nwords, &hits, &scalers);

      uint32_t corrected_midas_time = event->time_stamp + fSyncDiffToSequencer[ibank];
      for (CbHit& hit: hits)
         hit.midastime = corrected_midas_time;

      //if (hits.size() > 0) {
      //   if (0) {
      //      printf("Data from %s: ", name);
      //      PrintCbHits(hits);
      //   }
      //}

      //if (ibank == 0) {
      //for (size_t i=0; i<hits.size(); i++) {
      //   printf("%s: hit %zu, time %.6f sec, %d+%d, channel %2d (%d)\n", cbbank->name.c_str(), i, hits[i].time, hits[i].timestamp, hits[i].epoch, hits[i].channel, (hits[i].flags&CB_HIT_FLAG_TE));
      //}
      //}

      fCheckHitsOk &= CheckHits(ibank, hits);

      for (size_t i=0; i<hits.size(); i++) {
         fCbHits[ibank][hits[i].channel].push_back(hits[i]);
      }

      //if (scalers.size() > 0) {
      //   if (print) {
      //      printf("Data from %s: ", name);
      //      PrintCbScalers(scalers);
      //   }
      //}
   }

   bool static comp(const CbHit& h1, const CbHit& h2)
   {
      return h1.time < h2.time;
   } 

   int period = 0;
   TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow)
   {
      //return flow;

      if (fTrace)
         printf("Analyze, run %d, event serno %d, id 0x%04x, data size %d\n", runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
      //event->FindAllBanks();
      //printf("Event: %s\n", event->HeaderToString().c_str());
      //printf("Banks: %s\n", event->BankListToString().c_str());

      //if (event->serial_number == 0) {
      //   printf("AAA\n");
      //   //fUnpackCb01.fEpochFromReset = true;
      //   exit(1);
      //}

      fRunEventCounter++;

      event->FindAllBanks();

      // Use the sequencer XML data event as trigger (and backbone) for syncing the 'system time'
      // of the chronoboxes (the MIDAS BANK timestamp)

      const TMBank* b = event->FindBank("SEQ2");
      if (b)
      {
         for (size_t i = 0; i < fSyncToSequencer.size(); i++)
         {
            fSyncToSequencer.at(i) = true;
            fSyncDiffToSequencer.at(i) = event->time_stamp;
         }
      }

      for (size_t ibank=0; ibank<fCbBanks.size(); ibank++) {
         TMBank* cbbank = event->FindBank(fCbBanks[ibank].c_str());
         if (cbbank)
         {
            AnalyzeCbFifo(ibank, fCbUnpack[ibank], event, cbbank);
         }
      }
      if (fCbHits.size() )
      {
         TCbFlow* cbflow = new TCbFlow(flow);

         for (size_t ibank=0; ibank<fCbBanks.size(); ibank++)
         {
            //std::cout << "Flushing " << fCbNames[ibank] << "\n";
            std::vector<CbHit>& flowhits =  cbflow->fCbHits[fCbNames[ibank]];
            for (size_t i=0; i<fCbHits[ibank].size(); i++)
            {
               const size_t n_chans = fCbHits[ibank][i].size();
               for (size_t j = 0; j < n_chans; j++)
               {
                  flowhits.push_back(fCbHits[ibank][i][j]);
               }
               // Sort events for flow (so counts are in time order)...
               // its a dirty fix to avoid re-writing the later parts of the 
               // analysis chain
               //std::sort(flowhits.begin(),flowhits.end(),comp);
               // Clear modules internal cache
               fCbHits[ibank][i].clear();            
            }
         }
         flow = cbflow;
      }
      else
      {
         *flags|=TAFlag_SKIP_PROFILE;
      }

      return flow;
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      TCbFlow* cbflow = flow->Find<TCbFlow>();
      if (!cbflow)
      {
         *flags|=TAFlag_SKIP_PROFILE;
         return flow;
      }
      TCbFIFOEventFlow* fifoflow = new TCbFIFOEventFlow(flow);
      for (const std::pair<const std::string,CbHits>& cbhits: cbflow->fCbHits)
      {
         fifoflow->fCbHits[cbhits.first] = BuildFIFOFlow(cbhits.first,cbhits.second,runinfo->fRunNo);
      }
      flow = fifoflow;

      return flow;
   }
};

class CbkoModuleFactory: public TAFactory
{
public:
   void Usage()
   {
      //      printf("\tCbModuleFactory Usage:\n");
      //printf("\t--print-cb-data : print chronobox raw data\n");
      //#ifdef HAVE_ROOT
      //printf("\t--dumpchronojson write out the chronobox channel list as json\n");
      //printf("\t--loadchronojson filename.json override odb channel list with json\n");
      //#endif
   }

   void Init(const std::vector<std::string> &/*args*/)
   {
      //printf("Init!\n");
      //printf("Arguments:\n");
      //for (unsigned i=0; i<args.size(); i++) {
      //   printf("arg[%d]: [%s]\n", i, args[i].c_str());
      //   if (args[i] == "--print-cb-data") {
      //      fFlags.fPrint = true;
      //   }
      //#ifdef HAVE_ROOT
      //   if (args[i] == "--dumpchronojson")
      //      fFlags.fDumpJsonChannelNames = true;
      //   if (args[i] == "--loadchronojson")
      //   {
      //      fFlags.fLoadJsonChannelNames = true;
      //      i++;
      //      fFlags.fLoadJsonFile=args[i];
      //   }
      //#endif
      //}
   }
   
   void Finish()
   {
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      return new CbkoModule(runinfo);
   }
};

static TARegister tar(new CbkoModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
