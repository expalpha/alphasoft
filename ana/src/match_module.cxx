#include "AgFlow.h"
#include "RecoFlow.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TMath.h"
#include "TSpectrum.h"
#include "TFitResult.h"
#include "Math/MinimizerOptions.h"

#include "SignalsType.hh"
#include <set>
#include <iostream>

#include "AnaSettings.hh"
#include "Match.hh"
#include "PadMerge.hh"

class MatchFlags
{
public:
   bool fRecOff = false; //Turn reconstruction off
   bool fTimeCut = false;
   bool fUseSpec = false;
   double start_time = -1.;
   double stop_time = -1.;
   bool fEventRangeCut = false;
   int start_event = -1;
   int stop_event = -1;
   AnaSettings* ana_settings=NULL;
   bool fDiag = false;
   bool fTrace = false;
   bool fForceReco = false;

   int ThreadID=-1;
   int TotalThreads=0;
   MatchFlags() // ctor
   { }

   ~MatchFlags() // dtor
   { }
};

class MatchModule: public TARunObject
{
public:
   MatchFlags* fFlags = NULL;
   bool fTrace = false;
   int fCounter = 0;
   bool diagnostic = false;
   

private:
   Match* match;

public:

   MatchModule(TARunInfo* runinfo, MatchFlags* f)
      : TARunObject(runinfo)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Match Module";
#endif
      if (fTrace)
         printf("MatchModule::ctor!\n");

      fFlags = f;

      //First thread
#ifdef HAVE_MANALYZER_PROFILER
      if (fFlags->ThreadID < 0)
         fModuleName="Match Module (CombPads)";
      //Multithreaded fitting
      else if ( fFlags->ThreadID < fFlags->TotalThreads ) 
         fModuleName="Match Module (Combine " +
            std::to_string(fFlags->ThreadID) + 
            "/" +
            std::to_string(fFlags->TotalThreads) +
            ")";
      else if (fFlags->TotalThreads==0 && fFlags->ThreadID==1)
         fModuleName="Match Module (spacepoints)";
#endif
      diagnostic=fFlags->fDiag; // dis/en-able histogramming
      fTrace=fFlags->fTrace; // enable verbosity
   }

   ~MatchModule()
   {
      if(fTrace)
         printf("MatchModule::dtor!\n");
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(fTrace)
         printf("MatchModule::BeginRun, run %d, file %s\n", 
                runinfo->fRunNo, runinfo->fFileName.c_str());
      fCounter = 0;
      bool MTing = runinfo->fMtInfo;
      if(fTrace)
         printf("MatchModule::BeginRun Are we MTing? %d\n",MTing);
                
      match=new Match(fFlags->ana_settings, MTing);
      //Global lock from manalzer (needed if your using roots basic fitting methods)
      match->SetGlobalLockVariable(&TAMultithreadHelper::gfLock);
      match->SetTrace(fTrace);
      match->SetDiagnostic(diagnostic);
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      // match->Setup(runinfo->fRoot->fOutputFile);
      // match->Init();
   }
   void EndRun(TARunInfo* runinfo)
   {
      if(fTrace)
         printf("MatchModule::EndRun, run %d    Total Counter %d\n", runinfo->fRunNo, fCounter);
      delete match;
   }

   void PauseRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      if (fTrace)
         printf("ResumeRun, run %d\n", runinfo->fRunNo);
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      if(fTrace)
         printf("MatchModule::Analyze, run %d, counter %d\n",
                runinfo->fRunNo, fCounter++);

      // turn off recostruction
      if (fFlags->fRecOff)
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      

      const AgEventFlow* ef = flow->Find<AgEventFlow>();

      if (!ef || !ef->fEvent)
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      
      if (fFlags->fTimeCut)
         {
            if (ef->fEvent->time<fFlags->start_time)
               {
#ifdef HAVE_MANALYZER_PROFILER
                  *flags|=TAFlag_SKIP_PROFILE;
#endif
                  return flow;
               }
            if (ef->fEvent->time>fFlags->stop_time)
               {
#ifdef HAVE_MANALYZER_PROFILER
                  *flags|=TAFlag_SKIP_PROFILE;
#endif
                  return flow;
               }
         }

      if (fFlags->fEventRangeCut)
         {
            if (ef->fEvent->counter<fFlags->start_event)
               {
#ifdef HAVE_MANALYZER_PROFILER
                  *flags|=TAFlag_SKIP_PROFILE;
#endif
                  return flow;
               }
            if (ef->fEvent->counter>fFlags->stop_event)
               {
#ifdef HAVE_MANALYZER_PROFILER
                  *flags|=TAFlag_SKIP_PROFILE;
#endif
                  return flow;
               }
         }

      AgSignalsFlow* SigFlow = flow->Find<AgSignalsFlow>();
      if( !SigFlow ) 
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( SigFlow->bad || SigFlow->awSig.empty() )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      if( fTrace )
         {
            printf("MatchModule::Analyze, AW # signals %d\n", int(SigFlow->awSig.size()));
            printf("MatchModule::Analyze, PAD # signals %d\n", int(SigFlow->pdSig.size()));
         }  
     
      // allow events without pwbs
      std::vector< std::pair<ALPHAg::TWireSignal,ALPHAg::TPadSignal> > spacepoints;
      if( SigFlow->combinedPads.size() )
         {
            if( fTrace )
               printf("PadMergeModule::Analyze, combined pads # %d\n", int(SigFlow->combinedPads.size()));
            SigFlow->DeletePadSignals(); //Replace pad signals with combined ones
            SigFlow->AddPadSignals( SigFlow->combinedPads );
         }
      if( SigFlow->pdSig.size() )
         {
            if( fTrace )
               printf("MatchModule::Analyze, combined pads # %d\n", int(SigFlow->pdSig.size()));
            spacepoints =
               match->MatchElectrodes( SigFlow->awSig,SigFlow->pdSig );
            spacepoints = match->CombPoints(spacepoints);
         }
      else if( fFlags->fForceReco ) // <-- this probably goes before, where there are no pad signals -- AC 2019-6-3
         {
            if( fTrace )
               printf("MatchModule::Analyze, NO combined pads, Set Z=0\n");
            //delete match->GetCombinedPads();?
            spacepoints = match->FakePads( SigFlow->awSig );
         }

      if( spacepoints.size() )
         {
            if(fFlags->fTrace)
               printf("MatchModule::Analyze, Spacepoints # %d\n", int(spacepoints.size()));
            if( spacepoints.size() > 0 )
               SigFlow->AddMatchSignals( spacepoints );
         }
      else
            printf("MatchModule::Analyze Spacepoints should exists at this point\n");

      return flow;
   }
};


class MatchModuleFactory: public TAFactory
{
public:
   MatchFlags fFlags;

public:
   void Help()
   {
      printf("MatchModuleFactory::Help\n");
      printf("\t--forcereco\t\tEnable reconstruction when no pads are associated with the event by setting z=0\n");
   }
   void Usage()
   {
      Help();
   }

   void Init(const std::vector<std::string> &args)
   {
      TString json="default";
      //printf("MatchModuleFactory::Init!\n");
      for(unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--usetimerange" )
               {
                  fFlags.fTimeCut=true;
                  i++;
                  fFlags.start_time=atof(args[i].c_str());
                  i++;
                  fFlags.stop_time=atof(args[i].c_str());
                  printf("Using time range for reconstruction: ");
                  printf("%f - %fs\n",fFlags.start_time,fFlags.stop_time);
               }
            if( args[i] == "--useeventrange" )
               {
                  fFlags.fEventRangeCut=true;
                  i++;
                  fFlags.start_event=atoi(args[i].c_str());
                  i++;
                  fFlags.stop_event=atoi(args[i].c_str());
                  printf("Using event range for reconstruction: ");
                  printf("Analyse from (and including) %d to %d\n",fFlags.start_event,fFlags.stop_event);
               }
            if (args[i] == "--recoff")
               fFlags.fRecOff = true;
            if( args[i] == "--diag" )
               fFlags.fDiag = true;
            if( args[i] == "--trace" )
               fFlags.fTrace = true;
            if(args[i] == "--forcereco")
               fFlags.fForceReco=true;
            if (args[i] == "--anasettings")
               json = args[++i];
         }
      fFlags.ana_settings = new AnaSettings(json,args);
      if(fFlags.fTrace) fFlags.ana_settings->Print();
   }
   
   MatchModuleFactory() {}

   void Finish()
   {
      if(fFlags.fTrace == true)
         printf("MatchModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      if(fFlags.fTrace == true)
         printf("MatchModuleFactory::NewRunObject, run %d, file %s\n", 
                runinfo->fRunNo, runinfo->fFileName.c_str());
      return new MatchModule(runinfo, &fFlags);
   }
};

static TARegister tar(new MatchModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
