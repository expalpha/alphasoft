//
// saves the spacepoints to an h5 file.
//
// Gareth Smith April 2023
// ChatGPT3.5 and Simon https://stackoverflow.com/questions/15379399/ were very helpful
//

#include <stdio.h>

#include "manalyzer.h"
#include "midasio.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include "RecoFlow.h"
#include "AgFlow.h"
#include "hdf5.h"
#include "TAGDetectorEvent.hh"
#include "TPCconstants.hh"
#include "TAGSpill.h"

typedef struct {
    int fEvent;
    double fx;
    double fy;
    double fz;
    double ferrx;
    double ferry;
    double ferrz;
    double fwireamp;
    double fpadamp;
    double ftime;
} h5sp;

typedef struct {
    int fEvent;
    int nSp;
    double x;
    double y;
    double z;
    double simX;
    double simY;
    double simZ;
} h5evt;


class TSPDumperFlags
{
    public:
        // std::string fFileName;
        // std::string fFileName2;
        // std::string fTreeName;
        bool fTrace;
        std::string fSaveFile;
        std::string fSaveFile2;
        std::vector<int> fEventCut;
        bool fTimeCut;
        double start_time;
        double stop_time;

        TSPDumperFlags(): fTrace(false),
                          fSaveFile(" "), fSaveFile2(" "),
                          fTimeCut(false), start_time(0.), stop_time(-1.)
    {}
};

class TSPDumper: public TARunObject
{
public:

    //Flags & counters.
    TSPDumperFlags* fFlags;
    hid_t file_id, datatype_id, dataset_id, dataspace_id, filespace_id, memspace_id;
    hid_t file_id2, datatype_id2, dataset_id2, dataspace_id2, filespace_id2, memspace_id2;
    hsize_t event_no, max_sp;

    TSPDumper(TARunInfo* runInfo, TSPDumperFlags* flags) : TARunObject(runInfo)
    {
        fFlags = flags;
        fModuleName="sp_dump_module";
        printf("%s ctor, run %d, file %s\n", fModuleName.c_str(), runInfo->fRunNo, runInfo->fFileName.c_str());        
    }

    ~TSPDumper()
    {
        //printf("dtor!\n");
    }
    
    void BeginRun(TARunInfo* runInfo)
    {
        printf("BeginRun, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
        std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
        event_no = 0;
        max_sp = 0;

        // Create a new H5 file
        std::string fileName = getenv("AGRELEASE");
        fileName +=  + "/spacepoints";
        fileName += std::to_string(runInfo->fRunNo);
        fileName += ".h5";
        if (fFlags->fSaveFile!=" ") fileName = fFlags->fSaveFile;
        printf("Saving h5 spacepoint file at %s\n",fileName.c_str());
        file_id = H5Fcreate(fileName.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        std::string fileName2 = getenv("AGRELEASE");
        fileName2 +=  + "/vertices";
        fileName2 += std::to_string(runInfo->fRunNo);
        fileName2 += ".h5";
        if (fFlags->fSaveFile2!=" ") fileName2 = fFlags->fSaveFile2;
        printf("Saving h5 vertices file at %s\n",fileName2.c_str());
        file_id2 = H5Fcreate(fileName2.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        // Create a dataspace for the dataset
        hsize_t max_dims[2] = {H5S_UNLIMITED, H5S_UNLIMITED};
        hsize_t init_dims[2] = {0, 0};
        dataspace_id = H5Screate_simple(2, init_dims, max_dims);

        hsize_t max_dims2[1] = {H5S_UNLIMITED};
        hsize_t init_dims2[1] = {0};
        dataspace_id2 = H5Screate_simple(1,init_dims2,max_dims2);

        // Set up the dataset creation properties
        hid_t prop_id = H5Pcreate(H5P_DATASET_CREATE);
        H5Pset_layout(prop_id, H5D_CHUNKED);
        hsize_t chunk_dims[2] = { 1000, 1000 };  // chunk size of 1000 objects
        H5Pset_chunk(prop_id, 2, chunk_dims);

        hid_t prop_id2 = H5Pcreate(H5P_DATASET_CREATE);
        H5Pset_layout(prop_id2, H5D_CHUNKED);
        hsize_t chunk_dims2[1] = { 1000 };  // chunk size of 1000 objects
        H5Pset_chunk(prop_id2, 1, chunk_dims2);

        // Create a compound datatype for the h5sp struct
        datatype_id = H5Tcreate(H5T_COMPOUND, sizeof(h5sp));
        H5Tinsert(datatype_id, "fEvent", HOFFSET(h5sp, fEvent), H5T_NATIVE_INT);
        H5Tinsert(datatype_id, "fx", HOFFSET(h5sp, fx), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id, "fy", HOFFSET(h5sp, fy), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id, "fz", HOFFSET(h5sp, fz), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id, "ferrx", HOFFSET(h5sp, ferrx), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id, "ferry", HOFFSET(h5sp, ferry), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id, "ferrz", HOFFSET(h5sp, ferrz), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id, "fwireamp", HOFFSET(h5sp, fwireamp), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id, "fpadamp", HOFFSET(h5sp, fpadamp), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id, "ftime", HOFFSET(h5sp, ftime), H5T_NATIVE_DOUBLE);

        datatype_id2 = H5Tcreate(H5T_COMPOUND, sizeof(h5evt));
        H5Tinsert(datatype_id2, "fEvent", HOFFSET(h5evt, fEvent), H5T_NATIVE_INT);
        H5Tinsert(datatype_id2, "nSp", HOFFSET(h5evt, nSp), H5T_NATIVE_INT);
        H5Tinsert(datatype_id2, "x", HOFFSET(h5evt, x), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id2, "y", HOFFSET(h5evt, y), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id2, "z", HOFFSET(h5evt, z), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id2, "simX", HOFFSET(h5evt, simX), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id2, "simY", HOFFSET(h5evt, simY), H5T_NATIVE_DOUBLE);
        H5Tinsert(datatype_id2, "simZ", HOFFSET(h5evt, simZ), H5T_NATIVE_DOUBLE);

        // Create a dataset in the file
        dataset_id = H5Dcreate2(file_id, "/data", datatype_id, dataspace_id,H5P_DEFAULT, prop_id, H5P_DEFAULT);
        dataset_id2 = H5Dcreate2(file_id2, "/data", datatype_id2, dataspace_id2,H5P_DEFAULT, prop_id2, H5P_DEFAULT);

        H5Pclose(prop_id);
        H5Sclose(dataspace_id);
        H5Pclose(prop_id2);
        H5Sclose(dataspace_id2);

    }

    void EndRun(TARunInfo* runInfo)
    {
        printf("EndRun, run %d\n", runInfo->fRunNo);
        std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);

        // Closes the HDF5 file
        H5Dclose(dataset_id);
        H5Tclose(datatype_id);
        H5Dclose(dataset_id2);
        H5Tclose(datatype_id2);
        H5Fclose(file_id);
        H5Fclose(file_id2);
    }

    TAFlowEvent* AnalyzeFlowEvent(__attribute__((unused)) TARunInfo* runInfo, TAFlags* flags, TAFlowEvent* flow)
    {
      AgEventFlow *ef = flow->Find<AgEventFlow>();
      if (!ef || !ef->fEvent)
      {
         *flags|=TAFlag_SKIP_PROFILE;
         return flow;
      }
      AgEvent* age = ef->fEvent;
      if( fFlags->fTrace ) age->Print();

      AgSignalsFlow* SigFlow = flow->Find<AgSignalsFlow>();
      if( !SigFlow ) 
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }

      std::vector<TSpacePoint> sps = SigFlow->fSpacePoints;
      if( sps.size()==0 ) 
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      if( fFlags->fTrace ) printf("Will dump %ld spacepoints\n",sps.size());

      AgAnalysisFlow* analysis_flow = flow->Find<AgAnalysisFlow>();
      if( !analysis_flow || !analysis_flow->fEvent )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      if( fFlags->fTrace ) analysis_flow->fEvent->Print();

        AgBarEventFlow *bef = flow->Find<AgBarEventFlow>();
      if( !bef || !bef->BarEvent )
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
        if( fFlags->fTrace ) bef->BarEvent->Print();

        TBarEvent *barEvt = bef->BarEvent;
        TStoreEvent* stEvt = analysis_flow->fEvent;
        TPCSimEvent* simEvt = age->sim;

        double evt_time = stEvt->GetTimeOfEvent();

        if (fFlags->fTimeCut) {
            if (evt_time<fFlags->start_time or evt_time>fFlags->stop_time)
                {
#ifdef HAVE_MANALYZER_PROFILER
                    *flags|=TAFlag_SKIP_PROFILE;
#endif
                    return flow;
                }
        }
        if( fFlags->fTrace ) printf("Time of the event %1.2lf\n",evt_time);

        std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
        
        hsize_t i = event_no;
        hsize_t n_sp = sps.size();
        if (n_sp > max_sp) max_sp=n_sp;

        // Check if the TAGDetectorEvent passes cut 0 and any cuts supplied with --cut
        TAGDetectorEvent detEvt(stEvt,barEvt);
        //if (!detEvt.GetOnlinePassCuts(0)) return flow; // disabled - include events where the helix fit method fails
        for (int c: fFlags->fEventCut) {
            if (!detEvt.GetOnlinePassCuts(c)) return flow;
        }

        // Extend the dataset
        hsize_t new_dims[2] = {i+1, max_sp};
        H5Dset_extent(dataset_id, new_dims);

        hsize_t new_dims2[1] = {i+1};
        H5Dset_extent(dataset_id2, new_dims2);


        // Loop over every spacepoint in the event
        for (hsize_t j=0;j<n_sp;j++) {

            TSpacePoint &spacepoint = sps.at(j);

            hsize_t added_size[2] = {1,1};
            memspace_id = H5Screate_simple(2, added_size, NULL);

            // Get the dataspace of the dataset and select the new hyperslab
            filespace_id = H5Dget_space(dataset_id);
            hsize_t start[2] = {i, j};
            hsize_t count[2] = {1, 1};
            H5Sselect_hyperslab(filespace_id, H5S_SELECT_SET, start, NULL, count, NULL);

            // Put all the needed data into a struct
            h5sp sp = GetH5SP(spacepoint,ef->fEvent->counter);

            // Write the new data entry to the hyperslab
            H5Dwrite(dataset_id, datatype_id, memspace_id, filespace_id, H5P_DEFAULT, &sp);

            // Close the dataspace
            H5Sclose(memspace_id);
            H5Sclose(filespace_id);
        }

        hsize_t added_size2[1] = {1};
        memspace_id2 = H5Screate_simple(1, added_size2, NULL);
        // Get the dataspace of the dataset and select the new hyperslab
        filespace_id2 = H5Dget_space(dataset_id2);
        hsize_t start2[1] = {i};
        hsize_t count2[1] = {1};
        H5Sselect_hyperslab(filespace_id2, H5S_SELECT_SET, start2, NULL, count2, NULL);

        // Put all the needed data into a struct
        h5evt evt = GetH5Evt(stEvt, simEvt);

        // Write the new data entry to the hyperslab
        H5Dwrite(dataset_id2, datatype_id2, memspace_id2, filespace_id2, H5P_DEFAULT, &evt);
        if( fFlags->fTrace ) printf("Spacepoint dumped\n");

        // Close the dataspace
        H5Sclose(memspace_id2);
        H5Sclose(filespace_id2);

        event_no++;
        return flow;

    }

    void AnalyzeSpecialEvent(TARunInfo* runInfo, TMEvent* event)
    {
        if(fFlags->fTrace)
            printf("agdump::AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", runInfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
    }

    h5sp GetH5SP(const TSpacePoint p, int evtNo) {
        h5sp sp;
        sp.fEvent = evtNo;
        sp.fx = p.GetX();
        sp.fy = p.GetY();
        sp.fz = p.GetZ();
        sp.ferrx = p.GetErrX();
        sp.ferry = p.GetErrY();
        sp.ferrz = p.GetErrZ();
        sp.fwireamp = p.GetWireHeight();
        sp.fpadamp = p.GetPadHeight();
        sp.ftime = p.GetTime();
        return sp;
    }

    h5evt GetH5Evt(const TStoreEvent* e, const TPCSimEvent* simEvt) {
        h5evt evt;
        evt.fEvent = e->GetEventNumber();
        evt.nSp = e->GetNumberOfPoints();
        if (e->GetVertexStatus()>0) {
            evt.x = e->GetVertexX();
            evt.y = e->GetVertexY();
            evt.z = e->GetVertexZ();
        }
        else {
            evt.x = evt.y = evt.z = ALPHAg::kUnknown;
        }
        if (simEvt) {
            evt.simX = simEvt->GetVertexX();
            evt.simY = simEvt->GetVertexY();
            evt.simZ = simEvt->GetVertexZ();
        }
        else {
            evt.simX = 0.;
            evt.simY = 0.;
            evt.simZ = 0.;
        }
        return evt;
    }
};

class TSPDumperFactory: public TAFactory
{
public:
    TSPDumperFlags fFlags;
   void Usage()
   {
      printf("TSPDumperFactory::Help!\n");
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("Init!\n");
      fFlags.fSaveFile = " ";
      fFlags.fSaveFile2 = " ";
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose")
               fFlags.fTrace = true;
            if( args[i] == "--savefile" )
               fFlags.fSaveFile = args[++i];
            if( args[i] == "--savefile2" )
               fFlags.fSaveFile2 = args[++i];
            if( args[i] == "--cut" )
               fFlags.fEventCut.push_back(atoi(args[++i].c_str()));
            if( args[i] == "--usetimerange" )
                {
                fFlags.fTimeCut=true;
                i++;
                fFlags.start_time=atof(args[i].c_str());
                i++;
                fFlags.stop_time=atof(args[i].c_str());
                printf("Using time range for spacepoint dumper: ");
                printf("%f - %fs\n",fFlags.start_time,fFlags.stop_time);
                }


         }
   }
   
   void Finish()
   {
      printf("Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runInfo)
   {
      printf("NewRunObject, run %d, file %s\n", runInfo->fRunNo, runInfo->fFileName.c_str());
      return new TSPDumper(runInfo, &fFlags);
   }
};

static TARegister tar(new TSPDumperFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
