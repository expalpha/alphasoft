/* Look at documentation.pdf for a detailed explanation of the data collected by
 * the BV.
 */

#ifndef BVEVENT_HPP
#define BVEVENT_HPP

#include"ADCWaveform.hpp"
#include"TDCTime.hpp"
#include<vector>

namespace BV {
	class Event {
		private:
			std::vector<DAQ::ADCWaveform> adc_waveforms;
			std::vector<DAQ::TDCTime> tdc_times;
		public:
			Event(const std::vector<DAQ::ADCWaveform>& wfs, const std::vector<DAQ::TDCTime>& ts);

			const std::vector<DAQ::ADCWaveform>& getADCWaveforms() const;
			const std::vector<DAQ::TDCTime>& getTDCTimes() const;

			bool isEmpty() const;
	};
}

#endif
