/* Look at documentation.pdf for a detailed explanation of the MIDAS File
 * structure.
 */

#ifndef MFILE_HPP
#define MFILE_HPP

#include<string>
#include<iostream>
#include<vector>
#include<fstream>
#include<nlohmann/json.hpp>
#include"ADCWaveform.hpp"
#include"TDCTime.hpp"
#include"BVEvent.hpp"

namespace MIDAS {
	std::size_t sizeOfType(unsigned int type);
	std::string nameOfType(unsigned int type);

	class BankHeader {
		private:
			std::string bank_name;
			unsigned int data_type;
			unsigned int num_items;//This is the actual number of items of type 'data_type'
			std::streampos data_pos;//Position in the file where this data stream begins
		public:
			BankHeader(std::string name, unsigned int type, 
					unsigned int number_items, std::streampos start_position);

			std::string getBankName() const;
			unsigned int getDataType() const;
			unsigned int getNumItems() const;
			std::streampos getDataPos() const;

			bool isBankNameCompatible(std::string pattern) const;
	};

	class Event {
		private:
			unsigned short int event_id;
			unsigned short int trigger_mask;
			unsigned int serial_number;
			unsigned int time_stamp;
			unsigned int flags;
			std::vector<BankHeader> bank_headers;
		public:
			Event(unsigned short int id, unsigned short int mask, 
					unsigned int serial, unsigned int time, unsigned int flag);

			void addBankHeader(const BankHeader& bank_header);
			void addBankHeader(std::string name, unsigned int type, 
					unsigned int number_items, std::streampos start_position);

			unsigned short int getEventId() const;
			unsigned short int getTriggerMask() const;
			unsigned int getSerialNumber() const;
			unsigned int getTimeStamp() const;
			unsigned int getFlags() const;
			const std::vector<BankHeader>& getBankHeaders() const;
	};

	class File {
		private:
			std::string file_name;
			std::vector<Event> events;

			//Guarantee that the BankHeader/Event corresponds to this file by having
			//the unpacking private
			BV::Event unpackBVEvent(const MIDAS::Event& event) const;
			DAQ::ADCWaveform unpackADCWaveform(const MIDAS::BankHeader& header) const;
			std::vector<DAQ::TDCTime> unpackTDCTimes(const MIDAS::BankHeader& header) const;
		public:
			File(std::string a_file);

			std::string getFileName() const;
			const std::vector<Event>& getEvents() const;
			unsigned int getRunNumber() const;
			nlohmann::json getFromInitialODB(const nlohmann::json::json_pointer& ptr) const;
			nlohmann::json getFromFinalODB(const nlohmann::json::json_pointer& ptr) const;
			nlohmann::json getFromODB(const nlohmann::json::json_pointer& ptr) const;

			std::vector<BV::Event> getBVEvents() const;
	};
}

template<class T> std::vector<T> getFromBin(std::ifstream& ifs, int num);

template<class T> std::vector<T> getFromBin(std::ifstream& ifs, int num) {
	std::vector<T> output;
	output.resize(num);

	ifs.read(reinterpret_cast<char*>(output.data()), sizeof(T) * num);

	return output;
}
#endif
