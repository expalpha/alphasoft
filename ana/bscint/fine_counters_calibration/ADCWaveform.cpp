#include "ADCWaveform.hpp"

DAQ::ADCWaveform::ADCWaveform(unsigned char board_num, unsigned char chan_num,
		const std::vector<short int>& data, double freq) : 
	board(board_num), channel(chan_num), samples(data), sample_freq(freq) {

}

unsigned char DAQ::ADCWaveform::getBoard() const {
	return board;
}

unsigned char DAQ::ADCWaveform::getChannel() const {
	return channel;
}

const std::vector<short int>& DAQ::ADCWaveform::getSamples() const {
	return samples;
}

double DAQ::ADCWaveform::getSampleFreq() const {
	return sample_freq;
}
