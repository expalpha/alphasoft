#include<iostream>
#include<string>
#include<array>
#include"MFile.hpp"
#include<nlohmann/json.hpp>
#include"DAQConstants.hpp"


int main(int argc, char* argv[]) {
	constexpr unsigned int num_channels{ 256 };
	//There are a total of 256 TDC channels.
	//Each channel has a 10 bits fine counter i.e. 1024 possible values [0-1023]
	//fine_counters[i] is the ith channel
	constexpr unsigned int max_counter{ 1024 };
	std::array<std::array<unsigned int, max_counter>, num_channels> fine_counters{};

	for(int i = 1; i < argc; ++i) {
		std::string file_name{ argv[i] };
		std::cout << "Reading " << file_name << '\n';
		MIDAS::File file(file_name);

		std::vector<BV::Event> bv_events(file.getBVEvents());
		for(const BV::Event& event : bv_events) {
			for(const DAQ::TDCTime& time : event.getTDCTimes()) {
				unsigned short int fpga{ time.getFPGA() };
				unsigned char channel{ time.getChannel() };

				if(fpga > 3) {
					std::cerr << "Error: FPGA index larger than 3.\n";
					return 1;
				}
				if(channel > 64) {
					std::cerr << "Error: TDC channel larger than 64.\n";
					return 1;
				}
				//Channel 0 is not a BV channel
				if(channel != 0) {
					unsigned short int fine{ time.getFine() };
					std::size_t index = static_cast<std::size_t>(64*fpga + channel - 1);

					fine_counters[index][fine] += 1;
				}
			}
		}
	}

	std::array<std::array<double, max_counter>, num_channels> look_up_table{};
	for(unsigned int i = 0; i < num_channels; ++i) {
		unsigned int total_counts{ 0 };
		//Ignore the 1023 counts; these are bad hits that should be ignored.
		for(unsigned int j = 0; j < max_counter - 1; ++j) {
			total_counts += fine_counters[i][j];
		}
		if(total_counts == 0) {
			continue;
		}

		double current_time{ 0 };
		for(unsigned int j = 0; j < max_counter; ++j) {
			double percentage{ static_cast<double>(fine_counters[i][j]) / total_counts };
			double bin_size{ DAQ::coarse_t * percentage };
			current_time += bin_size;
			look_up_table[i][j] = current_time - 0.5 * bin_size;
		}
	}

	const std::string json_filename{ "fine_counters.json" };
	std::ofstream json_file;
	json_file.open(json_filename);
	if(json_file.is_open()) {
		json_file.precision(std::numeric_limits<double>::digits10);
		nlohmann::json json_table(look_up_table);
		json_file << json_table.dump(4);
	} else {
		std::cerr << "Error: Unable to open file " << json_filename << '\n';
		return 1;
	}
	
	return 0;
}
