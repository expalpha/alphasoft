/* The ADCWaveform object represents the digitized pulse at an ADC channel. Find
 * more information in the documentation.pdf
 */

#ifndef ADCWAVEFORM_HPP
#define ADCWAVEFORM_HPP

#include<vector>

namespace DAQ {
	class ADCWaveform {
		private:
			unsigned char board;
			unsigned char channel;
			std::vector<short int> samples;
			double sample_freq;
		public:
			ADCWaveform(unsigned char board_num, unsigned char chan_num, 
					const std::vector<short int>& data, double freq);

			unsigned char getBoard() const;
			unsigned char getChannel() const;
			const std::vector<short int>& getSamples() const;
			double getSampleFreq() const;
	};
}

#endif
