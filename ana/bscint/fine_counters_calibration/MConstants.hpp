/* The values of these constants are described throughout the documentation
 */
#ifndef MCONSTANTS_HPP
#define MCONSTANTS_HPP

namespace MIDAS {
	constexpr short unsigned int bor_id {32768}; //Begin Of Run id
	constexpr short unsigned int eor_id {32769}; //End Of Run id
	constexpr short unsigned int se_mask {18765}; //Special Event mask (BOR and EOR)
	constexpr unsigned int bank32 {17}; //Flags indicating 32-bit banks
	constexpr unsigned int bank32_64a {49}; //Flags indicating 32-bit bank 64-bit aligned
	constexpr unsigned int bank64_res {4}; //Amount of bytes reserved in 32-bit 64 aligned banks
	constexpr unsigned int d_align {8}; //Data banks aligned to multiples of 8

	enum : unsigned int { //Type field in Data Banks
		U_INT8 = 1,
		CHAR,
		U_CHAR,
		U_SHORT_INT,
		SHORT_INT,
		U_INT,
		INT,
		BOOL,
		FLOAT,
		DOUBLE,
		STRUCT = 14
	};
}

#endif
