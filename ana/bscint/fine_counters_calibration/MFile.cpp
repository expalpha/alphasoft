#include"MFile.hpp"
#include<stdexcept>
#include"MConstants.hpp"
#include"DAQConstants.hpp"
#include<bit>

std::size_t MIDAS::sizeOfType(unsigned int type) {
	switch(type) {
		case MIDAS::U_INT8:
			return sizeof(unsigned char);
		case MIDAS::CHAR:
			return sizeof(char);
		case MIDAS::U_CHAR:
			return sizeof(unsigned char);
		case MIDAS::U_SHORT_INT:
			return sizeof(unsigned short int);
		case MIDAS::SHORT_INT:
			return sizeof(short int);
		case MIDAS::U_INT:
			return sizeof(unsigned int);
		case MIDAS::INT:
			return sizeof(int);
		case MIDAS::BOOL:
			return sizeof(bool);
		case MIDAS::FLOAT:
			return sizeof(float);
		case MIDAS::DOUBLE:
			return sizeof(double);
		case MIDAS::STRUCT://Known structure can be of any size
			return 1;
		default:
			throw std::runtime_error("Unknown MIDAS data type " + std::to_string(type));
	}
}
std::string MIDAS::nameOfType(unsigned int type) {
	switch(type) {
		case MIDAS::U_INT8:
			return "unsigned char";
		case MIDAS::CHAR:
			return "char";
		case MIDAS::U_CHAR:
			return "unsigned char";
		case MIDAS::U_SHORT_INT:
			return "unsigned short int";
		case MIDAS::SHORT_INT:
			return "short int";
		case MIDAS::U_INT:
			return "unsigned int";
		case MIDAS::INT:
			return "int";
		case MIDAS::BOOL:
			return "bool";
		case MIDAS::FLOAT:
			return "float";
		case MIDAS::DOUBLE:
			return "double";
		case MIDAS::STRUCT://Known structure can be of any size
			return "bytes for structure";
		default:
			throw std::runtime_error("Unknown MIDAS data type " + std::to_string(type));
	}
}

//--------------- MIDAS::BankHeader -------------------------

MIDAS::BankHeader::BankHeader(std::string name, unsigned int type,
		unsigned int number_items, std::streampos start_position) :
	bank_name(name), data_type(type), num_items(number_items), data_pos(start_position) {

}

std::string MIDAS::BankHeader::getBankName() const {
	return bank_name;
}
unsigned int MIDAS::BankHeader::getDataType() const {
	return data_type;
}
unsigned int MIDAS::BankHeader::getNumItems() const {
	return num_items;
}
std::streampos MIDAS::BankHeader::getDataPos() const {
	return data_pos;
}

bool MIDAS::BankHeader::isBankNameCompatible(std::string pattern) const {
	if(bank_name.size() != pattern.size()) {
		return false;
	}
	for(unsigned int i = 0; i < pattern.size(); ++i) {
		if(pattern[i] == '?') {
			continue;
		} else if(pattern[i] != bank_name[i]) {
			return false;
		}
	}
	return true;
}

//---------------- MIDAS::Event -----------------------------

MIDAS::Event::Event(unsigned short int id, unsigned short int mask,
		unsigned int serial, unsigned int time, unsigned int flag) :
	event_id(id), trigger_mask(mask), serial_number(serial), time_stamp(time),
	flags(flag) {

}

void MIDAS::Event::addBankHeader(const MIDAS::BankHeader& bank_header) {
	bank_headers.push_back(bank_header);
}
void MIDAS::Event::addBankHeader(std::string name, unsigned int type,
		unsigned int number_items, std::streampos start_position) {
	bank_headers.emplace_back(name, type, number_items, start_position);
}

unsigned short int MIDAS::Event::getEventId() const {
	return event_id;
}
unsigned short int MIDAS::Event::getTriggerMask() const {
	return trigger_mask;
}
unsigned int MIDAS::Event::getSerialNumber() const {
	return serial_number;
}
unsigned int MIDAS::Event::getTimeStamp() const {
	return time_stamp;
}
unsigned int MIDAS::Event::getFlags() const {
	return flags;
}
const std::vector<MIDAS::BankHeader>& MIDAS::Event::getBankHeaders() const {
	return bank_headers;
}

//--------------- MIDAS::File ---------------------------------

MIDAS::File::File(std::string a_file) : file_name(a_file) {
	std::ifstream ifs(file_name, std::ios::binary | std::ios::ate);
	if(ifs.is_open()) {
		std::streampos total_bytes(ifs.tellg());
		ifs.seekg(0, std::ios::beg);
		//Read one event at a time
		while(ifs.tellg() != total_bytes) {
			std::vector<unsigned short int> h(getFromBin<unsigned short int>(ifs, 2));
			unsigned short int event_id(h[0]);
			unsigned short int trigger_mask(h[1]);
			if(event_id != MIDAS::bor_id && event_id != MIDAS::eor_id) {
				std::vector<unsigned int> eader(getFromBin<unsigned int>(ifs, 5));
				unsigned int serial_number(eader[0]);
				unsigned int timestamp(eader[1]);
				unsigned int event_size(eader[2]);
				unsigned int banks_size(eader[3]);
				unsigned int flags(eader[4]);
				if(banks_size != event_size - 8) {
					throw std::runtime_error("File::File Incongruence between 'Event' and 'All Banks' size in " + file_name);
				}
				if(flags != MIDAS::bank32 && flags != MIDAS::bank32_64a) {
					throw std::runtime_error("File::File Unknown flag (bank type) " + std::to_string(flags) + " in " + file_name);
				}
				events.emplace_back(event_id, trigger_mask, serial_number, timestamp, flags);
				//Now go through all the banks in this event one by one
				std::streampos read(0);
				while(read < banks_size) {
					std::vector<char> name(getFromBin<char>(ifs,4));
					std::string bank_name(name.begin(), name.end());
					read += 4;

					std::vector<unsigned int> body(getFromBin<unsigned int>(ifs,2));
					unsigned int type(body[0]);
					unsigned int bank_size(body[1]);
					read += 8;
					//If bank is 64-bit aligned, jump whatever amount is
					//reserved
					if(flags == MIDAS::bank32_64a) {
						ifs.seekg(MIDAS::bank64_res, std::ios::cur);
						read += MIDAS::bank64_res;
					}
					if(bank_size % MIDAS::sizeOfType(type) != 0) {
						throw std::runtime_error("File::File 'Bank Size' error in " + file_name);
					}
					unsigned int num_items(bank_size / MIDAS::sizeOfType(type));
					events.back().addBankHeader(bank_name, type, num_items, ifs.tellg());

					ifs.seekg(bank_size, std::ios::cur);
					read += bank_size;
					//There might still be some trash before the next bank.
					//Remember that the bank_size + trash needs to be a multiple
					//of MIDAS::d_align.
					unsigned int mismatch(bank_size % MIDAS::d_align);
					if(mismatch != 0) {
						unsigned int remaining(MIDAS::d_align - mismatch);
						ifs.seekg(remaining, std::ios::cur);
						read += remaining;
					}
				}
				if(read != banks_size) {
					throw std::runtime_error("File::File 'All Banks Size' error in " + file_name);
				}
			} else {
				if(trigger_mask != MIDAS::se_mask) {
					throw std::runtime_error("File::File BOR or EOR with wrong trigger mask in " + file_name);
				}
				std::vector<unsigned int> eader(getFromBin<unsigned int>(ifs, 3));
				unsigned int serial_number(eader[0]);
				unsigned int timestamp(eader[1]);
				unsigned int event_size(eader[2]);

				if(event_id == MIDAS::bor_id) {
					if(!events.empty()) {
						throw std::runtime_error("File::File BOR event out of place in " + file_name);
					}
					events.emplace_back(event_id, trigger_mask, serial_number, timestamp, 0);
					events.back().addBankHeader("ODBI", MIDAS::CHAR, event_size, ifs.tellg());
				} else if(event_id == MIDAS::eor_id) {
					if(total_bytes - ifs.tellg() != event_size) {
						throw std::runtime_error("File::File EOR event out of place in " + file_name);
					}
					if(events[0].getSerialNumber() != serial_number) {
						throw std::runtime_error("File::File Run number mismatch BOR and EOR in " + file_name);
					}
					events.emplace_back(event_id, trigger_mask, serial_number, timestamp, 0);
					events.back().addBankHeader("ODBF", MIDAS::CHAR, event_size, ifs.tellg());
				}

				ifs.seekg(event_size, std::ios::cur);
			}
		}
	} else {
		throw std::invalid_argument("File::File Unable to open " + file_name);
	}
}

std::string MIDAS::File::getFileName() const {
	return file_name;
}

const std::vector<MIDAS::Event>& MIDAS::File::getEvents() const {
	return events;
}
unsigned int MIDAS::File::getRunNumber() const {
	//The BOR is enough, because we have checked it matches the EOR in the
	//constructor
	return events[0].getSerialNumber();
}
nlohmann::json MIDAS::File::getFromInitialODB(const nlohmann::json::json_pointer& ptr) const {
	const MIDAS::BankHeader odb_header(events[0].getBankHeaders()[0]);
	std::streampos data_pos(odb_header.getDataPos());
	unsigned int num_items(odb_header.getNumItems());

	std::ifstream ifs(file_name, std::ios::binary);
	if(ifs.is_open()) {
		ifs.seekg(data_pos, std::ios::beg);

		std::vector<char> odb(getFromBin<char>(ifs, num_items));
		nlohmann::json json(nlohmann::json::parse(odb));

		return json.at(ptr);
	} else {
		throw std::runtime_error("File::getFromInitialODB File removed during execution " + file_name);
	}	
}
nlohmann::json MIDAS::File::getFromFinalODB(const nlohmann::json::json_pointer& ptr) const {
	const MIDAS::BankHeader odb_header(events.back().getBankHeaders()[0]);
	std::streampos data_pos(odb_header.getDataPos());
	unsigned int num_items(odb_header.getNumItems());

	std::ifstream ifs(file_name, std::ios::binary);
	if(ifs.is_open()) {
		ifs.seekg(data_pos, std::ios::beg);

		std::vector<char> odb(getFromBin<char>(ifs, num_items));
		nlohmann::json json(nlohmann::json::parse(odb));

		return json.at(ptr);
	} else {
		throw std::runtime_error("File::getFromInitialODB File removed during execution " + file_name);
	}	
}
nlohmann::json MIDAS::File::getFromODB(const nlohmann::json::json_pointer& ptr) const {
	nlohmann::json initial_json(getFromInitialODB(ptr));
	nlohmann::json final_json(getFromFinalODB(ptr));

	if(initial_json == final_json) {
		return initial_json;
	} else {
		throw std::invalid_argument("File::getFromODB Non-constant ODB value");
	}
}

std::vector<BV::Event> MIDAS::File::getBVEvents() const {
	std::vector<BV::Event> bv_events;
	for(const MIDAS::Event& event : events) {
		if(event.getEventId() == 1) {
			BV::Event bv_event(unpackBVEvent(event));
			if(!bv_event.isEmpty()) {
				bv_events.push_back(bv_event);
			}
		}
	}
	return bv_events;
}

BV::Event MIDAS::File::unpackBVEvent(const MIDAS::Event& event) const {
	if(event.getEventId() != 1) {
		throw std::invalid_argument("File::unpackBVEvent Event with wrong event ID");
	}

	std::vector<DAQ::ADCWaveform> adc_waveforms;
	std::vector<DAQ::TDCTime> tdc_times;
	for(const MIDAS::BankHeader& header : event.getBankHeaders()) {
		if(header.isBankNameCompatible(DAQ::adc_16_name)) {
			if(header.getNumItems() > 36) {
				adc_waveforms.push_back(unpackADCWaveform(header));
			}
		} else if(header.isBankNameCompatible(DAQ::tdc_name)) {
			std::vector<DAQ::TDCTime> hits(unpackTDCTimes(header));
			//Append whatever you find. This includes repeated bank names
			if(!hits.empty()) {
				tdc_times.insert(tdc_times.end(), hits.begin(), hits.end());
			}
		}
	}

	return BV::Event(adc_waveforms, tdc_times);
}

DAQ::ADCWaveform MIDAS::File::unpackADCWaveform(const MIDAS::BankHeader& header) const {
	if(!header.isBankNameCompatible(DAQ::adc_16_name)) {
		throw std::invalid_argument("File::unpackADCWaveform Not BV ADC16 data bank");
	}
	if(header.getDataType() != MIDAS::U_INT8) {
		throw std::invalid_argument("File::unpackADCWaveform Data bank with wrong data type");
	}
	if(header.getNumItems() % 2 != 0) {
		throw std::invalid_argument("File::unpackADCWaveform Data bank with odd number of items");
	}

	std::ifstream ifs(file_name, std::ios::binary);
	if(ifs.is_open()) {
		std::streampos data_pos(header.getDataPos());
		ifs.seekg(data_pos, std::ios::beg);

		std::vector<unsigned char> info(getFromBin<unsigned char>(ifs, 32));
		if(info[0] != 1) {
			throw std::invalid_argument("File::unpackADCWaveform Wrong packet type");
		}
		if(info[1] != 3) {
			throw std::invalid_argument("File::unpackADCWaveform Wrong packet version");
		}

		unsigned char board_number(info[4]);
		unsigned char channel_number(info[5]);
		std::vector<short int> samples;

		if(header.getNumItems() > 36) {
			unsigned int num_samples((header.getNumItems() - 36) / 2);
			samples.reserve(num_samples);
			//The bytes from this data bank are in big-endian. Need to invert
			//them and shift (<<, >>) is undefined behavior with signed type.
			//First use unsigned, then cast. 
			std::vector<unsigned short int> vals(getFromBin<unsigned short int>(ifs, num_samples));
			for(unsigned short int x : vals) {
				unsigned short int y = ((x >> 8) | (x << 8));
				samples.push_back(std::bit_cast<short int>(y));
			}
		}

		return DAQ::ADCWaveform(board_number, channel_number, samples, DAQ::adc_16_freq);
	} else {
		throw std::runtime_error("File::unpackADCWaveform File removed during execution " + file_name);
	}
}
std::vector<DAQ::TDCTime> MIDAS::File::unpackTDCTimes(const MIDAS::BankHeader& header) const {
	if(!header.isBankNameCompatible(DAQ::tdc_name)) {
		throw std::invalid_argument("File::unpackTDCTimes Not BV TDC data bank");
	}
	if(header.getDataType() != MIDAS::U_INT) {
		throw std::invalid_argument("File::unpackTDCTimes Data bank with wrong data type");
	}

	std::ifstream ifs(file_name, std::ios::binary);
	if(ifs.is_open()) {
		std::vector<DAQ::TDCTime> tdc_times;

		std::streampos data_pos(header.getDataPos());
		ifs.seekg(data_pos, std::ios::beg);

		unsigned int num_words(header.getNumItems());
		std::vector<unsigned int> words(getFromBin<unsigned int>(ifs, num_words));
		//Data comes in big-endian. Invert the bytes
		for(unsigned int& x : words) {
			x = (x>>24) | ((x<<8)&0xff0000) | ((x>>8)&0xff00) | (x<<24);
		}

		unsigned int epoch{ 0 };
		//Ignore first 6 words. Reason unknown.
		for(unsigned int i = 6; i < num_words; ++i) {
			unsigned int word = words[i];
			if((word & 0xfffc) == 0x100) {
				unsigned short int fpga = static_cast<unsigned short int>(word & 0x3);
				unsigned int num_tdc = (word >> 16) & 0x1fff;
				for(unsigned int j = 0; j < num_tdc; ++j) {
					++i;
					if(i >= num_words) {
						break;
					}
					word = words[i];
					unsigned int three_bits = word >> 29;
					if(three_bits == 4) {
						unsigned char chan = static_cast<unsigned char>((word >> 22) & 0x7f);
						unsigned short int fine = static_cast<unsigned short int>((word >> 12) & 0x3ff);
						bool edge = static_cast<bool>((word >> 11) & 1);
						unsigned short int coarse = static_cast<unsigned short int>(word & 0x7ff);

						tdc_times.emplace_back(fpga, chan, epoch, coarse, fine, edge);
					} else if(three_bits == 3) {
						epoch = word & 0x0fffffff;
					}
				}
			} else if((word & 0xffff) == 0xc001) {
				//cts word
				unsigned int num_cts = (word >> 16) & 0xff;
				i += num_cts;
			} else if((word & 0xffff) == 0x5555) {
				//End of sub-event
				break;
			}
		}
		return tdc_times;
	} else {
		throw std::runtime_error("File::unpackTDCTimes File removed during execution " + file_name);
	}
}
