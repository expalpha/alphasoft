/* Constants related to the DAQ
 */

#ifndef DAQCONSTANTS_HPP
#define DAQCONSTANTS_HPP

#include<string>

namespace DAQ {
	constexpr double adc_16_freq {105e6}; //105 Msps sampling frequency 16 channels (BV) of the ADC

	constexpr double epoch_t {10.24e-6}; //Each epoch counter corresponds to 10.24 microseconds
	constexpr double coarse_t {5e-9}; //Each coarse counter corresponds to 5 nanoseconds

	const std::string adc_16_name {"B???"}; //Name of all the data banks for BV analog waveforms. '?' wildcard
	const std::string tdc_name {"TRBA"};
}

#endif
