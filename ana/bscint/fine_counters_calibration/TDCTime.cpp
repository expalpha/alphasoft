#include "TDCTime.hpp"

DAQ::TDCTime::TDCTime(unsigned short int fpga_index, unsigned char chan_num,
	unsigned int E, unsigned short int C, unsigned short int F, bool edge) :
	fpga(fpga_index), channel(chan_num), epoch(E), coarse(C), fine(F), rising(edge) {
}

unsigned short int DAQ::TDCTime::getFPGA() const {
	return fpga;
}
unsigned char DAQ::TDCTime::getChannel() const {
	return channel;
}
unsigned int DAQ::TDCTime::getEpoch() const {
	return epoch;
}
unsigned short int DAQ::TDCTime::getCoarse() const {
	return coarse;
}
unsigned short int DAQ::TDCTime::getFine() const {
	return fine;
}
bool DAQ::TDCTime::isRising() const {
	return rising;
}
bool DAQ::TDCTime::isFalling() const {
	return !rising;
}
