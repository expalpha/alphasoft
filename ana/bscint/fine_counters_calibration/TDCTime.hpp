/* The TDCtime object represents the time recorder by a signal reaching the TDC.
 * Find more information in the documentation.pdf
 */

#ifndef TDCTIME_HPP
#define TDCTIME_HPP

namespace DAQ{
	class TDCTime {
		private:
			unsigned short int fpga;
			unsigned char channel;
			unsigned int epoch;
			unsigned short int coarse;
			unsigned short int fine;
			bool rising; //falling edge if it is not rising edge
		public:
			TDCTime(unsigned short int fpga_index, unsigned char chan_num, 
					unsigned int E, unsigned short int C, unsigned short int F,
					bool edge);
			
			unsigned short int getFPGA() const;
			unsigned char getChannel() const;
			unsigned int getEpoch() const;
			unsigned short int getCoarse() const;
			unsigned short int getFine() const;
			bool isRising() const;
			bool isFalling() const;
	};
}

#endif
