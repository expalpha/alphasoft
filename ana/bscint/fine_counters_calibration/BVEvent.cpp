#include"BVEvent.hpp"

BV::Event::Event(const std::vector<DAQ::ADCWaveform>& wfs,
		const std::vector<DAQ::TDCTime>& ts) : adc_waveforms(wfs), tdc_times(ts) {
}

const std::vector<DAQ::ADCWaveform>& BV::Event::getADCWaveforms() const {
	return adc_waveforms;
}
const std::vector<DAQ::TDCTime>& BV::Event::getTDCTimes() const {
	return tdc_times;
}

bool BV::Event::isEmpty() const {
	if(!adc_waveforms.empty() || !tdc_times.empty()) {
		return false;
	}
	return true;
}
