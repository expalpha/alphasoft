#ifndef _TSISChannel_
#define _TSISChannel_

#include <iostream>
#include "TString.h"
#define NUM_SIS_MODULES 2
#define NUM_SIS_CHANNELS 32

/// @brief Class to identify SIS Channels
///
/// Simple container to for handing SIS channel IDs. Avoid users having to do manipulation of array indexes

class TSISChannel
{
   public:
      int fChannel; ///< The input number on a SIS module (counting from 0)
      int fModule;  ///< The SIS module number (counting from 0)

   TSISChannel();
   TSISChannel(const int CombinedChannelAndModule);
   TSISChannel(const int chan, const int mod);
   TSISChannel(const TSISChannel& c);
   TSISChannel& operator=(const TSISChannel& rhs);
   /// @brief Check that the channel numbers are within valid range
   ///
   /// fChannel and fModule must be above zero
   /// fChannel must be under 32 (NUM_SIS_CHANNELS)
   /// fModule must be under 2 (NUM_SIS_MODULES)
   /// The default constructor intentionally initialised to values that fail IsValid()
   bool IsValid() const
   {
      if (fChannel >= 0 && fChannel < NUM_SIS_CHANNELS)
         if (fModule >= 0 && fModule < NUM_SIS_MODULES)
            return true;
      return false;
   }
   /// @brief Decode the TSISChannel to a single integer ID number
   /// @return 
   Int_t toInt() const {
       return fModule* NUM_SIS_CHANNELS + fChannel;
   }
};

bool operator==(const TSISChannel& lhs, const TSISChannel& rhs);
std::ostream& operator<< (std::ostream& os, const TSISChannel& ch);
TString& operator+=(TString& s, const TSISChannel& lhs);
#endif
