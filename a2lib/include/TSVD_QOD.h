#ifndef _SVDQOD_
#define _SVDQOD_

#include "TH1I.h"
#include "TAlphaEvent.h"
#include "TSiliconEvent.h"

#ifndef RFCUT
//~4mHz Background (42% efficiency)
#define RFCUT 0.398139
//45mHz Background (72% efficiency)
//#define RFCUT 0.230254
//100mHz Background (78% efficiency)
//#define RFCUT 0.163
#endif


class TSVD_QOD: public TObject
{
   private:
   int fRunNumber;                  ///< Run number of event
   double fVF48Timestamp;           ///< Time of event according to the VF48 clock
   int fVF48NEvent = -1;            ///< Event number according to VF48 (event number of read out)
   int fCosmicTracks = 0;           ///< Count the number of 'CosmicTracks', possibly unused
   int fProjectedVertices = 0;      ///< Count the number of vertices from the projection method, possible unused
   int fNRawHits = 0;               ///< Raw hits on the N side of all silicon modules
   int fPRawHits = 0;               ///< Raw hits on the P side of all silicon modules
   int fNHits = 0;                  ///< Number of hits in the silicon modules (coincidence between P and N hits)
   int fNTracks = 0;                ///< Number of tracks in event
   int fNVertices = 0;              ///< Number of vertices in event (at time of writing only 0 or 1)
   std::vector<bool> fPassedCuts;   ///< Container for multiple cuts
   double fX = 0;                   ///< Vertex X position
   double fY = 0;                   ///< Vertex T position
   double fZ = 0;                   ///< Vertex Z position
   double fT = 0;                   ///< Run time of event
   int fHitOccupancy[72];           ///< Occupancy of each hybrid module
   int fOccupancyUS = 0;            ///< Occupancy of upstream end of detector
   int fOccupancyDS = 0;            ///< Occupancy of downstream end of detector
   double fRFOut = -1;              ///< rfout value from MVA
   std::vector<std::string> fCutNames = {"NoCut","Passed Cut","OnlineMVA"};
   std::vector<double> fMVACutVals = {};
//TTC
//Inner layer TTC Counts Median
// Inner layer TTC Counts Mean
// Inner layer TTC Counts Mean Error
//Middle layer TTC Counts Median
// Inner layer TTC Counts Mean
// Inner layer TTC Counts Mean Error
//Outer layer TTC Counts Median
// Inner layer TTC Counts Mean
// Inner layer TTC Counts Mean Error

   public:
   TSVD_QOD();
   TSVD_QOD(TAlphaEvent* a, TSiliconEvent* s, double rfout );
   virtual ~TSVD_QOD();

   /// Get the run Number of event
   int GetRunNumber() const { return fRunNumber; }
   /// Get the VF48 timestamp of event
   double GetVF48Timestamp() const { return fVF48Timestamp; }
   /// Get the event number acording to the VF48 (read out event number)
   int GetEventNumber() const { return fVF48NEvent; }

   /// Get the number of hits in event
   int GetNHits() const { return fNHits; }
   /// Get the number of tracks in event
   int GetNTracks() const { return fNTracks; }
   /// Get the number of verticies in event (at time of writing either 0 or 1)
   int GetNVerticies() const { return fNVertices; }

   /// Get X position of vertex
   double X() const { return fX; }
   /// Get Y position of vertex
   double Y() const { return fY; }
   /// Get Z position of vertex
   double Z() const { return fZ; }
   /// Get official timestamp of event
   double GetTimeOfEvent() const { return fT; }
   /// Alternative name for official timestamp of event
   double GetRunTime() const { return GetTimeOfEvent(); }
   /// Get the radius of event
   double R() const { return TMath::Sqrt(fX * fX + fY * fY); }
   /// Get the rfout value of event
   double GetRFOut() const { return fRFOut; }

   /// Get the occupancy of the upstream end of the detector
   int GetOccupancyUS() const { return fOccupancyUS; }
   /// Get the occupancy of the downstream end of the detector
   int GetOccupancyDS() const { return fOccupancyDS; }
   /// Get the occupancy of a specific hybrid module
   int GetHitOccupancy(const int i) const
   {
      if (i < 72 && i >= 0)
         return fHitOccupancy[i];
      std::cout << "Requested occupancy of module that doesn't exist...\n";
      return -1;
   }

   /// Get the labels for the fPassCuts
   std::vector<std::string> CutNames()
   {
      return fCutNames;
   }
   /// Get the number of verticies? This looks wrong
   int GetVertexStatus() const { return fNVertices; }
   /// Get array of the types of cuts
   const std::vector<bool> GetPassedCuts() const { return fPassedCuts; }
   /// Get a specific by its ID number cut on event
   bool GetOnlinePassCuts(const size_t i) const
   {
      if (i < fPassedCuts.size())
         return fPassedCuts.at(i);
      std::cout << "Requested cut that doesn't exist...\n";
      return false;
   }
   /// Get the name of a specific cut ID
   std::string GetOnlinePassCutsName(const size_t i) {return CutNames().at(i); }
   /// Overwrite the timestamp of the event
   void SetTimeOfEvent(const double event_time) { fT = event_time; }
   /// Add an MVA cut.
   void AddMVACut(double val) {
      fCutNames.push_back(Form("OfflineMVA_%zu",fMVACutVals.size()));
      fMVACutVals.push_back(val);
      fPassedCuts.push_back(fRFOut > val);
   }

   ClassDef(TSVD_QOD,3); 
};

#endif

