#ifndef _SIMOD_
#define _SIMOD_

#define nASICs 4
#define nSTRIPs 128
#ifdef ALPHA1COMP
  #define nSil 60
  #define nVF48 6
  #define TTC_TA_inputs 256
#else
  #define nSil 72
  #define nVF48 8
  #define TTC_TA_inputs 512
#endif
#define nSiAlpha1 60
#define nSiAlpha2 72

#endif

