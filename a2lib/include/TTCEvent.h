//
// TTCEvent.h
//
// Class for trigger events
// L GOLINO, J STOREY
//


#ifndef _TTCEvent_
#define _TTCEvent_

#include "TObject.h"
#include <iostream>
#include <stdint.h>

class TTCEvent : public TObject
{
private:
  
  Int_t 		FPGA;                
  uint16_t      Latch[8];          
  uint16_t      Mult;
  uint16_t      Mult2;

  uint32_t      TTCNEvent;           // TTC event number
  uint32_t      TTCID;               // TTC event ID (tree location)
  uint32_t      TTCCounter;          // TTC counter
  uint32_t      TTCTimestamp;        // TTC Timestamp

  Double_t    	RunTime;             // Time of event from start of MIDAS run ( time = time of corresponding ADC trigger into SIS )
  Double_t      TSRunTime;           // Time of event from start of MIDAS run ( time = timestamp + TTCoffset (SIS time of 1st TTC trigger) )
  Int_t         RunNumber;           // MIDAS runnumber
  Int_t         ExptNumber;          // Sequencer experiment number (aka. chain number)
  Double_t      ExptTime;            // SIS time since the start of the experiment 
  Double_t      EventTime;       

  Int_t         VertexCounter;       // Link to the vertex tree
  Int_t         LabVIEWCounter;      // Link to the labview tree
  Int_t         SISCounter;          // Link to the SIS tree

public:
  
  // setters  
  void SetFPGA(Int_t fpga) 		                	 { FPGA = fpga; }
  void SetLatch(uint16_t TApattern, Int_t latchN )     { Latch[latchN] = TApattern; }
  void SetMult(uint16_t mult)			                 { Mult = mult; }
  void SetMult2(uint16_t mult)			             { Mult2 = mult; }

  void SetTTCNEvent(uint32_t Nevent)                { TTCNEvent = Nevent; }
  void SetID(uint32_t ID)                        { TTCID = ID; }
  void SetTTCCounter(uint32_t Ncounter)                { TTCCounter = Ncounter; }
  void SetTTCTimestamp(uint32_t time)               { TTCTimestamp = time; }

  void SetRunTime(Double_t time)                     { RunTime = time; }
  void SetTSRunTime(Double_t time)                   { TSRunTime = time; }
  void SetRunNumber(Int_t runnumber)		         { RunNumber = runnumber; }
  void SetExptNumber(Int_t exptnumber)               { ExptNumber = exptnumber; }
  void SetExptTime(Double_t expttime)		         { ExptTime = expttime; }
  void SetEventTime(Double_t time)                   { EventTime = time; }

  void SetVertexCounter( Int_t event ) 	             { VertexCounter = event; }
  void SetLabVIEWCounter( Int_t event )	             { LabVIEWCounter = event; }
  void SetSISCounter( Int_t event )                  { SISCounter = event; }

  void ClearTTCEvent();
  
  // getters
  Int_t GetFPGA( ) 		                        	 { return FPGA; }
  uint16_t GetLatch(Int_t latchN)                      { return Latch[latchN]; }
  uint16_t GetMult( )		                             { return Mult; }
  uint16_t GetMult2( )		                         { return Mult2; }

  uint32_t GetTTCNEvent( )                          { return TTCNEvent; }
  uint32_t GetID( )                              { return TTCID; }
  uint32_t GetTTCCounter( )                            { return TTCCounter; }
  uint32_t GetTTCTimestamp( )                       { return TTCTimestamp; }

  Double_t GetRunTime( )                             { return RunTime; }
  Double_t GetTSRunTime( )                           { return TSRunTime; }
  Int_t GetRunNumber( ) 		                     { return RunNumber; }
  Int_t GetExptNumber( )                             { return ExptNumber; }
  Double_t GetExptTime( )		                     { return ExptTime; }
  Double_t GetEventTime( )                           { return EventTime; }
  
  Int_t GetVertexCounter( ) 	                     { return VertexCounter; }
  Int_t GetLabVIEWCounter( )	                     { return LabVIEWCounter; }
  Int_t GetSISCounter( )                             { return SISCounter; }

  using TObject::Print;
  virtual void Print();
  
  // default class member functions
  TTCEvent();  
  virtual ~TTCEvent(); 
  
  ClassDef(TTCEvent,1) 
};
#endif
