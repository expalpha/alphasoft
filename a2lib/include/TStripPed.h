#ifndef _TStripPed_
#define _TStripPed_

#include <math.h>
#include <vector>
#include <array>
#include "TObject.h"
#include <iostream>
class TStripPed: public TObject
{
public:
   std::array<int,1024> histo;
   std::array<int,1025> rawhisto;
   double sigma=99999.;
   double rawADCMean=0.;
   double rawADCRMS=0.;
   //First pass variables
   double stripMean=0.;
   double stripRMS=0.;
   //Second pass variables
   double stripMeanSubRMS=-9999.; //filtered mean
   double StripRMSsAfterFilter=0;
   
   
   
   bool FirstPassFinished=false;
   int DataPoints=0.;

private:
   const double hmax;
   const double hmin;
   const double strip_bin_width;
   const double one_over_strip_bin_width;
   const double strip_bins;

public:
   TStripPed(const int nBins, const double binWidth);
   virtual ~TStripPed();
   int GetBin(const double x) const
   {
      return (x - hmin) * one_over_strip_bin_width;
   }
   double GetX(const int x) const
   {
      return x * strip_bin_width + hmin;
   }
   
   void InsertValue(const double ped_sub, const double raw_adc);
   double GetMean(const double _min=-9999999., const double _max=9999999.);
   double GetStdev(const double mean,const double _min=-9999999., const double _max=9999999.);

   double GetRAWMean(const int _min=-9999999., const int _max=9999999.);
   double GetRAWStdev(const double mean,int _min=-9999999., int _max=9999999.);

   double GetRMS(const double _min=-9999999., const double _max=9999999.);

   void CalculatePed();
   ClassDef(TStripPed,1)
};


#endif
