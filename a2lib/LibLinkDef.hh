#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class  TSettings+;
#pragma link C++ class  TVF48SiMap+;

#pragma link C++ class TSISChannel+;
#pragma link C++ class  TSISChannels+;
#pragma link C++ class  TSISEvent+;

#pragma link C++ class  TSiliconEvent+;
#pragma link C++ class  TSiliconVA+;
#pragma link C++ class  TSiliconModule+;
#pragma link C++ class  TTCEvent+;


//Legacy:

#pragma link C++ class  TAlphaEvent+;
#pragma link C++ class  TAlphaEventObject; // Don't put a + here it will break things! -Gareth after a 7 day headache
#pragma link C++ class  TAlphaEventMap;
#pragma link C++ class  TAlphaEventHit+;
#pragma link C++ class  TAlphaEventSil+;
#pragma link C++ class  TAlphaEventVerbose+;
#pragma link C++ class  TAlphaDisplay+;
#pragma link C++ class  TAlphaEventNCluster+;
#pragma link C++ class  TAlphaEventPCluster+;
#pragma link C++ class  TAlphaEventTrack+;
#pragma link C++ class  TAlphaEventVertex+;
#pragma link C++ class  TAlphaEventHelix+;
#pragma link C++ class  TAlphaEventCosmicHelix;
#pragma link C++ class  TAlphaGeoDetectorXML;
#pragma link C++ class  TAlphaGeoMaterialXML;
#pragma link C++ class  TAlphaGeoEnvironmentXML;
//#pragma link C++ class  TAlphaGeoPMTXML;
#pragma link C++ class  TAlphaEventSilArray;
#pragma link C++ class  THoughPeakFinder;
#pragma link C++ class  THoughPeak;
#pragma link C++ class  TProjCluster+;
#pragma link C++ class  TProjClusterAna+;
#pragma link C++ class  TProjClusterBase+;

//Silicon Strip Pedestal calculator
#pragma link C++ class TStripPed+;

#pragma link C++ class TSVD_QOD+;
#pragma link C++ class TA2RunQOD;

#endif
