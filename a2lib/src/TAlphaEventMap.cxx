#include "TAlphaEventMap.h"

ClassImp( TAlphaEventMap )

TAlphaEventMap::TAlphaEventMap()
{
  LoadGeometry();
  SetValues();
}

//______________________________________________________________________________
void TAlphaEventMap::LoadGeometry( )
{
  if (gGeoManager!=NULL) return;
  printf("Geometry not loaded, loading it now. This should only happen once...\n");

  new TGeoManager("TALPHAGeo", "ALPHA ROOT geometry manager");
  TAlphaGeoMaterialXML * materialXML = new TAlphaGeoMaterialXML();

  std::ostringstream pn;
  pn << getenv("AGRELEASE") << "/a2lib/geo/material2.xml";
  // std::cout << "===> " << pn.str().c_str() << std::endl;
  materialXML->ParseFile(pn.str().c_str());
  delete materialXML;

  TAlphaGeoEnvironmentXML * environmentXML = new TAlphaGeoEnvironmentXML();
  pn.clear(); pn.str("");
  pn << getenv("AGRELEASE") << "/a2lib/geo/environment2_geo.xml";
  environmentXML->ParseFile(pn.str().c_str());
  delete environmentXML;

  TAlphaGeoDetectorXML * detectorXML = new TAlphaGeoDetectorXML();
  pn.clear(); pn.str("");
  pn << getenv("AGRELEASE") << "/a2lib/geo/detector2_geo.xml";
  detectorXML->ParseFile(pn.str().c_str());
  delete detectorXML;

  // close geometry
  gGeoManager->CloseGeometry();
  printf("Done loading geometry.\n");
}

//______________________________________________________________________________
void TAlphaEventMap::SetValues( )
{
   assert( gGeoManager );
   for (int j=0; j<72;j++)
   {
      char* SilName=ReturnSilName(j);
      fCos[j] = 0.;
      fSin[j] = 0.;
      fXCenter[j] = 0.;
      fYCenter[j] = 0.;
      fZCenter[j] = 0.;

      // Grab the module parameters from the GeoManager
      Int_t n = (Int_t) gGeoManager->GetTopVolume()->GetNodes()->GetEntries();
      TGeoNode* node = NULL;
      for( Int_t i = 0; i < n; i++ )
      {
         node = gGeoManager->GetTopVolume()->GetNode( i );
         if( strncmp(node->GetName(),SilName,4) == 0 ) break;
         else node = NULL;
      }
      if( node )
      {
         fXCenter[j] = node->GetMatrix()->GetTranslation()[0];
         fYCenter[j] = node->GetMatrix()->GetTranslation()[1];
         fZCenter[j] = node->GetMatrix()->GetTranslation()[2];

         Double_t radius = TMath::Sqrt( fXCenter[j]*fXCenter[j] + fYCenter[j]*fYCenter[j] );
         fCos[j] = fXCenter[j]/radius;
         fSin[j] = fYCenter[j]/radius;
         fLayer[j]=ReturnLayer(j);
      }
      else
      {
         fCos[j] = 0;
         fSin[j] = 0;
         fXCenter[j] = 0;
         fYCenter[j] = 0;
         fZCenter[j] = 0;
      }
      delete [] SilName;
   }
}


//______________________________________________________________________________
Int_t TAlphaEventMap::ReturnLayer(Int_t SilNum)
{
  if(nSil == 72){
  // Take the module number 0..71
  // return the detector layer
  
  if(SilNum < 10) return 0;
  else if(SilNum >= 10 && SilNum < 22) return 1;
  else if(SilNum >= 22 && SilNum < 36) return 2;
  else if(SilNum >= 36 && SilNum < 46) return 0;
  else if(SilNum >= 46 && SilNum < 58) return 1;
  else if(SilNum >= 58 && SilNum < 72) return 2;

  return -1;
  }
  if(nSil == 60) {

  // Take the module number 0..59
  // return the detector layer

  if(SilNum < 8) return 0;
  else if(SilNum >= 8  && SilNum < 18) return 1;
  else if(SilNum >= 18 && SilNum < 30) return 2;
  else if(SilNum >= 30 && SilNum < 38) return 0;
  else if(SilNum >= 38 && SilNum < 48) return 1;
  else if(SilNum >= 48 && SilNum < 60) return 2;

  return -1;
  }
}


//______________________________________________________________________________
char * TAlphaEventMap::ReturnSilName(Int_t SilNum)
{
  assert( SilNum >= 0 );
  assert( SilNum < nSil );
  
  if(nSil == 72){
  // Return the silname e.g. 4si5
  // from the silicon module number 0..71

  char * name = new char[5];
  
  // AD end
  if ( SilNum < 10 ) snprintf(name,5,"0si%c",'0' + SilNum);
  if ( SilNum >= 10 && SilNum < 22 )
  {
      if(SilNum == 20) snprintf(name, 5, "1siA" );
      else if ( SilNum == 21 ) snprintf(name, 5, "1siB");
      else  snprintf(name,5,"1si%c",'0'+SilNum - 10 );
  }
  if ( SilNum >= 22 && SilNum < 36 )
  {
      if( SilNum == 32 ) snprintf( name,5,"2siA" );
      else if( SilNum == 33 ) snprintf( name,5,"2siB" );
      else if( SilNum == 34 ) snprintf( name,5,"2siC" );
      else if( SilNum == 35 ) snprintf( name,5,"2siD" );
      else snprintf(name,5,"2si%c",'0'+SilNum - 10 - 12);
  }

  if ( SilNum >= 36 && SilNum < 46 ) snprintf(name,5,"3si%c",'0'+SilNum - 10 - 12 - 14);
  if ( SilNum >= 46 && SilNum < 58 )
  {
      if (SilNum == 56) snprintf(name,5, "4siA");
      else if (SilNum == 57) snprintf(name,5, "4siB");
      else snprintf(name,5,"4si%c",'0'+SilNum - 10 - 12 - 14 - 10);
  }
  if ( SilNum >= 58 && SilNum < 72 )
  {
      if( SilNum == 68 ) snprintf( name,5,"5siA" );
      else if( SilNum == 69 ) snprintf( name,5,"5siB" );
      else if( SilNum == 70 ) snprintf( name,5,"5siC" );
      else if( SilNum == 71 ) snprintf( name,5,"5siD" );
      else snprintf(name,5,"5si%c",'0'+SilNum - 10 - 12 - 14 - 10 - 12);
  }
  return name;
}
  
  
  if(nSil ==60) {
    // Return the silname e.g. 4si5
    // from the silicon module number 0..59

    char * name = new char[5];
  // AD end
    if ( SilNum < 8 )
      {
        snprintf(name,5,"0si%1d",SilNum);
      }
    if ( SilNum >= 8 && SilNum < 18 )
      {
        snprintf(name,5,"1si%1d",SilNum - 8);
      }
    if ( SilNum >=18 && SilNum < 30 )
      {
        if( SilNum == 28 )
          snprintf( name,5,"2siA" );
        else if( SilNum == 29 )
          snprintf( name,5,"2siB" );

        else
          snprintf(name,5,"2si%d",SilNum - 18);
      }
      
    // e+ end
    if ( SilNum >= 30 && SilNum < 38 )
      {
        snprintf(name,5,"3si%1d",SilNum - 30);
      }
    if ( SilNum >= 38 && SilNum < 48 )
      {
        snprintf(name,5,"4si%1d",SilNum - 38);
      }
    if ( SilNum >= 48 && SilNum < 60 )
      {
        if( SilNum == 58)
          snprintf( name,5,"5siA" );
        else if( SilNum == 59 )
          snprintf( name,5,"5siB" );
        else
          snprintf(name,5,"5si%1d",SilNum - 48);
  
      }
  return name;
  }
}

void TAlphaEventMap::Print(Option_t* /*option*/) const
{
   for (int i=0; i<nSil; i++)
   {
      printf("cos: \t%f\n",fCos[i]);     //cos from vertical
      printf("sin: \t%f\n",fSin[i]);     //sin from vertical
      printf("X center:\t%f\n",fXCenter[i]); //x center of module
      printf("Y center:\t%f\n",fYCenter[i]); //y center of module
      printf("Z center:\t%f\n",fZCenter[i]); //z center of module
      printf("Layer: \t%d\n",fLayer[i]);
   }
}
