#include "ALPHA2SettingsDatabase.h"

namespace ALPHA2SettingsDatabase
{
   TSettings* GetTSettings()
   {
#ifdef ALPHASOFT_DB_INSTALL_PATH
      return new TSettings(
            std::string(ALPHASOFT_DB_INSTALL_PATH) + "/main.db"
      );
#else
      return new TSettings(
            std::string( getenv("AGRELEASE") ) +  "/a2lib/main.db"
      );
#endif
   }

   TSettings* GetTSettings(const int runno)
   {
#ifdef ALPHASOFT_DB_INSTALL_PATH
      return new TSettings(
            std::string(ALPHASOFT_DB_INSTALL_PATH) + "/main.db",
            runno
      );
#else
      return new TSettings(
            std::string( getenv("AGRELEASE") ) +  "/a2lib/main.db",
            runno
      );
#endif
   }
}

