#include "TSISChannel.h"

/// @brief Constructor
TSISChannel::TSISChannel()
{
   fChannel = -1;
   fModule = -1;
}

/// @brief Deconstructor
/// @param chan 
/// @param mod 
///
/// The default constructor intentionally initialised to values that fail IsValid()
TSISChannel::TSISChannel(const int chan, const int mod)
{
   fChannel = chan;
   fModule = mod;
}

/// @brief Constructor
/// @param CombinedChannelAndModule 
///
/// Decodes an integer from toInt() into a module and channel number)
TSISChannel::TSISChannel(const int CombinedChannelAndModule)
{
   fModule = 0;
   fChannel = CombinedChannelAndModule;
   while (  fChannel >= NUM_SIS_CHANNELS)
   {
     fModule++;
     fChannel -= NUM_SIS_CHANNELS;
   }
}

/// @brief Copy constructor
/// @param c 
TSISChannel::TSISChannel(const TSISChannel& c): fChannel(c.fChannel), fModule(c.fModule)
{

}

/// @brief = Operator
/// @param rhs 
/// @return 
TSISChannel& TSISChannel::operator=(const TSISChannel& rhs)
{
    fChannel = rhs.fChannel;
    fModule = rhs.fModule;
    return *this;
}

/// @brief == comparison
/// @param lhs 
/// @param rhs 
/// @return 
///
/// Checks that the fChannel matches and the fModule matches
bool operator==(const TSISChannel& lhs, const TSISChannel& rhs)
{
   return ( lhs.fChannel == rhs.fChannel && lhs.fModule == rhs.fModule);
}

/// @brief ostream << operator for inline printing
/// @param os 
/// @param ch 
/// @return 
std::ostream& operator<< (std::ostream& os, const TSISChannel& ch)
{
    os <<"[ " << ch.fChannel << ": " << ch.fModule << " ]";
    return os;
}

/// @brief TString += operator for building human readable labels
/// @param s 
/// @param lhs 
/// @return 
TString& operator+=(TString& s, const TSISChannel& lhs)
{
    s += "[ ";
    s += lhs.fChannel;
    s += ", ";
    s += lhs.fModule;
    s += "]";
    return s;
}