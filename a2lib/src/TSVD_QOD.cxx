#include "TSVD_QOD.h"


/*! \class TSVD_QOD
    \brief Class for 'quality of data' data for the SVD

*/

/// @brief Constructor
/// @param 
///
/// Each time window has its own zero time, allowing for complex plotting like events around a LyALPHA pulse
TSVD_QOD::TSVD_QOD()
{
}

/// @brief Constructor
/// @param a TAlphaEvent, the reconstruction class ALPHA2
/// @param s TSiliconEvent, a large event container for ALPHA2 (also applies basic cuts)
/// @param online_mva basic event classification with a BDT from the online_mva_module
///
/// Each event comes from a single read out of the SVD (both TAlphaEvent, TSiliconEvent and OnlineMVA must be from the same event)

TSVD_QOD::TSVD_QOD(TAlphaEvent* a, TSiliconEvent* s, double rfout )
{
   fRunNumber        = s->GetRunNumber();
   fVF48Timestamp    = s->GetVF48Timestamp();
   fVF48NEvent       = s->GetVF48NEvent();
   fNRawHits         = s->GetNsideNRawHits();
   fPRawHits         = s->GetPsideNRawHits();
   fNHits            = s->GetNHits();
   fNTracks          = s->GetNTracks();
   fNVertices        = s->GetNVertices();
   fProjectedVertices= (int)(s->GetVertexType() & 2);
   fPassedCuts.resize(4); //?????
   fPassedCuts.at(0) = s->GetNVertices();
   fPassedCuts.at(1) = s->GetPassedCuts();
   fPassedCuts.at(2) = rfout > RFCUT;      //RFCUT defined in TSVD_QOD.h
   fPassedCuts.at(3) = rfout > RFCUT;      // Edit this one to your cut value in post analysis with TA2Plot.ApplyMVA(3, 0.142).
   fRFOut            = rfout;
   TVector3* vertex = s->GetVertex();
   if (fNVertices)
   {
      fX = vertex->X();
      fY = vertex->Y();
      fZ = vertex->Z();
   }
   else
   {
      fX = -99;
      fY = -99;
      fZ = -99;
   }
   for (int i = 0; i < 72; i++)
      fHitOccupancy[i] = 0;

   const std::vector<TAlphaEventHit*>* hits = a->GatherHits();
   const int NHits = hits->size();

   for(int j = 0; j < NHits; j++)
   {
      const TAlphaEventHit* c = hits->at(j);
      int SilNum = c->GetSilNum();
      fHitOccupancy[SilNum]++;
      if (j < 36)
         fOccupancyUS++;
      else
         fOccupancyDS++;
   }

}

/// @brief Deconstructor
TSVD_QOD::~TSVD_QOD()
{
}
