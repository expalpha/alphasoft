//
// TTCEvent.h
//
// Class for trigger events
// L GOLINO, J STOREY
//


#include "TTCEvent.h"

ClassImp(TTCEvent)

//Default Constructor
TTCEvent::TTCEvent()
{
  ClearTTCEvent();
}
//Default Destructor
TTCEvent::~TTCEvent()
{
}

void TTCEvent::ClearTTCEvent()
{
  FPGA = -1;
  
  memset( Latch, 0, sizeof(Latch) );

  Mult = 0;
  Mult2 = 0;

  TTCNEvent = 0;
  TTCID = -1;
  TTCCounter = 0;
  TTCTimestamp = 0;
  
  RunTime = -1.;
  TSRunTime = -1.;
  
  RunNumber = 0;
  ExptNumber = 0;
  ExptTime = -1;
  EventTime = -1.;
  
  VertexCounter = -1;
  LabVIEWCounter = -1;
  SISCounter = -1;


}

void TTCEvent::Print()
{
    int ma = Mult&0xF;
    int mb = (Mult>>4)&0xF;
    int mc = (Mult>>8)&0xF;
    int multa = Mult&0x1000;
    int multb = Mult&0x2000;
    int multc = Mult&0x4000;
    int mult_and = Mult&0x8000;

    int ma_2 = Mult2&0xF;
    int mb_2 = (Mult2>>4)&0xF;
    int mc_2 = (Mult2>>8)&0xF;
    int multa_2 = Mult2&0x1000;
    int multb_2 = Mult2&0x2000;
    int multc_2 = Mult2&0x4000;
    int mult_and_2 = Mult2&0x8000;

    printf("TTC1: TTCevent %d, counter 0x%04x, FPGA%i, ts 0x%08x, RunTime %.6f s, EventTime %.3f ms mult 0x%04x, a=%d, b=%d, c=%d, bits %s %s %s %s, latch 0x%04x %04x %04x %04x %04x %04x %04x %04x\n",
           TTCNEvent, TTCCounter, FPGA, TTCTimestamp, RunTime, EventTime, Mult, ma, mb, mc, multa?"A":"", multb?"B":"", multc?"C":"", mult_and?"ABC":"",
           Latch[0], Latch[1], Latch[2], Latch[3], Latch[4], Latch[5], Latch[6], Latch[7]);

    printf("TTC2:  mult_2 0x%04x, a_2=%d, b_2=%d, c_2=%d, bits %s %s %s %s\n",
           Mult2, ma_2, mb_2, mc_2, multa_2?"A":"", multb_2?"B":"", multc_2?"C":"", mult_and_2?"ABC":"");

}
