# Basic Requirements:

* [root][rootlink], **supported version >= 6.14**

  [rootlink]: https://root.cern.ch/

* [manalyzer][manalyzerlink], it can be obtained as a git submodule

  [manalyzerlink]: https://midas.triumf.ca/MidasWiki/index.php
  
* [CMake][cmakelink] **version >=3.0**

  [cmakelink]: https://cmake.org/ "CMake website"
  
## Optional Requirements:

* [Midas][midaslink], this is useful for "online analysis"

  [midaslink]: https://midas.triumf.ca/MidasWiki/index.php/Main_Page "MIDAS Wiki"

* [HDF5][hdf5link], this is required if one wants to save the spacepoints in a separated file (using in Machine Learning) 

[hdf5link]: https://support.hdfgroup.org/documentation/hdf5/latest/index.html

```
sudo apt install libhdf5-dev
```


OS (strongly recommended):

* [cern-centos7][cern-centos7link]

  [cern-centos7link]: http://linux.web.cern.ch/linux/centos7/

Succefully tested on [Ubuntu 20.04LTS][ubuntu-link]

[ubuntu-link]: https://ubuntu.com/blog/ubuntu-20-04-lts-arrives "Canonical Announcement"



# INSTALLATION:

```
git clone https://bitbucket.org/expalpha/alphasoft.git --recursive
cd alphasoft
. agconfig.sh
mkdir build && cd build
```

`cmake3` if OS is CentOS7, `cmake` for Ubuntu

```
cmake3 ../
cmake3 --build . --target install
```

if you want the documenation to build:
```
cmake3 --build . --target doxygen
```

for multi-process build

```
cmake3 --build . --target install -- -j`nproc --ignore=2`
```


# RUNNING:

Basic invocation

```
agana.exe run01234sub*.mid.lz4
```


Additional options

```
--mt                    : Enable multithreaded mode.
-O/path/to/newfile.root : Specify output root file filename

--: All following arguments are passed to the analyzer modules Init() method

    --recoff                                Turn off reconstruction
    --aged                                  Turn on event display
    --diag                                  Enable histogramming
    --anasettings /path/to/settings.json    Load analysis settings
    --lite                                  Save only vertex information
```

For example:

```
agana.exe --mt -Otest01234.root run01234sub*.mid.lz4 -- --diag --anasettings ana/cosm.json

```

# RUNNING WITH MVA:

ROOT's TMVA tools are integrated into perform Multi Variate Analysis (what we now call ML). Scripts to train can be found in scripts/condor_job/AGMVA/. These are written by Lukas to utilise the parrellism of CERN's HTCondor service. See scripts/condor_job/README.md for details

Once you have a model trained, you can apply it to data, using all the tools you are familiar with online. Simply add `--usemva` to agana (and `--usemva myModel.xml` if you have a new model trained)

# Plotting Vertex Data

Plotting tools are very similar for ALPHA2 and ALPHAg, for example if you want to plot the entire run for ALPHAg you can interactively write in root:

```C++
root [0] TAGPlot a;
root [1] a.AddTimeGate(9610,0.,-1);
End time from CB0cb01:72.4202
End time from CB0cb02:72.7598
End time from CB0cb03:72.5266
End time from CB0cb04:73.1646
root [2] a.DrawVertexCanvas(
TCanvas* DrawVertexCanvas(const char* name = "cVTX", const string CutsMode = "0")
root [2] a.DrawVertexCanvas("NoCuts","0")->SaveAs("NoCuts.png")
root [3] a.DrawVertexCanvas("Cuts","1")->SaveAs("Cut_1.png")
root [4] a.DrawVertexCanvas("Cuts","7")->SaveAs("Cut_7.png")

```

Each cut has a string to label it, so here I dont want to state "Cut 7 is MVA" as the numbers can change and evolve.
Note cuts can be combined "1&7" will be MVA that has a vertex

Use tab to see what other functions are available in TA2Plot / TAGPlot

```C++

root [2] a.Add
AddDumpGates
AddTimeGate
AddTimeGates
```

 # BARREL VETO CALIBRATION:

First, generate root files for the runs you want by running agana (you can use --lite option).
Then run the calibration macro:
```
root -l -b
.L ana/macros/bsc/CalibrateBarrel.C
CalibrateBarrel(6231);
```
This saves a calibration root file to ${AGOUTPUT}/BarrelCalibration06231.root.
If run 6231 does not have enough data, it will also use 6230, then 6229, up to 20 runs before the given number.
Then the next time you run agana, it will look there for a calibration file less than 100 runs old.


# Internal definitions:


Spacepoints:

* -1    Anode wire outside range
* -2    Negative time
* -3    X or Y not a number
* -4    Z not defined
* -5    R outside fiducial volume
* -6   	X, Y or Z errors not a number
* -7    Z error not defined
*  1    Good point


Helix:

*  -1	Number of spacepoints is less than 5
*   0	Fitted helix
*   1	Good helix - NOT used for vertexing
*   2	Seed helix - used for vertexing 
*   3	Added helix - used for vertexing
*  -2	R fit failed
*  -3	Z fit failed
*  -4	R chi^2 cut failed
*  -5	Z chi^2 cut failed
*  -6	D cut failed
*  -7	duplicated
*  -8 C cut failed
*  -9 pT cut failed
* -14   R chi^2 too small
* -15   Z chi^2 too small
* -11   Too few points


Vertex:

* -2	no good helices
*  0	only one good helix
* -1	failed to find minimum-distance-pair
*  1	only two good helices
*  2	didn't improve vertex (more than 2 helices)
*  3	vertex improved

