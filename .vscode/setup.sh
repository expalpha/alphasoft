#!/bin/bash
echo "\n\n\n ============ \n source agconfig.sh \n ============ \n\n\n"
source /afs/cern.ch/work/l/lgolino/private/alphasofts/AGMVA-TOF/agconfig.sh # or whatever other script you want to have executed

echo "\n\n\n ============ \n cd build \n ============ \n\n\n"
pwd
cd /afs/cern.ch/work/l/lgolino/private/alphasofts/AGMVA-TOF/build
#pwd
#echo "\n\n\n ============ \n cmake ../ \n ============ \n\n\n"
cmake ../
#echo "$@" 
#cmake "$@" 
#/usr/bin/cmake "$@"  
make install
