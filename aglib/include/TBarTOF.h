#ifndef _TBARTOF_
#define _TBARTOF_

#include "TBarHit.h"
#include "TVector3.h"

#define TDC_PILEUP_TIME 200e-9

class TBarTOF: public TObject
{
private:
   TBarHit fFirstHit;
   TBarHit fSecondHit;
   double fFirstGlobalOffset;
   double fSecondGlobalOffset;
   double fBarToBarOffset;
   double fBarToBarSigma;
   bool fOffsetsLoaded;

public:
   TBarTOF(); // ctor
   TBarTOF(const TBarTOF &h);
   TBarTOF(const TBarHit &h1, const TBarHit &h2);
   using TObject::Print;
   virtual void Print();
   virtual ~TBarTOF(); // dtor

   const TBarHit& GetFirstHit() const {return fFirstHit;}
   const TBarHit& GetSecondHit() const {return fSecondHit;}
   void AddHits(const TBarHit &h1, const TBarHit &h2);

   void SetFirstGlobalOffset(double o) { fFirstGlobalOffset = o; }
   void SetSecondGlobalOffset(double o) { fSecondGlobalOffset = o; }
   void SetBarToBarOffset(double o) { fBarToBarOffset = o; }
   void SetBarToBarSigma(double o) { fBarToBarSigma = o; }
   void SetOffsetsLoaded(bool o) { fOffsetsLoaded = o; }

   double GetFirstGlobalOffset() const { return fFirstGlobalOffset; }
   double GetSecondGlobalOffset() const { return fSecondGlobalOffset; }
   double GetBarToBarOffset() const { return fBarToBarOffset; }
   double GetBarToBarSigma() const { return fBarToBarSigma; }
   bool IsOffsetsLoaded() const { return fOffsetsLoaded; }

   double GetDistance(bool Z_from_BV=true) const;
   double GetDistanceOverC(bool Z_from_BV=true) const { return GetDistance(Z_from_BV)/299792458; }
   double GetDZ(bool Z_from_BV=true) const;
   int GetOpeningAngle() const; // in bars

   double GetTOF(bool use_global_offsets=true) const;
   double GetTOFmDist(bool Z_from_BV=true, bool use_global_offsets=true) const { return GetTOF(use_global_offsets)-GetDistanceOverC(Z_from_BV); } // Time of flight minus distance/c
   double GetSignificance(bool Z_from_BV=true, bool use_global_offsets=false) const;
   
   bool IsComplete() const { return (fFirstHit.IsComplete() and fSecondHit.IsComplete());}
   bool MatchedToDifferentTPC() const { return(fFirstHit.HasTPCHit() and fSecondHit.HasTPCHit() and fFirstHit.GetZedTPC()!=fSecondHit.GetZedTPC()); }
   bool MatchedToTPC() const { return(fFirstHit.HasTPCHit() and fSecondHit.HasTPCHit()); }

   bool IsPileUp() const { return (TMath::Abs(GetTOF()) > TDC_PILEUP_TIME); }
   bool IsGood() const { return !IsPileUp(); }

  ClassDef(TBarTOF,4);
};


#endif
