#ifndef _TBARTDCHIT_
#define _TBARTDCHIT_
#include "TObject.h"
#include "TMath.h"
#include <iostream>

class TBarTDCHit: public TObject
{
private:
    int fEndID=-1; // 0-63 bottom, 64-127 top
    double fTime=-1; // TDC time from coarse, fine, epoch counters
    double fChannelOffset=-1; // TDC offset calibration
    int fFineCount=-1; // Fine counter
    double fFineTime=-1; // TDC time from fine counter
    double fTimeOverThr=-1; // TDC time between leading and falling edges, no calibration
    bool fMatchedToADC=false;
    bool fIs165Reflection=false;
    bool fIsGoodForTOF=false;

public:
    TBarTDCHit(); // ctor
    using TObject::Print;
    virtual void Print();
    virtual ~TBarTDCHit(); // dtor

    int GetEndID() const {return fEndID;}
    int GetBarID() const {return fEndID%64;}
    bool IsBottom() const {return (0<=fEndID and fEndID<64);}
    bool IsTop() const {return (64<=fEndID and fEndID<128);}
    double GetTime() const {return fTime;} // Raw TDC time, minus TDC zero time, minus event time, seconds
    double GetTimeCali() const {return fTime + fChannelOffset;} // Raw TDC time, minus TDC zero time, minus event time, minus channel offset, seconds
    int GetFineCount() const {return fFineCount;}
    double GetFineTime() const {return fFineTime;}
    double GetTimeOverThr() const {return fTimeOverThr;}
    bool IsMatchedToADC() const {return fMatchedToADC;}
    bool Is165Reflection() const {return fIs165Reflection;}
    bool IsGood() const {return !fIs165Reflection;}
    bool IsGoodForTOF() const {return fIsGoodForTOF;}

    inline void SetEndID(int n) {fEndID = n;}
    inline void SetTime(double t) {fTime = t;}
    inline void SetChannelOffset(double t) {fChannelOffset = t;}
    inline void SetFineCount(int c) {fFineCount = c;}
    inline void SetFineTime(double t) {fFineTime = t;}
    inline void SetTimeOverThr(double t) {fTimeOverThr = t;}
    inline void SetMatchedToADC(bool y) {fMatchedToADC = y;}
    inline void SetIs165Reflection(bool y) {fIs165Reflection = y;}
    inline void SetIsGoodForTOF(bool y) {fIsGoodForTOF = y;}

   ClassDef(TBarTDCHit, 6);
};

#endif
