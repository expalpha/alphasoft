#ifndef _TBARMVAVARS_
#define _TBARMVAVARS_

#include "TBarEvent.hh"
#include <map>
#include <string>

#define NUM_BV_VARS 107

class TBarMVAVars: public TObject
{
private:
   TBarEvent fBarEvt;
   std::map<std::string, float> fVars;
   static std::vector<std::string> fVarNames; // Declared in cxx file. Add new variables there.
   static std::vector<std::string> fUsedVarNames; // Declared in cxx file. Add new variables there.

public:
   TBarMVAVars(); // ctor
   TBarMVAVars(const TBarEvent& barEvt);
   using TObject::Print;
   virtual void Print();
   virtual ~TBarMVAVars(); // dtor

   void AddBarEvent(const TBarEvent& barEvt) { fBarEvt = barEvt;}
   void AddVar(std::string varName, float varVal);
   void Reset();
   void CalculateVars();
   void EnforceValueRange(float range_limit);

   static int GetNumVars() { return fVarNames.size(); }
   static std::vector<std::string> GetVarNames() { return fVarNames; }
   static std::string GetVarName(int i) { return fVarNames[i]; }

   float GetVar(std::string name) const { 
      if (fVars.count(name)>0) return fVars.at(name);
      else return 0;
   }
   static bool IsVarUsed(std::string name);

   float GetVar(int i) const { return fVars.at(GetVarName(i)); }
   std::map<std::string, float> GetVars() const { return fVars; }

   std::vector<float> GetTOFVals(bool global=false, bool tpc_matched=false, bool nonneighbour=false, bool diffcluster=false) const;
   std::vector<float> GetDistVals(bool tpc_matched=false) const;
   std::vector<float> GetTOFmDistVals(bool global=false, bool tpc_matched=false, bool nonneighbour=false, bool diffcluster=false) const;
   std::vector<float> GetTOFValsOpeningAngle(int min_opening_angle) const;
   std::vector<float> GetTOFmDistValsOpeningAngle(int min_opening_angle) const;
   std::vector<float> GetDZVals(bool tpc_matched=false) const;

   float GetMean(std::vector<float> v) const;
   float GetStdDev(std::vector<float> v) const;


   ClassDef(TBarMVAVars,3);
};

#endif
