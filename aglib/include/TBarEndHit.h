#ifndef _TBARENDHIT_
#define _TBARENDHIT_
#include "TObject.h"
#include "TMath.h"
#include <iostream>
#include "TBarADCHit.h"
#include "TBarTDCHit.h"

class TBarEndHit: public TObject
{
private:
   int fEndID;
   TBarADCHit fADCHit;
   TBarTDCHit fTDCHit;
   double fTW;

public:
   TBarEndHit(); // ctor
   TBarEndHit(const TBarADCHit& _fADCHit, const TBarTDCHit& _fTDCHit);
   using TObject::Print;
   virtual void Print();
   virtual ~TBarEndHit(); // dtor

   TBarEndHit(const TBarEndHit& h): TObject(h)
   {
      fEndID = h.fEndID;
      fADCHit = h.fADCHit;
      fTDCHit = h.fTDCHit;
      fTW = h.fTW;
   }

   TBarEndHit& operator=(const TBarEndHit& h)
   {
      fEndID = h.fEndID;
      fADCHit = h.fADCHit;
      fTDCHit = h.fTDCHit;
      fTW = h.fTW;
      return *this;
   }

   void SetADCHit(const TBarADCHit& _fADCHit) 
   {
      fEndID = _fADCHit.GetEndID();
      fADCHit=_fADCHit;
   }
   void SetTDCHit(const TBarTDCHit& _fTDCHit)
   {
      if (fEndID!=_fTDCHit.GetEndID()) {
         printf("TBarEndHit::SetTDCHit EndID does not match! Should never happen.\n");
         return;
      }
      fTDCHit = _fTDCHit;
   }
   inline void SetTimeWalkFromADC(double t) {fTW = t;}

   int GetEndID() const {return fEndID;}
   int GetBarID() const {return fEndID%64;}
   bool IsBottom() const {return (0<=fEndID and fEndID<64);}
   bool IsTop() const {return (64<=fEndID and fEndID<128);}
   double GetAmpRaw() const {return fADCHit.GetAmpRaw();}
   double GetAmpFit() const {return fADCHit.GetAmpFit();}
   double GetADCTime() const {return fADCHit.GetStartTime();}
   double GetTDCTimeRaw() const {return fTDCHit.GetTime(); }
   double GetTDCTimeCali() const {return fTDCHit.GetTimeCali(); }
   double GetTimeWalkFromADC() const {return fTW; }
   double GetTime() const {return fTDCHit.GetTimeCali() - fTW;}
   TBarADCHit& GetADCHit() {return fADCHit;}
   TBarTDCHit& GetTDCHit() {return fTDCHit;}
   const TBarADCHit& GetADCHit() const {return fADCHit;}
   const TBarTDCHit& GetTDCHit() const {return fTDCHit;}
   ClassDef(TBarEndHit, 8);
};

#endif
