
#include "TMath.h"
#include <vector>
#include "TVector3.h"
#include "TMatrixD.h"
#include "TMatrixDEigen.h"

// easier to copy this here than to figure out why aglib/TAGMVADumper.h is no longer finding the sphericity function in analib. Sorry. - G.S.

void sphericity(std::vector<double> x, std::vector<double> y, std::vector<double> z, int r, TVector3** axis, TVector3** values);
