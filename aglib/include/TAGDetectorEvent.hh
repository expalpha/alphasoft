#ifndef _TAGDETECTOREVENT_
#define _TAGDETECTOREVENT_
#ifdef BUILD_AG
#include "TObject.h"
#include "TVector3.h"
#include <iostream>
#include <algorithm>  

#include "TStoreEvent.hh"
#include "TBarEvent.hh"

// Compound mini container that holds data for "online" analysis
// TStoreEvent +  TBarEvent but on a diet for small root file size (therefore speed)!
class TAGDetectorEvent: public TObject
{
public:
   bool fHasBarFlow;
   bool fHasAnalysisFlow;
   
   
   int fRunNumber;
   uint32_t fSerialNumber;
   int fEventNo;
   double fRunTime; // Official time
   double fTPCTime; //TPC time stamp
   std::vector<bool> fCutsResult;
   TVector3 fVertex;
   int fVertexStatus;
   int fNumHelices;  // helices used for vertexing
   int fNumTracks; // reconstructed (good) helices
   int fNumADCBars;
   int fNumTDCBars;
   double fBarTime;
   double fBarMaxTOF;
   double fBarMaxDist;
   double fBarTOFstdv;
   double fBarMinTOFminusDist;
   double fMVA;
   TAGDetectorEvent();
   ~TAGDetectorEvent();
   // Construct with TStoreEvent
   TAGDetectorEvent( const TStoreEvent* e, const TBarEvent* b );
   bool AddAnalysisFlow(const TStoreEvent* event, const TBarEvent* b);
   // Online analysis cuts
   bool VertexGoodCut(const TStoreEvent* e) const;

   double X() const { return fVertex.X(); }
   double Y() const { return fVertex.Y(); }
   double Z() const { return fVertex.Z(); }

   bool NHelixCut(const TStoreEvent* e, int cut) const;
   bool NTrackCut(const TStoreEvent* e, int cut) const;

   bool RadiusCut(const TStoreEvent* e, const double R_2 = 85, const double R_2_plus = 80) const;
   bool BarMultiplicityCut(const TBarEvent* b, int cut = 3) const;
   bool LowTOFCut(const TBarEvent* b, double cut) const;
   bool TOFDistCut(const TBarEvent* b, double cut) const;
   bool MVABDTCut(const TStoreEvent* e, const double cut) const;

   int SimpleCountBarClusters(const std::vector<TBarHit>& bars) const;

   bool SimpleClusterCutComplete(const TBarEvent*, int cut) const;
   bool SimpleClusterCutHalfComplete(const TBarEvent*, int cut) const;
   bool ClusterCut(const TBarEvent*, int cut) const;
   
   std::vector<bool> ApplyCuts(const TStoreEvent* e, const TBarEvent* b) const
   {
      std::vector<bool> a;
      a.push_back(VertexGoodCut(e));
      //a.push_back(RadiusCut(e));
      //a.push_back(BarMultiplicityCut(b,3));
      //a.push_back(LowTOFCut(b,1.1E-9));
      //a.push_back(TOFDistCut(b,-0.5e-9));
      //a.push_back(SimpleClusterCutComplete(b,3));
      a.push_back(VertexGoodCut(e) and ClusterCut(b,4));
      a.push_back(VertexGoodCut(e) and MVABDTCut(e,0.1468));
      //a.push_back(VertexGoodCut(e) and MVABDTCut(e,0.1816));
      //a.push_back(VertexGoodCut(e) and MVABDTCut(e,0.2495));
      return a;
   }
   // Cut names as a member function to reduce bloat of written TObject
   std::vector<std::string> CutNames() const
   {
      std::vector<std::string> a;
      a.push_back("HasVertex");
      //a.push_back("Radius Cut");
      //a.push_back("Bar Multiplicity Cut >= 3");
      //std::string name = "TOF Cut ";
      //a.push_back(name + "< 1.1 ns");
      //a.push_back("TOF-Dist < -0.5 ns");
      //a.push_back("BarClusters >= 3");
      a.push_back("PassCut");
      a.push_back("OnlineMVA");
      //a.push_back("MVA-BDT LowBkg");
      //a.push_back("MVA-BDT UltraLowBkg");
      return a;
   }
   // Spill log:
   double GetTimeOfEvent() const { return fRunTime; }
   double GetRunTime() const { return fRunTime; }
   int GetEventNumber() const { return fEventNo; }
   int GetVertexStatus() const { return fVertexStatus; }
   bool GetOnlinePassCuts(size_t i) const
   {
      if (i < fCutsResult.size() )
         return fCutsResult.at(i);
      return false;
   }
   std::string GetOnlinePassCutsName(int i) const {return CutNames().at(i); }
   

   ClassDef(TAGDetectorEvent,3)
};

#endif
#endif
