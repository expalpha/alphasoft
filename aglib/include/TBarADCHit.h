#ifndef _TBARADCHIT_
#define _TBARADCHIT_
#include "TObject.h"
#include "TMath.h"
#include <iostream>
#include <array>

class TBarADCHit: public TObject
{
private:
    int fEndID=-1; // 0-63 bottom, 64-127 top
    bool fFlatTop=false; // Pulse exceeds maximum ADC, has flat top
    bool fFlatBottom=false; // Pulse exceeds minimum ADC, has flat bottom
    double fAmp=-1; // Raw amplitude
    double fAmpFit=-1; // With fitting, without channel calibration
    int fStartTime=-1; // adc_bin * 10, from 0 to 7000
    int fEndTime=-1; // adc_bin * 10, from 0 to 7000
    bool fStartsNegative=false; // Pulse begins by going negative. Induced noise on ADC cable.
    bool fMatchedToTDC=false;
    double fBaseline=-1;
    double fBaselineSigma=-1;
    double fChannelNoise=-1; // Calibrated noise level for this channel
    std::array<double, 5> fFitParams;

public:
   TBarADCHit(); // ctor
   using TObject::Print;
   virtual void Print();
   virtual ~TBarADCHit(); // dtor

   TBarADCHit& operator=(const TBarADCHit& h)
   {
      fEndID = h.fEndID;
      fAmp = h.fAmp;
      fAmpFit = h.fAmpFit;
      fFlatTop =  h.fFlatTop;
      fFlatBottom = h.fFlatBottom;
      fStartTime = h.fStartTime;
      fEndTime = h.fEndTime;
      fStartsNegative = h.fStartsNegative;
      fMatchedToTDC = h.fMatchedToTDC;
      fChannelNoise = h.fChannelNoise;
      fFitParams = h.fFitParams;
      return *this;
   }

    int GetEndID() const {return fEndID;}
    int GetBarID() const {return fEndID%64;}
    bool IsBottom() const {return (0<=fEndID and fEndID<64);}
    bool IsTop() const {return (64<=fEndID and fEndID<128);}
    int GetPeakTime() const {return int((fEndTime+fStartTime)/2);}
    int GetStartTime() const {return fStartTime;} // ns
    int GetEndTime() const {return fEndTime;} // ns
    int GetTimeOverThreshold() const {return fEndTime-fStartTime;} // ns
    bool IsFlatTop() const {return fFlatTop;}
    bool IsFlatBottom() const {return fFlatBottom;}
    double GetAmpRaw() const {return fAmp;}
    double GetAmpFit() const {return fAmpFit;}
    bool GetStartsNegative() const {return fStartsNegative;}
    bool IsMatchedToTDC() const {return fMatchedToTDC;}
    double GetBaseline() const {return fBaseline;}
    double GetBaselineSigma() const {return fBaselineSigma;}
    double GetChannelNoise() const {return fChannelNoise;}
    bool IsGood() const {return fAmp>fChannelNoise;}
    double GetFitParam(int i) const {
        if (i<0 or i>4) {return -1;}
        return fFitParams[i];
    }

    inline void SetEndID(int n) {fEndID = n;}
    inline void SetStartTime(int t) {fStartTime = t;}
    inline void SetEndTime(int t) {fEndTime = t;}
    inline void SetFlatTop(bool y) {fFlatTop = y;}
    inline void SetFlatBottom(bool y) {fFlatBottom = y;}
    inline void SetAmp(double a) {fAmp = a;}
    inline void SetAmpFit(double a) {fAmpFit = a;}
    inline void SetStartsNegative(bool y) {fStartsNegative = y;}
    inline void SetMatchedToTDC(bool y) {fMatchedToTDC = y;}
    inline void SetBaseline(double b) {fBaseline = b;}
    inline void SetBaselineSigma(double s) {fBaselineSigma = s;}
    inline void SetChannelNoise(double t) {fChannelNoise = t;}
    inline void SetFitParam(int i, double p) {
        if (i<0 or i>4) {return;}
        fFitParams[i] = p;
    }


    ClassDef(TBarADCHit, 5);
};

#endif
