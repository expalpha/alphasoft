#ifndef _TBARHIT_
#define _TBARHIT_

#include "TBarEndHit.h"
#include "TVector3.h"
#include "TPCconstants.hh"

class TBarHit: public TObject
{
private:
  int fBarID;
  TBarEndHit fTopEndHit;
  TBarEndHit fBotEndHit;
  TBarADCHit fTopADCHit;
  TBarADCHit fBotADCHit;
  TBarTDCHit fTopTDCHit;
  TBarTDCHit fBotTDCHit;
  TVector3 fTPCHit; // in m
  double fZed=-9999;
  double fZedADC=-9999;
  double fTime=0;
  int fHitType;
  bool fHasZed;
  bool fHasTime;
  bool fHasTopEndHit;
  bool fHasBotEndHit;
  bool fHasTopADCHit;
  bool fHasBotADCHit;
  bool fHasTopTDCHit;
  bool fHasBotTDCHit;
  bool fHasTPCHit;
  int fTPCTrackNum=-1;
  int fClusterNum=-1;
  bool fIsInMainGroup;
  bool fIsGoodForTOF;

public:
  TBarHit(); // ctor
  TBarHit(const TBarEndHit& tophit, const TBarEndHit& bothit);
  TBarHit(const TBarEndHit& endhit, const TBarTDCHit& tdchit);
  TBarHit(const TBarEndHit& endhit, const TBarADCHit& adchit);
  TBarHit(const TBarTDCHit& tophit, const TBarTDCHit& bothit);
  TBarHit(const TBarTDCHit& tdchit, const TBarADCHit& adchit);
  TBarHit(const TBarADCHit& tophit, const TBarADCHit& bothit);
  TBarHit(const TBarEndHit& hit);
  TBarHit(const TBarTDCHit& hit);
  TBarHit(const TBarADCHit& hit);
  TBarHit(const TBarHit &h);
  using TObject::Print;
  virtual void Print();
  virtual ~TBarHit(); // dtor

  void SetBotEndHit(TBarEndHit& _fBotEndHit) { fBotEndHit = _fBotEndHit; fHasBotEndHit = true; }
  void SetTopEndHit(TBarEndHit& _fTopEndHit) { fTopEndHit = _fTopEndHit; fHasTopEndHit = true; }
  void SetBotADCHit(TBarADCHit& _fBotADCHit) { fBotADCHit = _fBotADCHit; fHasBotADCHit = true; }
  void SetTopADCHit(TBarADCHit& _fTopADCHit) { fTopADCHit = _fTopADCHit; fHasTopADCHit = true; }
  void SetBotTDCHit(TBarTDCHit& _fBotTDCHit) { fBotTDCHit = _fBotTDCHit; fHasBotTDCHit = true; }
  void SetTopTDCHit(TBarTDCHit& _fTopTDCHit) { fTopTDCHit = _fTopTDCHit; fHasTopTDCHit = true; }
  void SetBarID(const int _fBarID) { fBarID=_fBarID; }
  void SetHitType(const int _fHitType) { fHitType = _fHitType; }
  void SetZed(const double _fZed) { fZed=_fZed; fHasZed = true; }
  void SetADCZed(const double _fZedADC) { fZedADC=_fZedADC; }
  void SetTime(const double _fTime) { fTime=_fTime; fHasTime = true; }
  void SetIsInMainGroup(const bool y) {fIsInMainGroup = y;}
  void SetIsGoodForTOF(const bool y) {fIsGoodForTOF = y;}
  void SetTPCHit(const TVector3 _fTPCHit) {
     fTPCHit=_fTPCHit;
     fHasTPCHit=true;
  }
  void SetTPCTrackNum(int tr) {fTPCTrackNum=tr;}
  void SetClusterNum(int c) {fClusterNum=c;}
  
  TBarEndHit& GetTopEndHit() {return fTopEndHit;}
  TBarEndHit& GetBotEndHit() {return fBotEndHit;}
  TBarADCHit& GetTopADCHit() {return fTopADCHit;}
  TBarADCHit& GetBotADCHit() {return fBotADCHit;}
  TBarTDCHit& GetTopTDCHit() {return fTopTDCHit;}
  TBarTDCHit& GetBotTDCHit() {return fBotTDCHit;}
  const TBarEndHit& GetTopEndHit() const {return fTopEndHit;}
  const TBarEndHit& GetBotEndHit() const {return fBotEndHit;}
  const TBarADCHit& GetTopADCHit() const {return fTopADCHit;}
  const TBarADCHit& GetBotADCHit() const {return fBotADCHit;}
  const TBarTDCHit& GetTopTDCHit() const {return fTopTDCHit;}
  const TBarTDCHit& GetBotTDCHit() const {return fBotTDCHit;}
  bool HasTopEndHit() const {return fHasTopEndHit;}
  bool HasBotEndHit() const {return fHasBotEndHit;}
  bool HasTopADCHit() const {return fHasTopADCHit;}
  bool HasBotADCHit() const {return fHasBotADCHit;}
  bool HasTopTDCHit() const {return fHasTopTDCHit;}
  bool HasBotTDCHit() const {return fHasBotTDCHit;}
  const TVector3 GetTPCHit() const {return fTPCHit;}
  int GetTPCTrackNum() const {return fTPCTrackNum;}
  int GetClusterNum() const {return fClusterNum;}
  double GetZedTPC() const {return fTPCHit.z();}
  bool HasTPCHit() const {return fHasTPCHit;}
  int GetHitType() const {return fHitType;}
  int GetBarID() const {return fBarID;}
  double GetZed() const {return fZed;}
  double GetADCZed() const { return fZedADC;}
  double GetTime() const {return fTime;}
  bool HasZed() const {return fHasZed;}
  bool fIsNan() const {return std::isnan(fZed);}
  bool HasTime() const {return fHasTime;}
  bool IsComplete() const {return fHitType==1;}
  bool IsHalfComplete() const {return (0<fHitType and fHitType<=4);}
  bool IsInMainGroup() const {return fIsInMainGroup;}
  bool IsGoodForTOF() const {return fIsGoodForTOF;}
  
  double GetCombinedAmplitude() const
  {
     if (fHasTopADCHit and fHasBotADCHit) {return TMath::Log(fTopADCHit.GetAmpFit()) + TMath::Log(fBotADCHit.GetAmpFit());}
     else {return -1;}
  }

  double GetPhi() const
  {
     double offset_angle=3.414895; // Radians
     double theta=fBarID*2.*TMath::Pi()/64;
     return theta+offset_angle;
  }
  void GetXY(double &x, double &y) const
  {
      double r=(.223+.243)/2.;
      double offset_angle=3.414895; // Radians
      double theta=fBarID*2.*TMath::Pi()/64;
      x=r*TMath::Cos(theta + offset_angle);
      y=r*TMath::Sin(theta + offset_angle);
      return;
   }
   const TVector3 Get3Vector() const
   {
      double x,y;
      this->GetXY(x,y);
      double z = ALPHAg::kLargeNegativeUnknown;
      if (fHasZed) z = fZed;
      TVector3 bv_point = TVector3(x,y,z);
      return bv_point;
   }
   const TVector3 Get3VectorZTPC() const
   {
      double x,y;
      this->GetXY(x,y);
      double z = ALPHAg::kLargeNegativeUnknown;
      if (fHasTPCHit) z = fTPCHit.z();
      TVector3 point = TVector3(x,y,z);
      return point;
   }

  ClassDef(TBarHit,10);
};


#endif
