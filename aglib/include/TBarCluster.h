#ifndef _TBARCLUSTER_
#define _TBARCLUSTER_

#include "TBarHit.h"
#include "TVector3.h"

class TBarCluster: public TObject
{
private:
   std::vector<TBarHit> fBarHits;

public:
   TBarCluster(); // ctor
   TBarCluster(const TBarCluster &h);
   using TObject::Print;
   virtual void Print();
   virtual ~TBarCluster(); // dtor

   void AddBarHit(TBarHit& h) {fBarHits.push_back(h);}
   std::vector<TBarHit>& GetBarHits() {return fBarHits;}
   const std::vector<TBarHit>& GetBarHits() const {return fBarHits;}
  
   int GetNumHits() {return fBarHits.size();}
   int GetNumHitsComplete();
   int GetNumHitsHalfComplete();

   TBarHit& GetHit(int i) {return fBarHits.at(i);}
   void GetHitComplete(int i, TBarHit &h);
   void GetHitHalfComplete(int i, TBarHit &h);

   int GetStartBarID();
   int GetEndBarID();
   int GetBarIDBefore(int n_before=1);
   int GetBarIDAfter(int n_after=1);
   bool BarIDIsInside(int barID) const;
  
   double GetStartZ();
   double GetEndZ();
   double GetAverageTime();

  ClassDef(TBarCluster,1);
};


#endif
