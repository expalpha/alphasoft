#ifdef BUILD_AG
#ifndef __TBAREVENT__
#define __TBAREVENT__ 1

#include <iostream>
#include <vector>
#include "assert.h"
#include "TMath.h"
#include <TVector3.h>
#include <map>
#ifndef ROOT_TObject
#include "TObject.h"
#endif
#include "TBarEndHit.h"
#include "TBarHit.h"
#include "TBarADCHit.h"
#include "TBarTDCHit.h"
#include "TBarCluster.h"
#include "TBarTOF.h"


class TBarEvent: public TObject
{
private:
  int fEventID;
  double fEventTime;
  double fEventTDCTime;
  int fNMatchedTracks;
  int fNUnmatchedTracks;
  int fNPileUpTracks;
  std::vector<TBarHit> fBarHit;
  std::vector<TBarEndHit> fEndHit;
  std::vector<TBarADCHit> fADCHit;
  std::vector<TBarTDCHit> fTDCHit;
  std::vector<TBarCluster> fClusters;
  std::vector<TBarTOF> fTOF;

public:
  TBarEvent(); //ctor
  TBarEvent(TBarEvent &barEvent); //copy ctor
  TBarEvent(const TBarEvent &barEvent); //const copy ctor
  using TObject::Print;
  virtual void Print();
  virtual ~TBarEvent(); //dtor
  
  inline void SetID(int ID){ fEventID=ID;}
  inline void SetRunTime(double time){ fEventTime=time;}
  inline void SetEventTDCTime(double t) {fEventTDCTime = t;}

  int GetID() const { return fEventID;}
  double GetRunTime() const { return fEventTime;}
  double GetEventTDCTime() const {return fEventTDCTime;}

  inline int GetNumberOfMatchedTracks() const {return fNMatchedTracks;}
  inline void IncrementNumberOfMatchedTracks() {fNMatchedTracks++;}
  inline int GetNumberOfUnmatchedTracks() const {return fNUnmatchedTracks;}
  inline void IncrementNumberOfUnmatchedTracks() {fNUnmatchedTracks++;}
  inline int GetNumberOfPileUpTracks() const {return fNPileUpTracks;}
  inline void IncrementNumberOfPileUpTracks() {fNPileUpTracks++;}

  void Reset()
  {
    fEventID=-1;
    fEventTime=-1.;
    fEndHit.clear();
    fBarHit.clear();
    fADCHit.clear();
    fTDCHit.clear();
    fClusters.clear();
    fTOF.clear();
  }
  void ResetTDCHits() {fTDCHit.clear();}
  void ResetBarHits() {fBarHit.clear();}
  void AddEndHit(const TBarEndHit& e) {fEndHit.emplace_back(e);}
  void AddBarHit(const TBarHit& b) {fBarHit.emplace_back(b);}
  void AddADCHit(const TBarADCHit& a) {fADCHit.emplace_back(a);}
  void AddTDCHit(const TBarTDCHit& t) {fTDCHit.emplace_back(t);}
  void AddCluster(const TBarCluster& c) {fClusters.emplace_back(c);}
  void AddTOF(const TBarTOF& t) {fTOF.emplace_back(t);}

  void FindMainGroup();

  int GetNumBars() const { return fBarHit.size(); }
  int GetNumBarsComplete() const;
  int GetNumBarsHalfComplete() const;
  int GetNumMatchedBars() const;
  int GetNumMatchedBarsComplete() const;
  int GetNumMatchedBarsHalfComplete() const;

  int GetNumEnds() const { return fEndHit.size(); }
  int GetNumADC() const { return fADCHit.size(); }
  int GetNumGoodADC() const;
  int GetNumTDC() const { return fTDCHit.size(); }
  int GetNumGoodTDC() const;
  int GetNumClusters() const { return fClusters.size();}
  int GetNumTOF() const { return fTOF.size();}
  int GetNumGoodTOF() const;

  std::vector<TBarHit>& GetBarHits() { return fBarHit; }
  const std::vector<TBarHit>& GetBarHits() const { return fBarHit; }
  std::vector<TBarEndHit>& GetEndHits() { return fEndHit; }
  const std::vector<TBarEndHit>& GetEndHits() const { return fEndHit; }
  std::vector<TBarADCHit>& GetADCHits() {return fADCHit;}
  const std::vector<TBarADCHit>& GetADCHits() const {return fADCHit;}
  std::vector<TBarTDCHit>& GetTDCHits() {return fTDCHit;}
  const std::vector<TBarTDCHit>& GetTDCHits() const {return fTDCHit;}
  std::vector<TBarCluster>& GetClusters() {return fClusters;}
  const std::vector<TBarCluster>& GetClusters() const {return fClusters;}
  std::vector<TBarTOF>& GetTOFs() {return fTOF;}
  const std::vector<TBarTOF>& GetTOFs() const {return fTOF;}

  bool IsInjection() const;
  bool IsPileUp() const;

  ClassDef(TBarEvent, 8);
};


#endif
#endif
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
