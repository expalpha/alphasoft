#ifdef BUILD_AG
#ifndef __TBARCALIFILE__
#define __TBARCALIFILE__ 1
#include "TObject.h"
#include "TMath.h"
#include <iostream>
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include <string>
#include <fstream>
#include <sstream>


class TBarCaliFile: public TObject
{
private:
    int run_num_request;
    int run_num_used;
    std::string filename_used;
    bool loaded;
    const int static num_bars = 64;
    const int static num_ends = num_bars*2;
    const int static max_age = 100;
    std::array<double,num_bars> global_offset, tw_top, tw_bot; 
    std::array<double,num_bars> bot_top_offset, bot_top_offset_err, veff, veff_err;
    std::array<std::array<double,num_bars>,num_bars> bar_to_bar_offset, bar_to_bar_sigma;
    std::array<double,num_ends> adc_noise_level, adc_lambda, adc_zed_offset;

public:

    TBarCaliFile(); // ctor
    using TObject::Print;
    virtual void Print();
    virtual ~TBarCaliFile(); // dtor

    TBarCaliFile(int run_num); // alternate constructor
    
    int LoadCali(int run_num, bool verbose=false);
    int LoadCaliFile(std::string filename, bool verbose=true);
    void Reset();

    int GetRunNumRequest() const {return run_num_request;}
    int GetRunNumUsed() const {return run_num_used;}
    double GetGlobalOffset(int bar) const {return global_offset[bar];}
    double GetTWTop(int bar) const {return tw_top[bar];}
    double GetTWBot(int bar) const {return tw_bot[bar];}
    double GetBotTopOffset(int bar) const {return bot_top_offset[bar];}
    double GetBotTopOffsetErorr(int bar) const {return bot_top_offset_err[bar];}
    double GetVEff(int bar) const {return veff[bar];}
    double GetVEffError(int bar) const {return veff_err[bar];}
    double GetBarToBarOffset(int bar_i, int bar_j) const {return bar_to_bar_offset[bar_i][bar_j];}
    double GetBarToBarSigma(int bar_i, int bar_j) const {return bar_to_bar_sigma[bar_i][bar_j];}
    bool IsLoaded() const {return loaded;}
    double GetEndOffset(int end) const {
        int bar = end%num_bars;
        bool top = (bool)TMath::Floor(end/num_bars);
        return (top) ? global_offset[bar] + bot_top_offset[bar]/2. : global_offset[bar] - bot_top_offset[bar]/2.;
    }
    double GetADCNoiseLevel(int end) const {return adc_noise_level[end];}
    double GetADCLambda(int end) const {return adc_lambda[end];}
    double GetADCZedOffset(int end) const {return adc_zed_offset[end];}

    ClassDef(TBarCaliFile, 1);

};

#endif
#endif
