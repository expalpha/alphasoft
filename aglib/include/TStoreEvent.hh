// Store event class definition
// for ALPHA-g TPC AGTPCanalysis
// Stores essential information from TEvent
// Authors: A. Capra, M. Mathers
// Date: April 2017
#ifdef BUILD_AG
#ifndef __TSTOREEVENT__
#define __TSTOREEVENT__ 1

#include <TObject.h>
#include <TObjArray.h>
#include "TFitLine.hh"
#include "TFitHelix.hh"
#include "TFitVertex.hh"
#include "TStoreHelix.hh"
#include "TStoreLine.hh"
#include "TSpacePoint.hh"
#include <TClonesArray.h>
#include <TVector3.h>
#include "TBarEvent.hh"
#include <iomanip>

#include "TSpacePoint.hh"
#include "TSimEvent.hh"

class TStoreEvent: public TObject
{
private:
  int fRunNumber;
  int fID;
  uint32_t fSerialNumber;
  double fEventTime;

  int fNpoints;
  int fNusedpoints;
  int fNtracks;

  int fNpadSigs;
  int fNawSigs;

  int fNumAWHits;
  int fNumADCHits;
  int fNumPADHits;
  int fNumSPHits;

  TObjArray fStoreHelixArray;
  TObjArray fStoreLineArray;
  TObjArray fSpacePoints;

  TFitLine fTrivialLine;
  TFitHelix fTrivialHelix;

  TFitLine fUsedTrivialLine;
  TFitHelix fUsedTrivialHelix;

  TObjArray fUsedHelices;

  TVector3 fVertex;
  vertexstatus_t fVertexStatus;
  double fVertexChi2;
  double fDCA;

  double fPattRecEff;

  double fMVA;

  TSimEvent* fSimEvent = NULL;

  // Maybe we dont need the whole TBarEvent?
  TBarEvent* fBarEvent = NULL;
  //std::vector<TBarHit*> fBarHit; //Barrel hits

public:
  TStoreEvent();
  TStoreEvent(const TStoreEvent&);
  virtual ~TStoreEvent();  // destructor

  TStoreEvent& operator=(const TStoreEvent&);
  void SetEvent(const std::vector<TSpacePoint>& points, const std::vector<TFitLine>& lines,
                const std::vector<TFitHelix>& helices);
  // Maybe we dont need the whole TBarEvent?
  void AddBarEvent(TBarEvent* b)
  {
    //fBarTime = b->GetRunTime();
    //fBarHit  = b->GetBars();
    fBarEvent = b;
  }

  inline int GetEventNumber() const {return fID;}
  inline void SetEventNumber(int n) {fID = n;}

  inline uint32_t GetSerialNumber() const {return fSerialNumber;}
  inline void SetSerialNumber(uint32_t n) {fSerialNumber = n;}

  inline double GetMVAValue() const {return fMVA;}
  inline void SetMVAValue(double mva) {fMVA = mva;}

  inline double GetDCA() const {return fDCA;}
  inline void SetDCA(double dca) {fDCA = dca;}

  inline int GetRunNumber() const { return fRunNumber; }
  inline void SetRunNumber(const int n) { fRunNumber = n; }

  inline int GetNumAWHits() const {return fNumAWHits;}
  inline void SetNumAWHits(int num) {fNumAWHits = num;}

  inline int GetNumADCHits() const {return fNumADCHits;}
  inline void SetNumADCHits(int num) {fNumADCHits = num;}

  inline int GetNumPADHits() const {return fNumPADHits;}
  inline void SetNumPADHits(int num) {fNumPADHits = num;}

  inline int GetNumSPHits() const {return fNumSPHits;}
  inline void SetNumSPHits(int num) {fNumSPHits = num;}

  inline TFitLine GetTrivialLine() const {return fTrivialLine;}
  inline void SetTrivialLine(TFitLine line) {fTrivialLine = line;}
  inline TFitHelix GetTrivialHelix() const {return fTrivialHelix;}
  inline void SetTrivialHelix(TFitHelix helix) {fTrivialHelix = helix;}

  inline TFitLine GetUsedTrivialLine() const {return fUsedTrivialLine;}
  inline void SetUsedTrivialLine(TFitLine line) {fUsedTrivialLine = line;}
  inline TFitHelix GetUsedTrivialHelix() const {return fUsedTrivialHelix;}
  inline void SetUsedTrivialHelix(TFitHelix helix) {fUsedTrivialHelix = helix;}

  inline double GetTimeOfEvent() const {return fEventTime;}
  inline double GetRunTime() const {return fEventTime;} // To be consistent with TAGDetectorEvent, TBarEvent, TChronoEvent
  inline void SetTimeOfEvent(double t) {fEventTime = t;}
  inline double GetBarTime() const {
    if (!fBarEvent)
       return -1;
    else
       return fBarEvent->GetRunTime();
  }

  const TSimEvent* GetSimEvent() const { return fSimEvent; }
  inline void AddSimEvent(TSimEvent* s) {
    if (fSimEvent) delete fSimEvent; // Should never come up buts lets preemptively prevent any leaks
    fSimEvent = s;
  } 

  const TBarEvent* GetBarEvent() const { return fBarEvent; }
  TBarEvent* GetBarEvent() { return fBarEvent; }
  inline int GetNumberOfPoints() const {return fNpoints;}
  inline int GetNumberOfUsedPoints() const {return fNusedpoints;}
  inline void SetNumberOfPoints(int Npoints) {fNpoints = Npoints;}
  inline void SetNumberOfUsedPoints(int Nusedpoints) {fNusedpoints = Nusedpoints;}
  inline int GetNumberOfTracks() const {return fNtracks;}
  inline void SetNumberOfTracks(int Ntrk) {fNtracks = Ntrk;}
  inline int GetNumberOfPadSigs() const {return fNpadSigs;}
  inline void SetNumberOfPadSigs(int NpadSigs) {fNpadSigs = NpadSigs;}
  inline int GetNumberOfAwSigs() const {return fNawSigs;}
  inline void SetNumberOfAwSigs(int NawSigs) {fNawSigs = NawSigs;}
  int GetNumberOfGoodHelices() const {
    int n = 0;
    for (int i=0; i<fNtracks; i++) {
      TStoreHelix* h = (TStoreHelix*) fStoreHelixArray.At(i);
      if (h) {
        if (h->GetStatus()>0) {
          n++;
        }
      }
    }
    return n;
  }

  double GetMeanZSigma() const
  {
    double mean=0;
    int nhelix=0;
    for (int i=0; i<fNtracks; i++)
    {
      TStoreHelix* h=(TStoreHelix*)fStoreHelixArray.At(i);
      if (h)
      {
        if (h->GetStatus() >0 )
        {
          mean+=h->GetZchi2();
          nhelix++;
        }
      }
    }
    if (nhelix) mean=mean/(double)nhelix;
    return mean;
  }
  double GetMeanRSigma() const
  {
    double mean=0;
    int nhelix=0;
    for (int i=0; i<fNtracks; i++)
    {
      TStoreHelix* h=(TStoreHelix*)fStoreHelixArray.At(i);
      if (h)
      {

        if (h->GetStatus() >0 )
        {
          mean+=h->GetRchi2();
          nhelix++;
        }
      }
    }
    if (nhelix) mean=mean/(double)nhelix;
    return mean;
  }

  inline const TObjArray* GetHelixArray() const {return &fStoreHelixArray;}
  inline const TObjArray* GetLineArray() const {return &fStoreLineArray;}
  //  inline const TObjArray* GetTracksArray() const {return &fStoredTracks;}

  inline const TObjArray* GetUsedHelices()       const {return &fUsedHelices;}
  inline void SetUsedHelices(const std::vector<TFitHelix*>* a)
  {
    for (TFitHelix* h: * a)
    {
      fUsedHelices.AddLast( new TStoreHelix(h, h->GetPointsArray()));
    }
  }

  int AddLine(const TFitLine* l);
  int AddHelix(const TFitHelix* h);

  inline const TObjArray* GetSpacePoints() const { return &fSpacePoints; }

  inline void SetVertex(TVector3 vtx)     {fVertex = vtx; }
  inline const TVector3 GetVertex() const      {return fVertex;}
  inline double GetVertexX() const        { return fVertex.X(); }
  inline double GetVertexY() const        { return fVertex.Y(); }
  inline double GetVertexZ() const        { return fVertex.Z(); }

  inline void SetVertexChi2(double chi2)  {fVertexChi2 = chi2; }
  inline double GetVertexChi2() const        { return fVertexChi2; }

  inline void SetVertexStatus(vertexstatus_t status)  {fVertexStatus = status; }
  inline vertexstatus_t GetVertexStatus() const       {return fVertexStatus;}

  inline double GetNumberOfPointsPerTrack() const {return fPattRecEff;}

  /*void AddBarrelHits(TBarEvent* b)
  {

  }*/
  Int_t GetBarMultiplicity() const {
    if (!fBarEvent)
       return -1;
    else
       return fBarEvent->GetNumBars();
  }

  virtual void Print(Option_t *option="") const;
  virtual void Reset();

  ClassDef(TStoreEvent,11)
};

#endif
#endif
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
