#ifdef BUILD_AG
#ifndef __TSIMEVENT__
#define __TSIMEVENT__ 1


#include <iostream>
#include <vector>
#include "assert.h"
#include "TMath.h"
#include <TVector3.h>
#include <map>
#ifndef ROOT_TObject
#include "TObject.h"
#endif

class TSimEvent: public TObject
{
private:
  int fEventID;
  TVector3 fVertex;

public:
  TSimEvent(); //ctor
  TSimEvent(TSimEvent &simEvent); //copy ctor
  TSimEvent(const TSimEvent &simEvent); //const copy ctor
  using TObject::Print;
  virtual void Print();
  virtual ~TSimEvent(); //dtor
  
  inline void SetID(const int ID){ fEventID=ID;}
  inline void SetVertex(const TVector3 &v) { fVertex = v; }
  inline void SetVertexX(const double x) { fVertex.SetX(x); }
  inline void SetVertexY(const double y) { fVertex.SetY(y); }
  inline void SetVertexZ(const double z) { fVertex.SetZ(z); }

  int GetID() const { return fEventID;}
  TVector3 GetVertex() const { return fVertex; }
  double GetVertexX() const { return fVertex.x(); }
  double GetVertexY() const { return fVertex.y(); }
  double GetVertexZ() const { return fVertex.z(); }


  void Reset()
  {
    fEventID=-1;
    fVertex.SetXYZ(-999,-999,-999);
  }

  ClassDef(TSimEvent, 1);
};


#endif
#endif
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
