#ifdef BUILD_AG
#include "TSimEvent.hh"




ClassImp(TSimEvent)

TSimEvent::TSimEvent()
{
// ctor
  fEventID=0;
  fVertex=TVector3(-999,-999,-999);
}

TSimEvent::TSimEvent(const TSimEvent &simEvt): TObject(simEvt)
{
// copy ctor
  fEventID = simEvt.GetID();
  fVertex = simEvt.GetVertex();
}


TSimEvent::~TSimEvent()
{

}

void TSimEvent::Print()
{
   std::cout << "TSimEvent::Print() -------------"<<std::endl;
   std::cout <<"Event ID:\t"  << fEventID    <<std::endl;
   std::cout << "End of TSimEvent::Print() ------"<<std::endl;
}

#endif
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
