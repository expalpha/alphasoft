#include "TBarADCHit.h"
ClassImp(TBarADCHit)

TBarADCHit::TBarADCHit()
{
   fEndID=-1;
   fStartTime=-1;
   fEndTime=-1;
   fFlatTop=false;
   fFlatBottom=false;
   fAmp=-1;
   fAmpFit=-1;
   fChannelNoise=-1;
   fStartsNegative=-1;
   fMatchedToTDC=false;
}

void TBarADCHit::Print()
{
  std::cout<<"-----TBarADCHit::Print()-----"<<std::endl;
  std::cout<<"Bar ID: "<<fEndID<<std::endl;
  std::cout<<"FlatTop? "<<fFlatTop<<" Amplitude: "<<fAmp<<std::endl;
  std::cout<<"-------TBarADCHit done-------"<<std::endl;
}

TBarADCHit::~TBarADCHit()
{
// dtor
}
