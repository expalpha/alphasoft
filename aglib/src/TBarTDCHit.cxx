#include "TBarTDCHit.h"
ClassImp(TBarTDCHit)

TBarTDCHit::TBarTDCHit()
{
   fEndID=-1;
    fTime=-1; // TDC time from coarse, fine, epoch counters
    fChannelOffset=-1; // TDC time after offset calibration
    fFineCount=-1; // Fine counter
    fFineTime=-1; // TDC time from fine counter
    fTimeOverThr=-1; // TDC time between leading and falling edges, no calibration
    fMatchedToADC=false;
    fIsGoodForTOF=false;
}

void TBarTDCHit::Print()
{
  std::cout<<"-----TBarTDCHit::Print()-----"<<std::endl;
  std::cout<<"Bar ID: "<<fEndID<<std::endl;
  std::cout<<"Time: "<<fTime<<" Calibrated time: "<<fTime-fChannelOffset<<std::endl;
  std::cout<<"-------TBarTDCHit done-------"<<std::endl;
}

TBarTDCHit::~TBarTDCHit()
{
// dtor
}
