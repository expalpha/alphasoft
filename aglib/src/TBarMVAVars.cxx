
#include "TBarMVAVars.h"
#include "TPCconstants.hh"

ClassImp(TBarMVAVars)

TBarMVAVars::TBarMVAVars()
{
}

TBarMVAVars::TBarMVAVars(const TBarEvent& barEvt)
{
  AddBarEvent(barEvt);
}

std::vector<std::string> TBarMVAVars::fVarNames = {"numBVBars","numBVEnds","numADC","numTDC","numGoodADC","numGoodTDC","numClusters",
                                                  "numTOF","numGoodTOF","minBarTOF","maxBarTOF","meanBarTOF","stddevBarTOF","minGlobalTOF","maxGlobalTOF","meanGlobalTOF",
                                                  "stddevGlobalTOF","minBarTOFmDist","maxBarTOFmDist","meanBarTOFmDist","stddevBarTOFmDist","minGlobalTOFmDist","maxGlobalTOFmDist",
                                                  "meanGlobalTOFmDist","stddevGlobalTOFmDist","minBarTOFMatched","maxBarTOFMatched","meanBarTOFMatched","stddevBarTOFMatched",
                                                  "minGlobalTOFMatched","maxGlobalTOFMatched","meanGlobalTOFMatched","stddevGlobalTOFMatched","minBarTOFmDistMatched",
                                                  "maxBarTOFmDistMatched","meanBarTOFmDistMatched","stddevBarTOFmDistMatched","minGlobalTOFmDistMatched","maxGlobalTOFmDistMatched",
                                                  "meanGlobalTOFmDistMatched","stddevGlobalTOFmDistMatched","minDist","maxDist","meanDist","stddevDist","minDistMatched",
                                                  "maxDistMatched","meanDistMatched","stddevDistMatched","minBarTOFNN","minBarTOFDCL","minGlobalTOFNN","minGlobalTOFDCL",
                                                  "minBarTOFmDistNN","minBarTOFmDistDCL","minGlobalTOFmDistNN","minGlobalTOFmDistDCL","directionTheta","directionality",
                                                  "directionalityZ","largestBarTOFDifference","largestGlobalTOFDifference","largestGlobalTOFDifferenceMatched",
                                                  "minAmp","maxAmp","stddevAmp","meanAmp","minDZ","maxDZ","meanDZ","stddevDZ",
                                                  "minDZ_matched","maxDZ_matched","meanDZ_matched","stddevDZ_matched",
                                                  "minTOFmDist8","maxTOFmDist8","meanTOFmDist8","stddevTOFmDist8","minTOFmDist16","maxTOFmDist16","meanTOFmDist16","stddevTOFmDist16",
                                                  "minTOFmDist4","maxTOFmDist4","meanTOFmDist4","stddevTOFmDist4","minTOFmDist12","maxTOFmDist12","meanTOFmDist12","stddevTOFmDist12",
                                                  "minTOF8","maxTOF8","meanTOF8","stddevTOF8","minTOF16","maxTOF16","meanTOF16","stddevTOF16",
                                                  "minTOF4","maxTOF4","meanTOF4","stddevTOF4","minTOF12","maxTOF12","meanTOF12","stddevTOF12"};
/*
std::vector<std::string> TBarMVAVars::fUsedVarNames = {"numGoodADC","numGoodTDC","numTOF","numGoodTOF",
                                                      "minTOFbar", "maxTOFbar", "stddevTOFbar","minTOFglobal", "maxTOFglobal", "stddevTOFglobal",
                                                      "minTOFmDistbar", "maxTOFmDistbar", "stddevTOFmDistbar", "minTOFmDistglobal", "maxTOFmDistglobal",
                                                      "minTOFbarNN", "minTOFglobalNN", "minTOFmDistbarNN", "minTOFmDistglobalNN",
                                                      "directionTheta","directionality","zdirectionality"};
*/
std::vector<std::string> TBarMVAVars::fUsedVarNames = {"numBVBars","numBVEnds","numADC","numTDC","numGoodADC","numGoodTDC","numClusters",
                                                  "numTOF","numGoodTOF","minBarTOF","maxBarTOF","meanBarTOF","stddevBarTOF","minGlobalTOF","maxGlobalTOF","meanGlobalTOF",
                                                  "stddevGlobalTOF","minBarTOFmDist","maxBarTOFmDist","meanBarTOFmDist","stddevBarTOFmDist","minGlobalTOFmDist","maxGlobalTOFmDist",
                                                  "meanGlobalTOFmDist","stddevGlobalTOFmDist","minBarTOFMatched","maxBarTOFMatched","meanBarTOFMatched","stddevBarTOFMatched",
                                                  "minGlobalTOFMatched","maxGlobalTOFMatched","meanGlobalTOFMatched","stddevGlobalTOFMatched","minBarTOFmDistMatched",
                                                  "maxBarTOFmDistMatched","meanBarTOFmDistMatched","stddevBarTOFmDistMatched","minGlobalTOFmDistMatched","maxGlobalTOFmDistMatched",
                                                  "meanGlobalTOFmDistMatched","stddevGlobalTOFmDistMatched","minDist","maxDist","meanDist","stddevDist","minDistMatched",
                                                  "maxDistMatched","meanDistMatched","stddevDistMatched","minBarTOFNN","minBarTOFDCL","minGlobalTOFNN","minGlobalTOFDCL",
                                                  "minBarTOFmDistNN","minBarTOFmDistDCL","minGlobalTOFmDistNN","minGlobalTOFmDistDCL","directionTheta","directionality",
                                                  "directionalityZ","largestBarTOFDifference","largestGlobalTOFDifference","largestGlobalTOFDifferenceMatched",
                                                  "minAmp","maxAmp","stddevAmp","meanAmp","minDZ","maxDZ","meanDZ","stddevDZ",
                                                  "minDZ_matched","maxDZ_matched","meanDZ_matched","stddevDZ_matched",
                                                  "minTOFmDist8","maxTOFmDist8","meanTOFmDist8","stddevTOFmDist8","minTOFmDist16","maxTOFmDist16","meanTOFmDist16","stddevTOFmDist16",
                                                  "minTOFmDist4","maxTOFmDist4","meanTOFmDist4","stddevTOFmDist4","minTOFmDist12","maxTOFmDist12","meanTOFmDist12","stddevTOFmDist12",
                                                  "minTOF8","maxTOF8","meanTOF8","stddevTOF8","minTOF16","maxTOF16","meanTOF16","stddevTOF16",
                                                  "minTOF4","maxTOF4","meanTOF4","stddevTOF4","minTOF12","maxTOF12","meanTOF12","stddevTOF12"};

void TBarMVAVars::Print()
{
   std::cout<<"TBarMVAVars::Print()"<<std::endl;
}


TBarMVAVars::~TBarMVAVars()
{
}

void TBarMVAVars::Reset()
{
  fVars.clear();
  fBarEvt = TBarEvent();
}

void TBarMVAVars::AddVar(std::string varName, float varVal)
{
  auto it = std::find(fVarNames.begin(),fVarNames.end(),varName);
  if (it!=fVarNames.end()) fVars[varName] = varVal;
}

bool TBarMVAVars::IsVarUsed(std::string var)
{
  for (std::string varname: fUsedVarNames) {
    if (var==varname) return true;
  }
  return false;
}

void TBarMVAVars::EnforceValueRange(float range_limit)
{
  for (unsigned int i=0; i<fVars.size(); i++) {
    std::string var_name = fVarNames.at(i);
    float val = fVars.at(var_name);
    float lower_range_limit = -1*range_limit;
    float upper_range_limit = range_limit;
    if (var_name.substr(0,3)=="num") { // Set the range limit separately for counting variables
      lower_range_limit = -1;
      upper_range_limit = 65;
    }
    if (std::isnan(val)) fVars.at(var_name) = lower_range_limit;
    if (val < lower_range_limit) fVars.at(var_name) = lower_range_limit;
    if (val > upper_range_limit) fVars.at(var_name) = upper_range_limit;
  }
}

void TBarMVAVars::CalculateVars()
{
  AddVar("numBVBars",fBarEvt.GetNumBars());
  AddVar("numBVEnds",fBarEvt.GetNumEnds());
  AddVar("numADC",fBarEvt.GetNumADC());
  AddVar("numTDC",fBarEvt.GetNumTDC());
  AddVar("numGoodADC",fBarEvt.GetNumGoodADC());
  AddVar("numGoodTDC",fBarEvt.GetNumGoodTDC());
  AddVar("numClusters",fBarEvt.GetNumClusters());
  AddVar("numTOF",fBarEvt.GetNumTOF());
  AddVar("numGoodTOF",fBarEvt.GetNumGoodTOF());
  
  std::vector<float> TOF_bar = GetTOFVals(false,false);
  auto min_TOF_bar = std::min_element(TOF_bar.begin(),TOF_bar.end());
  AddVar("minBarTOF",1e9*((min_TOF_bar!=TOF_bar.end()) ? *min_TOF_bar : ALPHAg::kLargeNegativeUnknown));
  auto max_TOF_bar = std::max_element(TOF_bar.begin(),TOF_bar.end());
  AddVar("maxBarTOF",1e9*((max_TOF_bar!=TOF_bar.end()) ? *max_TOF_bar : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanBarTOF",1e9*(GetMean(TOF_bar)));
  AddVar("stddevBarTOF",1e9*(GetStdDev(TOF_bar)));
  
  std::vector<float> TOF_global = GetTOFVals(true,false);
  auto min_TOF_global = std::min_element(TOF_global.begin(),TOF_global.end());
  AddVar("minGlobalTOF",1e9*((min_TOF_global!=TOF_global.end()) ? *min_TOF_global : ALPHAg::kLargeNegativeUnknown));
  auto max_TOF_global = std::max_element(TOF_global.begin(),TOF_global.end());
  AddVar("maxGlobalTOF",1e9*((max_TOF_global!=TOF_global.end()) ? *max_TOF_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanGlobalTOF",1e9*(GetMean(TOF_global)));
  AddVar("stddevGlobalTOF",1e9*(GetStdDev(TOF_global)));
  
  std::vector<float> TOFmDist_bar = GetTOFmDistVals(false,false);
  auto min_TOFmDist_bar = std::min_element(TOFmDist_bar.begin(),TOFmDist_bar.end());
  AddVar("minBarTOFmDist",1e9*((min_TOFmDist_bar!=TOFmDist_bar.end()) ? *min_TOFmDist_bar : ALPHAg::kLargeNegativeUnknown));
  auto max_TOFmDist_bar = std::max_element(TOFmDist_bar.begin(),TOFmDist_bar.end());
  AddVar("maxBarTOFmDist",1e9*((max_TOFmDist_bar!=TOFmDist_bar.end()) ? *max_TOFmDist_bar : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanBarTOFmDist",1e9*(GetMean(TOFmDist_bar)));
  AddVar("stddevBarTOFmDist",1e9*(GetStdDev(TOFmDist_bar)));
  
  std::vector<float> TOFmDist_global = GetTOFmDistVals(true,false);
  auto min_TOFmDist_global = std::min_element(TOFmDist_global.begin(),TOFmDist_global.end());
  AddVar("minGlobalTOFmDist",1e9*((min_TOFmDist_global!=TOFmDist_global.end()) ? *min_TOFmDist_global : ALPHAg::kLargeNegativeUnknown));
  auto max_TOFmDist_global = std::max_element(TOFmDist_global.begin(),TOFmDist_global.end());
  AddVar("maxGlobalTOFmDist",1e9*((max_TOFmDist_global!=TOFmDist_global.end()) ? *max_TOFmDist_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanGlobalTOFmDist",1e9*(GetMean(TOFmDist_global)));
  AddVar("stddevGlobalTOFmDist",1e9*(GetStdDev(TOFmDist_global)));
  
  std::vector<float> TOF_bar_matched = GetTOFVals(false,true);
  auto min_TOF_bar_matched = std::min_element(TOF_bar_matched.begin(),TOF_bar_matched.end());
  AddVar("minBarTOFMatched",1e9*((min_TOF_bar_matched!=TOF_bar_matched.end()) ? *min_TOF_bar_matched : ALPHAg::kLargeNegativeUnknown));
  auto max_TOF_bar_matched = std::max_element(TOF_bar_matched.begin(),TOF_bar_matched.end());
  AddVar("maxBarTOFMatched",1e9*((max_TOF_bar_matched!=TOF_bar_matched.end()) ? *max_TOF_bar_matched : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanBarTOFMatched",1e9*(GetMean(TOF_bar_matched)));
  AddVar("stddevBarTOFMatched",1e9*(GetStdDev(TOF_bar_matched)));
  
  std::vector<float> TOF_global_matched = GetTOFVals(true,true);
  auto min_TOF_global_matched = std::min_element(TOF_global_matched.begin(),TOF_global_matched.end());
  AddVar("minGlobalTOFMatched",1e9*((min_TOF_global_matched!=TOF_global_matched.end()) ? *min_TOF_global_matched : ALPHAg::kLargeNegativeUnknown));
  auto max_TOF_global_matched = std::max_element(TOF_global_matched.begin(),TOF_global_matched.end());
  AddVar("maxGlobalTOFMatched",1e9*((max_TOF_global_matched!=TOF_global_matched.end()) ? *max_TOF_global_matched : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanGlobalTOFMatched",1e9*(GetMean(TOF_global_matched)));
  AddVar("stddevGlobalTOFMatched",1e9*(GetStdDev(TOF_global_matched)));
  
  std::vector<float> TOFmDist_bar_matched = GetTOFmDistVals(false,true);
  auto min_TOFmDist_bar_matched = std::min_element(TOFmDist_bar_matched.begin(),TOFmDist_bar_matched.end());
  AddVar("minBarTOFmDistMatched",1e9*((min_TOFmDist_bar_matched!=TOFmDist_bar_matched.end()) ? *min_TOFmDist_bar_matched : ALPHAg::kLargeNegativeUnknown));
  auto max_TOFmDist_bar_matched = std::max_element(TOFmDist_bar_matched.begin(),TOFmDist_bar_matched.end());
  AddVar("maxBarTOFmDistMatched",1e9*((max_TOFmDist_bar_matched!=TOFmDist_bar_matched.end()) ? *max_TOFmDist_bar_matched : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanBarTOFmDistMatched",1e9*(GetMean(TOFmDist_bar_matched)));
  AddVar("stddevBarTOFmDistMatched",1e9*(GetStdDev(TOFmDist_bar_matched)));
  
  std::vector<float> TOFmDist_global_matched = GetTOFmDistVals(true,true);
  auto min_TOFmDist_global_matched = std::min_element(TOFmDist_global_matched.begin(),TOFmDist_global_matched.end());
  AddVar("minGlobalTOFmDistMatched",1e9*((min_TOFmDist_global_matched!=TOFmDist_global_matched.end()) ? *min_TOFmDist_global_matched : ALPHAg::kLargeNegativeUnknown));
  auto max_TOFmDist_global_matched = std::max_element(TOFmDist_global_matched.begin(),TOFmDist_global_matched.end());
  AddVar("maxGlobalTOFmDistMatched",1e9*((max_TOFmDist_global_matched!=TOFmDist_global_matched.end()) ? *max_TOFmDist_global_matched : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanGlobalTOFmDistMatched",1e9*(GetMean(TOFmDist_global_matched)));
  AddVar("stddevGlobalTOFmDistMatched",1e9*(GetStdDev(TOFmDist_global_matched)));
  
  std::vector<float> Dist = GetDistVals(false);
  auto min_Dist = std::min_element(Dist.begin(),Dist.end());
  AddVar("minDist",1e9*((min_Dist!=Dist.end()) ? *min_Dist : ALPHAg::kLargeNegativeUnknown));
  auto max_Dist = std::max_element(Dist.begin(),Dist.end());
  AddVar("maxDist",1e9*((max_Dist!=Dist.end()) ? *max_Dist : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanDist",1e9*(GetMean(Dist)));
  AddVar("stddevDist",1e9*(GetStdDev(Dist)));

  std::vector<float> Dist_matched = GetDistVals(true);
  auto min_Dist_matched = std::min_element(Dist_matched.begin(),Dist_matched.end());
  AddVar("minDistMatched",1e9*((min_Dist_matched!=Dist_matched.end()) ? *min_Dist_matched : ALPHAg::kLargeNegativeUnknown));
  auto max_Dist_matched = std::max_element(Dist_matched.begin(),Dist_matched.end());
  AddVar("maxDistMatched",1e9*((max_Dist_matched!=Dist_matched.end()) ? *max_Dist_matched : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanDistMatched",1e9*(GetMean(Dist_matched)));
  AddVar("stddevDistMatched",1e9*(GetStdDev(Dist_matched)));

  std::vector<float> DZ = GetDZVals(false);
  auto min_DZ = std::min_element(DZ.begin(),DZ.end());
  AddVar("minDZ",((min_DZ!=DZ.end()) ? *min_DZ : ALPHAg::kLargeNegativeUnknown));
  auto max_DZ = std::max_element(DZ.begin(),DZ.end());
  AddVar("maxDZ",((max_DZ!=DZ.end()) ? *max_DZ : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanDZ",(GetMean(DZ)));
  AddVar("stddevDZ",(GetStdDev(DZ)));

  std::vector<float> DZ_matched = GetDZVals(true);
  auto min_DZ_matched = std::min_element(DZ_matched.begin(),DZ_matched.end());
  AddVar("minDZ_matched",((min_DZ_matched!=DZ_matched.end()) ? *min_DZ_matched : ALPHAg::kLargeNegativeUnknown));
  auto max_DZ_matched = std::max_element(DZ_matched.begin(),DZ_matched.end());
  AddVar("maxDZ_matched",((max_DZ_matched!=DZ_matched.end()) ? *max_DZ_matched : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanDZ_matched",(GetMean(DZ_matched)));
  AddVar("stddevDZ_matched",(GetStdDev(DZ_matched)));

  std::vector<float> TOF_bar_nn = GetTOFVals(false,false,true,false);
  auto min_TOF_bar_nn = std::min_element(TOF_bar_nn.begin(),TOF_bar_nn.end());
  AddVar("minBarTOFNN",1e9*((min_TOF_bar_nn!=TOF_bar_nn.end()) ? *min_TOF_bar_nn : ALPHAg::kLargeNegativeUnknown));
  
  std::vector<float> TOF_bar_dcl = GetTOFVals(false,false,true,false);
  auto min_TOF_bar_dcl = std::min_element(TOF_bar_dcl.begin(),TOF_bar_dcl.end());
  AddVar("minBarTOFDCL",1e9*((min_TOF_bar_dcl!=TOF_bar_dcl.end()) ? *min_TOF_bar_dcl : ALPHAg::kLargeNegativeUnknown));
  
  std::vector<float> TOF_global_nn = GetTOFVals(false,false,true,false);
  auto min_TOF_global_nn = std::min_element(TOF_global_nn.begin(),TOF_global_nn.end());
  AddVar("minGlobalTOFNN",1e9*((min_TOF_global_nn!=TOF_global_nn.end()) ? *min_TOF_global_nn : ALPHAg::kLargeNegativeUnknown));
  
  std::vector<float> TOF_global_dcl = GetTOFVals(false,false,true,false);
  auto min_TOF_global_dcl = std::min_element(TOF_global_dcl.begin(),TOF_global_dcl.end());
  AddVar("minGlobalTOFDCL",1e9*((min_TOF_global_dcl!=TOF_global_dcl.end()) ? *min_TOF_global_dcl : ALPHAg::kLargeNegativeUnknown));
  
  std::vector<float> TOFmDist_bar_nn = GetTOFmDistVals(false,false,true,false);
  auto min_TOFmDist_bar_nn = std::min_element(TOFmDist_bar_nn.begin(),TOFmDist_bar_nn.end());
  AddVar("minBarTOFmDistNN",1e9*((min_TOFmDist_bar_nn!=TOFmDist_bar_nn.end()) ? *min_TOFmDist_bar_nn : ALPHAg::kLargeNegativeUnknown));
  
  std::vector<float> TOFmDist_bar_dcl = GetTOFmDistVals(false,false,true,false);
  auto min_TOFmDist_bar_dcl = std::min_element(TOFmDist_bar_dcl.begin(),TOFmDist_bar_dcl.end());
  AddVar("minBarTOFmDistDCL",1e9*((min_TOFmDist_bar_dcl!=TOFmDist_bar_dcl.end()) ? *min_TOFmDist_bar_dcl : ALPHAg::kLargeNegativeUnknown));
  
  std::vector<float> TOFmDist_global_nn = GetTOFmDistVals(false,false,true,false);
  auto min_TOFmDist_global_nn = std::min_element(TOFmDist_global_nn.begin(),TOFmDist_global_nn.end());
  AddVar("minGlobalTOFmDistNN",1e9*((min_TOFmDist_global_nn!=TOFmDist_global_nn.end()) ? *min_TOFmDist_global_nn : ALPHAg::kLargeNegativeUnknown));
  
  std::vector<float> TOFmDist_global_dcl = GetTOFmDistVals(false,false,true,false);
  auto min_TOFmDist_global_dcl = std::min_element(TOFmDist_global_dcl.begin(),TOFmDist_global_dcl.end());
  AddVar("minGlobalTOFmDistDCL",1e9*((min_TOFmDist_global_dcl!=TOFmDist_global_dcl.end()) ? *min_TOFmDist_global_dcl : ALPHAg::kLargeNegativeUnknown));

  std::vector<float> TOF8 = GetTOFValsOpeningAngle(8);
  auto minTOF8 = std::min_element(TOF8.begin(),TOF8.end());
  AddVar("minTOF8",1e9*((minTOF8!=TOF8.end()) ? *minTOF8 : ALPHAg::kLargeNegativeUnknown));
  auto maxTOF8 = std::max_element(TOF8.begin(),TOF8.end());
  AddVar("maxTOF8",1e9*((maxTOF8!=TOF8.end()) ? *max_TOF_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanTOF8",1e9*(GetMean(TOF8)));
  AddVar("stddevTOF8",1e9*(GetStdDev(TOF8)));
    
  std::vector<float> TOFmDist8 = GetTOFmDistValsOpeningAngle(8);
  auto minTOFmDist8 = std::min_element(TOFmDist8.begin(),TOFmDist8.end());
  AddVar("minTOFmDist8",1e9*((minTOFmDist8!=TOFmDist8.end()) ? *minTOFmDist8 : ALPHAg::kLargeNegativeUnknown));
  auto maxTOFmDist8 = std::max_element(TOFmDist8.begin(),TOFmDist8.end());
  AddVar("maxTOFmDist8",1e9*((maxTOFmDist8!=TOFmDist8.end()) ? *max_TOFmDist_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanTOFmDist8",1e9*(GetMean(TOFmDist8)));
  AddVar("stddevTOFmDist8",1e9*(GetStdDev(TOFmDist8)));
    
  std::vector<float> TOF16 = GetTOFValsOpeningAngle(16);
  auto minTOF16 = std::min_element(TOF16.begin(),TOF16.end());
  AddVar("minTOF16",1e9*((minTOF16!=TOF16.end()) ? *minTOF16 : ALPHAg::kLargeNegativeUnknown));
  auto maxTOF16 = std::max_element(TOF16.begin(),TOF16.end());
  AddVar("maxTOF16",1e9*((maxTOF16!=TOF16.end()) ? *max_TOF_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanTOF16",1e9*(GetMean(TOF16)));
  AddVar("stddevTOF16",1e9*(GetStdDev(TOF16)));
    
  std::vector<float> TOFmDist16 = GetTOFmDistValsOpeningAngle(16);
  auto minTOFmDist16 = std::min_element(TOFmDist16.begin(),TOFmDist16.end());
  AddVar("minTOFmDist16",1e9*((minTOFmDist16!=TOFmDist16.end()) ? *minTOFmDist16 : ALPHAg::kLargeNegativeUnknown));
  auto maxTOFmDist16 = std::max_element(TOFmDist16.begin(),TOFmDist16.end());
  AddVar("maxTOFmDist16",1e9*((maxTOFmDist16!=TOFmDist16.end()) ? *max_TOFmDist_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanTOFmDist16",1e9*(GetMean(TOFmDist16)));
  AddVar("stddevTOFmDist16",1e9*(GetStdDev(TOFmDist16)));
    
  std::vector<float> TOF4 = GetTOFValsOpeningAngle(4);
  auto minTOF4 = std::min_element(TOF4.begin(),TOF4.end());
  AddVar("minTOF4",1e9*((minTOF4!=TOF4.end()) ? *minTOF4 : ALPHAg::kLargeNegativeUnknown));
  auto maxTOF4 = std::max_element(TOF4.begin(),TOF4.end());
  AddVar("maxTOF4",1e9*((maxTOF4!=TOF4.end()) ? *max_TOF_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanTOF4",1e9*(GetMean(TOF4)));
  AddVar("stddevTOF4",1e9*(GetStdDev(TOF4)));
    
  std::vector<float> TOFmDist4 = GetTOFmDistValsOpeningAngle(4);
  auto minTOFmDist4 = std::min_element(TOFmDist4.begin(),TOFmDist4.end());
  AddVar("minTOFmDist4",1e9*((minTOFmDist4!=TOFmDist4.end()) ? *minTOFmDist4 : ALPHAg::kLargeNegativeUnknown));
  auto maxTOFmDist4 = std::max_element(TOFmDist4.begin(),TOFmDist4.end());
  AddVar("maxTOFmDist4",1e9*((maxTOFmDist4!=TOFmDist4.end()) ? *max_TOFmDist_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanTOFmDist4",1e9*(GetMean(TOFmDist4)));
  AddVar("stddevTOFmDist4",1e9*(GetStdDev(TOFmDist4)));
    
  std::vector<float> TOF12 = GetTOFValsOpeningAngle(12);
  auto minTOF12 = std::min_element(TOF12.begin(),TOF12.end());
  AddVar("minTOF12",1e9*((minTOF12!=TOF12.end()) ? *minTOF12 : ALPHAg::kLargeNegativeUnknown));
  auto maxTOF12 = std::max_element(TOF12.begin(),TOF12.end());
  AddVar("maxTOF12",1e9*((maxTOF12!=TOF12.end()) ? *max_TOF_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanTOF12",1e9*(GetMean(TOF12)));
  AddVar("stddevTOF12",1e9*(GetStdDev(TOF12)));
    
  std::vector<float> TOFmDist12 = GetTOFmDistValsOpeningAngle(12);
  auto minTOFmDist12 = std::min_element(TOFmDist12.begin(),TOFmDist12.end());
  AddVar("minTOFmDist12",1e9*((minTOFmDist12!=TOFmDist12.end()) ? *minTOFmDist12 : ALPHAg::kLargeNegativeUnknown));
  auto maxTOFmDist12 = std::max_element(TOFmDist12.begin(),TOFmDist12.end());
  AddVar("maxTOFmDist12",1e9*((maxTOFmDist12!=TOFmDist12.end()) ? *max_TOFmDist_global : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanTOFmDist12",1e9*(GetMean(TOFmDist12)));
  AddVar("stddevTOFmDist12",1e9*(GetStdDev(TOFmDist12)));
    


  // Direcitonality
  if (fBarEvt.GetNumBarsComplete()<2) {
    AddVar("directionTheta",ALPHAg::kLargeNegativeUnknown);
    AddVar("directionality",ALPHAg::kLargeNegativeUnknown);
    AddVar("directionalityZ",ALPHAg::kLargeNegativeUnknown);

  }
  else {

    // Finds first and last hit
    double first_time = 0; double last_time = 0;
    TVector3 vfirst, vlast;
    for (const TBarHit& h: fBarEvt.GetBarHits()) {
      if (!h.IsComplete()) continue;
      double time = h.GetTime()*1e9 - fBarEvt.GetEventTDCTime()*1e9;
      if (first_time==0 or time<first_time) {
        first_time = time;
        vfirst = h.Get3Vector();
      }
      if (last_time==0 or time>last_time) {
        last_time = time;
        vlast = h.Get3Vector();
      }
    }

    // Finds main axis, i.e. the line that connects first and last hit
    TVector3 axis = vlast - vfirst;
    AddVar("directionTheta",TMath::ATan(TMath::Sqrt(axis.x()*axis.x()+axis.y()*axis.y())/axis.z()));

    std::vector<double> aproj, atime, zproj;
    int n_hits = 0;
    for (const TBarHit& h: fBarEvt.GetBarHits()) {
      if (!h.IsComplete()) continue;
      double time = h.GetTime()*1e9 - fBarEvt.GetEventTDCTime()*1e9;
      TVector3 vec = h.Get3Vector();
      // Projection along main axis
      double proj = vec.Dot(axis)/(axis.Dot(axis));
      aproj.push_back(proj);
      atime.push_back(time);
      zproj.push_back(vec.z());
      n_hits++;
    }

    // Correlation coefficienct between TDC time and projection along main axis
    double sum_xy=0; double sum_x=0; double sum_y=0; double sum_x2=0; double sum_y2=0;
    for (int ih=0; ih<n_hits; ih++) {
      double x = aproj[ih];
      double y = atime[ih];
      sum_xy+=x*y; sum_x+=x; sum_y+=y; sum_x2+=x*x; sum_y2+=y*y;
    }
    double correlation = (n_hits*sum_xy - sum_x*sum_y) / ((n_hits*sum_x2 - sum_x*sum_x) * (n_hits*sum_y2 - sum_y*sum_y));
    AddVar("directionality",correlation);

    // Correlation coefficienct between TDC time and projection along zed axis
    double zsum_xy=0; double zsum_x=0; double zsum_y=0; double zsum_x2=0; double zsum_y2=0;
    for (int ih=0; ih<n_hits; ih++) {
      double x = zproj[ih];
      double y = atime[ih];
      zsum_xy+=x*y; zsum_x+=x; zsum_y+=y; zsum_x2+=x*x; zsum_y2+=y*y;
    }
    double zcorrelation = (n_hits*zsum_xy - zsum_x*zsum_y) / ((n_hits*zsum_x2 - zsum_x*zsum_x) * (n_hits*zsum_y2 - zsum_y*zsum_y));
    AddVar("directionalityZ",zcorrelation);


  }

  // ADC amplitudes
  std::vector<float> amp;
  for (const TBarADCHit& h: fBarEvt.GetADCHits()) {
    if (!h.IsGood()) continue;
    amp.push_back(h.GetAmpFit());
  }
  auto minAmp = std::min_element(amp.begin(),amp.end());
  AddVar("minAmp",((minAmp!=amp.end()) ? *minAmp/32000 : ALPHAg::kLargeNegativeUnknown));
  auto maxAmp = std::max_element(amp.begin(),amp.end());
  AddVar("maxAmp",((maxAmp!=amp.end()) ? *maxAmp/32000 : ALPHAg::kLargeNegativeUnknown));
  AddVar("meanAmp",(GetMean(amp)/32000));
  AddVar("stddevAmp",(GetStdDev(amp)/32000));



  //Testing some additions:
  AddVar("largestBarTOFDifference",max_TOF_bar-min_TOF_bar); //Technically this is encoded but sometimes BDTs like the actual variable 
  AddVar("largestGlobalTOFDifference",max_TOF_global-min_TOF_global); //Technically this is encoded but sometimes BDTs like the actual variable 
  AddVar("largestGlobalTOFDifferenceMatched",max_TOF_global_matched-min_TOF_global_matched); //Technically this is encoded but sometimes BDTs like the actual variable 




  

}

float TBarMVAVars::GetMean(std::vector<float> vec) const {
  if (vec.size()==0) return -999;
  if (vec.size()==1) return vec.at(0);
  float sum = 0;
  for (float v: vec) sum+=v;
  return sum/vec.size();
}

float TBarMVAVars::GetStdDev(std::vector<float> vec) const {
  if (vec.size()==0) return -999;
  if (vec.size()==1) return -999;
  float mean = GetMean(vec);
  float sumsq = 0;
  for (float v: vec) sumsq+=(v-mean)*(v-mean);
  return TMath::Sqrt(sumsq/vec.size());
}

std::vector<float> TBarMVAVars::GetTOFVals(bool global, bool tpc_matched, bool nonneighbour, bool diffcluster) const
{
  std::vector<float> TOFvals;
  for (const TBarTOF& tof: fBarEvt.GetTOFs()) {
    if (!tof.IsComplete()) continue;
    if (tof.IsPileUp()) continue;
    if (tpc_matched and !tof.MatchedToDifferentTPC()) continue;
    if (nonneighbour) {
      int bar1 = tof.GetFirstHit().GetBarID();
      int bar2 = tof.GetSecondHit().GetBarID();
      if (TMath::Abs(bar1-bar2)==1 or TMath::Abs(bar1-bar2)==63) continue;
    }
    if (diffcluster) {
      bool samecluster = false;
      int bar1 = tof.GetFirstHit().GetBarID();
      int bar2 = tof.GetSecondHit().GetBarID();
      for (const TBarCluster& cl: fBarEvt.GetClusters()) {
        if (cl.BarIDIsInside(bar1) and cl.BarIDIsInside(bar2)) samecluster = true;
      }
      if (samecluster) break;
    }
    TOFvals.push_back((float)tof.GetTOF(global));
  }
  return TOFvals;
}
std::vector<float> TBarMVAVars::GetTOFmDistVals(bool global, bool tpc_matched, bool nonneighbour, bool diffcluster) const
{
  std::vector<float> TOFvals;
  for (const TBarTOF& tof: fBarEvt.GetTOFs()) {
    if (!tof.IsComplete()) continue;
    if (tof.IsPileUp()) continue;
    if (tpc_matched and !tof.MatchedToDifferentTPC()) continue;
    if (nonneighbour) {
      int bar1 = tof.GetFirstHit().GetBarID();
      int bar2 = tof.GetSecondHit().GetBarID();
      if (TMath::Abs(bar1-bar2)==1 or TMath::Abs(bar1-bar2)==63) continue;
    }
    if (diffcluster) {
      bool samecluster = false;
      int bar1 = tof.GetFirstHit().GetBarID();
      int bar2 = tof.GetSecondHit().GetBarID();
      for (const TBarCluster& cl: fBarEvt.GetClusters()) {
        if (cl.BarIDIsInside(bar1) and cl.BarIDIsInside(bar2)) samecluster = true;
      }
      if (samecluster) break;
    }
    TOFvals.push_back((float)tof.GetTOFmDist(true,global));
  }
  return TOFvals;
}
std::vector<float> TBarMVAVars::GetDistVals(bool tpc_matched) const
{
  std::vector<float> TOFvals;
  for (const TBarTOF& tof: fBarEvt.GetTOFs()) {
    if (!tof.IsComplete()) continue;
    if (tof.IsPileUp()) continue;
    if (tpc_matched and !tof.MatchedToDifferentTPC()) continue;
    TOFvals.push_back((float)tof.GetDistanceOverC());
  }
  return TOFvals;
}
std::vector<float> TBarMVAVars::GetDZVals(bool tpc_matched) const
{
  std::vector<float> DZvals;
  for (const TBarTOF& tof: fBarEvt.GetTOFs()) {
    if (!tof.IsComplete()) continue;
    if (tof.IsPileUp()) continue;
    if (tpc_matched and !tof.MatchedToDifferentTPC()) continue;
    DZvals.push_back((float)tof.GetDZ());
  }
  return DZvals;
}
std::vector<float> TBarMVAVars::GetTOFValsOpeningAngle(int min_opening_angle) const
{
  std::vector<float> TOFvals;
  for (const TBarTOF& tof: fBarEvt.GetTOFs()) {
    if (!tof.IsComplete()) continue;
    if (tof.IsPileUp()) continue;
    if (tof.GetOpeningAngle()<min_opening_angle) continue;
    TOFvals.push_back((float)tof.GetTOF());
  }
  return TOFvals;
}
std::vector<float> TBarMVAVars::GetTOFmDistValsOpeningAngle(int min_opening_angle) const
{
  std::vector<float> TOFvals;
  for (const TBarTOF& tof: fBarEvt.GetTOFs()) {
    if (!tof.IsComplete()) continue;
    if (tof.IsPileUp()) continue;
    if (tof.GetOpeningAngle()<min_opening_angle) continue;
    TOFvals.push_back((float)tof.GetTOFmDist());
  }
  return TOFvals;
}
