#include "TBarEndHit.h"
ClassImp(TBarEndHit)

TBarEndHit::TBarEndHit(): TObject()
{
  fEndID = -1;
  fADCHit=TBarADCHit();
  fTDCHit=TBarTDCHit();
  fTW = -9999;
}
TBarEndHit::TBarEndHit(const TBarADCHit& _fADCHit, const TBarTDCHit& _fTDCHit): TObject()
{
  fEndID = _fADCHit.GetEndID();
  fADCHit = _fADCHit;
  fTDCHit = _fTDCHit;
  fTW = -9999;
}

void TBarEndHit::Print()
{
  std::cout<<"EndHit::Print() -- End ID:"<<fEndID<<std::endl;
  std::cout<<"ADC time: "<<fADCHit.GetStartTime()<<" Amplitude: "<<fADCHit.GetAmpRaw()<<std::endl;
  std::cout<<"TDC time: "<<fTDCHit.GetTime()<<std::endl;
}

TBarEndHit::~TBarEndHit()
{
// dtor
}
