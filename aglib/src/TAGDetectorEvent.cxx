#include "TAGDetectorEvent.hh"

TAGDetectorEvent::TAGDetectorEvent():
   fVertex(ALPHAg::kUnknown,ALPHAg::kUnknown,ALPHAg::kUnknown)
{
   fHasBarFlow = false;
   fHasAnalysisFlow = false;
   fRunNumber = -1;
   fEventNo = -1;
   fRunTime = -1;
   fCutsResult.clear();
   fVertexStatus = 0;
   fTPCTime = -1;
   fNumHelices = -1;
   fNumTracks = -1;
   fNumADCBars = -1;
   fNumTDCBars = -1;
   fBarTime = -1;
   fBarMaxTOF = -1;
   fBarMaxDist = -1;
   fBarTOFstdv = -1;
   fBarMinTOFminusDist = -1;
   fMVA = -99;
}
TAGDetectorEvent::~TAGDetectorEvent()
{
   
}
// Construct with TStoreEvent
TAGDetectorEvent::TAGDetectorEvent( const TStoreEvent* e, const TBarEvent* b ): TAGDetectorEvent()
{
   fHasBarFlow = false;
   fHasAnalysisFlow = false;
   AddAnalysisFlow(e,b);
}
bool TAGDetectorEvent::AddAnalysisFlow(const TStoreEvent* event, const TBarEvent* b)
{
   //This already has TPC data
   if (fHasAnalysisFlow)
      return false;
   // If this contains bar data, the event number must match!
   if (fHasBarFlow && fEventNo != event->GetEventNumber())
      return false;
   fHasAnalysisFlow = true;
   fRunNumber =  event->GetRunNumber();
   fSerialNumber = event->GetSerialNumber();
   fEventNo = event->GetEventNumber();
   fRunTime = event->GetTimeOfEvent();
   fTPCTime = event->GetTimeOfEvent();

   fVertex = event->GetVertex();
   fVertexStatus = event->GetVertexStatus();
   fNumHelices = event->GetUsedHelices()->GetEntriesFast();
   fNumTracks = event->GetNumberOfTracks();

   fBarTime = event->GetBarTime();
   
   if (!b)
      return false;
   fNumADCBars = b->GetNumADC();
   fNumTDCBars = b->GetNumTDC();
   fBarTime = b->GetRunTime();
   fBarMaxDist = -1;
   fBarMaxTOF = -1;
   fBarMinTOFminusDist = 9e9;
   fBarTOFstdv = 0;
   for (const TBarTOF& tof: b->GetTOFs()) {
      if (tof.GetTOF()>fBarMaxTOF) fBarMaxTOF = tof.GetTOF();
      if (tof.GetDistance()>fBarMaxDist) fBarMaxDist = tof.GetDistance();
      if (tof.GetTOFmDist()<fBarMinTOFminusDist) fBarMinTOFminusDist = tof.GetTOFmDist();
      fBarTOFstdv += tof.GetTOF()*tof.GetTOF();
   }
   fBarTOFstdv = sqrt(fBarTOFstdv);

   //std::cout << "Adding event MVA value to TAGDetectorEvent: " << event->GetMVAValue() << "\n";
   fMVA = event->GetMVAValue();



   // Avoid confusion counting from zero... Zeroth entry is always true (signify no cuts)
   fCutsResult = ApplyCuts(event,b);
   return true;
}

bool TAGDetectorEvent::VertexGoodCut(const TStoreEvent* e) const
{
   return e->GetVertexStatus()>0;
}

bool TAGDetectorEvent::NHelixCut(const TStoreEvent* e, int cut) const
{
   return e->GetUsedHelices()->GetEntriesFast() > cut;
}

bool TAGDetectorEvent::NTrackCut(const TStoreEvent* e, int cut) const
{
   return e->GetNumberOfTracks() > cut;
}

bool TAGDetectorEvent::RadiusCut(const TStoreEvent* e, const double R_2, const double R_2_plus) const
{
   //Dummy example of ApplyCuts for a TStoreEvent... 
   //Change this when we have pbars!
   const Double_t R=e->GetVertex().Perp();
   const Int_t NTracks=e->GetNumberOfTracks();
   if (NTracks==2)
      if (R < R_2) return true;
   if (NTracks > 2)
      if (R < R_2_plus) return true;
   return false;
}

bool TAGDetectorEvent::BarMultiplicityCut(const TBarEvent* b, const int cut) const
{
   if (!b)
      return false;
   if (b->GetNumBarsComplete() < cut)
      return false;
   return true;
}

bool TAGDetectorEvent::LowTOFCut(const TBarEvent* b, double cut) const
{
   if (!b) return false;
   double BarMaxTOF = -1;
   for (const TBarTOF& tof: b->GetTOFs()) {
      if (tof.GetTOF()>BarMaxTOF) BarMaxTOF = tof.GetTOF();
   }
   if (BarMaxTOF > 0)
      return BarMaxTOF < cut;
   return false;
}

bool TAGDetectorEvent::TOFDistCut(const TBarEvent* b, double cut) const
{
   if (!b) return false;
   double BarMinTOFmDist = 9e9;
   for (const TBarTOF& tof: b->GetTOFs()) {
      if (tof.GetTOFmDist()>BarMinTOFmDist) BarMinTOFmDist = tof.GetTOFmDist();
   }
   if (BarMinTOFmDist < 1e9)
      return BarMinTOFmDist < cut;
   return false;
}

bool TAGDetectorEvent::MVABDTCut(const TStoreEvent* e, const double cut) const
{
   return (e->GetMVAValue() > cut);
}

int TAGDetectorEvent::SimpleCountBarClusters(const std::vector<TBarHit>& bars) const
{
   const int numBars = bars.size();

   //If there is 0 or 1 bar hit, there will be the same number of clusters so we can just return it.
   if(numBars < 2)
      return numBars;

   // If all bars are here, then we have one mega cluster
   // GS note!! This is broken and has been broken for events with 63+ hits. It assumes only one hit per bar, which is not necessarily true.
   if(numBars >= 63)
      return 1;

   // Unroll bar hits into a continuous vector
   std::vector<bool> hits(64,false);
   for (const TBarHit& h: bars)
   {
      hits.at(h.GetBarID()) = true;
   }

   int risingedge = 0;
   int fallingedge = 0;
   // Count number of rising edges and falling edges
   bool A = hits.back(); // Start with wrap around
   for (int i = 0; i < 64; i++)
   {
      bool B = hits.at(i);
      if ( !A && B ) // 01 - Rising edge
         ++risingedge;
      else if ( A && !B) // 10 - Falling edge
         ++fallingedge;
      A = B;
   }

   // Clusters need both a rising edge and falling edge... double check
   if (risingedge == fallingedge)
      return risingedge;

   // Clustering failed!
   std::cout << "CLUSTER COUNT FAILED " << std::endl;
   return -1;
}

bool TAGDetectorEvent::SimpleClusterCutComplete(const TBarEvent* b, int cut) const
{
   std::vector<TBarHit> bars;
   for (const TBarHit& bar: b->GetBarHits()) {
      if (bar.IsComplete()) bars.push_back(bar);
   }
   return (SimpleCountBarClusters(bars) >= cut); // Can probably retire this now.
}

bool TAGDetectorEvent::SimpleClusterCutHalfComplete(const TBarEvent* b, int cut) const
{
   std::vector<TBarHit> bars;
   for (const TBarHit& bar: b->GetBarHits()) {
      if (bar.IsHalfComplete()) bars.push_back(bar);
   }
   return (SimpleCountBarClusters(bars) >= cut); // Old implementation
   //return (b->GetClusters().size() >= cut); // New implementation, need to test this.
}

bool TAGDetectorEvent::ClusterCut(const TBarEvent* e, int cut) const
{
   return (e->GetNumClusters() >= cut);
}

ClassImp(TAGDetectorEvent)
