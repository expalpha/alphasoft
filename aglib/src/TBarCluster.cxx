
#include "TBarCluster.h"
ClassImp(TBarCluster)

TBarCluster::TBarCluster()
{
}


TBarCluster::TBarCluster(const TBarCluster& c): TObject(c)
{
  // copy ctor
  for (const TBarHit &h: c.GetBarHits()) fBarHits.push_back(h); 
}

void TBarCluster::Print()
{
   std::cout<<"TBarCluster::Print() -- Number of hits:"<<fBarHits.size()<<std::endl;
}


TBarCluster::~TBarCluster()
{
}

int TBarCluster::GetNumHitsComplete() {
  int n=0;
  for (TBarHit &h: fBarHits) {
    if (h.IsComplete()) n++;
  }
  return n;
}

int TBarCluster::GetNumHitsHalfComplete() {
  int n=0;
  for (TBarHit &h: fBarHits) {
    if (h.IsHalfComplete()) n++;
  }
  return n;
}

void TBarCluster::GetHitComplete(int i, TBarHit& hit) {
  int j=0;
  for (TBarHit &h: fBarHits) {
    if (h.IsComplete()) {
      if (i==j) {
        hit = h;
        return;
      }
      j++;
    }
  }
}

void TBarCluster::GetHitHalfComplete(int i, TBarHit& hit) {
  int j=0;
  for (TBarHit &h: fBarHits) {
    if (h.IsHalfComplete()) {
      if (i==j) {
        hit = h;
        return;
      }
      j++;
    }
  }
}

int TBarCluster::GetStartBarID() {
  if (fBarHits.size()==0) return -1;
  if (fBarHits.size()==64) return -1;
  int current = fBarHits[0].GetBarID();
  while (true) {
    bool found = false;
    for (const TBarHit &h: fBarHits) {
      int next = h.GetBarID();
      if (next==current-1 or (current==0 and next==63)) {
        current = next;
        found = true;
        continue;
      }
    }
    if (!found) return current;
  }
}

int TBarCluster::GetEndBarID() {
  if (fBarHits.size()==0) return -1;
  if (fBarHits.size()==64) return -1;
  int current = fBarHits[0].GetBarID();
  while (true) {
    bool found = false;
    for (const TBarHit &h: fBarHits) {
      int next = h.GetBarID();
      if (next==current+1 or (current==63 and next==0)) {
        current = next;
        found = true;
        continue;
      }
    }
    if (!found) return current;
  }
}

int TBarCluster::GetBarIDBefore(int n_before) {
  int start = GetStartBarID();
  if (start-n_before<0) return start-n_before+64;
  if (start-n_before>=64) return start-n_before-64;
  return start-n_before;
}

int TBarCluster::GetBarIDAfter(int n_after) {
  int end = GetEndBarID();
  if (end+n_after>=64) return end+n_after-64;
  if (end+n_after<0) return end+n_after+64;
  return end+n_after;
}

bool TBarCluster::BarIDIsInside(int barID) const {
  for (const TBarHit& h: fBarHits) {
    if (h.GetBarID()==barID) return true;
  }
  return false;
}

double TBarCluster::GetStartZ() {
  int n_over = 0;
  while (n_over<(int)fBarHits.size()) {
    for (const TBarHit &h: fBarHits) {
      if (h.GetBarID()==GetBarIDBefore(-1*n_over)) {
        if (h.HasZed()) return h.GetZed();
      }
    }
    n_over++;
  }
  return -999999.;
}

double TBarCluster::GetEndZ() {
  int n_over = 0;
  while (n_over<(int)fBarHits.size()) {
    for (const TBarHit &h: fBarHits) {
      if (h.GetBarID()==GetBarIDAfter(-1*n_over)) {
        if (h.HasZed()) return h.GetZed();
      }
    }
    n_over++;
  }
  return -999999.;
}

double TBarCluster::GetAverageTime() {
  double sum = 0;
  int n = 0;
  for (const TBarHit &h: fBarHits) {
    if (h.HasTime()) {
      sum += h.GetTime();
      n++;
    }
  }
  if (n==0) return -1;
  return sum/n;
}