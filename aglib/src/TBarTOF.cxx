
#include "TBarTOF.h"
ClassImp(TBarTOF)

TBarTOF::TBarTOF()
{
  fFirstGlobalOffset = -1;
  fSecondGlobalOffset = -1;
  fBarToBarOffset = -1;
  fBarToBarSigma = -1;
  fOffsetsLoaded = false;
}


TBarTOF::TBarTOF(const TBarTOF& c): TObject(c)
{
  // copy ctor
  fFirstHit = c.GetFirstHit();
  fSecondHit = c.GetSecondHit();
  fFirstGlobalOffset = c.GetFirstGlobalOffset();
  fSecondGlobalOffset = c.GetSecondGlobalOffset();
  fBarToBarSigma = c.GetBarToBarSigma();
  fBarToBarOffset = c.GetBarToBarOffset();
  fOffsetsLoaded = c.IsOffsetsLoaded();
}

TBarTOF::TBarTOF(const TBarHit &h1, const TBarHit &h2)
{
  fFirstGlobalOffset = -1;
  fSecondGlobalOffset = -1;
  fBarToBarOffset = -1;
  fBarToBarSigma = -1;
  fOffsetsLoaded = false;
  AddHits(h1,h2);
}

void TBarTOF::Print()
{
   std::cout<<"TBarTOF::Print() -- Time of flight: "<<GetTOF()<<std::endl;
}


TBarTOF::~TBarTOF()
{
}

double TBarTOF::GetDistance(bool Z_from_BV) const
{
  TVector3 p1, p2;
  if (Z_from_BV) {
    p1 = fFirstHit.Get3Vector();
    p2 = fSecondHit.Get3Vector();
  }
  else {
    p1 = fFirstHit.Get3VectorZTPC();
    p2 = fSecondHit.Get3VectorZTPC();
  }
  TVector3 diff = p2-p1;
  return diff.Mag();
}

double TBarTOF::GetDZ(bool Z_from_BV) const
{
  double p1, p2;
  if (Z_from_BV) {
    p1 = fFirstHit.Get3Vector().z();
    p2 = fSecondHit.Get3Vector().z();
  }
  else {
    p1 = fFirstHit.Get3VectorZTPC().z();
    p2 = fSecondHit.Get3VectorZTPC().z();
  }
  return p2-p1;
}

int TBarTOF::GetOpeningAngle() const
{
  int b1 = fFirstHit.GetBarID();
  int b2 = fSecondHit.GetBarID();
  int diff = b2-b1;
  if (0<=diff and diff<=32) return diff;
  if (-32<=diff and diff<=0) return -1*diff;
  if (32<=diff and diff<=64) return 64-diff;
  if (-64<=diff and diff<=-32) return 64+diff;
  return -99;
}

void TBarTOF::AddHits(const TBarHit &h1, const TBarHit &h2)
{
  double dt = h2.GetTime() - h1.GetTime();
  if (dt>0) {
    fFirstHit = h1;
    fSecondHit = h2;
  }
  else {
    fFirstHit = h2;
    fSecondHit = h1;
  }
}

double TBarTOF::GetTOF(bool use_global_offsets) const
{
  if (!fOffsetsLoaded or use_global_offsets) return fSecondHit.GetTime() - fFirstHit.GetTime();
  return ( (fSecondHit.GetTime() - fFirstHit.GetTime()) - (fSecondGlobalOffset - fFirstGlobalOffset) - fBarToBarOffset);
}

double TBarTOF::GetSignificance(bool Z_from_BV, bool use_global_offsets) const
{
  if (!fOffsetsLoaded) return 999999;
  return GetTOFmDist(Z_from_BV,use_global_offsets)/fBarToBarSigma;
}
