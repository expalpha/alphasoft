// Store event class implementation
// for ALPHA-g TPC AGTPCanalysis
// Authors: A. Capra, M. Mathers
// Date: April 2017
#ifdef BUILD_AG
#include "TStoreEvent.hh"
#include "TStoreHelix.hh"
#include "TStoreLine.hh"
#include "TSpacePoint.hh"
//#include "TStoreTrack.hh"

#include "TFitLine.hh"
#include "TFitHelix.hh"

#include "TPCconstants.hh"

#include <iostream>

TStoreEvent::TStoreEvent():TObject(),fID(-1),
         fSerialNumber(-1),
         fNpoints(-1),fNusedpoints(-1),fNtracks(-1),
         fNpadSigs(-1), fNawSigs(-1),
         fStoreHelixArray(20), fStoreLineArray(20),
         fSpacePoints(5000),
         fUsedHelices(20),
         fVertex(ALPHAg::kUnknown,ALPHAg::kUnknown,ALPHAg::kUnknown),
         fVertexStatus(kUninitialized),
         fDCA(ALPHAg::kLargeNegativeUnknown),
         fPattRecEff(-1.),
         fMVA(ALPHAg::kLargeNegativeUnknown)
{}

TStoreEvent::TStoreEvent(const TStoreEvent& right):TObject(right), fID(right.fID),
         fSerialNumber(right.fSerialNumber),
         fEventTime(right.fEventTime),
         fNpoints(right.fNpoints),
         fNusedpoints(right.fNusedpoints),
         fNtracks(right.fNtracks),
         fNpadSigs(right.fNpadSigs),
         fNawSigs(right.fNawSigs),
         fStoreHelixArray(right.fStoreHelixArray),
         fStoreLineArray(right.fStoreLineArray),
         fSpacePoints(right.fSpacePoints),
         fTrivialLine(right.fTrivialLine),
         fUsedHelices(right.fUsedHelices),
         fVertex(right.fVertex),fVertexStatus(right.fVertexStatus),
         fDCA(right.fDCA),fPattRecEff(right.fPattRecEff),
         fMVA(right.fMVA),
         fBarEvent(right.fBarEvent)
{}

TStoreEvent& TStoreEvent::operator=(const TStoreEvent& right)
{
  fID = right.fID;
  fSerialNumber = right.fSerialNumber;
  fEventTime = right.fEventTime;
  fNpoints = right.fNpoints;
  fNusedpoints = right.fNusedpoints;
  fNtracks = right.fNtracks;
  fNpadSigs = right.fNpadSigs;
  fNawSigs = right.fNawSigs;
  fStoreHelixArray = right.fStoreHelixArray;
  fStoreLineArray = right.fStoreLineArray;
  fSpacePoints = right.fSpacePoints;
  fTrivialLine = right.fTrivialLine;
  fUsedHelices = right.fUsedHelices;
  fVertex = right.fVertex;
  fVertexStatus = right.fVertexStatus;
  fDCA = right.fDCA;
  fPattRecEff = right.fPattRecEff;
  fBarEvent = right.fBarEvent;
  fMVA = right.fMVA;
  return *this;
}

void TStoreEvent::SetEvent(const std::vector<TSpacePoint>& points, const std::vector<TFitLine>& lines, 
            const std::vector<TFitHelix>& helices)
{
   int N_points_total = points.size();
   for (int i = 0; i < N_points_total; ++i) {
      fSpacePoints.AddLast(new TSpacePoint(points.at(i)));
   }
   fSpacePoints.Compress();

   int nlines=lines.size();
   for(int i=0; i<nlines; ++i)
   {
      const TFitLine &aLine = lines.at(i);
      if( aLine.GetStatus() > 0 )
      {
         fStoreLineArray.AddLast( new TStoreLine( aLine, aLine.GetPointsArray() ) );
       }
   }
   fStoreLineArray.Compress();

   fNtracks = helices.size();
   fNpoints = points.size();
   fNusedpoints = 0;
   for(int i=0; i<fNtracks; ++i)
   {
      const TFitHelix &anHelix = helices.at(i);
      //if( anHelix.GetStatus() > 0 )
      {
         fStoreHelixArray.AddLast( new TStoreHelix( &anHelix, anHelix.GetPointsArray() ) );   // FIXME: why does TStoreHelix want a pointer, when TStoreLine wants a reference?
         fNusedpoints += anHelix.GetNumberOfPoints();
       }
   }
   fStoreHelixArray.Compress();

   if( fNtracks > 0 )
      fPattRecEff = (double)fNusedpoints/(double)fNtracks;
   else
      fPattRecEff = 0.;
}


TStoreEvent::~TStoreEvent()
{
  Reset();
}
void TStoreEvent::Print(Option_t* o) const
{
   if (strcmp(o,"title")==0)
      std::cout<<"EventNo\tTPCtime\tNpoints\tNtracks\tX\tY\tZ\t";
   else if (strcmp(o,"line")==0) 
      printf("%d\t%f\t%d\t%d\t%d\t%.2f\t%.2f\t%.2f\t",fID,fEventTime,fNpoints,fNtracks,fVertexStatus,fVertex.X(),fVertex.Y(),fVertex.Z());
   else
   {
      std::cout<<"=============== TStoreEvent "<<std::setw(5)
         <<fID<<" ==============="<<std::endl;
      std::cout<<"EventTime: "<< fEventTime <<std::endl;
      std::cout<<"Number of Points: "<<fNpoints
         <<"\tNumber Of Tracks: "<<fNtracks<<std::endl;
      std::cout<<"*** Vertex Position ***"<<std::endl;
      fVertex.Print();
      std::cout<<"Vertex Status: "<< fVertexStatus <<std::endl;
      std::cout<<"***********************"<<std::endl;
      std::cout<<"fPattRecEff: "<< fPattRecEff <<std::endl;
      std::cout<<"fMVA: "<< fMVA <<std::endl;


      //std::cout<<"*** Bar Hits ***"<<std::endl;
      //std::cout<<"fBarHit: "<< fBarHit.size() <<std::endl;
      //for(int i=0; i<fBarHit.size(); ++i)
      //   {
      //     ((TFitLine*)fStoreLineArray.At(i))->Print();
      //   }
      std::cout<<"***********************"<<std::endl;

      std::cout<<"*** TObj Arrays ***"<<std::endl;
      std::cout<<"fStoreHelixArray***********************"<<std::endl;
      std::cout<<"fStoreHelixArray: "<< fStoreHelixArray.GetEntries() <<std::endl;
       //for(int i=0; i<fStoreHelixArray.GetEntries(); ++i)
       for(int i=0; i<std::min(1,fStoreHelixArray.GetEntries()) ; ++i)
         {
           ((TFitLine*)fStoreHelixArray.At(i))->Print();
         }
      std::cout<<"***********************"<<std::endl;
      
      std::cout<<"fStoreLineArray***********************"<<std::endl;
      std::cout<<"fStoreLineArray: "<< fStoreLineArray.GetEntries() <<std::endl;
       //for(int i=0; i<fStoreLineArray.GetEntries(); ++i)
       for(int i=0; i<std::min(1,fStoreLineArray.GetEntries()); ++i)
         {
           ((TFitLine*)fStoreLineArray.At(i))->Print();
         }
      std::cout<<"***********************"<<std::endl;
      
      std::cout<<"fSpacePoints***********************"<<std::endl;
      std::cout<<"fSpacePoints: "<< fSpacePoints.GetEntries() <<std::endl;
       //for(int i=0; i<fSpacePoints.GetEntries(); ++i)
       for(int i=0; i<std::min(1,fSpacePoints.GetEntries()); ++i)
         {
           ((TFitLine*)fSpacePoints.At(i))->Print();
         }
      std::cout<<"***********************"<<std::endl;

      std::cout<<"fUsedHelices***********************"<<std::endl;
      std::cout<<"fUsedHelices: "<< fUsedHelices.GetEntries() <<std::endl;
       //for(int i=0; i<fSpacePoints.GetEntries(); ++i)
       /*for(int i=0; i<std::min(1,fUsedHelices.GetEntries()); ++i)
         {
           ((TFitLine*)fUsedHelices.At(i))->Print();
         }*/
      std::cout<<"***********************"<<std::endl;
      
      std::cout<<"======================================="<<std::endl;
   }
}

int TStoreEvent::AddLine(const TFitLine* l) 
{ 
  fStoreLineArray.AddLast( new TStoreLine( *l, l->GetPointsArray() ) ); 
  return fStoreLineArray.GetEntriesFast(); 
}
       
int TStoreEvent::AddHelix(const TFitHelix* h) 
{ 
  fStoreHelixArray.AddLast( new TStoreHelix( h, h->GetPointsArray() ) ); 
  return fStoreHelixArray.GetEntriesFast(); 
}

void TStoreEvent::Reset()
{
  fID = -1;
  fSerialNumber = -1;
  fNpoints = -1;
  fNusedpoints = -1;
  fNtracks = -1;
  fNpadSigs = -1;
  fNawSigs = -1;
  fMVA = -99;
  fDCA = ALPHAg::kLargeNegativeUnknown;

  fStoreLineArray.SetOwner(kTRUE);
  fStoreLineArray.Delete();
  fStoreLineArray.Clear();

  fStoreHelixArray.SetOwner(kTRUE);
  fStoreHelixArray.Delete();
  fStoreHelixArray.Clear();

  fUsedHelices.SetOwner(kTRUE);
  fUsedHelices.Delete();
  fUsedHelices.Clear();

  fTrivialLine.Clear();

  fSpacePoints.SetOwner(kTRUE);
  fSpacePoints.Delete();
  fSpacePoints.Clear();

  fVertex.SetXYZ(ALPHAg::kUnknown,ALPHAg::kUnknown,ALPHAg::kUnknown);

  fPattRecEff = -1.;
  
  //fBarHit.clear();

}

ClassImp(TStoreEvent)
#endif
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
