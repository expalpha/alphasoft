#ifdef BUILD_AG
#include "TBarEvent.hh"




ClassImp(TBarEvent)

TBarEvent::TBarEvent()
{
// ctor
  fEventID=0;
  fEventTime=0;
  fNMatchedTracks=0;
  fNUnmatchedTracks=0;
  fNPileUpTracks=0;
}

TBarEvent::TBarEvent(TBarEvent &barEvt): TObject(barEvt)
{
// copy ctor
  fEventID = barEvt.GetID();
  fEventTime = barEvt.GetRunTime();
  fNMatchedTracks = barEvt.GetNumberOfMatchedTracks();
  fNUnmatchedTracks = barEvt.GetNumberOfUnmatchedTracks();
  fNPileUpTracks = barEvt.GetNumberOfPileUpTracks();
  for(const TBarHit& barhit: barEvt.GetBarHits())  {
      fBarHit.push_back(barhit);
  }
  for(const TBarEndHit& endhit: barEvt.GetEndHits()) {
      fEndHit.push_back(endhit);
  }
  for(const TBarADCHit& adchit: barEvt.GetADCHits()) {
      fADCHit.push_back(adchit);
  }
  for(const TBarTDCHit& tdchit: barEvt.GetTDCHits()) {
      fTDCHit.push_back(tdchit);
  }
  for(const TBarCluster& c: barEvt.GetClusters()) {
      fClusters.push_back(c);
  }
  for(const TBarTOF& tof: barEvt.GetTOFs()) {
      fTOF.push_back(tof);
  }
}

TBarEvent::TBarEvent(const TBarEvent &barEvt): TObject(barEvt)
{
// copy ctor
  fEventID = barEvt.GetID();
  fEventTime = barEvt.GetRunTime();
  fNMatchedTracks = barEvt.GetNumberOfMatchedTracks();
  fNUnmatchedTracks = barEvt.GetNumberOfUnmatchedTracks();
  fNPileUpTracks = barEvt.GetNumberOfPileUpTracks();
  for(const TBarHit& barhit: barEvt.GetBarHits())  {
      fBarHit.push_back(barhit);
  }
  for(const TBarEndHit& endhit: barEvt.GetEndHits()) {
      fEndHit.push_back(endhit);
  }
  for(const TBarADCHit& adchit: barEvt.GetADCHits()) {
      fADCHit.push_back(adchit);
  }
  for(const TBarTDCHit& tdchit: barEvt.GetTDCHits()) {
      fTDCHit.push_back(tdchit);
  }
  for(const TBarCluster& c: barEvt.GetClusters()) {
      fClusters.push_back(c);
  }
  for(const TBarTOF& tof: barEvt.GetTOFs()) {
      fTOF.push_back(tof);
  }
}


TBarEvent::~TBarEvent()
{

}

int TBarEvent::GetNumBarsComplete() const {
    int n = 0;
    for (const TBarHit& h: fBarHit) {
      if (h.IsComplete()) n++;
    }
    return n;
  }

int TBarEvent::GetNumBarsHalfComplete() const {
    int n = 0;
    for (const TBarHit& h: fBarHit) {
      if (h.IsHalfComplete()) n++;
    }
    return n;
  }

int TBarEvent::GetNumMatchedBars() const {
    int n = 0;
    for (const TBarHit& h: fBarHit) {
      if (h.HasTPCHit()) n++;
    }
    return n;
  }

int TBarEvent::GetNumMatchedBarsHalfComplete() const {
    int n = 0;
    for (const TBarHit& h: fBarHit) {
      if (h.HasTPCHit() and h.IsHalfComplete()) n++;
    }
    return n;
  }

int TBarEvent::GetNumMatchedBarsComplete() const {
    int n = 0;
    for (const TBarHit& h: fBarHit) {
      if (h.HasTPCHit() and h.IsComplete()) n++;
    }
    return n;
  }

int TBarEvent::GetNumGoodADC() const {
    int n = 0;
    for (const TBarADCHit& h: fADCHit) {
      if (h.IsGood()) n++;
    }
    return n;
  }

int TBarEvent::GetNumGoodTDC() const {
    int c=0;
    for (TBarTDCHit h: fTDCHit) {
      if (h.IsGood()) c++;
    }
    return c;
  }

int TBarEvent::GetNumGoodTOF() const {
    int c=0;
    for (TBarTOF h: fTOF) {
      if (h.IsGood()) c++;
    }
    return c;
  }

bool TBarEvent::IsInjection() const {
    int nFlatBottom=0;
    for (const TBarADCHit& a: fADCHit) {
      if (a.IsFlatBottom()) nFlatBottom++;
    }
    return nFlatBottom>16;
  }

bool TBarEvent::IsPileUp() const {
    for (const TBarTOF& t: fTOF) {
      if (!t.IsComplete()) continue;
      if (t.IsPileUp()) return true;
    }
    return false;
  }

void TBarEvent::FindMainGroup() {
  double first_tdc_time = 0;
  // Start by finding the time of the first complete hit (i.e. hopefully the hit which triggered the event)
  for (const TBarHit& h: fBarHit) {
    if (!h.IsComplete()) continue;
    double time = h.GetTime();
    if (first_tdc_time==0 or time<first_tdc_time) first_tdc_time = time;
  }
  // Now tell each hit whether they are in the main group, i.e. at the same time as the first hit,
  //    or if they are from pileup or a secondary process.
  std::vector<TBarHit> new_bar_hits = fBarHit;
  for (TBarHit& h: new_bar_hits) {
    if (!h.HasTime()) continue;
    if (TMath::Abs(h.GetTime() - first_tdc_time) < TDC_PILEUP_TIME) h.SetIsInMainGroup(true);
  }
  fBarHit = new_bar_hits; // why is this necessary.....
}


void TBarEvent::Print()
{
   std::cout << "TBarEvent::Print() -------------"<<std::endl;
   std::cout <<"Event time:\t"  << fEventTime    <<std::endl;
   std::cout <<"ADC Hits:\t"     << fADCHit.size()<<std::endl;
   std::cout <<"TDC Hits:\t"     << fTDCHit.size()<<std::endl;
   std::cout <<"End Hits:\t"     << fEndHit.size()<<std::endl;
   std::cout <<"Bar Hits:\t"     << fBarHit.size()<<std::endl;
   std::cout <<"Clusters:\t"     << fClusters.size()<<std::endl;
   std::cout <<"TOFs:\t"         << fTOF.size()<<std::endl;
   std::cout << "End of TBarEvent::Print() ------"<<std::endl;
}

#endif
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
