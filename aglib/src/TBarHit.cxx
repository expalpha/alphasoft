
#include "TBarHit.h"
ClassImp(TBarHit)

TBarHit::TBarHit()
{
  fBarID=-1;
  fHitType=-1;
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasTime=false;
  fHasZed=false;
  fHasTopEndHit=false;
  fHasBotEndHit=false;
  fHasTopADCHit=false;
  fHasBotADCHit=false;
  fHasTopTDCHit=false;
  fHasBotTDCHit=false;
  fHasTPCHit=false;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
}

TBarHit::TBarHit(const TBarEndHit& tophit, const TBarEndHit& bothit) // Hit type 1
{
  fBarID=tophit.GetBarID();
  fHitType = 1;
  fTopEndHit = tophit;
  fBotEndHit = bothit;
  fTopADCHit = tophit.GetADCHit();
  fBotADCHit = bothit.GetADCHit();
  fTopTDCHit = tophit.GetTDCHit();
  fBotTDCHit = bothit.GetTDCHit();
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasZed=false;
  fHasTime=false;
  fHasTopEndHit=true;
  fHasBotEndHit=true;
  fHasTopADCHit=true;
  fHasBotADCHit=true;
  fHasTopTDCHit=true;
  fHasBotTDCHit=true;
  fHasTPCHit=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
}

TBarHit::TBarHit(const TBarEndHit& endhit, const TBarTDCHit& tdchit) // Hit type 2
{
  fBarID=endhit.GetBarID();
  fHitType = 2;
  if (endhit.IsTop()) {
    fTopEndHit = endhit;
    fTopADCHit = endhit.GetADCHit();
    fTopTDCHit = endhit.GetTDCHit();
    fBotTDCHit = tdchit;
    fHasTopEndHit=true;
    fHasBotEndHit=false;
    fHasTopADCHit=true;
    fHasBotADCHit=false;
    fHasTopTDCHit=true;
    fHasBotTDCHit=true;
  }
  if (endhit.IsBottom()) {
    fBotEndHit = endhit;
    fBotADCHit = endhit.GetADCHit();
    fTopTDCHit = tdchit;
    fBotTDCHit = endhit.GetTDCHit();
    fHasTopEndHit=false;
    fHasBotEndHit=true;
    fHasTopADCHit=false;
    fHasBotADCHit=true;
    fHasTopTDCHit=true;
    fHasBotTDCHit=true;
  }
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasZed=false;
  fHasTime=false;
  fHasTPCHit=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
}

TBarHit::TBarHit(const TBarEndHit& endhit, const TBarADCHit& adchit) // Hit type 3
{
  fBarID=endhit.GetBarID();
  fHitType = 3;
  if (endhit.IsTop()) {
    fTopEndHit = endhit;
    fTopADCHit = endhit.GetADCHit();
    fBotADCHit = adchit;
    fTopTDCHit = endhit.GetTDCHit();
    fHasTopEndHit=true;
    fHasBotEndHit=false;
    fHasTopADCHit=true;
    fHasBotADCHit=true;
    fHasTopTDCHit=true;
    fHasBotTDCHit=false;
  }
  if (endhit.IsBottom()) {
    fBotEndHit = endhit;
    fTopADCHit = adchit;
    fBotADCHit = endhit.GetADCHit();
    fBotTDCHit = endhit.GetTDCHit();
    fHasTopEndHit=false;
    fHasBotEndHit=true;
    fHasTopADCHit=true;
    fHasBotADCHit=true;
    fHasTopTDCHit=false;
    fHasBotTDCHit=true;
  }
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasZed=false;
  fHasTime=false;
  fHasTPCHit=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
}
TBarHit::TBarHit(const TBarEndHit& endhit) // Hit type 4
{
  fBarID=endhit.GetBarID();
  fHitType = 4;
  if (endhit.IsTop()) {
    fTopEndHit = endhit;
    fTopADCHit = endhit.GetADCHit();
    fTopTDCHit = endhit.GetTDCHit();
    fHasTopEndHit=true;
    fHasBotEndHit=false;
    fHasTopADCHit=true;
    fHasBotADCHit=false;
    fHasTopTDCHit=true;
    fHasBotTDCHit=false;
  }
  if (endhit.IsBottom()) {
    fBotEndHit = endhit;
    fBotADCHit = endhit.GetADCHit();
    fBotTDCHit = endhit.GetTDCHit();
    fHasTopEndHit=false;
    fHasBotEndHit=true;
    fHasTopADCHit=false;
    fHasBotADCHit=true;
    fHasTopTDCHit=false;
    fHasBotTDCHit=true;
  }
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasZed=false;
  fHasTime=false;
  fHasTPCHit=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
}
TBarHit::TBarHit(const TBarTDCHit& tophit, const TBarTDCHit& bothit) // Hit type 5
{
  fBarID=tophit.GetBarID();
  fTopTDCHit = tophit;
  fBotTDCHit = bothit;
  fHitType = 5;
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasZed=false;
  fHasTime=false;
  fHasTopEndHit=false;
  fHasBotEndHit=false;
  fHasTopADCHit=false;
  fHasBotADCHit=false;
  fHasTopTDCHit=true;
  fHasBotTDCHit=true;
  fHasTPCHit=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
}

TBarHit::TBarHit(const TBarTDCHit& tdchit, const TBarADCHit& adchit) // Hit type 6
{
  fBarID=tdchit.GetBarID();
  if (tdchit.IsTop()) {
    fBotADCHit = adchit;
    fTopTDCHit = tdchit;
    fHasTopEndHit=false;
    fHasBotEndHit=false;
    fHasTopADCHit=false;
    fHasBotADCHit=true;
    fHasTopTDCHit=true;
    fHasBotTDCHit=false;
  }
  if (tdchit.IsBottom()) {
    fTopADCHit = adchit;
    fBotTDCHit = tdchit;
    fHasTopEndHit=false;
    fHasBotEndHit=false;
    fHasTopADCHit=true;
    fHasBotADCHit=false;
    fHasTopTDCHit=false;
    fHasBotTDCHit=true;
  }
  fHitType = 6;
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasZed=false;
  fHasTime=false;
  fHasTPCHit=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
}
TBarHit::TBarHit(const TBarADCHit& tophit, const TBarADCHit& bothit) // Hit type 7
{
  fBarID=tophit.GetBarID();
  fTopADCHit = tophit;
  fBotADCHit = bothit;
  fHitType = 7;
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasZed=false;
  fHasTime=false;
  fHasTopEndHit=false;
  fHasBotEndHit=false;
  fHasTopADCHit=true;
  fHasBotADCHit=true;
  fHasTopTDCHit=false;
  fHasBotTDCHit=false;
  fHasTPCHit=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
}

TBarHit::TBarHit(const TBarTDCHit& hit) // Hit type 8
{
  fBarID=hit.GetBarID();
  if (hit.IsTop()) {
    fTopTDCHit = hit;
    fHasTopEndHit=false;
    fHasBotEndHit=false;
    fHasTopADCHit=false;
    fHasBotADCHit=false;
    fHasTopTDCHit=true;
    fHasBotTDCHit=false;
  }
  if (hit.IsBottom()) {
    fBotTDCHit = hit;
    fHasTopEndHit=false;
    fHasBotEndHit=false;
    fHasTopADCHit=false;
    fHasBotADCHit=false;
    fHasTopTDCHit=false;
    fHasBotTDCHit=true;
  }
  fHitType = 8;
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasZed=false;
  fHasTime=false;
  fHasTPCHit=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
}
TBarHit::TBarHit(const TBarADCHit& hit) // Hit type 9
{
  fBarID=hit.GetBarID();
  if (hit.IsTop()) {
    fTopADCHit = hit;
    fHasTopEndHit=false;
    fHasBotEndHit=false;
    fHasTopADCHit=true;
    fHasBotADCHit=false;
    fHasTopTDCHit=false;
    fHasBotTDCHit=false;
  }
  if (hit.IsBottom()) {
    fBotADCHit = hit;
    fHasTopEndHit=false;
    fHasBotEndHit=false;
    fHasTopADCHit=false;
    fHasBotADCHit=true;
    fHasTopTDCHit=false;
    fHasBotTDCHit=false;
  }  fHitType = 9;
  fZed=-9999;
  fZedADC=-9999;
  fTime=0;
  fHasZed=false;
  fHasTime=false;
  fHasTPCHit=false;
  fTPCTrackNum=-1;
  fClusterNum=-1;
  fIsInMainGroup=false;
  fIsGoodForTOF=false;
}

TBarHit::TBarHit(const TBarHit& h): TObject(h)
{
// copy ctor
  fBarID=h.GetBarID();
  fHitType = h.GetHitType();
  fTopEndHit = h.GetTopEndHit();
  fBotEndHit = h.GetBotEndHit();
  fTopADCHit = h.GetTopADCHit();
  fBotADCHit = h.GetBotADCHit();
  fTopTDCHit = h.GetTopTDCHit();
  fBotTDCHit = h.GetBotTDCHit();
  fTPCHit = h.GetTPCHit();
  fZed = h.GetZed();
  fZedADC = h.GetADCZed();
  fTime = h.GetTime();
  fHasTime = h.HasTime();
  fHasZed = h.HasZed();
  fHasTopEndHit = h.HasTopEndHit();
  fHasBotEndHit = h.HasBotEndHit();
  fHasTopADCHit = h.HasTopADCHit();
  fHasBotADCHit = h.HasBotADCHit();
  fHasTopTDCHit = h.HasTopTDCHit();
  fHasBotTDCHit = h.HasBotTDCHit();
  fHasTPCHit = h.HasTPCHit();
  fTPCTrackNum = h.GetTPCTrackNum();
  fClusterNum = h.GetClusterNum();
  fIsInMainGroup = h.IsInMainGroup();
  fIsGoodForTOF = h.IsGoodForTOF();
}

void TBarHit::Print()
{
   std::cout<<"BarHit::Print() -- Bar ID:"<<fBarID<<std::endl;
}


TBarHit::~TBarHit()
{


}
