#include "TBarCaliFile.h"
ClassImp(TBarCaliFile)

TBarCaliFile::TBarCaliFile()
{
  TBarCaliFile::Reset();
}

void TBarCaliFile::Print()
{
  std::cout<<"-----TBarCaliFile::Print()-----"<<std::endl;
  std::cout<<"Run number requested: "<<run_num_request<<std::endl;
  std::cout<<"Run number used: "<<run_num_used<<std::endl;
  std::cout<<"-------TBarCaliFile done-------"<<std::endl;
}

TBarCaliFile::~TBarCaliFile()
{
// dtor
}

void TBarCaliFile::Reset()
{
  run_num_request = -1;
  run_num_used = -1;
  filename_used = "";
  loaded = false;
  for (int i_bar=0; i_bar<num_bars; i_bar++) {
    global_offset[i_bar] = -1;
    tw_top[i_bar] = -1;
    tw_bot[i_bar] = -1;
    bot_top_offset[i_bar] = -1;
    bot_top_offset_err[i_bar] = -1;
    veff[i_bar] = -1;
    veff_err[i_bar] = -1;
    for (int j_bar=0; j_bar<num_bars; j_bar++) {
      bar_to_bar_offset[i_bar][j_bar] = -1;
      bar_to_bar_sigma[i_bar][j_bar] = -1;
    }
    adc_lambda[i_bar] = -1;
    adc_zed_offset[i_bar] = -1;
  }
  for (int i_end=0; i_end<num_ends; i_end++) {
    adc_noise_level[i_end] = -1;
  }

}

TBarCaliFile::TBarCaliFile(int run_num)
{
  TBarCaliFile::Reset();
  TBarCaliFile::LoadCali(run_num);
}

int TBarCaliFile::LoadCaliFile(std::string filename, bool verbose)
{
  TBarCaliFile::Reset();
  if (gSystem->AccessPathName(filename.data())) return 0;
  TFile* file = new TFile(filename.data());
  if (!file) return 0;
  TTree* tree;
  file->GetObject("BarrelCalibrationTree",tree);
  if (!tree) {
    if(file) delete file;
    return 0;
  }
  int barNumber;
  double barBotTopOffset, barBotTopOffsetErr, barVeff, barVeffErr, barTWtop, barTWbot, barGlobalOffset;
  double barADCnoiseTop, barADCnoiseBot, barADCLambda, barADCOffset;
  std::array<double, num_bars> barToBarOffset, barToBarSigma;
  tree->SetBranchAddress("barNumber",&barNumber);
  tree->SetBranchAddress("barBotTopOffset",&barBotTopOffset);
  tree->SetBranchAddress("barBotTopOffsetErr",&barBotTopOffsetErr);
  tree->SetBranchAddress("barVeff",&barVeff);
  tree->SetBranchAddress("barVeffErr",&barVeffErr);
  tree->SetBranchAddress("barTWtop",&barTWtop);
  tree->SetBranchAddress("barTWbot",&barTWbot);
  tree->SetBranchAddress("barGlobalOffset",&barGlobalOffset);
  tree->SetBranchAddress("barADCnoiseTop",&barADCnoiseTop);
  tree->SetBranchAddress("barADCnoiseBot",&barADCnoiseBot);
  tree->SetBranchAddress("barADCLambda",&barADCLambda);
  tree->SetBranchAddress("barADCOffset",&barADCOffset);
  for (int i_bar=0; i_bar<num_bars; i_bar++) {
    tree->SetBranchAddress(Form("barToBarOffset%d",i_bar),&barToBarOffset[i_bar]);
    tree->SetBranchAddress(Form("barToBarSigma%d",i_bar),&barToBarSigma[i_bar]);
  }
  Long64_t nentries = tree->GetEntries();
  if (nentries!=num_bars) {
    if (verbose) printf("TBarCaliFile::LoadCaliFile wrong number of bars ????\n");
    if(tree) delete tree;
    if(file) delete file;
    return 0;
  }
  for (Long64_t i=0;i<nentries;i++) {
    tree->GetEntry(i);
    if (std::isnan(barNumber) or i!=barNumber) {
      if (verbose) printf("TBarCaliFile::LoadCaliFile read error\n");
      continue;
    }
    bot_top_offset[i] = barBotTopOffset;
    bot_top_offset_err[i] = barBotTopOffsetErr;
    veff[i] = barVeff;
    veff_err[i] = barVeffErr;
    tw_top[i] = barTWtop;
    tw_bot[i] = barTWbot;
    global_offset[i] = barGlobalOffset;
    for (int j=0; j<num_bars; j++) {
      bar_to_bar_offset[i][j] = barToBarOffset[j];
      bar_to_bar_sigma[i][j] = barToBarSigma[j];
    }
    adc_noise_level[i] = barADCnoiseBot;
    adc_noise_level[i+num_bars] = barADCnoiseTop;
    adc_lambda[i] = barADCLambda;
    adc_zed_offset[i] = barADCOffset;
  }
  if (verbose) printf("TBarCaliFile::LoadCali file successfully loaded from %s\n",filename.data());
  filename_used = filename;
  loaded = true;
  if(tree) delete tree;
  if(file) delete file;
  return 1;
}


int TBarCaliFile::LoadCali(int run_num, bool verbose)
{
  if (verbose) printf("TBarCaliFile::LoadCali attempting to load barrel calibration file \n");
  int run_num_current = run_num;
  std::string filename;

  while (run_num_current > run_num-max_age) {
    filename = Form("%s/BarrelCalibration%05d.root",getenv("AGOUTPUT"),run_num_current);
    int good = TBarCaliFile::LoadCaliFile(filename.data(), false);
    if (good) {
      run_num_used = run_num_current;
      break;
    }
    run_num_current--;
  }
  
  run_num_request = run_num;
  if (run_num_used==-1) {
    if (verbose) printf("TBarCaliFile::LoadCali failed to find a calibration file for any run between run %d to run %d\n",run_num-max_age,run_num);
    return 0;
  }
  else {
    if (verbose) printf("TBarCaliFile::LoadCali file successfully loaded from run %d - file %s\n",run_num_current,filename.data());
    return 1;
  }

}