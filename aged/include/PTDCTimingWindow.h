//==============================================================================
// File:        PTDCTimingWindow.h
//
// Description: Window to display TDC timing
//
// Gareth Smith, adapted from
// Copyright (c) 2017, Phil Harvey, Queen's University
//==============================================================================
#ifndef __PTDCTimingWindow_h__
#define __PTDCTimingWindow_h__

#include <Xm/Xm.h>
#include "PImageWindow.h"
#include "PListener.h"
#include "PMenu.h"
#include "ImageData.h"

class PHistImage;

class PTDCTimingWindow : public PImageWindow, public PListener, public PMenuHandler {
public:
    PTDCTimingWindow(ImageData *data);
    virtual ~PTDCTimingWindow();
    
    virtual void    UpdateSelf();
    virtual void    Listen(int message, void *message_data);
    virtual void    DoMenuCommand(int anID);
    virtual void    ScrollValueChanged(EScrollBar bar, int value);

private:
    void            SetChannels(int chan_mask);
    
    Widget          mChannel[kMaxWaveformChannels]; // channel canvas widgets
    PHistImage    * mHist[kMaxWaveformChannels];
    int             mLastNum;                       // hit number of last display waveforms
    int             mChanMask;                      // channels shown
};


#endif // __PTDCTimingWindow_h__
