//==============================================================================
// File:        ImageData.h
//
// Copyright (c) 2017, Phil Harvey, Queen's University
//==============================================================================
#ifndef __ImageData_h__
#define __ImageData_h__

#include "AgedResource.h"
#include "AgedWindow.h"
#include "PSpeaker.h"
#include "PProjImage.h"
#include "PHistImage.h"
#include <vector>
#include <map>
#include <array>

#define MAX_FNODES      4               // maximum number of nodes per face
#define NODE_HID        0x01            // node is hidden
#define NODE_OUT        0x02            // node is outside plotting area
#define NODE_MISSED     0x04            // node in cherenkov cone missed intersecting sphere
#define EDGE_HID        0x01            // edge is hidden
#define FACE_HID        0x01            // face is hidden
#define FACE_COL_SHFT   2               // bit shift for colour in face flags

#define FORMAT_LEN      512             // maximum length of event label
#define FILELEN         4096            // maximum length of file name
//#define FILELEN         8192

//#define AG_SCALE        150.0           // size of Alpha-G geometry (units?)
#define AG_SCALE        800.0           // size of Alpha-G geometry (units?)
#define BV_SCALE        0.8             // spacepoint XYZ is reported in mm, barhit XYZ is reported in m, pretty sure this is the correct scaling - GS

#ifndef PI
#define PI              3.14159265358979324
#endif

#define MAX_EDGES       350
#define NUM_AG_WIRES    256             //TEST
#define NUM_AG_PADS     (32 * 576)      //TEST
#define NUM_AG_BARS     64

enum HitInfoFlags {
    HIT_NORMAL      = 0x01,
    HIT_ALL_MASK    = HIT_NORMAL,
    HIT_DISCARDED   = 0x100,
    HIT_OVERSCALE   = 0x200,
    HIT_UNDERSCALE  = 0x400,
    HIT_HIDDEN      = 0x800
};
enum BarInfoFlags {
    BAR_HIT_NORMAL      = 0x01,
    BAR_HIT_ALL_MASK    = HIT_NORMAL,
    BAR_HIT_DISCARDED   = 0x100,
    BAR_HIT_OVERSCALE   = 0x200,
    BAR_HIT_UNDERSCALE  = 0x400,
    BAR_HIT_HIDDEN      = 0x800
};

enum ESmoothFlags {
    kSmoothText     = 0x01,
    kSmoothLines    = 0x02
};

struct Point3 {
    float       x,y,z;
};

struct Node {
    float       x3,y3,z3;           // physical sphere coordinates (radius=1)
    float       xr,yr,zr;           // rotated physical coordinates
    int         x,y;                // screen coordinates after rotating
    int         flags;              // flag for x,y outside limits
};

struct Face {
    int         num_nodes;
    int         flags;
    Node        *nodes[MAX_FNODES];
    Point3      norm;               // unit vector normal to face
};

struct Edge {
    Node        *n1, *n2;           // nodes for endpoints of edge
    Face        *f1, *f2;           // associated faces
    int         flags;              // used to indicate a hidden line
};

struct WireFrame {
    int         num_nodes;
    int         num_edges;
    Node        *nodes;
    Edge        *edges;
};

struct Polyhedron {
    int         num_nodes;
    int         num_edges;
    int         num_faces;
    float       radius;
    Node        *nodes;
    Edge        *edges;
    Face        *faces;
};

struct BarInfo {
    bool        matched;            // matched to TPC track
    bool        complete;           // both ends of the bar are hit
    bool        main;
    int         hittype;
    int         track_num;
    short       hit_val;            // colour index for drawing this hit
    short       barID;              // bar number
    short       clusterID;
    bool        main_group;
    short       index;              // index of hi in position array
    short       flags;              // hit info flags
    double      TDCtop;
    double      TDCbot;
    double      TDCtop_raw;
    double      TDCbot_raw;
    double      TDCtop_cc;
    double      TDCbot_cc;
    double      TDCtop_TOT;
    double      TDCbot_TOT;
    double      ADCtop;
    double      ADCbot;
    double      combADC;
    double      ADCtop_start;
    double      ADCbot_start;
    double      ADCtop_stop;
    double      ADCbot_stop;
    double      time;
    std::vector<double> TDCtop_all;
    std::vector<double> TDCbot_all;
    std::vector<double> TDCtop_all_reflection;
    std::vector<double> TDCbot_all_reflection;
    std::array<double, 5> ADCtop_fit_params;
    std::array<double, 5> ADCbot_fit_params;
};

struct HitInfo {
    int         wire;               // wire number
    int         pad;                // pad number
    float       wireheight;             // pulse height
    float       padheight;             // pulse height
    float       time;               // pulse time
    float       error[3];           // error in XYZ position
    float       d2;                 // XY distance to track
    float       dz;                 // Z distance to track
    short       finder_status;      // track finder status
    short       flags;              // hit info flags
    short       hit_val;            // colour index for drawing this hit
    short       index;              // index of hi in position array
    short       track;              // track number
};

struct TrackInfo {
    
    int         track_num;
    int         status;
    bool        double_branched;
    double      c;
    double      Rc;
    double      phi0;
    double      D;
    double      lambda;
    double      z0;
    double      x0;
    double      y0;
    double      err2c;
    double      err2Rc;
    double      err2phi0;
    double      err2D;
    double      err2lambda;
    double      err2z0;
    double      chi2r;
    double      chi2z;
    double      pZ;
    double      pX;
    double      pY;
    double      branch;
    bool        has_bar;
};

struct BarPoints {
    int         num_nodes;
    Node        *nodes;
    BarInfo     *bar_info;
};


struct SpacePoints {
    int         num_nodes;
    Node        *nodes;
    HitInfo     *hit_info;          // corresponding array of hit information
};

struct TrackPoints {
    int         num_nodes;
    Node        *nodes;
    TrackInfo   *track_info;
};

class TStoreEvent;
// class AgAnalysisFlow;
// class AgSignalsFlow;
// class AgBarEventFlow;
class TBarEvent;
class TAGDetectorEvent;
class TBarHit;
struct AgEvent;
namespace ALPHAg {
class wf_ref;
}

struct ImageData : AgedResource {
    AgedWindow    * mMainWindow;        // main Aged window
    PWindow       * mWindow[NUM_WINDOWS];// Aged windows
    PSpeaker      * mSpeaker;           // broadcasts messages to listeners
    MenuStruct    * main_menu;          // pointer to menu struct
    PProjImage    * mLastImage;         // last projection image to transform hits
    PProjImage    * mCursorImage;       // last image to be cursor'd in
    PHistImage    * mScaleHist;         // histogram image currently being scaled
    int             mNext;              // true to step to next event (exit event loop)

    TStoreEvent   * agEvent;            // the event we are displaying
    // AgAnalysisFlow* anaFlow;            // the analysis flow
    // AgSignalsFlow * sigFlow;            // the signals flow
    const std::vector<ALPHAg::wf_ref> *AWwf;
    const std::vector<ALPHAg::wf_ref> *PADwf;

    std::map<int,std::vector<double>*> *BVwf;

    // const AgEvent*        age;                // ALPHAg event (ADC data)
    Widget          toplevel;           // top level Aged widget
    SpacePoints     hits;               // tube hit information
    // AgBarEventFlow* barFlow;
    const TBarEvent*      barEvent;
    const TAGDetectorEvent* detectorEvent; // Cuts information
    BarPoints       barpoints;          // BV hit information
    TrackPoints       tracks;          // Track fit information
    short             n_tracks;           // Number of tracks
    short             n_helices;           // Number of helices
    short             n_clusters;
    float            max_tdc_time;
    float            min_tdc_time;
    float            upper_limit_tdc_time;
    float            lower_limit_tdc_time;
    float            max_adc_amp;
    float            min_adc_amp;
    
    Node            sun_dir;            // direction to sun
    int             num_disp;           // number of displayed hits
    float           angle_conv;         // conversion from radians to displayed angle units

    // menu ID (IDM_) variables
    int             wDataTypeTPC;       // flag for displayed data type
    int             wDataTypeBV;        // flag for displayed data type
    int             wSpStyle;           // flag for displayed space point style
    int             wProjType;          // current projection type
    int             wShapeOption;       // current projection hit shape option

    char          * projName;           // name of current projection
    char          * dispNameTPC;           // name of data display type
    char          * dispNameBV;           // name of data display type
    int             cursor_hit;         // hit index for current cursor location
    int             cursor_hit_type;        // 1=spacepoint, 2=bar hit, 3=track
    int             cursor_sticky;      // flag for sticky hit cursor
    char            print_string[2][FILELEN];// print command(0)/filename(1) strings
    char            label_format[FORMAT_LEN];// format of event label
    long            event_id;           // global trigger ID of currently displayed event
    double          event_time;         // time of event
    char          * argv;               // command line to run program
    
    double          display_time;       // time to display next event

    int             trigger_flag;       // trigger flag (continuous/single/off)
    long            run_number;         // run number for event
    int             last_cur_x;         // last cursor x location
    int             last_cur_y;         // last cursor y location
};

// ImageData routines
void    freePoly(Polyhedron *poly);
void    freeWireFrame(WireFrame *frame);
void    initNodes(WireFrame *fm, Point3 *pt, int num);
void    initEdges(WireFrame *fm, int *n1, int *n2, int num);
const char *  loadGeometry(Polyhedron *poly, int geo, char *argv);
void    transform(Node *node, Projection *pp, int num);
void    transformPoly(Polyhedron *poly, Projection *pp);
struct tm *getTms(double aTime, int time_zone);
int isIntegerDataType(ImageData *data);

void    initData(ImageData *data, int load_settings);
void    freeData(ImageData *data);
void    deleteData(ImageData *data);
void    sendMessage(ImageData *data, int message, void *dataPt=NULL);
void    aged_next(ImageData *data, int dir);
void    setTriggerFlag(ImageData *data, int theFlag, int end_of_data=0);
void    setLabel(ImageData *data, int on);
float   getHitValPad(ImageData *data, HitInfo *hi);
float   getHitValPad(ImageData *data, BarInfo *bi);
void    calcHitVals(ImageData *data);
void    calcBarVals(ImageData *data);
void    clearEvent(ImageData *data);

#endif // __ImageData_h__
