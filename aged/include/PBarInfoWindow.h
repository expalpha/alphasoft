//==============================================================================
// File:        PBarInfoWindow.h
//
// Copyright (c) 2017, Phil Harvey, Queen's University
//==============================================================================
#ifndef __PBarInfoWindow_h__
#define __PBarInfoWindow_h__

#include <Xm/Xm.h>
#include "PWindow.h"
#include "PListener.h"
#include "PLabel.h"


class PBarInfoWindow : public PWindow, public PListener {
public:
    PBarInfoWindow(ImageData *data);
    ~PBarInfoWindow();
    
    virtual void    UpdateSelf();
    virtual void    Listen(int message, void *message_data);
    
private:
    static void     NextProc(Widget w, PBarInfoWindow *win, caddr_t call_data);
    static void     PrevProc(Widget w, PBarInfoWindow *win, caddr_t call_data);

    void            ClearEntries();
    void            SetHitXYZ();
    void            ManageXYZ(int manage);
    void            ResizeToFit();
    
    PLabel          bar_index, bar_num, bar_cluster, bar_main_group;
    PLabel          bar_top_adc, bar_bot_adc, bar_comb_adc, bar_bot_adc_time, bar_top_adc_time, bar_top_tdc, bar_bot_tdc;
    PLabel          bar_type, bar_match, bar_n_tdc_top, bar_n_tdc_bot, bar_time;
    PLabel          bar_xyz_labels[3], bar_xyz[3];
    int             mLastNum;
};


#endif // __PBarInfoWindow_h__
