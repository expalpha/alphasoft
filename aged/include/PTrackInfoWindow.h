//==============================================================================
// File:        PTrackInfoWindow.h
//
// Copyright (c) 2017, Phil Harvey, Queen's University
//==============================================================================
#ifndef __PTrackInfoWindow_h__
#define __PTrackInfoWindow_h__

#include <Xm/Xm.h>
#include "PWindow.h"
#include "PListener.h"
#include "PLabel.h"


class PTrackInfoWindow : public PWindow, public PListener {
public:
    PTrackInfoWindow(ImageData *data);
    ~PTrackInfoWindow();
    
    virtual void    UpdateSelf();
    virtual void    Listen(int message, void *message_data);
    
private:
    static void     NextProc(Widget w, PTrackInfoWindow *win, caddr_t call_data);
    static void     PrevProc(Widget w, PTrackInfoWindow *win, caddr_t call_data);

    void            ClearEntries();
    void            SetHitXYZ();
    void            ManageXYZ(int manage);
    void            ResizeToFit();
    
    PLabel          track_index, track_status, track_has_bar;
    PLabel          track_rc, track_phi0, track_D, track_lambda;
    PLabel          track_x0, track_y0, track_z0;
    PLabel          track_px, track_py, track_pz;
    PLabel          track_chi2r, track_chi2z;
    int             mLastNum;
};


#endif // __PTrackInfoWindow_h__
