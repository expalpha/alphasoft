//==============================================================================
// File:        PEventInfoWindow.cxx
//
// Description: Window to display event information in text form
//
// Copyright (c) 2017, Phil Harvey, Queen's University
//
// Notes:       11/11/99 - PH To reduce flickering of text items in this window,
//              I'm now calling SetStringNow() instead of SetString().  Previously,
//              the flickering could be so bad that the text could be invisible
//              most of the time.
//==============================================================================

#include <Xm/RowColumn.h>
#include <Xm/Label.h>
#include <Xm/Form.h>
#include <time.h>
#include "ImageData.h"
#include "PEventInfoWindow.h"
#include "PUtils.h"
#include "PSpeaker.h"
#include "TStoreEvent.hh"
#include "TAGDetectorEvent.hh"

//---------------------------------------------------------------------------
// PEventInfoWindow constructor
//
PEventInfoWindow::PEventInfoWindow(ImageData *data)
                : PWindow(data)
{
    Widget  rc1, rc2;
    int     n;
    Arg     wargs[16];

    mTimeZone = 0;

    data->mSpeaker->AddListener(this);
    
    n = 0;
    XtSetArg(wargs[n], XmNtitle, "Event Info"); ++n;
    XtSetArg(wargs[n], XmNx, 175); ++n;
    XtSetArg(wargs[n], XmNy, 175); ++n;
    XtSetArg(wargs[n], XmNminWidth, 182); ++n;
    XtSetArg(wargs[n], XmNminHeight, 100); ++n;
    SetShell(CreateShell("eiPop",data->toplevel,wargs,n));
    SetMainPane(XtCreateManagedWidget("agedForm", xmFormWidgetClass,GetShell(),NULL,0));

    n = 0;
    XtSetArg(wargs[n], XmNpacking,          XmPACK_COLUMN); ++n;
    XtSetArg(wargs[n], XmNleftAttachment,   XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM); ++n;
    rc1 = XtCreateManagedWidget("eiRC1",xmRowColumnWidgetClass,GetMainPane(),wargs,n);
    
    XtCreateManagedWidget("Event #:",    xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Run #:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Event time (s):",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("NHits:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("BV ADC:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("BV TDC:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("BV Half:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("BV Complete:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("BV+TPC:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Clusters:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("TOF pairs:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("#TOF < 20ns:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Tracks:",     xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Lines:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Helices:",    xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Good Helices:",    xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Vertex X:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Vertex Y:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Vertex Z:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Vertex Status:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Sim X:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Sim Y:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Sim Z:",   xmLabelWidgetClass,rc1,NULL,0);
    TAGDetectorEvent dummyDetEvt;
    std::vector<std::string> cut_names = dummyDetEvt.CutNames();
    int Ncuts = cut_names.size();
    for (int i_cut=0; i_cut<Ncuts; i_cut++) {
        XtCreateManagedWidget(cut_names[i_cut].data(),   xmLabelWidgetClass,rc1,NULL,0);
    }

    n = 0;
    XtSetArg(wargs[n], XmNpacking,          XmPACK_COLUMN); ++n;
    XtSetArg(wargs[n], XmNleftAttachment,   XmATTACH_WIDGET); ++n;
    XtSetArg(wargs[n], XmNleftWidget, rc1); ++n;
    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNrightAttachment,  XmATTACH_FORM); ++n;
    rc2 = XtCreateManagedWidget("eiRC2",xmRowColumnWidgetClass,GetMainPane(),wargs,n);
    
    tw_evt      .CreateLabel("evt",      rc2,NULL,0);
    tw_run      .CreateLabel("run",      rc2,NULL,0);
    tw_time     .CreateLabel("time",      rc2,NULL,0);
    tw_nhit     .CreateLabel("nhit",     rc2,NULL,0);
    tw_adc  .CreateLabel("bv_adc",  rc2,NULL,0);
    tw_tdc  .CreateLabel("bv_tdc",  rc2,NULL,0);
    tw_half  .CreateLabel("bv_half",  rc2,NULL,0);
    tw_complete  .CreateLabel("bv_complete",  rc2,NULL,0);
    tw_match .CreateLabel("bv_tpc",  rc2,NULL,0);
    tw_clusters .CreateLabel("clusters",  rc2,NULL,0);
    tw_tof .CreateLabel("tof",  rc2,NULL,0);
    tw_good_tof .CreateLabel("good_tof",  rc2,NULL,0);
    tw_tracks   .CreateLabel("tracks",   rc2,NULL,0);
    tw_lines    .CreateLabel("lines",    rc2,NULL,0);
    tw_helices  .CreateLabel("helices",  rc2,NULL,0);
    tw_helices_good  .CreateLabel("helices_good",  rc2,NULL,0);
    tw_vertexX  .CreateLabel("vx",      rc2,NULL,0);
    tw_vertexY  .CreateLabel("vy",      rc2,NULL,0);
    tw_vertexZ  .CreateLabel("vz",      rc2,NULL,0);
    tw_vertex_status  .CreateLabel("vstat",      rc2,NULL,0);
    tw_simX  .CreateLabel("sx",      rc2,NULL,0);
    tw_simY  .CreateLabel("sy",      rc2,NULL,0);
    tw_simZ  .CreateLabel("sz",      rc2,NULL,0);    
    for (int i_cut=0; i_cut<Ncuts; i_cut++) {
        PLabel* cut_label = new PLabel();
        cut_label->CreateLabel(cut_names[i_cut].data(),      rc2,NULL,0);
        cuts.push_back(cut_label);
    }
}

void PEventInfoWindow::Listen(int message, void*/*message_data*/)
{
    switch (message) {
        case kMessageNewEvent:
        case kMessageEventCleared:
        case kMessageTimeFormatChanged:
        case kMessageEvIDFormatChanged:
        case kMessageHitsChanged:
            SetDirty();
            break;
    }
}

// UpdateSelf
void PEventInfoWindow::UpdateSelf()
{
    ImageData   *data = GetData();
    char        buff[256];
    TStoreEvent *evt = data->agEvent;
    const TBarEvent *barEvt = data->barEvent;
    const TAGDetectorEvent* detEvt = data->detectorEvent;
 
#ifdef PRINT_DRAWS
    Printf("-updateEventInfo\n");
#endif

    if (!evt) {
        strcpy(buff, "-");
        tw_evt      .SetStringNow(buff);
        tw_run      .SetStringNow(buff);
        tw_time      .SetStringNow(buff);
        tw_nhit     .SetStringNow(buff);
        tw_adc  .SetStringNow(buff);
        tw_tdc  .SetStringNow(buff);
        tw_complete  .SetStringNow(buff);
        tw_half  .SetStringNow(buff);
        tw_match  .SetStringNow(buff);
        tw_clusters  .SetStringNow(buff);
        tw_tof  .SetStringNow(buff);
        tw_good_tof  .SetStringNow(buff);
        tw_tracks   .SetStringNow(buff);
        tw_lines    .SetStringNow(buff);
        tw_helices  .SetStringNow(buff);
        tw_helices_good  .SetStringNow(buff);
        tw_vertexX  .SetStringNow(buff);
        tw_vertexY  .SetStringNow(buff);
        tw_vertexZ  .SetStringNow(buff);
        tw_vertex_status  .SetStringNow(buff);
        tw_simX  .SetStringNow(buff);
        tw_simY  .SetStringNow(buff);
        tw_simZ  .SetStringNow(buff);
        std::vector<std::string> cut_names = detEvt->CutNames();
        int Ncuts = cut_names.size();
        for (int i_cut=0; i_cut<Ncuts; i_cut++) {
            cuts[i_cut]->SetStringNow(buff);
        }
        return;
    }

    snprintf(buff,256,data->hex_id ? "0x%.6lx" : "%ld",(long)evt->GetEventNumber());
    tw_evt.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)data->run_number);
    tw_run.SetStringNow(buff);
    snprintf(buff,256, "%.3f", (double)data->event_time);
    tw_time.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)evt->GetNumberOfPoints());
    tw_nhit.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)barEvt->GetNumGoodADC());
    tw_adc.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)barEvt->GetNumGoodTDC());
    tw_tdc.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)barEvt->GetNumBarsComplete());
    tw_complete.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)(barEvt->GetNumBarsHalfComplete()));
    tw_half.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)barEvt->GetNumMatchedBars());
    tw_match.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)barEvt->GetNumClusters());
    tw_clusters.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)barEvt->GetNumTOF());
    tw_tof.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)barEvt->GetNumGoodTOF());
    tw_good_tof.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)evt->GetNumberOfTracks());
    tw_tracks.SetStringNow(buff);
    snprintf(buff,256, "%d", evt->GetLineArray() ? evt->GetLineArray()->GetEntries() : 0);
    tw_lines.SetStringNow(buff);
    snprintf(buff,256, "%d", evt->GetHelixArray() ? evt->GetHelixArray()->GetEntries() : 0);
    tw_helices.SetStringNow(buff);
    snprintf(buff,256, "%ld", (long)evt->GetNumberOfGoodHelices());
    tw_helices_good.SetStringNow(buff);
    if (evt->GetVertex().X() < -998) {
        strcpy(buff,"-");
    } else {
        snprintf(buff,256, "%g", evt->GetVertex().X());
    }
    tw_vertexX.SetStringNow(buff);
    if (evt->GetVertex().Y() < -998) {
        strcpy(buff,"-");
    } else {
        snprintf(buff,256, "%g", evt->GetVertex().Y());
    }
    tw_vertexY.SetStringNow(buff);
    if (evt->GetVertex().Z() < -998) {
        strcpy(buff,"-");
    } else {
        snprintf(buff,256, "%g", evt->GetVertex().Z());
    }
    tw_vertexZ.SetStringNow(buff);
    snprintf(buff,256, "%d", evt->GetVertexStatus() );
    tw_vertex_status.SetStringNow(buff);
    if (!evt->GetSimEvent()) {
        strcpy(buff,"-");
    } else {
        snprintf(buff,256, "%g", evt->GetSimEvent()->GetVertexX());
    }
    tw_simX.SetStringNow(buff);
    if (!evt->GetSimEvent()) {
        strcpy(buff,"-");
    } else {
        snprintf(buff,256, "%g", evt->GetSimEvent()->GetVertexY());
    }
    tw_simY.SetStringNow(buff);
    if (!evt->GetSimEvent()) {
        strcpy(buff,"-");
    } else {
        snprintf(buff,256, "%g", evt->GetSimEvent()->GetVertexZ());
    }
    tw_simZ.SetStringNow(buff);
    std::vector<std::string> cut_names = detEvt->CutNames();
    std::vector<bool> cut_results = detEvt->fCutsResult;
    int Ncuts = cut_names.size();
    for (int i_cut=0; i_cut<Ncuts; i_cut++) {
        snprintf(buff,256, cut_results[i_cut] ? "pass" : "fail");
        cuts[i_cut]->SetStringNow(buff);
    }

}
