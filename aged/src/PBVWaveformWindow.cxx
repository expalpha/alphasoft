//==============================================================================
// File:        PBVWaveformWindow.cxx
//
// Description: Window to display waveform data
//
// Revisions:   2012/03/06 - PH Created (borrowed heavily from PNCDScopeWindow.cxx)
//
// Copyright (c) 2017, Phil Harvey, Queen's University
//==============================================================================

#include <Xm/RowColumn.h>
#include <Xm/Form.h>
#include <Xm/DrawingA.h>
#include <math.h>
#include "PBVWaveformWindow.h"
#include "PHistImage.h"
#include "ImageData.h"
#include "PSpeaker.h"
#include "CUtils.h"
#include "AgFlow.h"
#include "RecoFlow.h"

const int   kDirtyEvent     = 0x02;
const int   kDirtyAll       = 0x04;
const int   kShowChannels   = 0x03;     // show only channels 0 and 1 to start

enum {
    kWireHist,      // index for anode wire histogram in Waveform window
    kPadHist,       // index for pad histogram in Waveform window
    kNumHists
};

enum {
    kTopBVHist,      // index for anode wire histogram in Waveform window
    kBottomBVHist,       // index for pad histogram in Waveform window
    kNumBVHists
};

static MenuStruct channels_menu[] = {
//      { "Add Overlay",      0, XK_A, IDM_WAVE_ADD_OVERLAY,   NULL, 0, 0 },
      { "Clear Overlays",   0, XK_C, IDM_WAVE_CLEAR_OVERLAY, NULL, 0, 0 },
    //{ NULL,               0, 0,       0,                  NULL, 0, 0},
};

static MenuStruct wave_main_menu[] = {
    { "Display",    0, 0,   0, channels_menu, XtNumber(channels_menu), 0 },
};

static const char * hist_label[kMaxWaveformChannels] = { "Top ADC", "Bottom ADC", "","","","","","" };

//---------------------------------------------------------------------------
// PBVWaveformWindow constructor
//
PBVWaveformWindow::PBVWaveformWindow(ImageData *data)
           : PImageWindow(data), mLastNum(-1), mChanMask(0)
{
    int     n;
    Arg     wargs[20];

    data->mSpeaker->AddListener(this);
    
    n = 0;
    XtSetArg(wargs[n], XmNtitle, "BV Waveforms"); ++n;
    XtSetArg(wargs[n], XmNx, 400);             ++n;
    XtSetArg(wargs[n], XmNy, 300);             ++n;
    XtSetArg(wargs[n], XmNminWidth, 200);      ++n;
    XtSetArg(wargs[n], XmNminHeight, 200);     ++n;
    SetShell(CreateShell("wavePop",data->toplevel,wargs,n));
    
    n = 0;
    XtSetArg(wargs[n], XmNwidth, 600);         ++n;
    XtSetArg(wargs[n], XmNheight, 400);        ++n;
    XtSetArg(wargs[n], XmNbackground, data->colour[BKG_COL]); ++n;
    SetMainPane(XtCreateManagedWidget("waveMain", xmFormWidgetClass,GetShell(),wargs,n));

    n = 0;
    XtSetArg(wargs[n],XmNmarginHeight,      1); ++n;
    XtSetArg(wargs[n],XmNleftAttachment,    XmATTACH_FORM); ++n;
    XtSetArg(wargs[n],XmNtopAttachment,     XmATTACH_FORM); ++n;
    XtSetArg(wargs[n],XmNrightAttachment,   XmATTACH_FORM); ++n;
    Widget menu = XmCreateMenuBar(GetMainPane(), (char*)"agedMenu" , wargs, n);
    XtManageChild(menu);
    CreateMenu(menu, wave_main_menu, XtNumber(wave_main_menu), this);

//    GetMenu()->SetToggle(IDM_WAVE_SUBT,      data->wave_subt);
//    GetMenu()->SetToggle(IDM_WAVE_SUM,       data->wave_sum);
//    GetMenu()->SetToggle(IDM_WAVE_PULSE,     data->wave_pulse);
//    GetMenu()->SetToggle(IDM_QT_PULSE,       data->qt_pulse);
//    GetMenu()->SetToggle(IDM_WAVE_CURSOR,    data->wave_cursor);

    // add resource labels to channels in menu
/*    for (int i=0; i<kWaveformRsrcNum; ++i) {
        if (!strncmp(data->wave_lbl[i], "Channel ", 8)) continue;
        char buff[256];
        snprintf(buff,256, "Channel %d - %s", i, data->wave_lbl[i]);
        GetMenu()->SetLabel(IDM_WAVE_0+i, buff);
    }*/

    n = 0;
    XtSetArg(wargs[n], XmNtopAttachment, XmATTACH_WIDGET);  ++n;
    XtSetArg(wargs[n], XmNtopWidget, menu); ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM);  ++n;
    XtSetArg(wargs[n], XmNrightAttachment, XmATTACH_FORM);  ++n;
    XtSetArg(wargs[n], XmNwidth, 15);  ++n;
    XtSetArg(wargs[n], XmNorientation, XmVERTICAL);  ++n;
    NewScrollBar(kScrollRight,(char*)"waveScroll",wargs,n);
    SetScrollValue(kScrollRight, kScrollMax/2, 0);

    n = 0;
    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_WIDGET);    ++n;
    XtSetArg(wargs[n], XmNtopWidget,        menu);               ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM);      ++n;
    XtSetArg(wargs[n], XmNleftAttachment,   XmATTACH_FORM);      ++n;
    XtSetArg(wargs[n], XmNrightAttachment,  XmATTACH_WIDGET);    ++n;
    XtSetArg(wargs[n], XmNrightWidget,      GetScroll(kScrollRight)); ++n;
    XtSetArg(wargs[n], XmNbackground,       data->colour[BKG_COL]); ++n;
    Widget form = XtCreateManagedWidget("waveForm", xmFormWidgetClass,GetMainPane(),wargs,n);
    
    for (int i=0; i<kMaxWaveformChannels; ++i) {
        n = 0;
        XtSetArg(wargs[n], XmNbackground,           data->colour[BKG_COL]); ++n;
        XtSetArg(wargs[n], XmNleftAttachment,       XmATTACH_FORM);      ++n;
        XtSetArg(wargs[n], XmNrightAttachment,      XmATTACH_FORM);      ++n;
        mChannel[i] = XtCreateManagedWidget(hist_label[i], xmDrawingAreaWidgetClass, form, wargs, n);

        // create histogram
        mHist[i] = new PHistImage(this, mChannel[i], 0);
        mHist[i]->AllowLabel(FALSE);
        mHist[i]->SetScaleLimits(0,15000,10);
        mHist[i]->SetScaleMin(0);
        mHist[i]->SetScaleMax(4096);
        mHist[i]->SetYMin(data->bv_wave_min[i]);
        mHist[i]->SetYMax(data->bv_wave_max[i]);
        mHist[i]->SetStyle(kHistStyleSteps);
        mHist[i]->SetFixedBins();
        mHist[i]->SetPlotCol(WAVEFORM_COL);
//        mHist[i]->SetOverlayCol(SCOPE0_COL);
//        mHist[i]->SetAutoScale(1);
        mHist[i]->SetCursorTracking(1);
//        mHist[i]->SetLabel(hist_label[i]);
    }
    // arrange our channel histogram widgets in the window
    SetChannels(kShowChannels);
        
    SetDirty(kDirtyEvent);
}

PBVWaveformWindow::~PBVWaveformWindow()
{
    ImageData *data = GetData();
    for (int i=0; i<kMaxWaveformChannels; ++i) {
        // update current Y scale settings
        data->bv_wave_min[i] = mHist[i]->GetYMin();
        data->bv_wave_max[i] = mHist[i]->GetYMax();
        delete mHist[i];
        mHist[i] = NULL;
    }
    SetImage(NULL);     // make sure base class doesn't re-delete our image
}

void PBVWaveformWindow::Listen(int message, void *message_data)
{
    switch (message) {
        case kMessageNewEvent:
        case kMessageEventCleared:
            for (int i=0; i<kMaxWaveformChannels; ++i) {
                if (mChanMask & (1 << i)) mHist[i]->ClearOverlays();
            }
            SetDirty(kDirtyEvent);
            break;
        case kMessageColoursChanged:
        case kMessageSmoothTextChanged:
        case kMessageSmoothLinesChanged:
            SetDirty(kDirtyAll);
            for (int i=0; i<kMaxWaveformChannels; ++i) {
                if (mChanMask & (1 << i)) mHist[i]->SetDirty();
            }
            break;
        case kMessageCursorHit:
            if (mLastNum != mData->cursor_hit) {
                // only dirty if this is a different hit
                SetDirty(kDirtyEvent);
            }
            break;
        case kMessageAddOverlay:
            if (mData->cursor_hit >= 0) {
                // add to overlays if not done already
                for (int i=0; i<kMaxWaveformChannels; ++i) {
                    if (mChanMask & (1 << i)) {
                        char *lbl = mHist[i]->GetLabel();
                        int j;
                        if (lbl) {
                            for (j=mHist[i]->GetNumOverlays()-1; j>=0; --j) {
                                char *pt = mHist[i]->GetOverlayLabel(j);
                                if (pt && !strcmp(pt, lbl)) {
                                    break;
                                }
                            }
                            if (j > 0) {
                                mHist[i]->DeleteOverlay(j);
                            }
                            if (j != 0) {
                                mHist[i]->AddOverlay();
                                SetDirty(kDirtyEvent);
                            }
                        }
                    }
                }
            }
            break;
        case kMessageClearOverlay: {
            for (int i=0; i<kMaxWaveformChannels; ++i) {
                if (mChanMask & (1 << i)) {
                    mHist[i]->ClearOverlays();
                    mHist[i]->SetDirty();
                }
            }
            SetDirty(kDirtyEvent);
            break; }
        case kMessageHistScalesChanged: {
            // update current Y scale settings
            for (int i=0; i<kMaxWaveformChannels; ++i) {
                if (mHist[i] == (PHistImage *)message_data) {
                    mData->bv_wave_min[i] = mHist[i]->GetYMin();
                    mData->bv_wave_max[i] = mHist[i]->GetYMax();
                    break;
                }
            }
        }   break;
    }
}

void PBVWaveformWindow::DoMenuCommand(int anID)
{
    switch (anID) {

        case IDM_WAVE_ADD_OVERLAY:
            for (int i=0; i<kMaxWaveformChannels; ++i) {
                if (mChanMask & (1 << i)) {
                    mHist[i]->AddOverlay();
                }
            }
            SetDirty(kDirtyEvent);
            break;

        case IDM_WAVE_CLEAR_OVERLAY:
            for (int i=0; i<kMaxWaveformChannels; ++i) {
                if (mChanMask & (1 << i)) {
                    mHist[i]->ClearOverlays();
                    mHist[i]->SetDirty();
                }
            }
            SetDirty(kDirtyEvent);
            break;
    }
}

void PBVWaveformWindow::ScrollValueChanged(EScrollBar bar, int value)
{
    switch (bar) {
        case kScrollRight: {
            float f = value / (float)kScrollMax;
            for (int i=0; i<kMaxWaveformChannels; ++i) {
                if (!(mChanMask & (1 << i))) continue;
                mHist[i]->SetOverlaySep(f);
                if (mHist[i]->GetNumOverlays()) {
                    mHist[i]->SetDirty();
                    SetDirty(kDirtyEvent);
                }
            }
        }   break;
        default:
            break;
    }
}

// set our displayed channels
void PBVWaveformWindow::SetChannels(int chan_mask)
{
    int       n, i;
    Arg       wargs[20];
//    ImageData *data = GetData();

    if (chan_mask != mChanMask) {
        // count the number of channels displayed
        int chan_count = 0;
        for (i=0; i<kMaxWaveformChannels; ++i) {
            if (chan_mask & (1 << i)) {
                ++chan_count;
            }
        }
        // make necessary attachments and remap displayed channels
        float height = 100.0 / chan_count;
        int count = 0;
//        int num = chan_mask - 1;
        for (i=0; i<kMaxWaveformChannels; ++i) {
            // toggle on/off our channel displays
//            GetMenu()->SetToggle(IDM_WAVE_0 + i, (i == num ? 1 : 0));
            if (chan_mask & (1 << i)) {
                n = 0;
                if (count) {
                    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_POSITION);      ++n;
                    XtSetArg(wargs[n], XmNtopPosition,      int(count*height+0.5));  ++n;
                } else {
                    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_WIDGET);        ++n;
                    XtSetArg(wargs[n], XmNtopWidget,        GetMenu()->GetWidget()); ++n;
                }
                if (count < chan_count-1) {
                    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_POSITION);      ++n;
                    XtSetArg(wargs[n], XmNbottomPosition,   int((count+1)*height+0.5)); ++n;
                } else {
                    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM);          ++n;
                }
                XtSetValues(mChannel[i], wargs, n);
                ++count;
            }
        }
//        GetMenu()->SetToggle(IDM_WAVE_2, chan_mask == 0x03 ? 1 : 0);
        // map necessary channels
        for (i=0; i<kMaxWaveformChannels; ++i) {
            // only map/unmap channels that have changed
/*            if ((chan_mask ^ data->wave_mask) & (1 << i)) {
                // must be careful not to map a widget before it is realized
                if (XtIsRealized(mChannel[i])) {
                    if (chan_mask & (1 << i)) {
                        // show this channel
                        XtSetMappedWhenManaged(mChannel[i], True);
                    } else {
                        // hide this channel
                        XtSetMappedWhenManaged(mChannel[i], False);
                    }
                } else {
                    n = 0;
                    if (chan_mask & (1 << i)) {
                        XtSetArg(wargs[n], XmNmappedWhenManaged, TRUE); ++n;
                    } else {
                        XtSetArg(wargs[n], XmNmappedWhenManaged, FALSE); ++n;
                    }
                    XtSetValues(mChannel[i], wargs, n);
                }
            }*/
        }
        // set our canvas to the first displayed channel (for Print Image...)
        for (i=0; i<kMaxWaveformChannels; ++i) {
            if (chan_mask & (1 << i)) {
                SetImage(mHist[i]);
                break;
            }
        }
        // allow labels only on the last displayed channel
        int allow = TRUE;
        for (i=kMaxWaveformChannels-1; i>=0; --i) {
            if (chan_mask & (1 << i)) {
                mHist[i]->AllowLabel(allow);
                allow = FALSE;
            }
        }
        mChanMask = chan_mask;
    }
}

// UpdateSelf
void PBVWaveformWindow::UpdateSelf()
{
    int         i;
    ImageData * data = GetData();
    int         hit_num = data->cursor_hit; // current hit number near cursor
    char        buff[128];

#ifdef PRINT_DRAWS
    printf("-updateWaveformWindow\n");
#endif
    mLastNum = hit_num;

    if (IsDirty() & (kDirtyEvent | kDirtyAll)) {

        BarInfo *bi = NULL;
        void * wave[kMaxWaveformChannels] = { 0 };

        if (hit_num >= 0 and data->cursor_hit_type == 2) {
            // get waveforms for the space point at the cursor
            bi = data->barpoints.bar_info + hit_num;
            /*const Alpha16Event* aw = data->age->a16;
            const std::vector<Alpha16Channel*>* channels=&aw->hits;
            //for (auto it=channels->begin(); it!=channels->end(); ++it) {
            for (size_t i=0; i<channels->size(); i++) {
                Alpha16Channel* it=channels->at(i);
                if (it->tpc_wire -256 == hi->wire){
                    wave[kWireHist] = (void* )&(it->adc_samples);
                    printf("WaveFound on wire %d\n",it->tpc_wire);
                    //std::cout<<it->adc_samples<<std::endl;
                    break;
                }
            }*/
           
            // AgSignalsFlow *sigFlow = data->sigFlow;
            if (data->BVwf->size()==0)
               printf("No BVwf in flow\n");
            else
               for (int ii=0; ii<128; ii++) {
                    if (data->BVwf->count(ii)==0) continue;
                    std::vector<double>* wfi = data->BVwf->at(ii);
                    if (ii == bi->barID) {
                       wave[kBottomBVHist] = (void *)wfi;
                   }
                    if (ii == bi->barID+64) {
                       wave[kTopBVHist] = (void *)wfi;
                   }
               }
        }
        // update data for displayed histograms
        for (i=0; i<kMaxWaveformChannels; ++i) {

            if (!(mChanMask & (1 << i))) continue;
            if (!wave[i]) {
                if (mHist[i]->GetDataPt() || mHist[i]->GetOverlayPt()) {
                    mHist[i]->CreateData(0);
                    mHist[i]->SetDirty();
                }
            } else if (IsDirty() & kDirtyEvent) {
                mHist[i]->SetDirty();
                if (i == kBottomBVHist) {
                    const std::vector<double> *wf = (std::vector<double> *)wave[kBottomBVHist];
                    if (!wf) continue;
                    bool fit = (bi->ADCbot_fit_params[0] != -1);
                    mHist[i]->ClearFit();
                    mHist[i]->CreateData(wf->size(),0,fit);
                    long *pt = mHist[i]->GetDataPt();
                    if (pt) {
                        mHist[i]->SetScaleLimits(0, wf->size(), 10);
                        for (unsigned ibin=0; ibin<wf->size(); ++ibin) {
                            *pt++ = wf->at(ibin);
                        }
                        // Draw fit 
                        if (fit) {
                            long *fpt = mHist[i]->GetFitDataPt();
                            if (fpt) {
                                int nbins = mHist[i]->GetNumBins();
                                for (unsigned ibin=0; ibin<wf->size();ibin++) {
                                    double xval = ibin*700./nbins;
                                    double yval = bi->ADCbot_fit_params[0]*exp(-0.5*pow((xval-bi->ADCbot_fit_params[1])/(bi->ADCbot_fit_params[2]+(xval<bi->ADCbot_fit_params[1])*bi->ADCbot_fit_params[3]*(xval-bi->ADCbot_fit_params[1])),2))+bi->ADCbot_fit_params[4];
                                    *fpt++ = yval;
                                } 
                            }
                        }
                    }
                } else if (i == kTopBVHist) {
                    const std::vector<double> *wf = (std::vector<double> *)wave[kTopBVHist];
                    if (!wf) continue;
                    bool fit = (bi->ADCtop_fit_params[0] != -1);
                    mHist[i]->ClearFit();
                    mHist[i]->CreateData(wf->size(),0,fit);
                    long *pt = mHist[i]->GetDataPt();
                    if (pt) {
                        mHist[i]->SetScaleLimits(0, wf->size(), 10);
                        for (unsigned ibin=0; ibin<wf->size(); ++ibin) {
                            *pt++ = wf->at(ibin);
                        }
                        // Draw fit 
                        if (fit) {
                            long *fpt = mHist[i]->GetFitDataPt();
                            if (fpt) {
                                int nbins = mHist[i]->GetNumBins();
                                for (unsigned ibin=0; ibin<wf->size();ibin++) {
                                    double xval = ibin*700./nbins;
                                    double yval = bi->ADCtop_fit_params[0]*exp(-0.5*pow((xval-bi->ADCtop_fit_params[1])/(bi->ADCtop_fit_params[2]+(xval<bi->ADCtop_fit_params[1])*bi->ADCtop_fit_params[3]*(xval-bi->ADCtop_fit_params[1])),2))+bi->ADCtop_fit_params[4];
                                    *fpt++ = yval;
                                } 
                            }
                        }

                    }
                }
            }
            // Associated boxes
            mHist[i]->ClearBoxes();
            if (hit_num >= 0 and data->cursor_hit_type == 2) {
                if (i==kTopBVHist) mHist[i]->AddBox(bi->ADCtop_start/10.,bi->ADCtop_stop/10.,VDARK_COL);
                if (i==kBottomBVHist) mHist[i]->AddBox(bi->ADCbot_start/10.,bi->ADCbot_stop/10.,VDARK_COL);
            }

            // add bar number to histogram label
            buff[0] = '\0';
            if (hit_num >= 0 and data->cursor_hit_type == 2) {
                switch (i) {
                    case kBottomBVHist:
                        sprintf(buff, "%s %d", hist_label[i], bi->barID);
                        break;
                    case kTopBVHist:
                        sprintf(buff, "%s %d", hist_label[i], bi->barID+64);
                        break;
                }
            }
            if (buff[0]) {
                if (!mHist[i]->GetLabel() || strcmp(buff, mHist[i]->GetLabel())) {
                    mHist[i]->SetLabel(buff);
                    mHist[i]->SetDirty();
                }
            } else if (mHist[i]->GetLabel()) {
                mHist[i]->SetLabel(NULL);
                mHist[i]->SetDirty();
            } 
            // Add vertical lines for TDC times
            mHist[i]->ClearVerticals();
            if (hit_num >= 0 and data->cursor_hit_type == 2) {
                // Associated TDC time
                double xTDC = 0;
                if (i == kBottomBVHist) {
                    xTDC = bi->TDCbot/10.;
                }
                if (i == kTopBVHist) {
                    xTDC = (bi->TDCtop)/10.;
                }
                if (xTDC!=-0.1) mHist[i]->AddVertical(xTDC,FIT_SEED_COL,kLineTypeDot);
                // Other TDC times
                if (i==kBottomBVHist) {
                    for (double bot2: bi->TDCbot_all) {
                        double xTDC2 = (bot2)/10.;
                        if (xTDC==xTDC2) continue;
                        mHist[i]->AddVertical(xTDC2,FIT_SECOND_COL,kLineTypeOnOffDash);
                    }
                    for (double bot2: bi->TDCbot_all_reflection) {
                        double xTDC2 = (bot2)/10.;
                        if (xTDC==xTDC2) continue;
                        mHist[i]->AddVertical(xTDC2,FIT_PHOTON_COL,kLineTypeOnOffDash);
                    }
                }
                if (i==kTopBVHist) {
                    for (double top2: bi->TDCtop_all) {
                        double xTDC2 = (top2)/10.;
                        if (xTDC==xTDC2) continue;
                        mHist[i]->AddVertical(xTDC2,FIT_SECOND_COL,kLineTypeOnOffDash);
                    }
                    for (double top2: bi->TDCtop_all_reflection) {
                        double xTDC2 = (top2)/10.;
                        if (xTDC==xTDC2) continue;
                        mHist[i]->AddVertical(xTDC2,FIT_PHOTON_COL,kLineTypeOnOffDash);

                    }
                }
                mHist[i]->SetDirty();
            }
        }
    }
    // Update necessary histograms
    for (i=0; i<kMaxWaveformChannels; ++i) {
        // update this channel if required and visible
        if (mHist[i]->IsDirty()){ // && (data->wave_mask & (1<<i))) {
            mHist[i]->Draw();
        }
    }
}

