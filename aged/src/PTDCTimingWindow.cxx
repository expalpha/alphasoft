//==============================================================================
// File:        PTDCTimingWindow.cxx
//
// Description: Window to display TDC timing
//
// Gareth Smith, adapted from
// Copyright (c) 2017, Phil Harvey, Queen's University
//==============================================================================

#include <Xm/RowColumn.h>
#include <Xm/Form.h>
#include <Xm/DrawingA.h>
#include <math.h>
#include "PTDCTimingWindow.h"
#include "PHistImage.h"
#include "ImageData.h"
#include "PSpeaker.h"
#include "CUtils.h"
#include "AgFlow.h"
#include "RecoFlow.h"

const int   kDirtyEvent     = 5;
const int   kDirtyAll       = 6;

enum {
    kRaw,                   // index for raw tdc time window
    kChannelCorrected,      // index for channel corrected tdc time window
    kTWCorrected,           // index for channel + TW corrected tdc time window
    kAvg,                   // index for top+bot/2 tdc time window
    kDiff,                  // index for top-bot tdc time window
    kNumWindows
};

static MenuStruct channels_menu[] = {
//      { "Add Overlay",      0, XK_A, IDM_WAVE_ADD_OVERLAY,   NULL, 0, 0 },
      { "Clear Overlays",   0, XK_C, IDM_WAVE_CLEAR_OVERLAY, NULL, 0, 0 },
    //{ NULL,               0, 0,       0,                  NULL, 0, 0},
};

static MenuStruct wave_main_menu[] = {
    { "Display",    0, 0,   0, channels_menu, XtNumber(channels_menu), 0 },
};

static const char * hist_label[kNumWindows] = { "Raw TDC Time", "After Channel Correction", "After Time Walk Correction","Average TDC Time","TDC Time Difference" };

//---------------------------------------------------------------------------
// PTDCTimingWindow constructor
//
PTDCTimingWindow::PTDCTimingWindow(ImageData *data)
           : PImageWindow(data), mLastNum(-1), mChanMask(0)
{
    int     n;
    Arg     wargs[20];

    data->mSpeaker->AddListener(this);
    
    n = 0;
    XtSetArg(wargs[n], XmNtitle, "TDC Timing"); ++n;
    XtSetArg(wargs[n], XmNx, 600);             ++n;
    XtSetArg(wargs[n], XmNy, 0);             ++n;
    XtSetArg(wargs[n], XmNminWidth, 200);      ++n;
    XtSetArg(wargs[n], XmNminHeight, 200);     ++n;
    SetShell(CreateShell("wavePop",data->toplevel,wargs,n));
    
    n = 0;
    XtSetArg(wargs[n], XmNwidth, 600);         ++n;
    XtSetArg(wargs[n], XmNheight, 600);        ++n;
    XtSetArg(wargs[n], XmNbackground, data->colour[BKG_COL]); ++n;
    SetMainPane(XtCreateManagedWidget("waveMain", xmFormWidgetClass,GetShell(),wargs,n));

    n = 0;
    XtSetArg(wargs[n],XmNmarginHeight,      1); ++n;
    XtSetArg(wargs[n],XmNleftAttachment,    XmATTACH_FORM); ++n;
    XtSetArg(wargs[n],XmNtopAttachment,     XmATTACH_FORM); ++n;
    XtSetArg(wargs[n],XmNrightAttachment,   XmATTACH_FORM); ++n;
    Widget menu = XmCreateMenuBar(GetMainPane(), (char*)"agedMenu" , wargs, n);
    XtManageChild(menu);
    CreateMenu(menu, wave_main_menu, XtNumber(wave_main_menu), this);

//    GetMenu()->SetToggle(IDM_WAVE_SUBT,      data->wave_subt);
//    GetMenu()->SetToggle(IDM_WAVE_SUM,       data->wave_sum);
//    GetMenu()->SetToggle(IDM_WAVE_PULSE,     data->wave_pulse);
//    GetMenu()->SetToggle(IDM_QT_PULSE,       data->qt_pulse);
//    GetMenu()->SetToggle(IDM_WAVE_CURSOR,    data->wave_cursor);

    // add resource labels to channels in menu
/*    for (int i=0; i<kWaveformRsrcNum; ++i) {
        if (!strncmp(data->wave_lbl[i], "Channel ", 8)) continue;
        char buff[256];
        snprintf(buff, 256,"Channel %d - %s", i, data->wave_lbl[i]);
        GetMenu()->SetLabel(IDM_WAVE_0+i, buff);
    }*/

    n = 0;
    XtSetArg(wargs[n], XmNtopAttachment, XmATTACH_WIDGET);  ++n;
    XtSetArg(wargs[n], XmNtopWidget, menu); ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM);  ++n;
    XtSetArg(wargs[n], XmNrightAttachment, XmATTACH_FORM);  ++n;
    XtSetArg(wargs[n], XmNwidth, 15);  ++n;
    XtSetArg(wargs[n], XmNorientation, XmVERTICAL);  ++n;
    NewScrollBar(kScrollRight,(char*)"waveScroll",wargs,n);
    SetScrollValue(kScrollRight, kScrollMax/2, 0);

    n = 0;
    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_WIDGET);    ++n;
    XtSetArg(wargs[n], XmNtopWidget,        menu);               ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM);      ++n;
    XtSetArg(wargs[n], XmNleftAttachment,   XmATTACH_FORM);      ++n;
    XtSetArg(wargs[n], XmNrightAttachment,  XmATTACH_WIDGET);    ++n;
    XtSetArg(wargs[n], XmNrightWidget,      GetScroll(kScrollRight)); ++n;
    XtSetArg(wargs[n], XmNbackground,       data->colour[BKG_COL]); ++n;
    Widget form = XtCreateManagedWidget("waveForm", xmFormWidgetClass,GetMainPane(),wargs,n);
    
    std::vector<int> lab_col = {FIT_PHOTON_COL,FIT_BAD_COL,FIT_GOOD_COL,TEXT_COL,TEXT_COL};
    for (int i=0; i<kNumWindows; ++i) {
        n = 0;
        XtSetArg(wargs[n], XmNbackground,           data->colour[BKG_COL]); ++n;
        XtSetArg(wargs[n], XmNleftAttachment,       XmATTACH_FORM);      ++n;
        XtSetArg(wargs[n], XmNrightAttachment,      XmATTACH_FORM);      ++n;
        mChannel[i] = XtCreateManagedWidget(hist_label[i], xmDrawingAreaWidgetClass, form, wargs, n);

        // create histogram
        mHist[i] = new PHistImage(this, mChannel[i], 0);
        mHist[i]->AllowLabel(FALSE);
        mHist[i]->SetScaleLimits(0,15000,10);
        mHist[i]->SetScaleMin(0);
        mHist[i]->SetScaleMax(4096);
        mHist[i]->SetYMin(data->tdc_wave_min[i]);
        mHist[i]->SetYMax(data->tdc_wave_min[i]);
        mHist[i]->SetStyle(kHistStyleSteps);
        mHist[i]->SetFixedBins();
        mHist[i]->SetPlotCol(BKG_COL);
        mHist[i]->SetLabelCol(lab_col[i]);
        mHist[i]->SetLabel(hist_label[i]);
//        mHist[i]->SetOverlayCol(SCOPE0_COL);
//        mHist[i]->SetAutoScale(1);
        mHist[i]->SetCursorTracking(1);
    }
    // arrange our channel histogram widgets in the window
    SetChannels(kNumWindows);
        
    SetDirty(kDirtyEvent);
}

PTDCTimingWindow::~PTDCTimingWindow()
{
    ImageData *data = GetData();
    for (int i=0; i<kNumWindows; ++i) {
        // update current Y scale settings
        data->tdc_wave_min[i] = mHist[i]->GetYMin();
        data->tdc_wave_min[i] = mHist[i]->GetYMax();
        delete mHist[i];
        mHist[i] = NULL;
    }
    SetImage(NULL);     // make sure base class doesn't re-delete our image
}

void PTDCTimingWindow::Listen(int message, void *message_data)
{
    switch (message) {
        case kMessageNewEvent:
        case kMessageEventCleared:
            for (int i=0; i<kNumWindows; ++i) {
                mHist[i]->ClearOverlays();
            }
            SetDirty(kDirtyEvent);
            break;
        case kMessageColoursChanged:
        case kMessageSmoothTextChanged:
        case kMessageSmoothLinesChanged:
            SetDirty(kDirtyAll);
            for (int i=0; i<kNumWindows; ++i) {
                mHist[i]->SetDirty();
            }
            break;
        case kMessageCursorHit:
            if (mLastNum != mData->cursor_hit) {
                // only dirty if this is a different hit
                SetDirty(kDirtyEvent);
            }
            break;
        case kMessageAddOverlay:
            if (mData->cursor_hit >= 0) {
                // add to overlays if not done already
                for (int i=0; i<kNumWindows; ++i) {
                    mHist[i]->AddOverlayVerticals();
                    SetDirty(kDirtyEvent);
                }
            }
            break;
        case kMessageClearOverlay: {
            for (int i=0; i<kNumWindows; ++i) {
                mHist[i]->ClearOverlayVerticals();
                mHist[i]->SetDirty();
            }
            SetDirty(kDirtyEvent);
            break; }
        case kMessageHistScalesChanged: {
            // update current Y scale settings
            for (int i=0; i<kNumWindows; ++i) {
                if (mHist[i] == (PHistImage *)message_data) {
                    mData->tdc_wave_min[i] = mHist[i]->GetYMin();
                    mData->tdc_wave_min[i] = mHist[i]->GetYMax();
                    break;
                }
            }
        }   break;
    }
}

void PTDCTimingWindow::DoMenuCommand(int anID)
{
    switch (anID) {

        case IDM_WAVE_ADD_OVERLAY:
            for (int i=0; i<kNumWindows; ++i) {
                mHist[i]->AddOverlay();
            }
            SetDirty(kDirtyEvent);
            break;

        case IDM_WAVE_CLEAR_OVERLAY:
            for (int i=0; i<kNumWindows; ++i) {
                mHist[i]->ClearOverlayVerticals();
                mHist[i]->SetDirty();
            }
            SetDirty(kDirtyEvent);
            break;
    }
}

void PTDCTimingWindow::ScrollValueChanged(EScrollBar bar, int value)
{
    switch (bar) {
        case kScrollRight: {
            float f = value / (float)kScrollMax;
            for (int i=0; i<kNumWindows; ++i) {
                mHist[i]->SetOverlaySep(f);
                if (mHist[i]->GetNumOverlays()) {
                    mHist[i]->SetDirty();
                    SetDirty(kDirtyEvent);
                }
            }
        }   break;
        default:
            break;
    }
}

// set our displayed channels
void PTDCTimingWindow::SetChannels(int chan_mask)
{
    int       n, i;
    Arg       wargs[20];
//    ImageData *data = GetData();

    if (chan_mask != mChanMask) {
        // count the number of channels displayed
        int chan_count = chan_mask;
        // make necessary attachments and remap displayed channels
        float height = 100.0 / chan_count;
        int count = 0;
//        int num = chan_mask - 1;
        for (i=0; i<kNumWindows; ++i) {
        // toggle on/off our channel displays
//            GetMenu()->SetToggle(IDM_WAVE_0 + i, (i == num ? 1 : 0));
            n = 0;
            if (count) {
                XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_POSITION);      ++n;
                XtSetArg(wargs[n], XmNtopPosition,      int(count*height+0.5));  ++n;
            } else {
                XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_WIDGET);        ++n;
                XtSetArg(wargs[n], XmNtopWidget,        GetMenu()->GetWidget()); ++n;
            }
            if (count < chan_count-1) {
                XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_POSITION);      ++n;
                XtSetArg(wargs[n], XmNbottomPosition,   int((count+1)*height+0.5)); ++n;
            } else {
                XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM);          ++n;
            }
            XtSetValues(mChannel[i], wargs, n);
            ++count;
        }
        // set our canvas to the first displayed channel (for Print Image...)
        for (i=0; i<kNumWindows; ++i) {
            SetImage(mHist[i]);
            break;
        }
        // allow labels only on the last displayed channel
        int allow = TRUE;
        for (i=kNumWindows-1; i>=0; --i) {
            mHist[i]->AllowLabel(allow);
            allow = FALSE;
        }
        mChanMask = chan_mask;
    }
}

// UpdateSelf
void PTDCTimingWindow::UpdateSelf()
{
    int         i;
    ImageData * data = GetData();
    int         hit_num = data->cursor_hit; // current hit number near cursor
    char        buff[128];

#ifdef PRINT_DRAWS
    printf("-updateWaveformWindow\n");
#endif
    mLastNum = hit_num;

    if (IsDirty() & (kDirtyEvent | kDirtyAll)) {

        BarInfo *bi = NULL;

        if (hit_num >= 0 and data->cursor_hit_type == 2) {
            // get waveforms for the space point at the cursor
            bi = data->barpoints.bar_info + hit_num;
        }

        // write a flat waveform so the display works
        for (i=0; i<kNumWindows; ++i) {
            mHist[i]->CreateData(7000);
            mHist[i]->SetScaleLimits(-7000, 7000, 10);
            if (i==kRaw or i==kChannelCorrected or i==kTWCorrected or i==kAvg) {
                mHist[i]->SetScaleMin(data->lower_limit_tdc_time-2);
                mHist[i]->SetScaleMax(data->upper_limit_tdc_time+2);
            }
            else if (i==kDiff) {
                mHist[i]->SetScaleMin(-1*(data->upper_limit_tdc_time - data->lower_limit_tdc_time)-2);
                mHist[i]->SetScaleMax((data->upper_limit_tdc_time - data->lower_limit_tdc_time)+2);       
            }
        }

        // update data for displayed histograms
        for (i=0; i<kNumWindows; ++i) {

            // add bar number to histogram label
            buff[0] = '\0';
            if (hit_num >= 0 and data->cursor_hit_type == 2) {
                snprintf(buff,128, "%s; Bar %d", hist_label[i], bi->barID);            }
            if (buff[0]) {
                if (!mHist[i]->GetLabel() || strcmp(buff, mHist[i]->GetLabel())) {
                    mHist[i]->SetLabel(buff);
                    mHist[i]->SetDirty();
                }
            } else if (mHist[i]->GetLabel()) {
                mHist[i]->SetLabel(hist_label[i]);
                mHist[i]->SetDirty();
            } 
            // Add vertical lines for TDC times
            mHist[i]->ClearVerticals();
            if (hit_num >= 0 and data->cursor_hit_type == 2) {
                // Associated TDC time
                if (i == kRaw) {
                    mHist[i]->AddVertical(bi->TDCtop_raw,FIT_SEED_COL,kLineTypeDot);
                    mHist[i]->AddVertical(bi->TDCbot_raw,FIT_ADDED_COL,kLineTypeDot);
                }
                if (i == kChannelCorrected) {
                    mHist[i]->AddVertical(bi->TDCtop_cc,FIT_SEED_COL,kLineTypeDot);
                    mHist[i]->AddVertical(bi->TDCbot_cc,FIT_ADDED_COL,kLineTypeDot);
                }
                if (i == kTWCorrected) {
                    mHist[i]->AddVertical(bi->TDCtop,FIT_SEED_COL,kLineTypeDot);
                    mHist[i]->AddVertical(bi->TDCbot,FIT_ADDED_COL,kLineTypeDot);
                }
                if (i == kAvg) {
                    mHist[i]->AddVertical((bi->TDCtop_raw+bi->TDCbot_raw)/2.,FIT_PHOTON_COL,kLineTypeDot);
                    mHist[i]->AddVertical((bi->TDCtop_cc+bi->TDCbot_cc)/2.,FIT_BAD_COL,kLineTypeDot);
                    mHist[i]->AddVertical((bi->TDCtop+bi->TDCbot)/2.,FIT_GOOD_COL,kLineTypeDot);
                }
                if (i == kDiff) {
                    mHist[i]->AddVertical((bi->TDCtop_raw-bi->TDCbot_raw),FIT_PHOTON_COL,kLineTypeDot);
                    mHist[i]->AddVertical((bi->TDCtop_cc-bi->TDCbot_cc),FIT_BAD_COL,kLineTypeDot);
                    mHist[i]->AddVertical((bi->TDCtop-bi->TDCbot),FIT_GOOD_COL,kLineTypeDot);
                }
                // Other TDC times
                if (i == kRaw) {
                    for (double bot2: bi->TDCbot_all) {
                        if (bi->TDCbot_raw==bot2) continue;
                        mHist[i]->AddVertical(bot2,FIT_SECOND_COL,kLineTypeDot);
                    }
                    for (double top2: bi->TDCtop_all) {
                        if (bi->TDCtop_raw==top2) continue;
                        mHist[i]->AddVertical(top2,FIT_SECOND_COL,kLineTypeDot);
                    }
                }
                mHist[i]->SetDirty();
            }
        }
    }
    // Update necessary histograms
    for (i=0; i<kNumWindows; ++i) {
        // update this channel if required and visible
        if (mHist[i]->IsDirty()){ // && (data->wave_mask & (1<<i))) {
            mHist[i]->Draw();
        }
    }
}

