//==============================================================================
// File:        Aged.cxx
//
// Description: ALPHA-g Event Display main class
//
// Created:     2017-08-01 - Phil Harvey
//
// Copyright (c) 2017, Phil Harvey, Queen's University
//==============================================================================

#include "Aged.h"
#include "aged_version.h"

#include "CUtils.h"
#include "ImageData.h"
#include "AgedWindow.h"
#include "PEventControlWindow.h"

#include "TSpacePoint.hh"
#include "TStoreEvent.hh"
#include "TBarEvent.hh"
#include "TBarHit.h"
#include "TBarEndHit.h"
#include "TBarADCHit.h"
#include "TBarTDCHit.h"
#include "TStoreHelix.hh" // TEMPORARY

#define AnyModMask (Mod1Mask | Mod2Mask | Mod3Mask | Mod4Mask | Mod5Mask)

Aged::Aged()
{
   Printf((char *)"Version " AGED_VERSION "");
   /*
   ** Create main window and menus
   */
   fWindow = new AgedWindow(1);
   fData   = fWindow->GetData();
   fShowBarTypes = 1;
}

Aged::~Aged()
{
   delete fWindow;
   fWindow = NULL;
}

// dispatchEvent - dispatch the X event (PH 03/25/00)
// - handles menu accelerator key events internally
static void dispatchEvent(XEvent *event)
{
   const int      kBuffSize = 10;
   char           buff[kBuffSize];
   KeySym         ks;
   XComposeStatus cs;
   static int     altPressed = 0;

   switch (event->type) {
   case KeyPress:
      XLookupString(&event->xkey, buff, kBuffSize, &ks, &cs);
      switch (ks) {
      case XK_Alt_L: altPressed |= 0x01; break;
      case XK_Alt_R: altPressed |= 0x02; break;
      default:
         // handle as an accelerator if alt is pressed
         if (event->xkey.state & AnyModMask &&  // any modifier pressed?
             altPressed &&                      // was it Alt?
             PWindow::sMainWindow &&            // main window initialized?
             PWindow::sMainWindow->GetMenu() && // main window menu initialized?
             // do the accelerator
             PWindow::sMainWindow->GetMenu()->DoAccelerator(ks)) {
            // return now since the event was an accelerator
            // and we handled it internally
            return;
         }
         break;
      }
      break;
   case KeyRelease:
      XLookupString(&event->xkey, buff, kBuffSize, &ks, &cs);
      switch (ks) {
      case XK_Alt_L: altPressed &= ~0x01; break;
      case XK_Alt_R: altPressed &= ~0x02; break;
      }
      break;
   case ConfigureNotify:
      // check window offset at every configure event
      // - This is odd I know, but it seems to work with different X clients
      // - The problem was that some X clients will move the window after it
      //   is created.  So we monitor the configureNotify messages and capture
      //   the first one to figure out how the client places the windows.
      if (PWindow::sMainWindow && event->xconfigure.window == XtWindow(PWindow::sMainWindow->GetShell())) {
         PWindow::sMainWindow->CheckWindowOffset(event->xconfigure.border_width);
      }
      break;
   }
   // give the event to X
   XtDispatchEvent(event);
}

// callback from timer to show the next event
void do_next(XtPointer *data_in, XtIntervalId* /*ID*/)
{
   ImageData* data = (ImageData*)data_in;
   if (data->trigger_flag == TRIGGER_CONTINUOUS) {
      data->mNext = 1;
      // generate a ClientMessage event to break us out of waiting in XtNextEvent()
      if (data->mMainWindow && !XPending(data->display)) {
         XClientMessageEvent xev;
         memset(&xev, 0, sizeof(xev));
         xev.type   = ClientMessage;
         xev.format = 8;
         XSendEvent(data->display, XtWindow(data->mMainWindow->GetShell()), True, 0, (XEvent *)&xev);
      }
   }
}

void findWaveforms(TStoreEvent *anEvent, AgSignalsFlow *sigFlow)
{
   const TObjArray *points = anEvent->GetSpacePoints();
   int              num    = points->GetEntries();
   for (int i = 0; i < num; ++i) {
      int          found = 0;
      TSpacePoint *spi   = (TSpacePoint *)points->At(i);
      if (sigFlow->AWwf.empty())
         printf("Aged::No AWwf in flow\n");
      else
         for (auto it = sigFlow->AWwf.begin(); it != sigFlow->AWwf.end(); ++it) {
            // if (i==0) printf("Wire i=%d s=%d\n",it->i,it->sec);
            if (it->i == spi->GetWire()) {
               found |= 0x01;
               break;
            }
         }
      if (sigFlow->PADwf.empty())
         printf("Aged::No PADwf in flow\n");
      else
         for (auto it = sigFlow->PADwf.begin(); it != sigFlow->PADwf.end(); ++it) {
            int pad = it->sec + it->i * 32;
            if (pad == spi->GetPad()) {
               found |= 0x02;
               break;
            }
         }
      // printf("Spacepoint %d wire=%d pad=%d\n", i,spi->GetWire(),spi->GetPad());
      if (!(found & 0x01)) printf(" - no wire\n");
      if (!(found & 0x02)) printf(" - no pad\n");
   }
}

// Show ALPHA-g event in the displayTStore
TAFlags *Aged::ShowEvent(TStoreEvent &evt, TBarEvent& bars, const std::vector<ALPHAg::wf_ref> &AWwf,
                      const std::vector<ALPHAg::wf_ref> &PADwf, std::map<int,std::vector<double>*> &BVwf, long runNo, TAFlags *flags)
{
   ImageData *data = fData;

   if (data) clearEvent(data);

   if (data) {
      PEventControlWindow *pe_win = (PEventControlWindow *)data->mWindow[EVT_NUM_WINDOW];
      if (pe_win) {
         pe_win->Show();
      } else {
         data->mMainWindow->CreateWindow(EVT_NUM_WINDOW);
      }
      if (data->trigger_flag == TRIGGER_SINGLE) {
         setTriggerFlag(data, TRIGGER_OFF);
      }

      data->n_tracks = 0;
      data->n_clusters = 0;
      data->max_tdc_time = 0;
      data->min_tdc_time = 0;
      data->upper_limit_tdc_time = 0;
      data->lower_limit_tdc_time = 0;
      data->max_adc_amp = 0;
      data->min_adc_amp = 0;

      // copy the space point XYZ positions into our Node array
      const TObjArray *points = evt.GetSpacePoints();
      //std::cout << "points: " << points << std::endl;
      if (points) {
         int   num  = points->GetEntries();
       //std::cout << num << " points" << std::endl;
        Node *node = (Node *)XtMalloc(num * sizeof(Node));
         if (!node) {
            printf("Out of memory!\n");
            return flags;
         }
         data->hits.hit_info = (HitInfo *)XtMalloc(num * sizeof(HitInfo));
         if (!data->hits.hit_info) {
            printf("Out of memory!\n");
            free(node);
            return flags;
         }
         data->hits.nodes     = node;
         data->hits.num_nodes = num;
         memset(data->hits.hit_info, 0, num * sizeof(HitInfo));
         memset(node, 0, num * sizeof(Node));
         HitInfo *hi = data->hits.hit_info;
         const TObjArray* HelixArray = evt.GetHelixArray();
         for (int i = 0; i < num; ++i, ++node, ++hi) {
            TSpacePoint *spi = (TSpacePoint *)points->At(i);
            node->x3         = spi->GetX() / AG_SCALE;
            node->y3         = spi->GetY() / AG_SCALE;
            node->z3         = spi->GetZ() / AG_SCALE;
            hi->wire         = spi->GetWire();
            hi->pad          = spi->GetPad();
            hi->track        = spi->GetRecoTrackID();
            if (hi->track > data->n_tracks) data->n_tracks = hi->track;
            hi->finder_status = spi->GetTrackFinderStatus();
            hi->time         = spi->GetTime();
            hi->wireheight   = spi->GetWireHeight();
            hi->padheight    = spi->GetWireHeight();
            hi->error[0]     = spi->GetErrX();
            hi->error[1]     = spi->GetErrY();
            hi->error[2]     = spi->GetErrZ();
            hi->index        = i;
            if (std::isnan(hi->time)) hi->time = -1;
            if (std::isnan(hi->padheight)) hi->padheight = -1;
            if (std::isnan(hi->wireheight)) hi->wireheight = -1;

            // Residuals are only set on the TTrack/TFitHelix. Lets find the residual matching this spacepoint.
            std::pair<double,double> pair(spi->GetX(),spi->GetY());
            int num_helix = HelixArray->GetEntries();
            for (int ihe = 0; ihe < num_helix; ++ihe) {
               TStoreHelix *hel = (TStoreHelix *)HelixArray->At(ihe);
               TFitHelix h(*hel);
               h.CalculateResiduals();
               if (h.GetRResidualsXYMap().count(pair) == 0) {
                  continue;
               }
               hi->d2 = h.GetRResidualsXYMap().at(pair);
               hi->dz = h.GetZResidualsXYMap().at(pair);
            }

         }
      }

      if (bars.GetNumBars()>0) {
         int   num     = bars.GetNumBars();
         Node *barnode = (Node *)XtMalloc(num * sizeof(Node));
         if (!barnode) {
            printf("Out of memory!\n");
            return flags;
         }
         data->barpoints.bar_info = (BarInfo *)XtMalloc(num * sizeof(BarInfo));
         if (!data->barpoints.bar_info) {
            printf("Out of memory!\n");
            free(barnode);
            return flags;
         }
         data->barpoints.nodes     = barnode;
         data->barpoints.num_nodes = num;
         
         memset(barnode, 0, num * sizeof(Node));

         memset(data->barpoints.bar_info, 0, num * sizeof(BarInfo)); //This line causes warnings (see point below), need to maybe create a copy constructor for the struct. Attempts to fix and lose the warning are below.
         BarInfo *bi = data->barpoints.bar_info;
         //Shouldn't use memset/memcpy for object of non-trivially copyable type: https://stackoverflow.com/questions/26030873/using-stdmemcpy-to-object-of-non-trivially-copyable-type (TODO LMG)
         //BarInfo obj = {};
         //BarInfo* bi = &obj;
         //bi = data->barpoints.bar_info;

         const std::vector<TBarHit>& barhits = bars.GetBarHits();
         int n_good=0;
         for (int i = 0; i < num; ++i, ++barnode, ++bi) {
            const TBarHit& bar = barhits.at(i);
            if (bar.GetHitType()==8 and (!bar.IsInMainGroup() or n_good==0) ) { // If the bad TDC hit is first, this fails FIXME
               // Skip single TDC hits outside the main group
               data->barpoints.num_nodes--;
               --barnode;
               --bi;
               continue;
            }
            if (bar.GetHitType()>fShowBarTypes) {
               data->barpoints.num_nodes--;
               --barnode;
               --bi;
               continue;  
            }
            n_good++;
            double  x, y;
            bar.GetXY(x, y);
            barnode->x3 = x  /BV_SCALE;
            barnode->y3 = y  /BV_SCALE;
            if (bar.IsComplete()) {
               barnode->z3 = bar.GetZed()  /BV_SCALE;
            }
            else { // No calculable Z position, still show at end of bar
               if (bar.HasTopEndHit()) barnode->z3 = 1.5; 
               if (bar.HasBotEndHit()) barnode->z3 = -1.5;
               if (!bar.HasBotEndHit() and !bar.HasTopEndHit()) barnode->z3 = 0;
            }
            bi->barID   = bar.GetBarID();
            bi->clusterID = bar.GetClusterNum();
            if (bi->clusterID > data->n_clusters) data->n_clusters = bi->clusterID;
            bi->main_group = bar.IsInMainGroup();
            //bi->ADCtop  = bar.GetTopEndHit().GetAmpCalibrated();
            //bi->ADCbot  = bar.GetBotEndHit().GetAmpCalibrated();
            bi->ADCtop  = bar.GetTopEndHit().GetAmpFit();
            bi->ADCbot  = bar.GetBotEndHit().GetAmpFit();
            bi->time    = bar.GetTime()*1e9 - bars.GetEventTDCTime()*1e9;
            bi->ADCtop_start = bar.GetTopADCHit().GetStartTime();
            bi->ADCbot_start = bar.GetBotADCHit().GetStartTime();
            bi->ADCtop_stop = bar.GetTopADCHit().GetStartTime() + bar.GetTopADCHit().GetTimeOverThreshold();
            bi->ADCbot_stop = bar.GetBotADCHit().GetStartTime() + bar.GetBotADCHit().GetTimeOverThreshold();
            if (bi->ADCtop!=-1 and bi->ADCbot!=-1) bi->combADC = TMath::Log(bi->ADCtop) + TMath::Log(bi->ADCbot);
            if (bi->ADCtop==-1 and bi->ADCbot!=-1) bi->combADC = -1;
            if (bi->ADCtop!=-1 and bi->ADCbot==-1) bi->combADC = -1;
            if (bi->ADCtop==-1 and bi->ADCbot==-1) bi->combADC = -1;
            if (bar.HasTopADCHit() and bar.HasBotADCHit()) {
               if (bi->combADC > data->max_adc_amp or data->max_adc_amp==0) data->max_adc_amp = bi->combADC;
               if (bi->combADC < data->min_adc_amp or data->min_adc_amp==0) data->min_adc_amp = bi->combADC;
            }
            if (bar.HasTopTDCHit()) {
               if (bar.HasTopEndHit()) bi->TDCtop  = bar.GetTopEndHit().GetTime()*1e9 - bars.GetEventTDCTime()*1e9;
               else bi->TDCtop  = bar.GetTopTDCHit().GetTime()*1e9 - bars.GetEventTDCTime()*1e9;
               bi->TDCtop_raw  = bar.GetTopTDCHit().GetTime()*1e9 - bars.GetEventTDCTime()*1e9;
               bi->TDCtop_cc  = bar.GetTopTDCHit().GetTimeCali()*1e9 - bars.GetEventTDCTime()*1e9;
               bi->TDCtop_TOT  = bar.GetTopTDCHit().GetTimeOverThr()*1e9;
               if (bi->TDCtop > data->max_tdc_time or data->max_tdc_time==0) data->max_tdc_time = bi->TDCtop;
               if (bi->TDCtop < data->min_tdc_time or data->min_tdc_time==0) data->min_tdc_time = bi->TDCtop;
               if (bi->TDCtop > data->upper_limit_tdc_time or data->upper_limit_tdc_time==0) data->upper_limit_tdc_time = bi->TDCtop;
               if (bi->TDCtop < data->lower_limit_tdc_time or data->lower_limit_tdc_time==0) data->lower_limit_tdc_time = bi->TDCtop;
               if (bi->TDCtop_raw > data->upper_limit_tdc_time or data->upper_limit_tdc_time==0) data->upper_limit_tdc_time = bi->TDCtop_raw;
               if (bi->TDCtop_raw < data->lower_limit_tdc_time or data->lower_limit_tdc_time==0) data->lower_limit_tdc_time = bi->TDCtop_raw;
               if (bi->TDCtop_cc > data->upper_limit_tdc_time or data->upper_limit_tdc_time==0) data->upper_limit_tdc_time = bi->TDCtop_cc;
               if (bi->TDCtop_cc < data->lower_limit_tdc_time or data->lower_limit_tdc_time==0) data->lower_limit_tdc_time = bi->TDCtop_cc;
            }
            else {
               bi->TDCtop = -1;
               bi->TDCtop_raw = -1;
               bi->TDCtop_cc = -1;
               bi->TDCtop_TOT = -1;
            }
            if (bar.HasBotTDCHit()) {
               if (bar.HasBotEndHit()) bi->TDCbot  = bar.GetBotEndHit().GetTime()*1e9 - bars.GetEventTDCTime()*1e9;
               else bi->TDCbot  = bar.GetBotTDCHit().GetTime()*1e9 - bars.GetEventTDCTime()*1e9;
               bi->TDCbot_raw  = bar.GetBotTDCHit().GetTime()*1e9 - bars.GetEventTDCTime()*1e9;
               bi->TDCbot_cc  = bar.GetBotTDCHit().GetTimeCali()*1e9 - bars.GetEventTDCTime()*1e9;
               bi->TDCbot_TOT  = bar.GetBotTDCHit().GetTimeOverThr()*1e9;
               if (bi->TDCbot > data->max_tdc_time or data->max_tdc_time==0) data->max_tdc_time = bi->TDCbot;
               if (bi->TDCbot < data->min_tdc_time or data->min_tdc_time==0) data->min_tdc_time = bi->TDCbot;
               if (bi->TDCbot > data->upper_limit_tdc_time or data->upper_limit_tdc_time==0) data->upper_limit_tdc_time = bi->TDCbot;
               if (bi->TDCbot < data->lower_limit_tdc_time or data->lower_limit_tdc_time==0) data->lower_limit_tdc_time = bi->TDCbot;
               if (bi->TDCbot_raw > data->upper_limit_tdc_time or data->upper_limit_tdc_time==0) data->upper_limit_tdc_time = bi->TDCbot_raw;
               if (bi->TDCbot_raw < data->lower_limit_tdc_time or data->lower_limit_tdc_time==0) data->lower_limit_tdc_time = bi->TDCbot_raw;
               if (bi->TDCbot_cc > data->upper_limit_tdc_time or data->upper_limit_tdc_time==0) data->upper_limit_tdc_time = bi->TDCbot_cc;
               if (bi->TDCbot_cc < data->lower_limit_tdc_time or data->lower_limit_tdc_time==0) data->lower_limit_tdc_time = bi->TDCbot_cc;
            }
            else {
               bi->TDCbot = -1;
               bi->TDCbot_raw = -1;
               bi->TDCbot_cc = -1;
               bi->TDCbot_TOT = -1;
            }
            bi->index   = bar.GetBarID();
            bi->matched = bar.HasTPCHit();
            bi->complete= bar.IsComplete();
            bi->hittype = bar.GetHitType();
            bi->track_num = bar.GetTPCTrackNum();
            for (TBarTDCHit &tdc: bars.GetTDCHits()) {
               if (tdc.Is165Reflection()) {
                  if (tdc.GetEndID()==bi->barID) bi->TDCbot_all_reflection.push_back(tdc.GetTime()*1e9 - bars.GetEventTDCTime()*1e9);
                  if (tdc.GetEndID()==bi->barID+64) bi->TDCtop_all_reflection.push_back(tdc.GetTime()*1e9 - bars.GetEventTDCTime()*1e9);
               }
               else {
                  if (tdc.GetEndID()==bi->barID) bi->TDCbot_all.push_back(tdc.GetTime()*1e9 - bars.GetEventTDCTime()*1e9);
                  if (tdc.GetEndID()==bi->barID+64) bi->TDCtop_all.push_back(tdc.GetTime()*1e9 - bars.GetEventTDCTime()*1e9);
               }
            }
            for (int fpi=0; fpi<5; fpi++) {
               if (bar.HasBotADCHit()) bi->ADCbot_fit_params[fpi] = bar.GetBotADCHit().GetFitParam(fpi);
               else bi->ADCbot_fit_params[fpi] = -1;
               if (bar.HasTopADCHit()) bi->ADCtop_fit_params[fpi] = bar.GetTopADCHit().GetFitParam(fpi);
               else bi->ADCtop_fit_params[fpi] = -1;
            }
         }
         //std::cout << "-------------------------" << std::endl;
      }
      if (evt.GetNumberOfTracks()>0) {
         int num = evt.GetNumberOfTracks();
         Node *tracknode = (Node *)XtMalloc(num * sizeof(Node));
         data->tracks.track_info = (TrackInfo *)XtMalloc(num * sizeof(TrackInfo));
         data->tracks.nodes = tracknode;
         data->tracks.num_nodes = num;
         memset(data->tracks.track_info, 0, num * sizeof(TrackInfo));
         memset(tracknode, 0, num * sizeof(Node));
         TrackInfo *ti = data->tracks.track_info;
         const TObjArray* HelixArray = evt.GetHelixArray();
         for (int i = 0; i < num; ++i, ++tracknode, ++ti) {
            TStoreHelix *h = (TStoreHelix *)HelixArray->At(i);
            TFitHelix hel(*h);
            ti->c = h->GetC();
            ti->chi2r = h->GetRchi2();
            ti->chi2z = h->GetZchi2();
            ti->D = h->GetD();
            ti->err2c = h->GetErrC();
            ti->err2D = h->GetErrD();
            ti->err2lambda = h->GetErrLambda();
            ti->err2phi0 = h->GetErrPhi0();
            ti->err2Rc = h->GetErrRc();
            ti->err2z0 = h->GetErrZ0();
            ti->lambda = h->GetLambda();
            ti->phi0 = h->GetPhi0();
            ti->Rc = h->GetRc();
            ti->status = h->GetStatus();
            ti->has_bar = h->HasBarHit();
            ti->track_num = i;
            ti->x0 = h->GetX0();
            ti->y0 = h->GetY0();
            ti->z0 = h->GetZ0();
            //ti->z0 = hel.Evaluate(h->GetD()*h->GetD()).z();
            ti->pX = h->GetMomentumV().X();
            ti->pY = h->GetMomentumV().Y();
            ti->pZ = h->GetMomentumV().Z();
            ti->branch = h->GetBranch();
            ti->double_branched = h->IsDoubleBranched();
            tracknode->x3         = ti->x0 / AG_SCALE;
            tracknode->y3         = ti->y0 / AG_SCALE;
            tracknode->z3         = ti->z0 / AG_SCALE;

         }
      }

      /* calculate the hit colour indices */
      calcHitVals(data);
      calcBarVals(data);

      data->agEvent    = &evt;
      data->run_number = runNo;
      data->event_id   = evt.GetEventNumber();
      data->event_time   = evt.GetTimeOfEvent();
      data->n_helices  = evt.GetNumberOfTracks();
      data->AWwf = &AWwf;
      data->PADwf = &PADwf;
      data->BVwf = &BVwf;
      data->barEvent   = &bars;
      if (data->detectorEvent) delete data->detectorEvent;
      data->detectorEvent = new TAGDetectorEvent(&evt, &bars);

      sendMessage(data, kMessageNewEvent);

      if (data->trigger_flag == TRIGGER_CONTINUOUS) {
         long delay = (long)(data->time_interval);
         XtTimerCallbackProc process = (XtTimerCallbackProc)do_next;
         XtAppAddTimeOut(data->the_app, delay, process, data);
      }
      if (data->trigger_flag == TRIGGER_QUIT) {
         if (!flags) flags = new TAFlags();
         *flags = TAFlag_QUIT;
         return flags;
      }

      // main event loop
      while (data && data->mMainWindow != NULL && !data->mNext) {
         if (!data->the_app) return flags;
         XEvent theEvent;
         XtAppNextEvent(data->the_app, &theEvent);
         // fast-forward to most recent pointer motion event (avoids
         // falling behind current mouse position if drawing is slow)
         if (theEvent.type == MotionNotify) {
            while (XCheckTypedEvent(data->display, MotionNotify, &theEvent)) {
            }
         }
         if (theEvent.type == ClientMessage) // X11 command...
         {
            XClientMessageEvent *e = (XClientMessageEvent *)&theEvent;
            // std::cout<<e->message_type<<std::endl;
            if (e->message_type == 303) // Click on X (close)
            {
               if (!flags) flags = new TAFlags();
               *flags = TAFlag_QUIT; // Call end run
               return flags;
            }
         }

         // dispatch the X event
         dispatchEvent(&theEvent);
         // update windows now if necessary (but only after all X events have been dispatched)
         if (!XPending(data->display)) PWindow::HandleUpdates();
      }
      data->mNext = 0;
   }
   return flags;
}

#ifdef BUILD_AG_SIM
// int Aged::ShowEvent(TStoreEvent &evt, TClonesArray *awSignals, TClonesArray *padSignals, TClonesArray *barSignals)
// {
//    int                         status = 0;
//    std::vector<ALPHAg::signal> s, p;
//    // TAFlowEvent fe;
//    // AgAnalysisFlow anaFlow(&fe, &evt);
//    // AgSignalsFlow sigFlow(&anaFlow, &s, &p);
//    // AgBarEventFlow barFlow(nullptr, barSignals);
//    std::cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << std::endl;
//    return status;
// }
#endif
