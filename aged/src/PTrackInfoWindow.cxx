//==============================================================================
// File:        PTrackInfoWindow.cxx
//
// Copyright (c) 2017, Phil Harvey, Queen's University
//==============================================================================
#include <Xm/RowColumn.h>
#include <Xm/Label.h>
#include <Xm/Form.h>
#include <Xm/PushB.h>
#include "ImageData.h"
#include "PTrackInfoWindow.h"
#include "PImageWindow.h"
#include "PProjImage.h"
#include "PSpeaker.h"
#include "PUtils.h"
#include "menu.h"
#include "TMath.h"
#include <string>

//---------------------------------------------------------------------------------
// PTrackInfoWindow constructor
//
PTrackInfoWindow::PTrackInfoWindow(ImageData *data)
              : PWindow(data)
{
    Widget  rc1, rc2;
    int     n;
    Arg     wargs[16];
    Widget  w, but;
        
    mLastNum = -1;
    
    n = 0;
    XtSetArg(wargs[n], XmNtitle, "Track Fit"); ++n;
    XtSetArg(wargs[n], XmNx, 200); ++n;
    XtSetArg(wargs[n], XmNy, 200); ++n;
    XtSetArg(wargs[n], XmNminWidth, 165); ++n;
    XtSetArg(wargs[n], XmNminHeight, 100); ++n;
    SetShell(CreateShell("hiPop",data->toplevel,wargs,n));
    SetMainPane(w = XtCreateManagedWidget("agedForm", xmFormWidgetClass,GetShell(),NULL,0));

    n = 0;
    XtSetArg(wargs[n], XmNx, 16); ++n;
    XtSetArg(wargs[n], XmNy, 8); ++n;
    XtSetArg(wargs[n], XmNwidth, 60); ++n;
    but = XtCreateManagedWidget("Next",xmPushButtonWidgetClass,w,wargs,n);
    XtAddCallback(but, XmNactivateCallback, (XtCallbackProc)NextProc, this);

    n = 0;
    XtSetArg(wargs[n], XmNy, 8); ++n;
    XtSetArg(wargs[n], XmNrightAttachment,  XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNrightOffset, 16); ++n;
    XtSetArg(wargs[n], XmNwidth, 60); ++n;
    but = XtCreateManagedWidget("Prev",xmPushButtonWidgetClass,w,wargs,n);
    XtAddCallback(but, XmNactivateCallback, (XtCallbackProc)PrevProc, this);

    n = 0;
    XtSetArg(wargs[n], XmNtopOffset, 35); ++n;
    XtSetArg(wargs[n], XmNpacking,          XmPACK_COLUMN); ++n;
    XtSetArg(wargs[n], XmNleftAttachment,   XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_FORM);  ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM); ++n;
    rc1 = XtCreateManagedWidget("hiRC1",xmRowColumnWidgetClass,w,wargs,n);

    n = 0;
    XtSetArg(wargs[n], XmNtopOffset, 35); ++n;
    XtSetArg(wargs[n], XmNpacking,          XmPACK_COLUMN); ++n;
    XtSetArg(wargs[n], XmNleftAttachment,   XmATTACH_WIDGET); ++n;
    XtSetArg(wargs[n], XmNleftWidget, rc1); ++n;
    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_FORM);  ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNrightAttachment,  XmATTACH_FORM); ++n;
    rc2 = XtCreateManagedWidget("eiRC2",xmRowColumnWidgetClass,w,wargs,n);
    
    XtCreateManagedWidget("Index:",  xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Status:",  xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Has Bar?:",  xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Rc:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Phi0:", xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("D:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Lambda:", xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("x0:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("y0:",    xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("z0:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("px:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("py:",    xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("pz:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Chi2 R:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Chi2 Z:",      xmLabelWidgetClass,rc1,NULL,0);
        
    track_index      .CreateLabel("trackIndex",    rc2,NULL,0);
    track_status     .CreateLabel("trackStatus",   rc2,NULL,0);
    track_has_bar     .CreateLabel("trackHasBar",   rc2,NULL,0);
    track_rc   .CreateLabel("trackRc", rc2,NULL,0);
    track_phi0     .CreateLabel("trackPhi0",   rc2,NULL,0);
    track_D   .CreateLabel("trackD", rc2,NULL,0);
    track_lambda     .CreateLabel("trackLambda",   rc2,NULL,0);
    track_x0      .CreateLabel("trackX0",    rc2,NULL,0);
    track_y0      .CreateLabel("trackY0",    rc2,NULL,0);
    track_z0      .CreateLabel("trackZ0",    rc2,NULL,0);
    track_px      .CreateLabel("trackPx",    rc2,NULL,0);
    track_py      .CreateLabel("trackPy",    rc2,NULL,0);
    track_pz      .CreateLabel("trackPz",    rc2,NULL,0);
    track_chi2r        .CreateLabel("trackChi2R",      rc2,NULL,0);
    track_chi2z        .CreateLabel("trackChi2Z",      rc2,NULL,0);

    data->mSpeaker->AddListener(this);  // listen for cursor motion
    
    ClearEntries();
}

PTrackInfoWindow::~PTrackInfoWindow()
{
}

void PTrackInfoWindow::NextProc(Widget /*w*/, PTrackInfoWindow *win, caddr_t /*call_data*/)
{
    ImageData *data = win->GetData();
    if (data->tracks.num_nodes) {
        data->cursor_hit += 1;
        if (data->cursor_hit >= data->tracks.num_nodes) data->cursor_hit = 0;
        data->cursor_sticky = 1;
        sendMessage(data, kMessageAddOverlay);
        sendMessage(data, kMessageCursorHit);
    }
}

void PTrackInfoWindow::PrevProc(Widget /*w*/, PTrackInfoWindow *win, caddr_t /*call_data*/)
{
    ImageData *data = win->GetData();
    if (data->tracks.num_nodes) {
        data->cursor_hit -= 1;
        if (data->cursor_hit < 0) data->cursor_hit = data->tracks.num_nodes-1;
        data->cursor_sticky = 1;
        sendMessage(data, kMessageAddOverlay);
        sendMessage(data, kMessageCursorHit);
    }
}

void PTrackInfoWindow::ClearEntries()
{

    char    *str = (char*)"-";
    track_index      .SetString(str);
    track_status    .SetString(str);
    track_has_bar    .SetString(str);
    track_rc   .SetString(str);
    track_phi0     .SetString(str);
    track_D   .SetString(str);
    track_lambda     .SetString(str);
    track_x0     .SetString(str);
    track_y0   .SetString(str);
    track_z0    .SetString(str);
    track_px     .SetString(str);
    track_py    .SetString(str);
    track_pz    .SetString(str);
    track_chi2r      .SetString(str);
    track_chi2z  .SetString(str);
}

/* UpdateSelf - set hit information window for PMT nearest the specified cursor position */
/* Note: hits must be tranformed to appropriate projection BEFORE calling this routine */
void PTrackInfoWindow::UpdateSelf()
{
    TrackInfo     *ti;
    ImageData   *data = mData;
    int         num = data->cursor_hit; // current hit number near cursor
    
#ifdef PRINT_DRAWS
    Printf("-updateHitInfo\n");
#endif
    mLastNum = num;
    if (num == -1 or data->cursor_hit_type != 3 ) {
        ClearEntries();
    } else {
        ti = data->tracks.track_info + num;
        std::string track_info = std::to_string(num+1) + " of " + std::to_string(data->tracks.num_nodes);
        track_index.SetString(track_info.c_str());
        track_status.SetString(std::to_string(ti->status).c_str());
        track_has_bar.SetString(std::to_string(ti->has_bar).c_str());
        track_rc.SetString(std::to_string(ti->Rc).c_str());
        track_phi0.SetString(std::to_string(ti->phi0).c_str());
        track_D.SetString(std::to_string(ti->D).c_str());
        track_lambda.SetString(std::to_string(ti->lambda).c_str());
        track_x0.SetString(std::to_string(ti->x0).c_str());
        track_y0.SetString(std::to_string(ti->y0).c_str());
        track_z0.SetString(std::to_string(ti->z0).c_str());
        track_px.SetString(std::to_string(ti->pX).c_str());
        track_py.SetString(std::to_string(ti->pY).c_str());
        track_pz.SetString(std::to_string(ti->pZ).c_str());
        track_chi2r.SetString(std::to_string(ti->chi2r).c_str());
        track_chi2z.SetString(std::to_string(ti->chi2z).c_str());
    }
}

void PTrackInfoWindow::ManageXYZ(int manage)
{
    if (manage==0) {}
}

// ResizeToFit - resize shell height to fit the labels
void PTrackInfoWindow::ResizeToFit()
{
    Widget      last_label;

    last_label = track_chi2z.GetWidget();
    PWindow::ResizeToFit(last_label);
}

// SetHitXYZ - show or hide XYZ labels according to data->hit_xyz setting
void PTrackInfoWindow::SetHitXYZ()
{
    if (GetData()->hit_xyz) {
        // show the XYZ labels
        ManageXYZ(1);
    } else {
        // hide the XYZ labels
        ManageXYZ(0);
    }
    ResizeToFit();
}

void PTrackInfoWindow::Listen(int message, void*/*message_data*/)
{
    switch (message) {
        case kMessageNewEvent:
            if (mLastNum >= 0) {
                // the event has changed, so the displayed hit data is now invalid
                // -> set the last displayed hit number to something invalid too
                //    to force the new data to be displayed
                mLastNum = 99999;
            }
            SetDirty();
            break;
        case kMessageCursorHit:
            // only dirty if this is a different hit
            if (mLastNum != mData->cursor_hit) {
                SetDirty();
            }
            break;
        case kMessageHitDiscarded:
            SetDirty();
            break;
        case kMessageHitXYZChanged:
            SetHitXYZ();
            break;
    }
}
