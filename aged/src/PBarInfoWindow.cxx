//==============================================================================
// File:        PBarInfoWindow.cxx
//
// Copyright (c) 2017, Phil Harvey, Queen's University
//==============================================================================
#include <Xm/RowColumn.h>
#include <Xm/Label.h>
#include <Xm/Form.h>
#include <Xm/PushB.h>
#include "ImageData.h"
#include "PBarInfoWindow.h"
#include "PImageWindow.h"
#include "PProjImage.h"
#include "PSpeaker.h"
#include "PUtils.h"
#include "menu.h"
#include "TMath.h"
#include <string>

//---------------------------------------------------------------------------------
// PBarInfoWindow constructor
//
PBarInfoWindow::PBarInfoWindow(ImageData *data)
              : PWindow(data)
{
    Widget  rc1, rc2;
    int     n;
    Arg     wargs[16];
    Widget  w, but;
        
    mLastNum = -1;
    
    n = 0;
    XtSetArg(wargs[n], XmNtitle, "Bar hit"); ++n;
    XtSetArg(wargs[n], XmNx, 200); ++n;
    XtSetArg(wargs[n], XmNy, 200); ++n;
    XtSetArg(wargs[n], XmNminWidth, 165); ++n;
    XtSetArg(wargs[n], XmNminHeight, 100); ++n;
    SetShell(CreateShell("hiPop",data->toplevel,wargs,n));
    SetMainPane(w = XtCreateManagedWidget("agedForm", xmFormWidgetClass,GetShell(),NULL,0));

    n = 0;
    XtSetArg(wargs[n], XmNx, 16); ++n;
    XtSetArg(wargs[n], XmNy, 8); ++n;
    XtSetArg(wargs[n], XmNwidth, 60); ++n;
    but = XtCreateManagedWidget("Next",xmPushButtonWidgetClass,w,wargs,n);
    XtAddCallback(but, XmNactivateCallback, (XtCallbackProc)NextProc, this);

    n = 0;
    XtSetArg(wargs[n], XmNy, 8); ++n;
    XtSetArg(wargs[n], XmNrightAttachment,  XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNrightOffset, 16); ++n;
    XtSetArg(wargs[n], XmNwidth, 60); ++n;
    but = XtCreateManagedWidget("Prev",xmPushButtonWidgetClass,w,wargs,n);
    XtAddCallback(but, XmNactivateCallback, (XtCallbackProc)PrevProc, this);

    n = 0;
    XtSetArg(wargs[n], XmNtopOffset, 35); ++n;
    XtSetArg(wargs[n], XmNpacking,          XmPACK_COLUMN); ++n;
    XtSetArg(wargs[n], XmNleftAttachment,   XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_FORM);  ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM); ++n;
    rc1 = XtCreateManagedWidget("hiRC1",xmRowColumnWidgetClass,w,wargs,n);

    n = 0;
    XtSetArg(wargs[n], XmNtopOffset, 35); ++n;
    XtSetArg(wargs[n], XmNpacking,          XmPACK_COLUMN); ++n;
    XtSetArg(wargs[n], XmNleftAttachment,   XmATTACH_WIDGET); ++n;
    XtSetArg(wargs[n], XmNleftWidget, rc1); ++n;
    XtSetArg(wargs[n], XmNtopAttachment,    XmATTACH_FORM);  ++n;
    XtSetArg(wargs[n], XmNbottomAttachment, XmATTACH_FORM); ++n;
    XtSetArg(wargs[n], XmNrightAttachment,  XmATTACH_FORM); ++n;
    rc2 = XtCreateManagedWidget("eiRC2",xmRowColumnWidgetClass,w,wargs,n);
    
    XtCreateManagedWidget("Index:",  xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Bar:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Cluster ID:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("BarHit type:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("TPC match:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("# TDC top:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("# TDC bot:",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Main Group:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("BarHit Time",      xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Top Amplitude:", xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Bot Amplitude:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Combined Amplitude:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Top ADC time:", xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Bot ADC time:",   xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Top TDC time:",    xmLabelWidgetClass,rc1,NULL,0);
    XtCreateManagedWidget("Bot TDC time:",      xmLabelWidgetClass,rc1,NULL,0);
    
    // create XYZ labels invisible
    bar_xyz_labels[0].CreateLabel("X:",  rc1,NULL,0);
    bar_xyz_labels[1].CreateLabel("Y:",  rc1,NULL,0);
    bar_xyz_labels[2].CreateLabel("Z:",  rc1,NULL,0);
    
    bar_index      .CreateLabel("barIndex",    rc2,NULL,0);
    bar_num     .CreateLabel("barNum",   rc2,NULL,0);
    bar_cluster     .CreateLabel("barCluster",   rc2,NULL,0);
    bar_type        .CreateLabel("barType",      rc2,NULL,0);
    bar_match        .CreateLabel("barMatch",      rc2,NULL,0);
    bar_n_tdc_top        .CreateLabel("barNTDCtop",      rc2,NULL,0);
    bar_n_tdc_bot        .CreateLabel("barNTDCbot",      rc2,NULL,0);
    bar_main_group     .CreateLabel("barMainGroup",   rc2,NULL,0);
    bar_time            .CreateLabel("barTime",      rc2,NULL,0);
    bar_top_adc   .CreateLabel("barTopADC", rc2,NULL,0);
    bar_bot_adc     .CreateLabel("barBotADC",   rc2,NULL,0);
    bar_comb_adc     .CreateLabel("barCombADC",   rc2,NULL,0);
    bar_top_adc_time   .CreateLabel("barTopADCtime", rc2,NULL,0);
    bar_bot_adc_time     .CreateLabel("barBotADCtime",   rc2,NULL,0);
    bar_top_tdc      .CreateLabel("barTopTDC",    rc2,NULL,0);
    bar_bot_tdc        .CreateLabel("barBotTDC",      rc2,NULL,0);

    
    // create XYZ labels invisible
    bar_xyz[0].CreateLabel("barX",    rc2,NULL,0);
    bar_xyz[1].CreateLabel("barY",    rc2,NULL,0);
    bar_xyz[2].CreateLabel("barZ",    rc2,NULL,0);
    
    // manage XYZ labels if hit_xyz is on
    if (!data->hit_xyz) ManageXYZ(0);
    
    data->mSpeaker->AddListener(this);  // listen for cursor motion
    
    ClearEntries();
}

PBarInfoWindow::~PBarInfoWindow()
{
}

void PBarInfoWindow::NextProc(Widget /*w*/, PBarInfoWindow *win, caddr_t /*call_data*/)
{
    ImageData *data = win->GetData();
    if (data->barpoints.num_nodes) {
        data->cursor_hit += 1;
        if (data->cursor_hit >= data->barpoints.num_nodes) data->cursor_hit = 0;
        data->cursor_sticky = 1;
        sendMessage(data, kMessageCursorHit);
    }
}

void PBarInfoWindow::PrevProc(Widget /*w*/, PBarInfoWindow *win, caddr_t /*call_data*/)
{
    ImageData *data = win->GetData();
    if (data->barpoints.num_nodes) {
        data->cursor_hit -= 1;
        if (data->cursor_hit < 0) data->cursor_hit = data->barpoints.num_nodes-1;
        data->cursor_sticky = 1;
        sendMessage(data, kMessageCursorHit);
    }
}

void PBarInfoWindow::ClearEntries()
{
    int     i;
    
    char    *str = (char*)"-";
    bar_index.SetString(str);
    bar_num.SetString(str);
    bar_cluster.SetString(str);
    bar_type.SetString(str);
    bar_match.SetString(str);
    bar_n_tdc_top.SetString(str);
    bar_n_tdc_bot.SetString(str);
    bar_main_group.SetString(str);
    bar_time.SetString(str);
    bar_top_adc.SetString(str);
    bar_bot_adc.SetString(str);
    bar_comb_adc.SetString(str);
    bar_top_adc_time.SetString(str);
    bar_bot_adc_time.SetString(str);
    bar_top_tdc.SetString(str);
    bar_bot_tdc.SetString(str);
    for (i=0; i<3; ++i) {
        bar_xyz[i].SetString(str);
    }
}

/* UpdateSelf - set hit information window for PMT nearest the specified cursor position */
/* Note: hits must be tranformed to appropriate projection BEFORE calling this routine */
void PBarInfoWindow::UpdateSelf()
{
    BarInfo     *bar;
    char        buff[64];
    ImageData   *data = mData;
    int         num = data->cursor_hit; // current hit number near cursor
    
#ifdef PRINT_DRAWS
    Printf("-updateHitInfo\n");
#endif
    mLastNum = num;
    if (num == -1 or data->cursor_hit_type != 2 ) {
        ClearEntries();
    } else {
        bar = data->barpoints.bar_info + num;
        std::string bar_info = std::to_string(num+1) + " of " + std::to_string(data->barpoints.num_nodes);
        bar_index.SetString(bar_info.c_str());
        bar_num.SetString(std::to_string(bar->barID).c_str());
        bar_cluster.SetString(std::to_string(bar->clusterID).c_str());
        bar_main_group.SetString(std::to_string(bar->main_group).c_str());
        bar_bot_adc.SetString(std::to_string(int(bar->ADCbot)).c_str());
        bar_top_adc.SetString(std::to_string(int(bar->ADCtop)).c_str());
        bar_comb_adc.SetString(std::to_string(bar->combADC).c_str());
        bar_bot_adc_time.SetString(std::to_string(int(bar->ADCbot_start)).c_str());
        bar_top_adc_time.SetString(std::to_string(int(bar->ADCtop_start)).c_str());
        bar_time.SetString(std::to_string(bar->time).c_str());
        bar_bot_tdc.SetString(std::to_string(bar->TDCbot).c_str());
        bar_top_tdc.SetString(std::to_string(bar->TDCtop).c_str());
        bar_type.SetString(std::to_string(bar->hittype).c_str());
        bar_match.SetString(std::to_string(bar->matched).c_str());
        bar_n_tdc_bot.SetString(std::to_string(bar->TDCbot_all.size()).c_str());
        bar_n_tdc_top.SetString(std::to_string(bar->TDCtop_all.size()).c_str());
        float *xyz = &(data->barpoints.nodes[num].x3);
        for (int i=0; i<3; ++i) {
#ifdef ANTI_ALIAS
            snprintf(buff,64,"%.1f",xyz[i] * AG_SCALE);
#else
            snprintf(buff,64,"%.1f",xyz[i] * AG_SCALE);
#endif
            bar_xyz[i].SetString(buff);
        }
    }
}

void PBarInfoWindow::ManageXYZ(int manage)
{
    Widget      widgets[6];
    
    for (int i=0; i<3; ++i) {
        widgets[i] = bar_xyz[i].GetWidget();
        widgets[i+3] = bar_xyz_labels[i].GetWidget();
    }
    if (manage) {
        // must manage right and left rowcol widgets separately
        XtManageChildren(widgets, 3);
        XtManageChildren(widgets+3, 3);
    } else {
        XtUnmanageChildren(widgets, 3);
        XtUnmanageChildren(widgets+3, 3);
    }
}

// ResizeToFit - resize shell height to fit the labels
void PBarInfoWindow::ResizeToFit()
{
    Widget      last_label;

    last_label = bar_xyz[2].GetWidget();
    PWindow::ResizeToFit(last_label);
}

// SetHitXYZ - show or hide XYZ labels according to data->hit_xyz setting
void PBarInfoWindow::SetHitXYZ()
{
    if (GetData()->hit_xyz) {
        // show the XYZ labels
        ManageXYZ(1);
    } else {
        // hide the XYZ labels
        ManageXYZ(0);
    }
    ResizeToFit();
}

void PBarInfoWindow::Listen(int message, void*/*message_data*/)
{
    switch (message) {
        case kMessageNewEvent:
            if (mLastNum >= 0) {
                // the event has changed, so the displayed hit data is now invalid
                // -> set the last displayed hit number to something invalid too
                //    to force the new data to be displayed
                mLastNum = 99999;
            }
            SetDirty();
            break;
        case kMessageCursorHit:
            // only dirty if this is a different hit
            if (mLastNum != mData->cursor_hit) {
                SetDirty();
            }
            break;
        case kMessageHitDiscarded:
            SetDirty();
            break;
        case kMessageHitXYZChanged:
            SetHitXYZ();
            break;
    }
}
