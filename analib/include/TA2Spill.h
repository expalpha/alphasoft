#ifndef _TA2Spill_
#define _TA2Spill_
#include "TSpillScalerData.h"
#include "TA2SpillSequencerData.h"

#ifdef BUILD_A2
#include "TSVD_QOD.h"
#include "TSISChannel.h"
#include "TSISEvent.h"
#include "TA2SpillScalerData.h"

class TA2Spill: public TSpill
{
public:
   TA2SpillScalerData* fScalerData;
   TA2SpillSequencerData*  fSeqData;
   TA2Spill();
   TA2Spill(int runno, uint32_t unixtime);
   TA2Spill(int runno, uint32_t unixtime, const char* format, ...);
   TA2Spill(int runno, TDumpMarkerPair<TSVD_QOD,TSISEvent,NUM_SIS_MODULES>* d);
   TA2Spill* operator/( TA2Spill* b);
   TA2Spill* operator+( TA2Spill* b);
   TA2Spill(const TA2Spill& a);
   double GetStartTime() const
   {
      if (fScalerData)
         return fScalerData->GetStartTime();
      else
         return -1.;
   }
   double GetStopTime() const
   {
      if (fScalerData)
         return fScalerData->GetStopTime();
      else
         return -1;
   }
   std::string GetSequenceName() const
   {
      if (fSeqData)
         return fSeqData->fSeqName;
      else
         return "none";
   }
   using TObject::Print;
   virtual void Print();
   std::string toJson() const;


   int AddToDatabase(sqlite3 *db, sqlite3_stmt * stmt);
   TString Content(const std::vector<TSISChannel>,const size_t,const size_t,const size_t);
   std::string ContentCSVTitle(std::vector<std::string> ChannelNames = {}) const
   {
      std::string title = TSpill::ContentCSVTitle();
      if (fSeqData)
         title += fSeqData->ContentCSVTitle();
      if (fScalerData)
         title += fScalerData->ContentCSVTitle(ChannelNames);
      return title;
   }
   std::string ContentCSV() const
   {
      std::string line = TSpill::ContentCSV();
      if (fSeqData)
         line += fSeqData->ContentCSV();
      if (fScalerData)
         line += fScalerData->ContentCSV();
      return line;
   }

   bool Ready( bool have_svd);
   ~TA2Spill();
   ClassDef(TA2Spill,1);
};
#endif




#endif
