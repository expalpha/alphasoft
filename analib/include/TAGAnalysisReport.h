#ifndef _TAGANALYSISREPORT_
#define _TAGANALYSISREPORT_

#include "TAnalysisReport.h"

#ifdef BUILD_AG

/// @brief  Class to track the git versioning, settings and basic QOD metrics of analysis programs
/// Child class to TAnalysisReport
class TAGAnalysisReport: public TAnalysisReport
{
private:

    /// These are fast counters... do not use fIntValue and fDoubleValue maps for 
    /// these as we want to use these every event (not just at the end of run)
    int nStoreEvents = 0;
    int nSigEvents = 0;
    bool isSim = false;
    double last_tpc_ts = -1; ///< Track the last timestamp from the TPC this class sees
    /// @brief Track metrics of many trigger channels
    std::vector<std::string> fTriggerSettingChannels = {
        "TrigPulser",
        "TrigEsataNimGrandOr",
        "TrigAdc16GrandOr",
        "TrigAdc32GrandOr",
        "Trig1ormore",
        "Trig2ormore",
        "Trig3ormore",
        "Trig4ormore",
        "TrigAdcGrandOr",
        "TrigAwCoincA",
        "TrigAwCoincB",
        "TrigAwCoincC",
        "TrigAwCoincD",
        "TrigAw1ormore",
        "TrigAw2ormore",
        "TrigAw3ormore",
        "TrigAw4ormore",
        "TrigAwMLU",
        "TrigBscGrandOr",
        "TrigBscMult",
        "TrigCoinc",
        "TrigAwCoinc",
        "TrigAdc16_1ormore",
        "TrigAdc16_2ormore",
        "TrigAdc16_3ormore",
        "TrigAdc16_4ormore",
        "TrigAwGrandOr"};
 public:
    TAGAnalysisReport();
    TAGAnalysisReport(const TAGAnalysisReport& r);
    TAGAnalysisReport operator=(const TAGAnalysisReport& r);

    void Flush(
       double sum_aw,       //Results from deconv module
       double sum_pad,      //Results from deconv module
       double sum_match,    //Results from match module
       double sum_tracks,   //Results from reco module
       double sum_r_sigma,  //Results from reco module
       double sum_z_sigma,  //Results from reco module
       double sum_verts,    //Results from reco module
       double sum_vert_CT,
       double sum_vert_GT,
       double sum_vert_chi,
       double sum_hits,     //Results from reco module
       double sum_bars,
       int with_tracks,
       bool is_sim);
    void IncrementStoreEvents()
    {
       nStoreEvents++;
       return;
    }
    void IncrementSigEvents()
    {
        nSigEvents++;
        return;
    }
    void SetLastTPCTime(const double& t)
    {
        last_tpc_ts = t;
        return;
    }
    void SetIsSim(bool b) {isSim=b;}
    bool IsSim() const {return isSim;}
    TAGAnalysisReport(int runno);
    virtual ~TAGAnalysisReport();
    
    const std::vector<std::string> GetTriggerSettingChannels() const  { return fTriggerSettingChannels; }
    void PrintAllTriggerSettings() const;
    std::string ActiveTriggers() const;
    using TObject::Print;
    virtual void Print();
    std::string CSVTitleLine() const;
    std::string CSVLine() const;
    ClassDef(TAGAnalysisReport,3);
    };
#endif

#endif