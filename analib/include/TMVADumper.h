
#include "generalizedspher.h"
#include <stdio.h>



#include "TTree.h"
#include "TMVA/Reader.h"

class TMVADumper
{
   public:
   TTree* fTree;
   /// @brief Default constructor. Creates branches for each variable names in the dumper class.
   /// @param tree TTree to dump the data to.
   TMVADumper(TTree* tree)
   {
      fTree = tree;
   }

   /// @brief Loads the list of variables to the reader for dumping.
   /// @param reader The chosen TMVA reader
   virtual void LoadVariablesToReader(TMVA::Reader* reader) = 0;

};

