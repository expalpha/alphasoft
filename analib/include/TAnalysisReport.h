#ifndef _ANALYSIS_REPORT_
#define _ANALYSIS_REPORT_

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include "TObject.h"
#include <unistd.h> // readlink()
#include <ctime>
#include "TH1D.h"
#include <map>
#include "TObjString.h"

/// \class TAnalysisReport
/// Class to track the git versioning, settings and basic QOD metrics of analysis programs
/// Parent class to TAGAnalysisReport and TA2AnalsisReport
///
/// Only on TAnalysisReport is stored per run per analysis program
class TAnalysisReport : public TObject {
private:
   const int   fRunNumber;       ///< Run Number of analysis report
   std::string fProgramName;     ///< Name of analysis program that created this object
   std::string fProgramPath;     ///< Relative path of analysis program
   std::string fProgramPathFull; ///< Full path of analysis program

   uint32_t fStartRunUnixTime; ///< Start time of run in unix time from ODB at start
   uint32_t fStopRunUnixTime;  ///< Stop time of run in unix time from ODB at end run

   std::string    fAnalysisHost;    ///< Host name of system running analysis (MacOS fails to report this)
   const uint32_t fCompilationDate; ///< Date this repository was built
   const std::string
      GitBranch; ///< Branch of git repository at compile time, if the code has been modified this isn't always accurate
   const uint32_t    fGitDate;     ///< Date of last git commit in this repository
   const std::string fGitHash;     ///< Version of git at compile time
   const std::string fGitHashLong; ///< Long version of git at compile time
   const std::string fGitDiff;     ///< Changes to git since last commit at compile time

protected:
   /// General containers for child classes to store data
   // Its not super fast, but these only get called at Flush and Print (once per run)

   /// @brief General containers for child classes to store bool type data
   std::map<std::string, bool> fBoolValue;
   /// @brief General containers for child classes to store int type data
   std::map<std::string, int> fIntValue;
   /// @brief General containers for child classes to store double type data
   std::map<std::string, double> fDoubleValue;
   /// @brief General containers for child classes to store string type data
   std::map<std::string, std::string> fStringValue;

public:
   /// @brief Const getter for reading the bool data stored in this container
   /// @param s
   /// @return
   bool FindBool(const std::string &s) const { return fBoolValue.find(s)->second; }
   /// @brief Const getter for reading the int data stored in this container
   /// @param s
   /// @return
   int FindInt(const std::string &s) const { return fIntValue.find(s)->second; }

   /// @brief Const getter for reading the double data stored in this container
   /// @param s
   /// @return
   double FindDouble(const std::string &s) const { return fDoubleValue.find(s)->second; }
   /// @brief Const getter for reading the string data stored in this container
   /// @param s
   /// @return
   std::string FindString(const std::string &s) const { return fStringValue.find(s)->second; }

   /// @brief Add some bool data to this container
   /// @param s
   /// @param b
   void SetBool(const std::string &s, bool b) { fBoolValue[s] = b; }
   /// @brief Add some int data to this container
   /// @param s
   /// @param b
   void SetInt(const std::string &s, int i) { fIntValue[s] = i; }
   /// @brief Add some double data to this container
   /// @param s
   /// @param b
   void SetDouble(const std::string &s, double d) { fDoubleValue[s] = d; }
   /// @brief Add some string data to this container
   /// @param s
   /// @param b
   void SetString(const std::string &s, const std::string &v) { fStringValue[s] = v; }

   TAnalysisReport();
   TAnalysisReport(const TAnalysisReport &r);
   TAnalysisReport operator=(const TAnalysisReport &r);
   TAnalysisReport(int runno);
   virtual ~TAnalysisReport();
   /// @brief Get run number from this container
   /// @return
   int         GetRunNumber() const { return fRunNumber; }
   /// @brief Get the name of the analysis program that generated this report
   /// @return 
   std::string GetProgramName() const { return fProgramName; }
   /// @brief Get the relative path of the analysis program that generated this report
   /// @return 
   std::string GetProgramPath() const { return fProgramPath; }
   /// @brief Get the full path of the analysis program that generated this report
   /// @return 
   std::string GetProgramPathFull() const { return fProgramPathFull; }
   /// @brief Set the unix time of the run start
   /// @param time 
   ///
   /// Set by AnalysisReportModule during BeginRun()
   void        SetStartTime(uint32_t time) { fStartRunUnixTime = time; }
   /// @brief Set the unix time of the run start
   /// @param time 
   ///
   /// Set by AnalysisReportModule during EndRun()
   void        SetStopTime(uint32_t time) { fStopRunUnixTime = time; }
   /// @brief Get the unix time of the run start
   /// @param time 
   ///
   /// The value is collected from the ODB in the BeginRun stage of analysis, will return 0 if the first subrun wasnt processed
   uint32_t    GetRunStartTime() const { return fStartRunUnixTime; }
   /// @brief Get the unix time of the run stop
   /// @param time 
   ///
   /// The value is collected from the ODB in the EndRun stage of analysis, will return 0 if the last subrun wasnt to the end
   uint32_t    GetRunStopTime() const { return fStopRunUnixTime; }
   std::string GetRunStartTimeString() const;
   std::string GetRunStopTimeString() const;
   /// @brief Get the hostname of the machine that processed the MIDAS file
   /// @return 
   std::string GetAnalysisHost() const { return fAnalysisHost; }
   /// @brief Get unix time of last git commit found at build time
   /// @return 
   uint32_t    GetCompilationDate() const { return fCompilationDate; }
   std::string GetCompilationDateString() const;
   /// @brief Get the unix time of the last commit
   /// @return 
   uint32_t    GetGitDate() const { return fGitDate; }
   std::string GetGitDateString() const;
   /// @brief Get the git branch detected at build time
   /// @return 
   std::string GetGitBranch() const { return GitBranch; }
   /// @brief Get the git version detected at build time
   /// @return 
   std::string GetGitHash() const { return fGitHash; }
   /// @brief Get the long git version detected at build time
   /// @return 
   std::string GetGitHashLong() const { return fGitHashLong; }
   std::string GetGitDiff() const;
   /// @brief Get the filename of the jon file used in the reconstrution
   /// @return
   ///
   /// If there is no entry, it will return "Unknown json file", this value was not set before commit b6dc2c3
   std::string GetAnaSettingsFilename() const {
      if (fStringValue.count("AnaSettingsFilename"))
         return fStringValue.at("AnaSettingsFilename");
      return "Unknown json file";
   }
   /// @brief Set the filename of the json file used the reconstruction
   /// @return
   ///
   /// AnalysisReport_module sets this during BeginRun
   void SetAnaSettingsFilename(const std::string& filename)  { fStringValue["AnaSettingsFilename"] = filename; }
   /// @brief Get the filename of the json used in the reconstruction
   /// @return
   ///
   /// If nothing is set, it will return an empty string (very old root files wont have this set), ie before commit b6dc2c3
   std::string GetAnaSettingsString() const {
      if (fStringValue.count("AnaSettingsString"))
         return fStringValue.at("AnaSettingsString");
      return "";
   }
   /// @brief Set the contents of the json file used the reconstruction
   /// @return
   ///
   /// AnalysisReport_module sets this during BeginRun
   void SetAnaSettingsString(const TObjString& str) {fStringValue["AnaSettingsString"] = str.GetString().Data(); }

   void        PrintHeader();
   std::string HeaderTitleLine() const;
   std::string HeaderCSVLine() const;
   void        PrintFooter();
   std::string FooterTitleLine() const;
   std::string FooterCSVLine() const;
   std::string GetVersionLine() const;

   ClassDef(TAnalysisReport, 3);
};

#endif
