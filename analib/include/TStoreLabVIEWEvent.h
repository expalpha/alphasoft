#ifndef _TStoreLabVIEWEvent_
#define _TStoreLabVIEWEvent_

#include <iostream>
#ifndef ROOT_TObject
#include "TObject.h"
#endif

#ifndef ROOT_TString
#include "TString.h"
#endif

/// @brief Class for storing feLabVIEW data in root TTree
class TStoreLabVIEWEvent : public TObject {
private:
   std::string         BankName;   ///< 4 Character bank name from MIDAS
   std::vector<double> m_data;     ///< Array stored inside the MIDAS bank (list of doubles with a zero'th entry of a timestamp)
   uint32_t            MIDAS_TIME; ///< Timestmap according to MIDAS
   double              run_time;   ///< Seconds since the start of run 
   double              labview_time; ///< Time according to the zeroth entry of the timestamp
   int                 RunNumber;  ///< Run Number of event

public:
   TStoreLabVIEWEvent(const TStoreLabVIEWEvent &Event);
   TStoreLabVIEWEvent();
   TStoreLabVIEWEvent(std::string p_BankName, std::vector<double> p_data, uint32_t p_MIDAS_TIME, double p_run_time,
                      double p_labview_time, int runNumber);

   using TObject::Print;
   virtual void Print();
   virtual ~TStoreLabVIEWEvent();

   /// @brief Get the MIDAS bankname of event
   /// @return 
   const std::string          GetBankName() const { return BankName; }
   /// @brief Get full the array of the data in the MIDAS bank 
   /// @return 
   const std::vector<double> &GetData() const { return m_data; }
   /// @brief Get the element of array of the data in the MIDAS bank
   /// @param i 
   /// @return 
   ///
   /// Includes range check
   double                     GetArrayEntry(int i) const { return m_data.at(i); }
   /// @brief Get the MIDAS timestamp in the MIDAS bank
   /// @return 
   uint32_t                   GetMIDAS_TIME() const { return MIDAS_TIME; }
   /// @brief Second since the start of run
   /// @return 
   double                     GetRunTime() const { return run_time; }
   /// @brief Get the time stamp calculated from the zeroth entry of the data array
   /// @return 
   double                     GetLabviewTime() const { return labview_time; }
   /// @brief Get the run number of the event
   /// @return 
   int                        GetRunNumber() const { return RunNumber; }

   void Reset();

   ClassDef(TStoreLabVIEWEvent, 1);
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
