#ifndef _TSpill_
#define _TSpill_
#include "TObject.h"
#include <iostream>
#include <bitset>
#include "TString.h"
#include "sqlite3.h"
#include "TDumpList.h"
#include "Sequencer_Channels.h"
#include <map>

class TSpill: public TObject
{
public:
   int                   fRunNumber;
   bool                  fIsDumpType;
   bool                  IsInfoType;
   uint32_t              fUnixtime;
   std::string           fName;
   int                   fRepetition;
   static std::map<std::string,int> fDumpCounter;
   bool                  fFuzzyStart = false;
   bool                  fFuzzyStop = false;

   TSpill();
   TSpill(int runno, uint32_t unixtime);
   ~TSpill();
   //TSpill(const char* name);
   void InitByName(const char* format, va_list args);
   TSpill(int runno, uint32_t unixtime, const char* format, ...);
   TSpill(const TSpill& a);
   TSpill& operator =(const TSpill& rhs);
private:
   //Recursive function to compare strings with wildcard support (called by IsMatchForDumpName)
   bool MatchWithWildCards(const char *first, const char * second) const;
public:
   static void ClearDumpCounter() { fDumpCounter.clear(); }
   bool IsMatchForDumpName(const std::string& dumpname) const;
   virtual double GetStartTime() const = 0;
   virtual double GetStopTime() const = 0;
   bool DumpHasMathSymbol() const;
   using TObject::Print;
   virtual void Print();
   virtual std::string toJson() const;
   std::string GetUnsanitisedName() const {
     return fName;
   }

   std::string GetSanitisedName() const {
     std::string name = "";
     for (const char& c: fName)
     {
       // Ignore special characters
       if (c == '"' || c == '\\')
          continue;
       // Replace whitespace with _
       if ( c == ' ' || c == '\t')
       {
          name += '_';
          continue;
       }
       name += c;
     }
     return name;
   }


   virtual int AddToDatabase(sqlite3 *db, sqlite3_stmt * stmt);
   std::string ContentCSVTitle() const;
   std::string ContentCSV() const;
   ClassDef(TSpill,2);
};

#endif
