#include "TA2Spill.h"

#ifdef BUILD_A2

ClassImp(TA2Spill)

TA2Spill::TA2Spill()
{
   fSeqData    =NULL;
   fScalerData =NULL;
}
TA2Spill::TA2Spill(int runno, uint32_t unixtime): TSpill(runno, unixtime)
{
   fSeqData    =NULL;
   fScalerData =NULL;
}

TA2Spill::TA2Spill(int runno, uint32_t unixtime, const char* format, ...):
   TSpill(runno,unixtime)
{
   fSeqData    =NULL;
   fScalerData =NULL;
   va_list args;
   va_start(args,format);
   InitByName(format,args);
   va_end(args);
}

TA2Spill::TA2Spill(int runno,TDumpMarkerPair<TSVD_QOD,TSISEvent,NUM_SIS_MODULES>* d ):
   TSpill(runno, d->fStartDumpMarker->fMidasTime, d->fStartDumpMarker->fDescription.c_str())
{
   if (d->fStartDumpMarker && d->fStopDumpMarker) fIsDumpType=true;
   fScalerData = new TA2SpillScalerData(d);
   fSeqData = new TA2SpillSequencerData(d);
   //Print();
}

TA2Spill::TA2Spill(const TA2Spill& a):
   TSpill(a)
{
   if (a.fScalerData)
      fScalerData=new TA2SpillScalerData(*a.fScalerData);
   else
      fScalerData=NULL;
   if (a.fSeqData)
      fSeqData=new TA2SpillSequencerData(*a.fSeqData);
   else
      fSeqData=NULL;
}

TA2Spill::~TA2Spill()
{
   if (fScalerData)
      delete fScalerData;
   fScalerData=NULL;
   if (fSeqData)
      delete fSeqData;
   fSeqData=NULL;
}

#include "assert.h"


TA2Spill* TA2Spill::operator/( TA2Spill* b)
{
   //c=a/b
   TA2Spill* c=new TA2Spill(this->fRunNumber, this->fUnixtime);
   c->IsInfoType=true;
   assert(this->fRunNumber == b->fRunNumber);

   assert(this->fScalerData->ScalerFilled.size());
   assert(b->fScalerData->ScalerFilled.size());

   assert(this->fScalerData->VertexFilled);
   assert(b->fScalerData->VertexFilled);
   if (DumpHasMathSymbol())
      fName='('+fName+')';
   if (b->DumpHasMathSymbol())
   {
      b->fName='('+b->fName+')';
   }
   c->fName = this->fName + " / " + b->fName + "(%)";

   c->fScalerData =*fScalerData / b->fScalerData;

   c->fIsDumpType=false;
   return c;
}

TA2Spill* TA2Spill::operator+( TA2Spill* b)
{
   //c=a/b
   TA2Spill* c=new TA2Spill(this->fRunNumber, this->fUnixtime);
   c->IsInfoType=true;
   assert(this->fRunNumber == b->fRunNumber);

   assert(this->fScalerData->ScalerFilled.size());
   assert(b->fScalerData->ScalerFilled.size());

   assert(this->fScalerData->VertexFilled);
   assert(b->fScalerData->VertexFilled);

   if (DumpHasMathSymbol())
      fName='('+fName+')';

   if (b->DumpHasMathSymbol())
      b->fName='('+b->fName+')';

   c->fName = this->fName + " + " + b->fName;

   c->fScalerData =*fScalerData + b->fScalerData;

   c->fIsDumpType=false;
   return c;
}

bool TA2Spill::Ready( bool have_svd)
{
   if (fIsDumpType)
   {
      return fScalerData->Ready(have_svd);
   }
   else
   {
      return true;
   }
}

void TA2Spill::Print()
{
   std::cout<<"Dump name:"<<fName<<"\t\tIsDumpType:"<<fIsDumpType<<std::endl;
   if (fSeqData)
      fSeqData->Print();
   if (fScalerData)
      fScalerData->Print();
   std::cout<<"Ready? "<< Ready(true) << " " << Ready(false)<<std::endl;
   std::cout<<std::endl;
}

std::string TA2Spill::toJson() const
{
   std::string json = "{";
   json += "\"RunNumber\":" + std::to_string(fRunNumber) + ",";
   json += "\"UnixTime\":" + std::to_string(fUnixtime) + ",";
   json += "\"Name\":\"" + fName + "\"";
   if (fSeqData)
      json += ",\"Sequence\":" + fSeqData->toJson();
   if (fScalerData)
      json += ",\"Scaler\":" + fScalerData->toJson();
   json += "}";
   return json;
}

TString TA2Spill::Content(const std::vector<TSISChannel> sis_channels, const size_t SeqIndentation, const size_t DumpNameWidth, const size_t CountColumnWidth)
{
   
   TString log;
   //if (indent){
    log += "   "; // indentation     
   //}
   std::string units="";
   {
      int open=fName.find_last_of('(');
      int close=fName.find_last_of(')');
      if (open>0 &&       //Have open bracket
         close > open &&  //Have close bracket
         close - open<6)  //Units less than 6 characters long
         units=(fName.substr (open+1,close-open-1));
   }
   if (fScalerData)
   {
      char buf[200];
      if(!fFuzzyStart && !fFuzzyStop)
         snprintf(buf,200,"[%8.3lf-%8.3lf]=%8.3lfs |",
                  fScalerData->fStartTime,
                  fScalerData->fStopTime,
                  fScalerData->fStopTime-fScalerData->fStartTime
                  ); // timestamps 
      if(fFuzzyStart)
         snprintf(buf,200,"[%8.5s-%8.3lf]=%8.3lfs |",
                  "Fuzzy",
                  fScalerData->fStopTime,
                  fScalerData->fStopTime-fScalerData->fStartTime
                  ); // timestamps 
      if(fFuzzyStop)
         snprintf(buf,200,"[%8.3lf-%8.5s]=%8.3lfs |",
                  fScalerData->fStartTime,
                  "Fuzzy",
                  fScalerData->fStopTime-fScalerData->fStartTime
                  ); // timestamps 
      if(fFuzzyStart && fFuzzyStop)
         snprintf(buf,200,"[%8.5s-%8.5s]=%8.3lfs |",
                  "Fuzzy",
                  "Fuzzy",
                  fScalerData->fStopTime-fScalerData->fStartTime
                  ); // timestamps 
      log += buf;
   }
   else
   {
      if (fUnixtime)
      {
         char buf[80];
         struct tm * timeinfo = ( (tm*) &fUnixtime);
         snprintf(buf,80,"[%d:%d:%d]", timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
         log += buf;
      }
   }
   if (fSeqData)
   {
      std::string dump_name;
      for (int i = 0; i < fSeqData->fSequenceNum; i++)
         dump_name += std::string(SeqIndentation,' ');
      dump_name += fName; // dump description
      dump_name += "[" + std::to_string(fRepetition) + "]";
      if (dump_name.size() > DumpNameWidth)
         dump_name = dump_name.substr(0,DumpNameWidth);
      else if (dump_name.size() < DumpNameWidth)
         dump_name.insert(dump_name.size(), DumpNameWidth - dump_name.size(), ' ');
      log += dump_name + std::string("|");
   }
   else
   {
      log += std::string(" ") + fName;
   }
   if (fScalerData)
   {
      char buf[80];
      for (const TSISChannel& chan: sis_channels)
      {
         int counts=-1;
         //If valid channel number:
         if (chan.IsValid())
            counts=fScalerData->fDetectorCounts[chan.toInt()]; //toInt is a pretty unclean / unsafe solution. Look for [] operator overload
         snprintf(buf,80,"%*d",(int)CountColumnWidth,counts);
         log += buf;
         if (units.size())
            log += units;
         else
            log += " ";
      }
      snprintf(buf,80,"%*d",(int)CountColumnWidth,fScalerData->fPassCuts);
      log += buf;
      snprintf(buf,80,"%*d",(int)CountColumnWidth,fScalerData->fPassMVA);
      log += buf;
      log += "";
   }
   return log;
}


int TA2Spill::AddToDatabase(sqlite3 *db, sqlite3_stmt * stmt)
{
   if (!fSeqData) return -1;
   if (!fScalerData) return -1;
   TString sqlstatement = "INSERT INTO DumpTable VALUES ";
   sqlstatement+= "(";
   sqlstatement+= fRunNumber;
   sqlstatement+= ",";
   sqlstatement+= fSeqData->fSequenceNum;
   sqlstatement+= ",";
   sqlstatement+= fSeqData->fDumpID;
   sqlstatement+= ",";
   sqlstatement+= fName;
   sqlstatement+= ",";
   sqlstatement+= fUnixtime;
   sqlstatement+= ",";
   sqlstatement+= fScalerData->fStartTime;
   sqlstatement+= ",";
   sqlstatement+= fScalerData->fStopTime;
   for (int i=0;i<64; i++)
   {
      sqlstatement+= ",";
      sqlstatement+=fScalerData->fDetectorCounts[i];
   }
   sqlstatement+=",";
   sqlstatement+=fScalerData->fPassCuts;
   sqlstatement+=",";
   sqlstatement+=fScalerData->fPassMVA;
   sqlstatement+=");";
   std::cout<<"HELLO!!\t"<<sqlstatement<<std::endl;
   
   sqlite3_prepare( db, sqlstatement.Data(), -1, &stmt, NULL );//preparing the statement
   sqlite3_step( stmt );//executing the statement
   
   sqlite3_finalize(stmt);
   return 0;
}
#endif
