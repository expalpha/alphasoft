#ifndef _TSequencerState_
#include "TSequencerState.h"
#endif


ClassImp(TSequencerState)



TSequencerState::TSequencerState()
{
// ctor
  fID = 0;
  fSeqNum = 0;
  fState = 0;
  fTime = 0.;

  syncs_Nsyncsset_Digital = new TSequencerStateSyncs();
  syncs_Nsyncsset_HV = new TSequencerStateSyncs();
}

TSequencerState::TSequencerState(const TSequencerState& State):
   TObject(),
   fID(State.GetID()),
   fSeq(State.GetSeq()),
   fSeqNum(State.GetSeqNum()),
   fState(State.GetState()),
   fTime(State.GetDuration()),
   fDO(*State.GetDigitalOut()),
   fAO(*State.GetAnalogueOut()),
   fTI(*State.GetTriggerIn()),
   fComment(State.GetComment())
{
//copy constructor
  
}

void TSequencerState::Print()
{
  std::cout<<"Seq:\t"<<fSeq<<std::endl;
  std::cout<<"num:\t"<<fSeqNum<<std::endl;
  std::cout<<"ID:\t"<<fID<<std::endl;
  std::cout<<"state:\t"<<fState<<std::endl;
  std::cout<<"Duration:\t"<<fTime<<std::endl;
  std::cout<<"Digital Out:\t";
  if (fDO.Channels.size())
  {
     for (size_t i=0; i<fDO.Channels.size(); i++)
        std::cout<<fDO.Channels[i]<<" ";
     std::cout<<std::endl;
  }
  if (fAO.AOi.size() || fAO.AOf.size())
  {
    std::cout<<"Previous AO state:\t"<<fAO.PrevState<<std::endl;
    std::cout<<"Number of steps:\t"<<fAO.steps<<std::endl;
    std::cout<<"Analogue Out Initial:\t";
    for (size_t i=0; i<fAO.AOi.size(); i++)
      std::cout<<fAO.AOi[i]<<" ";
    std::cout<<std::endl;
    std::cout<<"Analogue Out Final:\t";
    for (size_t i=0; i<fAO.AOf.size(); i++)
      std::cout<<fAO.AOf[i]<<" ";
    std::cout<<std::endl;
  }
  if (fTI.Channels.size())
  {
    std::cout<<"Trigger In Wait t"<<fTI.waitTime<<std::endl;
    std::cout<<"Trigger In InfoWait"<<fTI.InfWait<<std::endl;
    std::cout<<"Trigger In:\t";
    for (size_t i=0; i<fTI.Channels.size(); i++)
      std::cout<<fTI.Channels[i]<<" ";
    std::cout<<std::endl;
  }
  std::cout<<"Comment:"<<GetComment()<<std::endl;
}

//assuming that the the second element odf the driver map is the Digital output channel
std::map<TString,int> TSequencerState::FindDigitalSyncs(std::map<TString,int> syncchan, int &nsyncs) const//sync channels of the current driver
{
  std::map<TString,int> map;
  int Nsyncs=0;
  if (fDO.Channels.size())
  {
    std::map<TString,int>::iterator it;
    for (it = syncchan.begin(); it != syncchan.end(); it++)
    {
      if(fDO.Channels[it->second]==1) //verify if in the current state there is a DO that corresponds to a sync channel of the driver
      {
        Nsyncs++; //counting how many syncs we have in the current state
        map.insert({it->first,1}); //otherwise insert a new sync in the map
      }
    }
  }
  nsyncs=Nsyncs;
  return map;
}

std::map<TString,int> TSequencerState::FindTriggerInSyncs(std::map<TString,int> syncchan, int &nsyncs) const //sync channels of the current driver
{
  std::map<TString,int> map;
  int Nsyncs=0;
  if (fTI.Channels.size())
  {
    std::map<TString,int>::iterator it;
    for (it = syncchan.begin(); it != syncchan.end(); it++)
    {
      if(fTI.Channels[it->second]==1) //verify if in the current state there is a DO that corresponds to a sync channel of the driver
      {
        Nsyncs++; //counting how many syncs we have in the current state
        map.insert({it->first,1}); //otherwise insert a new sync in the map
      }
    }
  }
  nsyncs=Nsyncs;
  return map;
}

TSequencerState::~TSequencerState()
{

}

//


