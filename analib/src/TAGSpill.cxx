#include "TAGSpill.h"

#if BUILD_AG
ClassImp(TAGSpill)

TAGSpill::TAGSpill()
{
   fSeqData    =NULL;
   fScalerData =NULL;
}
TAGSpill::TAGSpill(int runno, uint32_t unixtime): TSpill(runno, unixtime)
{
   fSeqData    =NULL;
   fScalerData =NULL;
}

TAGSpill::TAGSpill(int runno, uint32_t unixtime, const char* format, ...):
   TSpill(runno,unixtime)
{
   fSeqData    =NULL;
   fScalerData =NULL;
   va_list args;
   va_start(args,format);
   InitByName(format,args);
   va_end(args);
}

TAGSpill::TAGSpill(int runno, TDumpMarkerPair<TAGDetectorEvent,TChronoBoardCounter,CHRONO_N_BOARDS>* d):
   TSpill(runno, d->fStartDumpMarker->fMidasTime, d->fStartDumpMarker->fDescription.c_str())
{
   
   if (d->fStartDumpMarker && d->fStopDumpMarker) fIsDumpType=true;
   fScalerData = new TAGSpillScalerData(d);
   fSeqData = new TAGSpillSequencerData(d);
   //Print();
}

TAGSpill::TAGSpill(const TAGSpill& a):
   TSpill(a)
{
   if (a.fScalerData)
      fScalerData=new TAGSpillScalerData(*a.fScalerData);
   else
      fScalerData=NULL;
   if (a.fSeqData)
      fSeqData=new TAGSpillSequencerData(*a.fSeqData);
   else
      fSeqData=NULL;
}

TAGSpill::~TAGSpill()
{
   if (fScalerData)
      delete fScalerData;
   fScalerData=NULL;
   if (fSeqData)
      delete fSeqData;
   fSeqData=NULL;
}

#include "assert.h"


TAGSpill* TAGSpill::operator/( TAGSpill* b)
{
   //c=a/b
   TAGSpill* c=new TAGSpill(this->fRunNumber, this->fUnixtime);
   c->IsInfoType=true;
   assert(this->fRunNumber == b->fRunNumber);

   assert(this->fScalerData->ScalerFilled.size());
   assert(b->fScalerData->ScalerFilled.size());

   //assert(this->fScalerData->BVFilled);
   //assert(b->fScalerData->BVFilled);
   if (DumpHasMathSymbol())
      fName='('+fName+')';
   if (b->DumpHasMathSymbol())
   {
      b->fName='('+b->fName+')';
   }
   char dump_name[200];
   snprintf(dump_name,200,"%s / %s (%%)",this->fName.c_str(),b->fName.c_str());
   c->fName=dump_name;

   c->fScalerData =*fScalerData / b->fScalerData;

   c->fIsDumpType=false;
   return c;
}

TAGSpill* TAGSpill::operator+( TAGSpill* b)
{
   //c=a/b
   TAGSpill* c=new TAGSpill(this->fRunNumber, this->fUnixtime);
   c->IsInfoType=true;
   assert(this->fRunNumber == b->fRunNumber);

   assert(this->fScalerData->ScalerFilled.size());
   assert(b->fScalerData->ScalerFilled.size());

   //assert(this->fScalerData->BVFilled);
   //assert(b->fScalerData->BVFilled);

   if (DumpHasMathSymbol())
      fName='('+fName+')';

   if (b->DumpHasMathSymbol())
      b->fName='('+b->fName+')';

   char dump_name[200];
   snprintf(dump_name,200,"%s + %s",this->fName.c_str(),b->fName.c_str());

   c->fName=dump_name;

   c->fScalerData =*fScalerData + b->fScalerData;

   c->fIsDumpType=false;
   return c;
}

bool TAGSpill::Ready( bool have_BV)
{
   if (fIsDumpType)
   {
      return fScalerData->Ready(have_BV);
   }
   else
   {
      return true;
   }
}

void TAGSpill::Print()
{
   std::cout<<"Dump name:"<<fName<<"\t\tIsDumpType:"<<fIsDumpType<<std::endl;
   if (fSeqData)
      fSeqData->Print();
   if (fScalerData)
      fScalerData->Print();
   std::cout<<"Ready? "<< Ready(true) << " " << Ready(false)<<std::endl;
   std::cout<<std::endl;
}

TString TAGSpill::Content(std::vector<TChronoChannel> chrono_channels, const size_t SeqIndentation, const size_t DumpNameWidth, const size_t CountColumnWidth, bool withVertices, bool withPassCuts, bool withMVA)
{
   
   TString log;
   //if (indent){
    log += "   "; // indentation     
   //}
   std::string units="";
   {
      int open=fName.find_last_of('(');
      int close=fName.find_last_of(')');
      if (open>0 &&       //Have open bracket
         close > open &&  //Have close bracket
         close - open<6)  //Units less than 6 characters long
         units=(fName.substr (open+1,close-open-1));
   }
   if (fScalerData)
   {
      char buf[200];
      snprintf(buf,200,"[%8.3lf-%8.3lf]=%8.3lfs |",
                 fScalerData->fStartTime,
                 fScalerData->fStopTime,
                 fScalerData->fStopTime-fScalerData->fStartTime
                 ); // timestamps 
      log += buf;
   }
   else
   {
      if (fUnixtime)
      {
         char buf[80];
         struct tm * timeinfo = ( (tm*) &fUnixtime);
         snprintf(buf,80,"[%d:%d:%d]", timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
         log += buf;
      }
   }
   if (fSeqData)
   {
      std::string dump_name;
      for (int i = 0; i < fSeqData->fSequenceNum; i++)
         dump_name += std::string(SeqIndentation,' ');
      dump_name += fName; // dump description
      dump_name += "[" + std::to_string(fRepetition) + "]";
      if (dump_name.size() > DumpNameWidth)
         dump_name = dump_name.substr(0,DumpNameWidth);
      else if (dump_name.size() < DumpNameWidth)
         dump_name.insert(dump_name.size(), DumpNameWidth - dump_name.size(), ' ');
      log += dump_name + std::string("|");
   }
   else
   {
      log += std::string(" ") + fName;
   }
   if (fScalerData)
   {
      char buf[80];
      for (const TChronoChannel& c: chrono_channels)
      {
         int counts=-1;
         //If valid channel number:
         if (c.GetChannel() >= 0)
            counts = fScalerData->fDetectorCounts.at(c.GetBoardNumber() * CHRONO_N_CHANNELS + c.GetChannel());
         snprintf(buf,80,"%*d",(int)CountColumnWidth,counts);
         log += buf;
         if (units.size())
            log += units;
         else
            log += " ";
      }
      //ALPHA G is not fast enought for vertex data yet - Still the case?
      if (withVertices) {
         snprintf(buf,sizeof(buf),"%9d ",fScalerData->fVerticies);
         log += buf;
      }
      if (withPassCuts) {
         snprintf(buf,sizeof(buf),"%9d ",fScalerData->fPassCuts);
         log += buf;
      }
      if (withMVA) {
         snprintf(buf,sizeof(buf),"%9d ",fScalerData->fPassMVA);
         log += buf;
      }
      log += "";
   }
   return log;
}
#endif
