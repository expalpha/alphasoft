#include "TAGAnalysisReport.h"


#ifdef BUILD_AG

/// @brief Constructor
TAGAnalysisReport::TAGAnalysisReport()
{
    //ctor
    nStoreEvents = 0;
    nSigEvents = 0;
    nSigEvents = -1;
}

/// @brief Copy constructor
/// @param r 
TAGAnalysisReport::TAGAnalysisReport(const TAGAnalysisReport& r): TAnalysisReport(r)
{
    nStoreEvents = r.nStoreEvents;
    nSigEvents = r.nSigEvents;
    last_tpc_ts = r.nSigEvents;
}

/// @brief = operator
/// @param r 
/// @return 
TAGAnalysisReport TAGAnalysisReport::operator=(const TAGAnalysisReport& r)
{
    return TAGAnalysisReport(r);
}

/// @brief Constructor
/// @param runno 
TAGAnalysisReport::TAGAnalysisReport(int runno): TAnalysisReport(runno)
{
    nStoreEvents = 0;
    nSigEvents = 0;
    last_tpc_ts = -1;
}

/// @brief Deconstructor
TAGAnalysisReport::~TAGAnalysisReport()
{
    //dtor
}

/// @brief Normalise metrics tracked in AnalysisReportModule, store them in fDoubleValue map
/// @param sum_aw 
/// @param sum_pad 
/// @param sum_match 
/// @param sum_tracks 
/// @param sum_r_sigma 
/// @param sum_z_sigma 
/// @param sum_verts 
/// @param sum_hits 
/// @param sum_bars 
/// @param with_tracks
void TAGAnalysisReport::Flush(
   double sum_aw,       //Results from deconv module
   double sum_pad,      //Results from deconv module
   double sum_match,    //Results from match module
   double sum_tracks,   //Results from reco module
   double sum_r_sigma,  //Results from reco module
   double sum_z_sigma,  //Results from reco module
   double sum_verts,    //Results from reco module
   double sum_vertcandidate_tracks,
   double sum_vertused_tracks,
   double sum_vert_chi,
   double sum_hits,     //Results from reco module
   double sum_bars,
   int with_tracks,
   bool is_sim)
{
    if (nSigEvents>0)
    {
       fDoubleValue["TPC_Mean_AW"] = sum_aw/(double)nSigEvents;
       fDoubleValue["TPC_Mean_Pad"] = sum_pad/(double)nSigEvents;
       fDoubleValue["TPC_Mean_Match"] = sum_match/(double)nSigEvents;
    }
    if (nStoreEvents>0)
    {
       fDoubleValue["TPC_Mean_Hits"] = sum_hits/(double)nStoreEvents;
       fDoubleValue["TPC_Mean_Tracks"] = sum_tracks /(double)nStoreEvents;
       fDoubleValue["TPC_Mean_R_Sigma"] = sum_r_sigma/with_tracks;
       fDoubleValue["TPC_Mean_Z_Sigma"] = sum_z_sigma/with_tracks;
       fDoubleValue["TPC_Mean_Verts"] = sum_verts/(double)nStoreEvents;
       fDoubleValue["TPC_Mean_VertCT"] = sum_vertcandidate_tracks / (double)sum_verts;
       fDoubleValue["TPC_Mean_VertGT"] = sum_vertused_tracks / (double)sum_verts;
       fDoubleValue["TPC_Mean_Vert_Chi"] = sum_vert_chi / (double)sum_verts;
       fDoubleValue["Barrel_Mean_Bars"] = sum_bars/(double)nStoreEvents;
    }
    if (is_sim) isSim = true;
    return;
}

/// @brief Print trigger tables in table
void TAGAnalysisReport::PrintAllTriggerSettings() const
{
    for(const std::string& s: fTriggerSettingChannels)
       std::cout << s << ":\t" << FindBool(s) << "\n";
}

/// @brief Print activated triggers in CSV format
/// @return 
std::string TAGAnalysisReport::ActiveTriggers() const
{
    std::string line = "";
    for(const std::string&s : fTriggerSettingChannels)
    {
        if (FindBool(s))
        {
            if (line.size())
                line +=", ";
            line += s;
        }
    }
    return line;
}

/// @brief Print analysis report table
void TAGAnalysisReport::Print()
{
    PrintHeader();
    if(nStoreEvents>0)
    {
       std::cout <<"Active Triggers: " << ActiveTriggers() <<std::endl;
       std::cout <<"Is Simulation? " << (isSim ? "Yes" : "No") <<std::endl;
       std::cout << "Mean #AW:   \t"   << fDoubleValue["TPC_Mean_AW"]    << std::endl;
       std::cout << "Mean #PAD:  \t"  << fDoubleValue["TPC_Mean_Pad"]   << std::endl;
       std::cout << "Mean #MATCH:\t"<< fDoubleValue["TPC_Mean_Match"] << std::endl;
       std::cout << "Mean #Hits: \t"    << fDoubleValue["TPC_Mean_Hits"] << std::endl;
       std::cout << "Mean #Tracks:\t"   << fDoubleValue["TPC_Mean_Tracks"] << 
                        "\t(Mean ChiR:" << fDoubleValue["TPC_Mean_R_Sigma"] << 
                        " ChiZ:" << fDoubleValue["TPC_Mean_Z_Sigma"] << ")" << std::endl;
       std::cout << "Mean #Verts:\t"    << fDoubleValue["TPC_Mean_Verts"] <<
                         "\t(nCT:" << fDoubleValue["TPC_Mean_VertCT"] <<
                         " nGT:" << fDoubleValue["TPC_Mean_VertGT"] <<
                         " chi2:" << fDoubleValue["TPC_Mean_Vert_Chi"] << ")" << std::endl;
       std::cout << "Mean #Bars:\t"     << fDoubleValue["Barrel_Mean_Bars"] << std::endl;
       std::cout<<std::endl;
       
        std::cout << "Time of Last TPC Event: " << last_tpc_ts << " s" << std::endl;
    }
    PrintFooter();
}

/// @brief Print analysis report CSV column titles 
/// @return 
std::string TAGAnalysisReport::CSVTitleLine() const
{
   std::string line = TAnalysisReport::HeaderTitleLine();

   for(const std::string& s: fTriggerSettingChannels)
      line += s + ",";
   line += "Mean #AW, ";
   line += "Mean #PAD, ";
   line += "Mean #MATCH, ";
   line += "Mean #Hits, ";
   line += "Mean #Tracks, Mean Track ChiR, Mean Track ChiZ, ";
   line += "Mean #Verts,";
   line += "Mean #Canditate Tracks per Vert,";
   line += "Mean #Used Tracks Per Vertex,";
   line += "Mean Vertex Chi,";
   line += "Mean #Bars, "; 
   line += TAnalysisReport::FooterTitleLine() + "\n";
   return line;
}

/// @brief Print analysis report data as CSV line
/// @return 
std::string TAGAnalysisReport::CSVLine() const
{
   std::string line = TAnalysisReport::HeaderCSVLine();

    for(const std::string& s: fTriggerSettingChannels)
       line += std::to_string(FindBool(s)) + ",";

   line += std::to_string(FindDouble("TPC_Mean_AW")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_Pad")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_Match")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_Hits")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_Tracks")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_Z_Sigma")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_R_Sigma")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_Verts")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_VertCT")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_VertGT")) + ",";
   line += std::to_string(FindDouble("TPC_Mean_Vert_Chi")) + ",";
   line += std::to_string(FindDouble("Barrel_Mean_Bars")) + ",";

   line += TAnalysisReport::FooterCSVLine() + "\n";
   return line;
}

#endif
