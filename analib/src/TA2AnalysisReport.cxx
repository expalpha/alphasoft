
#include "TA2AnalysisReport.h"
#ifdef HAVE_MIDAS
#include "midas.h"
#endif

#ifdef BUILD_A2

// We dont want these as members of the class...
static TH1D *SVD_N_RawHits;
static TH1D *SVD_P_RawHits;
static TH1D *SVD_RawHits;
static TH1D *SVD_Hits;
static TH1D *SVD_Tracks;
static TH1D *SVD_Verts;
static TH1D *SVD_Pass;

/// @brief Constructor
TA2AnalysisReport::TA2AnalysisReport()
{
   // ctor
   fHybridNSideOccupancy = new TH1I("HybridNSideOccupancy", "HybridNSideOccupancy; HybridNumber; Count", 72, 0, 72);
   fHybridPSideOccupancy = new TH1I("HybridPSideOccupancy", "HybridPSideOccupancy; HybridNumber; Count", 72, 0, 72);
}

/// @brief Copy constructor
/// @param r
TA2AnalysisReport::TA2AnalysisReport(const TA2AnalysisReport &r) : TAnalysisReport(r)
{
   fNSVDEvents        = r.fNSVDEvents;
   fLastVF48TimeStamp = r.fLastVF48TimeStamp;

   fHybridNSideOccupancy = new TH1I(*r.fHybridNSideOccupancy);
   fHybridPSideOccupancy = new TH1I(*r.fHybridPSideOccupancy);

   fSVD_Verts_Sum   = r.fSVD_Verts_Sum;
   fSVD_PassCut_Sum = r.fSVD_PassCut_Sum;
}

/// @brief = operator
/// @param r
/// @return
TA2AnalysisReport TA2AnalysisReport::operator=(const TA2AnalysisReport &r)
{
   return TA2AnalysisReport(r);
}

/// @brief Constructor
/// @param runno
TA2AnalysisReport::TA2AnalysisReport(int runno) : TAnalysisReport(runno)
{

   fHybridNSideOccupancy = new TH1I("HybridNSideOccupancy", "HybridNSideOccupancy; HybridNumber; Count", 72, 0, 72);
   fHybridPSideOccupancy = new TH1I("HybridPSideOccupancy", "HybridPSideOccupancy; HybridNumber; Count", 72, 0, 72);

   fNSVDEvents        = 0;
   fLastVF48TimeStamp = -1;
   fSVD_Verts_Sum     = 0;
   fSVD_PassCut_Sum   = 0;

   SVD_N_RawHits = new TH1D("SVD_N_RawHits", "SVD_N_RawHits; Multiplicity; Count", 1000, 0, 1000);
   SVD_P_RawHits = new TH1D("SVD_P_RawHits", "SVD_P_RawHits; Multiplicity; Count", 1000, 0, 1000);
   // MeanMode SVD_N_Clusters = new TH1D(1000};
   // MeanMode SVD_P_Clusters = new TH1D(1000};
   SVD_RawHits = new TH1D("SVD_RawHits", "SVD_RawHits; Multiplicity; Count", 1000, 0, 1000);
   SVD_Hits    = new TH1D("SVD_Hits", "SVD_Hits;Multiplicity; Count ", 1000, 0, 1000);
   SVD_Tracks  = new TH1D("SVD_Tracks", "SVD_Tracks; Multiplicity; Count", 100, 0, 100);
   SVD_Verts   = new TH1D("SVD_Verts", "SVD_Verts; Multiplicity; Count", 2, 0, 2);
   SVD_Pass    = new TH1D("SVD_Pass", "SVD_Pass; Multiplicity; Count", 2, 0, 2);
}

/// @brief Deconstructor
TA2AnalysisReport::~TA2AnalysisReport()
{
   // dtor
   delete SVD_N_RawHits;
   delete SVD_P_RawHits;
   delete SVD_RawHits;
   delete SVD_Hits;
   delete SVD_Tracks;
   delete SVD_Verts;
   delete SVD_Pass;
   delete fHybridNSideOccupancy;
   delete fHybridPSideOccupancy;
}

/// @brief Add SVD data to internal (temporary) histograms
/// @param nraw
/// @param praw
/// @param raw_hits
/// @param hits
/// @param tracks
/// @param verts
/// @param pass
/// @param time
void TA2AnalysisReport::FillSVD(const Int_t &nraw, const Int_t &praw, const Int_t &raw_hits, const Int_t &hits,
                                const Int_t &tracks, const Int_t &verts, int pass, double time)
{
   SVD_N_RawHits->Fill(nraw);
   SVD_P_RawHits->Fill(praw);
   // SVD_N_Clusters->Fill(se->GetNNClusters());
   // SVD_P_Clusters->Fill(se->GetNPClusters());
   SVD_RawHits->Fill(raw_hits);
   SVD_Hits->Fill(hits);
   SVD_Tracks->Fill(tracks);
   SVD_Verts->Fill(verts);
   fSVD_Verts_Sum += verts;

   SVD_Pass->Fill(pass);
   fSVD_PassCut_Sum += pass;

   fLastVF48TimeStamp = time;
   fNSVDEvents++;
   return;
}

/// @brief Fill N side occupancy
/// @param module
void TA2AnalysisReport::FillHybridNSideOccupancy(const int module)
{
   fHybridNSideOccupancy->Fill(module);
}

/// @brief Fill P side occupancy
/// @param module
void TA2AnalysisReport::FillHybridPSideOccupancy(const int module)
{
   fHybridPSideOccupancy->Fill(module);
}

/// @brief Fill the fIntValue / fDoubleValue maps with analysis metrics from temprary histograms
void TA2AnalysisReport::Flush()
{
   fIntValue["SVD_N_RawHits_Mode"]    = SVD_N_RawHits->GetMaximumBin() - 1;
   fDoubleValue["SVD_N_RawHits_Mean"] = SVD_N_RawHits->GetMean();
   fIntValue["SVD_P_RawHits_Mode"]    = SVD_P_RawHits->GetMaximumBin() - 1;
   fDoubleValue["SVD_P_RawHits_Mean"] = SVD_P_RawHits->GetMean();
   fIntValue["SVD_Hits_Mode"]         = SVD_Hits->GetMaximumBin() - 1;
   fDoubleValue["SVD_Hits_Mean"]      = SVD_Hits->GetMean();
   fIntValue["SVD_Tracks_Mode"]       = SVD_Tracks->GetMaximumBin() - 1;
   fDoubleValue["SVD_Tracks_Mean"]    = SVD_Tracks->GetMean();
   fDoubleValue["SVD_Verts_Mean"]     = SVD_Verts->GetMean();
   fIntValue["SVD_Verts_Sum"]         = fSVD_Verts_Sum;
   // We used to calculate the rates from the ODB run start time and stop time
   // this allow the rates to be calculated even when the SIS wasn't producing
   // timestamps. Its overkill and its obvious when the SIS isn't working properly
   // (the spill log breaks)
   fDoubleValue["SVD_Vert_Rate"]   = fSVD_Verts_Sum / fLastVF48TimeStamp;
   fDoubleValue["SVD_PassCut_Mean"]   = SVD_Pass->GetMean();
   fDoubleValue["SVD_PassCut_Sum"]    = fSVD_PassCut_Sum;
   fDoubleValue["SVD_PassCut_Rate"]   = fSVD_PassCut_Sum / fLastVF48TimeStamp;
}


int TA2AnalysisReport::QODChecks()
{
   int error_cout = 0;
   if (fNSVDEvents == 0)
   {
      std::cout << "No SVD events found. Assuming SVD is off" << std::endl;
      return -1;
   }

   // Check if there were no hits
   if (fIntValue["SVD_Hits_Mode"] == 0)
   {
      std::cerr << "No hits found. Check for noisy SVD trigger." << std::endl;
#if HAVE_MIDAS
      cm_msg(MTALK, "alpha2online", "No hits found. Check for noisy SVD trigger.");
#endif
   }
      
   // Check if there were hits but no tracks
   if (fIntValue["SVD_Tracks_Mode"] == 0 && fIntValue["SVD_Hits_mode"] > 0) {
      std::cerr << "No tracks found, but hits present. Check SVD tracking." << std::endl;
#if HAVE_MIDAS
      cm_msg(MTALK, "alpha2online","No tracks found, but hits present. Check SVD tracking.");
#endif
      error_cout++;
   }

   // Check if vertex rate is too low (require decent statistics, ie > 60 seconds)
   if (fLastVF48TimeStamp > 60) {
      // Typical background rate is 45mHz
      if (fDoubleValue["SVD_Vert_Rate"] < 0.020) {
         std::cerr << "Vertex rate is too low. Check SVD reconstruction." << std::endl;
#if HAVE_MIDAS
         cm_msg(MTALK, "alpha2online","Vertex rate is too low. Check SVD trigger / reconstruction.");
#endif
         error_cout++;
      }

      // Check if pass cut rate is too low (require decent statistics, ie > 60 seconds)
      if (fDoubleValue["SVD_PassCut_Rate"] < 0.020) {
         std::cerr << "Pass cut rate is too low. Check SVD trigger / reconstruction." << std::endl;
#if HAVE_MIDAS
         cm_msg(MTALK, "alpha2online","Vertex rate is too low. Check SVD trigger / reconstruction.");
#endif
         error_cout++;
      }
   }
   return error_cout;
}

/// @brief Print analysis report in table
void TA2AnalysisReport::Print()
{
   PrintHeader();
   if (fNSVDEvents > 0) {
      std::cout << "Number of SVD Events:\t" << fNSVDEvents << std::endl;
      std::cout << "               \tMode\tMean" << std::endl;
      std::cout << "SVD #RawNHits: \t" << fIntValue["SVD_N_RawHits_Mode"] << "\t" << fDoubleValue["SVD_N_RawHits_Mean"]
                << std::endl;
      std::cout << "SVD #RawPHits: \t" << fIntValue["SVD_P_RawHits_Mode"] << "\t" << fDoubleValue["SVD_P_RawHits_Mean"]
                << std::endl;
      // std::cout <<"Mean SVD #RawHits: \t" <<SVD_RawHits->GetMode()  <<"\t"<<SVD_RawHits->GetMean()  <<std::endl;
      std::cout << "SVD #Hits: \t" << fIntValue["SVD_Hits_Mode"] << "\t" << fDoubleValue["SVD_Hits_Mean"] << std::endl;
      std::cout << "SVD #Tracks:\t" << fIntValue["SVD_Tracks_Mode"] << "\t" << fDoubleValue["SVD_Tracks_Mean"]
                << std::endl;
      std::cout << "----------------Sum-----Mean---------" << std::endl;
      // std::cout<<"SVD Events:\t"<< SVD_Verts
      std::cout << "SVD #Events:\t" << fNSVDEvents << std::endl;
      std::cout << "SVD #Verts:\t" << fSVD_Verts_Sum << "\t" << fDoubleValue["SVD_Verts_Mean"];

      if (fDoubleValue["SVD_Vert_Rate"] < 0.1)
         printf("\t~(%.1fmHz)", fDoubleValue["SVD_Vert_Rate"] * 1000.);
      else
         printf("\t~(%.1fHz)", fDoubleValue["SVD_Vert_Rate"]);
      std::cout << std::endl;
      std::cout << "SVD #Pass cuts:\t" << fSVD_PassCut_Sum << "\t" << fDoubleValue["SVD_PassCut_Mean"];
      
      if (fDoubleValue["SVD_PassCut_Rate"] < 0.1)
         printf("\t~(%.1fmHz)", fDoubleValue["SVD_PassCut_Rate"] * 1000.);
      else
         printf("\t~(%.1fHz)", fDoubleValue["SVD_PassCut_Rate"]);
      std::cout << std::endl;
      std::cout << "Time of Last VF48 Event: " << fLastVF48TimeStamp << " s" << std::endl;
   }
   PrintFooter();
}

/// @brief Return CSV format column headers (from CSVLine)
/// @return
std::string TA2AnalysisReport::CSVTitleLine() const
{
   std::string line = TAnalysisReport::HeaderTitleLine();
   line += " Raw N Hits (Mode), Raw N Hits (Mean), Raw P Hits (Mode), Raw P Hits (Mean), Hits (Mode), Hits (Mean), "
           "Tracks (Mode), Tracks (Mean), nSVDEvents, n Verts (Sum), n Verts (Mean), Vert Rate (Hz), Pass Cuts (Sum), "
           "Pass Cuts (Mean), Pass Cut Rate (Hz),Last VF48 Event Time,";
   line += TAnalysisReport::FooterTitleLine() + "\n";
   return line;
}

/// @brief Return the data contained in TA2Analysis report as CSV line
/// @return
std::string TA2AnalysisReport::CSVLine() const
{
   std::string line = TAnalysisReport::HeaderCSVLine();
   line += std::to_string(FindInt("SVD_N_RawHits_Mode")) + ",";
   line += std::to_string(FindDouble("SVD_N_RawHits_Mean")) + ",";
   line += std::to_string(FindInt("SVD_P_RawHits_Mode")) + ",";
   line += std::to_string(FindDouble("SVD_P_RawHits_Mean")) + ",";
   line += std::to_string(FindInt("SVD_Hits_Mode")) + ",";
   line += std::to_string(FindDouble("SVD_Hits_Mean")) + ",";
   line += std::to_string(FindInt("SVD_Tracks_Mode")) + ",";
   line += std::to_string(FindDouble("SVD_Tracks_Mean")) + ",";
   line += std::to_string(fNSVDEvents) + ",";
   line += std::to_string(fSVD_Verts_Sum) + ",";
   line += std::to_string(FindDouble("SVD_Verts_Mean")) + ",";
   line += std::to_string(FindDouble("SVD_Vert_Rate")) + ",";
   line += std::to_string(fSVD_PassCut_Sum) + ",";
   line += std::to_string(FindDouble("SVD_PassCut_Mean")) + ",";
   line += std::to_string(FindDouble("SVD_PassCut_Rate")) + ",";
   line += std::to_string(fLastVF48TimeStamp) + ",";
   line += TAnalysisReport::FooterCSVLine() + "\n";
   return line;
}
#endif
