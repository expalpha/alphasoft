#include "TSpill.h"


ClassImp(TSpill)
TSpill::TSpill(): fRunNumber(-1)
{
   fIsDumpType =true; //By default, expect this to be a dump
   IsInfoType =false;
   fFuzzyStart =false;
   fFuzzyStop =false;
   fUnixtime   =0;
}

std::map<std::string,int> TSpill::fDumpCounter = {};

// The first string may contain wildcard characters : * or ?
//https://www.geeksforgeeks.org/wildcard-character-matching/
bool TSpill::MatchWithWildCards(const char *first, const char * second) const
{ 
    // If we reach at the end of both strings, we are done 
    if (*first == '\0' && *second == '\0') 
        return true; 
  
    // Make sure that the characters after '*' are present 
    // in second string. This function assumes that the first 
    // string will not contain two consecutive '*' 
    if (*first == '*' && *(first+1) != '\0' && *second == '\0') 
        return false; 
  
    // If the first string contains '?', or current characters 
    // of both strings match 
    if (*first == '?' || *first == *second) 
        return MatchWithWildCards(first+1, second+1); 
  
    // If there is *, then there are two possibilities 
    // a) We consider current character of second string 
    // b) We ignore current character of second string. 
    if (*first == '*') 
        return MatchWithWildCards(first+1, second) || MatchWithWildCards(first, second+1); 
    return false; 
} 

//Always an exact match unless wild cards are used
bool TSpill::IsMatchForDumpName(const std::string& dumpname) const
{
   std::string QuoteMarkFreeName;
   if (fName[0]=='"' && fName[fName.size()-1]=='"')
      QuoteMarkFreeName=fName.substr(1,fName.size()-2);
   else
      QuoteMarkFreeName=fName;
   return MatchWithWildCards(dumpname.c_str(),QuoteMarkFreeName.c_str());
}

TSpill::TSpill(int runno, uint32_t unixtime): fRunNumber(runno)
{
   fIsDumpType =true; //By default, expect this to be a dump
   IsInfoType =false;
   fUnixtime   =unixtime;
}
TSpill::TSpill(int runno, uint32_t unixtime, const char* format, ...): fRunNumber(runno)
{
   fUnixtime = unixtime;
   va_list args;
   va_start(args,format);
   InitByName(format,args);
   va_end(args);
}
void TSpill::InitByName(const char* format, va_list args)
{
   char buf[256];
   vsnprintf(buf,255,format,args);
   fName       =buf;
   fRepetition = fDumpCounter[fName]++;
   fIsDumpType =false; //By default, expect this to be a information if given a string at construction
   IsInfoType =false;
   std::cout<<"NewSpill:";
   Print();
}

TSpill::TSpill(const TSpill& a) : TObject(a)
{
   fRunNumber    =a.fRunNumber;
   fName         =a.fName;
   fIsDumpType   =a.fIsDumpType;
   IsInfoType   =a.IsInfoType;
   fUnixtime     =a.fUnixtime;
}

TSpill& TSpill::operator=(const TSpill& rhs)
{
   if (this == &rhs)
      return *this;
   fRunNumber    =rhs.fRunNumber;
   fName         =rhs.fName;
   fIsDumpType   =rhs.fIsDumpType;
   IsInfoType   =rhs.IsInfoType;
   fUnixtime     =rhs.fUnixtime;
   return *this;
}

void TSpill::Print()
{
   std::cout<<"RunNumber:"<<fRunNumber<<"\t"
            <<"Name:"<<fName<<"\t"
            <<std::endl;
}

std::string TSpill::toJson() const
{
   std::string json = "{";
   json += "\"RunNumber\":" + std::to_string(fRunNumber) + ",";
   json += "\"UnixTime\":" + std::to_string(fUnixtime) + ",";
   json += "\"Name\":\"" + fName + "\"";
   json += "}";
   return json;
}

int TSpill::AddToDatabase(sqlite3 *db, sqlite3_stmt * stmt)
{
   assert(!"Implement me!");
   printf("AddToDatabase not implemented!!! %p %p",(void*)db, (void*)stmt);
   return -1;
}

bool TSpill::DumpHasMathSymbol() const
{
   char symbols[5]="+/*-";
   for (const char c: this->fName)
   {
      for (int i=0; i<4; i++)
      {
         //std::cout<<c<<"=="<<symbols[i]<<std::endl;
         if (c==symbols[i])
            return true;
      }
   }
   return false;
}

TSpill::~TSpill()
{

}


std::string TSpill::ContentCSVTitle() const
{
   std::string title = "RunNumber, Sequencer Start Time (Unix Time), Dump Name, Is Dump Type, Is Info Type,";
   return title;
}
std::string TSpill::ContentCSV() const
{
   std::string line = std::to_string(fRunNumber) + "," +
                      std::to_string(fUnixtime) + "," +
                      fName + ",";
                      if (fIsDumpType)
                         line +="1,";
                      else
                         line +="0,";
                      if (IsInfoType)
                         line +="1,";
                      else
                         line +="0,";
   return line;
}