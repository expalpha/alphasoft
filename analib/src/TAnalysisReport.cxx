#include "TAnalysisReport.h"
#include "GitInfo.h"

/// @brief Constructor
TAnalysisReport::TAnalysisReport()
   : fRunNumber(-1), fCompilationDate(COMPILATION_DATE), GitBranch(GIT_BRANCH), fGitDate(GIT_DATE),
     fGitHash(GIT_REVISION), fGitHashLong(GIT_REVISION_FULL), fGitDiff(GIT_DIFF_SHORT_STAT)
{
   // ctor
}

/// @brief Copy constructor
/// @param r
TAnalysisReport::TAnalysisReport(const TAnalysisReport &r)
   : TObject(r), fRunNumber(r.fRunNumber), fCompilationDate(r.fCompilationDate), GitBranch(r.GitBranch),
     fGitDate(r.fGitDate), fGitHash(r.fGitHash), fGitHashLong(r.fGitHashLong), fGitDiff(r.fGitDiff),
     fBoolValue(r.fBoolValue), fIntValue(r.fIntValue), fDoubleValue(r.fDoubleValue), fStringValue(r.fStringValue)
{
   fProgramName     = r.fProgramName;
   fProgramPath     = r.fProgramPath;
   fProgramPathFull = r.fProgramPathFull;

   fStartRunUnixTime = r.fStartRunUnixTime;
   fStopRunUnixTime  = r.fStopRunUnixTime;
   fAnalysisHost     = r.fAnalysisHost;
}

/// @brief = operator
/// @param r
/// @return
TAnalysisReport TAnalysisReport::operator=(const TAnalysisReport &r)
{
   return TAnalysisReport(r);
}

/// @brief Constructor
/// @param runno
TAnalysisReport::TAnalysisReport(int runno)
   : fRunNumber(runno), fCompilationDate(COMPILATION_DATE), GitBranch(GIT_BRANCH), fGitDate(GIT_DATE),
     fGitHash(GIT_REVISION), fGitHashLong(GIT_REVISION_FULL), fGitDiff(GIT_DIFF_SHORT_STAT)
{
   fStartRunUnixTime  = 0;
   fStopRunUnixTime   = 0;
   char   result[200] = {0};
   size_t result_len  = readlink("/proc/self/exe", result, 200);
   if (result_len) {
      fProgramPathFull  = result;
      std::size_t found = fProgramPathFull.find_last_of("/\\");
      // std::cout << " path: " << binary_path_full.substr(0,found).c_str() << '\n';
      // std::cout << " file: " << binary_path_full.substr(found+1).c_str() << '\n';
      fProgramPath = fProgramPathFull.substr(0, found);
      fProgramName = fProgramPathFull.substr(found + 1);
   } else {
      fProgramPath = "readlink of /proc/self/exe failed";
      fProgramName = "readlink of /proc/self/exe failed";
   }

   if (getenv("HOSTNAME") != nullptr) {
      fAnalysisHost = getenv("HOSTNAME");
   } else {
      fAnalysisHost = "UNKNOWN";
   }
}

/// @brief Deconstructor
TAnalysisReport::~TAnalysisReport()
{
   // dtor
}

/// @brief Get the unix time of the run start as human readable string
/// @param time 
///
/// The value is collected from the ODB in the BeginRun stage of analysis, will return 0 if the first subrun wasnt processed
std::string TAnalysisReport::GetRunStartTimeString() const
{
   time_t      t = fStartRunUnixTime;
   std::string date(asctime(localtime(&t)));
   date.erase(std::remove(date.begin(), date.end(), '\n'), date.end());
   return date;
}

/// @brief Get the unix time of the run stop as human readable string
/// @param time 
///
/// The value is collected from the ODB in the EndRun stage of analysis, will return 0 if the last subrun wasnt to the end
std::string TAnalysisReport::GetRunStopTimeString() const
{
   if (fStopRunUnixTime == 0) return std::string("UNKNOWN\t(end-of-run ODB entry not processed)");
   time_t      t = fStopRunUnixTime;
   std::string date(asctime(localtime(&t)));
   date.erase(std::remove(date.begin(), date.end(), '\n'), date.end());
   return date;
}

/// @brief Format the unixtime build time into human readable string
/// @return
///
/// In the format YY-mm-dd seconds?
std::string TAnalysisReport::GetCompilationDateString() const
{
   time_t     t  = fCompilationDate;
   struct tm *tm = localtime(&t);
   char       comp_date[20];
   strftime(comp_date, sizeof(comp_date), "%Y-%m-%d\t%X", tm);
   std::string date = comp_date;
   date.erase(std::remove(date.begin(), date.end(), '\n'), date.end());
   return date;
}

/// @brief Format the unixtime of the of the last git commit into a human readable string
/// @return
///
/// In the format YY-mm-dd seconds?
std::string TAnalysisReport::GetGitDateString() const
{
   // Git revision date:
   time_t     t  = fGitDate;
   struct tm *tm = localtime(&t);
   char       date[20];
   strftime(date, sizeof(date), "%Y-%m-%d\t%X", tm);
   return std::string(date);
}

/// @brief Get a short summary of the number of changes in the repository calculated at compile time
/// @return 
std::string TAnalysisReport::GetGitDiff() const
{
   std::string clean(fGitDiff);
   std::replace(clean.begin(), clean.end(), ',', ';');
   return clean;
}

/// @brief Return git version information in a human readable string
/// @return 
std::string TAnalysisReport::GetVersionLine() const
{
   std::string VersionLine = "";
   if (getenv("AGRELEASE")) VersionLine += std::string("Path: ") + getenv("AGRELEASE") + std::string("\t");
   VersionLine += "Program: " + this->GetProgramName() + std::string("\tGit version: ") + this->GetGitHash() +
                  std::string(" Build date: ") + this->GetCompilationDateString();
   if (this->GetGitDiff().size()) VersionLine += std::string(" with ") + this->GetGitDiff();
   return VersionLine;
}

/// @brief Print header of end of run analysis report for its child classes
void TAnalysisReport::PrintHeader()
{
   printf("===========================================================\n");
   printf("%s Report for run %d\n", fProgramName.c_str(), fRunNumber);
   printf("===========================================================\n");
   // This string reports warning if the start time is not set
   std::cout << "Start Run: " << GetRunStartTimeString() << "\n";
   // This string reports warning if the start time is not set
   std::cout << "Stop Run: " <<  GetRunStopTimeString() << "\n";
   if (fStopRunUnixTime != 0 && fStartRunUnixTime != 0) {
      std::cout << "Duration: ~" << fStopRunUnixTime - fStartRunUnixTime << "s"
                << "\n";
   } else {
      std::cout << std::endl;
   }
   return;
}

/// @brief Return CSV title for HeaderCSVLine
/// @return
std::string TAnalysisReport::HeaderTitleLine() const
{
   return "runNumber, ProgramName, Run Start (Unixtime), Run stop (Unixtime), Duration (s), Run Start, Run Stop,";
}

/// @brief Return parent class data as std string in csv format
/// @return
std::string TAnalysisReport::HeaderCSVLine() const
{
   std::string line = "";
   line += std::to_string(fRunNumber) + ",";
   line += fProgramName + ",";
   line += std::to_string(fStartRunUnixTime) + ",";
   line += std::to_string(fStopRunUnixTime) + ",";
   line += std::to_string(fStopRunUnixTime - fStartRunUnixTime) + ",";
   line += GetRunStartTimeString() + ",";
   line += GetRunStopTimeString() + ",";
   return line;
}

/// @brief Print footer for end of run analysis report for its child classes
void TAnalysisReport::PrintFooter()
{
   printf("Compilation date:%s\n", GetCompilationDateString().c_str());
   std::cout << "Analysis run on host: " << GetAnalysisHost().c_str() << std::endl;
   printf("Git branch:      %s\n", GetGitBranch().c_str());
   printf("Git date:         %s\n", GetGitDateString().c_str());
   printf("Git hash:        %s\n", GetGitHash().c_str());
   printf("Git hash (long): %s\n", GetGitHashLong().c_str());
   printf("Git diff (shortstat):%s\n", GetGitDiff().c_str());
   printf("===========================================================\n");
}

/// @brief Get CSV formatted headers for FooterCSVLine
/// @return 
std::string TAnalysisReport::FooterTitleLine() const
{
   return "Compilation Date, Analysis Host, Git Branch, Git date, Git hash, Git Hash (long), Git diff (shortstat),";
}

/// @brief Return string of footer information in csv format
/// @return 
std::string TAnalysisReport::FooterCSVLine() const
{
   std::string line = "";
   line += GetCompilationDateString() + ",";
   line += GetAnalysisHost() + ",";
   line += GetGitBranch() + ",";
   line += GetGitDateString() + ",";
   line += GetGitHash() + ",";
   line += GetGitHashLong() + ",";
   line += GetGitDiff() + ",";
   return line;
}
