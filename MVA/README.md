### AG MVA Usage ###

There are essentially 3 steps to running and applying the MVA in AG.

1. Run AGMVADump.sh to generate training data for your module.
2. Run AGTMVAClassification.C to retrain or alter your model (AGMVADump.sh will train a default model as a last step)
3. Run jagana/agana with --usemva flag to apply the model to your events.

We go in to more detail of each step below.


## AGMVADump.sh ##

```
cd alphasoft
source ./AGMMVADump.sh folder_name condor
```
The input folder_name defines the name of the folder within cd alphasoft/MVA and scripts/condor_jobs/DAGs to save all the resulting root files, and output logs.
The input condor is an option (0 or 1) of whether to use condor. It will by default create a DAG.


Within ./AGMVADump.sh the following things will run:

* jagana.exe will run to get all the spills and events for the run (!!!step depreciated!!!)
* macro_main_ag_eventlist_generator.exe will run. This takes a flag (cosmic/mixing) and will save the event IDs of either every event (cosmic) or every mixing event (mixing). This generates an eventlist0${run}.list file. (!!!step depreciated!!!)
* The above two steps will no longer run, instead a global eventlist "eventlistfull.list" exists in the repo in MVA/eventlists. Should you need to update this eventlist the above two steps describe how.
* agdumper.exe will run the analysis on each run chosen only on the events with IDs found in your chosen eventlist and save the variables defined (in TAGMVADumper.h/.cxx) to dumperoutput${run}.root
* macro_main_merger.exe takes all the dumperoutput${run}.root as well as the cosmic/mixing flag and dump to training, test, and validation .root datasets
* To chose your runs update the files "cosmicList" and "mixingList" also found in MVA/eventlists and include your runs in a list with each line containing a new run number. The cosmicList containing cosmic runs, and mixingList containing mixing runs. 
* The loop will run each mixing run, then the merger, then each cosmic run, then the cosmic merger.
* If running locally you will need all of your run files to be in the folder alphasoft/runs/ but if running on lxplus it will use EOS. There is no option for running on any of the DAQ machines because this is not recommended. 
* Once this is complete inside alphasoft/MVA/folder_name you will have 2 files: mvaSignal.root and mvaBackground.root. These are to be used in the training.
* If running with condor option *on* you will get the same files found in the same location in MVA/folder_name. But you will also get a folder in scripts/condor_job/DAGs/MVADump_folder_name containing all outputs from the DAG job. Assuming everything runs okay you don't need to worry about this but it's useful for debugging.

# STOP - Have you read $AGRELEASE/MVA/README.md? #
# ./MVA/${FOLDERNAME} has just been populated, change now the eventlist, cosmicList, mixingList, or AGTMVAClassification.C if required. #

If you see this warning and have not edited the required files it is not too late. At this stage just edit the following files (if required):

1. 2. MVA/folder_name/mixingList and MVA/folder_name/cosmicList
    * The dumper requires the files mixingList and cosmicList to exist in $AGRELEASE/MVA/folder_name/ directory to run.
    * They have no extension and are simply a list of runs you would like to use for signal and cosmic data.
    * Add a new run to each line and don't add anything else

3. eventlistfull.list
    * Unlikely you will want to change this by hand unless debugging. But if you have a seperate list to the default that you've generated and want to use it, copy paste it's contents here now. NOTE: It must have the same name as before.

4. AGTMVAClassification.C
    * This contains the best model we have found so far, but you might want to change this. If so change the model parameters in here now.

You may now continue by hitting 'Y'.

# STOP -  /scripts/condor_job/DAGs/MVADump_testish has just been populated, change now the 3 '.sub' files or the 3 '.sh' files found there if required. #

If you see this warning and have not edited the required files it is not too late. They are:

1. 2. 3. scripts/condor_job/DAGs/MVADump_folder_name/mva_dump.sub;mva_merge.sub;run_training.sub
    i) Change the request_cpus to your chosen value (default is 2) for all 3 submit files.
    ii) Change request_memory to chosen value (default is 2048MB) for all 3 submit files.
    iii) JobFlavour for all 3 submit files (default is testmatch: more info about JobFlavours is in the separate condor_job README.md)

6. scripts/condor_job/DAGs/MVADump_folder_name/run_mva_dump.sh
    i)      Change any anasettings you want by adding to the agdumper line (eg: --lite, --Bfield X, etc)
    ii)     Choose your eventlist you want to run with (likely the default global eventlist if you listened to the previous step and kept the same name)
    iii)    (IMPORTANT:) Change the folder location to copy the output.root files to if you are not using `-O/dev/null` and want to save the outputs. If you do not need them then you can use `-O/dev/null` and leave the line commented out as is default.

7. scripts/condor_job/DAGs/MVADump_folder_name/run_training.sh
    i)      (IMPORTANT:) Change your chosen model. If you edited, renamed, or created a new model in the step eariler now you will need to point the training to your new model here.
        `root -l ./AGTMVAClassification.C\(\"mynewModel\"\) &>> ./outputs/modelTraining.log`
    is the line you would want to change with your new named model. 

Once you have edited all these files you are free to hit 'y' in the script. This will submit the job to condor and start running. 

## AGTMVAClassification.C ##

This file is based on the ROOT TMVA guide (https://root.cern/manual/tmva/), and still contains all the guiding notes to help adjust your settings. The most important part is to include the variables you want to use for your training. 

Copy the file into the location containing your mvaSignal.root and mvaBackground.root.

```
cd alphasoft/MVA/folder_name/
scp ../eventlists/AGTMVAClassification.C .
```

Now adjust your settings, don't change the example found in alphasoft/eventlists.

Then run

```
root -l ./AGTMVAClassification.C\(\"yourModelName\"\)
```

to run the training on the 2 root files you created above.

This will generate dataset/weights within alphasoft/MVA/folder_name/ which is essentially your model.

## jagana/agana with --usemva ##

* Then run: `jagana.exe run${run}sub0*.lz4 -- --usemva folder_name` to point the offline MVA module to your model. Full command on a trapping run should look something like:
```
jagana.exe /eos/experiment/ALPHAg/midasdata_old/run${run}sub0*.lz4 -- --Bfield 1 --lite --anasettings ana/jagana_2022.json --usemva folder_name --usetimerange 5660 5680 
```
* Your folder_name, time range, anasettings, and run file/location will change.

Finally to apply the cut or change it see TAGDetectorEvent.hh/.cxx then plot your TAGPlots. (Note: This might require you to rerun analysis)


# More Info: #

DAGMan: https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html
Condor multiple arguments: http://chtc.cs.wisc.edu/multiple-jobs.shtml#args
Condor job flavours: https://batchdocs.web.cern.ch/local/submit.html#job-flavours
ROOT TMVA: https://root.cern/manual/tmva/


TODO:
1. ~~The fact that AGMVADump.sh runs the analysis twice is not ideal. Ideally we run alphagonline to get dumps then run only on mixing windows. (Still have to run twice but at least not the whole run.). Maybe the whole pipeline needs remodelling.~~
2. Clean up the AGTMVAClassification.C
3. Include all variables and allow user to choose.
4. ~~Write a submit to condor script. This could just the job length to about 30 mins for all data.~~
5. ~~Change to keep 1 root file for all dumperoutput? or just 1 for cosmic and 1 for signal.~~
6. ~~Make the dumper only use windows when the detector is saturated~~
7. ~~Change all references of mixing to signal and cosmic to background~~
8. Fix agdumper -mt mode