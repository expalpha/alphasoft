#!/bin/bash
if [ -z $AGRELEASE ]; then echo "AGRELEASE not set! source agconfig.sh please."; return; fi


FOLDERNAME=${1}
CONDOR=${2}
DL=${3}
mkdir -p MVA/${FOLDERNAME}
mkdir -p MVA/${FOLDERNAME}/dumperoutputs
mkdir -p MVA/${FOLDERNAME}/outputs
mkdir -p MVA/${FOLDERNAME}/outputs/a2dumper
scp MVA/eventlists/A2MVA/A2TMVAClassification.C MVA/${FOLDERNAME}/
scp MVA/eventlists/A2MVA/A2TMVARegression.C MVA/${FOLDERNAME}/
scp MVA/eventlists/A2MVA/mixingList MVA/${FOLDERNAME}/
scp MVA/eventlists/A2MVA/cosmicList MVA/${FOLDERNAME}/
scp MVA/eventlists/A2MVA/eventlistfull.list MVA/${FOLDERNAME}/

echo -e "\e[41m STOP - Have you read $AGRELEASE/MVA/README.md? \e[m";
echo -e "\e[41m ./MVA/${FOLDERNAME} has just been populated, change now the eventlist, cosmicList, mixingList, or A2TMVAClassification.C if required.  \e[m";
echo -e "\e[32m Either edit the files now and type Y when you're done; or type N to exit. \e[m";
read var;
if [ $var != 'Y' ] && [ $var != 'y' ];
then
  echo -e "\e[41m Okay, based. Exiting. \e[m";
  return;
fi
#echo -e "\e[32m Data being saved in ./MVA/${FOLDERNAME} \e[m"
if [ -z $CONDOR ]; then CONDOR=0; fi

if [ $CONDOR == 1 ];
then 
    echo -e " \e[32m CONDOR Selected\e[m"
    #echo -e "\e[32m Either edit the files now and type Y when you're done; or type N to exit. \e[m";
    #read var;
    #if [ $var != 'Y' ] && [ $var != 'y' ];
    #then
    #  echo -e "\e[41m Okay, based. Exiting. \e[m";
    #  return;
    #fi

  #do condor submission

  export currentdate=`date +'%d%m%Y-%H:%M:%S'`
  mkdir ./scripts/condor_job/DAGs/A2MVADump_$FOLDERNAME
  echo -e "\e[32m Populating ./scripts/condor_job/DAGs/A2MVADump_$FOLDERNAME with your job. \e[m";

  touch ./scripts/condor_job/DAGs/A2MVADump_$FOLDERNAME/mva_input.txt
  for run in `cat MVA/${FOLDERNAME}/mixingList`
  do
    echo "$run,$FOLDERNAME,mixing,`pwd`,$DL" >> ./scripts/condor_job/DAGs/A2MVADump_$FOLDERNAME/mva_input.txt
  done
  for run in `cat MVA/${FOLDERNAME}/cosmicList`
  do
    echo "$run,$FOLDERNAME,cosmic,`pwd`,$DL" >> ./scripts/condor_job/DAGs/A2MVADump_$FOLDERNAME/mva_input.txt
  done

  cd ./scripts/condor_job/DAGs/A2MVADump_$FOLDERNAME/

  mkdir -p outputs/error outputs/log outputs/out
  scp ../../A2MVA/MVADumpFull.dag .
  scp ../../A2MVA/run_mva_dump.sh ../../A2MVA/run_mergers.sh ../../A2MVA/mva_dump.sub ../../A2MVA/mva_merge.sub .
  scp ../../A2MVA/run_training.sh ../../A2MVA/mva_train.sub .

  echo -e "\e[41m STOP - /scripts/condor_job/DAGs/A2MVADump_$FOLDERNAME has just been populated, change now the 3 '.sub' files or the 3 '.sh' files found there if required.  \e[m";
  #echo -e "\e[32m Finished, do you see any errors? \e[m";
  #echo -e "\e[32m A default model will auto train, now you should look in your new folder and edit the AGTMVAClassification.C that was generated. \e[m";
  echo -e "\e[32m If everything looks good type Y to submit this job; or type N to exit. \e[m";
  read var;
  if [ $var != 'Y' ] && [ $var != 'y' ];
  then
    cd ../../../../
    echo -e "\e[41m Okay, based. Exiting. \e[m";
    return;
  fi

  condor_submit_dag -update_submit -outfile_dir ./outputs/out MVADumpFull.dag 
  condor_q -nobatch
  condor_q
  cd ../../../../
  echo -e "\e[32m Hopefully that worked and you see your jobs in the queue. \e[m";
  return;
fi





#Using this script without condor is becoming less and less likely. Maybe everything below can just go...
echo -e " \e[32m No CONDOR Selected \e[m"
case `hostname` in
lxplus* )
  echo -e " \e[33mlxplus detected...\033[0m"
  for run in `cat MVA/${FOLDERNAME}/mixingList`
    do
        echo "Signal run ${run} running ... "
        #jagana.exe /eos/experiment/alpha/midasdata/run0${run}sub0*.mid.lz4  -- --lite --Bfield 1 --anasettings ana/jagana_2022.json &>> MVA/${FOLDERNAME}/A2MVADump_${FOLDERNAME}_output.log
        #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --mixing
        #mv eventlist0${run}.list MVA/${FOLDERNAME}
        alphaStrips.exe runs/run0${run}sub*.mid.lz4   -O/dev/null --mt -- --lite 
        alphaAnalysis.exe runs/run0${run}sub*.mid.lz4 -O/dev/null --mt -- --lite 
        a2dumper.exe /eos/experiment/alpha/midasdata/run${run}sub*.mid.lz4 -O/dev/null -- --lite --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel mixing &>> MVA/${FOLDERNAME}/A2MVADump_${FOLDERNAME}_output.log
        mv dumperoutput${run}.root MVA/${FOLDERNAME}
        echo "run ${run} complete."
    done
    echo "Mixing merger running..."
    macro_main_merger.exe -- --listfile MVA/${FOLDERNAME}/mixingList --mixing --location MVA/${FOLDERNAME}
    echo "\\n done"

  for run in `cat MVA/${FOLDERNAME}/cosmicList`
    do
        echo "Background run ${run} running ... "
        #jagana.exe /eos/experiment/alpha/midasdata/run0${run}sub0*.mid.lz4  -- --lite --Bfield 1 --anasettings ana/jagana_2022.json &>> MVA/${FOLDERNAME}/A2MVADump_${FOLDERNAME}_output.log
        #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --cosmic
        #mv eventlist0${run}.list MVA/${FOLDERNAME}
        alphaStrips.exe runs/run0${run}sub*.mid.lz4   -O/dev/null --mt -- --lite 
        alphaAnalysis.exe runs/run0${run}sub*.mid.lz4 -O/dev/null --mt -- --lite 
        a2dumper.exe /eos/experiment/alpha/midasdata/run${run}sub*.mid.lz4 -O/dev/null -- --lite --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel cosmic &>> MVA/${FOLDERNAME}/A2MVADump_${FOLDERNAME}_output.log
        mv dumperoutput${run}.root MVA/${FOLDERNAME}
        echo "run ${run} complete."
    done
    echo "Cosmic merger running..."
    macro_main_merger.exe -- --listfile MVA/${FOLDERNAME}/cosmicList --cosmic --location MVA/${FOLDERNAME}
    echo "Moving root files to  MVA/${FOLDERNAME}"
    mv mvaSignal.root MVA/${FOLDERNAME}
    mv mvaBackground.root MVA/${FOLDERNAME}
    echo "Done with all. Files plus output log saved to MVA/${FOLDERNAME}"
  ;;
* )
  echo -e " \e[33mlocal setup detected...\033[0m"
  echo -e " \e[33mAre you sure you want to run this local?\033[0m"
  for run in `cat MVA/${FOLDERNAME}/mixingList`
    do
        echo "Signal run ${run} running ... "
        #jagana.exe runs/run0${run}sub*.mid.lz4  -- --lite --Bfield 1 --anasettings ana/jagana_2022.json &>> MVA/${FOLDERNAME}/A2MVADump_${FOLDERNAME}_output.log
        #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --mixing
        #mv eventlist0${run}.list MVA/${FOLDERNAME}
        #alphaStrips.exe runs/run0${run}sub*.mid.lz4   -O/dev/null --mt -- --lite 
        #alphaAnalysis.exe runs/run0${run}sub*.mid.lz4 -O/dev/null --mt -- --lite 
        a2dumper.exe /eos/experiment/alpha/midasdata/run${run}sub* -O/dev/null -- --lite --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel mixing &>> MVA/${FOLDERNAME}/A2MVADump_${FOLDERNAME}_output.log
        mv dumperoutput*${run}.root MVA/${FOLDERNAME}/dumperoutputs
        echo "run ${run} complete."
    done
    echo "Mixing merger running..."
    macro_main_merger.exe -- --listfile mixingList --mixing --location .MVA/${FOLDERNAME}/dumperoutputs
    echo "\\n done"

    for run in `cat MVA/${FOLDERNAME}/cosmicList`
      do
        echo "Background run ${run} running ... "
        #jagana.exe runs/run0${run}sub*.mid.lz4  -- --lite --Bfield 1 --anasettings ana/jagana_2022.json &>> MVA/${FOLDERNAME}/A2MVADump_${FOLDERNAME}_output.log
        #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --cosmic
        #mv eventlist0${run}.list MVA/${FOLDERNAME}
        #alphaStrips.exe runs/run0${run}sub*.mid.lz4   -O/dev/null --mt -- --lite 
        #alphaAnalysis.exe runs/run0${run}sub*.mid.lz4 -O/dev/null --mt -- --lite 
        a2dumper.exe /eos/experiment/alpha/midasdata/run${run}sub* -O/dev/null -- --lite --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel cosmic &>> MVA/${FOLDERNAME}/A2MVADump_${FOLDERNAME}_output.log
        mv dumperoutput*${run}.root MVA/${FOLDERNAME}/dumperoutputs
        echo "run ${run} complete."
    done
    echo "Cosmic merger running..."
    macro_main_merger.exe -- --listfile cosmicList --cosmic --location .MVA/${FOLDERNAME}/dumperoutputs
    echo "Moving root files to  MVA/${FOLDERNAME}"
    mv mvaSignal.root MVA/${FOLDERNAME}
    mv mvaBackground.root MVA/${FOLDERNAME}
    echo "Done with all. Files plus output log saved to MVA/${FOLDERNAME}"
  ;;
esac

return;
