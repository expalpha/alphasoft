#!/bin/bash
if [ -z $AGRELEASE ]; then echo "AGRELEASE not set! source agconfig.sh please"; return; fi


FOLDERNAME=${1}
CONDOR=${2}
DL=${3}
mkdir -p MVA/${FOLDERNAME}
mkdir -p MVA/${FOLDERNAME}/dumperoutputs
mkdir -p MVA/${FOLDERNAME}/outputs
mkdir -p MVA/${FOLDERNAME}/outputs/agdumper
mkdir -p MVA/${FOLDERNAME}/outputs/agana
mkdir -p MVA/${FOLDERNAME}/outputs/agonline
scp MVA/eventlists/AGMVA/AGTMVAClassification.C MVA/${FOLDERNAME}/
scp MVA/eventlists/AGMVA/AGTMVARegression.C MVA/${FOLDERNAME}/
scp MVA/eventlists/AGMVA/mixingList MVA/${FOLDERNAME}/
scp MVA/eventlists/AGMVA/cosmicList MVA/${FOLDERNAME}/
scp MVA/eventlists/AGMVA/eventlistfull.list MVA/${FOLDERNAME}/

echo -e "\e[41m STOP - Have you read $AGRELEASE/MVA/README.md? \e[m";
echo -e "\e[41m ./MVA/${FOLDERNAME} has just been populated, change now the eventlist, cosmicList, mixingList, or AGTMVAClassification.C if required.  \e[m";
echo -e "\e[32m Either edit the files now and type Y when you're done; or type N to exit. \e[m";
read var;
if [ $var != 'Y' ] && [ $var != 'y' ];
then
  echo -e "\e[41m Okay, based. Exiting. \e[m";
  return;
fi
#echo -e "\e[32m Data being saved in ./MVA/${FOLDERNAME} \e[m"
if [ -z $CONDOR ]; then CONDOR=0; fi

if [ $CONDOR == 1 ];
then 
    echo -e " \e[32m CONDOR Selected\e[m"
    #echo -e "\e[32m Either edit the files now and type Y when you're done; or type N to exit. \e[m";
    #read var;
    #if [ $var != 'Y' ] && [ $var != 'y' ];
    #then
    #  echo -e "\e[41m Okay, based. Exiting. \e[m";
    #  return;
    #fi

  #do condor submission

  export currentdate=`date +'%d%m%Y-%H:%M:%S'`
  mkdir ./scripts/condor_job/DAGs/AGMVADump_$FOLDERNAME
  echo -e "\e[32m Populating ./scripts/condor_job/DAGs/AGMVADump_$FOLDERNAME with your job. \e[m";

  touch ./scripts/condor_job/DAGs/AGMVADump_$FOLDERNAME/mva_input.txt
  for run in `cat MVA/${FOLDERNAME}/mixingList`
  do
    echo "$run,$FOLDERNAME,mixing,`pwd`,$DL" >> ./scripts/condor_job/DAGs/AGMVADump_$FOLDERNAME/mva_input.txt
  done
  for run in `cat MVA/${FOLDERNAME}/cosmicList`
  do
    echo "$run,$FOLDERNAME,cosmic,`pwd`,$DL" >> ./scripts/condor_job/DAGs/AGMVADump_$FOLDERNAME/mva_input.txt
  done

  cd ./scripts/condor_job/DAGs/AGMVADump_$FOLDERNAME/

  mkdir -p outputs/error outputs/log outputs/out
  scp ../../AGMVA/MVADumpFullBV.dag .
  #Dump and merge
  scp ../../AGMVA/run_mva_dump.sh ../../AGMVA/run_mva_dump_bv.sh ../../AGMVA/run_mergers.sh ../../AGMVA/mva_dump.sub ../../AGMVA/mva_dump_bv.sub ../../AGMVA/mva_merge.sub .
  #Training
  scp ../../AGMVA/run_training.sh ../../AGMVA/mva_train.sub .
  #Calib
  scp ../../AGMVA/bv_get_calib_runs.sub ../../AGMVA/run_get_calib_runs.sh .
  scp ../../AGMVA/bv_get_runs.sub ../../AGMVA/run_get_runs.sh .
  scp ../../AGMVA/bv_run_agana.sub ../../AGMVA/run_agana.sh .
  scp ../../AGMVA/bv_run_agonline.sub ../../AGMVA/run_agonline.sh .
  scp ../../AGMVA/bv_run_calib.sub ../../AGMVA/run_bv_calibration.sh .

  echo -e "\e[41m STOP - /scripts/condor_job/DAGs/AGMVADump_$FOLDERNAME has just been populated, change now the many '.sub' files or the many '.sh' files found there if required.  \e[m";
  echo -e "\e[41m Unfortunately, you have to set AGOUTPUT manually for now (I'm sorry).  \e[m";
  #echo -e "\e[32m Finished, do you see any errors? \e[m";
  #echo -e "\e[32m A default model will auto train, now you should look in your new folder and edit the AGTMVAClassification.C that was generated. \e[m";
  echo -e "\e[32m If everything looks good type Y to submit this job; or type N to exit. \e[m";
  read var;
  if [ $var != 'Y' ] && [ $var != 'y' ];
  then
    cd ../../../../
    echo -e "\e[41m Okay, based. Exiting. \e[m";
    return;
  fi

  condor_submit_dag -update_submit -outfile_dir ./outputs/out MVADumpFullBV.dag 
  condor_q -nobatch
  condor_q
  cd ../../../../
  echo -e "\e[32m Hopefully that worked and you see your jobs in the queue. \e[m";
  return;
fi








#Using this script without condor is becoming less and less likely. Maybe everything below can just go...
echo -e " \e[32m No CONDOR Selected \e[m"
case `hostname` in
lxplus* )
  echo -e " \e[33mlxplus detected...\033[0m"
  for run in `cat MVA/${FOLDERNAME}/mixingList`
    do
        echo "Signal run ${run} running ... "
        #jagana.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub0*.mid.lz4  -- --lite --Bfield 1 --anasettings ana/jagana_2022.json &>> MVA/${FOLDERNAME}/AGMVADump_${FOLDERNAME}_output.log
        #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --mixing
        #mv eventlist0${run}.list MVA/${FOLDERNAME}
        agdumper.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub*.mid.lz4 -O/dev/null -- --lite --Bfield 1 --anasettings ana/jagana_2022.json --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel mixing &>> MVA/${FOLDERNAME}/AGMVADump_${FOLDERNAME}_output.log
        mv dumperoutput${run}.root MVA/${FOLDERNAME}
        echo "run ${run} complete."
    done
    echo "Mixing merger running..."
    macro_main_merger.exe -- --listfile MVA/${FOLDERNAME}/mixingList --mixing --location MVA/${FOLDERNAME}
    echo "\\n done"

  for run in `cat MVA/${FOLDERNAME}/cosmicList`
    do
        echo "Background run ${run} running ... "
        #jagana.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub0*.mid.lz4  -- --lite --Bfield 1 --anasettings ana/jagana_2022.json &>> MVA/${FOLDERNAME}/AGMVADump_${FOLDERNAME}_output.log
        #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --cosmic
        #mv eventlist0${run}.list MVA/${FOLDERNAME}
        agdumper.exe /eos/experiment/ALPHAg/midasdata_old/run0${run}sub*.mid.lz4 -O/dev/null -- --lite --Bfield 1 --anasettings ana/jagana_2022.json --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel cosmic &>> MVA/${FOLDERNAME}/AGMVADump_${FOLDERNAME}_output.log
        mv dumperoutput${run}.root MVA/${FOLDERNAME}
        echo "run ${run} complete."
    done
    echo "Cosmic merger running..."
    macro_main_merger.exe -- --listfile MVA/${FOLDERNAME}/cosmicList --cosmic --location MVA/${FOLDERNAME}
    echo "Moving root files to  MVA/${FOLDERNAME}"
    mv mvaSignal.root MVA/${FOLDERNAME}
    mv mvaBackground.root MVA/${FOLDERNAME}
    echo "Done with all. Files plus output log saved to MVA/${FOLDERNAME}"
  ;;
* )
  echo -e " \e[33mlocal setup detected...\033[0m"
  echo -e " \e[33mAre you sure you want to run this local?\033[0m"
  for run in `cat mixingList`
    do
        echo "Signal run ${run} running ... "
        #jagana.exe runs/run0${run}sub*.mid.lz4  -- --lite --Bfield 1 --anasettings ana/jagana_2022.json &>> MVA/${FOLDERNAME}/AGMVADump_${FOLDERNAME}_output.log
        #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --mixing
        #mv eventlist0${run}.list MVA/${FOLDERNAME}
        agdumper.exe runs/run0${run}sub*.mid.lz4 -O/dev/null -- --lite --Bfield 1 --anasettings ana/jagana_2022.json --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel mixing &>> MVA/${FOLDERNAME}/AGMVADump_${FOLDERNAME}_output.log
        mv dumperoutput${run}.root MVA/${FOLDERNAME}
        echo "run ${run} complete."
    done
    echo "Mixing merger running..."
    macro_main_merger.exe -- --listfile mixingList --mixing --location ./MVA/${FOLDERNAME}
    echo "\\n done"

    for run in `cat cosmicList`
      do
        echo "Background run ${run} running ... "
        #jagana.exe runs/run0${run}sub*.mid.lz4  -- --lite --Bfield 1 --anasettings ana/jagana_2022.json &>> MVA/${FOLDERNAME}/AGMVADump_${FOLDERNAME}_output.log
        #macro_main_ag_eventlist_generator.exe -- --runnumber ${run} --cosmic
        #mv eventlist0${run}.list MVA/${FOLDERNAME}
        agdumper.exe runs/run0${run}sub*.mid.lz4 -O/dev/null -- --lite --Bfield 1 --anasettings ana/jagana_2022.json --eventlist MVA/${FOLDERNAME}/eventlistfull.list --datalabel cosmic &>> MVA/${FOLDERNAME}/AGMVADump_${FOLDERNAME}_output.log
        mv dumperoutput${run}.root MVA/${FOLDERNAME}
        echo "run ${run} complete."
    done
    echo "Cosmic merger running..."
    macro_main_merger.exe -- --listfile cosmicList --cosmic --location ./MVA/${FOLDERNAME}
    echo "Moving root files to  MVA/${FOLDERNAME}"
    mv *Mixing.root MVA/${FOLDERNAME}
    mv *Cosmic.root MVA/${FOLDERNAME}
    echo "Done with all. Files plus output log saved to MVA/${FOLDERNAME}"
  ;;
esac

return;
