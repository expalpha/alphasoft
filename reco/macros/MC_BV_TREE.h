#ifndef MC_BV_TREE_h
#define MC_BV_TREE_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TClonesArray.h"
#include "TBSCHit.hh"
#include "Riostream.h"


class MC_BV_TREE {
public :
   TTree          *tGen;   ///< TTree with the generated MC info
   TTree          *tBV;   ///< TTree with Barrel Veto info
   TTree          *tTPC;   ///< TTree with TPC info
   ///< Histos and numbers for the generated "primary particles"
   TH1F           *hGenZ;
   TH1F           *hGenMom;
   TH1F           *hGenCos2;
   TH2F           *hGenXY;
   TH2F           *hGenRZ;
   Int_t          nGenSphereBase = 0, nGenCylBase = 0;
   Int_t          nBVevents[3] = {0,0,0};
   Int_t          nBVTopCap[3] = {0,0,0}, nBVBotCap[3] = {0,0,0}, nBVLateral[3] = {0,0,0};
   Int_t          fCurrent;
   TString        filename;
   std::string    param_file_name;

// Fixed size dimensions of array or collections stored in the TTree if any.
   ///< Output trees
   TFile *fOut;
   TTree *treeDataBV;
   TTree *treeDataBV_4ML;
   Int_t event; // event number
   Double_t t_event; // event time 
   Bool_t pbar; // pbar annihilation event?
   Bool_t mc;   // mc?
   ///< MC generation info (primary particle information: pos, mom)
   Double_t xo, yo, zo, pxo, pyo, pzo;
   ///< TPC overall variables
   Int_t nTPCHits; ///< Number of TPC hits
   Int_t nTPCMCHits; ///< Number of TPC MC hits
   ///< Barrel Veto (BV) variables
   Int_t nDigi; // number of MC hits/digi (one digi/hit = one track)
   Int_t nBars; // number of "ON" bars
   ///< nBarEnds = number of "bar ends" ON (without cuts nBarEnds = nBars * 2) 
   Int_t nBarEnds;
   std::vector<Int_t> BarNumber;
   std::vector<Int_t> BarNTracks;
   std::vector<Double_t> Energy;
   std::vector<Double_t> Path;
   std::vector<Double_t> Zeta;
   std::vector<Double_t> Time;
   std::vector<Double_t> Phi;
   ///< -----------------------------------------
   ///< Variables to be stored in the output file
   std::vector<Int_t>    bars_id    ; // bars ID ON
   std::vector<Int_t>    bars_ntrks ; // bars number of tracks
   std::vector<Double_t> bars_edep  ; // bars Edep
   std::vector<Double_t> bars_path  ; // bars Edep
   std::vector<Double_t> bars_z     ; // bars Z
   std::vector<Double_t> bars_t     ; // bars Time
   std::vector<Double_t> bars_phi   ; // bars Phi
   std::vector<Double_t> bars_atop  ; // bars ATop
   std::vector<Double_t> bars_abot  ; // bars ABot
   std::vector<Double_t> bars_ttop  ; // bars tTop
   std::vector<Double_t> bars_tbot  ; // bars tBot
   ///< "Pair of bars" (vectors)
   std::vector<Double_t> pairs_tof  ; // TOF 
   std::vector<Double_t> pairs_dphi ; // Delta Phi
   std::vector<Double_t> pairs_dzeta; // Delta Zeta
   std::vector<Double_t> pairs_dist ; // Distance
   Double_t TOF_MIN, TOF_MAX, TOF_MEAN, TOF_STD;
   Double_t DPHI_MIN, DPHI_MAX, DPHI_MEAN, DPHI_STD;
   Double_t DZETA_MIN, DZETA_MAX, DZETA_MEAN, DZETA_STD;
   Double_t DIST_MIN, DIST_MAX, DIST_MEAN, DIST_STD;

   ///< Parameters to "emulate real detector" response
   ///< Resolutions
   Double_t a_res_top; ///< a_res*sqrt(a)
   Double_t a_res_bot; ///< a_res*sqrt(a)
   Double_t t_res_top; ///< 500 ps = 0.5 ns (time is in seconds)
   Double_t t_res_bot; ///< 500 ps = 0.5 ns (time is in seconds)
   ///< Different gain for bot/top SiPMs
   Double_t gain_top[64];
   Double_t gain_bot[64];
	Double_t top_gain;   ///< Overall gain (including MC EDEP to DATA Amplitude factor)
	Double_t top_spread; ///< Spread to simulate bar-to-bar fluctuations
	Double_t bot_gain;   ///< Overall gain (including MC EDEP to DATA Amplitude factor)
	Double_t bot_spread; ///< Spread to simulate bar-to-bar fluctuations
	Double_t deltaE_a;   ///< Energy resolution (imaging a deltaE_a/sqrt(E) dependance)
	Double_t gain_c;     ///< "Non linearity" of the SiPM
   // Double_t a_bot_top_asymm ;
   // Double_t a_top_gain_sigma;
   // Double_t a_bot_gain_sigma;
   // Double_t e_to_a_factor;
   ///< Cuts 
   Double_t A_CUT;
   ///< LAMBDA
   Double_t LAMBDA;
///< ML variables (to be used by the ML python code)
   Double_t EventTime_4ML;
   Int_t    BarNumber_4ML[64];
   Double_t AmpTop_4ML[64];
   Double_t AmpBot_4ML[64];
   Double_t TDCTop_4ML[64];
   Double_t TDCBot_4ML[64];

   // Declaration of leaf types
   TClonesArray    *MCvertex;
   TClonesArray    *MCpions;
   TClonesArray    *ScintBarDigiMCTruth;
   TClonesArray    *TPCHits, *TPCMCHits;


   // List of branches
   TBranch        *b_MCvertex;   //!
   TBranch        *b_MCpions;   //!
   TBranch        *b_ScintBarDigiMCTruth;   //!
   TBranch        *b_TPCHits;   //!
   TBranch        *b_TPCMCHits;   //!

   MC_BV_TREE(std::string f_data_name, std::string f_param_name);
   virtual ~MC_BV_TREE();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual void     InitGenTree(TTree *tree);
   virtual void     InitBVTree(TTree *tree);
   virtual void     InitTPCTree(TTree *tree);
   virtual Long64_t LoadGenTree(Long64_t entry);
   virtual Long64_t LoadBVTree(Long64_t entry);
   virtual Long64_t LoadTPCTree(Long64_t entry);
   virtual void     AnalyzeBVBars();
   virtual void     SetParameters();
   virtual void     SetGains();
   virtual void     PrintParameters();
   virtual Bool_t   Notify();
   virtual void     MeanSigma(std::vector<Double_t>, Double_t&, Double_t&, Double_t&, Double_t&);
   ///< Output tree methods
   virtual void     CreateOutputTree();
   virtual void     CreateMLOutputTree();
   virtual void     ClearTreeVariables();
   virtual void     ClearMLTreeVariables();
   ///< Fill variable methods
   virtual void     FillVariables();
   virtual void     FillPairVariables();
   virtual void     FillMLVariables();
   ///< Utility methods for generated muons
   virtual Double_t RfromZ(Double_t); ///< Find the radius at a given Z for the primary
   virtual Bool_t   IsCrossingBV();

};

#endif

#ifdef MC_BV_TREE_cxx

MC_BV_TREE::MC_BV_TREE(std::string f_data_name, std::string f_param_name) : tBV(0) 
{
   ///< Check the presence of the "pbar" string to assing pbar or "not pbar = cosmics" flag
   pbar = false; ///< default value
   mc = true; ///< Always true if generated with this macro (that reads the MC output)
   if(f_data_name.find("pbar") != std::string::npos) pbar = true;
   if(f_data_name.find("cosmic") != std::string::npos) pbar = false;
   
   param_file_name = f_param_name;
   filename=f_data_name;
   Int_t dot, slash, len;
   dot = filename.Last('.');
   len = filename.Length();
   filename.Remove(dot,len-dot);
   slash = filename.First('/');
   filename.Remove(0,slash+1);

   // TString fname = filename+".root";
   
   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(f_data_name.c_str());
   if (!f || !f->IsOpen()) {
      f = new TFile(f_data_name.c_str());
   }
   ///< Loading the required trees
   TTree *treeBVBars = 0;
   f->GetObject("ScintBarsMCdata",treeBVBars);
   InitBVTree(treeBVBars);

   TTree *treeMC = 0;
   f->GetObject("MCinfo",treeMC);
   InitGenTree(treeMC);

   TTree *treeTPC = 0;
   f->GetObject("TPCMCdata",treeTPC);
   InitTPCTree(treeTPC);

   std::ostringstream out_root_file;
   struct stat st;
   if(stat("root_output_files/",&st) != 0 || ((st.st_mode & (S_IFDIR | S_IFLNK)) == 0)) { // It doesn't exist (we accept links too, sir)
      mkdir("root_output_files/", 0755);
   }
   out_root_file << "root_output_files/" << "MC_BV_TREE_" << filename << ".root";
   fOut = new TFile(out_root_file.str().c_str(),"RECREATE");

}

MC_BV_TREE::~MC_BV_TREE()
{
   if (!tBV) return;
   delete tBV->GetCurrentFile();
}

Int_t MC_BV_TREE::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!tBV) return 0;
   return tBV->GetEntry(entry);
}

Long64_t MC_BV_TREE::LoadGenTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!tGen) return -5;
   Long64_t centry = tGen->LoadTree(entry);
   if (centry < 0) return centry;
   if (tGen->GetTreeNumber() != fCurrent) {
      fCurrent = tGen->GetTreeNumber();
      Notify();
   }
   return centry;
}

Long64_t MC_BV_TREE::LoadBVTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!tBV) return -5;
   Long64_t centry = tBV->LoadTree(entry);
   if (centry < 0) return centry;
   if (tBV->GetTreeNumber() != fCurrent) {
      fCurrent = tBV->GetTreeNumber();
      Notify();
   }
   return centry;
}

Long64_t MC_BV_TREE::LoadTPCTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!tTPC) return -5;
   Long64_t centry = tTPC->LoadTree(entry);
   if (centry < 0) return centry;
   if (tTPC->GetTreeNumber() != fCurrent) {
      fCurrent = tTPC->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MC_BV_TREE::InitGenTree(TTree *tree)
{
   // Set object pointer
   MCvertex = 0;
   MCpions = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   tGen = tree;
   fCurrent = -1;
   tGen->SetMakeClass(1);

   tGen->SetBranchAddress("MCvertex", &MCvertex, &b_MCvertex);
   tGen->SetBranchAddress("MCpions", &MCpions, &b_MCpions);
   Notify();
}

void MC_BV_TREE::InitBVTree(TTree *tree)
{
   // Set object pointer
   ScintBarDigiMCTruth = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   tBV = tree;
   fCurrent = -1;
   tBV->SetMakeClass(1);

   tBV->SetBranchAddress("ScintBarDigiMCTruth", &ScintBarDigiMCTruth, &b_ScintBarDigiMCTruth);
   Notify();
}

void MC_BV_TREE::InitTPCTree(TTree *tree)
{
   // Set object pointer
   TPCHits = 0;
   TPCMCHits = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   tTPC = tree;
   fCurrent = -1;
   tTPC->SetMakeClass(1);

   tTPC->SetBranchAddress("TPCHits", &TPCHits, &b_TPCHits);
   tTPC->SetBranchAddress("TPCMCHits", &TPCMCHits, &b_TPCMCHits);
   Notify();
}

Bool_t MC_BV_TREE::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef MC_BV_TREE_cxx
