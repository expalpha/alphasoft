#include "Utils.hh"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <set>

#include "TGraph.h"
#include "TGraphErrors.h"
#include "TEllipse.h"
#include "TPolyMarker.h"
#include "TLine.h"
#include "TMath.h"
#include "TLegend.h"

#include "TMChit.hh"
#include "LookUpTable.hh"
#include "TSpacePoint.hh"
#include "TFitLine.hh"
#include "TWaveform.hh"

#include "TStoreEvent.hh"

Utils::Utils(double B):fHisto(),pmap(),
                       fMagneticField(B),tmax(4500.)
{
   BookG4Histos();
   MakeCanvases();
}


Utils::Utils(std::string fname, double B):fHisto(fname),pmap(),
                                          fMagneticField(B),tmax(4500.)
{
   std::cout<<"Utils::Utils(std::string fname, double B)"<<std::endl;
}


void Utils::MakeCanvases()
{
   std::cout<<"Utils::MakeCanvases()"<<std::endl;
   
   csig = new TCanvas("csig","csig",2400,2400);
   csig->Divide(2,2);
   
   creco = new TCanvas("creco","creco",2400,2400);
   creco->Divide(2,2);
}

void Utils::BookG4Histos()
{
   fHisto.Book("hNhel","Reconstructed Helices",10,0.,10.);
   fHisto.Book("hhchi2R","Hel #chi^{2}_{R}",100,0.,200.);
   fHisto.Book("hhchi2Z","Hel #chi^{2}_{Z}",100,0.,200.);
   fHisto.Book("hhD","Hel D;[mm]",200,-200.,200.);
   fHisto.Book("hhc","Hel c;[mm^{-1}]",200,-1.e-1,1.e-1);
   fHisto.Book("hhspxy","Spacepoints in Helices;x [mm];y [mm]",
               100,-190.,190.,100,-190.,190.);
   fHisto.Book("hhspzr","Spacepoints in Helices;z [mm];r [mm]",
               600,-1200.,1200.,61,109.,174.);
   fHisto.Book("hhspzp","Spacepoints in Helices;z [mm];#phi [deg]",
               600,-1200.,1200.,100,0.,360.);
   fHisto.Book("hhsprp","Spacepoints in Helices;#phi [deg];r [mm]",
               100,0.,TMath::TwoPi(),61,109.,174.);
   fHisto.Book("hNusedhel","Used Helices",10,0.,10.);
   fHisto.Book("huhchi2R","Used Hel #chi^{2}_{R}",100,0.,200.);
   fHisto.Book("huhchi2Z","Used Hel #chi^{2}_{Z}",100,0.,200.);
   fHisto.Book("huhD","Used Hel D;[mm]",200,-200.,200.);
   fHisto.Book("huhc","Used Hel c;[mm^{-1}]",200,-1.e-1,1.e-1);
   fHisto.Book("huhspxy","Spacepoints in Used Helices;x [mm];y [mm]",
               100,-190.,190.,100,-190.,190.);
   fHisto.Book("huhspzr","Spacepoints in Used Helices;z [mm];r [mm]",
               600,-1200.,1200.,61,109.,174.);
   fHisto.Book("huhspzp","Spacepoints in Used Helices;z [mm];#phi [deg]",
               600,-1200.,1200.,100,0.,360.);
   fHisto.Book("huhsprp","Spacepoints in Used Helices;#phi [deg];r [mm]",
               100,0.,TMath::TwoPi(),90,109.,174.);
   fHisto.Book("hvtxres","Vertex Resolution;[mm]",200,0.,200.);
}

void Utils::BookAGG4Histos()
{
   fHisto.Book("hpTgood","Transverse Momentum of Good Helices;p_{T} [MeV/c]",2000,0.,2000.);
   fHisto.Book("hpZgood","Longitudinal Momentum of Good Helices;p_{Z} [MeV/c]",4000,-2000.,2000.);
   fHisto.Book("hpTused","Transverse Momentum of Used Helices;p_{T} [MeV/c]",2000,0.,2000.);
   fHisto.Book("hpZused","Longitudinal Momentum of Used Helices;p_{Z} [MeV/c]",4000,-2000.,2000.);

   fHisto.Book("hpToTgood","Total Momentum of Good Helices;p_{Z} [MeV/c]",2000,0.,2000.);
   fHisto.Book("hpToTused","ToT Momentum of Used Helices;p_{T} [MeV/c]",2000,0.,2000.);

   fHisto.Book("hpTZgood","Momentum Component Correlation;p_{T} [MeV/c];p_{Z} [MeV/c]",
               2000,0.,2000.,2000,0.,2000.);
   fHisto.Book("hpTZused","Momentum Component Correlation;p_{T} [MeV/c];p_{Z} [MeV/c]",
               2000,0.,2000.,2000,0.,2000.);

   fHisto.Book("hVchi2","Vertex Fit #chi^{2};#chi^{2};events",5000,0.,15.);

   fHisto.Book("hResX", "Vertex Resolution X,x_{MC} - x_{REC} [mm];events",3000,-150.,150.);
   fHisto.Book("hResY", "Vertex Resolution Y,y [mm];events",3000,-150.,150.);
   fHisto.Book("hResRad","Vertex Resolution R;r [mm];events",3000,-150.,150.);
   fHisto.Book("hResPhi","Vertex Resolution #phi;#phi [rad];events",6000,
               -TMath::TwoPi(),TMath::TwoPi());
   fHisto.Book("hResZed","Vertex Resolution Z;z [mm];events",5000,-1152.,1152.);

   fHisto.Book("hRadProf","Vertex Radial Profile;r [mm];events",500,0.,100.);
   fHisto.Book("hAxiProf","Vertex Axial Profile;z [mm];events",3000,-1152.,1152);

   fHisto.Book("hvrMC","MC Vertex X-Y",500,-50.,50.,500,-50.,50.);
   fHisto.GetHisto("hvrMC")->SetMarkerColor(kRed);
   fHisto.Book("hvrRC","REC Vertex X-Y",500,-50.,50.,500,-50.,50.);

   fHisto.Book("hAWamplitudes","Anode wires amplitude distribution;Amplitude;Counts",65536,-32768.,32767.);
   fHisto.Book("hPWBamplitudes","PWB amplitude distribution;Amplitude;Counts",65536,-32768.,32767.);
}

void Utils::BookRecoHistos()
{
   fHisto.Book("hNpoints","Reconstructed Spacepoints",1000,0.,1000.);
   fHisto.Book("hNpointstracks","Reconstructed Spacepoints in Found Tracks",1000,0.,1000.);
   fHisto.Book("hNgoodpoints","Reconstructed Spacepoints Used in Tracking",1000,0.,1000.);

   fHisto.Book("hPointsTracksCorr", "Points vs Points in Tracks;Spacepoints;Spacepoints in tracks", 1000, 0., 1000., 1000, 0., 1000.);

   fHisto.Book("hOccAwpoints","Aw Occupancy for Points;aw",256,-0.5,255.5);
   fHisto.Book("hAwpointsOccIsec","Number of AW hits Inside Pad Sector for Points;N",8,0.,8.);
   fHisto.GetHisto("hAwpointsOccIsec")->SetMinimum(0);
   fHisto.Book("hOccPadpoints","Pad Occupancy for Points;row;sec",576,-0.5,575.5,32,-0.5,31.5);

   fHisto.Book("hspzphipoints","Spacepoint Axial-Azimuth for Points;z [mm];#phi [deg]",
               500,-1152.,1152.,100,0.,360.);
   fHisto.Book("hspxypoints","Spacepoint X-Y for Points;x [mm];y [mm]",100,-190.,190.,100,-190.,190.);

   fHisto.Book("hNtracks","Reconstructed Tracks",10,0.,10.);
   fHisto.Book("hNgoodtracks","Reconstructed Good tracks",10,0.,10.);

   fHisto.Book("hpattreceff","Reconstructed Spacepoints/Tracks",200,0.,200.);
   fHisto.Book("hgoodpattreceff","Reconstructed Good Spacepoints/Tracks",200,0.,200.);

   // intermediate step: spacepoints from reco tracks
   fHisto.Book("hOccPadtracks","Pad Occupancy for Tracks;row;sec",576,-0.5,575.5,32,-0.5,31.5);
   fHisto.Book("hOccAwtracks","Aw Occupancy for Tracks;aw",256,-0.5,255.5);

   fHisto.Book("hspradtracks","Spacepoint Radius for Tracks;r [mm]",100,109.,174.);
   fHisto.Book("hspphitracks","Spacepoint Azimuth for Tracks;#phi [deg]",100,0.,360.);
   fHisto.GetHisto("hspphitracks")->SetMinimum(0);
   fHisto.Book("hspzedtracks","Spacepoint Axial for Tracks;z [mm]",125,-1152.,1152.);

   fHisto.Book("hspzphitracks","Spacepoint Axial-Azimuth for Tracks;z [mm];#phi [deg]",
               500,-1152.,1152.,100,0.,360.);
   fHisto.Book("hspxytracks","Spacepoint X-Y for Tracks;x [mm];y [mm]",100,-190.,190.,100,-190.,190.);

   fHisto.Book("hchi2","#chi^{2} of Straight Lines",200,0.,200.); // chi^2 of line fit

   fHisto.Book("hhchi2R","Hel #chi^{2}_{R}",200,0.,200.); // R chi^2 of helix
   fHisto.Book("hhchi2Z","Hel #chi^{2}_{Z}",200,0.,1000.); // Z chi^2 of helix
   fHisto.Book("hhD","Hel D;[mm]",500,-190.,190.);
   fHisto.GetHisto("hhD")->SetMinimum(0);

   // spacepoints from fit tracks
   fHisto.Book("hOccPad","Pad Occupancy for Good Tracks;row;sec",576,-0.5,575.5,32,-0.5,31.5);
   fHisto.Book("hOccAw","Aw Occupancy for Good Tracks;aw",256,-0.5,255.5);
   fHisto.GetHisto("hOccAw")->SetMinimum(0);
   fHisto.Book("hAwOccIsec","Number of AW hits Inside Pad Sector for Good Tracks;N",8,0.,8.);
   fHisto.GetHisto("hAwOccIsec")->SetMinimum(0);

   fHisto.Book("hsprad","Spacepoint Radius for Good Tracks;r [mm]",100,109.,174.);
   fHisto.Book("hspphi","Spacepoint Azimuth for Good Tracks;#phi [deg]",100,0.,360.);
   fHisto.GetHisto("hspphi")->SetMinimum(0);
   fHisto.Book("hspzed","Spacepoint Axial for Good Tracks;z [mm]",125,-1152.,1152.);

   fHisto.Book("hspzphi","Spacepoint Axial-Azimuth for Good Tracks;z [mm];#phi [deg]",
               600,-1200.,1200.,256,0.,360.);
   fHisto.Book("hspxy","Spacepoint X-Y for Good Tracks;x [mm];y [mm]",100,-190.,190.,100,-190.,190.);

   fHisto.Book("hTrackXaw","Number of Good Tracks per AW;aw",256,-0.5,255.5);
   fHisto.Book("hTrackXpad","Number of Good Tracks per Pad;row;sec",576,-0.5,575.5,32,-0.5,31.5);
 

   // reco vertex
   fHisto.Book("hvtxrad","Vertex R;r [mm]",200,0.,190.);
   fHisto.Book("hvtxphi","Vertex #phi;#phi [deg]",360,0.,360.);
   fHisto.GetHisto("hvtxphi")->SetMinimum(0);
   fHisto.Book("hvtxzed","Vertex Z;z [mm]",1000,-1152.,1152.);
   fHisto.Book("hvtxzedphi","Vertex Z-#phi;z [mm];#phi [deg]",100,-1152.,1152.,180,0.,360.);

}

void Utils::FillSignalsHistos(TClonesArray* aw_signals, TClonesArray* pwb_signals) {
	for(int i = 0; i < aw_signals->GetEntries(); ++i) {
		TWaveform* waveform = (TWaveform*) aw_signals->ConstructedAt(i);
		int max_amplitude = 0;
		for(int sample : waveform->GetWaveform()) {
			if(abs(sample) > abs(max_amplitude)) {
				max_amplitude = sample;
			}
		}
		if(max_amplitude != 0) {
			fHisto.FillHisto("hAWamplitudes", max_amplitude);
		}
	}
	for(int i = 0; i < pwb_signals->GetEntries(); ++i) {
		TWaveform* waveform = (TWaveform*) pwb_signals->ConstructedAt(i);
		int max_amplitude = 0;
		for(int sample : waveform->GetWaveform()) {
			if(abs(sample) > abs(max_amplitude)) {
				max_amplitude = sample;
			}
		}
		if(max_amplitude != 0) {
			fHisto.FillHisto("hPWBamplitudes", max_amplitude);
		}
	}
}

void Utils::FillRecoPointsHistos(const std::vector<TSpacePoint>* points)
{  
   int row,sec;
   for(size_t p=0; p<points->size(); ++p)
      {
         const TSpacePoint& ap = points->at(p);
         if( !ap.IsGood(ALPHAg::_cathradius, ALPHAg::_fwradius) ) continue;
         fHisto.FillHisto("hOccAwpoints",ap.GetWire());
         fHisto.FillHisto("hAwpointsOccIsec",ap.GetWire()%8);
         pmap.get(ap.GetPad(),sec,row);
         fHisto.FillHisto("hOccPadpoints",row,sec);
         
         fHisto.FillHisto("hspzphipoints",ap.GetZ(),ap.GetPhi()*TMath::RadToDeg());
         fHisto.FillHisto("hspxypoints",ap.GetX(),ap.GetY());
      }
}

void Utils::FillRecoTracksHisto(const std::vector<TTrack>* found_tracks)
{  
   int row,sec;
   Npointstracks=0;
   for(size_t t=0; t<found_tracks->size(); ++t)
      {
         const TTrack& at = found_tracks->at(t);
         const std::vector<TSpacePoint>* spacepoints = at.GetPointsArray();
         for( const auto& it: *spacepoints )
            {
               fHisto.FillHisto("hOccAwtracks",it.GetWire());
               pmap.get(it.GetPad(),sec,row);
               fHisto.FillHisto("hOccPadtracks",row,sec);
		  
               fHisto.FillHisto("hspradtracks",it.GetR());
               fHisto.FillHisto("hspphitracks",it.GetPhi()*TMath::RadToDeg());
               fHisto.FillHisto("hspzedtracks",it.GetZ());
		  
               fHisto.FillHisto("hspzphitracks",it.GetZ(),it.GetPhi()*TMath::RadToDeg());
               fHisto.FillHisto("hspxytracks",it.GetX(),it.GetY());
               ++Npointstracks;
            }
      }
}

void Utils::FillFitTracksHisto(const std::vector<TFitHelix>* tracks_array)
{  
   int row,sec;
   Npoints=0;
   std::set<int> trkXpad;
   std::set<int> trXaw;
   for(size_t t=0; t<tracks_array->size(); ++t)
      {
         const TFitHelix& at = tracks_array->at(t);
         const std::vector<TSpacePoint>* spacepoints = at.GetPointsArray();
         for(const auto& it: *spacepoints )
            {
               fHisto.FillHisto("hOccAw",it.GetWire());
               fHisto.FillHisto("hAwOccIsec",it.GetWire()%8);
               trXaw.insert(it.GetWire());
               pmap.get(it.GetPad(),sec,row);
               trkXpad.insert(it.GetPad());
               fHisto.FillHisto("hOccPad",row,sec);
		  
               fHisto.FillHisto("hsprad",it.GetR());
               fHisto.FillHisto("hspphi",it.GetPhi()*TMath::RadToDeg());
               fHisto.FillHisto("hspzed",it.GetZ());
		  
               fHisto.FillHisto("hspzphi",it.GetZ(),it.GetPhi()*TMath::RadToDeg());
               fHisto.FillHisto("hspxy",it.GetX(),it.GetY());

               ++Npoints;
            }
              
         for(auto iaw = trXaw.begin(); iaw != trXaw.end(); ++iaw)
            {
               fHisto.FillHisto("hTrackXaw",*iaw);
            }
         for(auto ipd = trkXpad.begin(); ipd != trkXpad.end(); ++ipd)
            {
               pmap.get(*ipd,sec,row);
               fHisto.FillHisto("hTrackXpad",row,sec);                
            }

            double chi2 = at.GetRchi2();
            double ndf = (double) at.GetRDoF();
            fHisto.FillHisto("hhchi2R",chi2/ndf);

            chi2 = at.GetZchi2();
            ndf = (double) at.GetZDoF();
            fHisto.FillHisto("hhchi2Z",chi2/ndf);

            fHisto.FillHisto("hhD", at.GetD() );

      }
}

void Utils::FillFitTracksHisto(const std::vector<TFitLine>* tracks_array)
{  
   int row,sec;
   Npoints=0;
   std::set<int> trkXpad;
   std::set<int> trXaw;
   for(size_t t=0; t<tracks_array->size(); ++t)
      {
         const TFitLine& at =tracks_array->at(t);
         const std::vector<TSpacePoint>* spacepoints = at.GetPointsArray();
         for(const auto& it: *spacepoints )
            {
               fHisto.FillHisto("hOccAw",it.GetWire());
               fHisto.FillHisto("hAwOccIsec",it.GetWire()%8);
               trXaw.insert(it.GetWire());
               pmap.get(it.GetPad(),sec,row);
               trkXpad.insert(it.GetPad());
               fHisto.FillHisto("hOccPad",row,sec);
		  
               fHisto.FillHisto("hsprad",it.GetR());
               fHisto.FillHisto("hspphi",it.GetPhi()*TMath::RadToDeg());
               fHisto.FillHisto("hspzed",it.GetZ());
		  
               fHisto.FillHisto("hspzphi",it.GetZ(),it.GetPhi()*TMath::RadToDeg());
               fHisto.FillHisto("hspxy",it.GetX(),it.GetY());

               ++Npoints;
            }
              
         for(auto iaw = trXaw.begin(); iaw != trXaw.end(); ++iaw)
            {
               fHisto.FillHisto("hTrackXaw",*iaw);
            }
         for(auto ipd = trkXpad.begin(); ipd != trkXpad.end(); ++ipd)
            {
               pmap.get(*ipd,sec,row);
               fHisto.FillHisto("hTrackXpad",row,sec);                
            }

            double ndf= (double) at.GetDoF();
            double chi2 = at.GetChi2();
            fHisto.FillHisto("hchi2",chi2/ndf);
      }
}


void Utils::FillRecoVertex(const TFitVertex* Vertex)
{
   fHisto.FillHisto("hvtxrad",Vertex->GetRadius());
   double phi = Vertex->GetAzimuth();
   if( phi < 0. ) phi+=TMath::TwoPi();
   phi*=TMath::RadToDeg();
   fHisto.FillHisto("hvtxphi",phi);
   fHisto.FillHisto("hvtxzed",Vertex->GetElevation());
   fHisto.FillHisto("hvtxzedphi",Vertex->GetElevation(),phi);
}

// ===============================================================================================

void Utils::FillFinalHistos(const TStoreEvent* r, int ntracks)
{
   fHisto.FillHisto("hNpoints",r->GetNumberOfPoints());
   if(Npointstracks) fHisto.FillHisto("hNpointstracks",Npointstracks);
   if(Npoints) fHisto.FillHisto("hNgoodpoints",Npoints);

   fHisto.FillHisto("hNtracks",r->GetNumberOfTracks());
   fHisto.FillHisto("hNgoodtracks",double(ntracks));

   if( r->GetNumberOfTracks() > 0 )
      fHisto.FillHisto("hpattreceff", double(Npointstracks)/double(r->GetNumberOfTracks()));
   else
      fHisto.FillHisto("hpattreceff",0.);
      
   if( ntracks > 0 )
      fHisto.FillHisto("hgoodpattreceff", double(Npoints)/double(ntracks));
   else
      fHisto.FillHisto("hgoodpattreceff",0.);
}

void Utils::FillFinalHistos(int total_points, int total_tracks, int good_tracks)
{
   fHisto.FillHisto("hNpoints",total_points);
   fHisto.FillHisto("hNpointstracks",Npointstracks); //This comes from FillRecoTracksHisto. Hence that function HAS to be called before this one
   fHisto.FillHisto("hNgoodpoints",Npoints); //This comes from FillFitTracksHisto. Hence that function HAS to be called before this one

   fHisto.FillHisto("hPointsTracksCorr",total_points,Npointstracks);

   fHisto.FillHisto("hNtracks",total_tracks);
   fHisto.FillHisto("hNgoodtracks",double(good_tracks));

   if( total_tracks > 0 )
      fHisto.FillHisto("hpattreceff", double(Npointstracks)/double(total_tracks));
   else
      fHisto.FillHisto("hpattreceff",0.);
      
   if( good_tracks > 0 )
      fHisto.FillHisto("hgoodpattreceff", double(Npoints)/double(good_tracks));
   else
      fHisto.FillHisto("hgoodpattreceff",0.);
}

// ===============================================================================================

void Utils::Display(const TClonesArray* mcpoints, const TClonesArray* awpoints,
                    const std::vector<TSpacePoint>* recopoints,
                    const std::vector<TTrack>* tracks,
                    const std::vector<TFitHelix>* helices)
{
   PlotMCpoints(creco, mcpoints);
   PlotAWhits(creco, awpoints);
   PlotRecoPoints(creco, recopoints);
   PlotTracksFound(creco, tracks);
   PlotFitHelices(creco, helices);
   DrawTPCxy(creco);
}

void Utils::Display(const std::vector<TSpacePoint>* recopoints,
                    const std::vector<TTrack>* tracks,
                    const std::vector<TFitHelix>* helices)
{
   PlotRecoPoints(creco, recopoints, true);
   PlotTracksFound(creco, tracks);
   PlotFitHelices(creco, helices);
   DrawTPCxy(creco);
}


void Utils::PlotMCpoints(TCanvas* c, const TClonesArray* points)
{
   int Npoints = points->GetEntries();
   std::cout<<"[utils]#  GarfHits --> "<<Npoints<<std::endl;
   if(!Npoints) return;
   TGraph* gxy = new TGraph(Npoints);
   gxy->SetMarkerStyle(6);
   gxy->SetMarkerColor(kGreen+2);
   gxy->SetTitle("G4/Garf++ Reco Hits X-Y;x [mm];y [mm]");
   TGraph* grz = new TGraph(Npoints);
   grz->SetMarkerStyle(6);
   grz->SetMarkerColor(kGreen+2);
   grz->SetTitle("G4/Garf++ Reco Hits R-Z;r [mm];z [mm]");
   TGraph* grphi = new TGraph(Npoints);
   grphi->SetMarkerStyle(6);
   grphi->SetMarkerColor(kGreen+2);
   grphi->SetTitle("G4/Garf++ Reco Hits R-#phi;r [mm];#phi [deg]");
   TGraph* gzphi = new TGraph(Npoints);
   gzphi->SetMarkerStyle(6);
   gzphi->SetMarkerColor(kGreen+2);
   gzphi->SetTitle("G4/Garf++ Reco Hits Z-#phi;z [mm];#phi [deg]");
   for( int i=0; i<Npoints; ++i )
      {
         //TMChit* h = (TMChit*) points->ConstructedAt(i);
         TMChit* h = (TMChit*) points->At(i);
         gxy->SetPoint(i,h->GetX(),h->GetY());
         grz->SetPoint(i,h->GetRadius(),h->GetZ());
         grphi->SetPoint(i,h->GetRadius(),h->GetPhi()*TMath::RadToDeg());
         gzphi->SetPoint(i,h->GetZ(),h->GetPhi()*TMath::RadToDeg());
      }
   c->cd(1);
   gxy->Draw("AP");
   // gxy->GetXaxis()->SetRangeUser(109.,190.);
   // gxy->GetYaxis()->SetRangeUser(0.,190.);
   gxy->GetXaxis()->SetRangeUser(TMath::MinElement(gxy->GetN(),gxy->GetX())*0.9,
                                 TMath::MaxElement(gxy->GetN(),gxy->GetX())*1.1);
   gxy->GetYaxis()->SetRangeUser(TMath::MinElement(gxy->GetN(),gxy->GetY())*0.9,
                                 TMath::MaxElement(gxy->GetN(),gxy->GetY())*1.1);
   c->cd(2);
   grz->Draw("AP");
   // grz->GetXaxis()->SetRangeUser(109.,190.);
   // grz->GetYaxis()->SetRangeUser(-10.,10.);
   grz->GetXaxis()->SetRangeUser(TMath::MinElement(grz->GetN(),grz->GetX())*0.9,
                                 TMath::MaxElement(grz->GetN(),grz->GetX())*1.1);
   grz->GetYaxis()->SetRangeUser(TMath::MinElement(grz->GetN(),grz->GetY())*0.9,
                                 TMath::MaxElement(grz->GetN(),grz->GetY())*1.1);
   c->cd(3);
   grphi->Draw("AP");
   // grphi->GetXaxis()->SetRangeUser(109.,190.);
   // grphi->GetYaxis()->SetRangeUser(0.,40.);
   grphi->GetXaxis()->SetRangeUser(TMath::MinElement(grphi->GetN(),grphi->GetX())*0.9,
                                   TMath::MaxElement(grphi->GetN(),grphi->GetX())*1.1);
   grphi->GetYaxis()->SetRangeUser(TMath::MinElement(grphi->GetN(),grphi->GetY())*0.9,
                                   TMath::MaxElement(grphi->GetN(),grphi->GetY())*1.1);
   c->cd(4);
   //   TH1D* hh = new TH1D("hh","G4/Garf++ Reco Hits Z-#phi;z [mm];#phi [deg]",1,-10.,10.);
   TH1D* hh = new TH1D("hh","G4/Garf++ Reco Hits Z-#phi;z [mm];#phi [deg]",1,
                       TMath::MinElement(gzphi->GetN(),gzphi->GetX())*0.9,
                       TMath::MaxElement(gzphi->GetN(),gzphi->GetX())*1.1);
   hh->SetStats(kFALSE);
   hh->Draw();
   //  gzphi->Draw("AP");
   gzphi->Draw("Psame");
   //  gzphi->GetXaxis()->SetRangeUser(-10.,10.);
   //  gzphi->GetYaxis()->SetRangeUser(20.,30.);
   // hh->GetYaxis()->SetRangeUser(21.,28.);
   hh->GetYaxis()->SetRangeUser(TMath::MinElement(gzphi->GetN(),gzphi->GetY())*0.9,
                                TMath::MaxElement(gzphi->GetN(),gzphi->GetY())*1.1);
}

void Utils::PlotAWhits(TCanvas* c, const TClonesArray* points)
{
   LookUpTable fSTR(0.3, 1.); // uniform field version (simulation)
   int Npoints = points->GetEntries();
   TGraph* gxy = new TGraph(Npoints);
   gxy->SetMarkerStyle(6);
   gxy->SetMarkerColor(kBlue);
   gxy->SetTitle("G4/Garf++ Reco Hits X-Y;x [mm];y [mm]");
   TGraph* grz = new TGraph(Npoints);
   grz->SetMarkerStyle(6);
   grz->SetMarkerColor(kBlue);
   grz->SetTitle("G4/Garf++ Reco Hits R-Z;r [mm];z [mm]");
   TGraph* grphi = new TGraph(Npoints);
   grphi->SetMarkerStyle(6);
   grphi->SetMarkerColor(kBlue);
   grphi->SetTitle("G4/Garf++ Reco Hits R-#phi;r [mm];#phi [deg]");
   TGraph* gzphi = new TGraph(Npoints);
   gzphi->SetMarkerStyle(6);
   gzphi->SetMarkerColor(kBlue);
   gzphi->SetTitle("G4/Garf++ Reco Hits Z-#phi;z [mm];#phi [deg]");
   for( int j=0; j<Npoints; ++j )
      {
         TMChit* h = (TMChit*) points->At(j);
         double time = h->GetTime(),
            zed = h->GetZ();
         double rad = fSTR.GetRadius( time , zed );
         double phi = h->GetPhi() - fSTR.GetAzimuth( time , zed );
         if( phi < 0. ) phi += TMath::TwoPi();
         if( phi >= TMath::TwoPi() )
            phi = fmod(phi,TMath::TwoPi());
         double y = rad*TMath::Sin( phi ),
            x = rad*TMath::Cos( phi );
         gxy->SetPoint(j,x,y);
         grz->SetPoint(j,rad,zed);
         grphi->SetPoint(j,rad,phi*TMath::RadToDeg());
         gzphi->SetPoint(j,zed,phi*TMath::RadToDeg());
      }
   c->cd(1);
   gxy->Draw("Psame");
   c->cd(2);
   grz->Draw("Psame");
   c->cd(3);
   grphi->Draw("Psame");
   c->cd(4);
   gzphi->Draw("Psame");
}

void Utils::PlotRecoPoints(TCanvas* c, const std::vector<TSpacePoint>* points,
                           bool as)
{
   int Npoints = points->size();
   std::cout<<"[utils]#  Reco points --> "<<Npoints<<std::endl;
   TGraph* gxy = new TGraph(Npoints);
   gxy->SetMarkerStyle(2);
   gxy->SetMarkerColor(kRed);
   gxy->SetTitle("Reco Hits X-Y;x [mm];y [mm]");
   TGraph* grz = new TGraph(Npoints);
   grz->SetMarkerStyle(2);
   grz->SetMarkerColor(kRed);
   grz->SetTitle("Reco Hits R-Z;r [mm];z [mm]");
   TGraph* grphi = new TGraph(Npoints);
   grphi->SetMarkerStyle(2);
   grphi->SetMarkerColor(kRed);
   grphi->SetTitle("Reco Hits R-#phi;r [mm];#phi [deg]");
   TGraph* gzphi = new TGraph(Npoints);
   gzphi->SetMarkerStyle(2);
   gzphi->SetMarkerColor(kRed);
   gzphi->SetTitle("Reco Hits Z-#phi;z [mm];#phi [deg]");
   for( int i=0; i<Npoints; ++i )
      {
         //TSpacePoint* p = (TSpacePoint*) points->ConstructedAt(i);
         const TSpacePoint &p = points->at(i);
         gxy->SetPoint(i,p.GetX(),p.GetY());
         grz->SetPoint(i,p.GetR(),p.GetZ());
         grphi->SetPoint(i,p.GetR(),p.GetPhi()*TMath::RadToDeg());
         gzphi->SetPoint(i,p.GetZ(),p.GetPhi()*TMath::RadToDeg());
      }
   if( Npoints==0 ) as=false;
   if( as )
      {
         c->cd(1);
         gxy->Draw("AP");
         gxy->GetXaxis()->SetRangeUser(TMath::MinElement(gxy->GetN(),gxy->GetX())*0.9,
                                       TMath::MaxElement(gxy->GetN(),gxy->GetX())*1.1);
         gxy->GetYaxis()->SetRangeUser(TMath::MinElement(gxy->GetN(),gxy->GetY())*0.9,
                                       TMath::MaxElement(gxy->GetN(),gxy->GetY())*1.1);
         c->cd(2);
         grz->Draw("AP");
         // grz->GetXaxis()->SetRangeUser(TMath::MinElement(grz->GetN(),grz->GetX())*0.9,
         //                               TMath::MaxElement(grz->GetN(),grz->GetX())*1.1);
         grz->GetXaxis()->SetRangeUser(109.,190.);
         grz->GetYaxis()->SetRangeUser(TMath::MinElement(grz->GetN(),grz->GetY())*0.9,
                                       TMath::MaxElement(grz->GetN(),grz->GetY())*1.1);
         c->cd(3);
         grphi->Draw("AP");
         // grphi->GetXaxis()->SetRangeUser(TMath::MinElement(grphi->GetN(),grphi->GetX())*0.9,
         //                                 TMath::MaxElement(grphi->GetN(),grphi->GetX())*1.1);
         grphi->GetXaxis()->SetRangeUser(109.,190.);
         grphi->GetYaxis()->SetRangeUser(TMath::MinElement(grphi->GetN(),grphi->GetY())*0.9,
                                         TMath::MaxElement(grphi->GetN(),grphi->GetY())*1.1);
         c->cd(4);
         TH1D* hh = new TH1D("hh","G4/Garf++ Reco Hits Z-#phi;z [mm];#phi [deg]",1,
                             TMath::MinElement(gzphi->GetN(),gzphi->GetX())*0.9,
                             TMath::MaxElement(gzphi->GetN(),gzphi->GetX())*1.1);
         hh->SetStats(kFALSE);
         hh->Draw();
         gzphi->Draw("Psame");
         hh->GetYaxis()->SetRangeUser(TMath::MinElement(gzphi->GetN(),gzphi->GetY())*0.9,
                                      TMath::MaxElement(gzphi->GetN(),gzphi->GetY())*1.1);
      }
   else
      {
         c->cd(1);
         gxy->Draw("Psame");
         c->cd(2);
         grz->Draw("Psame");
         c->cd(3);
         grphi->Draw("Psame");
         c->cd(4);
         gzphi->Draw("Psame");
      }
}

void Utils::PlotTracksFound(TCanvas* c, const std::vector<TTrack>* tracks)
{
   const int Ntracks = tracks->size();
   std::cout<<"[utils]#  Reco tracks --> "<<Ntracks<<std::endl;
   // int cols[] = {kBlack,kGray,kGray+1,kGray+2,kGray+3};
   int cols[] = {kBlack,kMagenta,kCyan,kOrange,kViolet,kGray,kPink,kTeal,kSpring};
   int ncols = 9;
   // if(Ntracks > 9) Ntracks = 9;
   for(int t=0; t<Ntracks; ++t)
      {
         const TTrack &aTrack = tracks->at(t);
         int Npoints = aTrack.GetNumberOfPoints();
         std::cout<<"[utils]#  Reco points in track --> "<<Npoints<<std::endl;
         TGraphErrors* gxy = new TGraphErrors(Npoints);
         gxy->SetMarkerStyle(2);
         gxy->SetMarkerColor(cols[t%ncols]);
         gxy->SetLineColor(cols[t%ncols]);
         gxy->SetTitle("Reco Hits X-Y;x [mm];y [mm]");
         TGraphErrors* grz = new TGraphErrors(Npoints);
         grz->SetMarkerStyle(2);
         grz->SetMarkerColor(cols[t%ncols]);
         grz->SetLineColor(cols[t%ncols]);
         grz->SetTitle("Reco Hits R-Z;r [mm];z [mm]");
         TGraphErrors* grphi = new TGraphErrors(Npoints);
         grphi->SetMarkerStyle(2);
         grphi->SetMarkerColor(cols[t%ncols]);
         grphi->SetLineColor(cols[t%ncols]);
         grphi->SetTitle("Reco Hits R-#phi;r [mm];#phi [deg]");
         TGraphErrors* gzphi = new TGraphErrors(Npoints);
         gzphi->SetMarkerStyle(2);
         gzphi->SetMarkerColor(cols[t%ncols]);
         gzphi->SetLineColor(cols[t%ncols]);
         gzphi->SetTitle("Reco Hits Z-#phi;z [mm];#phi [deg]");
         const std::vector<TSpacePoint>* points = aTrack.GetPointsArray();
         for( uint i=0; i<points->size(); ++i )
            {
               const TSpacePoint& p = points->at(i);

               gxy->SetPoint(i,p.GetX(),p.GetY());
               gxy->SetPointError(i,p.GetErrX(),p.GetErrY());

               grz->SetPoint(i,p.GetR(),p.GetZ());
               grz->SetPointError(i,p.GetErrR(),p.GetErrZ());

               grphi->SetPoint(i,p.GetR(),p.GetPhi()*TMath::RadToDeg());
               grphi->SetPointError(i,p.GetErrR(),p.GetErrPhi()*TMath::RadToDeg());

               gzphi->SetPoint(i,p.GetZ(),p.GetPhi()*TMath::RadToDeg());
               gzphi->SetPointError(i,p.GetErrZ(),p.GetErrPhi()*TMath::RadToDeg());
            }
         c->cd(1);
         gxy->Draw("Psame");
         c->cd(2);
         grz->Draw("Psame");
         c->cd(3);
         grphi->Draw("Psame");
         c->cd(4);
         gzphi->Draw("Psame");
      }
}

void Utils::PlotFitHelices(TCanvas* c, const std::vector<TFitHelix>* tracks)
{
   const int Ntracks = tracks->size();
   std::cout<<"[utils]#  Reco helices --> "<<Ntracks<<std::endl;
   int cols[] = {kViolet,kOrange,kTeal,kPink,kSpring};
   int ncols = 5;
   for(int t=0; t<Ntracks; ++t)
      {
         const TFitHelix &aTrack = tracks->at(t);
         //         aTrack.Print();
         int Npoints = aTrack.GetNumberOfPoints();
         std::cout<<"[utils]#  Reco points in helix --> "<<Npoints<<std::endl;
         TPolyLine* hxy = new TPolyLine(Npoints+2);
         hxy->SetLineColor(cols[t%ncols]);
         hxy->SetLineWidth(3);
         TPolyLine* hrz = new TPolyLine(Npoints+2);
         hrz->SetLineColor(cols[t%ncols]);
         hrz->SetLineWidth(3);
         TPolyLine* hrphi = new TPolyLine(Npoints+2);
         hrphi->SetLineColor(cols[t%ncols]);
         hrphi->SetLineWidth(3);
         TPolyLine* hzphi = new TPolyLine(Npoints+2);
         hzphi->SetLineColor(cols[t%ncols]);
         hzphi->SetLineWidth(3);
         
         TVector3 p = aTrack.Evaluate(ALPHAg::_padradius*ALPHAg::_padradius);
         //p.Print();
         hxy->SetNextPoint(p.X(),p.Y());
         //         std::cout<<"Utils::PlotFitHelices\n x:"<<p.X()<<" y:"<<p.Y()<<std::endl;
         hrz->SetNextPoint(p.Perp(),p.Z());
         hrphi->SetNextPoint(p.Perp(),p.Phi()*TMath::RadToDeg());
         hzphi->SetNextPoint(p.z(),p.Phi()*TMath::RadToDeg());
         for(int n=1; n<=Npoints; ++n)
            {
               double rad = ALPHAg::_padradius*(1.-double(n)/double(Npoints));
               //std::cout<<"n: "<<n<<" rad: "<<rad<<std::endl;
               p = aTrack.Evaluate(rad*rad);
               //p.Print();
               hxy->SetNextPoint(p.X(),p.Y());
               //std::cout<<" x:"<<p.X()<<" y:"<<p.Y()<<std::endl;
               hrz->SetNextPoint(p.Perp(),p.Z());
               hrphi->SetNextPoint(p.Perp(),p.Phi()*TMath::RadToDeg());
               hzphi->SetNextPoint(p.z(),p.Phi()*TMath::RadToDeg());
            }
         p = aTrack.Evaluate(0.);
         //p.Print();
         hxy->SetNextPoint(p.X(),p.Y());
         //std::cout<<" x:"<<p.X()<<" y:"<<p.Y()<<" -- end"<<std::endl;
         hrz->SetNextPoint(p.Perp(),p.Z());
         hrphi->SetNextPoint(p.Perp(),p.Phi()*TMath::RadToDeg());
         hzphi->SetNextPoint(p.z(),p.Phi()*TMath::RadToDeg());

         c->cd(1);
         hxy->Draw("Psame");
         c->cd(2);
         hrz->Draw("Psame");
         c->cd(3);
         hrphi->Draw("Psame");
         c->cd(4);
         hzphi->Draw("Psame");
      }
}

void Utils::DrawTPCxy(TCanvas* c)
{
   TEllipse* TPCcath = new TEllipse(0.,0.,109.,109.);
   TPCcath->SetFillStyle(0);
   c->cd(1);
   TPCcath->Draw("same");
   TEllipse* TPCpads = new TEllipse(0.,0.,190.,190.);
   TPCpads->SetFillStyle(0);
   c->cd(1);
   TPCpads->Draw("same");

   double pitch = 2.*M_PI / 256., offset = 0.5*pitch;

   TPolyMarker* AWxy = new TPolyMarker(256);
   AWxy->SetMarkerStyle(45);
   AWxy->SetMarkerColor(kBlack);
   TPolyMarker* AWrphi = new TPolyMarker(256);
   AWrphi->SetMarkerStyle(45);
   AWrphi->SetMarkerColor(kBlack);
   TPolyMarker* FWxy = new TPolyMarker(256);
   FWxy->SetMarkerStyle(43);
   FWxy->SetMarkerColor(kBlack);
   TPolyMarker* FWrphi = new TPolyMarker(256);
   FWrphi->SetMarkerStyle(43);
   FWrphi->SetMarkerColor(kBlack);
   TLine* AWzphi[256];
   for( int p = 0; p<256; ++p )
      {
         double AWphi = pitch * p + offset;
         AWxy->SetPoint(p,182.*cos(AWphi),182.*sin(AWphi));
         AWrphi->SetPoint(p,182.,AWphi*TMath::RadToDeg());

         double FWphi = pitch * p;
         FWxy->SetPoint(p,174.*cos(FWphi),174.*sin(FWphi));
         FWrphi->SetPoint(p,174.,FWphi*TMath::RadToDeg());

         AWzphi[p] = new TLine(-ALPHAg::_halflength,AWphi*TMath::RadToDeg(),ALPHAg::_halflength,AWphi*TMath::RadToDeg());
         AWzphi[p]->SetLineColor(kGray+1);
         AWzphi[p]->SetLineStyle(2);
         AWzphi[p]->SetLineWidth(1);
         c->cd(4);
         AWzphi[p]->Draw("same");
      }

   c->cd(1);
   AWxy->Draw("same");
   FWxy->Draw("same");

   c->cd(3);
   AWrphi->Draw("same");
   FWrphi->Draw("same");

   TLine* AWrz = new TLine(182.,-ALPHAg::_halflength,182.,ALPHAg::_halflength);
   AWrz->SetLineColor(kGray+1);
   AWrz->SetLineStyle(2);
   AWrz->SetLineWidth(2);
   c->cd(2);
   AWrz->Draw("same");
   TLine* FWrz = new TLine(174.,-ALPHAg::_halflength,174.,ALPHAg::_halflength);
   FWrz->SetLineColor(kGray+1);
   FWrz->SetLineStyle(3);
   FWrz->SetLineWidth(2);
   c->cd(2);
   FWrz->Draw("same");
}

// If these template functions are to be used from outside this source file, they must be moved into the header instead
template<class T>
TH1D* Utils::PlotSignals(std::vector<T>* sig, std::string name)
{
   std::ostringstream hname;
   hname<<"hsig"<<name;
   std::string htitle(";t [ns];H [a.u.]");
   TH1D* h = new TH1D(hname.str().c_str(),htitle.c_str(),411,0.,16.*411.);
   h->SetStats(kFALSE);
   if(sig) {
   for(auto s: *sig)
      {
         const ALPHAg::signal &ss = static_cast<ALPHAg::signal>(s);
         if( ss.t < 16. ) continue;
         h->Fill(ss.t,ss.height);
      }
   }
   return h;
}

void Utils::Draw(std::vector<ALPHAg::TWireSignal>* awsig, std::vector<ALPHAg::TPadSignal>* padsig, 
                 std::vector<ALPHAg::TPadSignal>* combpads, bool norm)
{
   TH1D* haw=PlotSignals( awsig, "anodes" );
   if(norm) haw->Scale(1./haw->Integral());
   else haw->Scale(10.);
   haw->SetLineColor(kRed);
   //cout<<"[main]# "<<i<<"\tPlotAnodeTimes: "<<haw->GetEntries()<<endl;
   csig->cd(1);
   haw->Draw("hist");
   haw->SetTitle("Deconv Times");
   haw->GetXaxis()->SetRangeUser(0.,tmax);

   TH1D* hpads = PlotSignals( padsig, "pads" );
   if(norm) hpads->Scale(1./hpads->Integral());
   hpads->SetLineColor(kBlue);
   csig->cd(1);
   hpads->Draw("histsame");

   TLegend* leg = new TLegend(0.7,0.8,0.95,0.95);
   if(norm) leg->AddEntry(haw,"anodes", "l");
   else leg->AddEntry(haw,"anodes x10", "l");
   leg->AddEntry(hpads,"pads", "l");
   csig->cd(1);
   leg->Draw("same");

   TH1D* hcombpads = PlotSignals( combpads, "combinedpads" );
   if(norm) hcombpads->Scale(1./hcombpads->Integral());
   hcombpads->SetLineColor(kBlue);
   csig->cd(2);
   haw->Draw("hist");
   hcombpads->Draw("histsame");

   TH2D* hmatch = PlotSignals( awsig, combpads, "sector");
   //TH2D* hmatch = PlotSignals( awsig, padsig, "sector");
   csig->cd(3);
   //hmatch->Draw("colz");
   hmatch->Draw();
   hmatch->GetXaxis()->SetRangeUser(0.,tmax);
   hmatch->GetYaxis()->SetRangeUser(0.,tmax);
   
   TH1D* hoccaw = PlotOccupancy( awsig, "anodes" );
   if(norm) hoccaw->Scale(1./hoccaw->Integral());
   hoccaw->SetLineColor(kRed);
   TH1D* hocccombpads = PlotOccupancy( combpads, "pads" );
   if(norm) hocccombpads->Scale(1./hocccombpads->Integral());
   hocccombpads->SetLineColor(kBlue);
   csig->cd(4);
   hoccaw->Draw("hist");
   hocccombpads->Draw("histsame");
   gPad->SetGridx();
   hoccaw->GetXaxis()->SetNdivisions(32,kFALSE);
   hoccaw->GetXaxis()->SetLabelSize(0.02);
}

void Utils::Draw(std::vector<ALPHAg::TWireSignal>* awsig, std::vector<ALPHAg::TPadSignal>* padsig, bool norm)
{
   TH1D* haw=PlotSignals( awsig, "anodes" );
   if(norm) haw->Scale(1./haw->Integral());
   else haw->Scale(10.);
   haw->SetLineColor(kRed);
   //cout<<"[main]# "<<i<<"\tPlotAnodeTimes: "<<haw->GetEntries()<<endl;
   csig->cd(1);
   haw->Draw("hist");
   haw->SetTitle("Deconv Times");
   haw->GetXaxis()->SetRangeUser(0.,tmax);

   TH1D* hpads = PlotSignals( padsig, "pads" );
   if(norm) hpads->Scale(1./hpads->Integral());
   hpads->SetLineColor(kBlue);
   csig->cd(1);
   hpads->Draw("histsame");

   TLegend* leg = new TLegend(0.7,0.8,0.95,0.95);
   if(norm) leg->AddEntry(haw,"anodes", "l");
   else leg->AddEntry(haw,"anodes x10", "l");
   leg->AddEntry(hpads,"pads", "l");
   csig->cd(1);
   leg->Draw("same");

   // TH1D* hcombpads = PlotSignals( combpads, "combinedpads" );
   // if(norm) hcombpads->Scale(1./hcombpads->Integral());
   // hcombpads->SetLineColor(kBlue);
   // csig->cd(2);
   // haw->Draw("hist");
   // hcombpads->Draw("histsame");

   TH2D* hmatch = PlotSignals( awsig, padsig, "sector");
   //TH2D* hmatch = PlotSignals( awsig, padsig, "sector");
   csig->cd(3);
   //hmatch->Draw("colz");
   hmatch->Draw("*");
   hmatch->GetXaxis()->SetRangeUser(0.,tmax);
   hmatch->GetYaxis()->SetRangeUser(0.,tmax);
   
   TH1D* hoccaw = PlotOccupancy( awsig, "anodes" );
   if(norm) hoccaw->Scale(1./hoccaw->Integral());
   hoccaw->SetLineColor(kRed);
   TH1D* hoccpadsig = PlotOccupancy( padsig, "pads" );
   if(norm) hoccpadsig->Scale(1./hoccpadsig->Integral());
   hoccpadsig->SetLineColor(kBlue);
   csig->cd(4);
   hoccaw->Draw("hist");
   hoccpadsig->Draw("histsame");
   gPad->SetGridx();
   hoccaw->GetXaxis()->SetNdivisions(32,kFALSE);
   hoccaw->GetXaxis()->SetLabelSize(0.02);
}

// If these template functions are to be used from outside this source file, they must be moved into the header instead
template <class T>
TH1D* Utils::PlotOccupancy(std::vector<T>* sig, std::string name)
{
   std::ostringstream hname;
   hname<<"hocc"<<name;
   std::string htitle("Occupancy Azimuth;AW index / PAD sector");
   TH1D* h = new TH1D(hname.str().c_str(),htitle.c_str(),256,0.,256.);
   h->SetStats(kFALSE);
   for(auto s: *sig)
      {
         const ALPHAg::signal &ss = static_cast<ALPHAg::signal>(s);
         if( ss.t < 16. ) continue;
         if( name == "anodes" )
            h->Fill(ss.idx);
         else if( name == "pads" )
            {
               // int col = ss.sec*8+4;
               for(int i=0; i<8; ++i)
                  {
                     int col = ss.sec*8+i;
                     h->Fill(col);
                  }
            }
      }
   return h;
}

TH2D* Utils::PlotSignals(std::vector<ALPHAg::TWireSignal>* awsignals,
                         std::vector<ALPHAg::TPadSignal>* padsignals, std::string type)
{
   std::multiset<ALPHAg::TWireSignal, ALPHAg::TWireSignal::timeorder> aw_bytime(awsignals->begin(),
                                                                      awsignals->end());
   std::multiset<ALPHAg::TPadSignal, ALPHAg::TPadSignal::timeorder> pad_bytime(padsignals->begin(),
                                                                       padsignals->end());
   // Unused
   //int Nmatch=0;
   std::ostringstream hname;
   hname<<"hmatch"<<type;
   std::ostringstream htitle;
   htitle<<"AW vs PAD Time with Matching "<<type<<";AW [ns];PAD [ns]";
   TH2D* hh = new TH2D(hname.str().c_str(),htitle.str().c_str(),300,0.,6000.,300,0.,6000.);
   hh->SetStats(kFALSE);
   for( auto iaw=aw_bytime.begin(); iaw!=aw_bytime.end(); ++iaw )
      {
         for( auto ipd=pad_bytime.begin(); ipd!=pad_bytime.end(); ++ipd )
            {
               bool match=false;
               if( type == "time" )
                  {
                     double delta = fabs( iaw->t - ipd->t );
                     if( delta < 16. )
                        match=true;
                  }
               else if( type == "sector" )
                  {
                     short sector = short(iaw->idx/8);
                     if( sector == ipd->sec )
                        match=true;
                  }
               else if( type == "both" )
                  {
                     double delta = fabs( iaw->t - ipd->t );
                     short sector = short(iaw->idx/8);
                     if( delta < 16. && sector == ipd->sec )
                        match=true;
                  }
               else
                  match=true;

               if( match )
                  {
                     hh->Fill( iaw->t , ipd->t );
                     //Unused
                     //++Nmatch;
                  }
            }
      }
   return hh;
}

// ===============================================================================================
double Utils::Average(std::vector<double>* v)
{
   if( v->size() == 0 ) return -1.;
   double avg=0.;
   for( auto& x: *v ) avg+=x;
   return avg/double(v->size());
}

double Utils::EvaluateMatch_byResZ(TClonesArray* lines)
{
   int Nlines = lines->GetEntriesFast();
   if( Nlines > 1 )
      std::cerr<<"WARNING EvaluateMatch # of lines = "<<Nlines<<std::endl;
   double resZ=0.;
   for( int n=0; n<Nlines; ++n )
      {
         resZ += ((TFitLine*) lines->At(n))->GetResidual().Z();
      }
   if( Nlines != 0 ) resZ/=double(Nlines);
   return resZ;
}

int Utils::EvaluatePattRec(TClonesArray* lines)
{
   int Nlines = lines->GetEntriesFast();
   if( Nlines > 1 )
      std::cerr<<"WARNING EvaluateMatch # of lines = "<<Nlines<<std::endl;
   int Npoints=0;
   for( int n=0; n<Nlines; ++n )
      {
         Npoints+=((TFitLine*) lines->At(n))->GetNumberOfPoints();
      }
   if( Nlines != 0 ) Npoints/=Nlines;
   return Npoints;
}

// ===============================================================================================
double Utils::PointResolution(std::vector<TFitHelix>* helices, const TVector3* vtx)
{
   double res=0.,N=double(helices->size());
   for(size_t i=0; i<helices->size(); ++i)
      {
         TFitHelix &hel = helices->at(i);
         TVector3 eval = hel.Evaluate( ALPHAg::_trapradius * ALPHAg::_trapradius );
         eval.Print();
         res+=(eval-(*vtx)).Mag();
         std::cout<<i<<"\tPointResolution\tEval 3D: "<<(eval-(*vtx)).Mag()<<" mm"<<std::endl;
         std::cout<<i<<"\tPointResolution\tEval Z: "<<fabs(eval.Z()-vtx->Z())<<" mm"<<std::endl;

         hel.SetPoint( vtx );
         TVector3 minpoint;
         hel.MinDistPoint( minpoint );
         minpoint.Print();
         std::cout<<i<<"\tPointResolution\tMinDistPoint 3D: "<<(minpoint-(*vtx)).Mag()<<" mm"<<std::endl;
         std::cout<<i<<"\tPointResolution\tMinDistPoint R: "<<fabs(minpoint.Perp()-vtx->Perp())<<" mm"<<std::endl;
         std::cout<<i<<"\tPointResolution\tMinDistPoint Z: "<<fabs(minpoint.Z()-vtx->Z())<<" mm"<<std::endl;

         TVector3 int1,int2;
         int s = hel.TubeIntersection( int1, int2 );
         if( s )
            {
               int1.Print();
               std::cout<<i<<"\tPointResolution\tIntersection (1) 3D: "<<(int1-(*vtx)).Mag()<<" mm"<<std::endl;
               std::cout<<i<<"\tPointResolution\tIntersection (1) Z: "<<fabs(int1.Z()-vtx->Z())<<" mm"<<std::endl;
               int2.Print();
               std::cout<<i<<"\tPointResolution\tIntersection (2) 3D: "<<(int2-(*vtx)).Mag()<<" mm"<<std::endl;
               std::cout<<i<<"\tPointResolution\tIntersection (2) Z: "<<fabs(int2.Z()-vtx->Z())<<" mm"<<std::endl;
            }
         else
            {
               int1.Print();
               std::cout<<i<<"\tPointResolution\tIntersection 3D: "<<(int1-(*vtx)).Mag()<<" mm"<<std::endl;
               std::cout<<i<<"\tPointResolution\tIntersection Z: "<<fabs(int1.Z()-vtx->Z())<<" mm"<<std::endl;
            }
      }
   if( N>0 ) res/=N;
   return res;
}

// ===============================================================================================
void Utils::HelixPlots(std::vector<TFitHelix>* helices)
{
   int nhel=0;
   for(size_t i=0; i<helices->size(); ++i)
      {
         TFitHelix &hel = helices->at(i);
         fHisto.FillHisto("hhD",hel.GetD());
         fHisto.FillHisto("hhc",hel.GetC());
         fHisto.FillHisto("hhchi2R",hel.GetRchi2());
         fHisto.FillHisto("hhchi2Z",hel.GetZchi2());
         TVector3 p = hel.GetMomentumV();
         fHisto.FillHisto("hpTgood", p.Perp() );
         fHisto.FillHisto("hpZgood", p.Z() );
         fHisto.FillHisto("hpToTgood", p.Mag());
         fHisto.FillHisto("hpTZgood", p.Perp(), p.Z() );
         ++nhel;
         const std::vector<TSpacePoint> *sp = hel.GetPointsArray();
         for( uint ip = 0; ip<sp->size(); ++ip )
            {
               const TSpacePoint& ap = sp->at(ip);
               fHisto.FillHisto( "hhspxy" , ap.GetX(), ap.GetY() );
               fHisto.FillHisto( "hhspzp" , ap.GetZ(), ap.GetPhi()*TMath::RadToDeg() );
               fHisto.FillHisto( "hhspzr" , ap.GetZ(), ap.GetR() );
               fHisto.FillHisto( "hhsprp" , ap.GetPhi(), ap.GetR() );
            }
      }
   fHisto.FillHisto("hNhel",double(nhel));
}

void Utils::UsedHelixPlots(const TObjArray* helices)
{
   int nhel = 0;
   for(int i=0; i<helices->GetEntriesFast(); ++i)
      {
         TFitHelix* hel = (TFitHelix*) helices->At(i);
         fHisto.FillHisto("huhD",hel->GetD());
         fHisto.FillHisto("huhc",hel->GetC());
         fHisto.FillHisto("huhchi2R",hel->GetRchi2());
         fHisto.FillHisto("huhchi2Z",hel->GetZchi2());
         TVector3 p = hel->GetMomentumV();
         fHisto.FillHisto("hpTused", p.Perp() );
         fHisto.FillHisto("hpZused", p.Z() );
         fHisto.FillHisto("hpToTused", p.Mag());
         fHisto.FillHisto("hpTZused", p.Perp(), p.Z() );
         ++nhel;
         const std::vector<TSpacePoint> *sp = hel->GetPointsArray();
         for( uint ip = 0; ip<sp->size(); ++ip )
            {
               const TSpacePoint& ap = sp->at(ip);
               fHisto.FillHisto( "huhspxy" , ap.GetX(), ap.GetY() );
               fHisto.FillHisto( "huhspzp" , ap.GetZ(), ap.GetPhi()*TMath::RadToDeg() );
               fHisto.FillHisto( "huhspzr" , ap.GetZ(), ap.GetR() );
               fHisto.FillHisto( "huhsprp" , ap.GetPhi(), ap.GetR() );
            }
      }   
   fHisto.FillHisto("hNusedhel",double(nhel));
   //   std::cout<<"Utils::UsedHelixPlots Used Helices: "<<nhel<<std::endl;
}


void Utils::UsedHelixPlots(const std::vector<TFitHelix*>* helices)
{
   int nhel = 0;
   for(size_t i=0; i<helices->size(); ++i)
      {
         TFitHelix *hel =  helices->at(i);
         fHisto.FillHisto("huhD",hel->GetD());
         fHisto.FillHisto("huhc",hel->GetC());
         fHisto.FillHisto("huhchi2R",hel->GetRchi2());
         fHisto.FillHisto("huhchi2Z",hel->GetZchi2());
         TVector3 p = hel->GetMomentumV();
         fHisto.FillHisto("hpTused", p.Perp() );
         fHisto.FillHisto("hpZused", p.Z() );
         fHisto.FillHisto("hpToTused", p.Mag());
         fHisto.FillHisto("hpTZused", p.Perp(), p.Z() );
         ++nhel;
         const std::vector<TSpacePoint> *sp = hel->GetPointsArray();
         for( uint ip = 0; ip<sp->size(); ++ip )
            {
               const TSpacePoint& ap = sp->at(ip);
               fHisto.FillHisto( "huhspxy" , ap.GetX(), ap.GetY() );
               fHisto.FillHisto( "huhspzp" , ap.GetZ(), ap.GetPhi()*TMath::RadToDeg() );
               fHisto.FillHisto( "huhspzr" , ap.GetZ(), ap.GetR() );
               fHisto.FillHisto( "huhsprp" , ap.GetPhi(), ap.GetR() );
            }
      }
   fHisto.FillHisto("hNusedhel",double(nhel));
   //   std::cout<<"Utils::UsedHelixPlots Used Helices: "<<nhel<<std::endl;
}

// ===============================================================================================
double Utils::VertexResolution(const TVector3* vtx, const TVector3* mcvtx)
{
   fHisto.FillHisto("hResX",   vtx->X()    - mcvtx->X() );
   fHisto.FillHisto("hResY",   vtx->Y()    - mcvtx->Y() );
   fHisto.FillHisto("hResRad", vtx->Perp() - mcvtx->Perp() );
   double dphi=vtx->Phi() - mcvtx->Phi();
   if(dphi > TMath::Pi() )
      dphi -= TMath::TwoPi();
   if(dphi < -TMath::Pi() )
      dphi += TMath::TwoPi();
   fHisto.FillHisto("hResPhi", dphi );
   fHisto.FillHisto("hResZed", vtx->Z()    - mcvtx->Z() );

   fHisto.FillHisto("hvrMC", mcvtx->X(), mcvtx->Y() );

   double res = TMath::Sqrt( (vtx->X()-mcvtx->X())*(vtx->X() - mcvtx->X()) +
                             (vtx->Y()-mcvtx->Y())*(vtx->Y() - mcvtx->Y()) +
                             (vtx->Z()-mcvtx->Z())*(vtx->Z() - mcvtx->Z()) );
   fHisto.FillHisto("hvtxres",res);
   return res;
}

// ===============================================================================================
void Utils::VertexPlots(const TFitVertex* v)
{
   const TVector3* vtx=v->GetVertex();
   fHisto.FillHisto("hvrRC", vtx->X() ,  vtx->Y() );

   fHisto.FillHisto("hRadProf",vtx->Perp());
   fHisto.FillHisto("hAxiProf",vtx->Z());

   fHisto.FillHisto("hVchi2", v->GetChi2());
}

// ===============================================================================================
void Utils::WriteSettings(TObjString* sett)
{
   std::cout<<"Utils::WriteSettings AnaSettings to rootfile... "<<std::endl;
   int status = fHisto.WriteObject(sett,"ana_settings");
   if( status > 0 ) std::cout<<"Utils: Write AnaSettings Success!"<<std::endl;
}

void Utils::WriteHisto()
{
   std::cout<<"Utils::WriteHisto histograms to rootfile... "<<std::endl;
   int status = fHisto.Save();
   if( status > 0 ) std::cout<<"Utils: Write Histograms ok: "<<status<<std::endl;
}

// ===============================================================================================

TStoreEvent Utils::CreateStoreEvent(const std::vector<TSpacePoint>& points, const std::vector<TFitHelix>& hels, const std::vector<TFitLine>& lines)
{
   TStoreEvent evt;
   evt.SetEvent(points, lines, hels);
   return evt;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
